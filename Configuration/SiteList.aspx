﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.SiteList, App_Web_cjjrcgyq" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
        <div Class="section">

    <div cssClass="group">
    <h4>
                    <strong>
                <asp:Label ID="fsFindSite" runat="server" Text="Find Site" CssClass="label"></asp:Label>
            </strong></h4>
            <table style="width: 100%">
                <tr>
                    <td class="editTextBox" style="width: 59px">
                        <asp:Label ID="lblFindby" runat="server" Text="Find By" CssClass="label"></asp:Label>
                    </td>
                    <td class="editDropDown" style="width: 181px">
                        <asp:DropDownList ID="ddlSelectBy" runat="server" 
                            AutoPostBack="True" 
                            onselectedindexchanged="ddlSelectBy_SelectedIndexChanged" CssClass="textbox">
                            <asp:ListItem Value="0">Select By</asp:ListItem>
                            <asp:ListItem Value="1">Region</asp:ListItem>
                            <asp:ListItem Value="2">Site Name</asp:ListItem>
                            <asp:ListItem Value="3">Site Code</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 63px">
                        <asp:Label ID="lblValue" runat="server" Text="Value" CssClass="label"></asp:Label>
                    </td>
                    <td style="width: 151px">
                        <asp:TextBox ID="txtValue" runat="server" Visible="False" CssClass="textbox"></asp:TextBox>
                        <br />
                        <asp:DropDownList ID="ddlRegion" runat="server" DataTextField="RegionName" 
                            DataValueField="Id" Visible="False" 
                            AppendDataBoundItems="True" CssClass="textbox">
                            <asp:ListItem Value="-1">Select Region</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" />
                    </td>
                </tr>
            </table>
            </div>
  </div>
         <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
                <asp:Label ID="fsSiteList" runat="server" Text="Site List"></asp:Label>
            </h4>
            
             <asp:GridView ID="grvSiteList" runat="server" DataKeyNames="Id" AllowPaging="True" 
                 AlternatingRowStyle-CssClass="alt" AutoGenerateColumns="False" CellPadding="3" 
                 CssClass="mGrid" 
                 EnableModelValidation="True" GridLines="Horizontal" 
                 onrowdatabound="grvSiteList_RowDataBound" 
                 onselectedindexchanged="grvSiteList_SelectedIndexChanged" 
                 PagerStyle-CssClass="pgr" style="text-align: left" Width="100%">
                 <Columns>
                     <asp:BoundField DataField="Name" HeaderText="Name" />
                    <asp:BoundField DataField="Code" HeaderText="Code" />
                    <asp:BoundField DataField="City" HeaderText="City" />
                    <asp:BoundField DataField="Telephone1" HeaderText="Telephone 1" />
                    <asp:BoundField DataField="Telephone2" HeaderText="Telephone 2" />
                    <asp:BoundField DataField="Email" HeaderText="Email" />
                     <asp:TemplateField>
                         <ItemTemplate>
                             <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField></asp:TemplateField>
                     <asp:CommandField SelectText="Add Instrument" ShowSelectButton="True" />
                 </Columns>
                 <PagerStyle ForeColor="White" HorizontalAlign="Center" />
                 <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
             </asp:GridView>
            <br />
            <table style="width: 100%">
                <tr>
                    <td style="text-align: right">
                        <asp:LinkButton ID="lnkNew" runat="server" onclick="lnkNew_Click">Add New Site</asp:LinkButton>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <br />
       </asp:Panel>
</asp:Content>

