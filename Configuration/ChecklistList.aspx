﻿ 

<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.View.ChecklistList, App_Web_ap4qmzsw" stylesheettheme="Default" %>

 
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
   <div Class="section">

    <div cssClass="group">
    <h4>
                    <strong>
            <asp:Label ID="Label1" runat="server" Text="Find Checklist Task" CssClass="label"></asp:Label></strong></h4>
        
        <table style="width: 100%">
            <tr>
                <td style="width: 119px">
                    <asp:Label ID="Label3" runat="server" Text="Instrument" CssClass="label"></asp:Label>
                </td>
                <td style="width: 241px">
                    <asp:DropDownList ID="ddlInstrument" runat="server" AppendDataBoundItems="True" 
                        AutoPostBack="True" DataTextField="InstrumentName" 
                        DataValueField="Id" CssClass="textbox">
                        <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 136px">
                    <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click" 
                        Visible="False" />
                </td>
                <td style="width: 291px">
                    <asp:TextBox ID="txtTask" runat="server" Visible="False" CssClass="textbox"></asp:TextBox>
                    <asp:Label ID="Label2" runat="server" Text="Task" Visible="False"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </div></div>
      <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
            <asp:Label ID="Label4" runat="server" Text="Task List" CssClass="label"></asp:Label></h4>
       
        <asp:GridView ID="grvChecklist" runat="server" AutoGenerateColumns="False" 
            CellPadding="3" EnableModelValidation="True" ForeColor="#333333" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
            GridLines="Horizontal" 
            style="text-align: left" Width="100%" 
             AllowPaging="True" 
             onpageindexchanging="grvChecklist_PageIndexChanging" 
             onrowdatabound="grvChecklist_RowDataBound">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Task" />
                <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplEdit" runat="server" Text="Edit"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            
            <PagerStyle ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        </asp:GridView>
    
   
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click">Add New Task</asp:LinkButton>
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>

