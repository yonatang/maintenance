﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.InstrumentList, App_Web_cjjrcgyq" stylesheettheme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    
       <div Class="section">

    <div cssClass="group">
        
    <h4>
                    <strong>
                <asp:Label ID="fsFindSite" runat="server" Text="Find Instrument" CssClass="label"></asp:Label></strong></h4>
           
            <table style="width: 100%">
                <tr>
                    <td class="editTextBox" style="width: 59px">
                        <asp:Label ID="lblFindby" runat="server" Text="Find By" CssClass="label"></asp:Label>
                    </td>
                    <td class="editDropDown" style="width: 181px">
                        <asp:DropDownList ID="ddlSelectBy" runat="server" Width="150px" 
                            AutoPostBack="True" 
                            onselectedindexchanged="ddlSelectBy_SelectedIndexChanged" 
                            CssClass="textbox">
                            <asp:ListItem Value="0">Select By</asp:ListItem>
                            <asp:ListItem Value="1">Serial No.</asp:ListItem>
                            <asp:ListItem Value="2">Lot Number</asp:ListItem>
                            <asp:ListItem Value="3">Instrument Name</asp:ListItem>
                            <asp:ListItem Value="4">Site</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 63px">
                        <asp:Label ID="lblValue" runat="server" Text="Value" CssClass="label"></asp:Label>
                    </td>
                    <td style="width: 151px">
                        <asp:TextBox ID="txtValue" runat="server" Visible="False" CssClass="textbox"></asp:TextBox>
                        <br />
                        <asp:DropDownList ID="ddlInstrumentName" runat="server" DataTextField="InstrumentName" 
                            DataValueField="Id" Visible="False" 
                            AppendDataBoundItems="True" CssClass="textbox">
                            <asp:ListItem Value="0">Select Instrument Name</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                            onselectedindexchanged="ddlRegion_SelectedIndexChanged" Visible="False" 
                            CssClass="textbox">
                            <asp:ListItem Value="0">Select Region</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlSite" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                            Visible="False" CssClass="textbox">
                            <asp:ListItem Value="0">Select Site</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" />
                    </td>
                </tr>
            </table>
        </div></div>
        <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
                <asp:Label ID="fsSiteList" runat="server" Text="Instrument List" 
                    CssClass="label"></asp:Label></h4>
            
            <asp:GridView ID="grvInstrumentList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True" 
                    ForeColor="#333333" GridLines="Horizontal" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    onrowdatabound="grvInstrumentList_RowDataBound" 
                Width="100%" AllowPaging="True" 
                onpageindexchanging="grvInstrumentList_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument Name" />
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="LotNumber" HeaderText="Lot Number" />
                    <asp:BoundField DataField="SerialNo" HeaderText="SerialNo." />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle ForeColor="White" HorizontalAlign="Center" />
                
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
       
        <table style="width: 100%">
            <tr>
                <td style="text-align: right">
                    <asp:LinkButton ID="lnkNew" runat="server" onclick="lnkNew_Click">Add New Instrument</asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

