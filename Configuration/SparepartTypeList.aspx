﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.SparepartTypeList, App_Web_ap4qmzsw" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    
         <div Class="section">

    <div cssClass="group">
    <h4>
                    <strong>
                <asp:Label ID="fsFindSparepart" runat="server" Text="Find Sparepart Type" 
                        CssClass="label"></asp:Label></strong></h4>
            
            <table style="width: 100%">
                <tr>
                    <td class="editTextBox" style="width: 59px">
                        <asp:Label ID="lblFindby" runat="server" Text="Find By" CssClass="label"></asp:Label>
                    </td>
                    <td class="editDropDown" style="width: 181px">
                        <asp:DropDownList ID="ddlSelectBy" runat="server" 
                            onselectedindexchanged="ddlSelectBy_SelectedIndexChanged" 
                            AutoPostBack="True" CssClass="textbox">
                            <asp:ListItem Value="0">Select Criteria</asp:ListItem>
                            <asp:ListItem Value="1">Sparepart Name</asp:ListItem>
                            <asp:ListItem Value="2">Sparepart Part No.</asp:ListItem>
                            <asp:ListItem Value="3">Instrument</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 63px">
                        <asp:Label ID="lblValue" runat="server" Text="Value" CssClass="label"></asp:Label>
                    </td>
                    <td style="width: 151px">
                        <asp:TextBox ID="txtValue" runat="server" Visible="False" CssClass="textbox"></asp:TextBox>
                        <br />
                        <asp:DropDownList ID="ddlValue" runat="server" 
                            DataTextField="InstrumentName" DataValueField="Id" 
                            AppendDataBoundItems="True" Visible="False" CssClass="textbox">
                            <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" />
                    </td>
                </tr>
            </table>
        </div></div>
          <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
                <asp:Label ID="fs" runat="server" Text="Sparepart Type List" CssClass="label"></asp:Label></h4>
            
            <asp:GridView ID="grvSparepartTypeList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True" 
                    ForeColor="#333333" GridLines="Horizontal" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    onrowdatabound="grvRegionList_RowDataBound" 
                Width="100%" AllowPaging="True" 
                onpageindexchanging="grvSparepartTypeList_PageIndexChanging">
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <Columns>
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instruments" />
                    <asp:BoundField DataField="Name" HeaderText="Sparepart Name" />
                    <asp:BoundField DataField="PartNumber" HeaderText="Part Number" />
                    <asp:BoundField DataField="UOMName" HeaderText="UoM" />
                    <asp:BoundField DataField="ReorderQty" HeaderText="Reorder Qty" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle  ForeColor="White" HorizontalAlign="Center" />
                
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            
            <table style="width: 100%">
                <tr>
                    <td style="text-align: right">
                        <asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click">Add New Sparepart Type</asp:LinkButton>
                    </td>
                </tr>
            </table>
            
        </asp:Panel>
</asp:Content>

