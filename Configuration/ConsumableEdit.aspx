﻿ 

<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.View.ConsumableEdit, App_Web_ap4qmzsw" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <asp:ValidationSummary ID="vlsummery" runat="server" HeaderText="Error Message" 
        ValidationGroup="1" />
    <br />
     <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4><asp:Label ID="Label1" runat="server" Text="Consumable" CssClass="label"></asp:Label></h4>
    
    
    <table  style="width: 100%">
    <tr>
    <td>
        <asp:Label ID="lblConsumable" runat="server" Text="Consumable Name" 
            CssClass="label"></asp:Label>
        </td>

    <td>
        <asp:TextBox ID="txtConsumable" runat="server" ValidationGroup="1" 
            CssClass="textbox"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            Display="Dynamic" ErrorMessage="Consumable Required" 
            ValidationGroup="1" ControlToValidate="txtConsumable">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
    <td>
        <asp:Label ID="Label2" runat="server" Text="Description" CssClass="label"></asp:Label>
        </td>

    <td>
        <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" 
            CssClass="textboxDescShort"></asp:TextBox>
        </td>
    </tr>
     </table>
    
    </fieldset>
      <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" 
        ValidationGroup="1" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
        onclick="btnCancel_Click" />
    <asp:Button ID="btnDelete" runat="server" Text="Delete" 
        onclick="btnDelete_Click" />
        </asp:Panel>
</asp:Content>
