﻿ 
<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.ProblemTypeList, App_Web_cjjrcgyq" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
   <div Class="section">

    <div cssClass="group">
    <h4>
                    <strong>
            <asp:Label ID="Label1" runat="server" Text="Find Problem Type" CssClass="label"></asp:Label></strong></h4>
        </legend>
        <table style="width: 100%">
            <tr>
                <td style="width: 190px">
                    <asp:Label ID="Label4" runat="server" Text="Problem Type" CssClass="label"></asp:Label>
                </td>
                <td style="width: 277px">
                    <asp:TextBox ID="txtProblemtype" runat="server" CssClass="textbox"></asp:TextBox>
                </td>
                <td style="width: 103px">
                    <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click" />
                </td>
                <td style="width: 340px">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </div></div>
      <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
            <asp:Label ID="Label3" runat="server" Text="Problem Type List" CssClass="label"></asp:Label></h4>
        
        <asp:GridView ID="grvProblemTypeList" runat="server" AutoGenerateColumns="False" 
            CellPadding="3" EnableModelValidation="True" ForeColor="#333333" 
            GridLines="Horizontal" onrowdatabound="grvProblemTypeList_RowDataBound" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
            style="text-align: left" Width="100%" 
            onpageindexchanging="grvProblemTypeList_PageIndexChanging" 
             AllowPaging="True">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Problem Type" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplEdit" runat="server" Text="Edit"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
           
            <PagerStyle ForeColor="White" HorizontalAlign="Center" />
            
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        </asp:GridView>
    
    <table style="width: 100%">
        <tr>
            <td align="right">
                <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click">Add New Problem Type</asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Panel>
</asp:Content>
