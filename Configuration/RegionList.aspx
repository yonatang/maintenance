﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.RegionList, App_Web_dezozgvm" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register src="EditSite.ascx" tagname="EditSite" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
        <br />
    <link href="../App_Themes/Modal.css" rel="stylesheet" type="text/css" />
        <br />
        
            <div Class="section">

    <div cssClass="group">
    <h4>
                    <strong>
                    <asp:Label ID="fsFindRegion" runat="server" Text="Find Region" CssClass="label"></asp:Label>
                    </strong></h4>
               
                <table style="width: 100%">
                    <tr>
                        <td class="editTextBox" style="width: 59px">
                            <asp:Label ID="lblFindby" runat="server" Text="Find By" CssClass="label"></asp:Label>
                        </td>
                        <td class="editDropDown" style="width: 181px">
                            <asp:DropDownList ID="ddlSelectBy" runat="server" CssClass="textbox">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <asp:ListItem Value="1">Region Name</asp:ListItem>
                                <asp:ListItem Value="2">Region Code</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 63px">
                            <asp:Label ID="lblValue" runat="server" Text="Value" CssClass="label"></asp:Label>
                        </td>
                        <td style="width: 151px">
                            <asp:TextBox ID="txtValue" runat="server" CssClass="textbox"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" />
                        </td>
                    </tr>
                </table>
           </div>
  </div>
            <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
                    <asp:Label ID="fs" runat="server" Text="Region List"></asp:Label></h4>
               
                <asp:GridView ID="grvRegionList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id,RegionName,RegionCode" EnableModelValidation="True" 
                    GridLines="Horizontal" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    onrowdatabound="grvRegionList_RowDataBound" style="text-align: left" AllowPaging="True" 
                    onselectedindexchanged="grvRegionList_SelectedIndexChanged" Width="100%">
                    
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" />
                        <asp:BoundField DataField="RegionName" HeaderText="Region Name" />
                        <asp:BoundField DataField="RegionCode" HeaderText="Region Code" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField></asp:TemplateField>
                        <asp:CommandField SelectText="Add Site" ShowSelectButton="True" />
                    </Columns>
                    <PagerStyle ForeColor="White" HorizontalAlign="Center" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>
                <br />
            </asp:Panel>
 <table style="width: 100%">
         <tr>
             <td style="text-align: right">
                 &nbsp;
                 <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click" 
                     Visible="False">Add New Region</asp:LinkButton>
&nbsp;&nbsp;</td>
         </tr>
     </table>
</asp:Content>

