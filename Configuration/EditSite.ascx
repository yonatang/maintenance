﻿<%@ control language="C#" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.EditSite, App_Web_ap4qmzsw" %>

<style type="text/css">
    .style1
    {
        width: 100%;
        height: 3px;
    }

a
{
	color: #3333CC
}

    .style2
    {
        width: 36px;
    }
    .style3
    {
        width: 54px;
    }
</style>


<fieldset><legend>
            <asp:Label ID="lblSite" runat="server" Font-Bold="False" 
                style="font-size: small; font-family: Arial, Helvetica, sans-serif" 
                Text="Site"></asp:Label></legend>
<table class="style1">
    <tr>
        <td class="editDropDown">
            <asp:Label ID="lblSiteName" runat="server" Font-Bold="False" 
                style="font-size: small; font-family: Arial, Helvetica, sans-serif" 
                Text="Site Name"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtSiteName" runat="server" Width="250px"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="lblAddress" runat="server" Font-Bold="False" 
                style="font-size: small; font-family: Arial, Helvetica, sans-serif" 
                Text="Address"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtAddress" runat="server" Width="250px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="editDropDown">
            <asp:Label ID="lblSiteCode" runat="server" Font-Bold="False" 
                style="font-size: small; font-family: Arial, Helvetica, sans-serif" 
                Text="Site Code"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtSiteCode" runat="server" Width="200px"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="lblTel1" runat="server" Font-Bold="False" 
                style="font-size: small; font-family: Arial, Helvetica, sans-serif" 
                Text="Telephone1"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtTel1" runat="server" Width="200px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="editDropDown">
            <asp:Label ID="lblCity" runat="server" Font-Bold="False" 
                style="font-size: small; font-family: Arial, Helvetica, sans-serif" Text="City"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtCity" runat="server" Width="200px"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="lblTelephone2" runat="server" Font-Bold="False" 
                style="font-size: small; font-family: Arial, Helvetica, sans-serif" 
                Text="Telephone2"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txttel2" runat="server" Width="200px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="editDropDown">
            <asp:Label ID="lblSiteType" runat="server" Font-Bold="False" 
                style="font-size: small; font-family: Arial, Helvetica, sans-serif" 
                Text="Site Type"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="ddlSiteType" runat="server" Width="200px">
                <asp:ListItem Value="1">Hosipital</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:Label ID="lblEmail" runat="server" Font-Bold="False" 
                style="font-size: small; font-family: Arial, Helvetica, sans-serif" 
                Text="Email"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtEmail" runat="server" Width="250px"></asp:TextBox>
        </td>
    </tr>
</table>
    
</fieldset><table class="style1">
    <tr>
        <td class="style2">
            <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" />
        </td>
        <td class="style3">
            <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" 
                Text="Delete" />
        </td>
        <td>
            &nbsp;</td>
    </tr>
</table>
<br />
<fieldset><legend style="width: 54px">
            <asp:Label ID="lblSiteList" runat="server" Font-Bold="False" 
                style="font-size: small; font-family: Arial, Helvetica, sans-serif" 
                Text="Site List"></asp:Label></legend>

    <asp:GridView ID="grvRegionList" runat="server" AutoGenerateColumns="False"
            CellPadding="1" ForeColor="#333333" GridLines="Horizontal" Width="99%" 
            EnableModelValidation="True" 
        style="font-family: Arial, Helvetica, sans-serif; font-size: small" 
        onselectedindexchanged="grvRegionList_SelectedIndexChanged">
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Site Name" />
            <asp:BoundField DataField="Code" HeaderText="Site Code" />
            <asp:BoundField DataField="City" HeaderText="City" />
            <asp:CommandField SelectText="Edit" ShowSelectButton="True" />
        </Columns>
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>

</fieldset>