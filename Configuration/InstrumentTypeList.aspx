﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.InstrumentTypeList, App_Web_cjjrcgyq" stylesheettheme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    
         <div Class="section">

    <div cssClass="group">
    <h4>
                    <strong>
                <asp:Label ID="fsFindInstrumentType" runat="server" Text="Find Instrument Type" 
                        CssClass="label"></asp:Label></strong></h4>
            
            <table style="width: 100%">
                <tr>
                    <td class="editTextBox" style="width: 59px">
                        <asp:Label ID="lblFindby" runat="server" Text="Find By" CssClass="label"></asp:Label>
                    </td>
                    <td class="editDropDown" style="width: 181px">
                        <asp:DropDownList ID="ddlSelectBy" runat="server" 
                            onselectedindexchanged="ddlSelectBy_SelectedIndexChanged" 
                            AutoPostBack="True" CssClass="textbox">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="1">Instrument Catagory</asp:ListItem>
                            <asp:ListItem Value="2">Instrument Name</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 63px">
                        <asp:Label ID="lblValue" runat="server" Text="Value" CssClass="label"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtValue" runat="server" Visible="False" CssClass="textbox"></asp:TextBox>
                        <asp:DropDownList ID="ddlCatagory" runat="server" Visible="False" 
                            DataTextField="Name" DataValueField="Id" AppendDataBoundItems="True" 
                            CssClass="textbox">
                            <asp:ListItem Value="0">Select Category</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </div></div>
         <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
                <asp:Label ID="fs" runat="server" Text="Region List" CssClass="label"></asp:Label></h4>
           
            <asp:GridView ID="grvInstrumentType" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True" 
                    ForeColor="#333333" GridLines="Horizontal"  CssClass="mGrid" 
                 PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    onrowdatabound="grvInstrumentType_RowDataBound" Width="100%" 
                 onselectedindexchanged="grvInstrumentType_SelectedIndexChanged">
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <Columns>
                    <asp:BoundField DataField="Name" 
                        HeaderText="Instrument Category" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument Name" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField SelectText="Download Service Manual" 
                        ShowSelectButton="True" />
                </Columns>
                <PagerStyle ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            <br />
            <table style="width: 100%">
                <tr>
                    <td style="text-align: right">
                        <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click">Add New Instrument Type</asp:LinkButton>
                    </td>
                </tr>
            </table>
            </asp:Panel>
</asp:Content>

