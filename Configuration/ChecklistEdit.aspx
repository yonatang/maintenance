﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.View.ChecklistEdit, App_Web_cjjrcgyq" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <asp:ValidationSummary ID="vlsummery" runat="server" HeaderText="Error Message" 
        ValidationGroup="1" />
   <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4><asp:Label ID="Label1" runat="server" Text="Checklist" CssClass="label"></asp:Label></h4>
    
    
    <table  style="width: 100%">
    <tr>
    <td style="width: 165px">
        <asp:Label ID="Label2" runat="server" Text="Instrument" CssClass="label"></asp:Label>
        </td>

    <td>
        <asp:DropDownList ID="ddlInstrument" runat="server" AppendDataBoundItems="True" 
            DataTextField="InstrumentName" DataValueField="Id" CssClass="textbox">
            <asp:ListItem Value="0">Select Instrument</asp:ListItem>
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ControlToValidate="ddlInstrument" ErrorMessage="Select Instrument" 
            ValidationGroup="1">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
    <td style="width: 165px">
        <asp:Label ID="lblConsumable" runat="server" Text="Checklist" CssClass="label"></asp:Label>
        </td>

    <td>
        <asp:TextBox ID="txtChecklist" runat="server" ValidationGroup="1" 
            Width="499px" Height="45px" TextMode="MultiLine" 
            CssClass="textboxDescLong"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            Display="Dynamic" ErrorMessage="Checklist Required" 
            ValidationGroup="1" ControlToValidate="txtChecklist">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
    <td style="width: 165px">
        &nbsp;</td>

    <td>
        &nbsp;</td>
    </tr>
     </table>
    
    
    <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" 
        ValidationGroup="1" />
&nbsp;<asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" 
        Text="Cancel" />
&nbsp;<asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" 
        Text="Delete" />
        </asp:Panel>
</asp:Content>

