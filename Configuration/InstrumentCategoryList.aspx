﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.InstrumentCategoryList, App_Web_cjjrcgyq" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DefaultContent" Runat="Server">
      <div Class="section">

    <div cssClass="group">
    <h4>
                    <strong>
             <asp:Label ID="Label1" runat="server" Text="Find Instrument Category" CssClass="label"></asp:Label></strong></h4>
         
         <table style="width: 100%">
             <tr>
                 <td style="width: 190px">
                     <asp:Label ID="Label2" runat="server" Text="Instrument Category" 
                         CssClass="label"></asp:Label>
                 </td>
                 <td>
                     <asp:TextBox ID="txtInstCat" runat="server" CssClass="textbox"></asp:TextBox>
                     <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click" />
                 </td>
             </tr>
         </table>
     </div></div>

       <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
    <asp:Label ID="Label3" runat="server" Text="Instrument Category List"></asp:Label></h4>
   
    <asp:GridView ID="grvInstCatList" runat="server" CellPadding="2" 
        EnableModelValidation="True" ForeColor="#333333" GridLines="Horizontal" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
        Width="100%" AutoGenerateColumns="False" 
        onrowdatabound="grvInstCatList_RowDataBound" style="text-align: left">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Instrument Category" />
            <asp:BoundField DataField="Description" HeaderText="Description" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink ID="hplEdit" runat="server" Text="Edit"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        
        <PagerStyle  ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    </asp:GridView>
    
     
     <table style="width: 100%">
         <tr>
             <td style="text-align: right">
                 &nbsp;
                 <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click">Add New Instrument Catagory</asp:LinkButton>
             </td>
         </tr>
     </table>
     </asp:Panel>
 </asp:Content>