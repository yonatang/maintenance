﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.RegionEdit, App_Web_ap4qmzsw" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <br />
    <asp:ValidationSummary ID="vlsummery" runat="server" HeaderText="Error Message" 
        ValidationGroup="1" />
     <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
        <asp:Label ID="fsetRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
        </h4>
    <table style="width: 100%">
        <tr>
            <td style="height: 11px">
                <asp:Label ID="lblRegionName" runat="server" Text="Region Name" 
                    CssClass="label"></asp:Label>
            </td>
            <td style="height: 11px">
                <asp:TextBox ID="txtRegionName" runat="server" Width="300px" CssClass="textbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvRegion" runat="server" 
                    ErrorMessage="Region Name Required" ValidationGroup="1" 
                    ControlToValidate="txtRegionName" Display="Dynamic">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblRegionCode" runat="server" Text="Region Code" 
                    CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtRegionCode" runat="server" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMapId" runat="server" CssClass="label" Text="Map Id"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtmapId" runat="server" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
    </table>
    </fieldset>
    <br />
    <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" 
        ValidationGroup="1" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
        onclick="btnCancel_Click" />
    <asp:Button ID="btnDelete" runat="server" Text="Delete" 
        onclick="btnDelete_Click" />
        </asp:Panel>
</asp:Content>

