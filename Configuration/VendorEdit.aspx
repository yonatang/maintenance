﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.VendorEdit, App_Web_ap4qmzsw" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
 
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">






    <asp:ValidationSummary ID="vlsummery" runat="server" HeaderText="Error Message" 
        ValidationGroup="1" />
    <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4><asp:Label ID="Label1" runat="server" Text="Vendor" CssClass="label"></asp:Label></h4>
    
    
    <table  style="width: 100%">
    <tr>
    <td>
        <asp:Label ID="lblVendor" runat="server" Text="Vendor Name" CssClass="label"></asp:Label>
        </td>

    <td>
        <asp:TextBox ID="txtVendor" runat="server" ValidationGroup="1" 
            CssClass="textbox"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            Display="Dynamic" ErrorMessage="Vendor Required" 
            ValidationGroup="1" ControlToValidate="txtVendor">*</asp:RequiredFieldValidator>
        </td>
    </tr>
     </table>
    
  <br />
    <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" 
        ValidationGroup="1" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
        onclick="btnCancel_Click" />
    <asp:Button ID="btnDelete" runat="server" Text="Delete" 
        onclick="btnDelete_Click" />
        </asp:Panel>




</asp:Content>