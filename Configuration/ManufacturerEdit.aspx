﻿ 

<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.ManufacturerEdit, App_Web_cjjrcgyq" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <asp:ValidationSummary ID="vlsummery" runat="server" ValidationGroup="1" 
        HeaderText="Error Message" />
     <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
    <asp:Label ID="Label1" runat="server" Text="Manufacturer" CssClass="label"></asp:Label></h4>
    
    
    <table  style="width: 100%">
    <tr>
    <td>
        <asp:Label ID="lblManufacturer" runat="server" Text="Manufacturer" 
            CssClass="label"></asp:Label>
        </td>

    <td>
        <asp:TextBox ID="txtManufacturer" runat="server" ValidationGroup="1" 
            CssClass="textbox"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            Display="Dynamic" ErrorMessage="Manufacturer Required" 
            ValidationGroup="1" ControlToValidate="txtManufacturer">*</asp:RequiredFieldValidator>
        </td>
    </tr>
   
    </table>
    
    
    <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="1" 
        onclick="btnSave_Click" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
        onclick="btnCancel_Click" />
    <asp:Button ID="btnDelete" runat="server" Text="Delete" 
        onclick="btnDelete_Click" />
</asp:Panel>
</asp:Content>
