﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.InstrumentTypeEdit, App_Web_ap4qmzsw" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        ValidationGroup="1" />
  <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
       <asp:Label ID="lblInstrumentType" runat="server" Text="Instrument Type" 
            CssClass="label"></asp:Label></h4>
       </legend> <table style="width: 100%">
        <tr>
            <td class="editDropDown" style="width: 150px">
                <asp:Label ID="lblCatagoryId" runat="server" Text="Instrument Catagory" 
                    CssClass="label"></asp:Label>
            </td>
            <td colspan="5">
                <asp:DropDownList ID="ddlCatagory" runat="server" 
                    DataTextField="Name" DataValueField="Id" AppendDataBoundItems="True" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Instrument Catagory</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RfvCatagory" runat="server" 
                    ControlToValidate="ddlCatagory" Display="Dynamic" 
                    ErrorMessage="Catagory Required" InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 150px">
                <asp:Label ID="lblInstrumentName" runat="server" Text="Instrument Name" 
                    CssClass="label"></asp:Label>
            </td>
            <td colspan="5">
                <asp:TextBox ID="txtName" runat="server" CssClass="textbox" 
                    style="margin-left: 3px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txtName" Display="Dynamic" ErrorMessage="name Required" 
                    ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
        </tr>
          <tr>
              <td class="editDropDown" style="width: 150px">
                  <asp:Label ID="Label1" runat="server" Text="Service Manual"></asp:Label>
              </td>
              <td colspan="5">
                  <asp:FileUpload ID="FileUploadmnuservice" runat="server" />
              </td>
          </tr>
        <tr>
            <td class="editDropDown" style="width: 150px">
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" 
                    ValidationGroup="1" />
                &nbsp;
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    onclick="btnCancel_Click" />
            </td>
            <td>
                <asp:Button ID="btnDelete" runat="server" Text="Delete" 
                    onclick="btnDelete_Click" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table></asp:Panel>
</asp:Content>

