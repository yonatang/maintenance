﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.AutoNumberList, App_Web_cjjrcgyq" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
 
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <fieldset>
        <legend>
            <asp:Label ID="lblManufacturer" runat="server" Text="Find AutoNumbers"></asp:Label>
        </legend>
        <table style="width: 100%">
            <tr>
                <td style="width: 190px">
                    <asp:Label ID="Label2" runat="server" Text="Select Page"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlPage" runat="server" AppendDataBoundItems="True" 
                        AutoPostBack="True" Width="200px">
                        <asp:ListItem Value="0">Select a Page</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </fieldset><br />
      <fieldset><legend>
    <asp:Label ID="Label3" runat="server" Text="Auto-Numbers"></asp:Label>
    </legend>
    <asp:GridView ID="grvAutoNumberList" runat="server" CellPadding="3" 
        EnableModelValidation="True" ForeColor="#333333" GridLines="Horizontal" 
        Width="100%" AutoGenerateColumns="False" 
        onrowdatabound="grvAutoNumberList_RowDataBound" style="text-align: center">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="PageString" HeaderText="Page" />
            <asp:BoundField DataField="Prefix" HeaderText="Prefix" />
            <asp:BoundField DataField="Sequence" HeaderText="Sequence" />
            <asp:BoundField DataField="Status" HeaderText="Status" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink ID="hplEdit" runat="server" Text="Edit"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    </asp:GridView>
    </fieldset>
   <table style="width: 100%">
         <tr>
             <td style="text-align: right">
                 Add&nbsp;
                 <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click" 
                     Enabled="False">New</asp:LinkButton>
                 &nbsp;&nbsp;Auto-Number</td>
         </tr>
         </table>
</asp:Content>
