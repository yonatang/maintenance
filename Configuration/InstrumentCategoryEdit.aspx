﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.InstrumentCategoryEdit, App_Web_cjjrcgyq" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <asp:ValidationSummary ID="vlsummery" runat="server" ValidationGroup="1" 
        HeaderText="Error Message" />
     
     <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> <asp:Label ID="Label1" runat="server" Text="Instrument Category" 
            CssClass="label"></asp:Label></legend></h4>
    
    
    <table  style="width: 100%">
    <tr>
    <td>
        <asp:Label ID="lblInstCat" runat="server" Text="Instrument Category" 
            CssClass="label"></asp:Label>
        </td>

    <td>
        <asp:TextBox ID="txtInstCategory" runat="server" ValidationGroup="1" 
            Width="200px" CssClass="textbox"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            Display="Dynamic" ErrorMessage="Instrument Category Required" 
            ValidationGroup="1" ControlToValidate="txtInstCategory">*</asp:RequiredFieldValidator>
        </td>
    </tr>
     <tr>
    <td>
        <asp:Label ID="Label3" runat="server" Text="Description" CssClass="label"></asp:Label>
         </td>

    <td>
        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" 
            CssClass="textboxDescShort"></asp:TextBox>
         </td>
    </tr>
    </table>
    </asp:Panel>
    
    <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="1" 
        onclick="btnSave_Click" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
        onclick="btnCancel_Click1" />
    <asp:Button ID="btnDelete" runat="server" Text="Delete" 
        onclick="btnDelete_Click" />
&nbsp;
</asp:Content>
