﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.EditSite, App_Web_cjjrcgyq" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
<div Class="section">

    <div cssClass="group">
        
    <h4>
                    <strong><asp:Label ID="RegionInformation" runat="server" 
                        Text="Region Information" CssClass="label"></asp:Label></strong></h4>
                    
    <table style="width: 70%; height: 22px">
        <tr>
            <td>
                <asp:Label ID="lblRegionName" runat="server" Text="Region Name" 
                    CssClass="label"></asp:Label>
            </td>
            <td style="width: 153px">
                <asp:Label ID="lblGetRegionName" runat="server" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblRegionCode" runat="server" Text="Region Code" 
                    CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblGetRegionCode" runat="server" CssClass="label"></asp:Label>
            </td>
        </tr>
    </table>
    </div></div>
     <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
                    <asp:Label ID="lblEnterSite" runat="server" Text="Site Info" CssClass="label"></asp:Label></h4>

    <asp:ValidationSummary ID="ValidationSummarySite" runat="server" 
        HeaderText="Error" ValidationGroup="1" />
    <br />
<table width="100%">
    <tr>
        <td class="editDropDown">
            <asp:Label ID="lblSiteName" runat="server" Font-Bold="False" 
                CssClass="label"
                Text="Site Name"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtSiteName" runat="server" CssClass="textbox"></asp:TextBox>
            <cc1:FilteredTextBoxExtender ID="txtSiteName_FilteredTextBoxExtender" 
                runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters" 
                TargetControlID="txtSiteName" ValidChars="/_ ">
            </cc1:FilteredTextBoxExtender>
            <asp:RequiredFieldValidator ID="RqvSiteName" runat="server" 
                ControlToValidate="txtSiteName" ErrorMessage="Site Name Required" 
                ValidationGroup="1">*</asp:RequiredFieldValidator>
        </td>
        <td>
            <asp:Label ID="lblAddress" runat="server" Font-Bold="False" 
                CssClass="label"
                Text="Address"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtAddress" runat="server" CssClass="textbox"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="editDropDown">
            <asp:Label ID="lblSiteCode" runat="server" Font-Bold="False" 
                CssClass="label"
                Text="Site Code"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtSiteCode" runat="server" CssClass="textbox"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="lblTel1" runat="server" Font-Bold="False" 
                CssClass="label"
                Text="Telephone1"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtTel1" runat="server" ValidationGroup="1" CssClass="textbox"></asp:TextBox>
            <cc1:MaskedEditExtender ID="txtTel1_MaskedEditExtender" runat="server" 
                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                Mask="251999999999" MaskType="Number" TargetControlID="txtTel1">
            </cc1:MaskedEditExtender>
        </td>
    </tr>
    <tr>
        <td class="editDropDown">
            <asp:Label ID="lblCity" runat="server" Font-Bold="False" 
                CssClass="label"              

                Text="City"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtCity" runat="server" CssClass="textbox"></asp:TextBox>
            <cc1:FilteredTextBoxExtender ID="txtCity_FilteredTextBoxExtender" 
                runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters" 
                TargetControlID="txtCity" ValidChars=" /">
            </cc1:FilteredTextBoxExtender>
        </td>
        <td>
            <asp:Label ID="lblTelephone2" runat="server" Font-Bold="False" 
                CssClass="label"
                Text="Telephone2"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txttel2" runat="server" ValidationGroup="1" CssClass="textbox"></asp:TextBox>
            <cc1:MaskedEditExtender ID="txttel2_MaskedEditExtender" runat="server" 
                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                Mask="+251999999999" MaskType="Number" TargetControlID="txttel2">
            </cc1:MaskedEditExtender>
        </td>
    </tr>
    <tr>
        <td class="editDropDown">
            <asp:Label ID="lblSiteType" runat="server" Font-Bold="False" 
                CssClass="label"
                Text="Site Type"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="ddlSiteType" runat="server" 
                AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                CssClass="textbox">
                <asp:ListItem Value="0">Select Site Type</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RfvSiteType" runat="server" 
                ControlToValidate="ddlSiteType" ErrorMessage="Site Type Required" 
                InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator>
        </td>
        <td>
            <asp:Label ID="lblEmail" runat="server" Font-Bold="False" 
                                Text="Email" CssClass="label"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtEmail" runat="server" CssClass="textbox"></asp:TextBox>
        </td>
    </tr>
</table>
<table class="style1">
    <tr>
        <td class="style2">
            <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" 
                ValidationGroup="1" CommandName="Save" />
        </td>
        <td class="style3">
            <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" 
                Text="Delete" />
        </td>
        <td class="style3">
            <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click1" 
                Text="Cancel" />
        </td>
        <td>
            &nbsp;</td>
    </tr>
</table>
</asp:Panel>
 <asp:Panel ID="Panel1" runat="server" CssClass="group">
    <h4> 

            <asp:Label ID="lblSiteList" runat="server" Font-Bold="False" Text="Site List"></asp:Label></h4>
 <asp:GridView ID="grvRegionList" runat="server" AutoGenerateColumns="False"
            CellPadding="1" ForeColor="#333333" GridLines="Horizontal" Width="100%" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
            EnableModelValidation="True" 
               onselectedindexchanged="grvRegionList_SelectedIndexChanged" 
        DataKeyNames="Id" AllowPaging="True" 
        onpageindexchanging="grvRegionList_PageIndexChanging">
         <Columns>
            <asp:BoundField DataField="Name" HeaderText="Site Name" />
            <asp:BoundField DataField="Code" HeaderText="Site Code" />
            <asp:BoundField DataField="City" HeaderText="City" />
            <asp:BoundField DataField="Telephone1" HeaderText="Telephone 1" />
            <asp:BoundField DataField="Telephone2" HeaderText="Telephone 2" />
            <asp:CommandField SelectText="Edit" ShowSelectButton="True" />
        </Columns>
        <PagerStyle ForeColor="White" HorizontalAlign="Center" />
        
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
</asp:Panel>
</asp:Content>

