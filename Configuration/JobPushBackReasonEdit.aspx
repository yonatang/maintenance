﻿ 
<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.JobPushBackReasonEdit, App_Web_ap4qmzsw" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <asp:ValidationSummary ID="vlsummery" runat="server" ValidationGroup="1" 
        HeaderText="Error Message" />
     
     <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> <asp:Label ID="Label1" runat="server" Text="Reason" CssClass="label"></asp:Label></legend></h4>
    
    
    <table  style="width: 100%">
    <tr>
    <td>
        <asp:Label ID="lblReason" runat="server" Text="Reason" CssClass="label"></asp:Label>
        </td>

    <td>
        <asp:TextBox ID="txtReason" runat="server" ValidationGroup="1" Height="59px" 
            TextMode="MultiLine" CssClass="textboxDescShort"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            Display="Dynamic" ErrorMessage="Reason Required" 
            ValidationGroup="1" ControlToValidate="txtReason">*</asp:RequiredFieldValidator>
        </td>
    </tr>
   
    </table>
    
    
    <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="1" 
        onclick="btnSave_Click" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
        onclick="btnCancel_Click" />
    <asp:Button ID="btnDelete" runat="server" Text="Delete" 
        onclick="btnDelete_Click" />
</asp:Panel>

</asp:Content>

