﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.ErrorCodeList, App_Web_ap4qmzsw" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
 <div Class="section">

    <div cssClass="group">
    <h4>
                    <strong> <asp:Label ID="Label1" runat="server" Text="Find Error Code" 
                        CssClass="label"></asp:Label></strong></h4>
    
    
        <table>
            <tr>
                <td >
                    <asp:Label ID="Label2" runat="server" Text="Error Code" CssClass="label"></asp:Label>
                </td>
                <td  >
                    <asp:TextBox ID="txtErrorCode" runat="server" CssClass="textbox"></asp:TextBox>
                </td>
                <td  >
                    <asp:Label ID="Label4" runat="server" Text="Problem Type" CssClass="label"></asp:Label>
                </td>
                <td >
                    <asp:DropDownList ID="ddlProblemType" runat="server" 
                        AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                        CssClass="textbox">
                        <asp:ListItem Value="0">Select Problem Type</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click" />
                </td>
            </tr>
        </table>
    </div>
    </div>

     <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4>        <asp:Label ID="Label3" runat="server" Text="Error Code List"></asp:Label></h4>
       
        <asp:GridView ID="grvErrorCodeList" runat="server" AutoGenerateColumns="False" 
            CellPadding="3" EnableModelValidation="True" ForeColor="#333333" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
            GridLines="Horizontal" onrowdatabound="grvErrorCodeList_RowDataBound" 
            style="text-align: left" Width="100%" 
            onpageindexchanging="grvErrorCodeList_PageIndexChanging" 
            AllowPaging="True">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="ProblemTypeName" HeaderText="Problem Type" />
                <asp:BoundField DataField="Name" HeaderText="Error Code" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplEdit" runat="server" Text="Edit"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            
            <PagerStyle ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        </asp:GridView>
</asp:Panel>

    <table style="width: 100%">
        <tr>
            <td style="text-align: right">

    <asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click">Add New Error Code</asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Content>


