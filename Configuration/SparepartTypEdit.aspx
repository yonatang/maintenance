﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.SparepartTypEdit, App_Web_ap4qmzsw" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <asp:ValidationSummary ID="SparepartErrorSummary" runat="server" 
        HeaderText="Error" Height="67px" ValidationGroup="1" />
    <br />
     <asp:Panel ID="Panel2" runat="server" CssClass="group" Width="100%">
    <h4>
       <asp:Label ID="lblSparepartType" runat="server" Text="Sparepart Type" 
            CssClass="label"></asp:Label></h4>
       <table style="width: 100%">
        <tr>
            <td>
                <asp:Label ID="lblCatagory" runat="server" Text="Instrument" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlCatagory" runat="server" 
                    AppendDataBoundItems="True" DataTextField="InstrumentName" 
                    DataValueField="Id" CssClass="textbox">
                    <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcatagory" runat="server" 
                    ControlToValidate="ddlCatagory" Display="Dynamic" 
                    ErrorMessage="Catagory Required" InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
            <td >
                <asp:Label ID="lblUom" runat="server" Text="Unit of Measurment" 
                    CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlUom" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Unit Of Meaurement</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="height: 9px" >
                <asp:Label ID="lblName" runat="server" Text="Sparepart Name" CssClass="label"></asp:Label>
            </td>
            <td style="height: 9px" >
                <asp:TextBox ID="txtName" runat="server" CssClass="textbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvName" runat="server" 
                    ControlToValidate="txtName" ErrorMessage="Sparepart Name Required " 
                    ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
            <td style="height: 9px">
                <asp:Label ID="lblReorderQty" runat="server" Text="Reorder Qty" 
                    CssClass="label"></asp:Label>
            </td>
            <td style="height: 9px">
                <asp:TextBox ID="txtQty" runat="server" CssClass="textbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvQty" runat="server" 
                    ControlToValidate="txtQty" ErrorMessage="Reorder Qty Required" 
                    ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblpartNo" runat="server" Text="Part Number" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtpartno" runat="server" CssClass="textbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvPartNo" runat="server" 
                    ControlToValidate="txtpartno" Display="Dynamic" 
                    ErrorMessage="Part Number Required" ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Label ID="lblDescription" runat="server" Text="Description" 
                    CssClass="label"></asp:Label>
            </td>
            <td rowspan="2">
                <asp:TextBox ID="txtDescription" runat="server" Height="42px" 
                    TextMode="MultiLine" Width="250px" CssClass="textboxDescShort"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" 
                    ValidationGroup="1" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    onclick="btnCancel_Click" />
                <asp:Button ID="btnDelete" runat="server" Text="Delete" 
                    onclick="btnDelete_Click" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>

