﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.SiteTypeList, App_Web_cjjrcgyq" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="DefaultContent" Runat="Server">
       <div Class="section">

    <div cssClass="group">
    <h4>
                    <strong>
            <asp:Label ID="lblsitetpe" runat="server" Text="Find Site Type" CssClass="label"></asp:Label> </strong></h4>
        
        <table style="width: 100%">
            <tr>
                <td style="width: 190px">
                    <asp:Label ID="Label2" runat="server" Text="Site Type" CssClass="label"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSiteType" runat="server" CssClass="textbox"></asp:TextBox>
                    <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click" />
                </td>
            </tr>
        </table>
    </div></div>

           <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
    <asp:Label ID="Label3" runat="server" Text="Site Type List"></asp:Label>
    </h4>
    <asp:GridView ID="grvSiteTypeList" runat="server" CellPadding="3" 
        EnableModelValidation="True" ForeColor="#333333" GridLines="Horizontal" 
        Width="100%" AutoGenerateColumns="False"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
        onrowdatabound="grvSiteTypeList_RowDataBound" style="text-align: left">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Site Type" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink ID="hplEdit" runat="server" Text="Edit"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        
        <PagerStyle ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    </asp:GridView>
          <table style="width: 100%">
         <tr>
             <td style="text-align: right">
                 &nbsp;<asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click">Add New Site Type</asp:LinkButton>
                 &nbsp;</td>
         </tr>
         </table>
         </asp:Panel>
</asp:Content>