﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Configuration.Views.InstrumentEdit, App_Web_ap4qmzsw" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <div Class="section">

    <div cssClass="group">
        
    <h4> <strong><asp:Label ID="SiteInformation" runat="server" Text="Site Information" 
            CssClass="label"></asp:Label></strong></h4>
    <table style="width: 100%">
        <tr>
            <td>
                <asp:Label ID="lblSiteName" runat="server" Text="Site Name" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblResultName" runat="server" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblSiteCode" runat="server" Text="Site Code" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblresultCode" runat="server" CssClass="label"></asp:Label>
            </td>
        </tr>
    </table></div></div>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        HeaderText="Error" ValidationGroup="1" />
    
     <asp:Panel ID="Panel2" runat="server" CssClass="group" 
        style="left: 0px; top: 0px; width: 844px">
    <h4> 
        <asp:Label ID="lblInstrument" runat="server" Text="Instrument" CssClass="label"></asp:Label></h4>
       <table style="width: 100%">
        <tr>
            <td style="width: 137px">
                <asp:Label ID="lblInstrumentName" runat="server" Text="Instrument Name" 
                    CssClass="label"></asp:Label>
            </td>
            <td style="width: 258px">
                <asp:DropDownList ID="ddlInsName" runat="server" 
                    AppendDataBoundItems="True" DataTextField="InstrumentName" 
                    DataValueField="Id" CssClass="textbox" style="margin-left: 0px">
                    <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                    <asp:ListItem Value="1">AUTOLAB 18 and SABA</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RfvInstrumentName" runat="server" 
                    ControlToValidate="ddlInsName" Display="Dynamic" 
                    ErrorMessage="Select Instrument Name" InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Label ID="lblUnderServiceContract" runat="server" 
                    Text="Is Under Service Contract" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:CheckBox ID="chkContract" runat="server" CssClass="label" />
            </td>
        </tr>
        <tr>
            <td style="width: 137px">
                <asp:Label ID="lblLotNumber" runat="server" Text="Lot Number" CssClass="label"></asp:Label>
            </td>
            <td style="width: 258px">
                <asp:TextBox ID="txtLotNumber" runat="server" CssClass="textbox"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lblContractWith_Id" runat="server" Text="Contract With" 
                    CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlContractWith" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Vendor</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 137px">
                <asp:Label ID="lblSerialNo" runat="server" Text="Serial No." CssClass="label"></asp:Label>
            </td>
            <td style="width: 258px">
                <asp:TextBox ID="txtSerialNo" runat="server" CssClass="textbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvSerialno" runat="server" 
                    ControlToValidate="txtSerialNo" Display="Dynamic" 
                    ErrorMessage="Serial Number Required" ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Label ID="lblPreventiveMaintenancePeriod" runat="server" CssClass="label" 
                    Text="Preventive Maintenance Period"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlPeriod" runat="server" CssClass="textbox">
                    <asp:ListItem Value="&quot;&quot;">Select Period</asp:ListItem>
                    <asp:ListItem Value="1Year">1 Year</asp:ListItem>
                    <asp:ListItem Value="6Month">6 Month</asp:ListItem>
                    <asp:ListItem Value="3Month">3 Month</asp:ListItem>
                    <asp:ListItem Value="1Month">1 Month</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 137px">
                <asp:Label ID="lblManufacturer" runat="server" CssClass="label" 
                    Text="Manufacturer"></asp:Label>
            </td>
            <td style="width: 258px">
                <asp:DropDownList ID="ddlManufacturer" runat="server" 
                    AppendDataBoundItems="True" CssClass="textbox" DataTextField="Name" 
                    DataValueField="Id">
                    <asp:ListItem Value="0">Select Manufacturer</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="lblIsFunctional" runat="server" CssClass="label" 
                    Text="Is Functional"></asp:Label>
            </td>
            <td>
                <asp:CheckBox ID="chkIsFunctional" runat="server" CssClass="label" />
            </td>
        </tr>
        <tr>
            <td style="width: 137px; height: 32px;">
                <asp:Label ID="lblVendor" runat="server" CssClass="label" Text="Vendor"></asp:Label>
            </td>
            <td style="width: 258px; height: 32px;">
                <asp:DropDownList ID="ddlVendor" runat="server" AppendDataBoundItems="True" 
                    CssClass="textbox" DataTextField="Name" DataValueField="Id" 
                    onselectedindexchanged="ddlVendor_SelectedIndexChanged">
                    <asp:ListItem Value="0">Select Vendor</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td style="height: 32px">
                <asp:Label ID="lblWarranityExpireDate" runat="server" CssClass="label" 
                    Text="Warranity Expire Date"></asp:Label>
            </td>
            <td style="height: 32px">
                <cc1:Calendar ID="Calendar2" runat="server" Height="63px" />
                <asp:RegularExpressionValidator ID="REVWExpireDate" runat="server" 
                    ControlToValidate="Calendar2" 
                    ErrorMessage="Warraniity Expire Date Is Not Valid" 
                    ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                    ValidationGroup="1">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 137px">
                <asp:Label ID="lblInstrallationDate" runat="server" CssClass="label" 
                    Text="Installation Date"></asp:Label>
            </td>
            <td style="width: 258px">
                <cc1:Calendar ID="Calendar1" runat="server" Height="47px" />
                <asp:RegularExpressionValidator ID="REVInstallationDate" runat="server" 
                    ControlToValidate="Calendar1" ErrorMessage="Installation Date Is Not Valid" 
                    ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                    ValidationGroup="1">*</asp:RegularExpressionValidator>
            </td>
            <td>
                <asp:Label ID="lblLastPreventiveMaintenanceDate" runat="server" 
                    CssClass="label" Text="Last Preventive Maintenance Date"></asp:Label>
            </td>
            <td>
                <cc1:Calendar ID="Calendar3" runat="server" Height="58px" />
                <asp:RegularExpressionValidator ID="REVLPMDate" runat="server" 
                    ControlToValidate="Calendar1" 
                    ErrorMessage="Last Preventive Maintenance Date Is Not Valid" 
                    ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                    ValidationGroup="1">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" 
                    ValidationGroup="1" />
                <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" 
                    Text="Cancel" />
                <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" 
                    Text="Delete" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table></asp:Panel>
<asp:Panel ID="Panel1" runat="server" CssClass="group">
    <h4> 
            <strong>
            <asp:Label ID="lblInstrumentList" runat="server" CssClass="label" 
                Font-Bold="False" Text="Instrument List"></asp:Label>
            </strong></h4>
 <asp:GridView ID="grvInstrumentList" runat="server" AutoGenerateColumns="False"
            CellPadding="1" ForeColor="#333333" GridLines="Horizontal" Width="100%" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
            EnableModelValidation="True" 
            onselectedindexchanged="grvInstrumentList_SelectedIndexChanged" 
        DataKeyNames="Id" AllowPaging="True" 
        onpageindexchanging="grvInstrumentList_PageIndexChanging">
        
        <Columns>
            <asp:BoundField DataField="InstrumentName" HeaderText="Instrument Name" />
            <asp:BoundField DataField="LotNumber" HeaderText="Lot Number" />
            <asp:BoundField DataField="SerialNo" HeaderText="Serial No." />
            <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
            <asp:CommandField SelectText="Edit" ShowSelectButton="True" />
        </Columns>
        <PagerStyle ForeColor="White" HorizontalAlign="Center" />
        
    </asp:GridView>
</asp:Panel>
</asp:Content>

