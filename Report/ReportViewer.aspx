﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Report.Views.ReportViewer, App_Web_jzrke3zq" stylesheettheme="Default" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
 
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">

     
   <div Class="section">

    <div cssClass="group">
    <h4>
                    <strong>
        <asp:Label ID="lblCriteria" runat="server" Text="Select Criteria" CssClass="label"></asp:Label>
        </strong></h4>
    <table style="width: 100%">
        <tr>
            <td>
                <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
            </td>
            <td style="width: 295px">
                <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                    AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                    onselectedindexchanged="ddlRegion_SelectedIndexChanged" CssClass="textbox">
                    <asp:ListItem Value="-1">Select Region</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="lblSiteType" runat="server" Text="Site Type" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlSiteType" runat="server" AppendDataBoundItems="True" 
                    DataTextField="Name" DataValueField="Id" CssClass="textbox">
                    <asp:ListItem Value="0">Select Site Type</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
            </td>
            <td style="width: 295px">
                <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                    DataTextField="Name" DataValueField="Id" CssClass="textbox">
                    <asp:ListItem Value="0">Select Site</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="lblManufacturer" runat="server" Text="Manufacturer" 
                    CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlManufacturer" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Manufacturer</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblInstrumentCatagory" runat="server" Text="Instrument Catagory" 
                    CssClass="label"></asp:Label>
            </td>
            <td style="width: 295px">
                <asp:DropDownList ID="ddlInstrumentCatagory" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Instrument Catagory</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Consumable" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlConsumables" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Consumable</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblDateFrom" runat="server" Text="Date From" CssClass="label"></asp:Label>
            </td>
            <td style="width: 295px">
                <cc1:Calendar ID="CalDateFrom" runat="server" Width="200px" 
                    SelectedDate="1909-01-01" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                            ControlToValidate="CalDateFrom" 
                            ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1" style="font-size: small">*</asp:RegularExpressionValidator>
            </td>
            <td>
                <asp:Label ID="lblDateTo" runat="server" Text="Date To" CssClass="label"></asp:Label>
            </td>
            <td>
                <cc1:Calendar ID="CalDateTo" runat="server" Width="200px" 
                    SelectedDate="2012-01-20" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                            ControlToValidate="CalDateTo" 
                            ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1" style="font-size: small">*</asp:RegularExpressionValidator>
            <asp:Button ID="btnView" runat="server" onclick="btnView_Click" Text="View" 
                    Height="22px" Width="55px" />
            </td>
        </tr>
    </table></div></div>
     <asp:Panel ID="Panel2" runat="server" CssClass="group">
    
            <rsweb:ReportViewer ID="Viewer" runat="server" Width="100%" 
        DocumentMapWidth="100%">
            </rsweb:ReportViewer></asp:Panel>
        </asp:Content>

