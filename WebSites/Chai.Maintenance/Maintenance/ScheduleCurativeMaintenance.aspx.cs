﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class ScheduleCurativeMaintenance : Microsoft.Practices.CompositeWeb.Web.UI.Page, IScheduleCurativeMaintenanceView
    {
        private ScheduleCurativeMaintenancePresenter _presenter;
        private Problem _Problem;
        private Instrument _instrument;
        private Schedule _Schedule;        
        private IList<User> _userList;
        private IList<User> _externalUserList;
        private IList<Schedule> scheduleList;

       static IList<DateTime> busyDays = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();

            if (!Page.IsPostBack)
                BindControls();
            if (Mode == 0 || Mode == 1)
            {
                if (InstrumentId != 0 && ProblemId != 0)
                {
                    if(Mode == 1)
                        lblEditing.Text = "Scheduling Escalated Job...";
                    _instrument = _presenter.GetInstrumentByIdForProblem(InstrumentId);
                    _Problem = _presenter.GetProblemByProblemId(ProblemId);
                    lblSelectedinstrument.Text = _instrument.InstrumentName;
                    lblInstallationDate.Text = _instrument.InstallationDate.ToString();
                    lblregionName.Text = _instrument.RegionName;
                    lblSiteName.Text = _instrument.SiteName;
                    
                    lblProblemtype.Text = _Problem.ProblemTypeName;
                    lblErrorCode.Text = _Problem.ErrorCodeName;
                    txtDescription.Text = _Problem.Description;
                }
                else if (InstrumentIdEdit != 0 && ProblemIdEdit != 0)
                {
                   
                    lblEditing.Visible = true;
                    _instrument = _presenter.GetInstrumentByIdForProblem(InstrumentIdEdit);
                    _Problem = _presenter.GetProblemByProblemId(ProblemIdEdit);
 
                    lblSelectedinstrument.Text = _instrument.InstrumentName;
                    lblInstallationDate.Text = _instrument.InstallationDate.ToString();
                    lblregionName.Text = _instrument.RegionName;
                    lblSiteName.Text = _instrument.SiteName;
                    lblProblemtype.Text = _Problem.ProblemTypeName;
                    lblErrorCode.Text = _Problem.ErrorCodeName;
                    txtDescription.Text = _Problem.Description;
                }
            }
            else if (Mode == 2)
            {
                lblEditing.Visible = true;
                lblEditing.Text = "Re-Scheduling Pushed Job...";
                _instrument = _presenter.GetInstrumentByIdForProblem(EscalatedInstrumentId);
                _Problem = _presenter.GetProblemByProblemId(EscalatedProblemId);
                lblSelectedinstrument.Text = _instrument.InstrumentName;
                lblInstallationDate.Text = _instrument.InstallationDate.ToString();
                lblregionName.Text = _instrument.RegionName;
                lblSiteName.Text = _instrument.SiteName;
                lblProblemtype.Text = _Problem.ProblemTypeName;
                lblErrorCode.Text = _Problem.ErrorCodeName;
                txtDescription.Text = _Problem.Description;
            }
            if (_instrument != null) 
            {
                if (_instrument.UnderServiceContract)
                {
                    lblContract.Visible = _instrument.UnderServiceContract;
                    lblcontractedWith.Visible = _instrument.UnderServiceContract;
                    lblWarantiyDate.Visible = _instrument.UnderServiceContract;
                    lblexpireDate.Visible = _instrument.UnderServiceContract;
                    lblcontractedWith.Text = _instrument.ContractedWith;
                    lblWarantiyDate.Text = _instrument.WarranityExpireDate.ToString();
                }
            }
            BindEnginers();
           
        }

        [CreateNew]
        public ScheduleCurativeMaintenancePresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindEnginers()
        {
            if (!IsPostBack)
            {
                _userList = _presenter.GetEnginersList();
                ddlEnginer.DataSource = _userList;
                ddlEnginer.DataBind();
            }
        }
        private void BindExternalEnginers()
        {
            //if (!IsPostBack)
            //{
                _externalUserList = _presenter.GetExternalUserList();
                ddlExternalEnginer.DataSource = _externalUserList;
                ddlExternalEnginer.DataBind();
            //}
        }

        private void BindControls()
        {
            if (_Schedule.IsExternal)
            {
                this.chkAssign.Checked = true;
                this.ddlExternalEnginer.SelectedValue = _Schedule.EnginerId.ToString();
                checkAssignExternal();
            }
            else
            {
                this.ddlEnginer.SelectedValue = _Schedule.EnginerId.ToString();
            }
            this.CalendarFrom.Text = _Schedule.ScheduledDateFrom.ToString();
            this.CalendarTo.Text = _Schedule.ScheduledDateTo.ToString();
            this.txtdesc.Text = _Schedule.Description;
            this.CalendarDate.Text = _Schedule.Date.ToString();
            this.btnDelete.Visible = (_Schedule.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");
        }

        private void SaveSchedule()
        {
            try
            {
                if (_Schedule.IsNew())
                {
                    if (Mode == 0)
                        _presenter.SaveOrUpdateSchedule(_Schedule, "Curative", 0);
                    else if (Mode == 1)
                        _presenter.SaveOrUpdateSchedule(_Schedule, "EscalatedCurative", EscalateId);
                    else if(Mode==2)
                        _presenter.SaveOrUpdateSchedule(_Schedule, "Rescheduling", ScheduleIdForPushed);
                    Master.TransferMessage(new AppMessage("Curative Maintenance Scheduled", RMessageType.Info));
                    //_presenter.UpdateStatus("CurativeSchedule", 3, _Schedule.ProblemId, "");// changes the notifed problem status to scheduled
                    _presenter.CancelPage(_instrument.Id);
                }
                else
                {
                    _presenter.SaveOrUpdateSchedule(_Schedule,"", 0);
                    Master.ShowMessage(new AppMessage("Curative Maintenance Scheduled", RMessageType.Info));

                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlExternalEnginer.SelectedValue) == 0 & Convert.ToInt32(ddlEnginer.SelectedValue) == 0)
            {
                errormsg.Visible = true;
                errormsg.Text = "Please select an engineer";
            }
            else
            {
                if (Page.IsValid)
                {
                    if (Convert.ToInt32(ddlEnginer.SelectedValue) != 0)
                    {
                        _Schedule.EnginerId = Convert.ToInt32(ddlEnginer.SelectedValue);
                        _Schedule.IsExternal = false;
                    }
                    else if (chkAssign.Checked && Convert.ToInt32(ddlExternalEnginer.SelectedValue) != 0)
                    {
                        _Schedule.EnginerId = Convert.ToInt32(ddlExternalEnginer.SelectedValue);
                        _Schedule.IsExternal = chkAssign.Checked;
                    }

                    _Schedule.ScheduledDateFrom = Convert.ToDateTime(CalendarFrom.Text);
                    _Schedule.ScheduledDateTo = Convert.ToDateTime(CalendarTo.Text);
                    _Schedule.Description = txtdesc.Text;
                    _Schedule.Date = Convert.ToDateTime(CalendarDate.Text);
                    if (Mode == 0)
                    {
                        if (ProblemIdEdit == 0 && InstrumentIdEdit == 0)
                        {
                            _Schedule.ProblemId = ProblemId;
                            _Schedule.InstrumentId = InstrumentId;
                        }
                        else
                        {
                            _Schedule.ProblemId = ProblemIdEdit;
                            _Schedule.InstrumentId = InstrumentIdEdit;
                        }
                    }
                    else if (Mode == 1) // Escalated
                    {
                        _Schedule.ProblemId = EscalatedProblemId;
                        _Schedule.InstrumentId = EscalatedInstrumentId;
                    }
                    else if (Mode == 2) //re-scheuling
                    {
                        _Schedule.ProblemId = ProblemId;
                        _Schedule.InstrumentId = InstrumentId;
                    }
                    _Schedule.SiteId = _Problem.SiteId;
                    _Schedule.UserId = _presenter.GetLocationId()[2];
                    _Schedule.CurativeScheduleStatus = "Not Performed";
                    SaveSchedule();
                }
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {

            Response.Redirect("ScheduleCurativeMaintenanceList.aspx");

        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteSchedule(_Schedule.Id);
                Master.ShowMessage(new AppMessage("Schedule was deleted successfully", Chai.Maintenance.Enums.RMessageType.Info));

            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Delete Failed. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            
            Response.Redirect(String.Format("~/Maintenance/ScheduleCurativeMaintenanceList.aspx?{0}=2", AppConstants.TABID));
        }
        public int InstrumentId
        {
            get 
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentId"]);
                else
                    return 0;
            }
        }
        public int ProblemId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ProblemId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ProblemId"]);
                else
                    return 0;
            }
        }
        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ScheduleId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ScheduleId"]);
                else
                    return 0;

            }
        }
        public int InstrumentIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]);
                else
                    return 0;
            }
        }
        public int ProblemIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ProblemIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ProblemIdEdit"]);
                else
                    return 0;
            }
        }
        public int ScheduleIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ScheduleIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ScheduleIdEdit"]);
                else
                    return 0;
            }
        }
        public int EscalatedInstrumentId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentId"]);
                else
                    return 0;
            }
        }
        public int EscalatedProblemId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ProblemId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ProblemId"]);
                else
                    return 0;
            }
        }
        public int Mode
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["Mode"]) != 0)
                    return Convert.ToInt32(Request.QueryString["Mode"]);
                else
                    return 0;
            }
        }
        public int ScheduleIdForPushed
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ScheduleIdForPushed"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ScheduleIdForPushed"]);
                else
                    return 0;
            }
        }
        public int EscalateId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["EscalateId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["EscalateId"]);
                else
                    return 0;
            }
        }
        Schedule IScheduleCurativeMaintenanceView._Schedule
        {
            get { return _Schedule; }
            set { _Schedule = value; }
        }

        private  void checkAssignExternal()
        {

            if (chkAssign.Checked)
            {
                BindExternalEnginers();
                lblexternalegn.Visible = true;
                ddlExternalEnginer.Visible = true;
                inlineexternal.Visible = true;
                lblenginer.Visible = false;
                ddlEnginer.Visible = false;
                busyDays = null;
                Calendar2.Visible = false;
                Image1.Visible = false;
            }
            else
            {

                lblexternalegn.Visible = false;
                ddlExternalEnginer.Visible = false;
                inlineexternal.Visible = false;

                lblenginer.Visible = true;
                ddlEnginer.Visible = true;
            }
        }

        protected void chkAssign_CheckedChanged(object sender, EventArgs e)
        {
            checkAssignExternal();
           
        }

     
        protected void ddlEnginer_SelectedIndexChanged(object sender, EventArgs e) 
        {
            if (Convert.ToInt32(ddlEnginer.SelectedValue) != 0)
            {
                scheduleList = _presenter.GetSchedulesByEnginerId(Convert.ToInt32(ddlEnginer.SelectedValue));
                busyDays = new List<DateTime>();
                foreach (Schedule items in scheduleList)
                {
                    DateTime scheduledDates = items.ScheduledDateFrom;
                    while (items.ScheduledDateTo >= scheduledDates)
                    {

                        busyDays.Add(new DateTime(scheduledDates.Year, scheduledDates.Month, scheduledDates.Day));
                        scheduledDates = scheduledDates.AddDays(1);
                    }
                }
                    Calendar2.Visible = true;
                    Image1.Visible = true;
 
            }
            else
            {
                busyDays = null;
                Calendar2.Visible = false;
                Image1.Visible = false;
            }
        }


      
        protected void Calendar2_DayRender1(object sender, DayRenderEventArgs e)
        {
            e.Day.IsSelectable = false;
            if (busyDays != null)
            {
                if (busyDays.Contains(e.Day.Date))
                {
                    e.Cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffaaff");
                }
            }
        }
}
}