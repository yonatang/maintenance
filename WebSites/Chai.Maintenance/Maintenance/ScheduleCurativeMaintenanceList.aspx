﻿ 

<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ScheduleCurativeMaintenanceList.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.ScheduleCurativeMaintenanceList" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
        <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Find Instrument with Reported Problems</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   

  
        
                        <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
                    <label class="select">
                        <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                            onselectedindexchanged="ddlRegion_SelectedIndexChanged" 
                            Enabled="False" CssClass="textbox">
                            <asp:ListItem Value="0">Select Region</asp:ListItem>
                        </asp:DropDownList><i></i></label></section>
                    <section class="col col-6">   
                        <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                  <label class="select">
                        <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                            DataTextField="Name" DataValueField="Id"  AutoPostBack="True" 
                            Enabled="False" CssClass="textbox">
                            <asp:ListItem Value="0">Select Site</asp:ListItem>
                        </asp:DropDownList><i></i></label></section>
                    </div>
            <div class="row">
									<section class="col col-6">   
                   
                        <asp:Label ID="lblInstrument" runat="server" Text="Instrument Category" 
                            CssClass="label"></asp:Label>
                     <label class="select">
                        <asp:DropDownList ID="ddlInstrumentCategory" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             AutoPostBack="True" CssClass="textbox">
                            <asp:ListItem Value="0">Select Instrument Category</asp:ListItem>
                        </asp:DropDownList><i></i></label></section>
                   <section class="col col-6">   
                        <asp:Label ID="lblProblemType" runat="server" Text="Problem Type" 
                            CssClass="label"></asp:Label>
                    <label class="select">
                        <asp:DropDownList ID="ddlproblemType" runat="server" 
                            AppendDataBoundItems="True"  DataTextField="Name" 
                            DataValueField="Id" AutoPostBack="True" CssClass="textbox">
                            <asp:ListItem Value="0">Select Problem Type</asp:ListItem>
                        </asp:DropDownList><i></i></label></section>
                    
                    </div></fieldset> 
                   
              <footer></footer>
        
            <asp:GridView ID="grvProblemWithInstrumentList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True" 
                    ForeColor="#333333" GridLines="Horizontal"  CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    onrowdatabound="grvProblemWithInstrumentList_RowDataBound" 
                Width="100%" style="text-align: left" AllowPaging="True" 
                onpageindexchanging="grvProblemWithInstrumentList_PageIndexChanging">
            <%--    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />--%>
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="SerialNo" HeaderText="Serial No" />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="WarranityExpireDate" 
                        HeaderText="Waranty Expire Date" />
                    <asp:BoundField DataField="ProblemNumber" HeaderText="Problem No" />
                    <asp:BoundField DataField="ProblemTypeName" HeaderText="Problem Type" />
                    <asp:BoundField DataField="ErrorCodeName" HeaderText="Error Code" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplSchedule" runat="server" 
                                Text="Schedule Curative Maintenance"></asp:HyperLink>
                        </ItemTemplate>
                        </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                        <asp:Button runat="server" ID="btnStatus" Text="" BorderStyle="None" />
                        </ItemTemplate>
                        </asp:TemplateField>
                    
                </Columns>
         <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:GridView>
            </div>
            <br />
         <br />
        <table style="width: 100%">
            <tr>
                <td style="text-align: right">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/schedule.png" />
                </td>
            </tr>
        </table>
       

</asp:Content>
