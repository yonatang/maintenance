﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="MaintenanceConfirmation.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.MaintenanceConfirmation" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc11" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
     <div class="jarviswidget" id="Div1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Confirm</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
                <section class="col col-6">  
             
                <asp:RadioButton ID="rbtPrvConfirmation" runat="server" AutoPostBack="True" 
                    Checked="True" GroupName="confirm" Text=" Confirm Preventive Maintenance" 
                    oncheckedchanged="rbtPrvConfirmation_CheckedChanged"  />
           </section>
           <section class="col col-6"> 
         
                <asp:RadioButton ID="btCurative" runat="server" AutoPostBack="True" 
                    GroupName="confirm" Text=" Confirm Curative Maintenance" 
                    oncheckedchanged="btCurative_CheckedChanged"  /></section>
                    </div></fieldset></div></div></div></div>
        
            <asp:GridView ID="grvCurativeMainenance" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3"   CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
        DataKeyNames="ScheduleID,InstrumentId,ProblemId" EnableModelValidation="True" 
                    GridLines="Horizontal" 
                Width="100%"  
            onrowdatabound="grvCurativeMainenance_RowDataBound" Visible="False" 
        onselectedindexchanged="grvCurativeMainenance_SelectedIndexChanged" 
        AllowPaging="True" 
        onpageindexchanging="grvCurativeMainenance_PageIndexChanging">
                 
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="PreventiveMaintenancePeriod" 
                        HeaderText="Preventive Maint Period" />
                    <asp:BoundField DataField="LastPreventiveMaintenanceDate" 
                        HeaderText="Last Preventive Maint. Date" />
                    <asp:BoundField DataField="ErrorCode" HeaderText="Error Code" />
                    <asp:BoundField DataField="Type" HeaderText="Type" />
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                CommandName="Select" Text="Confirm"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                        <asp:Button runat="server" ID="btnStatus" Text="" BorderStyle="None" />
                        </ItemTemplate>
                        </asp:TemplateField>
                </Columns>
               <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:GridView>
            <asp:GridView ID="grvPrevMaintenances" runat="server" AutoGenerateColumns="False" 
                     CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
        DataKeyNames="ScheduleID,InstrumentId,ProblemId" EnableModelValidation="True" 
                    GridLines="Horizontal" 
                Width="100%"  
            onrowdatabound="grvPrevMaintenances_RowDataBound" Visible="False" 
        onselectedindexchanged="grvPrevMaintenances_SelectedIndexChanged" 
        AllowPaging="True" onpageindexchanging="grvPrevMaintenances_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="PreventiveMaintenancePeriod" 
                        HeaderText="Preventive Maint Period" />
                    <asp:BoundField DataField="LastPreventiveMaintenanceDate" 
                        HeaderText="Last Preventive Maint. Date" />
                    <asp:BoundField DataField="Type" HeaderText="Type" />
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                CommandName="Select" Text="Confirm"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                        <asp:Button runat="server" ID="btnStatus" Text="" BorderStyle="None" />
                        </ItemTemplate>
                        </asp:TemplateField>
                </Columns>
                 <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:GridView>
            
               <asp:Panel ID="pnlConfirmation" runat="server" CssClass="group"  Visible="False">
                     <div class="alert alert-info fade in">
       <i class="fa-fw fa fa-info"></i>
         <strong>Instrument and Notified Problems/ </strong>
                   
               
                                           <asp:Label ID="lblInstrument" runat="server" style="font-weight: 700" 
                                               Text="Selected Instrument" ></asp:Label>
                                           
                                           <asp:Label ID="lblSelectedinstrument" runat="server" 
                                               ></asp:Label>
                                      
                                           <asp:Label ID="lblRegion111" runat="server" Text="Region" style="font-weight: 700" ></asp:Label>
                                       
                                           <asp:Label ID="lblregionName" runat="server" ></asp:Label>
                                      
                                           <asp:Label ID="Label1" runat="server" Text="Installation Date" style="font-weight: 700" ></asp:Label>
                                                                             
                                           <asp:Label ID="lblInstallationDate" runat="server" ></asp:Label>
                                      
                                           <asp:Label ID="lblMainType" runat="server" style="font-weight: 700" 
                                               ></asp:Label>
                                               </div>
                                      
                             <div class="jarviswidget" id="Div2" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Confirm</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
                <section class="col col-6">  

                                       <label class="checkbox">
                                  
                                        <asp:CheckBox ID="CheckBox1" runat="server" text="Is Resolved?" Enabled="False" /></label></section></div>
                                        <div class="row">
                <section class="col col-6">  
                                        <asp:Label ID="Label6" runat="server" style="text-align: left" 
                                            Text="User Comments" CssClass="label"></asp:Label>
                                         <label class="textarea">
                                        <asp:TextBox ID="txtcomments" runat="server"  
                                            TextMode="MultiLine"    Enabled="False"></asp:TextBox></label></section></div>
                                   <div class="row">
                <section class="col col-6">  
                                        <asp:Label ID="Label7" runat="server" CssClass="label" Text="Date"></asp:Label>
                                   <label class="input">
                                        <asp:TextBox ID="txtDate" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"
                         ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                            ControlToValidate="txtDate" Display="Dynamic" ValidationGroup="1">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator22" 
                                            runat="server" ControlToValidate="txtDate" ErrorMessage="Date is not valid" 
                                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                                            ValidationGroup="1">*</asp:RegularExpressionValidator></label></section></div>
                                            </fieldset><footer>
                                           <asp:LinkButton ID="lnkConfirm0" runat="server" onclick="lnkConfirm0_Click" 
                                            ValidationGroup="1" Enabled="False" Cssclass="btn btn-primary">Confirm</asp:LinkButton>
                                        &nbsp;<asp:LinkButton ID="lnkCancel" runat="server" onclick="lnkCancel_Click" Cssclass="btn btn-primary">Close</asp:LinkButton></footer>
                                  </div></div></div></div>
    </asp:Panel>
    <br />
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                        <asp:Image ID="Image2" runat="server" 
                            ImageUrl="~/Images/PrevConfirmationLegend.png" />
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/legends.png" 
                            Visible="False" />
                        </td>
        </tr>
    </table>
</asp:Content>
