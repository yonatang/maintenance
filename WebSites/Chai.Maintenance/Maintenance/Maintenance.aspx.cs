﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class Maintenance : Microsoft.Practices.CompositeWeb.Web.UI.Page, ICurativeMaintenanceView
    {
        private CurativeMaintenancePresenter _presenter;
        private CurativeMaintenance _Curative;
        private Instrument _instrument;
        private Problem _problem;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();

            if (!Page.IsPostBack)
                BindCurativeMaintenanceControls();
            if (InstrumentId != 0 && ProblemId != 0)
            {
                _instrument = _presenter.GetInstrumentByIdForProblem(InstrumentId);
                _problem = _presenter.GetProlemById(ProblemId);
            }
            if (InstrumentIdEdit != 0 && ProblemIdEdit != 0)
            {
                _instrument = _presenter.GetInstrumentByIdForProblem(InstrumentIdEdit);
                _problem = _presenter.GetProlemById(ProblemIdEdit);
                lblEditing.Visible = true;
            }
            if (_instrument != null & _problem != null)
            {

                lblSelectedinstrument.Text = _instrument.InstrumentName;
                lblSiteName.Text = _instrument.SiteName;
                lblSerialNumber.Text = _instrument.SerialNo;
                lblInstallationDate.Text = _instrument.InstallationDate.ToString();

                if (_problem.ProblemNumber == "")
                {
                    lblProblemNumber.Text = "Not Available";
                    lblMaintenanceType.Text = "Preventive";
                }
                else
                {
                    lblProblemNumber.Text = _problem.ProblemNumber;
                    lblMaintenanceType.Text = "Curative";
                }
                lblProblem.Text = _problem.ProblemTypeName;
            }

            if (!IsPostBack)
            {
                if (ScheduleIdEdit != 0 && ProblemIdEdit != 0)
                {
                    this.hlinkEscalate.NavigateUrl = "javascript:;";
                    this.hlinkEscalate.Attributes.Add("onclick",
                                                      String.Format(
                                                          "window.open(\"EscalateReasonEdit.aspx?InstrumentId={0}&ScheduleId={1}&ProblemId={2}&SiteId={3}&EnginerId={4}\", \"Preview\", \"width=370, height=200, resizable=false,scrollbars=1\")",
                                                          _instrument.Id, ScheduleIdEdit, ProblemIdEdit, _presenter.GetLocationId()[1], _presenter.GetLocationId()[2]));
                }
                else
                {
                    this.hlinkEscalate.NavigateUrl = "javascript:;";
                    this.hlinkEscalate.Attributes.Add("onclick",
                                                      String.Format(
                                                          "window.open(\"EscalateReasonEdit.aspx?InstrumentId={0}&ScheduleId={1}&ProblemId={2}&SiteId={3}&EnginerId={4}\", \"Preview\", \"width=370, height=200, resizable=false,scrollbars=1\")",
                                                          _instrument.Id, ScheduleId, ProblemId, _presenter.GetLocationId()[1], _presenter.GetLocationId()[2]));
                }
                BindCurativeMaintenanceControls();
                BindSpareparts();
            }
        }
        [CreateNew]
        public CurativeMaintenancePresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindSparepartType(DropDownList ddlSparepart, int  InstrumentNameId)
        {
            ddlSparepart.DataSource = _presenter.GetSparepartType(InstrumentNameId);
            ddlSparepart.DataBind();
        }
        private void BindCurativeMaintenanceControls()
        {

            this.txtEqpFailureDesc.Text = _Curative.DescriptionOfEqptFailure;
            this.txtFailureCode.Text = _Curative.FailureCode;
            this.txtCauseOfFailure.Text = _Curative.CauseOfEqptFailure;
            this.txtCurativeActionTaken.Text = _Curative.CurativeActionTaken;
            this.txtMaintainedParts.Text = _Curative.MaintainedParts;
            this.txtEngComments.Text = _Curative.EngineerComments;
            this.txtVistcomdate.Text = _Curative.VisitCompletionDate.ToString();
            this.txtDate.Text = _Curative.Date.ToString();
            this.chkPerformance.Checked = _Curative.PerformanceTestsDone;
            this.chkFollowup.Checked = _Curative.FollowUpRequired;
            this.chkFunctional.Checked = _Curative.EquipmentFullyFunctional;

            this.btnDelete.Visible = (_Curative.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }
        #region interface
        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["CurativeId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["CurativeId"]);
                else
                    return 0;
            }
        }
        public int DataMode
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["DataMode"]) != 0)
                    return Convert.ToInt32(Request.QueryString["DataMode"]);
                else
                    return 0;
            }
        }
        public CoreDomain.Maintenance.CurativeMaintenance curative
        {
            get
            {
                return _Curative;
            }
            set
            {
                _Curative = value;
            }
        }

       public int InstrumentId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentId"]);
                else
                    return 0;
            }
        }
       public int ProblemId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ProblemId"]) != 0)
                    return (Convert.ToInt32(Request.QueryString["ProblemId"]));
                else
                    return 0;
            }
        }
       public int ScheduleId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ScheduleId"]) != 0)
                    return (Convert.ToInt32(Request.QueryString["ScheduleId"]));
                else
                    return 0;
            }
        }
        #endregion
        private void SaveCurativeMaintenance()
        {
            try
            {
                if (_Curative.IsNew())
                {
                    _presenter.SaveOrUpdateCurativeMaintenance(_Curative);

                    Master.ShowMessage(new AppMessage("Curative Maintenance saved", RMessageType.Info));
                    //if (this.chkFunctional.Checked)
                    //    _presenter.UpdateStatus("CurativeMaintenance", 2, _problem.Id, ""); //changes the status of problem to Resolved
                    //else
                    //    _presenter.UpdateStatus("CurativeMaintenance", 6, _problem.Id, ""); //changes the status of problem to not Resolved
                }
                else
                {
                    _presenter.SaveOrUpdateCurativeMaintenance(_Curative);
                    Master.ShowMessage(new AppMessage("Curative Maintenance Updated", RMessageType.Info));
                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _Curative.DescriptionOfEqptFailure = this.txtEqpFailureDesc.Text;
                _Curative.FailureCode = this.txtFailureCode.Text;
                _Curative.CauseOfEqptFailure = this.txtCauseOfFailure.Text;
                _Curative.CurativeActionTaken = this.txtCurativeActionTaken.Text;
                _Curative.MaintainedParts = this.txtMaintainedParts.Text;
                _Curative.EngineerComments = this.txtEngComments.Text;
                _Curative.VisitCompletionDate = Convert.ToDateTime(this.txtVistcomdate.Text.ToString());
                _Curative.Date = Convert.ToDateTime(this.txtDate.Text.ToString());
                _Curative.PerformanceTestsDone = this.chkPerformance.Checked;
                _Curative.FollowUpRequired = this.chkFollowup.Checked;
                _Curative.EquipmentFullyFunctional = this.chkFunctional.Checked;
                _Curative.InstrumentId = _instrument.Id;
                _Curative.ProblemId = _problem.Id;
                _Curative.EnginerId = Convert.ToInt32(_presenter.GetLocationId()[2]); // current user id
                if (ScheduleIdEdit == 0)
                    _Curative.Scheduleid = ScheduleId;
                else
                    _Curative.Scheduleid = ScheduleIdEdit;
                SaveCurativeMaintenance();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteCurativeMaintenance(_Curative.Id);
                Master.ShowMessage(new AppMessage("Curative Maintenance Was Deleted Sucessfully", Chai.Maintenance.Enums.RMessageType.Info));

            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Error: Unable to delete Curative Maintenance" + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        #region Spareparts
        private void BindSpareparts()
        {
            dgspare.DataSource = _Curative.Sapres;
            dgspare.DataBind();
        }
        protected void dgspare_CancelCommand(object source, DataGridCommandEventArgs e)
        {
            this.dgspare.EditItemIndex = -1;
            BindSpareparts();
        }
        protected void dgspare_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                LinkButton lnkbtn = e.Item.FindControl("lnkDelete") as LinkButton;
                if (lnkbtn != null)
                    lnkbtn.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this Sparepart?');");

                if (_Curative.Sapres[e.Item.DataSetIndex].Id != 0)
                {
                    _presenter.DeleteSpareparts(_Curative.Sapres[e.Item.DataSetIndex].Id);
                }
                _Curative.Sapres.Remove(_Curative.Sapres[Convert.ToInt32(e.Item.DataSetIndex)]);
                BindSpareparts();

                Master.ShowMessage(new AppMessage("Sparepart was Removed Sucessfully", Chai.Maintenance.Enums.RMessageType.Info));
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Error: Unable to delete Sparepart item Detail. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        protected void dgspare_EditCommand(object source, DataGridCommandEventArgs e)
        {
            this.dgspare.EditItemIndex = e.Item.ItemIndex;

            BindSpareparts();
        }
        protected void dgspare_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "AddNew")
            {
                try
                {
                    Spareparts spare = new Spareparts();

                    DropDownList ddlSparepart = e.Item.FindControl("ddlFSparepart") as DropDownList;
                    spare.SparepartId = int.Parse(ddlSparepart.SelectedValue);
                    spare.SparePartName = ddlSparepart.SelectedItem.Text;
                    TextBox txtqty = e.Item.FindControl("txtFQty") as TextBox;
                    spare.Quantity = Convert.ToInt32(txtqty.Text);
                    _Curative.Sapres.Add(spare);
                    Master.ShowMessage(new AppMessage("Sparepart Detail Added Sucessfully.", Chai.Maintenance.Enums.RMessageType.Info));
                    dgspare.EditItemIndex = -1;
                    BindSpareparts();
                }
                catch (Exception ex)
                {
                    Master.ShowMessage(new AppMessage("Error: Unable to Add Sparepart " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
                }
            }
        }
        protected void dgspare_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
           
            if (e.Item.ItemType == ListItemType.Footer)
            {
                DropDownList ddlFSparepart = e.Item.FindControl("ddlFSparepart") as DropDownList;
                BindSparepartType(ddlFSparepart, _instrument.nameId);
                
            }
            else
            {


                if (_Curative.Sapres != null)
                {
                    LinkButton lnkbtn = e.Item.FindControl("lnkDelete") as LinkButton;
                    if (lnkbtn != null)
                        lnkbtn.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this Sparepart?');");
                    DropDownList ddlSparepart = e.Item.FindControl("ddlSparepart") as DropDownList;
                    if (ddlSparepart != null)
                    {
                        BindSparepartType(ddlSparepart, _instrument.nameId);
                        if (_Curative.Sapres[e.Item.DataSetIndex].SparepartId != null)
                        {
                            ListItem li = ddlSparepart.Items.FindByValue((_Curative.Sapres[e.Item.DataSetIndex].SparepartId.ToString()));
                            if (li != null)
                                li.Selected = true;
                        }
                    }

                }

            }

        }
        protected void dgspare_UpdateCommand(object source, DataGridCommandEventArgs e)
        {
            Spareparts Spare = _Curative.Sapres[e.Item.DataSetIndex];
            try
            {

                DropDownList ddlSparepart = e.Item.FindControl("ddlSparepart") as DropDownList;
                Spare.SparepartId = int.Parse(ddlSparepart.SelectedValue);
                Spare.SparePartName = ddlSparepart.SelectedItem.Text;

                TextBox txtqty = e.Item.FindControl("txtQty") as TextBox;
                Spare.Quantity = Convert.ToInt32(txtqty.Text);
                
                _Curative.Sapres.Insert(Convert.ToInt32(e.Item.DataSetIndex), Spare);
                _Curative.Sapres.RemoveAt(e.Item.DataSetIndex);
                Master.ShowMessage(new AppMessage("Spareparts Updated Sucessfully.", Chai.Maintenance.Enums.RMessageType.Info));
                dgspare.EditItemIndex = -1;
                BindSpareparts();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Error: Unable to Update Spareparts. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        #endregion

        public int CurativeIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["CurativeIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["CurativeIdEdit"]);
                else
                    return 0;
            }
        }

        public int InstrumentIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]);
                else
                    return 0;
            }
        }

        public int ProblemIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ProblemIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ProblemIdEdit"]);
                else
                    return 0;
            }
        }
        public int ScheduleIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ScheduleIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ScheduleIdEdit"]);
                else
                    return 0;
            }
        }
        protected void btnCancel_Click1(object sender, EventArgs e)
        {
            string url = String.Format("~/Maintenance/AssignedJobs.aspx?{0}=2", AppConstants.TABID);

            Response.Redirect(url);
        }
    
}
}