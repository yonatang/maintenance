﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="InstrumentTransferList.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.InstrumentTransferList" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    
        
       <div class="jarviswidget" id="Div1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Find Transfered Instruments</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
                <section class="col col-6">  
       
       
                    <asp:Label ID="lblInstrument" runat="server" Text="Instrument Category" 
                        CssClass="label"></asp:Label>
               <label class="select">
                    <asp:DropDownList ID="ddlInstrumentCategory" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                              AutoPostBack="True">
                        <asp:ListItem Value="0">Select Instrument Category</asp:ListItem>
                    </asp:DropDownList><i></i></label></section></div></fieldset><footer>  </footer></div></div></div>
                
    <asp:GridView ID="grvTransferedInstruments" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True"   CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    GridLines="Horizontal" 
                Width="100%"  
            onrowdatabound="grvTransferedInstruments_RowDataBound" 
        AllowPaging="True" 
        onpageindexchanging="grvTransferedInstruments_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="OldSite" HeaderText="Old Site" />
                    <asp:BoundField DataField="NewSite" HeaderText="New Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="Reason" 
                        HeaderText="Transfer Reason" />
                    <asp:BoundField DataField="Date" HeaderText="Date" />
                    <asp:TemplateField>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit Transfer"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:GridView>
          </div>
</asp:Content>

