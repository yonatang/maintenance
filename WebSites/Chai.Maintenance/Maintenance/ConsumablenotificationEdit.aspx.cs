﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class ConsumablenotificationEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IConsumableNotificationEditView
    {
        private ConsumableNotificationEditPresenter _presenter;

        private ConsumableNotification _consumableNotification;
        IList<Site> _siteList;
        IList<Consumables> _consumables;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();

            if (!Page.IsPostBack)
                BindControls();

            if (ConsumableNotificationIdEdit == 0)
                this.lblNotifiedById.Text = _presenter.GetUserList(Convert.ToInt32(_presenter.GetLocationId()[2])).FullName;
            else
                this.lblEditing.Visible = true;

            if (_presenter.GetLocationId()[0] == 1) //site user
                ddlSite.Enabled = false;
            else if (_presenter.GetLocationId()[0] == 2) // region user
            {
                ddlSite.Enabled = true;
                BindSite(_presenter.GetLocationId()[1]);// pass the current region Id

            }
             BindConsumables();


        }
        [CreateNew]
        public ConsumableNotificationEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindSite(int regionId)
        {
            if (!IsPostBack)
            {
                _siteList = _presenter.GetSiteList(regionId);
                ddlSite.DataSource = _siteList;
                ddlSite.DataBind();
            }
        }
        private void BindConsumables()
        {
            if (!IsPostBack)
            {
                _consumables = _presenter.GetConsumablesList();
                ddlConsumbale.DataSource = _consumables;
                ddlConsumbale.DataBind();
            }
        }
        #region IConsumableNotificationEditView
        public ConsumableNotification _consumable
        {
            get { return _consumableNotification; }
            set { _consumableNotification = value; }
        }

        public int ConsumableNotificationIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ConsumableNotificationIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ConsumableNotificationIdEdit"]);
                else
                    return 0;
            } 
        }
        #endregion
        private void BindControls()
        {

            if (ConsumableNotificationIdEdit != 0)
                this.lblNotifiedById.Text = _presenter.GetUserList(_consumableNotification.NotifiedById).FullName;
            else
                this.lblNotifiedById.Text = _presenter.GetUserList(Convert.ToInt32(_presenter.GetLocationId()[2])).FullName;
            this.ddlConsumbale.SelectedValue = _consumableNotification.ConsumableId.ToString();
            this.txtremark.Text = _consumableNotification.Remark;
            this.txtdate.Text = _consumableNotification.Date.ToString();
            this.ddlSite.SelectedValue = _consumableNotification.SiteId.ToString();
            this.btnDelete.Visible = (_consumableNotification.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");

        }
        private void SaveConsumableNotification()
        {
            try
            {
                if (_consumableNotification.IsNew())
                {
                    _presenter.SaveOrUpdateConsumableNotification(_consumableNotification);
                    Master.ShowMessage(new AppMessage("Consumable Notification Saved", RMessageType.Info));
                }
                else
                {
                    _presenter.SaveOrUpdateConsumableNotification(_consumableNotification);
                    Master.ShowMessage(new AppMessage("Consumable Notification Saved", RMessageType.Info));

                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _consumableNotification.ConsumableId = Convert.ToInt32(ddlConsumbale.SelectedValue);
                _consumableNotification.Remark = this.txtremark.Text;
                _consumableNotification.Date = Convert.ToDateTime(this.txtdate.Text.ToString());
                if (_presenter.GetLocationId()[0] == 1) //site user
                    _consumableNotification.SiteId = Convert.ToInt32(_presenter.GetLocationId()[1]);
                else if (_presenter.GetLocationId()[0] == 2) // region user
                    _consumableNotification.SiteId = Convert.ToInt32(ddlSite.SelectedValue);

                _consumableNotification.NotifiedById = Convert.ToInt32(_presenter.GetLocationId()[2]); // current user id
               


                SaveConsumableNotification();
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            string url = String.Format("~/Maintenance/ConsumableNotificationList.aspx?{0}=2", AppConstants.TABID);
            Response.Redirect(url);
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteConsumableNotification(_consumableNotification.Id);
                Master.ShowMessage(new AppMessage("Notification was deleted successfully", Chai.Maintenance.Enums.RMessageType.Info));

            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Error: Unable to delete Notification. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            string url = String.Format("~/Maintenance/ConsumableNotificationList.aspx?{0}=2", AppConstants.TABID);
            Response.Redirect(url);
        }
}
}