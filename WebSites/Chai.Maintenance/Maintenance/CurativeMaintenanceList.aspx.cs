﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class CurativeMaintenanceList : Microsoft.Practices.CompositeWeb.Web.UI.Page, ICurativeMaintenanceListView
    {
        private Chai.Maintenance.Modules.Maintenance.Views.CurativeMaintenanceListPresenter _presenter;
        IList<CoreDomain.Maintenance.CurativeMaintenance> _CurativeMaintList;
        IList<Chai.Maintenance.CoreDomain.Configuration.Site> _siteList;
        IList<Chai.Maintenance.CoreDomain.Configuration.InstrumentCategory> _instrumentCategoryList;
        int _instrumentId;
        IList<Chai.Maintenance.CoreDomain.Configuration.Region> _regionList;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();


            //if (_presenter.GetLocationId()[0] == 1) //site user
            //{
            //    lblRegion.Enabled = false;
            //    ddlRegion.Enabled = false;

            //    lblSite.Enabled = false;
            //    ddlSite.Enabled = false;
            //}
            //else if (_presenter.GetLocationId()[0] == 2) // region user
            //{
            //    if (_presenter.GetLocationId()[1] == 0) // federal user
            //    {
            //        BindRegion();
            //        BindSite(0);
            //        lblRegion.Enabled = true;
            //        ddlRegion.Enabled = true;

            //        lblSite.Enabled = true;
            //        ddlSite.Enabled = true;
            //    }
            //    else //regional user, get list of site based on selected region
            //    {
            //        BindSite(_presenter.GetLocationId()[1]); // passes the id 
            //        lblRegion.Enabled = false;
            //        ddlRegion.Enabled = false;

            //        lblSite.Enabled = true;
            //        ddlSite.Enabled = true;
            //    }

            //}
            grvCurativeMaintenance.DataSource = _CurativeMaintList;
            grvCurativeMaintenance.DataBind();
            BindInstrumentCategory();
        }
        //private void BindRegion()
        //{
        //    if (!IsPostBack)
        //    {
        //        _regionList = _presenter.GetRegionList();
        //        ddlRegion.DataSource = _regionList;
        //        ddlRegion.DataBind();
        //    }
        //}
        //private void BindSite(int regionId)
        //{
        //    if (!IsPostBack)
        //    {
        //        _siteList = _presenter.GetSiteList(regionId);
        //        ddlSite.DataSource = _siteList;
        //        ddlSite.DataBind();
        //    }
        //}
        private void BindInstrumentCategory()
        {
            if (!IsPostBack)
            {
                _instrumentCategoryList = _presenter.GetInstrumentCategoryList();
                ddlInstrumentCategory.DataSource = _instrumentCategoryList;
                ddlInstrumentCategory.DataBind();
            }
        }
        [CreateNew]
        public CurativeMaintenanceListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }




        protected void grvCurativeMaintenance_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvCurativeMaintenance.PageIndex = e.NewPageIndex;
            grvCurativeMaintenance.DataSource = _CurativeMaintList;
            grvCurativeMaintenance.DataBind();
        }
        protected void grvCurativeMaintenance_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            CoreDomain.Maintenance.CurativeMaintenance curative = e.Row.DataItem as CoreDomain.Maintenance.CurativeMaintenance;
            HyperLink lnkEdit = e.Row.FindControl("lnkEdit") as HyperLink;
            if (curative != null)
            {
                string url2 = String.Format("~/Maintenance/Maintenance.aspx?{0}=2&InstrumentIdEdit={1}&CurativeIdEdit={2}&ProblemIdEdit={3}&ScheduleIdEdit={4}", AppConstants.TABID, curative.InstrumentId, curative.Id, curative.ProblemId, curative.Scheduleid);
                lnkEdit.NavigateUrl = this.ResolveUrl(url2);
                // lnkEdit.Enabled = false;

                //if (preventive.PreventiveScheduleStatus == "Not Performed")
                //{
                //    lnkEdit.Enabled = true;
                //}
                //else
                //{
                //    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C"); // Already on Maintenance Process
                //    lnkEdit.Enabled = false;
                //}

            }
        }

        public IList<CurativeMaintenance> _CurativeMaintenanceList
        {
            set { _CurativeMaintList = value; }
        }

        public int _SiteId//parameter from edit Preventive mainteanance page after save, cancel clicked. to keep the selected prev Maintenance
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["SiteID"]) != 0)
                    return Convert.ToInt32(Request.QueryString["SiteID"]);
                else
                    return 0;
            }
        }
        public int _InstrumentCatId
        {
            get
            {
                { return Convert.ToInt32(ddlInstrumentCategory.SelectedValue); }
            }
        }

        public int SiteId
        {
            get
            {
                if (_presenter.GetLocationId()[0] != 1)
                    return 0;
                else
                    return _presenter.GetLocationId()[1];
            }
        }
        //protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    _siteList = _presenter.GetSiteList(Convert.ToInt32(ddlRegion.SelectedValue));
        //    if (ddlSite.Items.Count > 0)
        //    {
        //        ddlSite.Items.Clear();
        //    }
        //    ListItem lst = new ListItem();
        //    lst.Text = "Select Site";
        //    lst.Value = "0";
        //    ddlSite.Items.Add(lst);



        //    ddlSite.DataSource = _siteList;
        //    ddlSite.DataBind();
        //}


        public int userId
        {
            get { return _presenter.GetLocationId()[2]; }
        }
    }
}