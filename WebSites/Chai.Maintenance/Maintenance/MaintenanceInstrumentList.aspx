﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="MaintenanceInstrumentList.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.MaintenanceInstrumentList" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
   

       <div class="jarviswidget" id="Div1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Find Instrument</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
                <section class="col col-6">  
         
                        <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
                     <label class="select">
                        <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                             onselectedindexchanged="ddlRegion_SelectedIndexChanged" 
                            CssClass="textbox">
                            <asp:ListItem Value="0">Select Region</asp:ListItem>
                        </asp:DropDownList><i></i></label></section>
                    <section class="col col-6">
                        <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                    <label class="select">
                        <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="Name" DataValueField="Id" 
                            CssClass="textbox">
                            <asp:ListItem Value="0">Select Site</asp:ListItem>
                        </asp:DropDownList><i></i></label></section></div>
                    <div class="row">
                <section class="col col-6">  
         
                        <asp:Label ID="lblInstrument" runat="server" Text="Instrument Category" 
                            CssClass="label"></asp:Label>
                   <label class="select">
                        <asp:DropDownList ID="ddlInstrumentCategory" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             AutoPostBack="True" CssClass="textbox">
                            <asp:ListItem Value="0">Select Instrument Category</asp:ListItem>
                        </asp:DropDownList><i></i></label></section>
                 
                <section class="col col-6">  
                        <asp:Label ID="Label1" runat="server" Text="Functionality" CssClass="label"></asp:Label>
                   <label class="select">
                        <asp:DropDownList ID="ddlFunctionality" runat="server" AutoPostBack="True" 
                             CssClass="textbox">
                            <asp:ListItem Value="0">Select Functionality</asp:ListItem>
                            <asp:ListItem Value="1">Functional</asp:ListItem>
                            <asp:ListItem Value="2">Not-Functional</asp:ListItem>
                            <asp:ListItem Value="5">Functional With Problem</asp:ListItem>
                        </asp:DropDownList><i></i></label></section></div></fieldset></div></div></div>
                    
     
               
      
            <asp:GridView ID="grvInstrumentList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active"  AlternatingRowStyle-CssClass=""
                     GridLines="Horizontal" 
                    onrowdatabound="grvInstrumentList_RowDataBound" 
                 
                onselectedindexchanged="grvInstrumentList_SelectedIndexChanged" 
                onpageindexchanging="grvInstrumentList_PageIndexChanging" 
                AllowPaging="True">
                
                 
                <Columns>
                    <asp:BoundField DataField="RegionName" HeaderText="Region" />
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument Name" />
                    <asp:BoundField DataField="SerialNo" HeaderText="SerialNo." />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="ManufacturerName" HeaderText="Manufacturer" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplNotifyProblem" runat="server">Report Problem</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField >
                    <ItemTemplate>
                        <asp:Button runat="server" ID="btnStatus" Text="" BorderStyle="None" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:GridView>
           </div>
        <table style="width: 100%; margin-bottom: 4px;">
            <tr>
                <td style="width: 700px">
                    &nbsp;</td>
                <td style="text-align: right">
                    <asp:Image ID="Image1" runat="server" 
                        ImageUrl="~/Images/functionalNotfunction.png" />
                </td>
            </tr>
        </table>
 
</asp:Content>



