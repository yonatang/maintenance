﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="SchedulePreventiveMaintenanceList.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.SchedulePreventiveMaintenanceList" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
 <%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
 
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
      <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Find Instrument To Schedule Preventive Maintenance</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
                     
                     <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
                <label class="select">
                     <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                              onselectedindexchanged="ddlRegion_SelectedIndexChanged" 
                         CssClass="textbox">
                         <asp:ListItem Value="0">Select Region</asp:ListItem>
                     </asp:DropDownList><i></i></label></section>
                     <section class="col col-6">  
                     <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                <label class="select">
                     <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                            DataTextField="Name" DataValueField="Id" 
                         AutoPostBack="True" CssClass="textbox">
                         <asp:ListItem Value="0">Select Site</asp:ListItem>
                     </asp:DropDownList><i></i></label></section>
                </div>
               <div class="row">
									<section class="col col-6">  
                     <asp:Label ID="lblInstrument" runat="server" Text="Instrument Category" 
                         CssClass="label"></asp:Label>
                <label class="select">
                     <asp:DropDownList ID="ddlInstrumentCategory" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             AutoPostBack="True" CssClass="textbox">
                         <asp:ListItem Value="0">Select Instrument Category</asp:ListItem>
                     </asp:DropDownList><i></i></label></section>
                 	<section class="col col-6">  
                     <asp:Label ID="Label2" runat="server" Text="Last Prev Maintenance Date" 
                         CssClass="label"></asp:Label>
                  <label class="input">
                   <i class="icon-append fa fa-calendar"></i>
                     <asp:TextBox ID="txtlastprevMdate" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"
                         ></asp:TextBox>
                   
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" 
                            runat="server" ControlToValidate="txtlastprevMdate" ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator></label></section></div>
                            </fieldset>
                            <footer>
               
                     <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" 
                         ValidationGroup="1" Cssclass="btn btn-primary"/>
              </footer></div></div></div>

    
        
        
            <asp:GridView ID="grvInstrumentList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True"  CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                      GridLines="Horizontal" 
                    onrowdatabound="grvInstrumentList_RowDataBound" 
                 AllowPaging="True" 
             onpageindexchanging="grvInstrumentList_PageIndexChanging">
               
                
                <Columns>
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument Name" />
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="SerialNo" HeaderText="Serial Number" />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="PreventiveMaintenancePeriod" 
                        HeaderText="Preventive Maintenance Period" />
                    <asp:BoundField DataField="LastPreventiveMaintenanceDate" 
                        HeaderText="Last Preventive Maintenance Date" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplschedule" runat="server">Schedule Preventive Maintenance</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                        <asp:Button runat="server" ID="btnStatus" Text="" BorderStyle="None" />
                        </ItemTemplate>
                        </asp:TemplateField>
                </Columns>
               <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:GridView>
            </div>
         <table style="width: 100%">
             <tr>
                 <td style="text-align: right">
                     <asp:Image ID="Image1" runat="server" 
                         ImageUrl="~/Images/PreventiveScehudleLegend9999.png" />
                 </td>
             </tr>
         </table>
        
 </asp:Content>
