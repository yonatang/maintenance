﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class InstrumentTransferList : Microsoft.Practices.CompositeWeb.Web.UI.Page, ITransferListView
    {
        private Chai.Maintenance.Modules.Maintenance.Views.TransferListPresenter _presenter;
        IList<CoreDomain.Maintenance.Transfer> _TransferList;
        IList<Chai.Maintenance.CoreDomain.Configuration.Site> _siteList;
        IList<Chai.Maintenance.CoreDomain.Configuration.InstrumentCategory> _instrumentCategoryList;
        int _instrumentId;
        IList<Chai.Maintenance.CoreDomain.Configuration.Region> _regionList;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();


            //if (_presenter.GetLocationId()[0] == 1) //site user
            //{
            //    lblRegion.Enabled = false;
            //    ddlRegion.Enabled = false;

            //    lblSite.Enabled = false;
            //    ddlSite.Enabled = false;
            //}
            //else if (_presenter.GetLocationId()[0] == 2) // region user
            //{
            //    if (_presenter.GetLocationId()[1] == 0) // federal user
            //    {
            //        BindRegion();
            //        BindSite(0);
            //        lblRegion.Enabled = true;
            //        ddlRegion.Enabled = true;

            //        lblSite.Enabled = true;
            //        ddlSite.Enabled = true;
            //    }
            //    else //regional user, get list of site based on selected region
            //    {
            //        BindSite(_presenter.GetLocationId()[1]); // passes the id 
            //        lblRegion.Enabled = false;
            //        ddlRegion.Enabled = false;

            //        lblSite.Enabled = true;
            //        ddlSite.Enabled = true;
            //    }

            //}
            grvTransferedInstruments.DataSource = _TransferList;
            grvTransferedInstruments.DataBind();
            BindInstrumentCategory();
        }
        //private void BindRegion()
        //{
        //    if (!IsPostBack)
        //    {
        //        _regionList = _presenter.GetRegionList();
        //        ddlRegion.DataSource = _regionList;
        //        ddlRegion.DataBind();
        //    }
        //}
        //private void BindSite(int regionId)
        //{
        //    if (!IsPostBack)
        //    {
        //        _siteList = _presenter.GetSiteList(regionId);
        //        ddlSite.DataSource = _siteList;
        //        ddlSite.DataBind();
        //    }
        //}
        private void BindInstrumentCategory()
        {
            if (!IsPostBack)
            {
                _instrumentCategoryList = _presenter.GetInstrumentCategoryList();
                ddlInstrumentCategory.DataSource = _instrumentCategoryList;
                ddlInstrumentCategory.DataBind();
            }
        }
        [CreateNew]
        public TransferListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
       
        protected void grvTransferedInstruments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvTransferedInstruments.PageIndex = e.NewPageIndex;
            grvTransferedInstruments.DataSource = _TransferList;
            grvTransferedInstruments.DataBind();
        }
        protected void grvTransferedInstruments_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            CoreDomain.Maintenance.Transfer transfered = e.Row.DataItem as CoreDomain.Maintenance.Transfer;
            HyperLink lnkEdit = e.Row.FindControl("lnkEdit") as HyperLink;
            if (transfered != null)
            {
                string url2 = String.Format("~/Maintenance/MaintenanceTransfer.aspx?{0}=2&InstrumentIdEdit={1}&TransferedIdEdit={2}", AppConstants.TABID, transfered.InstrumentId, transfered.Id);
                lnkEdit.NavigateUrl = this.ResolveUrl(url2);
            }
        }

        public IList<Transfer> _transferList
        {
            set { _TransferList = value; }
        }

        public int _SiteId//parameter from edit Transfer page after save or cancel clicked. to keep the selected Instrument
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["SiteID"]) != 0)
                    return Convert.ToInt32(Request.QueryString["SiteID"]);
                else
                    return 0;
            }
        }

        public int _instCategoryId
        {
            get
            {
                { return Convert.ToInt32(ddlInstrumentCategory.SelectedValue); }
            }
        }

        public int UserId
        {
            get
            {
                    return _presenter.GetLocationId()[2];
            }
        }
    }
}