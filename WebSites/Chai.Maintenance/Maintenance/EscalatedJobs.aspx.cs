﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class EscalatedJobs : Microsoft.Practices.CompositeWeb.Web.UI.Page, IEscalatedJobsView
    {
        private EscalatedJobsPresenter _presenter;
        IList<CoreDomain.Maintenance.EscalatedJobs> _escalatedJobs;
        IList<Site> _siteList;
        IList<Region> _regionlist;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();
            grvEscalatedJobs.DataSource = _escalatedJobs;
            grvEscalatedJobs.DataBind();

            BindRegion();
        }
        [CreateNew]
        public EscalatedJobsPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }


        private void BindSite(int regionId)
        {
            if (!IsPostBack)
            {
                _siteList = _presenter.GetSiteList(regionId);
                ddlSite.DataSource = _siteList;
                ddlSite.DataBind();
            }
        }
        private void BindRegion()
        {
            if (!IsPostBack)
            {
                _regionlist = _presenter.GetRegionList();
                ddlRegion.DataSource = _regionlist;
                ddlRegion.DataBind();
            }
        }
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            _siteList = _presenter.GetSiteList(Convert.ToInt32(ddlRegion.SelectedValue));
            if (ddlSite.Items.Count > 0)
            {
                ddlSite.Items.Clear();
            }
            ListItem lst = new ListItem();
            lst.Text = "Select Site";
            lst.Value = "0";
            ddlSite.Items.Add(lst);



            ddlSite.DataSource = _siteList;
            ddlSite.DataBind();
        }
        protected void grvEscalatedJobs_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvEscalatedJobs.PageIndex = e.NewPageIndex;
            grvEscalatedJobs.DataSource = _escalatedJobs;
            grvEscalatedJobs.DataBind();
        }
        protected void grvEscalatedJobs_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            CoreDomain.Maintenance.EscalatedJobs escalated = e.Row.DataItem as CoreDomain.Maintenance.EscalatedJobs;
            if (escalated != null)
            {
                HyperLink hpl = e.Row.FindControl("hplschedule") as HyperLink;
                Button btnStatus = e.Row.FindControl("btnStatus") as Button;
                string url = String.Format("~/Maintenance/ScheduleCurativeMaintenance.aspx?{0}=2&InstrumentId={1}&ProblemId={2}&Mode={3}&EscalateId={4}", AppConstants.TABID, escalated.InstrumentId, escalated.ProblemId, 1, escalated.Id);
                if (escalated.Status == "New")
                {
                    hpl.Visible = true;
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C");
                }
                else if(escalated.Status == "Scheduled")
                {
                    hpl.Visible = false;
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#99CC00");
                }
                hpl.NavigateUrl = this.ResolveUrl(url);  
            }
        }

        #region IEscalatedJobsView Members

        IList<CoreDomain.Maintenance.EscalatedJobs> IEscalatedJobsView.EscalatedJobs
        {
            set { _escalatedJobs = value; }
        }

        public int SiteId
        {
            get { return Convert.ToInt32(ddlSite.SelectedValue.ToString()); }
        }

        public int RegionId
        {
            get { return Convert.ToInt32(ddlRegion.SelectedValue.ToString()); }
        }

        #endregion
    }
}