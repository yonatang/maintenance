﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="Maintenance.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.Maintenance" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
 <%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
 <%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc11" %>
 <asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
      <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblEditing" runat="server" 
                    style="color: #FF3300; background-color: #FFFFFF; font-weight: 700;" Text="Editing..." 
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
     <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-danger fade in"
         HeaderText="Error Message" ValidationGroup="4" />
    <div class="alert alert-info fade in"><i class="fa-fw fa fa-info"></i>
         <strong>Instrument Info!/ </strong>  
                      
       
                    <asp:Label ID="lblInstrument" runat="server" Text="Selected Instrument" 
                        style="font-weight: 700" ></asp:Label>
               
                    <asp:Label ID="lblSelectedinstrument" runat="server" 
                        ></asp:Label>
                
                   
              
                    <asp:Label ID="lblSiteName" runat="server" ></asp:Label>
              
                    <asp:Label ID="Label5" runat="server" Text="Problem Number" style="font-weight: 700" ></asp:Label>
              
                    <asp:Label ID="lblProblemNumber" runat="server" ></asp:Label>
            
                    <asp:Label ID="Label8" runat="server" Text="Serial Number" style="font-weight: 700" ></asp:Label>
               
                    <asp:Label ID="lblSerialNumber" runat="server" ></asp:Label>
               
                    <asp:Label ID="Label4" runat="server" Text="Problem" style="font-weight: 700" ></asp:Label>
             
                    <asp:Label ID="lblProblem" runat="server"></asp:Label>
                
                    <asp:Label ID="Label1" runat="server" Text="Installation Date" style="font-weight: 700" ></asp:Label>
               
                    <asp:Label ID="lblInstallationDate" runat="server" ></asp:Label>
               
                    <asp:Label ID="Label6" runat="server" Text="Maintenance Type" style="font-weight: 700" ></asp:Label>
              
                    <asp:Label ID="lblMaintenanceType" runat="server"></asp:Label>
                <footer><asp:HyperLink ID="hlinkEscalate" runat="server" Font-Bold="True" Cssclass="btn btn-primary">Push Job</asp:HyperLink> </footer></div>
 
        
  <div class="jarviswidget" id="Div1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Curative Maintenance</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
                <section class="col col-6">  
                  
                
               <asp:Label ID="Label11" runat="server" Text="Description Of Equipment Failure" CssClass="label"></asp:Label>
                        
                <label class="textarea">
                <asp:TextBox ID="txtEqpFailureDesc" runat="server"  TextMode="MultiLine" CssClass="textboxDescLong"></asp:TextBox>
                        
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" 
                    runat="server" ControlToValidate="txtEqpFailureDesc" Display="Dynamic" 
                    ErrorMessage="Describe Eqt Failure" ValidationGroup="4">*</asp:RequiredFieldValidator></label></section>
          <section class="col col-6">

                 <asp:Label ID="Label18" runat="server" Text="Failure Codes(s)" CssClass="label"></asp:Label>
       <label class="textarea">
                <asp:TextBox ID="txtFailureCode" runat="server" TextMode="MultiLine" 
                       ></asp:TextBox></label></section></div>
                        <div class="row">
                <section class="col col-6">

                  <asp:Label ID="Label20" runat="server" Text="Part of Machine/Equipment to be maintained" CssClass="label"></asp:Label> 
                     <label class="textarea">                  
                <asp:TextBox ID="txtMaintainedParts" runat="server" TextMode="MultiLine" ></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                   ControlToValidate="txtMaintainedParts" Display="Dynamic" 
                                   ErrorMessage="Mention Parts Maintained" ValidationGroup="4">*</asp:RequiredFieldValidator></label></section>
<section class="col col-6">
                  <asp:Label ID="Label2" runat="server" Text="Cause Of Equipment Failure (If Known)" CssClass="label"></asp:Label> 
                     <label class="textarea">                       
                <asp:TextBox ID="txtCauseOfFailure" runat="server"  
                      TextMode="MultiLine"   
                         CssClass="textboxDescLong"></asp:TextBox></label></section></div>
               <div class="row">
                <section class="col col-6">
                <asp:Label ID="Label22" runat="server"  Text="Engineer Comments" CssClass="label"></asp:Label> 
                        <label class="textarea">                   
                <asp:TextBox ID="txtEngComments" runat="server" TextMode="MultiLine" CssClass="textboxDescShort"></asp:TextBox></label></section>
                      <section class="col col-6">
                           <asp:Label ID="Label21" runat="server" 
                                       Text="Curative Acion Taken" CssClass="label"></asp:Label> 
                                  <label class="textarea">             
                <asp:TextBox ID="txtCurativeActionTaken" runat="server" 
                     TextMode="MultiLine" 
                        CssClass="textboxDescLong"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                   ControlToValidate="txtCurativeActionTaken" Display="Dynamic" 
                                   ErrorMessage="Mention Curative Action Taken " ValidationGroup="4">*</asp:RequiredFieldValidator></label></section></div>
                                   <div class="row">
                <section class="col col-6">
                <label class="checkbox">  
                 <asp:CheckBox ID="chkPerformance" runat="server" 
                    Text="" CssClass="label" /><i></i>Performance Test Done</label></section>
               <section class="col col-6">
               <label class="checkbox">  
                <asp:CheckBox ID="chkFunctional" runat="server" 
                    Text="" CssClass="label" /><i></i>Equipment Fully Functional?</label></section>
               <section class="col col-6">
                <label class="checkbox">  
                <asp:CheckBox ID="chkFollowup" runat="server" Text="" 
                    CssClass="label" /><i></i>Follow Up Required?</label></section></div>
          

<div class="row">
                <section class="col col-6">

                  <asp:Label ID="Label19" runat="server" CssClass="label" 
                    Text="Vist Completion Date"></asp:Label>
 <label class="input">
                   <i class="icon-append fa fa-calendar"></i>
                     <asp:TextBox ID="txtVistcomdate" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"
                         ></asp:TextBox>                   
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" 
                            runat="server" ControlToValidate="txtVistcomdate" ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator></label></section>
            <section class="col col-6">
                <asp:Label ID="Label23" runat="server" Text="Date" CssClass="label"></asp:Label>
           <label class="input"> 
           <i class="icon-append fa fa-calendar"></i>
                       <asp:TextBox ID="txtDate" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"
                         ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtDate" Display="Dynamic" ErrorMessage="Date Required" 
                    ValidationGroup="4">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator22" 
                            runat="server" ControlToValidate="txtDate" ErrorMessage="Date is not valid" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="4">*</asp:RegularExpressionValidator></label></section></div></fieldset></div></div></div></div>
          

 
 <div class="jarviswidget jarviswidget-color-darken jarviswidget-sortable" id="wid-id-0" data-widget-editbutton="false" role="widget" style="">
    <header role="heading">
    <h2>Spare parts replaced</h2>
    </header>
    <div role="content">
                <asp:DataGrid ID="dgspare" runat="server" AutoGenerateColumns="False" 
                   CellPadding="0"  CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    DataKeyField="Id"   GridLines="None" 
                    oncancelcommand="dgspare_CancelCommand" 
                    ondeletecommand="dgspare_DeleteCommand" 
                    oneditcommand="dgspare_EditCommand" 
                    onitemcommand="dgspare_ItemCommand" 
                    onitemdatabound="dgspare_ItemDataBound" 
                    onupdatecommand="dgspare_UpdateCommand" ShowFooter="True" Width="99%">
                   
                    <Columns>
                        <asp:TemplateColumn HeaderText="Part Name">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "SparePartName")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlSparepart" runat="server" AppendDataBoundItems="True" 
                                    DataTextField="Name" DataValueField="Id" Width="300px">
                                    <asp:ListItem Value="0">Select Spare</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RfvSparepart" runat="server" 
                                    ControlToValidate="ddlSparepart" ErrorMessage="Sparepart Required" 
                                    InitialValue="0" ValidationGroup="3">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlFSparepart" runat="server" 
                                    AppendDataBoundItems="True" DataTextField="Name" 
                                    DataValueField="Id" EnableViewState="true" 
                                    ValidationGroup="2" Width="300px">
                                    <asp:ListItem Value="0">Select Spare</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RfvFSparepart" runat="server" 
                                    ControlToValidate="ddlFSparepart" Display="Dynamic" 
                                    ErrorMessage="Sparepart Required" InitialValue="0" ValidationGroup="2">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                     
                    
                        <asp:TemplateColumn HeaderText="Quantity">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtQty" runat="server" 
                                    Text=' <%# DataBinder.Eval(Container.DataItem, "Quantity")%>' ValidationGroup="3" 
                                    Width="100px"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtQty_FilteredTextBoxExtender" runat="server" 
                                    Enabled="True" FilterType="Numbers" TargetControlID="txtQty">
                                </cc1:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="RfvQty" runat="server" 
                                    ControlToValidate="txtQty" ErrorMessage="Qty Required" InitialValue="0" 
                                    ValidationGroup="3">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtFQty" runat="server" EnableViewState="true" 
                                    ValidationGroup="2" Width="100px"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtFQty_FilteredTextBoxExtender" 
                                    runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtFQty">
                                </cc1:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="RfvFQty" runat="server" 
                                    ControlToValidate="txtFQty" Display="Dynamic" ErrorMessage="Quantity Required" 
                                    InitialValue="0" ValidationGroup="2">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Quantity")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                       
                    
                        <asp:TemplateColumn>
                            <EditItemTemplate>
                                <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="Update" 
                                    ValidationGroup="3">Update</asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:LinkButton ID="lnkAddNew" runat="server" CommandName="AddNew" 
                                    ValidationGroup="2">Add New</asp:LinkButton>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" 
                                    Text="Delete" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    
                   <PagerStyle  ForeColor="White" HorizontalAlign="Center" />
                  
                </asp:DataGrid>
 </div>
 </div>
      <footer>
      <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" Cssclass="btn btn-primary"
                    ValidationGroup="4" />

                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Cssclass="btn btn-primary"
                    onclick="btnCancel_Click1" />

                <asp:Button ID="btnDelete" runat="server" Text="Delete" Cssclass="btn btn-primary"/>
    
    </footer>
              
          
   
                    
          
 </asp:Content>
 