﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class InstrumentDisposal : Microsoft.Practices.CompositeWeb.Web.UI.Page, IDisposalView
    {
        private DisposalPresenter _presenter;

        private Disposal _Disposal;
        private Instrument _instrument;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();

            if (!Page.IsPostBack)
                BindControls();

            if (DisposalIdEdit == 0)
                this.lblDisposedById.Text = _presenter.GetUserList(Convert.ToInt32(_presenter.GetLocationId()[2])).FullName;
            if (InstrumentId != 0 || InstrumentIdEdit !=0)
            {
                if (InstrumentIdEdit == 0)
                    _instrument = _presenter.GetInstrumentById(InstrumentId);
                else
                    _instrument = _presenter.GetInstrumentById(InstrumentIdEdit);
                lblSelectedinstrument.Text = _instrument.InstrumentName;
                lblInstallationDate.Text = _instrument.InstallationDate.ToString();
                lblManufacturer.Text = _instrument.ManufacturerName;
                lblregionName.Text = _instrument.RegionName;
                lblSiteName.Text = _instrument.SiteName;
            }
           
        }
        [CreateNew]
        public DisposalPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
       

        private void BindControls()
        {
           
                if (DisposalIdEdit != 0)
                    this.lblDisposedById.Text = _presenter.GetUserList(_Disposal.DisposedByID).FullName;
                else
                    this.lblDisposedById.Text = _presenter.GetUserList(Convert.ToInt32(_presenter.GetLocationId()[2])).FullName;
                this.txtreason.Text = _Disposal.Reason;
                this.txtdate.Text = _Disposal.Date.ToString();
                this.btnDelete.Visible = (_Disposal.Id > 0);
                this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");
           
        }
        private void SaveDisposal()
        {
            try
            {
                if (_Disposal.IsNew())
                {
                    _presenter.SaveOrUpdateDisposal(_Disposal);
                    Master.ShowMessage(new AppMessage("Instrument Disposed", RMessageType.Info));
                    //_presenter.UpdateStatus("Disposal", 4, InstrumentId, "");
                   
                }
                else
                {
                    _presenter.SaveOrUpdateDisposal(_Disposal);
                    Master.ShowMessage(new AppMessage("Instrument Disposal Updated", RMessageType.Info));

                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _Disposal.InstrumentId = InstrumentId;
                if (DisposalIdEdit == 0)
                    _Disposal.DisposedByID = Convert.ToInt32(_presenter.GetLocationId()[2]); // current user id
                _Disposal.Reason = this.txtreason.Text;
                _Disposal.Date = Convert.ToDateTime(this.txtdate.Text.ToString());

                SaveDisposal();
            }

        }
        protected void btnNew_Click(object sender, EventArgs e)
        {

            string url = String.Format("~/Maintenance/InstrumentListForDisposal.aspx?{0}=2", AppConstants.TABID);

            Response.Redirect(url);

        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteDisposal(_Disposal.Id);
                Master.ShowMessage(new AppMessage("Disposal was deleted successfully", Chai.Maintenance.Enums.RMessageType.Info));

            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete Disposal. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["DisposalId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["DisposalId"]);
                else
                    return 0;
            }
        }

        public Disposal disposal
        {
            get { return _Disposal; }
            set { _Disposal = value; }
        }

        public int InstrumentId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentId"]);
                else
                    return 0;
            }
        }
        
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/Maintenance/InstrumentListForDisposal.aspx?{0}=2", AppConstants.TABID));
        }



        public int InstrumentIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]);
                else
                    return 0;
            }
        }

        public int DisposalIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["DisposalIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["DisposalIdEdit"]);
                else
                    return 0;
            }
        }
    }
}