﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="SchedulePreventiveMaintenance.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.SchedulePreventiveMaintenance" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %><%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc11" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblEditing" runat="server" 
                    style="color: #FF3300; background-color: #FFFFFF; font-weight: 700;" Text="Editing..." 
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        HeaderText="Error Message" ValidationGroup="1" class="alert alert-danger fade in"/>
   <asp:Label ID="errormsg" runat="server" Visible="false" ForeColor="Red" class="alert alert-danger fade in"></asp:Label>
   
      
     <div class="alert alert-info fade in">
       <i class="fa-fw fa fa-info"></i>
         <strong>Instrument Info/ </strong>
									   
                      
                    <asp:Label ID="lblInstrument" runat="server" Text="Selected Instrument" style="font-weight: 700"
                        ></asp:Label>
                   
                    <asp:Label ID="lblSelectedinstrument" runat="server"  
                     ></asp:Label>
                
                   
                    <asp:Label ID="lblRegion111" runat="server" Text="Region" style="font-weight: 700"></asp:Label>
                    
                    <asp:Label ID="lblregionName" runat="server" ></asp:Label>
                   
                    <asp:Label ID="Label1" runat="server" Text="Installation Date" style="font-weight: 700"></asp:Label>
                
                    <asp:Label ID="lblInstallationDate" runat="server" ></asp:Label>
                
                    <asp:Label ID="lblContract" runat="server" Text="Contracted With" style="font-weight: 700"
                        Visible="False" ></asp:Label>
               
                    <asp:Label ID="lblcontractedWith" runat="server" Visible="False" 
                        ></asp:Label>
                 
                    <asp:Label ID="lblSite111" runat="server" Text="Site" style="font-weight: 700" ></asp:Label>
               
                    <asp:Label ID="lblSiteName" runat="server" ></asp:Label>
                 
                    <asp:Label ID="label111" runat="server" Text="Last Preventive Maintenance Date" style="font-weight: 700"
                     ></asp:Label>
                 
                    <asp:Label ID="lblLastPreventivemain" runat="server" ></asp:Label>
               
                    <asp:Label ID="lblexpireDate" runat="server" Text="Warranty Expire Date" style="font-weight: 700"
                        Visible="False" ></asp:Label>
               
                    <asp:Label ID="lblWarantiyDate" runat="server" Visible="False"></asp:Label>
                </div>
                <div class="row">

	
		<article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">
        <div class="jarviswidget" id="Div1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Schedule Preventive Maintenance</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
                <section class="col col-6">  
                <label class="checkbox">
                <asp:CheckBox ID="chkAssign" runat="server" 
                    Text="Assign For External Enginer" AutoPostBack="True" 
                    oncheckedchanged="chkAssign_CheckedChanged"  /><i></i></label>
                </section></div>
           <div class="row">
                <section class="col col-6">  
                <asp:Label ID="lblexternalegn" runat="server" Text="External Enginer" 
                    Visible="False" CssClass="label"></asp:Label>
                             
            <label class="select">
                <asp:DropDownList ID="ddlExternalEnginer" runat="server"  
                    AppendDataBoundItems="True" Visible="False" DataTextField="FullName" 
                    DataValueField="Id" CssClass="textbox">
                    <asp:ListItem Value="0">Select External Enginer</asp:ListItem>
                </asp:DropDownList><i id="inlinenginer" runat="server" visible="false"></i></label></section></div>
                 <div class="row">
                <section class="col col-6">  
               <asp:Label ID="lblenginer" runat="server" Text="Enginer" CssClass="label"></asp:Label>
               <label class="select">
                <asp:DropDownList ID="ddlEnginer" runat="server" AppendDataBoundItems="True" 
                     DataTextField="FullName" DataValueField="Id" 
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlEnginer_SelectedIndexChanged" CssClass="textbox">
                    <asp:ListItem Value="0">Select Enginer</asp:ListItem>
                </asp:DropDownList><i></i></label></section></div>
          
                <div class="row">
                <section class="col col-6">  
           
                <asp:Label ID="Label7" runat="server" Text="Schedule Date From" 
                    CssClass="label"></asp:Label>
            <label class="input">
            <i class="icon-append fa fa-calendar"></i>
                        <asp:TextBox ID="CalendarFrom" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox>
                      
            
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="CalendarFrom" Display="Dynamic" 
                    ErrorMessage="Schedules Date From Required" ValidationGroup="1">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator22" 
                            runat="server" ControlToValidate="CalendarFrom" 
                            ErrorMessage="Schedule Date from is not valid" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator></label></section></div>
            
             <div class="row">
                <section class="col col-6">  
                <asp:Label ID="Label10" runat="server" Text="Schedule Date To" CssClass="label"></asp:Label>
               <label class="input">
               <i class="icon-append fa fa-calendar"></i>
                        <asp:TextBox ID="CalendarTo" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox>
                      
            
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="CalendarTo" Display="Dynamic" 
                    ErrorMessage="Scheduled Date To Required" ValidationGroup="1">*</asp:RequiredFieldValidator>
            &nbsp;<asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToCompare="CalendarTo" ControlToValidate="CalendarFrom" 
                    Display="Dynamic" 
                    ErrorMessage="Schedule date from should be lessthan to schedule date to" 
                    Operator="LessThanEqual" ValidationGroup="1" Type="Date">*</asp:CompareValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" 
                            runat="server" ControlToValidate="CalendarTo" 
                            ErrorMessage="Schedule Date To is not valid" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator></label></section></div>
                <div class="row">
                <section class="col col-6">  
                <asp:Label ID="Label8" runat="server" Text="Description" CssClass="label"></asp:Label>
           <label class="textarea">
                <asp:TextBox ID="txtdesc" runat="server"  TextMode="MultiLine" 
                    ></asp:TextBox></label></section></div>
            <div class="row">
                <section class="col col-6"> 
            
                <asp:Label ID="Label9" runat="server" Text="Date" CssClass="label"></asp:Label>
            <label class="input">
            <i class="icon-append fa fa-calendar"></i>
                <asp:TextBox ID="txtDate" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox>
            
           
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtDate" Display="Dynamic" ErrorMessage="Date Required" 
                    ValidationGroup="1">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator24" 
                    runat="server" ControlToValidate="txtDate" ErrorMessage="Date is not valid" 
                    ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                    ValidationGroup="1">*</asp:RegularExpressionValidator></label></section></div></fieldset>
                    <footer>
                       <asp:Button ID="btnNew" runat="server" Text="New" onclick="btnNew_Click" Cssclass="btn btn-primary"/>

                <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="1" 
                    onclick="btnSave_Click" Cssclass="btn btn-primary"/>

                <asp:Button ID="btnDelete" runat="server" Text="Delete" 
                    onclick="btnDelete_Click" Cssclass="btn btn-primary"/>

                <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    onclick="btnCancel_Click" Cssclass="btn btn-primary"/>
                    
                    </footer>
           
    </div></div></div></div></article>
    <article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">
        <div class="jarviswidget" id="Div2" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Enginer Booked Dates</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
       <asp:Calendar ID="Calendar2" BorderColor="#3366CC" 
                    BorderWidth="1px" CellPadding="1"
                                DayNameFormat="Shortest" Font-Names="Arial" Font-Size="8pt"
                                ForeColor="#003399" NextPrevFormat="ShortMonth" 
                     runat="server" ondayrender="Calendar2_DayRender" Height="164px" 
                    Width="278px" SelectedDate="11/15/2011 03:51:33" Visible="False"><WeekendDayStyle BackColor="#CCCCFF" /><DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle ForeColor="#ffffff" Font-Size="7pt"/>
                    <TitleStyle BackColor="#004488" BorderColor="#3366CC" BorderWidth="1px"  
                        Font-Bold="True" Font-Size="10pt"  ForeColor="#ffffff" Height="25px" 
                        Font-Names="Arial Narrow" />
                    </asp:Calendar>
     
             <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Booked.png" 
                    Visible="False" />
                                </div></fieldset></div></div></div></div></article>
    
    
    
    </div>
   
         
             
       
</asp:Content>
