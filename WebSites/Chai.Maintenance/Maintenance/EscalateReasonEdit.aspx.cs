﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;
using Chai.Maintenance.DataAccess.Configuration;
public partial class Maintenance_EscalateReasonEdit : System.Web.UI.Page
{
    public int InstrumentId = 0;
    public int ScheduleId = 0;
    private int EnginerId = 0;
    private int ProblemId = 0;
    private IList<EscalateReason> _escalateReasons;
    private IList<JobPushBackReason> jobPushBackReasons; 
    protected void Page_Load(object sender, EventArgs e)
    {
        InstrumentId = Convert.ToInt32(Request.QueryString["InstrumentId"]);
        ScheduleId = Convert.ToInt32(Request.QueryString["ScheduleId"]);
        EnginerId = Convert.ToInt32(Request.QueryString["EnginerId"]);
        ProblemId = Convert.ToInt32(Request.QueryString["ProblemId"]);
        BindReason();

    }
    void BindReason()
    {
        JobPushBackReasonDao  jobPushBackReasonDao = new JobPushBackReasonDao();
        jobPushBackReasons = jobPushBackReasonDao.GetJobPushBackReason(string.Empty);
        ddlReason.DataSource = jobPushBackReasons;
        ddlReason.DataBind();
    }
    protected void lnkEcsalate_Click(object sender, EventArgs e)
    {

        EscalateReason escalateReason = new EscalateReason();
        EscalateReasonDao escalateReasonDao = new EscalateReasonDao();
        escalateReason.InstrumentId = InstrumentId;
        escalateReason.ScheduleId = ScheduleId;
        escalateReason.ProblemId = ProblemId;
        escalateReason.EnginerId = EnginerId;
        escalateReason.ReasonId = Convert.ToInt32(ddlReason.SelectedValue);
        escalateReason.Date = Convert.ToDateTime(this.Calendar1.Text.ToString());
        escalateReasonDao.Save(escalateReason);
       
        lblEscalate.Text = "Job Pushed Back!";
        lnkEcsalate.Enabled = false;
         

    }

    protected void lnkClose_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.close()</script>");

    }
}