﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="EscalatedJobs.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.EscalatedJobs" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %><%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <div class="jarviswidget" id="Div1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Find Escalated Jobs</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
                <section class="col col-6">  
     
       
            
     
                    <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
                <label class="select">
                    <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                            onselectedindexchanged="ddlRegion_SelectedIndexChanged" 
                        >
                        <asp:ListItem Value="0">Select Region</asp:ListItem>
                    </asp:DropDownList><i></i></label></section>
                <section class="col col-6">  
                    <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                <label class="select">
                    <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                            DataTextField="Name" DataValueField="Id" 
                         AutoPostBack="True" >
                        <asp:ListItem Value="0">Select Site</asp:ListItem>
                    </asp:DropDownList><i></i></section></div></fieldset><footer></footer></div></div></div></div>
                   
  
    <h4>     <asp:Label ID="Lblll" ForeColor="Black" runat="server" Text=" Escalated Curative Maintenance Jobs From Regions" 
            CssClass="label"></asp:Label></h4>

   
    
    
    <asp:GridView ID="grvEscalatedJobs" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True"  CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                     GridLines="Horizontal"  
                    onrowdatabound="grvEscalatedJobs_RowDataBound" 
                 AllowPaging="True" 
             onpageindexchanging="grvEscalatedJobs_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="RegionName" 
                        HeaderText="Region" />
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="SerialNo" 
                        HeaderText="Serial No" />
                    <asp:BoundField DataField="EnginerName" HeaderText="Enginer" />
                    <asp:BoundField DataField="ProblemNumber" HeaderText="Problem Number" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplschedule" runat="server">Re-Schedule Curative Maintenance</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                        <asp:Button runat="server" ID="btnStatus" Text="" BorderStyle="None" />
                        </ItemTemplate>
                        </asp:TemplateField>
                </Columns>
                  <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:GridView>
    
  
    <br />
    <table style="width: 100%">
        <tr>
            <td align="right">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/EscalatedScheduleLegend.jpg" />
            </td>
        </tr>
    </table>
</asp:Content>
