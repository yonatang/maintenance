﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ProblemNotification.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.ProblemNotification" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc11" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
   
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblEditing" runat="server" 
                    style="color: #FF3300; background-color: #FFFFFF; font-weight: 700;" Text="Editing..." 
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
   
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        ValidationGroup="1" HeaderText="Error Message" class="alert alert-danger fade in"/>
    <div class="alert alert-info fade in">
       <i class="fa-fw fa fa-info"></i>
         <strong>Instrument Info!</strong>
        
                    <asp:Label ID="lblInstrument" runat="server" Text="Selected Instrument" 
                        style="font-weight: 700"></asp:Label> 
               
                    <asp:Label ID="lblSelectedinstrument" runat="server" 
                        ></asp:Label> 
               
                    <asp:Label ID="lblRegion" runat="server" Text="Region" style="font-weight: 700"></asp:Label> 
                
                    <asp:Label ID="lblregionName" runat="server"></asp:Label> 
               
                    <asp:Label ID="Label1" runat="server" Text="Installation Date" style="font-weight: 700"></asp:Label> 
               
                    <asp:Label ID="lblInstallationDate" runat="server"></asp:Label> 
              
                    <asp:Label ID="lblSite" runat="server" Text="Site" style="font-weight: 700"></asp:Label> 
               
                    <asp:Label ID="lblSiteName" runat="server"></asp:Label> 
                
                    <asp:Label ID="Label2" runat="server" Text="Manufacturer" style="font-weight: 700"></asp:Label> 
               
                    <asp:Label ID="lblManufacturer" runat="server"></asp:Label> 
                
                </div>
               <div class="jarviswidget" id="Div1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Report Problem</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
                <section class="col col-6">  
        
                <asp:Label ID="Label4" runat="server" Text="Problem Number" CssClass="label"></asp:Label>
               <label class="input">

                <asp:TextBox ID="lblProbNumber" runat="server" ReadOnly="true"
                                         ></asp:TextBox></label></section>
          	
                <section class="col col-6">  
                <asp:Label ID="lblProblemType" runat="server" Text="Problem Type" CssClass="label"></asp:Label>
               <label class="select">
                <asp:DropDownList ID="ddlProblemType" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="ddlProblemType_SelectedIndexChanged" 
                    DataTextField="Name" DataValueField="Id" ValidationGroup="1" 
                    AppendDataBoundItems="True" CssClass="textbox">
                    <asp:ListItem Value="0">Select Problem Type</asp:ListItem>
                </asp:DropDownList><i></i>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    Display="Dynamic" ErrorMessage="Select Problem Type" style="font-size: medium" 
                    ValidationGroup="1" ControlToValidate="ddlProblemType" InitialValue="0">*</asp:RequiredFieldValidator></label>
                </section></div>
                <div class="row">
                <section class="col col-6">  
          
                <asp:Label ID="lbldate" runat="server" Text="Date Reported" CssClass="label"></asp:Label>
           <label class="input">
       <i class="icon-append fa fa-calendar"></i>
                        <asp:TextBox ID="txtDateReported" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtDateReported" Display="Dynamic" ErrorMessage="Date Required" 
                    style="font-size: medium" ValidationGroup="1">*</asp:RequiredFieldValidator>

                
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                            ControlToValidate="txtDateReported" ErrorMessage="Date Reported is not a valid date" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator></label></section>
                <section class="col col-6">  
                <asp:Label ID="lblErrorCode" runat="server" Text="Error Code" CssClass="label"></asp:Label>
                <label class="select">
                <asp:DropDownList ID="ddlErrorCode" runat="server" AppendDataBoundItems="True" 
                     DataTextField="Name" DataValueField="Id" ValidationGroup="1" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Error Code</asp:ListItem>
                </asp:DropDownList><i></i>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    Display="Dynamic" ErrorMessage="Select Error Code " style="font-size: medium" 
                    ValidationGroup="1" ControlToValidate="ddlErrorCode">*</asp:RequiredFieldValidator></label></section>
                    </div>
                 
                 <div class="row">
                <section class="col col-6">  
           
                <asp:Label ID="lblDescription" runat="server" CssClass="label" 
                    Text="Description"></asp:Label>
                 <label class="textarea">
                 <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine" 
                    ></asp:TextBox></label></section>
                      
                <section class="col col-6"> 
                       <label class="checkbox">
                <asp:CheckBox ID="chkIsFunctional" runat="server" Text="Not Functional" 
                    CssClass="label" /><i></i></label></section></div>
                    </fieldset>
             <footer>
               
          
                    <asp:Button ID="btnNew" runat="server" onclick="btnNew_Click" Text="New" Cssclass="btn btn-primary"/>

                    <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" Cssclass="btn btn-primary"
                        ValidationGroup="1" />

                    <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" Cssclass="btn btn-primary"
                        Text="Delete" />

                    <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" Cssclass="btn btn-primary"
                        Text="Cancel"  />
                        </footer></div></div></div></div>
               
    </asp:Content>

