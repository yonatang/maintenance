﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class ConsumableNotificationList : Microsoft.Practices.CompositeWeb.Web.UI.Page,
                                                   IConsumableNotificationListView
    {
        private ConsumableNotificationListPresenter _presenter;
        IList<Chai.Maintenance.CoreDomain.Maintenance.ConsumableNotification> _consumableNotifications;
        IList<Site> _siteList;

        IList<Consumables> _consumables;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();

            if (_presenter.GetLocationId()[0] == 1) //site user
                ddlSite.Enabled = false;
            else if (_presenter.GetLocationId()[0] == 2) // region user
            {
                if (_presenter.GetLocationId()[1] == 1000) // federal user
                { 
                    BindSite(1000);
                    ddlSite.Enabled = true;
                }
                else //regional user, get list of site based on selected region
                {
                    BindSite(_presenter.GetLocationId()[1]); // passes the id 
                    ddlSite.Enabled = true;
                }

            }
            grvConsumableNotification.DataSource = _consumableNotifications;
            grvConsumableNotification.DataBind();

            BindConsumables();
        }
        [CreateNew]
        public ConsumableNotificationListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }


        private void BindSite(int regionId)
        {
            if (!IsPostBack)
            {
                _siteList = _presenter.GetSiteList(regionId);
                ddlSite.DataSource = _siteList;
                ddlSite.DataBind();
            }
        }
        private void BindConsumables()
        {
            if (!IsPostBack)
            {
                _consumables = _presenter.GetConsumablesList();
                ddlConsumables.DataSource = _consumables;
                ddlConsumables.DataBind();
            }
        }
        IList<ConsumableNotification> IConsumableNotificationListView.ConsumableNotificationList
        {
            set { _consumableNotifications = value; }
        }
        
        public int ConsumablesId
        {
            get { return Convert.ToInt32(ddlConsumables.SelectedValue); }
        }

        public int UserId
        {
            get {return _presenter.GetLocationId()[2]; }
        }

        public int SiteId
        {
            get
             {
                 if (Convert.ToInt32(ddlSite.SelectedValue)  != 0)
                     return Convert.ToInt32(ddlSite.SelectedValue);
                 else
                 {
                     if (_presenter.GetLocationId()[0] == 2)
                         return 0;
                     else
                         return _presenter.GetLocationId()[1];
                 }
                     
              }
        }
        protected void grvConsumableNotification_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ConsumableNotification consumableNotification = e.Row.DataItem as CoreDomain.Maintenance.ConsumableNotification;
            HyperLink lnkEdit = e.Row.FindControl("lnkEdit") as HyperLink;
            if (consumableNotification != null)
            {
                string url2 = String.Format("~/Maintenance/ConsumablenotificationEdit.aspx?{0}=2&ConsumableNotificationIdEdit={1}", AppConstants.TABID, consumableNotification.Id);
                lnkEdit.NavigateUrl = this.ResolveUrl(url2);
            }
        }
        protected void grvConsumableNotification_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvConsumableNotification.PageIndex = e.NewPageIndex;
            grvConsumableNotification.DataSource = _consumableNotifications;
            grvConsumableNotification.DataBind();
        }
}
}