﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ConsumablenotificationEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.ConsumablenotificationEdit" %>

<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %><%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc11" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblEditing" runat="server" 
                    style="color: #FF3300; background-color: #FFFFFF; font-weight: 700;" Text="Editing..." 
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
   
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-danger fade in"
        ValidationGroup="1" />
         <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Consumbale Notification</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>	
                    <div class="row">
									   
                      <section class="col col-6">  
                <asp:Label ID="Label11" runat="server" Text="Notified By" CssClass="label"></asp:Label>
                <label class="select">
                <asp:Label ID="lblNotifiedById" runat="server" style="font-weight: 700" 
                    CssClass="label"></asp:Label></label></section></div>				
								<div class="row">
									   
                      <section class="col col-6">  
   

  
     
   
                <asp:Label ID="Label13" runat="server" CssClass="label" Text="Site"></asp:Label>
                 <label class="select">
                <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                    DataTextField="Name" DataValueField="Id" CssClass="textbox">
                    <asp:ListItem Value="0">Select Site</asp:ListItem>
                </asp:DropDownList><i></i>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                    ControlToValidate="ddlSite" ErrorMessage="Site is Required" InitialValue="0" 
                    ValidationGroup="1">*</asp:RequiredFieldValidator></label></section></div>
           
       <div class="row">
									   
                      <section class="col col-6"> 
               
                <asp:Label ID="Label12" runat="server" Text="Consumable" CssClass="label"></asp:Label>
           <label class="select">
                <asp:DropDownList ID="ddlConsumbale" runat="server" AppendDataBoundItems="True" 
                    DataTextField="Name" DataValueField="Id" CssClass="textbox">
                    <asp:ListItem Value="0">Select Consumable</asp:ListItem>
                </asp:DropDownList><i></i>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                    ControlToValidate="ddlConsumbale" ErrorMessage="Consumable is required" 
                    InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator></label></section></div>
           <div class="row">
									   
                      <section class="col col-6"> 
            
                <asp:Label ID="Label9" runat="server" Text="Date" CssClass="label"></asp:Label>
          <label class="input">
                   <i class="icon-append fa fa-calendar"></i>
                     <asp:TextBox ID="txtdate" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"
                         ></asp:TextBox>
                   
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" 
                            runat="server" ControlToValidate="txtdate" ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator></label></section></div>
           <div class="row">
									   
                      <section class="col col-6"> 
           
                <asp:Label ID="Label8" runat="server" Text="Remark" CssClass="label"></asp:Label>
           <label class="textarea">
                <asp:TextBox ID="txtremark" runat="server"  TextMode="MultiLine" 
                    ></asp:TextBox></label></section></div>
          </fieldset>
          <footer>
                <asp:Button ID="btnNew" runat="server" Text="New" onclick="btnNew_Click" Cssclass="btn btn-primary"/>

                <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="1" Cssclass="btn btn-primary"
                    onclick="btnSave_Click" />

                <asp:Button ID="btnDelete" runat="server" Text="Delete" Cssclass="btn btn-primary"
                    onclick="btnDelete_Click" />

                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Cssclass="btn btn-primary"
                    onclick="btnCancel_Click" /></footer></div></div></div>
           </div>
</asp:Content>

