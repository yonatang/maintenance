﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="PreventiveMaintenance.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.PreventiveMaintenance" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
 
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc11" %>
 

 
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">

     <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblEditing" runat="server" 
                    style="color: #FF3300; background-color: #FFFFFF; font-weight: 700;" Text="Editing..." 
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-danger fade in"
         HeaderText="Error Message" ValidationGroup="4" />
    <div class="alert alert-info fade in"><i class="fa-fw fa fa-info"></i>
         <strong>Instrument Info!/ </strong>  
          
                  
                    <asp:Label ID="lblInstrument" runat="server" Text="Selected Instrument" 
                        style="font-weight: 700" ></asp:Label>
               
                    <asp:Label ID="lblSelectedinstrument" runat="server"  
                      ></asp:Label>
                
                    <asp:Label ID="lblSite" runat="server" Text="Site" style="font-weight: 700"></asp:Label>
                
                    <asp:Label ID="lblSiteName" runat="server" ></asp:Label>
                
                    <asp:Label ID="Label1" runat="server" Text="Installation Date" style="font-weight: 700"></asp:Label>
                
                    <asp:Label ID="lblInstallationDate" runat="server" ></asp:Label>
              
                    <asp:Label ID="Label8" runat="server" Text="Serial Number" style="font-weight: 700"></asp:Label>
               
                    <asp:Label ID="lblSerialNumber" runat="server"></asp:Label>
              
                    <asp:Label ID="Label6" runat="server" Text="Maintenance Type" style="font-weight: 700"></asp:Label>
           
                    <asp:Label ID="lblMaintenanceType" runat="server"></asp:Label>
            </div>

   <div class="jarviswidget" id="Div1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Instrument IQC Performed and Recorded?</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									 <section class="col col-6">     
                       <table style="width: 100%; margin-left: 3px;">
                        <tr>
                            <td style="width: 96px">
                                &nbsp;</td>
                            <td style="text-align: center; width: 260px;">
                                <b>
                                <asp:Label ID="Label27" runat="server" Text="Regularly" CssClass="label"></asp:Label>
                                </b>
                            </td>
                            <td style="text-align: center; width: 260px">
                                <b>
                                <asp:Label ID="Label28" runat="server" Text="Sometimes" CssClass="label"></asp:Label>
                                </b>
                            </td>
                            <td style="text-align: center; width: 260px">
                                <b>
                                <asp:Label ID="Label29" runat="server" Text="Never" CssClass="label"></asp:Label>
                                </b>
                            </td>
                            <td style="text-align: center; width: 260px">
                                <b>
                                <asp:Label ID="Label30" runat="server" Text="N/A" CssClass="label"></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 260px">
                                <asp:Label ID="Label24" runat="server" Text="Daily IQC" CssClass="label"></asp:Label>
                            </td>
                            <td style="text-align: center; width: 260px">
                               <asp:RadioButton ID="rbtD_Regular" runat="server" GroupName="Daily" 
                                    Checked="True"  />
                            </td>
                            <td style="text-align: center; width: 260px">
                                <asp:RadioButton ID="rbtD_smtms" runat="server" GroupName="Daily" 
                                    CssClass="label" />
                            </td>
                            <td style="text-align: center; width: 260px">
                                <asp:RadioButton ID="rbtD_Never" runat="server" GroupName="Daily" 
                                    />
                            </td>
                            <td style="text-align: center;width: 260px">
                               <asp:RadioButton ID="rbtD_NA" runat="server" GroupName="Daily" 
                                    />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 260px">
                                <asp:Label ID="Label25" runat="server" Text="Weekly IQC" CssClass="label"></asp:Label>
                            </td>
                            <td style="text-align: center; width: 260px">
                                <asp:RadioButton ID="rbtW_Regular" runat="server" GroupName="Weekly" 
                                    Checked="True"  />
                            </td>
                            <td style="text-align: center; width: 260px">
                               
                                <asp:RadioButton ID="rbtW_smtms" runat="server" GroupName="Weekly" 
                                   />
                            </td>
                            <td style="text-align: center; width: 260px">
                                <asp:RadioButton ID="rbtW_Never" runat="server" GroupName="Weekly" 
                                     />
                            </td>
                            <td style="text-align: center; width: 260px">
                                <asp:RadioButton ID="rbtW_NA" runat="server" GroupName="Weekly" 
                                     />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label26" runat="server" Text="Monthly IQC" CssClass="label"></asp:Label>
                            </td>
                            <td style="text-align: center; width: 148px">
                                <asp:RadioButton ID="rbtM_regular" runat="server" GroupName="Monthly" 
                                    Checked="True" CssClass="label" />
                            </td>
                            <td style="text-align: center; width: 259px">
                                <asp:RadioButton ID="rbtM_smtms" runat="server" GroupName="Monthly" 
                                    CssClass="label" />
                            </td>
                            <td style="text-align: center; width: 252px">
                               <asp:RadioButton ID="rbtM_Never" runat="server" GroupName="Monthly" 
                                    CssClass="label" />
                            </td>
                            <td style="text-align: center">
                               <asp:RadioButton ID="rbtM_NA" runat="server" GroupName="Monthly" 
                                    CssClass="label" />
                            </td>
                        </tr>
                    </table></section>
                    </div>
                    <div class="row">
                     <section class="col col-6">  
                     <asp:Label ID="Label21" runat="server" Text="Preventive Acion Taken" CssClass="label"></asp:Label> 
                     <label class="textarea">
                        <asp:TextBox ID="txtPreventiveActionTaken" runat="server"  
                     TextMode="MultiLine"  
                         ></asp:TextBox> </label>              
                     </section>
                      <section class="col col-6">  
                     <asp:Label ID="Label2" runat="server" 
                                       Text="Engineer Comments" CssClass="label"></asp:Label> 
                                        <label class="textarea">
                                       <asp:TextBox ID="txtEngComments" runat="server" 
                      TextMode="MultiLine"  
                         CssClass="textboxDescLong"></asp:TextBox></label></section>
                    </div>
                    <div class="row">
                     <section class="col col-6">
                     <label class="checkbox">
                     <asp:CheckBox ID="chkPerformance" runat="server" 
                    Text="  Performance Test Done" CssClass="label" /><i></i></label></section>
                 <section class="col col-6">
                 <label class="checkbox">
                <asp:CheckBox ID="chkFunctional" runat="server" 
                    Text="Equipment Fully Functional?" CssClass="label" /><i></i></label></section>
                 <section class="col col-6">
                 <label class="checkbox">
                <asp:CheckBox ID="chkFollowup" runat="server" Text="  Follow Up Required?" 
                    CssClass="label" /><i></i></label></section></div>
 <div class="row">
                     <section class="col col-6">
                       <asp:Label ID="Label19" runat="server" Text="Next Visit Scheduled"   CssClass="label"></asp:Label>
                  <label class="input">
   <asp:TextBox ID="txtDateNextVisit" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txtDateNextVisit" Display="Dynamic" 
                    ErrorMessage="Next Vist Scheduled Required" ValidationGroup="4">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator22" 
                            runat="server" ControlToValidate="txtDateNextVisit" 
                            ErrorMessage="Next Schedule Date is not valid" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="4">*</asp:RegularExpressionValidator></label></section>
  <section class="col col-6">
  <asp:Label ID="Label23" runat="server" Text="Date" CssClass="label"></asp:Label>
               
           <label class="input">
                               
                        <asp:TextBox ID="txtDate" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtDate" Display="Dynamic" ErrorMessage="Date Required" 
                    ValidationGroup="4">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" 
                            runat="server" ControlToValidate="txtDate" ErrorMessage="Date is not valid" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="4">*</asp:RegularExpressionValidator></label></section></div>

                </fieldset><footer>   <asp:HyperLink ID="hplChecklist" runat="server"  Cssclass="btn btn-primary">Fill Out Maintenance Checklist</asp:HyperLink></footer></div></div></div></div>
                               
           <div class="jarviswidget jarviswidget-color-darken jarviswidget-sortable" id="wid-id-0" data-widget-editbutton="false" role="widget" style="">
              <header role="heading">
    <h2>Spare parts replaced</h2>
    </header>
       <div role="content">     
                <asp:DataGrid ID="dgspare" runat="server" AutoGenerateColumns="False" 
                    CellPadding="0" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    DataKeyField="Id"  GridLines="None" 
                    oncancelcommand="dgspare_CancelCommand" 
                    ondeletecommand="dgspare_DeleteCommand" 
                    oneditcommand="dgspare_EditCommand" 
                    onitemcommand="dgspare_ItemCommand" 
                    onitemdatabound="dgspare_ItemDataBound" 
                    onupdatecommand="dgspare_UpdateCommand" ShowFooter="True" Width="99%">
                    <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="Part Name">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "SparePartName")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlSparepart" runat="server" AppendDataBoundItems="True" 
                                    DataTextField="Name" DataValueField="Id" Width="300px" CssClass="textbox">
                                    <asp:ListItem Value="0">Select Spare</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RfvSparepart" runat="server" 
                                    ControlToValidate="ddlSparepart" ErrorMessage="Sparepart Required" 
                                    InitialValue="0" ValidationGroup="3">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlFSparepart" runat="server" 
                                    AppendDataBoundItems="True" DataTextField="Name" 
                                    DataValueField="Id" EnableViewState="true" 
                                    ValidationGroup="2" Width="300px" CssClass="textbox">
                                    <asp:ListItem Value="0">Select Spare</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RfvFSparepart" runat="server" 
                                    ControlToValidate="ddlFSparepart" Display="Dynamic" 
                                    ErrorMessage="Sparepart Required" InitialValue="0" ValidationGroup="2">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                     
                    
                        <asp:TemplateColumn HeaderText="Quantity">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtQty" runat="server" 
                                    Text=' <%# DataBinder.Eval(Container.DataItem, "Quantity")%>' ValidationGroup="3" 
                                    Width="100px" CssClass="textbox"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtQty_FilteredTextBoxExtender" runat="server" 
                                    Enabled="True" FilterType="Numbers" TargetControlID="txtQty">
                                </cc1:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="RfvQty" runat="server" 
                                    ControlToValidate="txtQty" ErrorMessage="Qty Required" 
                                    ValidationGroup="3">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtFQty" runat="server" EnableViewState="true" 
                                    ValidationGroup="2" Width="100px" CssClass="textbox"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtFQty_FilteredTextBoxExtender" 
                                    runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtFQty">
                                </cc1:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="RfvFQty" runat="server" 
                                    ControlToValidate="txtFQty" Display="Dynamic" 
                                    ErrorMessage="Quantity Required" ValidationGroup="2">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Quantity")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                       
                    
                        <asp:TemplateColumn>
                            <EditItemTemplate>
                                <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="Update" 
                                    ValidationGroup="3">Update</asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:LinkButton ID="lnkAddNew" runat="server" CommandName="AddNew" 
                                    ValidationGroup="2">Add New</asp:LinkButton>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="lnkDelete0" runat="server" CommandName="Delete" 
                                    Text="Delete" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle  ForeColor="White" HorizontalAlign="Center" />
                </asp:DataGrid>
                </div>
       </div>
    <footer>
                <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" Cssclass="btn btn-primary"
                    ValidationGroup="4" />

                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Cssclass="btn btn-primary"
                    onclick="btnCancel_Click" />

                <asp:Button ID="btnDelete" runat="server" Text="Delete" onclick="btnDelete_Click" Cssclass="btn btn-primary"/></footer>
        
   
 </asp:Content>
