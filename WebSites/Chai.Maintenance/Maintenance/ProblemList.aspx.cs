﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class ProblemList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IProblemListView
    {
        private Chai.Maintenance.Modules.Maintenance.Views.ProblemListPresenter _presenter;

        IList<Problem> _problemList;
        IList<Chai.Maintenance.CoreDomain.Configuration.Site> _siteList;
        IList<Chai.Maintenance.CoreDomain.Configuration.ProblemType> _ProblemTypeList;
        int _instrumentId;
        IList<Chai.Maintenance.CoreDomain.Configuration.Region> _regionList;
        IList<Chai.Maintenance.CoreDomain.Configuration.InstrumentCategory> _instrumentCategoryList;
        Instrument _inst;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();


            if (_presenter.GetLocationId()[0] == 1) //site user 
            {
                lblRegion.Enabled = false;
                ddlRegion.Enabled = false;

                lblSite.Enabled = false;
                ddlSite.Enabled = false;
            }
            else if (_presenter.GetLocationId()[0] == 2) // region user
            {
                if (_presenter.GetLocationId()[1] == 1000) // federal user
                {
                    BindRegion();
                    BindSite(1000);
                    lblRegion.Enabled = true;
                    ddlRegion.Enabled = true;

                    lblSite.Enabled = true;
                    ddlSite.Enabled = true;
                }
                else //regional user, get list of site based on selected region
                {
                    BindSite(_presenter.GetLocationId()[1]); // passes the id 
                    lblRegion.Enabled = false;
                    ddlRegion.Enabled = false;

                    lblSite.Enabled = true;
                    ddlSite.Enabled = true;
                }

            }

            grvProblem.DataSource = _problemList;
            grvProblem.DataBind();
            BindProblemType();
            BindInstrumentCategory();
        }
        [CreateNew]
        public ProblemListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        private void BindRegion()
        {
            if (!IsPostBack)
            {
                _regionList = _presenter.GetRegionList();
                ddlRegion.DataSource = _regionList;
                ddlRegion.DataBind();
            }
        }
        private void BindSite(int regionId)
        {
            if (!IsPostBack)
            {
                _siteList = _presenter.GetSiteList(regionId);
                ddlSite.DataSource = _siteList;
                ddlSite.DataBind();
            }
        }
        private void BindProblemType()
        {
            if (!IsPostBack)
            {
                _ProblemTypeList = _presenter.GetProblemTypeList();
                ddlProblemtype.DataSource = _ProblemTypeList;
                ddlProblemtype.DataBind();
            }
        }
        private void BindInstrumentCategory()
        {
            if (!IsPostBack)
            {
                _instrumentCategoryList = _presenter.GetInstrumentCategoryList();
                ddlInstrumentCategory.DataSource = _instrumentCategoryList;
                ddlInstrumentCategory.DataBind();
            }
        }
        protected void grvProblem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            CoreDomain.Maintenance.Problem problem = e.Row.DataItem as CoreDomain.Maintenance.Problem;
            HyperLink lnkEdit = e.Row.FindControl("lnkEdit") as HyperLink;
            Button btnStatus = e.Row.FindControl("btnStatus") as Button;
            if (problem != null)
            {
                string url2 = String.Format("~/Maintenance/ProblemNotification.aspx?{0}=2&InstrumentIdEdit={1}&ProblemIdEdit={2}&UserIdEdit={3}", AppConstants.TABID, problem.InstrumentId, problem.Id, problem.UserId);
                lnkEdit.NavigateUrl = this.ResolveUrl(url2);
                lnkEdit.Enabled = false;

                if (problem.Status == "1")
                {
                    lnkEdit.Enabled = true;
                }
                else
                {
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C"); // Already on schedule or Maintenance Process
                    lnkEdit.Enabled = false;
                }
            }
        }
       
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            _siteList = _presenter.GetSiteList(Convert.ToInt32(ddlRegion.SelectedValue));
            if (ddlSite.Items.Count > 0)
            {
                ddlSite.Items.Clear();
            }
            ListItem lst = new ListItem();
            lst.Text = "Select Site";
            lst.Value = "0";
            ddlSite.Items.Add(lst);



            ddlSite.DataSource = _siteList;
            ddlSite.DataBind();
        }

     

        public int _SiteId//parameter from edit problem page after save, cancel clicked. to keep the selected problem
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["SiteID"]) != 0)
                    return Convert.ToInt32(Request.QueryString["SiteID"]);
                else
                    return 0;
            }
        }
        public int regionId
        {
            get
            {

                if (Convert.ToInt32(ddlRegion.SelectedValue) != 0)
                    return Convert.ToInt32(ddlRegion.SelectedValue);
                else
                {
                    if (_presenter.GetLocationId()[0] == 2)
                        return _presenter.GetLocationId()[1];
                    else
                        return 0;
                }
            }
        }

        public int _ProblemType
        {
            get { return Convert.ToInt32(ddlProblemtype.SelectedValue); }
        }

        public int SiteId
        {
            get
            {
                if (Convert.ToInt32(ddlSite.SelectedValue) != 0)
                    return Convert.ToInt32(ddlSite.SelectedValue);
                else
                {
                    if (_presenter.GetLocationId()[0] == 2)
                        return 0;
                    else
                        return _presenter.GetLocationId()[1];
                }
            }
        }
        public int _InstrumentCatId
        {
            get
            {
                { return Convert.ToInt32(ddlInstrumentCategory.SelectedValue); }
            }
        }
        public IList<Problem> _ProblemList
        {
            set { _problemList = value; }
        }
        protected void grvProblem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvProblem.PageIndex = e.NewPageIndex;
            grvProblem.DataSource = _problemList;
            grvProblem.DataBind();
        }
}
}