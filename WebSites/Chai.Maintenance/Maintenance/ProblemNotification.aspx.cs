﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Modules.Configuration.Views;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class ProblemNotification : Microsoft.Practices.CompositeWeb.Web.UI.Page, IProblemNotificationView
    {

        private ProblemNotificationPresenter _presenter;
       
        private Problem _Problem;
        private Instrument _instrument;
        private IList<ErrorCode> _errorCodeList;
      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();
           
            if (InstrumentId != 0 || InstrumentIdEdit != 0)
            {
                if (InstrumentIdEdit == 0) 
                {
                    _instrument = _presenter.GetInstrumentByIdForProblem(InstrumentId);
                    lblProbNumber.Text = AutoNumber();
                }
                else // Edit Mode
                {
                    _instrument = _presenter.GetInstrumentByIdForProblem(InstrumentIdEdit);
                    lblEditing.Visible = true;
                }

               
               
                lblSelectedinstrument.Text = _instrument.InstrumentName;
                lblInstallationDate.Text = _instrument.InstallationDate.ToString();
                lblManufacturer.Text = _instrument.ManufacturerName;
                lblregionName.Text = _instrument.RegionName;
                lblSiteName.Text = _instrument.SiteName;
            }
            if (!Page.IsPostBack & InstrumentIdEdit != 0)
                BindControls();
            BindProblemType();
        }
        [CreateNew]
        public ProblemNotificationPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private string AutoNumber()
        {
            string _AutoNumber = "";
            string _prefix = "";
            int _sequence = 0;
            _sequence = _presenter.GetAutoNumberByPageId((int)AspxPageType.ProblemNotification).Sequence + 1;
            _prefix = _instrument.InstrumentName;
            _AutoNumber = _prefix + " - " + _sequence;
            return _AutoNumber;
        }
        private void BindProblemType()
        {
            if (!IsPostBack)
            {
                ddlProblemType.DataSource = _presenter.GetProblemTypeList();
                ddlProblemType.DataBind();
            }
           
        }
        private void BindControls()
        {

            this.lblProbNumber.Text = _Problem.ProblemNumber;
            this.ddlProblemType.SelectedValue = _Problem.ProblemtypeId.ToString();
            this.ddlErrorCode.SelectedValue = _Problem.ErrorCodeId.ToString();
            this.txtdescription.Text = _Problem.Description;
            this.txtDateReported.Text = _Problem.DateReported.ToString();
            if (InstrumentIdEdit != 0) //edit mode
            {
                if (_instrument.Status == 2)
                    this.chkIsFunctional.Checked = false;
                else
                    this.chkIsFunctional.Checked = true;
            }
            this.btnDelete.Visible = (_Problem.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");

        }
        private void SaveProblem()
        {
            try
            {
                if (_Problem.IsNew())
                {

                    _presenter.SaveOrUpdateProblem(_Problem, chkIsFunctional.Checked);

                    Master.TransferMessage(new AppMessage("Instrument Problem Notified", RMessageType.Info));
                    //if (chkIsFunctional.Checked == true)
                    //    _presenter.UpdateStatus("ProblemNotification", 2, _instrument.Id,"");
                   // _presenter.CancelPage(_instrument.Id, _instrument.SiteId);
                    
                 

                    Response.Redirect(String.Format("~/Maintenance/MaintenanceInstrumentList.aspx?{0}=2", AppConstants.TABID));
                }
                else
                {
                    _presenter.SaveOrUpdateProblem(_Problem, chkIsFunctional.Checked);
                    Master.TransferMessage(new AppMessage("Instrument Problem Notification Updated", RMessageType.Info));
                    Response.Redirect(String.Format("~/Maintenance/MaintenanceInstrumentList.aspx?{0}=2", AppConstants.TABID));
                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
             
                if (this.IsValid)
                {
                    _Problem.ProblemNumber = lblProbNumber.Text;
                    _Problem.ProblemtypeId = Convert.ToInt32(ddlProblemType.SelectedValue);
                    _Problem.ErrorCodeId = Convert.ToInt32(ddlErrorCode.SelectedValue);
                    _Problem.Description = txtdescription.Text;
                    _Problem.InstrumentId = _instrument.Id;
                    _Problem.SiteId = _instrument.SiteId;
                    _Problem.DateReported = Convert.ToDateTime(txtDateReported.Text.ToString());
                    if (UserIdEdit == 0)
                        _Problem.UserId = Convert.ToInt32(Request.QueryString["UserId"]);
                    else
                        _Problem.UserId = UserIdEdit;
                    _Problem.Status = "1";
                    SaveProblem();
                }
        }
        protected void btnNew_Click(object sender, EventArgs e)
        {
             
            string url = String.Format("~/Maintenance/ProblemNotification.aspx?{0}=2&InstrumentId={1}", AppConstants.TABID, _instrument.Id);

            Response.Redirect(url);
             
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteProblem(_Problem.Id);
                Master.ShowMessage(new AppMessage("Problem was deleted successfully", Chai.Maintenance.Enums.RMessageType.Info));
               
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete Problem. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
          
            Response.Redirect(String.Format("~/Maintenance/MaintenanceInstrumentList.aspx?{0}=2", AppConstants.TABID));
        }

        public int InstrumentId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentId"]);
                else
                    return 0;

            }
        }
        public int InstrumentIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]);
                else
                    return 0;

            }
        }
        public int ProblemIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ProblemIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ProblemIdEdit"]);
                else
                    return 0;

            }
        }
        public int UserIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["UserIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["UserIdEdit"]);
                else
                    return 0;

            }
        }
        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ProblemId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ProblemId"]);
                else
                    return 0;

            }
        }
        Problem IProblemNotificationView._Problem
        {
            get { return _Problem; }
            set { _Problem = value; }
        }
        protected void ddlProblemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            _errorCodeList = _presenter.GetErrorCodeListByProblemTypeID(Convert.ToInt32(ddlProblemType.SelectedValue));
            if (ddlErrorCode.Items.Count > 0)
            {
                ddlErrorCode.Items.Clear();
            }
            ListItem lst = new ListItem();
            lst.Text = "Select Error Code";
            lst.Value = "0";
            ddlErrorCode.Items.Add(lst);
            ddlErrorCode.DataSource = _errorCodeList;
            ddlErrorCode.DataBind();
        }
}
}