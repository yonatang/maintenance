﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;
using Chai.Maintenance.DataAccess.Configuration;

public partial class Maintenance_PMChecklist : System.Web.UI.Page
{
    public int InstrumentNameId = 0;
    public int PrevMaintId = 0;
    private IList<ChecklistTask> _checklistTask;
    ChecklistTaskDao checklist = new ChecklistTaskDao();
    protected void Page_Load(object sender, EventArgs e)
    {
        InstrumentNameId = Convert.ToInt32(Request.QueryString["InstrumentNameId"]);
        PrevMaintId = Convert.ToInt32(Request.QueryString["PrevMaintenanceId"]);
        BindGridView();
    }
    void BindGridView()
    {
        if (!IsPostBack)
        {  
            if (InstrumentNameId != 0)
                _checklistTask = checklist.GetListOfChecklistTask(string.Empty, InstrumentNameId);
            this.dgChecklist.DataSource = _checklistTask;
            this.dgChecklist.DataBind();
        }
        dgChecklist.Columns[0].Visible = false;
    }
   
    protected void lnkClose_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.close()</script>");
    }
 
    
   
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        foreach (DataGridItem items in dgChecklist.Items)
        {
            PreventiveMaintenanceChecklist preventiveMaintenanceChecklist = new PreventiveMaintenanceChecklist();
            PreventiveMaintenanceChecklistDao preventiveMaintenanceChecklistDao =
                new PreventiveMaintenanceChecklistDao();

            Label lblId = items.FindControl("Label1") as Label;
            preventiveMaintenanceChecklist.TaskId = Convert.ToInt32(lblId.Text);
            CheckBox checkBox = items.FindControl("CheckBox1") as CheckBox;
            preventiveMaintenanceChecklist.IsCompleted = checkBox.Checked;
            TextBox txtremark = items.FindControl("txtremark") as TextBox;
            preventiveMaintenanceChecklist.Remarks = txtremark.Text;
            preventiveMaintenanceChecklist.PreventiveMaintenanceId = PrevMaintId;
            preventiveMaintenanceChecklistDao.Save(preventiveMaintenanceChecklist);
            lblChecklist.Text = "Checklist Saved!";
            lnkSave.Enabled = false;
            dgChecklist.Enabled = false;
        }

       
    }
   
}