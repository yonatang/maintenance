﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;
using Chai.Maintenance.DataAccess.Configuration;

public partial class Maintenance_PrintMaintenanceHistory : System.Web.UI.Page
{
    int instrumentId = 0;
    int problemId = 0;
    int scheduleId = 0;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
           
            btnPrint.Attributes.Add("onclick", "javascript:Clickheretoprint('divprint'); return false;");
            BindJS();

        }
        instrumentId = Convert.ToInt32(Request.QueryString["InstrumentId"]);
        problemId = Convert.ToInt32(Request.QueryString["ProblemId"]);
        scheduleId = Convert.ToInt32(Request.QueryString["ScheduleID"]);

        InstrumentDao instrumentDao = new InstrumentDao();
        Instrument instrument = instrumentDao.GetInstrumentById(instrumentId);
       
       
        lblInstallationDate.Text = Convert.ToString(instrument.InstallationDate);
        lblName.Text = instrument.InstrumentName;
        lblSerialNumber.Text = instrument.SerialNo;
        lblLastPrevMaintenanceDate.Text = Convert.ToString(instrument.LastPreventiveMaintenanceDate);
        lblManufacturer.Text = instrument.ManufacturerName;
        lblSiteName.Text = instrument.SiteName;
        lblDate.Text = Convert.ToString(DateTime.Now);
        BindHistory();
    }

    private void BindHistory()
    {

        MaintenanceHistoryDao _maintenanceDao = new MaintenanceHistoryDao();
        IList<MaintenanceHistory> _maintenanceHistory = _maintenanceDao.GetMaintenanceHistory(instrumentId, scheduleId, problemId);
        if (_maintenanceHistory != null)
        {
            dgvMaintenanceHistory.DataSource = _maintenanceHistory;
            dgvMaintenanceHistory.DataBind();

          
            lblMesage.Visible = false;
            dgvMaintenanceHistory.Visible = true;
        }
        else
        {
            lblMesage.Visible = true;
            dgvMaintenanceHistory.Visible = false;
        }

        
    }
    protected void BindJS()
    {
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Maintenancescripts", String.Format("<script language=\"JavaScript\" src=\"http://localhost/maintenance/Maintenance.js\"></script>\n"));
    }
}