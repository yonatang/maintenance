﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.DataAccess;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class AssignedJobs : Microsoft.Practices.CompositeWeb.Web.UI.Page, IAssignedJobsView
    {
        private AssignedJobsPresenter _presenter;
        IList<CoreDomain.Maintenance.AssignedJobs> _AssignedJobs;
        string searchBy = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();
         
            if (rbtCurative.Checked == true)
            {
                searchBy = "Curative";
                grvAssignedJobs.Visible = true;
                grvAssignedJobs0.Visible = false;
                _AssignedJobs = _presenter.GetAssignedJobs(searchBy, Convert.ToInt32(_presenter.GetLocationId()[2]), Convert.ToInt32(_presenter.GetLocationId()[1]));
                grvAssignedJobs.DataSource = _AssignedJobs;
                grvAssignedJobs.DataBind();
                Image1.Visible = true;
                Image2.Visible = false;
               
            }
            else if (rbtPreventive.Checked == true)
            {
                searchBy = "Preventive";
                grvAssignedJobs.Visible = false;
                grvAssignedJobs0.Visible = true;
                _AssignedJobs = _presenter.GetAssignedJobs(searchBy, Convert.ToInt32(_presenter.GetLocationId()[2]), Convert.ToInt32(_presenter.GetLocationId()[1]));
                grvAssignedJobs0.DataSource = _AssignedJobs;
                grvAssignedJobs0.DataBind();
                Image1.Visible = false;
                Image2.Visible = true;
            }
          
        }
        [CreateNew]
        public AssignedJobsPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

       



        IList<CoreDomain.Maintenance.AssignedJobs> IAssignedJobsView._AssignedJobs
        {
            set { _AssignedJobs = value; }
        }


        protected void grvAssignedJobs_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            CoreDomain.Maintenance.AssignedJobs assigned = e.Row.DataItem as CoreDomain.Maintenance.AssignedJobs;
            HyperLink lnkEdit = e.Row.FindControl("lnkEdit") as HyperLink;
            HyperLink hpl = e.Row.FindControl("hplMaintain") as HyperLink;
            HyperLink hplViewHistory = e.Row.FindControl("hplViewHistory") as HyperLink;
            HyperLink a = e.Row.FindControl("dsf") as HyperLink;
            Button btnStatus = e.Row.FindControl("btnStatus") as Button;
            if (assigned != null)
            {
                hplViewHistory.NavigateUrl = "javascript:;";
                hplViewHistory.Attributes.Add("onclick",
                                                 String.Format(
                                                     "window.open(\"PrintMaintenanceHistory.aspx?InstrumentId={0}&ScheduleID={1}&ProblemId={2}\", \"Preview\", \"width=700, height=490, resizable=false,scrollbars=1\")",
                                                     assigned.InstrumentId, assigned.ScheduleID, assigned.ProblemId));
                string url2 = String.Format("~/Maintenance/Maintenance.aspx?{0}=2&InstrumentId={1}&ProblemId={2}&ScheduleId={3}", AppConstants.TABID, assigned.InstrumentId, assigned.ProblemId, assigned.ScheduleID);
                hpl.NavigateUrl = this.ResolveUrl(url2);
             
                hpl.Enabled = false;
                //if (Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) != 2 && Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) != 5)
                //{
                     
                      if (Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) == 3)
                      {
                          btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF7251"); //Scheduled Not Yet Maintained 
                          //e.Row.ForeColor = System.Drawing.Color.White;
                        
                          hpl.Enabled = true;
                         
                      }
                      else if (Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) == 6)
                      {
                          btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C"); // Maintained But not resolved
                        
                          hpl.Enabled = false;
                      }  
                //}
                //else if (Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) == 2 || Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) == 5)
                //{
                //    if (Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) == 2)
                //        e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C"); //Maintained and Resolved -  But Not Yet Approved By User
                //    else if (Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) == 5)
                //        e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#99CC00"); // Maintained - User Approved and Resolved 

                //    hpl.Enabled = false;
                //    lnkEdit.Enabled = true;
                //}
            }
        }


        protected void grvAssignedJobs0_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            CoreDomain.Maintenance.AssignedJobs assigned = e.Row.DataItem as CoreDomain.Maintenance.AssignedJobs;

            if (assigned != null)
            {
                HyperLink hpl = e.Row.FindControl("hplMaintain0") as HyperLink;
                Button btnStatus = e.Row.FindControl("btnStatus") as Button;
                string url2 = String.Format("~/Maintenance/PreventiveMaintenance.aspx?{0}=2&InstrumentId={1}&ScheduleId={2}", AppConstants.TABID, assigned.InstrumentId, assigned.ScheduleID);
                hpl.NavigateUrl = this.ResolveUrl(url2);

                if (assigned.PreventiveScheduleStatus == "Performed" || assigned.PreventiveScheduleStatus == "Needs Approval")
                {
                    hpl.Enabled = false;
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#99CC00"); //Preventive Maintenance Performed
                }
                else if (assigned.PreventiveScheduleStatus == "Not Performed")
                {
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C");//Preventive Maintenance Not Performed Yet
                    hpl.Enabled = true;
                }
            }
        }


        protected void grvAssignedJobs0_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvAssignedJobs0.PageIndex = e.NewPageIndex;
            grvAssignedJobs0.DataSource = _AssignedJobs;
            grvAssignedJobs0.DataBind();
        }
        protected void grvAssignedJobs_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvAssignedJobs.PageIndex = e.NewPageIndex;
            grvAssignedJobs.DataSource = _AssignedJobs;
            grvAssignedJobs.DataBind();
        }
        
}


}