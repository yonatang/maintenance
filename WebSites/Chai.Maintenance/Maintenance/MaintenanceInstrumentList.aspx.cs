﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class MaintenanceInstrumentList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IMaintenanceInstrumentListView
    {
        private Chai.Maintenance.Modules.Maintenance.Views.MaintenanceInstrumentListPresenter _presenter;
        
        IList<Chai.Maintenance.CoreDomain.Configuration.Instrument> _instrumentList;
        IList<Chai.Maintenance.CoreDomain.Configuration.Site> _siteList;
        IList<Chai.Maintenance.CoreDomain.Configuration.InstrumentCategory> _instrumentCategoryList;
        int _instrumentId;
        IList<Chai.Maintenance.CoreDomain.Configuration.Region> _regionList;
        Instrument _inst;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();

            
            if (_presenter.GetLocationId()[0] == 1) //site user
            {
                lblRegion.Enabled = false;
                ddlRegion.Enabled = false;

                lblSite.Enabled = false;
                ddlSite.Enabled = false;
            }
            else if(_presenter.GetLocationId()[0] == 2) // region user
            {
                if (_presenter.GetLocationId()[1] == 1000) // federal user
                {
                    BindRegion();
                    BindSite(1000);
                    lblRegion.Enabled = true;
                    ddlRegion.Enabled = true;

                    lblSite.Enabled = true;
                    ddlSite.Enabled = true;
                }
                else //regional user, get list of site based on selected region
                {
                    BindSite(_presenter.GetLocationId()[1]); // passes the id 
                    lblRegion.Enabled = false;
                    ddlRegion.Enabled = false;

                    lblSite.Enabled = true;
                    ddlSite.Enabled = true;
                }
                 
            }
          
                grvInstrumentList.DataSource = _instrumentList;
                grvInstrumentList.DataBind();
          

            BindInstrumentCategory();

          
        }
        private void BindRegion()
        {
            if (!IsPostBack)
            {
                _regionList = _presenter.GetRegionList();
                ddlRegion.DataSource = _regionList;
                ddlRegion.DataBind();
            }
        }
        private void BindSite(int regionId)
        {
            if (!IsPostBack)
            {
                _siteList = _presenter.GetSiteList(regionId);
                ddlSite.DataSource = _siteList;
                ddlSite.DataBind();
            }
        }
        private void BindInstrumentCategory()
        {
            if (!IsPostBack)
            {
                _instrumentCategoryList = _presenter.GetInstrumentCategoryList();
                ddlInstrumentCategory.DataSource = _instrumentCategoryList;
                ddlInstrumentCategory.DataBind();
            }
        }

        
        [CreateNew]
        public MaintenanceInstrumentListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }



      
        protected void grvInstrumentList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Instrument instrument = e.Row.DataItem as Instrument;
            if (instrument != null)
            {
                HyperLink hpl = e.Row.FindControl("hplNotifyProblem") as HyperLink;
                Button btnStatus = e.Row.FindControl("btnStatus") as Button;
                string url = String.Format("~/Maintenance/ProblemNotification.aspx?{0}=2&InstrumentId={1}&UserId={2}", AppConstants.TABID, instrument.Id, _presenter.GetLocationId()[2]);
                if (instrument.Status == 2) // Not Functional
                {
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF7251"); 
                    hpl.Enabled = false;
                }
                else if (instrument.Status == 1) // Functional
                {
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#99CC00");
                    hpl.Enabled = true;
                }
                else if(instrument.Status == 5) // Functional With problem
                {
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C");
                }
                hpl.NavigateUrl = this.ResolveUrl(url);
            }
        }
        #region instrument view

         public int ddlinstrumentId
        {
            get { return Convert.ToInt32(ddlInstrumentCategory.SelectedValue); }
        }

         public int siteId
         {

             get
             {
                 if (Convert.ToInt32(ddlSite.SelectedValue) != 0)
                     return Convert.ToInt32(ddlSite.SelectedValue);
                 else
                 {
                     if (_presenter.GetLocationId()[0] == 2)
                         return 0;
                     else
                         return _presenter.GetLocationId()[1];
                 }

             }

         }
         public int _siteId//parameter from Problem notification page after save, cancel clicked. to keep the selected instrument
         {
             get
             {
                 if (Convert.ToInt32(Request.QueryString["SiteID"]) != 0)
                     return Convert.ToInt32(Request.QueryString["SiteID"]);
                 else
                     return 0;
             }
         }
         public int instrumentCategoryID
         {
             get
             {
                 { return  Convert.ToInt32(ddlInstrumentCategory.SelectedValue); }
             }
         }
         public int Functionality
         {
             get
             {
                 { return Convert.ToInt32(ddlFunctionality.SelectedValue); }
             }
         }
         public int regionId
         {
             get
             {

                 if (Convert.ToInt32(ddlRegion.SelectedValue) != 0)
                     return Convert.ToInt32(ddlRegion.SelectedValue);
                 else
                 {
                     if (_presenter.GetLocationId()[0] == 2)
                         return _presenter.GetLocationId()[1];
                     else
                         return _presenter.ReturnRegionId();
                
                 }

             }

         }

        public IList<CoreDomain.Configuration.Instrument> instrumentlist
        {
            set { _instrumentList = value; }
        }

       
        #endregion


        public int _instrumentID //parameter from Problem notification page after save, cancel clicked. to keep the selected instrument
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["instrumentID"]) != 0)
                    return Convert.ToInt32(Request.QueryString["instrumentID"]);
                else
                    return 0;
            }
        }
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            _siteList = _presenter.GetSiteList(Convert.ToInt32(ddlRegion.SelectedValue));
            if (ddlSite.Items.Count > 0)
            {
                ddlSite.Items.Clear();
            }
            ListItem lst = new ListItem();
            lst.Text = "Select Site";
            lst.Value = "0";
            ddlSite.Items.Add(lst);



            ddlSite.DataSource = _siteList;
            ddlSite.DataBind();
        }
        protected void lnkConfirm_Click(object sender, EventArgs e)
        {

        }
        protected void grvInstrumentList_SelectedIndexChanged(object sender, EventArgs e)
        {
          

        }
        protected void grvInstrumentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvInstrumentList.PageIndex = e.NewPageIndex;
            grvInstrumentList.DataSource = _instrumentList;
            grvInstrumentList.DataBind();
        }
}
}