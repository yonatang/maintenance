﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="AssignedJobs.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.AssignedJobs" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Assigned Jobs</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
               <label class="radio">
                        <asp:RadioButton ID="rbtPreventive" runat="server" AutoPostBack="True" 
                            GroupName="1" Text=""  /><i></i>Show Preventive Jobs</label></section>
                   <section class="col col-6">   
              <label class="radio">
                        <asp:RadioButton ID="rbtCurative" runat="server" AutoPostBack="True" 
                            GroupName="1" Text="" /><i></i>Show Curative Jobs</label></section></div></fieldset>
                   </div></div></div></div>

            <asp:GridView ID="grvAssignedJobs" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True"  CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                     GridLines="Horizontal" 
                 
            onrowdatabound="grvAssignedJobs_RowDataBound" Visible="False" 
            AllowPaging="True" onpageindexchanging="grvAssignedJobs_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="PreventiveMaintenancePeriod" 
                        HeaderText="Preventive Maint Period" />
                    <asp:BoundField DataField="LastPreventiveMaintenanceDate" 
                        HeaderText="Last Preventive Maint. Date" />
                    <asp:BoundField DataField="ErrorCode" HeaderText="Error Code" />
                    <asp:BoundField DataField="Type" HeaderText="Type" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplMaintain" runat="server" 
                                Text="Maintain Instrument"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit Maintenance History"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplViewHistory" runat="server">View History</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField >
                    <ItemTemplate>
                        <asp:Button runat="server" ID="btnStatus" Text="" BorderStyle="None" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                 <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:GridView>
      



            
            <asp:GridView ID="grvAssignedJobs0" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    GridLines="Horizontal" 
               
            onrowdatabound="grvAssignedJobs0_RowDataBound" Visible="False" AllowPaging="True" 
                            onpageindexchanging="grvAssignedJobs0_PageIndexChanging">
               
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="PreventiveMaintenancePeriod" 
                        HeaderText="Preventive Maint Period" />
                    <asp:BoundField DataField="LastPreventiveMaintenanceDate" 
                        HeaderText="Last Preventive Maint. Date" />
                    <asp:BoundField DataField="Type" HeaderText="Type" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplMaintain0" runat="server" 
                                Text="Maintain Instrument"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit0" runat="server" Text="Edit Maintenance History"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField >
                    <ItemTemplate>
                        <asp:Button runat="server" ID="btnStatus" Text="" BorderStyle="None" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                 <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:GridView>
            <table><tr>
                        <td></td>
                </tr>
                 
                <tr>
                    <td style="text-align: right">
                    
                        <%-- </fieldset>--%>
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Preventivelegend.png" 
                            Visible="False" />
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/curMaintenance.png" 
                            Visible="False" />
                       <%-- </fieldset>--%>
                        </td>
                </tr>
    </table>
    
     
</asp:Content>




