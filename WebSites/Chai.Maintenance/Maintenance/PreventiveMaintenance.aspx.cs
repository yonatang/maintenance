﻿using System;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class PreventiveMaintenance : Microsoft.Practices.CompositeWeb.Web.UI.Page, IPreventiveMaintenanceView
    {
        private PreventiveMaintenancePresenter _presenter;
        private CoreDomain.Maintenance.PreventiveMaintenance _Preventive;
        private Instrument _instrument;
        private IList<ChecklistTask> checkList = null;
        private Schedule _Schedule;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();

            if (InstrumentId != 0 | InstrumentIdEdit != 0)
            {
                if (InstrumentIdEdit == 0)
                {
                    _instrument = _presenter.GetInstrumentByIdForProblem(InstrumentId);
                }
                else
                {
                    _instrument = _presenter.GetInstrumentByIdForProblem(InstrumentIdEdit); //editing
                    lblEditing.Visible = true;
                }
                lblSelectedinstrument.Text = _instrument.InstrumentName;
                lblSiteName.Text = _instrument.SiteName;
                lblSerialNumber.Text = _instrument.SerialNo;
                lblInstallationDate.Text = _instrument.InstallationDate.ToString();
                lblMaintenanceType.Text = "Preventive";
            }
            if (!IsPostBack)
            {
                BindPreventiveMaintenanceControls();
                BindSpareparts();
            }
        }
        [CreateNew]
        public PreventiveMaintenancePresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        void checklist()
        {
            if (_instrument != null)
                checkList = _presenter.GetListOfChecklistTask(_instrument.nameId);
            if (checkList.Count == 0)
            {

                hplChecklist.Text = string.Format("The Selected Instrument Does Not Have A Checklist");
                hplChecklist.ForeColor = System.Drawing.Color.Red;
                hplChecklist.Enabled = false;
            }
            else
            {
                if (_Preventive.Id > 0)
                {
                    this.hplChecklist.NavigateUrl = "javascript:;";
                    this.hplChecklist.Attributes.Add("onclick",
                                                     String.Format(
                                                         "window.open(\"PMChecklist.aspx?InstrumentNameId={0}&PrevMaintenanceId={1}\", \"Preview\", \"width=675, height=490, resizable=false,scrollbars=1\")",
                                                         _instrument.nameId, _Preventive.Id));
                }
                else
                {
                    hplChecklist.Enabled = false;
                }
            }
        }
        private void BindSparepartType(DropDownList ddlSparepart, int InstrumentNameId)
        {
            ddlSparepart.DataSource = _presenter.GetSparepartType(InstrumentNameId);
            ddlSparepart.DataBind();
        }
        private void BindPreventiveMaintenanceControls()
        {
            this.txtPreventiveActionTaken.Text = _Preventive.PreventiveActionTaken;
            this.txtEngComments.Text = _Preventive.EngineerComments;
            this.txtDateNextVisit.Text = _Preventive.NextVistScheduledDate.ToString();
            this.txtDate.Text = _Preventive.Date.ToString();
            this.chkPerformance.Checked = _Preventive.PerformanceTestsDone;
            this.chkFollowup.Checked = _Preventive.FollowUpRequired;
            this.chkFunctional.Checked = _Preventive.EquipmentFullyFunctional;

            if (_Preventive.DailyIQC == "Regular")
                rbtD_Regular.Checked = true;
            else if (_Preventive.DailyIQC == "Sometimes")
                rbtD_smtms.Checked = true;
            else if (_Preventive.DailyIQC == "Never")
                rbtD_Never.Checked = true;
            else if (_Preventive.DailyIQC == "Not Applicable")
                rbtD_NA.Checked = true;


            if (_Preventive.WeeklyIQC == "Regular")
                rbtW_Regular.Checked = true;
            else if (_Preventive.WeeklyIQC == "Sometimes")
                rbtW_smtms.Checked = true;
            else if (_Preventive.WeeklyIQC == "Never")
                rbtW_Never.Checked = true;
            else if (_Preventive.WeeklyIQC == "Not Applicable")
                rbtW_NA.Checked = true;


            if (_Preventive.MonthlyIQC == "Regular")
                rbtM_regular.Checked = true;
            else if (_Preventive.MonthlyIQC == "Sometimes")
                rbtM_smtms.Checked = true;
            else if (_Preventive.MonthlyIQC == "Never")
                rbtM_Never.Checked = true;
            else if (_Preventive.MonthlyIQC == "Not Applicable")
                rbtM_NA.Checked = true;

            this.btnDelete.Visible = (_Preventive.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");
        }

        #region interface
        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["CurativeId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["CurativeId"]);
                else
                    return 0;
            }
        }
        public int DataMode
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["DataMode"]) != 0)
                    return Convert.ToInt32(Request.QueryString["DataMode"]);
                else
                    return 0;
            }
        }
        public int InstrumentIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]);
                else
                    return 0;
            }
        }
        public int PreventiveIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["PreventiveIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["PreventiveIdEdit"]);
                else
                    return 0;
            }
        }
        public int ScheduleIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ScheduleIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ScheduleIdEdit"]);
                else
                    return 0;
            }
        }
        public CoreDomain.Maintenance.PreventiveMaintenance preventive
        {
            get
            {
                return _Preventive;
            }
            set
            {
                _Preventive = value;
            }
        }

        public int InstrumentId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentId"]);
                else
                    return 0;
            }
        }
        public int ScheduleId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ScheduleId"]) != 0)
                    return (Convert.ToInt32(Request.QueryString["ScheduleId"]));
                else
                    return 0;
            }
        }
        #endregion

        private void SavePreventiveMaintenance()
        {
            try
            {
                if (_Preventive.IsNew())
                {
                    _presenter.SaveOrUpdatePreventiveMaintenance(_Preventive);
                    checklist();
                    Master.ShowMessage(new AppMessage("Preventive Maintenance saved", RMessageType.Info));
                    if (this.chkFunctional.Checked)
                    {
                        //_presenter.UpdateStatus("PreventiveMaintenanceForInstrument", 1, InstrumentId); //changes the status of instrument to Functional
                        //_presenter.UpdateStatus("PreventiveMaintenanceForSchedule", 1, ScheduleId,""); //1=Performed And Needs Approval
                    }
                }
                else
                {
                    _presenter.SaveOrUpdatePreventiveMaintenance(_Preventive);
                    Master.ShowMessage(new AppMessage("Preventive Maintenance Updated", RMessageType.Info));
                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _Preventive.PreventiveActionTaken = this.txtPreventiveActionTaken.Text;           
                _Preventive.EngineerComments = this.txtEngComments.Text;
                _Preventive.NextVistScheduledDate = Convert.ToDateTime(this.txtDateNextVisit.Text);
                _Preventive.Date = Convert.ToDateTime(this.txtDate.Text.ToString());
                _Preventive.PerformanceTestsDone = this.chkPerformance.Checked;
                _Preventive.FollowUpRequired = this.chkFollowup.Checked;
                _Preventive.EquipmentFullyFunctional = this.chkFunctional.Checked;
                _Preventive.InstrumentId = _instrument.Id;
                _Preventive.EnginerId = Convert.ToInt32(_presenter.GetLocationId()[2]); // current user id
                if (ScheduleIdEdit == 0)
                    _Preventive.ScheduleId = ScheduleId;
                else
                    _Preventive.ScheduleId = ScheduleIdEdit;
                
                if (rbtD_Regular.Checked)
                    _Preventive.DailyIQC = "Regular";
                else if (rbtD_smtms.Checked)
                    _Preventive.DailyIQC = "Sometimes";
                else if (rbtD_Never.Checked)
                    _Preventive.DailyIQC = "Never";
                else if (rbtD_NA.Checked)
                    _Preventive.DailyIQC = "Not Applicable";

                if (rbtW_Regular.Checked)
                    _Preventive.WeeklyIQC = "Regular";
                else if (rbtW_smtms.Checked)
                    _Preventive.WeeklyIQC = "Sometimes";
                else if (rbtW_Never.Checked)
                    _Preventive.WeeklyIQC = "Never";
                else if (rbtW_NA.Checked)
                    _Preventive.WeeklyIQC = "Not Applicable";

                if (rbtM_regular.Checked)
                    _Preventive.MonthlyIQC = "Regular";
                else if (rbtM_smtms.Checked)
                    _Preventive.MonthlyIQC = "Sometimes";
                else if (rbtM_Never.Checked)
                    _Preventive.MonthlyIQC = "Never";
                else if (rbtM_NA.Checked)
                    _Preventive.MonthlyIQC = "Not Applicable";

                SavePreventiveMaintenance();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeletePreventiveMaintenance(_Preventive.Id);
                Master.ShowMessage(new AppMessage("Preventive Maintenance Was Deleted Sucessfully", Chai.Maintenance.Enums.RMessageType.Info));

            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Error: Unable to delete Preventive Maintenance" + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }

        #region Spareparts
        private void BindSpareparts()
        {
            dgspare.DataSource = _Preventive.Sapres;
            dgspare.DataBind();
        }

        protected void dgspare_CancelCommand(object source, DataGridCommandEventArgs e)
        {
            this.dgspare.EditItemIndex = -1;
            BindSpareparts();
        }
        protected void dgspare_DeleteCommand(object source, DataGridCommandEventArgs e)
        {
            try
            {
                LinkButton lnkbtn = e.Item.FindControl("lnkDelete") as LinkButton;
                if (lnkbtn != null)
                    lnkbtn.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this Sparepart?');");

                if (_Preventive.Sapres[e.Item.DataSetIndex].Id != 0)
                {
                    _presenter.DeleteSpareparts(_Preventive.Sapres[e.Item.DataSetIndex].Id);
                }
                _Preventive.Sapres.Remove(_Preventive.Sapres[Convert.ToInt32(e.Item.DataSetIndex)]);
                BindSpareparts();

                Master.ShowMessage(new AppMessage("Sparepart was Removed Sucessfully", Chai.Maintenance.Enums.RMessageType.Info));
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Error: Unable to delete Sparepart item Detail. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        protected void dgspare_EditCommand(object source, DataGridCommandEventArgs e)
        {
            this.dgspare.EditItemIndex = e.Item.ItemIndex;

            BindSpareparts();
        }
        protected void dgspare_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "AddNew")
            {
                try
                {
                    Spareparts spare = new Spareparts();


                    DropDownList ddlSparepart = e.Item.FindControl("ddlFSparepart") as DropDownList;
                    spare.SparepartId = int.Parse(ddlSparepart.SelectedValue);
                    spare.SparePartName = ddlSparepart.SelectedItem.Text;
                    TextBox txtqty = e.Item.FindControl("txtFQty") as TextBox;
                    spare.Quantity = Convert.ToInt32(txtqty.Text);
                    _Preventive.Sapres.Add(spare);
                    Master.ShowMessage(new AppMessage("Sparepart Detail Added Sucessfully.", Chai.Maintenance.Enums.RMessageType.Info));
                    dgspare.EditItemIndex = -1;
                    BindSpareparts();
                }
                catch (Exception ex)
                {
                    Master.ShowMessage(new AppMessage("Error: Unable to Add Sparepart " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
                }
            }
        }
        protected void dgspare_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Footer)
            {
                DropDownList ddlFSparepart = e.Item.FindControl("ddlFSparepart") as DropDownList;
                BindSparepartType(ddlFSparepart, _instrument.nameId);

            }
            else
            {

                if (_Preventive.Sapres != null)
                {
                    LinkButton lnkbtn = e.Item.FindControl("lnkDelete") as LinkButton;
                    if (lnkbtn != null)
                        lnkbtn.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this Sparepart?');");
                    DropDownList ddlSparepart = e.Item.FindControl("ddlSparepart") as DropDownList;
                    if (ddlSparepart != null)
                    {
                        BindSparepartType(ddlSparepart, _instrument.nameId);
                        if (_Preventive.Sapres[e.Item.DataSetIndex].SparepartId != null)
                        {
                            ListItem li = ddlSparepart.Items.FindByValue((_Preventive.Sapres[e.Item.DataSetIndex].SparepartId.ToString()));
                            if (li != null)
                                li.Selected = true;
                        }
                    }

                }

            }
        }
        protected void dgspare_UpdateCommand(object source, DataGridCommandEventArgs e)
        {
            Spareparts Spare = _Preventive.Sapres[e.Item.DataSetIndex];
            try
            {

                DropDownList ddlSparepart = e.Item.FindControl("ddlSparepart") as DropDownList;
                Spare.SparepartId = int.Parse(ddlSparepart.SelectedValue);
                Spare.SparePartName = ddlSparepart.SelectedItem.Text;

                TextBox txtqty = e.Item.FindControl("txtQty") as TextBox;
                Spare.Quantity = Convert.ToInt32(txtqty.Text);

                _Preventive.Sapres.Insert(Convert.ToInt32(e.Item.DataSetIndex), Spare);
                _Preventive.Sapres.RemoveAt(e.Item.DataSetIndex);
                Master.ShowMessage(new AppMessage("Spareparts Updated Sucessfully.", Chai.Maintenance.Enums.RMessageType.Info));
                dgspare.EditItemIndex = -1;
                BindSpareparts();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Error: Unable to Update Spareparts. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        #endregion



 
}
}