﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ConsumableNotificationList.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.ConsumableNotificationList" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    
   
                <div class="jarviswidget" id="Div1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Find Consumable Notification</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
                <section class="col col-6">  
       
                    <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                <label class="select">
                    <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                        AutoPostBack="True"   DataTextField="Name" DataValueField="Id" 
                        CssClass="textbox">
                        <asp:ListItem Value="0">Select Site</asp:ListItem>
                    </asp:DropDownList><i></i></label></section>
                 <section class="col col-6">  
                    <asp:Label ID="lblConsumable" runat="server" Text="Consumable" CssClass="label"></asp:Label>
                <label class="select">
                    <asp:DropDownList ID="ddlConsumables" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                          AutoPostBack="True" CssClass="textbox">
                        <asp:ListItem Value="0">Select Consumables</asp:ListItem>
                    </asp:DropDownList><i></i></label></section></div></fieldset><footer></footer></div></div></div>
                
  
    <asp:GridView ID="grvConsumableNotification" runat="server" AutoGenerateColumns="False" 
                 DataKeyNames="Id" EnableModelValidation="True"  CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    ForeColor="#333333" GridLines="Horizontal" 
               
            onrowdatabound="grvConsumableNotification_RowDataBound" 
        AllowPaging="True" 
        onpageindexchanging="grvConsumableNotification_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="ConsumableName" HeaderText="Consumable" />
                    <asp:BoundField DataField="NotifiedBy" 
                        HeaderText="Notified By" />
                    <asp:BoundField DataField="Date" HeaderText="Date" />
                    <asp:TemplateField>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit Notification"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                 <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:GridView>
            </div>
</asp:Content>

