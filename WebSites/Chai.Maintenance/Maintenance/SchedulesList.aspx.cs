﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class SchedulesList : Microsoft.Practices.CompositeWeb.Web.UI.Page, ISchedulesListView
    {
        private Chai.Maintenance.Modules.Maintenance.Views.SchedulesListPresenter _presenter;

        IList<Schedule> _ScheduleList;
        IList<Chai.Maintenance.CoreDomain.Configuration.Site> _siteList;
        IList<Chai.Maintenance.CoreDomain.Configuration.InstrumentCategory> _instrumentCategoryList;
        int _instrumentId;
        IList<Chai.Maintenance.CoreDomain.Configuration.Region> _regionList;
        Instrument _inst;
        private int selectedSceduleId = 0;
        private int selectedInstrumentId = 0;
        private int selectedProblemId = 0;
        private int selectedEscalateReasonId = 0;
        private int selectedEscalatedByEnginerId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();


           
            if (rbtCurative.Checked == true)
            {
            
                grvCurativetiveSchedule.Visible = true;
                grvPreventiveSchedule.Visible = false;
                grvCurativetiveSchedule.DataSource = _ScheduleList;
                grvCurativetiveSchedule.DataBind();
               
            }
            else if (rbtPreventive.Checked == true)
            {
              
                grvCurativetiveSchedule.Visible = false;
                grvPreventiveSchedule.Visible = true;
                grvPreventiveSchedule.DataSource = _ScheduleList;
                grvPreventiveSchedule.DataBind();
                
            }
            BindInstrumentCategory();
        }
       
        private void BindInstrumentCategory()
        {
            if (!IsPostBack)
            {
                _instrumentCategoryList = _presenter.GetInstrumentCategoryList();
                ddlInstrumentCategory.DataSource = _instrumentCategoryList;
                ddlInstrumentCategory.DataBind();
            }
        }


        [CreateNew]
        public SchedulesListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
       
        protected void grvPreventiveSchedule_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            CoreDomain.Maintenance.Schedule schedule = e.Row.DataItem as CoreDomain.Maintenance.Schedule;
            HyperLink lnkEdit = e.Row.FindControl("lnkEdit") as HyperLink;
            Button btnStatus = e.Row.FindControl("btnStatus") as Button;
            if (schedule != null)
            {
                string url2 = String.Format("~/Maintenance/SchedulePreventiveMaintenance.aspx?{0}=2&InstrumentIdEdit={1}&ScheduleIdEdit={2}", AppConstants.TABID, schedule.InstrumentId, schedule.Id);
                lnkEdit.NavigateUrl = this.ResolveUrl(url2);
                lnkEdit.Enabled = false;

                if (schedule.PreventiveScheduleStatus == "Not Performed")
                {
                    lnkEdit.Enabled = true;
                }
                else
                {
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C"); // Already on Maintenance Process
                    lnkEdit.Enabled = false;
                }
                
            }
        }
        protected void grvCurativetiveSchedule_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            CoreDomain.Maintenance.Schedule schedule = e.Row.DataItem as CoreDomain.Maintenance.Schedule;
            HyperLink lnkEdit = e.Row.FindControl("lnkEdit") as HyperLink;
            LinkButton lnkEscalate = e.Row.FindControl("lnkEscalate") as LinkButton;
            HyperLink hplReschedule = e.Row.FindControl("lnkreschdule") as HyperLink;
            Button btnStatus = e.Row.FindControl("btnStatus") as Button;
            if (schedule != null)                       
            {
               
                if (e.Row.Cells[6].Text == "&nbsp;")
                    e.Row.Cells[6].Text = "-";
               
                    
                string editurl = "";
                string reschduleurl = ""; 
                lnkEdit.Visible = false;
                lnkEscalate.Visible = false;
                hplReschedule.Visible = false;
                if (schedule.CurativeScheduleStatus == "Not Performed")
                {
                    lnkEdit.Visible = true;
                    editurl =
                        String.Format(
                            "~/Maintenance/ScheduleCurativeMaintenance.aspx?{0}=2&InstrumentIdEdit={1}&ScheduleIdEdit={2}&ProblemIdEdit={3}",
                            AppConstants.TABID, schedule.InstrumentId, schedule.Id, schedule.ProblemId);

                }
                else if (schedule.CurativeScheduleStatus == "Pushed")
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffaaff"); // Pushed Jobs
                    //e.Row.ForeColor = System.Drawing.Color.FloralWhite;
                    //lnkEdit.ForeColor = System.Drawing.Color.FloralWhite;
                    //lnkEscalate.ForeColor = System.Drawing.Color.FloralWhite;
                    //hplReschedule.ForeColor = System.Drawing.Color.FloralWhite;
                    hplReschedule.Visible = true;
                    lnkEscalate.Visible = true;
                    reschduleurl =
                       String.Format(
                           "~/Maintenance/ScheduleCurativeMaintenance.aspx?{0}=2&InstrumentId={1}&ScheduleIdForPushed={2}&ProblemId={3}&Mode={4}",
                           AppConstants.TABID, schedule.InstrumentId, schedule.Id, schedule.ProblemId, 2);
                }
                else
                {
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C"); // Already on Maintenance Process
                    lnkEdit.Visible = false;
                }
                lnkEdit.NavigateUrl = this.ResolveUrl(editurl);
                hplReschedule.NavigateUrl = ResolveUrl(reschduleurl);
            }
        }
        #region schedule view

        public string _type
        {
            get 
            {
                if (rbtPreventive.Checked == true)
                    return "Preventive";
                else
                    return "Curative";
            }
        }

        public int SiteId
        {
            get
            {
                //if (_presenter.GetLocationId()[0] == 0 && _presenter.GetLocationId()[1] == 0)
                //    return Convert.ToInt32(ddlSite.SelectedValue);
                //else
                    return _presenter.GetLocationId()[1];
            }
        }
        public int UserId
        {
            get
            {
                
                return _presenter.GetLocationId()[2];
            }
        }
        public int _SiteId //parameter from edit schedule page after save, cancel clicked. to keep the selected instrument
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["SiteID"]) != 0)
                    return Convert.ToInt32(Request.QueryString["SiteID"]);
                else
                    return 0;
            }
        }
        public int _instCategoryId
        {
            get
            {
                { return Convert.ToInt32(ddlInstrumentCategory.SelectedValue); }
            }
        }

        public IList<Schedule> _scheduleList
        {
            set { _ScheduleList = value; }
        }


        #endregion



        protected void grvPreventiveSchedule_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvPreventiveSchedule.PageIndex = e.NewPageIndex;
            grvPreventiveSchedule.DataSource = _ScheduleList;
            grvPreventiveSchedule.DataBind();
        }
        protected void grvCurativetiveSchedule_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvCurativetiveSchedule.PageIndex = e.NewPageIndex;
            grvCurativetiveSchedule.DataSource = _ScheduleList;
            grvCurativetiveSchedule.DataBind();
        }
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            //_siteList = _presenter.GetSiteList(Convert.ToInt32(ddlRegion.SelectedValue));
            //if (ddlSite.Items.Count > 0)
            //{
            //    ddlSite.Items.Clear();
            //}
            //ListItem lst = new ListItem();
            //lst.Text = "Select Site";
            //lst.Value = "0";
            //ddlSite.Items.Add(lst);



            //ddlSite.DataSource = _siteList;
            //ddlSite.DataBind();
        }
        protected void grvCurativetiveSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Include Confirmation and Escalate Message
            pnlConfirmation.Visible = true;
           selectedProblemId = Convert.ToInt32(grvCurativetiveSchedule.SelectedDataKey["ProblemId"].ToString());
           selectedInstrumentId = Convert.ToInt32(grvCurativetiveSchedule.SelectedDataKey["InstrumentId"].ToString());
           selectedSceduleId = Convert.ToInt32(grvCurativetiveSchedule.SelectedDataKey["Id"].ToString());
           CoreDomain.Maintenance.EscalateReason escalateReason = new CoreDomain.Maintenance.EscalateReason();
           escalateReason = _presenter.GetEscalateReasonByProblemIdANDScheduleId(selectedProblemId, selectedSceduleId);
           selectedEscalateReasonId = escalateReason.Id;
           selectedEscalatedByEnginerId = escalateReason.EnginerId;

           CoreDomain.Maintenance.EscalatedJobs escalatedJobs = new CoreDomain.Maintenance.EscalatedJobs();
           EscalatedJobsDao escalatedJobsDao = new EscalatedJobsDao();

           escalatedJobs.InstrumentId = selectedInstrumentId;
           escalatedJobs.ProblemId = selectedProblemId;
           escalatedJobs.ScheduleId = selectedSceduleId;
           escalatedJobs.EnginerEscalateReasonId = selectedEscalateReasonId;
           Site _site = _presenter.GetRegionIdBySite(Convert.ToInt32(grvCurativetiveSchedule.SelectedDataKey["SiteId"].ToString()));
           escalatedJobs.EscalatedFromRegionId = _site.RegionId;
           escalatedJobs.Status = "New";
           escalatedJobsDao.Save(escalatedJobs); 
        }
}
}