﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class InstrumentListForDisposal : Microsoft.Practices.CompositeWeb.Web.UI.Page, IInstrumentListForDisposalView
    {
        private InstrumentListForDisposalPresenter _presenter;
        IList<Chai.Maintenance.CoreDomain.Configuration.Instrument> _instrumentList;
        IList<Site> _siteList;
        IList<Region> _regionlist;
        IList<InstrumentCategory> _instrumentTypeList;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();
            if (_presenter.GetLocationId()[0] == 1) //site user
            {
                lblRegion.Enabled = false;
                ddlRegion.Enabled = false;

                lblSite.Enabled = false;
                ddlSite.Enabled = false;
            }
            else if (_presenter.GetLocationId()[0] == 2) // region user
            {
                if (_presenter.GetLocationId()[1] == 1000) // federal user
                {
                    BindRegion();
                    BindSite(1000);
                    lblRegion.Enabled = true;
                    ddlRegion.Enabled = true;

                    lblSite.Enabled = true;
                    ddlSite.Enabled = true;
                }
                else //regional user, get list of site based on selected region
                {
                    BindSite(_presenter.GetLocationId()[1]); // passes the id 
                    lblRegion.Enabled = false;
                    ddlRegion.Enabled = false;

                    lblSite.Enabled = true;
                    ddlSite.Enabled = true;
                }

            }
            grvInstrumentList.DataSource = _instrumentList;
            grvInstrumentList.DataBind();
            BindInstrumentType();
        }
        private void BindSite(int regionId)
        {
            if (!IsPostBack)
            {
                _siteList = _presenter.GetSiteList(regionId);
                ddlSite.DataSource = _siteList;
                ddlSite.DataBind();
            }
        }
        private void BindRegion()
        {
            if (!IsPostBack)
            {
                _regionlist = _presenter.GetRegionList();
                ddlRegion.DataSource = _regionlist;
                ddlRegion.DataBind();
            }
        }

        private void BindInstrumentType()
        {
            if (!IsPostBack)
            {
                _instrumentTypeList = _presenter.GetInstrumentCategoryList();
                ddlInstrumentCategory.DataSource = _instrumentTypeList;
                ddlInstrumentCategory.DataBind();
            }
        }

        [CreateNew]
        public InstrumentListForDisposalPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            _siteList = _presenter.GetSiteList(Convert.ToInt32(ddlRegion.SelectedValue));
            if (ddlSite.Items.Count > 0)
            {
                ddlSite.Items.Clear();
            }
            ListItem lst = new ListItem();
            lst.Text = "Select Site";
            lst.Value = "0";
            ddlSite.Items.Add(lst);



            ddlSite.DataSource = _siteList;
            ddlSite.DataBind();
        }

        public IList<CoreDomain.Configuration.Instrument> instrumentlist
        {
            set { _instrumentList = value; }
        }

        public int RegionId
        {
            get
            {

                if (Convert.ToInt32(ddlRegion.SelectedValue) != 0)
                    return Convert.ToInt32(ddlRegion.SelectedValue);
                else
                {
                    if (_presenter.GetLocationId()[0] == 2)
                        return _presenter.GetLocationId()[1];
                    else
                        return 0;
                }

            }

        }
        public int SiteId
        {

            get
            {
                if (Convert.ToInt32(ddlSite.SelectedValue) != 0)
                    return Convert.ToInt32(ddlSite.SelectedValue);
                else
                {
                    if (_presenter.GetLocationId()[0] == 2)
                        return 0;
                    else
                        return _presenter.GetLocationId()[1];
                }

            }

        }
        public int InstrumentCatId
        {
            get { return Convert.ToInt32(ddlInstrumentCategory.SelectedValue); }
        }

        protected void grvInstrumentList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Instrument instrument = e.Row.DataItem as Instrument;
            if (instrument != null)
            {
                HyperLink hpl = e.Row.FindControl("hplDispose") as HyperLink;
                Button btnStatus = e.Row.FindControl("btnStatus") as Button;
                string url = String.Format("~/Maintenance/InstrumentDisposal.aspx?{0}=2&InstrumentId={1}", AppConstants.TABID, instrument.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
                if (instrument.Status == 2) //Not Functional
                {
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF2C00"); //Scheduled Not Yet Maintained 
                    btnStatus.ForeColor = System.Drawing.Color.White;
                    hpl.Enabled = false;
                }
                else if (instrument.Status == 3) // Transfered
                {
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C");
                    hpl.Enabled = false;
                }
                else if (instrument.Status == 4) //Disposed
                {
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF7251");
                    hpl.Enabled = false;
                }
            }
        }

       

        protected void grvInstrumentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvInstrumentList.PageIndex = e.NewPageIndex;
            grvInstrumentList.DataSource = _instrumentList;
            grvInstrumentList.DataBind();
        }
        protected void grvInstrumentList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
}
}