﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="InstrumentListForTransfer.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.InstrumentListForTransfer" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
 
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
      <div class="jarviswidget" id="Div1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Find Instrument To Transfer</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
                <section class="col col-6">  
       
                    <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
              <label class="select">
                    <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                              onselectedindexchanged="ddlRegion_SelectedIndexChanged" 
                        Enabled="False" CssClass="textbox">
                        <asp:ListItem Value="0">Select Region</asp:ListItem>
                    </asp:DropDownList><i></i></label></section>
                <section class="col col-6">  
                    <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                <label class="select">
                    <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                            DataTextField="Name" DataValueField="Id" 
                         AutoPostBack="True" Enabled="False" CssClass="textbox">
                        <asp:ListItem Value="0">Select Site</asp:ListItem>
                    </asp:DropDownList></label></section></div>
                <div class="row">
                <section class="col col-6">  
               
                    <asp:Label ID="lblInstrument" runat="server" Text="Instrument Category" 
                        CssClass="label"></asp:Label>
                <label class="select">
                    <asp:DropDownList ID="ddlInstrumentCategory" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             AutoPostBack="True" CssClass="textbox">
                        <asp:ListItem Value="0">Select Instrument Category</asp:ListItem>
                    </asp:DropDownList><i></i></label></section></div></fieldset><footer></footer>
                </div></div></div>
            <asp:GridView ID="grvInstrumentList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True"   CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    GridLines="Horizontal" 
                    onrowdatabound="grvInstrumentList_RowDataBound" 
                 AllowPaging="True" 
             onpageindexchanging="grvInstrumentList_PageIndexChanging">
                <Columns>
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument Name" />
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="SerialNo" HeaderText="Serial Number" />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="LotNumber" HeaderText="Lot Number" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplTransfer" runat="server">Transfer</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                        <asp:Button runat="server" ID="btnStatus" Text="" BorderStyle="None" />
                        </ItemTemplate>
                        </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:GridView>
             </div>
           
    <br />
    <br />
    
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Image ID="Image1" runat="server" 
                    ImageUrl="~/Images/InstrumentLegend.png" />
            </td>
        </tr>
    </table>
    
 </asp:Content>

