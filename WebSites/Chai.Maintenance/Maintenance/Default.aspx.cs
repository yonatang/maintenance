﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Modules.Configuration.Views;
using Microsoft.Practices.ObjectBuilder;

namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class MaintenanceDefault : Microsoft.Practices.CompositeWeb.Web.UI.Page, IDefaultView
    {
        private DefaultViewPresenter _presenter;
        private User _user;
        private static DateTime dateFrom1;
        private static DateTime dateTo1;

        private static DateTime dateFrom2;
        private static DateTime dateTo2;

        private static DateTime dateFrom3;
        private static DateTime dateTo3;

        private static DateTime dateFrom4;
        private static DateTime dateTo4;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();
            
            BindNotifiedProblemsChart();
            BindFrequentlyMaintainedInstrumentsChart();
            BindFrequentProblemNotificationOnInstrumentsByManufacturerChart();
            BindFrequentlyReportedProblemTypesChart();
        }
        void BindNotifiedProblemsChart()
        {

            if (dateFrom1 == Convert.ToDateTime(null))
                dateFrom1 = Convert.ToDateTime("1/1/1753");

            if (dateTo1 == Convert.ToDateTime(null))
                dateTo1 = DateTime.Now;

            chartNotifiedProblems.DataSource = _presenter.GetNotifiedProblemsChart(dateFrom1,
                                                dateTo1);
            chartNotifiedProblems.DataBind();

            chartNotifiedProblems.Series[0].YValueMembers = "NoOfNotifiedProblems";
            chartNotifiedProblems.Series[0].XValueMember = "RegionName";

          

        }
        void BindFrequentlyMaintainedInstrumentsChart()
        {
            if (dateFrom2 == Convert.ToDateTime(null))
                dateFrom2 = Convert.ToDateTime("1/1/1753");

            if (dateTo2 == Convert.ToDateTime(null))
                dateTo2 = DateTime.Now;

            chartFrequentlyMaintainedInstruments.DataSource = _presenter.GetFrequentlyMaintainedInstrumentsChart(dateFrom2,
                                                dateTo2);
            chartFrequentlyMaintainedInstruments.DataBind();

            chartFrequentlyMaintainedInstruments.Series[0].YValueMembers = "NoOfMaintenance";
            chartFrequentlyMaintainedInstruments.Series[0].XValueMember = "Instrument";

            
        } 

        void BindFrequentProblemNotificationOnInstrumentsByManufacturerChart()
        {
            if (dateFrom4 == Convert.ToDateTime(null))
                dateFrom4 = Convert.ToDateTime("1/1/1753");

            if (dateTo4 == Convert.ToDateTime(null))
                dateTo4 = DateTime.Now;
            chartFrequentProblemNotificationOnInstrumentsByManufacturer.DataSource = _presenter.GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(dateFrom4,
                                                dateTo4);
            chartFrequentProblemNotificationOnInstrumentsByManufacturer.DataBind();

            chartFrequentProblemNotificationOnInstrumentsByManufacturer.Series[0].YValueMembers = "NoOfNotifiedproblem";
            chartFrequentProblemNotificationOnInstrumentsByManufacturer.Series[0].XValueMember = "Manufacturers";
            

        }

        void BindFrequentlyReportedProblemTypesChart()
        {
            if (dateFrom3 == Convert.ToDateTime(null))
                dateFrom3 = Convert.ToDateTime("1/1/1753");

            if (dateTo3 == Convert.ToDateTime(null))
                dateTo3 = DateTime.Now;

            chartFrequentlyReportedProblemTypes.DataSource = _presenter.GetRFrequentlyReportedProblemTypesChart(dateFrom3,
                                                dateTo3);
            chartFrequentlyReportedProblemTypes.DataBind();

            chartFrequentlyReportedProblemTypes.Series[0].YValueMembers = "NoOfProblems";
            chartFrequentlyReportedProblemTypes.Series[0].XValueMember = "ProblemType";
        }
        [CreateNew]
        public DefaultViewPresenter Presenter
        {
            set
            {
                this._presenter = value;
                this._presenter.View = this;
            }
        }

        public new User User
        {
            set { _user = value; }
        }
        protected void lnkView1_Click(object sender, EventArgs e)
        {
            if (this.txtDatefrom1.Text != "" && this.txtDateTo1.Text != "")
            {
                dateFrom1 = Convert.ToDateTime(this.txtDatefrom1.Text);
                dateTo1 = Convert.ToDateTime(this.txtDateTo1.Text);

            }
            else
            {
                dateFrom1 = Convert.ToDateTime(null);
                dateTo1 = Convert.ToDateTime(null);
            }
            BindNotifiedProblemsChart();
        }
        protected void lnkView2_Click(object sender, EventArgs e)
        {
            if (this.txtDatefrom2.Text != "" && this.txtDateTo2.Text != "")
            {
                dateFrom2 = Convert.ToDateTime(this.txtDatefrom2.Text);
                dateTo2 = Convert.ToDateTime(this.txtDateTo2.Text);
              
            }
            else
            {
                dateFrom2 = Convert.ToDateTime(null);
                dateTo2 = Convert.ToDateTime(null);
            }
            BindFrequentlyMaintainedInstrumentsChart();
        }
        protected void  lnkView3_Click(object sender, EventArgs e)
        {
            if (this.txtDatefrom3.Text != "" && this.txtDateTo3.Text != "")
            {
                dateFrom3 = Convert.ToDateTime(this.txtDatefrom3.Text);
                dateTo3 = Convert.ToDateTime(this.txtDateTo3.Text);
               
            }
            else
            {
                dateFrom3 = Convert.ToDateTime(null);
                dateTo3 = Convert.ToDateTime(null);
            }
            BindFrequentlyReportedProblemTypesChart();
        }
        protected void lnkView4_Click(object sender, EventArgs e)
        {
            if (this.txtDatefrom4.Text != "" && this.txtDateTo4.Text != "")
            {
                dateFrom4 = Convert.ToDateTime(this.txtDatefrom4.Text);
                dateTo4 = Convert.ToDateTime(this.txtDateTo4.Text);   
            }
            else
            {
                dateFrom4 = Convert.ToDateTime(null);
                dateTo4 = Convert.ToDateTime(null);
            }
            BindFrequentProblemNotificationOnInstrumentsByManufacturerChart();
        }
}
}
