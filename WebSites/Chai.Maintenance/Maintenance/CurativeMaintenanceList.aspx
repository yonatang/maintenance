﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="CurativeMaintenanceList.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.CurativeMaintenanceList" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
  
        
     <div class="jarviswidget" id="Div1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Find Curative Maintenance</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
                <section class="col col-6">  
     
                    <asp:Label ID="lblInstrument" runat="server" Text="Instrument Category" 
                        CssClass="label"></asp:Label>
                <label class="select">
                    <asp:DropDownList ID="ddlInstrumentCategory" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             AutoPostBack="True" CssClass="textbox">
                        <asp:ListItem Value="0">Select Instrument Category</asp:ListItem>
                    </asp:DropDownList><i></i></label></section></div></fieldset></div></div></div>
              

 
    
       <asp:GridView ID="grvCurativeMaintenance" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="InstrumentId,ProblemId,Id" EnableModelValidation="True"   CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    GridLines="Horizontal" 
                
            onrowdatabound="grvCurativeMaintenance_RowDataBound" 
        AllowPaging="True" 
        onpageindexchanging="grvCurativeMaintenance_PageIndexChanging">
             
              
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="DescriptionOfEqptFailure"  HeaderText="Description Of Eqpt Failure" />
                    <asp:BoundField DataField="ProblemType" HeaderText="Problem Type" />
                    <asp:BoundField DataField="Date" HeaderText="Date" />
                    <asp:TemplateField>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit Curative Maintenance"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:GridView>
    </div>
</asp:Content>

