﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class MaintenanceConfirmation : Microsoft.Practices.CompositeWeb.Web.UI.Page, IMaintenanceConfirmationView
    {

        private MaintenanceConfirmationPresenter _presenter;
        Confirmation _Confirmation;
        private Instrument _instrument;
        private IList<Chai.Maintenance.CoreDomain.Maintenance.AssignedJobs> _AssignedJobs;
        string searchBy = "";
        static int ScheduleId = 0;
        static int InstrumentId = 0;
        static int ProblemId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();

            if (rbtPrvConfirmation.Checked == true)
            {
                searchBy = "PreventiveConfirmation";
                grvPrevMaintenances.Visible = true;
                grvCurativeMainenance.Visible = false;
                _AssignedJobs = _presenter.GetAssignedJobs(searchBy, Convert.ToInt32(_presenter.GetLocationId()[2]), Convert.ToInt32(_presenter.GetLocationId()[1]));
                grvPrevMaintenances.DataSource = _AssignedJobs;
                grvPrevMaintenances.DataBind();
                Image1.Visible = true;
                Image2.Visible = false;
            }
            else if (btCurative.Checked == true)
            {
                searchBy = "CurativeConfirmation";
                grvPrevMaintenances.Visible = false;
                grvCurativeMainenance.Visible = true;
                _AssignedJobs = _presenter.GetAssignedJobs(searchBy, Convert.ToInt32(_presenter.GetLocationId()[2]), Convert.ToInt32(_presenter.GetLocationId()[1]));
                grvCurativeMainenance.DataSource = _AssignedJobs;
                grvCurativeMainenance.DataBind();
                Image1.Visible = false;
                Image2.Visible = true;
            }

        }
        [CreateNew]
        public MaintenanceConfirmationPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void SaveConfirmation()
        {
            try
            {
                if (_Confirmation.IsNew())
                {
                    _presenter.SaveOrUpdateConfirmation(_Confirmation, searchBy, ScheduleId);
                   
                    Response.Redirect(String.Format("~/Maintenance/MaintenanceConfirmation.aspx?{0}=2", AppConstants.TABID));
                    Master.TransferMessage(new AppMessage("Maintenance Confirmed", RMessageType.Info));

                    //if (CheckBox1.Checked == true)
                    //{
                    //    if (searchBy == "Preventive")
                    //        _presenter.UpdateStatus("ConfirmationForSchedule", 1, ScheduleId, "Preventive"); //Changes the status of table schedule


                    //    _presenter.UpdateStatus("ConfirmationForInstrument", 1, InstrumentId, ""); //Changes the status of table Instrumnet
                    //}
                }
                else
                {
                    _presenter.SaveOrUpdateConfirmation(_Confirmation,"",0);
                    Response.Redirect("MaintenanceConfirmation.aspx");
                    Master.TransferMessage(new AppMessage("Maintenance Confirmed", RMessageType.Info));

                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void grvCurativeMainenance_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            CoreDomain.Maintenance.AssignedJobs assigned = e.Row.DataItem as CoreDomain.Maintenance.AssignedJobs;
     
            LinkButton lnk = e.Row.FindControl("LinkButton1") as LinkButton;
            Button btnStatus = e.Row.FindControl("btnStatus") as Button;
            if (assigned != null)
            {

                if (Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) != 2 && Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) != 5)
                {

                    if (Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) == 3)
                    {
                        btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF2C00"); //Scheduled Not Yet Maintained 
                        btnStatus.ForeColor = System.Drawing.Color.White;
                        lnk.Enabled = false;
                     

                    }
                    else if (Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) == 6)
                    {
                        btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF7251"); // Maintained But not resolved
                        lnk.Enabled = false;
                       
                    }
                }
                else if (Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) == 2 || Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) == 5)
                {
                    if (Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) == 2)
                    {
                        btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C"); //Maintained and Resolved -  But Not Yet Approved By User
                        lnk.Enabled = true;
                    }
                    else if (Convert.ToInt32(_presenter.GetProlemById(assigned.ProblemId).Status) == 5)
                    {
                        btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#99CC00"); // Maintained - User Approved and Resolved 
                        lnk.Enabled = false;
                    }
                }
            }
        }
        protected void grvCurativeMainenance_SelectedIndexChanged(object sender, EventArgs e)
        {
            InstrumentId = Convert.ToInt32(grvCurativeMainenance.SelectedDataKey["InstrumentId"]);
            ScheduleId = Convert.ToInt32(grvCurativeMainenance.SelectedDataKey["ScheduleID"]);
            ProblemId = Convert.ToInt32(grvCurativeMainenance.SelectedDataKey["ProblemId"]);
            if (InstrumentId != 0)
            {
                _instrument = _presenter.GetInstrumentById(InstrumentId);
                lblSelectedinstrument.Text = _instrument.InstrumentName;
                lblInstallationDate.Text = _instrument.InstallationDate.ToString();
                lblMainType.Text = "Curative";
                lblregionName.Text = _instrument.RegionName;

            }
            pnlConfirmation.Visible = true;
        }
        protected void grvPrevMaintenances_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            CoreDomain.Maintenance.AssignedJobs assigned = e.Row.DataItem as CoreDomain.Maintenance.AssignedJobs;

            if (assigned != null)
            {
                LinkButton lnk = e.Row.FindControl("LinkButton1") as LinkButton;
                Button btnStatus = e.Row.FindControl("btnStatus") as Button;
                if (assigned.PreventiveScheduleStatus == "Needs Approval")
                {
                    lnk.Enabled = true;
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C"); //Maintained and Resolved -  But Not Yet Approved By User
                }
                else if (assigned.PreventiveScheduleStatus == "Performed")
                {

                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#99CC00"); // Maintained - User Approved and Resolved 
                    lnk.Enabled = false;
                }
                else if (assigned.PreventiveScheduleStatus == "Not Performed")
                {
                    btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FF7251"); // Maintained But not resolved
                    lnk.Enabled = false;
                }
            }
        }
        protected void grvPrevMaintenances_SelectedIndexChanged(object sender, EventArgs e)
        {
            InstrumentId = Convert.ToInt32(grvPrevMaintenances.SelectedDataKey["InstrumentId"]);
            ScheduleId = Convert.ToInt32(grvPrevMaintenances.SelectedDataKey["ScheduleID"]);
            ProblemId = Convert.ToInt32(grvPrevMaintenances.SelectedDataKey["ProblemId"]);
            pnlConfirmation.Visible = true;
            if (InstrumentId != 0)
            {
                _instrument = _presenter.GetInstrumentById(InstrumentId);
                lblSelectedinstrument.Text = _instrument.InstrumentName;
                lblInstallationDate.Text = _instrument.InstallationDate.ToString();
                lblMainType.Text = "Preventive";
                lblregionName.Text = _instrument.RegionName;
              
            }
          
        }
        protected void lnkConfirm0_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _Confirmation.InstrumentId = InstrumentId;
                _Confirmation.ProblemId = ProblemId;
                _Confirmation.UserComments = this.txtcomments.Text;
                _Confirmation.IsResolved = CheckBox1.Checked;
                _Confirmation.Date = Convert.ToDateTime(txtDate.Text.ToString());

                SaveConfirmation();
            }
        }
        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/Maintenance/MaintenanceConfirmation.aspx?{0}=2", AppConstants.TABID));
        }

        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["Id"]) != 0)
                    return Convert.ToInt32(Request.QueryString["Id"]);
                else
                    return 0;
            }
        }

        Confirmation IMaintenanceConfirmationView._Confirmation
        {
            get
            {
                return _Confirmation;
            }
            set
            {
                _Confirmation = value;
            }
        }

        int IMaintenanceConfirmationView.InstrumentId
        {
            get { return InstrumentId; }
        }

        int IMaintenanceConfirmationView.ProblemId
        {
            get { return ProblemId; }
        }
        protected void rbtPrvConfirmation_CheckedChanged(object sender, EventArgs e)
        {

        }
        protected void btCurative_CheckedChanged(object sender, EventArgs e)
        {

        }
        protected void grvCurativeMainenance_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvCurativeMainenance.PageIndex = e.NewPageIndex;
            grvCurativeMainenance.DataSource = _AssignedJobs;
            grvCurativeMainenance.DataBind();
        }
        protected void grvPrevMaintenances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvPrevMaintenances.PageIndex = e.NewPageIndex;
            grvPrevMaintenances.DataSource = _AssignedJobs;
            grvPrevMaintenances.DataBind();
        }
}
}