﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="SchedulesList.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.SchedulesList" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
     <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Find Schedule</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>		<legend></legend>			
								<div class="row">
									<section class="col col-6">   
                                    <label class="radio">
                <asp:RadioButton ID="rbtPreventive" runat="server" AutoPostBack="True" 
                            GroupName="1" Text=""/><i></i>Show Preventive Schedules</label></section>
                <section class="col col-6">   
                                    <label class="radio">
                <asp:RadioButton ID="rbtCurative" runat="server" AutoPostBack="True" 
                            GroupName="1" Text="" /><i></i>Show Curative Schedules</label></section></div>
         <div class="row">
									<section class="col col-6">   
                        <asp:Label ID="lblInstrument" runat="server" Text="Instrument Category" 
                            CssClass="label"></asp:Label>
                       <label class="select">
                        <asp:DropDownList ID="ddlInstrumentCategory" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             AutoPostBack="True" CssClass="textbox">
                            <asp:ListItem Value="0">Select Instrument Category</asp:ListItem>
                        </asp:DropDownList><i></i></label></section></div></fieldset>
                   
                   </div></div></div></div>



            <asp:GridView ID="grvPreventiveSchedule" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True"  CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                   GridLines="Horizontal" 
                
            onrowdatabound="grvPreventiveSchedule_RowDataBound" Visible="False" 
        AllowPaging="True" 
        onpageindexchanging="grvPreventiveSchedule_PageIndexChanging">
               
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="Type" HeaderText="Type" />
                    <asp:BoundField DataField="ScheduledDateFrom" 
                        HeaderText="Scheduled Date From" />
                    <asp:BoundField DataField="ScheduledDateTo" HeaderText="Scheduled Date To" />
                    <asp:TemplateField>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit Schedule"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField >
                    <ItemTemplate>
                        <asp:Button runat="server" ID="btnStatus" Text="" BorderStyle="None" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle  ForeColor="White" HorizontalAlign="Center" />
               <%-- <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
           <br />
            <asp:GridView ID="grvCurativetiveSchedule" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" 
        DataKeyNames="Id,InstrumentId,ProblemId,SiteId" EnableModelValidation="True"  CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    GridLines="Horizontal" 
               
            onrowdatabound="grvCurativetiveSchedule_RowDataBound" Visible="False" 
        AllowPaging="True" 
        onpageindexchanging="grvCurativetiveSchedule_PageIndexChanging" 
        onselectedindexchanged="grvCurativetiveSchedule_SelectedIndexChanged">
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="ScheduledDateFrom" 
                        HeaderText="Scheduled Date From" />
                    <asp:BoundField DataField="ScheduledDateTo" HeaderText="Scheduled Date To" />
                    <asp:BoundField DataField="FullName" HeaderText="Enginer" />
                    <asp:BoundField DataField="PushedReason" HeaderText="Pushed Reason" />
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit Schedule"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEscalate" runat="server" CausesValidation="False" 
                                CommandName="Select" Text="Escalate"></asp:LinkButton>
                            <cc1:ModalPopupExtender ID="lnkEscalate_ModalPopupExtender" runat="server" 
                                CancelControlID="btnNo" DynamicServicePath="" Enabled="True" 
                                OkControlID="btnYes" PopupControlID="pnlConfirmation" 
                                TargetControlID="lnkEscalate">
                            </cc1:ModalPopupExtender>
                            <cc1:ConfirmButtonExtender ID="lnkEscalate_ConfirmButtonExtender" 
                                runat="server" 
                                ConfirmText="Are you sure , You want to escalate?" 
                                DisplayModalPopupID="lnkEscalate_ModalPopupExtender" Enabled="True" 
                                TargetControlID="lnkEscalate">
                            </cc1:ConfirmButtonExtender>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkreschdule" runat="server" Text="Re-Schedule"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                     <ItemTemplate>
                        <asp:Button runat="server" ID="btnStatus" Text="" BorderStyle="None" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            
            </asp:GridView>
           <br />
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/pushedlegend.jpg" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlConfirmation" runat="server" Visible="false">
   
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"> <asp:Label ID="configmMessage" runat="server" 
                          Text="Are you sure , You want to escalate" 
                          meta:resourceKey="configmMessageResource1" ></asp:Label>
                     					
				</h4>
			</div>
    <div class="modal-body no-padding">
    <div class="smart-form">
    <footer>
                     
                 
                            <asp:Button ID="btnYes" runat="server" Text="Yes"  Width="60px" class="btn btn-primary"
                                meta:resourceKey="btnYesResource1"/>
                            <asp:Button ID="btnNo" runat="server" Text="No" Width="60px" class="btn btn-primary"
                                meta:resourceKey="btnNoResource1"/></footer></div></div>
                     
          </div>
          </div>  
               </asp:Panel>
           
    
           </asp:Content>