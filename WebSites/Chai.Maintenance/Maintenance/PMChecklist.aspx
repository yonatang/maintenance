﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PMChecklist.aspx.cs" Inherits="Maintenance_PMChecklist" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        
a
{
	color: #3333CC;
            font-size: xx-small;
            font-family: Arial, Helvetica, sans-serif;
        }

input, select, textarea
{
	font-size: x-small;
	margin-left: 0px;
	margin-right: 1px;
	padding: 1px;
    }

        .style5
        {
            width: 568px;
            text-align: left;
            font-size: xx-small;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="style1">
            <tr>
                <td class="style5">
                <asp:DataGrid ID="dgChecklist" runat="server" AutoGenerateColumns="False" 
                    BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                    DataKeyField="Id" ShowFooter="True" Width="117%" 
                        BackColor="White" 
                        
                        
                        
                        style="font-size: xx-small; font-family: Arial, Helvetica, sans-serif; margin-right: 1px;">
                    <Columns>
                    <asp:TemplateColumn HeaderText="Id">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text=' <%# DataBinder.Eval(Container.DataItem, "Id")%>'></asp:Label>
                               
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Task">
                            <ItemTemplate>
                                
                                <%# DataBinder.Eval(Container.DataItem, "Name")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                     
                    
                        <asp:TemplateColumn HeaderText="Complete">
                            <ItemTemplate>
                               <%-- <%# DataBinder.Eval(Container.DataItem, "IsCompleted")%>--%>
                                <asp:CheckBox ID="CheckBox1" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                         <asp:TemplateColumn HeaderText="Remark">
                            <ItemTemplate>
                               <%-- <%# DataBinder.Eval(Container.DataItem, "IsCompleted")%>--%>
                                 <asp:TextBox ID="txtremark" runat="server" Height="31px" TextMode="MultiLine" 
                                    Width="247px"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    
                       
                    </Columns>
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <ItemStyle ForeColor="#000066" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" 
                        Mode="NumericPages" />
                    <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td class="style5">
                    <asp:Label ID="lblChecklist" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:LinkButton ID="lnkSave" runat="server" CommandName="Save" 
                        onclick="lnkSave_Click">Save</asp:LinkButton>
&nbsp;<asp:LinkButton ID="lnkClose" runat="server" onclick="lnkClose_Click">Close</asp:LinkButton>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>



