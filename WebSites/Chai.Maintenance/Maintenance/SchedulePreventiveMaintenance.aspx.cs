﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class SchedulePreventiveMaintenance : Microsoft.Practices.CompositeWeb.Web.UI.Page, ISchedulePreventiveMaintenanceView
    {
        private SchedulePreventiveMaintenancePresenter _presenter;
       
        private Instrument _instrument;
        private Schedule _schedule;
        private IList<Schedule> scheduleList;
        static IList<DateTime> busyDays = null;
        private IList<User> _userList;
        private IList<User> _externalUserList;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();

            if (!Page.IsPostBack)
                BindControls();

            if (InstrumentId != 0 || InstrumentIdEdit !=0)
            {
                if (InstrumentIdEdit != 0)
                {
                    lblEditing.Visible = true;
                    _instrument = _presenter.GetInstrumentByIdForProblem(InstrumentIdEdit);
                }
                else
                {
                    lblEditing.Visible = false;
                    _instrument = _presenter.GetInstrumentByIdForProblem(InstrumentId);
                }

                lblSelectedinstrument.Text = _instrument.InstrumentName;
                lblInstallationDate.Text = _instrument.InstallationDate.ToString();
                lblregionName.Text = _instrument.RegionName;
                lblSiteName.Text = _instrument.SiteName;
                lblLastPreventivemain.Text = _instrument.LastPreventiveMaintenanceDate.ToString();
                if (_instrument.UnderServiceContract)
                {
                    lblContract.Visible = _instrument.UnderServiceContract;
                    lblcontractedWith.Visible = _instrument.UnderServiceContract;
                    lblWarantiyDate.Visible = _instrument.UnderServiceContract;
                    lblexpireDate.Visible = _instrument.UnderServiceContract;
                    lblcontractedWith.Text = _instrument.ContractedWith;
                    lblWarantiyDate.Text = _instrument.WarranityExpireDate.ToString();
                }
            }

            BindEnginers();
        }

        [CreateNew]
        public SchedulePreventiveMaintenancePresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindEnginers()
        {
            if (!IsPostBack)
            {
                _userList = _presenter.GetEnginersList();
                ddlEnginer.DataSource = _userList;
                ddlEnginer.DataBind();
            }
        }
        private void BindExternalEnginers()
        {
            //if (!IsPostBack)
            //{
            _externalUserList = _presenter.GetExternalUserList();
            ddlExternalEnginer.DataSource = _externalUserList;
            ddlExternalEnginer.DataBind();
            //}
        }

        private void BindControls()
        {
           
                this.ddlEnginer.SelectedValue = _schedule.EnginerId.ToString();
                this.CalendarFrom.Text = _schedule.ScheduledDateFrom.ToString();
                this.CalendarTo.Text = _schedule.ScheduledDateTo.ToString();
                this.txtdesc.Text = _schedule.Description;
                this.txtDate.Text = _schedule.Date.ToString();
                this.btnDelete.Visible = (_schedule.Id > 0);
                this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");

        }

        private void SaveSchedule()
        {
            try
            {
                if (_schedule.IsNew())
                {
                    _presenter.SaveOrUpdateSchedule(_schedule);
                    Master.TransferMessage(new AppMessage("Preventive Maintenance Scheduled", RMessageType.Info));
                    _presenter.CancelPage(_instrument.Id);
                }
                else
                {
                    _presenter.SaveOrUpdateSchedule(_schedule);
                    Master.ShowMessage(new AppMessage("Preventive Maintenance Scheduled", RMessageType.Info));

                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlExternalEnginer.SelectedValue) == 0 & Convert.ToInt32(ddlEnginer.SelectedValue) == 0)
            {
                errormsg.Visible = true;
                errormsg.Text = "Please select an engineer";
            }
            else
            {
                if (this.IsValid)
                {
                    if (Convert.ToInt32(ddlEnginer.SelectedValue) != 0)
                        _schedule.EnginerId = Convert.ToInt32(ddlEnginer.SelectedValue);
                    else
                        _schedule.EnginerId = Convert.ToInt32(ddlExternalEnginer.SelectedValue);
                    _schedule.ScheduledDateFrom = Convert.ToDateTime(CalendarFrom.Text.ToString());
                    _schedule.ScheduledDateTo = Convert.ToDateTime(CalendarTo.Text.ToString());
                    _schedule.Description = txtdesc.Text;
                    _schedule.Date = Convert.ToDateTime(txtDate.Text);
                    _schedule.IsExternal = chkAssign.Checked;
                    _schedule.ProblemId = 0;
                    _schedule.SiteId = _instrument.SiteId;
                    if (InstrumentIdEdit == 0)
                        _schedule.InstrumentId = InstrumentId;
                    else
                        _schedule.InstrumentId = InstrumentIdEdit;
                    _schedule.UserId = _presenter.GetLocationId()[2];
                    _schedule.PreventiveScheduleStatus = "Not Performed";
                    SaveSchedule();
                }
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            string url = String.Format("~/Maintenance/SchedulePreventiveMaintenanceList.aspx?{0}=2&InstrumentId={1}", AppConstants.TABID, InstrumentId);
            Response.Redirect(url);

        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteSchedule(_schedule.Id);
                Master.ShowMessage(new AppMessage("Schedule was deleted successfully", Chai.Maintenance.Enums.RMessageType.Info));

            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Delete Failed. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {

            string url = String.Format("~/Maintenance/SchedulePreventiveMaintenanceList.aspx?{0}=2", AppConstants.TABID);
            Response.Redirect(url);
        }

        public int InstrumentId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentId"]);
                else
                    return 0;
            }
        }

        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ScheduleId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ScheduleId"]);
                else
                    return 0;
                
            }
        }
       
        public int InstrumentIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]);
                else
                    return 0;
            }
        }

        public int ScheduleIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ScheduleIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ScheduleIdEdit"]);
                else
                    return 0;
            }
        }
        Schedule ISchedulePreventiveMaintenanceView._Schedule
        {
            get { return _schedule; }
            set { _schedule = value; }
        }



        protected void chkAssign_CheckedChanged(object sender, EventArgs e)
        {
          
            if (chkAssign.Checked)
            {
                BindExternalEnginers();
                lblexternalegn.Visible = true;
                ddlExternalEnginer.Visible = true;
                inlinenginer.Visible = true;
                lblenginer.Visible = false;
                ddlEnginer.Visible = false;
                busyDays = null;
                Calendar2.Visible = false;
                Image1.Visible = false;
            }
            else
            {

                lblexternalegn.Visible = false;
                ddlExternalEnginer.Visible = false;
                inlinenginer.Visible = false;
                lblenginer.Visible = true;
                ddlEnginer.Visible = true;
            }
        }

        protected void Calendar2_DayRender(object sender, DayRenderEventArgs e)
        {
            e.Day.IsSelectable = false;
            if (busyDays != null)
            {
                if (busyDays.Contains(e.Day.Date))
                {
                    e.Cell.BackColor = System.Drawing.ColorTranslator.FromHtml("#ffaaff");
                }
            }
        }
        protected void ddlEnginer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlEnginer.SelectedValue) != 0)
            {
                scheduleList = _presenter.GetSchedulesByEnginerId(Convert.ToInt32(ddlEnginer.SelectedValue));
                busyDays = new List<DateTime>();
                foreach (Schedule items in scheduleList)
                {
                    DateTime scheduledDates = items.ScheduledDateFrom;
                    while (items.ScheduledDateTo >= scheduledDates)
                    {

                        busyDays.Add(new DateTime(scheduledDates.Year, scheduledDates.Month, scheduledDates.Day));
                        scheduledDates = scheduledDates.AddDays(1);
                    }
                }

                Calendar2.Visible = true;
                Image1.Visible = true;

            }
            else
            {
                busyDays = null;
                Calendar2.Visible = false;
                Image1.Visible = false;
            }
        }
}
}