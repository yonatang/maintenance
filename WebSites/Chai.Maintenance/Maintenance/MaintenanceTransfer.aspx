﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="MaintenanceTransfer.aspx.cs" Inherits="Chai.Maintenance.Modules.Maintenance.Views.MaintenanceTransfer" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %><%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
 
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
 <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblEditing" runat="server" 
                    style="color: #FF3300; background-color: #FFFFFF" Text="Editing..." 
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" class="alert alert-danger fade in"
        ValidationGroup="1" />
         <div class="alert alert-info fade in">
       <i class="fa-fw fa fa-info"></i>
         <strong>Instrument Info!/ </strong>
    
      
       
                    <asp:Label ID="lblInstrument" runat="server" Text="Selected Instrument" 
                        style="font-weight: 700" ></asp:Label>
               
                    <asp:Label ID="lblSelectedinstrument" runat="server" 
                        ></asp:Label>
                
                    <asp:Label ID="lblRegion111" runat="server" Text="Region" style="font-weight: 700" ></asp:Label>
               
                    <asp:Label ID="lblregionName" runat="server"></asp:Label>
                    <asp:Label ID="lblSite111" runat="server" Text="Site" style="font-weight: 700" ></asp:Label>
                
                    <asp:Label ID="lblSiteName" runat="server" ></asp:Label>
                
                    <asp:Label ID="Label1" runat="server" Text="Installation Date" style="font-weight: 700" ></asp:Label>
              
                    <asp:Label ID="lblInstallationDate" runat="server" ></asp:Label>
              
                    
              
                    <asp:Label ID="Label14" runat="server" Text="Manufacturer" style="font-weight: 700" ></asp:Label>
                
                    <asp:Label ID="lblManufacturer" runat="server" ></asp:Label>
    
     </div>

        <div class="jarviswidget" id="Div1" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Instrument Transfer</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
                <section class="col col-6">  
         
    
   
                <asp:Label ID="Label13" runat="server" Text="Region" CssClass="label"></asp:Label>
            <label class="select">
                <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                    AutoPostBack="True"  DataTextField="RegionName" 
                    DataValueField="Id" 
                    onselectedindexchanged="ddlRegion_SelectedIndexChanged" CssClass="textbox">
                    <asp:ListItem Value="0">Select Region</asp:ListItem>
                </asp:DropDownList><i></i>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                    ControlToValidate="ddlRegion" Display="Dynamic" ErrorMessage="Region Required" 
                    InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator></label></section></div>
           <div class="row">
                <section class="col col-6">  
         
                <asp:Label ID="Label7" runat="server" Text="Select New Site" CssClass="label"></asp:Label>
           <label class="select">
                <asp:DropDownList ID="ddlNewSite" runat="server" AppendDataBoundItems="True" 
                    AutoPostBack="True"   DataTextField="Name" DataValueField="Id" 
                    onselectedindexchanged="ddlNewSite_SelectedIndexChanged" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select New Site</asp:ListItem>
                </asp:DropDownList><i></i>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="ddlNewSite" Display="Dynamic" 
                    ErrorMessage="New Site Required" ValidationGroup="1" InitialValue="0">*</asp:RequiredFieldValidator></label></section></div>
           <div class="row">
                <section class="col col-6">  
                <asp:Label ID="Label12" runat="server" Text="Select New User" CssClass="label"></asp:Label>
           <label class="select">
                <asp:DropDownList ID="ddlNewUser" runat="server" AppendDataBoundItems="True" 
                      DataTextField="FullName" DataValueField="Id" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select New User</asp:ListItem>
                </asp:DropDownList><i></i>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="ddlNewUser" Display="Dynamic" 
                    ErrorMessage="New User Required" ValidationGroup="1" InitialValue="0">*</asp:RequiredFieldValidator></label></section></div>
        <div class="row">
                <section class="col col-6">  
                <asp:Label ID="Label11" runat="server" Text="Transfered By" CssClass="label"></asp:Label>
           <label class="input">
                <asp:Label ID="lblTransferedById" runat="server" 
                    ></asp:Label></label></section></div>
           <div class="row">
                <section class="col col-6">  
                <asp:Label ID="Label8" runat="server" Text="Transfer Reason" CssClass="label"></asp:Label>
         <label class="input">
                <asp:TextBox ID="txtreason" runat="server"   
                      CssClass="textboxDescShort"></asp:TextBox></label></section></div>
        <div class="row">
                <section class="col col-6">  
                <asp:Label ID="Label9" runat="server" Text="Date" CssClass="label"></asp:Label>
          <label class="input">
           <i class="icon-append fa fa-calendar"></i>
                 <asp:TextBox ID="txtDate" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox>
               
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtDate" Display="Dynamic" ErrorMessage="Date Required" 
                    ValidationGroup="1">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator22" 
                    runat="server" ControlToValidate="txtDate" ErrorMessage="Date is not valid" 
                    ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                    ValidationGroup="1">*</asp:RegularExpressionValidator></label></section></div></fieldset>
                    <footer>
          
    
    
                <asp:Button ID="btnNew" runat="server" Text="New" onclick="btnNew_Click" Cssclass="btn btn-primary"/>

                <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="1" Cssclass="btn btn-primary"
                    onclick="btnSave_Click" />

                <asp:Button ID="btnDelete" runat="server" Text="Delete" Cssclass="btn btn-primary"
                    onclick="btnDelete_Click" />

                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Cssclass="btn btn-primary"
                    onclick="btnCancel_Click" /> </footer></div></div></div></div>
    
</asp:Content>
