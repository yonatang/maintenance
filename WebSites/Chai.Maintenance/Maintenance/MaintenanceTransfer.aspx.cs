﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class MaintenanceTransfer : Microsoft.Practices.CompositeWeb.Web.UI.Page, IMaintenanceTransferView
    {
        private MaintenanceTransferPresenter _presenter;

        private Transfer _Transfer;
        private Instrument _instrument;
        private IList<Region> _regionlist;
        private IList<Site> _siteList;
        private IList<User> _userList;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();

            if (!Page.IsPostBack)
                BindControls();
            if (TransferedIdEdit == 0)
                this.lblTransferedById.Text = _presenter.GetUserList(Convert.ToInt32(_presenter.GetLocationId()[2])).FullName;
            if (InstrumentId != 0 || InstrumentIdEdit != 0)
            {
                if (InstrumentIdEdit == 0)
                    _instrument = _presenter.GetInstrumentById(InstrumentId);
                else
                    _instrument = _presenter.GetInstrumentById(InstrumentIdEdit);
                lblSelectedinstrument.Text = _instrument.InstrumentName;
                lblInstallationDate.Text = _instrument.InstallationDate.ToString();
                lblManufacturer.Text = _instrument.ManufacturerName;
                lblregionName.Text = _instrument.RegionName;
                lblSiteName.Text = _instrument.SiteName;
            }
          BindRegion();
        }
        [CreateNew]
        public MaintenanceTransferPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindRegion()
        {
            if (!IsPostBack)
            {
                _regionlist = _presenter.GetRegionList();
                ddlRegion.DataSource = _regionlist;
                ddlRegion.DataBind();
            }
        }

        private void BindControls()
        {
         
                if (TransferedIdEdit != 0)
                    this.lblTransferedById.Text = _presenter.GetUserList(_Transfer.TransferedById).FullName;
                else
                    this.lblTransferedById.Text = _presenter.GetUserList(Convert.ToInt32(_presenter.GetLocationId()[2])).FullName;
                this.ddlNewSite.SelectedValue = Convert.ToString(_Transfer.TransferToSiteId);
                this.ddlNewUser.SelectedValue = _Transfer.NewUserId.ToString();

                this.txtreason.Text = _Transfer.Reason;
                this.txtDate.Text = _Transfer.Date.ToString();
                this.btnDelete.Visible = (_Transfer.Id > 0);
                this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");
        }
        private void SaveTransfer()
        {
            try
            {
                if (_Transfer.IsNew())
                {
                    _presenter.SaveOrUpdateTransfer(_Transfer);
                    //_presenter.UpdateStatus("Transfer", 3, InstrumentId,"");
                    _presenter.SaveTransferedInstrument(_instrument, InstrumentId, Convert.ToInt32(this.ddlNewSite.SelectedValue));
                    Master.ShowMessage(new AppMessage("Instrument Transfered", RMessageType.Info));
                }
                else
                {
                    _presenter.SaveOrUpdateTransfer(_Transfer);
                    Master.ShowMessage(new AppMessage("Instrument Transfer Updated", RMessageType.Info));

                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _Transfer.TransferToSiteId =  Convert.ToInt32(this.ddlNewSite.SelectedValue);
                _Transfer.NewUserId = Convert.ToInt32(this.ddlNewUser.SelectedValue);
                if (TransferedIdEdit == 0) // Edit mode
                {
                    _Transfer.TransferedById = Convert.ToInt32(_presenter.GetLocationId()[2]); // current user id
                    _Transfer.CurrentSiteId = _instrument.SiteId;
                }
                _Transfer.Reason = this.txtreason.Text;
                _Transfer.InstrumentId = InstrumentId;
                
                _Transfer.Date = Convert.ToDateTime(txtDate.Text);

                SaveTransfer();
            }
            
        }
        protected void btnNew_Click(object sender, EventArgs e)
        {

            string url = String.Format("~/Maintenance/InstrumentListForTransfer.aspx?{0}=2", AppConstants.TABID);

            Response.Redirect(url);

        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteTransfer(_Transfer.Id);
                Master.ShowMessage(new AppMessage("Transfer was deleted successfully", Chai.Maintenance.Enums.RMessageType.Info));

            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete Transfer. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["TransferId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["TransferId"]);
                else
                    return 0;
            }
        }

        public Transfer transfer
        {
            get { return _Transfer; }
            set { _Transfer = value; }
        }

        public int InstrumentId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentId"]);
                else
                    return 0;
            }
        }
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            _siteList = _presenter.GetSiteList(Convert.ToInt32(ddlRegion.SelectedValue));
            if (ddlNewSite.Items.Count > 0)
            {
                ddlNewSite.Items.Clear();
            }
            ListItem lst = new ListItem();
            lst.Text = "Select New Site";
            lst.Value = "0";
            ddlNewSite.Items.Add(lst);

            ddlNewSite.DataSource = _siteList;
            ddlNewSite.DataBind();
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/Maintenance/InstrumentListForTransfer.aspx?{0}=2", AppConstants.TABID));
        }
        protected void ddlNewSite_SelectedIndexChanged(object sender, EventArgs e)
        {
            _userList = _presenter.GetUserBySiteID(Convert.ToInt32(ddlNewSite.SelectedValue));
            if (ddlNewUser.Items.Count > 0)
            {
                ddlNewUser.Items.Clear();
            }
            ListItem lst = new ListItem();
            lst.Text = "Select New User";
            lst.Value = "0";
            ddlNewUser.Items.Add(lst);

            ddlNewUser.DataSource = _userList;
            ddlNewUser.DataBind();
        }


        public int InstrumentIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentIdEdit"]);
                else
                    return 0;
            }
        }

        public int TransferedIdEdit
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["TransferedIdEdit"]) != 0)
                    return Convert.ToInt32(Request.QueryString["TransferedIdEdit"]);
                else
                    return 0;
            }
        }

        public int CurrentSiteId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["CurrentSiteId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["CurrentSiteId"]);
                else
                    return 0;
            }
        }
    }
}