﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class ScheduleCurativeMaintenanceList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IScheduleCurativeMaintenanceListView
    {
        private ScheduleCurativeMaintenanceListPresenter _presenter;
        IList<Site> _siteList;
        IList<InstrumentCategory> _instrumentCategoryList;
        IList<Region> _regionList;
        IList<ProblemType> _problemType;
        IList<Problem> _problemList;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();
            if (_presenter.GetLocationId()[0] == 1) //site user
            {
                lblRegion.Enabled = false;
                ddlRegion.Enabled = false;

                lblSite.Enabled = false;
                ddlSite.Enabled = false;
            }
            else if (_presenter.GetLocationId()[0] == 2) // region user
            {
                if (_presenter.GetLocationId()[1] == 1000) // federal user
                {
                    BindRegion();
                    BindSite(1000);
                    lblRegion.Enabled = true;
                    ddlRegion.Enabled = true;

                    lblSite.Enabled = true;
                    ddlSite.Enabled = true;
                }
                else //regional user, get list of site based on selected region
                {
                    BindSite(_presenter.GetLocationId()[1]); // passes the id 
                    lblRegion.Enabled = false;
                    ddlRegion.Enabled = false;

                    lblSite.Enabled = true;
                    ddlSite.Enabled = true;
                }

            }
            grvProblemWithInstrumentList.DataSource = _problemList;
            grvProblemWithInstrumentList.DataBind();


            BindInstrumentCategory();
           
            BindProblemType();
        }
        private void BindRegion()
        {
            if (!IsPostBack)
            {
                _regionList = _presenter.GetRegionList();
                ddlRegion.DataSource = _regionList;
                ddlRegion.DataBind();
            }
        }
        private void BindSite(int regionId)
        {
            if (!IsPostBack)
            {
                _siteList = _presenter.GetSiteList(regionId);
                ddlSite.DataSource = _siteList;
                ddlSite.DataBind();
            }
        }
        private void BindInstrumentCategory()
        {
            if (!IsPostBack)
            {
                _instrumentCategoryList = _presenter.GetInstrumentCategoryList();
                ddlInstrumentCategory.DataSource = _instrumentCategoryList;
                ddlInstrumentCategory.DataBind();
            }
        }

        private void BindProblemType()
        {
            if (!IsPostBack)
            {
                _problemType = _presenter.GetProblemTypeList();
                ddlproblemType.DataSource = _problemType;
                ddlproblemType.DataBind();
            }
        }
        [CreateNew]
        public ScheduleCurativeMaintenanceListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }



      

        protected void grvProblemWithInstrumentList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Problem problem = e.Row.DataItem as Problem;
            if (problem != null)
            {
                  HyperLink hpl = e.Row.FindControl("hplSchedule") as HyperLink;
                  Button btnStatus = e.Row.FindControl("btnStatus") as Button;
                  string url = String.Format("~/Maintenance/ScheduleCurativeMaintenance.aspx?{0}=2&ProblemId={1}&InstrumentId={2}", AppConstants.TABID, problem.Id, problem.InstrumentId);
                  hpl.NavigateUrl = this.ResolveUrl(url);
                  if (Convert.ToInt32(problem.Status) == 1)
                  {
                      btnStatus.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C");
                      hpl.Enabled = true;
                  }
                  else
                  {
                      hpl.Visible = false;
                  }
            }
        }

        public int ProblemId //returns problem id if the user cancels scheduling curative problems(keeps track of the selected problem with instrument)
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ProblemId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ProblemId"]);
                else
                    return 0;
            }
        }

        public IList<Problem> problemlist
        {
            set { _problemList = value; }
        }


        public int _problemTypeId
        {
            get 
            {
                if (Convert.ToInt32(ddlproblemType.SelectedValue) != 0)
                    return Convert.ToInt32(ddlproblemType.SelectedValue);
                else
                    return 0;
            }
        }

        public int _instCategoryId
        {
            get
            {
                if (Convert.ToInt32(ddlInstrumentCategory.SelectedValue) != 0)
                    return Convert.ToInt32(ddlInstrumentCategory.SelectedValue);
                else
                    return 0;
            }
        }

        
        public int _siteId
        {

            get
            {
                if (Convert.ToInt32(ddlSite.SelectedValue) != 0)
                    return Convert.ToInt32(ddlSite.SelectedValue);
                else
                {
                    if (_presenter.GetLocationId()[0] == 2)
                        return 0;
                    else
                        return _presenter.GetLocationId()[1];
                }

            }

        }
        public int regionId
        {
            get
            {

                if (Convert.ToInt32(ddlRegion.SelectedValue) != 0)
                    return Convert.ToInt32(ddlRegion.SelectedValue);
                else
                {
                    if (_presenter.GetLocationId()[0] == 2)
                        return _presenter.GetLocationId()[1];
                    else
                        return 0;
                }

            }

        }

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            _siteList = _presenter.GetSiteList(Convert.ToInt32(ddlRegion.SelectedValue));
            if (ddlSite.Items.Count > 0)
            {
                ddlSite.Items.Clear();
            }
            ListItem lst = new ListItem();
            lst.Text = "Select Site";
            lst.Value = "0";
            ddlSite.Items.Add(lst);



            ddlSite.DataSource = _siteList;
            ddlSite.DataBind();
        }
        protected void grvProblemWithInstrumentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvProblemWithInstrumentList.PageIndex = e.NewPageIndex;
            grvProblemWithInstrumentList.DataSource = _problemList;
            grvProblemWithInstrumentList.DataBind();
        }
}
}