﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public partial class PreventiveMaintenanceList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IPreventiveMaintenanceListView
    {
        private Chai.Maintenance.Modules.Maintenance.Views.PreventiveMaintenanceListPresenter _presenter;
        IList<CoreDomain.Maintenance.PreventiveMaintenance> _PreventiveMainList;
        IList<Chai.Maintenance.CoreDomain.Configuration.Site> _siteList;
        IList<Chai.Maintenance.CoreDomain.Configuration.InstrumentCategory> _instrumentCategoryList;
        int _instrumentId;
        IList<Chai.Maintenance.CoreDomain.Configuration.Region> _regionList;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();


            //if (_presenter.GetLocationId()[0] == 1) //site user
            //{
            //    lblRegion.Enabled = false;
            //    ddlRegion.Enabled = false;

            //    lblSite.Enabled = false;
            //    ddlSite.Enabled = false;
            //}
            //else if (_presenter.GetLocationId()[0] == 2) // region user
            //{
            //    if (_presenter.GetLocationId()[1] == 0) // federal user
            //    {
            //        BindRegion();
            //        BindSite(0);
            //        lblRegion.Enabled = true;
            //        ddlRegion.Enabled = true;

            //        lblSite.Enabled = true;
            //        ddlSite.Enabled = true;
            //    }
            //    else //regional user, get list of site based on selected region
            //    {
            //        BindSite(_presenter.GetLocationId()[1]); // passes the id 
            //        lblRegion.Enabled = false;
            //        ddlRegion.Enabled = false;

            //        lblSite.Enabled = true;
            //        ddlSite.Enabled = true;
            //    }

            //}
            grvPreventiveMaintenance.DataSource = _PreventiveMainList;
            grvPreventiveMaintenance.DataBind();
            BindInstrumentCategory();
        }
        //private void BindRegion()
        //{
        //    if (!IsPostBack)
        //    {
        //        _regionList = _presenter.GetRegionList();
        //        ddlRegion.DataSource = _regionList;
        //        ddlRegion.DataBind();
        //    }
        //}
        //private void BindSite(int regionId)
        //{
        //    if (!IsPostBack)
        //    {
        //        _siteList = _presenter.GetSiteList(regionId);
        //        ddlSite.DataSource = _siteList;
        //        ddlSite.DataBind();
        //    }
        //}
        private void BindInstrumentCategory()
        {
            if (!IsPostBack)
            {
                _instrumentCategoryList = _presenter.GetInstrumentCategoryList();
                ddlInstrumentCategory.DataSource = _instrumentCategoryList;
                ddlInstrumentCategory.DataBind();
            }
        }
        [CreateNew]
        public PreventiveMaintenanceListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        protected void grvPreventiveMaintenance_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvPreventiveMaintenance.PageIndex = e.NewPageIndex;
            grvPreventiveMaintenance.DataSource = _PreventiveMainList;
            grvPreventiveMaintenance.DataBind();
        }
        protected void grvPreventiveMaintenance_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            CoreDomain.Maintenance.PreventiveMaintenance preventive = e.Row.DataItem as CoreDomain.Maintenance.PreventiveMaintenance;
            HyperLink lnkEdit = e.Row.FindControl("lnkEdit") as HyperLink;
            if (preventive != null)
            {
                string url2 = String.Format("~/Maintenance/PreventiveMaintenance.aspx?{0}=2&InstrumentIdEdit={1}&PreventiveIdEdit={2}&ScheduleIdEdit={3}", AppConstants.TABID, preventive.InstrumentId, preventive.Id, preventive.ScheduleId);
                lnkEdit.NavigateUrl = this.ResolveUrl(url2);
               // lnkEdit.Enabled = false;
                 
                //if (preventive.PreventiveScheduleStatus == "Not Performed")
                //{
                //    lnkEdit.Enabled = true;
                //}
                //else
                //{
                //    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF6C"); // Already on Maintenance Process
                //    lnkEdit.Enabled = false;
                //}

            }
        }
        //protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    _siteList = _presenter.GetSiteList(Convert.ToInt32(ddlRegion.SelectedValue));
        //    if (ddlSite.Items.Count > 0)
        //    {
        //        ddlSite.Items.Clear();
        //    }
        //    ListItem lst = new ListItem();
        //    lst.Text = "Select Site";
        //    lst.Value = "0";
        //    ddlSite.Items.Add(lst);



        //    ddlSite.DataSource = _siteList;
        //    ddlSite.DataBind();
        //}

        #region schedule view
        public int _userId
        {
            get
            {
                 
                return _presenter.GetLocationId()[2];
            }
        }
        
        public int SiteId
        {
            get
            {
                //if (_presenter.GetLocationId()[0] == 0 && _presenter.GetLocationId()[1] == 0)
                //    return Convert.ToInt32(ddlSite.SelectedValue);
                //else

                if (_presenter.GetLocationId()[0] != 1)
                    return 0;
                else
                    return _presenter.GetLocationId()[1];
            }
        }
        public int _SiteId //parameter from edit Preventive mainteanance page after save, cancel clicked. to keep the selected prev Maintenance
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["SiteID"]) != 0)
                    return Convert.ToInt32(Request.QueryString["SiteID"]);
                else
                    return 0;
            }
        }
        public int _InstrumentCatId
        {
            get
            {
                { return Convert.ToInt32(ddlInstrumentCategory.SelectedValue); }
            }
        }


        public IList<CoreDomain.Maintenance.PreventiveMaintenance> _PreventiveMaintenanceList
        {
            set { _PreventiveMainList = value; }
        }

        #endregion


      

       
    }
}