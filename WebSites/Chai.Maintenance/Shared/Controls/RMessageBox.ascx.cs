﻿using System;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;

namespace Chai.Maintenance.Modules.Shell.Views
{
    public partial class RMessageBox : BaseMessageControl, IRMessageBoxView
    {
        private RMessageBoxPresenter _presenter;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();
        }

        [CreateNew]
        public RMessageBoxPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        public void BindMessage()
        {
            this.imgIcon.ImageUrl = Message.IconFileName;
            this.ltrMessage.Text = Message.MessageText;

            if (Message.MessageType == RMessageType.Info)
                this.panMessage.Attributes.Add("class", "alert alert-success fade in");
            else
                this.panMessage.Attributes.Add("class", "alert alert-danger fade in");
        }
    }
}

