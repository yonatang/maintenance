﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;

using Chai.Maintenance.Shared;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Services.Admin;
using System.Collections.Generic;

namespace Chai.Maintenance.Modules.Shell.Views
{
    public partial class ModuleNavigation : Microsoft.Practices.CompositeWeb.Web.UI.UserControl, IModuleNavigationView
    {
        private ModuleNavigationPresenter _presenter;
        private int _tabId;
        private TabServices _tabService;
     
        protected void Page_Load(object sender, EventArgs e)
        {
            _tabService = new TabServices();
            if (Request.QueryString[AppConstants.TABID] != null)
                this._tabId = int.Parse(Request.QueryString[AppConstants.TABID]);
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();
            BuildNavigation();
        }

        [CreateNew]
        public ModuleNavigationPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        
        public string TabId
        {
            get { return Request.QueryString[AppConstants.TABID]; }
        }

        private void BuildNavigation()
        {
            HtmlGenericControl mainList = new HtmlGenericControl("ul");

            //mainList.Attributes["class"] = "group";

          mainList.Controls.Add(BuildListItem(1, "Dashboard", String.Format("~/Default.aspx?{0}=1", AppConstants.TABID)));
            
            for(int i=2;i<=5;i++)
            {
                    MenuTab tab = _tabService.GetMenuTab(i);
                    mainList.Controls.Add(BuildListItem(i, tab.TabName, String.Format("~/{0}/Default.aspx?{1}={2}", tab.Tab, AppConstants.TABID, (int)tab.Tab)));
            }
                   
           
         
          
            this.plhNavigation.Controls.Add(mainList);
        }
        
        private HtmlControl BuildListItem(int tabid, string text, string url)
        {
            
            HtmlGenericControl listItem = new HtmlGenericControl("li");
           
           
            if (tabid == 1)
            {
                //listItem.Attributes["class"] = "item first";
               
               
            }
            else if (tabid == 6 && this._presenter.CurrentUser != null && !this._presenter.CurrentUser.HasPermission(AccessLevel.Administrator))
            {
               
               
               // listItem.Attributes["class"] = "item last";
            }
            else if (tabid == 0 )
            {
               
           
                //listItem.Attributes["class"] = "item last";
            }
            else
            {
              
              
               // listItem.Attributes["class"] = "item middle";
            }

            HyperLink hpl = new HyperLink();
            if (tabid == _tabId)
            {
                listItem.Attributes.Add("class", "open");
            } 
            HtmlGenericControl inline = new HtmlGenericControl("i");
            inline.Attributes["class"] = "fa fa-lg fa-fw fa-home";

            HtmlGenericControl span = new HtmlGenericControl("span");
            span.Attributes["class"] = "menu-item-parent";




            if (text == "Dashboard")
            {
                hpl.NavigateUrl = this.Page.ResolveUrl(url);
                
            }
            span.InnerText = text;

            hpl.Controls.Add(inline);
            
            hpl.Controls.Add(span);

            listItem.Controls.Add(hpl);
           
            

           
           
            DataBind(tabid, listItem);
            return listItem;

        }

        private void BuildSubMenuNavigation(IList<TabNode> nodes, PlaceHolder plh,HtmlGenericControl parentlistItem)
        {
            HtmlGenericControl mainList = new HtmlGenericControl("ul");
            
            foreach (TabNode node in nodes)
            {
                if (node.Node.ViewAllowed(_presenter.CurrentUser))
                {
                    mainList.Controls.Add(BuildSubMenuListItem(node));
                    
                }
            }
            parentlistItem.Controls.Add(mainList);
            plh.Controls.Add(parentlistItem);
        }

        private HtmlControl BuildSubMenuListItem(TabNode tnode)
        {
            HtmlGenericControl listItem = new HtmlGenericControl("li");
            HyperLink hpl = new HyperLink();
            //string url = String.Format("{0}/{1}?{2}={3}&{4}={5}", tnode.Node.FolderPath, tnode.Node.NodeUrl, RamcsConstants.TABID, TabId, RamcsConstants.NODEID, tnode.Id);
            string url=null;
            if (Convert.ToInt32(TabId) == 5)
            {
                url = String.Format("{0}&{1}={2}&{3}={4}", tnode.Node.NodeUrl, AppConstants.TABID, tnode.TabId, AppConstants.NODEID, tnode.Id);

            }
            else
            {
                url = String.Format("{0}?{1}={2}&{3}={4}", tnode.Node.NodeUrl, AppConstants.TABID, tnode.TabId, AppConstants.NODEID, tnode.Id);
            }

            hpl.NavigateUrl = this.Page.ResolveUrl(url);
             
            hpl.Text = tnode.Node.Title;

           // if (tnode.Node.ImagePath != null && tnode.Node.ImagePath != "")
                // listItem.Attributes.Add("style", String.Format("background-image:  url(~/Images/{0}); background-repeat: no-repeat;	background-position: left top;", tnode.Node.ImagePath));

                listItem.Controls.Add(hpl);
           
            return listItem;

        }

        //public void BindPanel()
        //{
        //    if (_presenter.CurrentTab != null)
        //    {
        //        this.rptPanel.DataSource = _presenter.CurrentTab.LeftMenuSections();
        //        this.rptPanel.DataBind();
        //    }
        //}

        private void DataBind(int tabId,HtmlGenericControl li)
        {
            // e.Item.DataItem as string;
            MenuTab mnutab = _presenter.Getmenutab(tabId);
           // string section = "Action";
            if(mnutab !=null)
            {
                //if (section != null)
               // {
                   // MenuSectionType mst = (MenuSectionType)Enum.Parse(typeof(MenuSectionType), section);
                    Literal ltr = ltrTitle;// e.Item.FindControl("ltrTitle") as Literal;
                    //ltr.Text = section;

                    PlaceHolder plh = plhNode;// e.Item.FindControl("plhNode") as PlaceHolder;
                    if (_presenter.Getmenutab(tabId).Tab != TabType.Report)
                    {
                       // switch (mst)
                       // {
                         //   case MenuSectionType.Find:
                                BuildSubMenuNavigation(mnutab.FindSectionNodes, plh,li);
                            //    break;
                          //  case MenuSectionType.Action:
                                BuildSubMenuNavigation(mnutab.ActionSectionNodes, plh,li);
                            //    break;
                        //}
                    }
                    else
                    {
                        //switch (mst)
                        //{
                          //  case MenuSectionType.Find:
                                BuildSubMenuNavigation(mnutab.FindSectionNodes, plh, li);
                            //    break;
                           // case MenuSectionType.Action:
                                BuildSubMenuNavigation(mnutab.ActionSectionNodes, plh, li);
                            //    break;
                      //  }
                    }
            
        
            }
        }
    }
}

