﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;

using Chai.Maintenance.Shared;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Enums;

namespace Chai.Maintenance.Modules.Shell.Views
{
    public partial class NodeNavigation : Microsoft.Practices.CompositeWeb.Web.UI.UserControl, INodeNavigationView
    {
        private NodeNavigationPresenter _presenter;
        
        public string TabId
        {
            get { return Request.QueryString[AppConstants.TABID]; }
        }

        public string NodeId
        {
            get { return Request.QueryString[AppConstants.NODEID]; }
        }

        //private 
        protected void Page_Load(object sender, EventArgs e)
        {
            //int id = int.Parse(Request.QueryString[RamcsConstants.TABID]);
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();
           
        }

        [CreateNew]
        public NodeNavigationPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        private void BuildNavigation(IList<TabNode> nodes, PlaceHolder plh)
        {
            HtmlGenericControl mainList = new HtmlGenericControl("ul");

            foreach (TabNode node in nodes )
            {
                if (node.Node.ViewAllowed(_presenter.CurrentUser))
                {
                    mainList.Controls.Add(BuildListItem(node));
                }
            }

            plh.Controls.Add(mainList);
        }

        private HtmlControl BuildListItem(TabNode tnode)
        {
            HtmlGenericControl listItem = new HtmlGenericControl("li");
            HyperLink hpl = new HyperLink();
            //string url = String.Format("{0}/{1}?{2}={3}&{4}={5}", tnode.Node.FolderPath, tnode.Node.NodeUrl, RamcsConstants.TABID, TabId, RamcsConstants.NODEID, tnode.Id);
            string url;
            if (Convert.ToInt32(TabId) == 5)
            {
                 url = String.Format("{0}&{1}={2}&{3}={4}", tnode.Node.NodeUrl, AppConstants.TABID, TabId, AppConstants.NODEID, tnode.Id);

            }
            else 
            {
                url = String.Format("{0}?{1}={2}&{3}={4}", tnode.Node.NodeUrl, AppConstants.TABID, TabId, AppConstants.NODEID, tnode.Id);
            }
            
            hpl.NavigateUrl = this.Page.ResolveUrl(url);
            //hpl.NavigateUrl = "RegionEdit.aspx?tabid=4&nodeid=1";
            hpl.Text = tnode.Node.Title;
           
            if (tnode.Node.ImagePath != null && tnode.Node.ImagePath != "")
               // listItem.Attributes.Add("style", String.Format("background-image:  url(~/Images/{0}); background-repeat: no-repeat;	background-position: left top;", tnode.Node.ImagePath));
           
            listItem.Controls.Add(hpl);

            return listItem;

        }

        public void BindPanel()
        {
            if (_presenter.CurrentTab != null)
            {
                this.rptPanel.DataSource = _presenter.CurrentTab.LeftMenuSections();
                this.rptPanel.DataBind();
            }
        }

        protected void rptPanel_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            string section  = e.Item.DataItem as string;
            if (section != null)
            {
                MenuSectionType mst = (MenuSectionType)Enum.Parse(typeof(MenuSectionType),section);
                Literal ltr = e.Item.FindControl("ltrTitle") as Literal;
                ltr.Text = section;

                PlaceHolder plh = e.Item.FindControl("plhNode") as PlaceHolder;
                if (_presenter.CurrentTab.Tab != TabType.Report)
                {
                    switch (mst)
                    {
                        case MenuSectionType.Find:
                            BuildNavigation(_presenter.CurrentTab.FindSectionNodes, plh);
                            break;
                        case MenuSectionType.Action:
                            BuildNavigation(_presenter.CurrentTab.ActionSectionNodes, plh);
                            break;
                    }
                }
                else
                {
                    switch (mst)
                    {
                        case MenuSectionType.Find:
                            BuildNavigation(_presenter.CurrentTab.FindSectionNodes, plh);
                            break;
                        case MenuSectionType.Action:
                            BuildNavigation(_presenter.CurrentTab.ActionSectionNodes, plh);
                            break;
                    }
                }
            }
        }

        #region INodeNavigationView Members


        public string PanelId
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}

