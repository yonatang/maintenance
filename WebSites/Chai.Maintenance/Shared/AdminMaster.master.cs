﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.DataAccess;

namespace Chai.Maintenance.Modules.Shell.MasterPages
{
    public partial class AdminMaster : BaseMaster, IAdminMasterView
    {
        private AdminMasterPresenter _presenter;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
                GetUserInfo();
            }
            base.CheckTransferdMessage();
            _presenter.OnViewLoaded();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            base.Message += new MessageEventHandler(AdminMaster_Message);
        }

        void AdminMaster_Message(object sender, Chai.Maintenance.Shared.Events.MessageEventArgs e)
        {
            BaseMessageControl ctr = (BaseMessageControl)Page.LoadControl("~/Shared/Controls/RMessageBox.ascx");
            ctr.Message = e.Message;
            this.plhMessage.Controls.Add(ctr);   
        }
  
        [CreateNew]
        public AdminMasterPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        void GetUserInfo()
        {

            UserInfoDao userInfo = new UserInfoDao();
            User.Text = "|  " + userInfo.GetUserInfo(Convert.ToInt32(_presenter.GetLocationId()[2])).UserName;
            if (Convert.ToInt32(_presenter.GetLocationId()[0]) == 2)
            {
                if (Convert.ToInt32(_presenter.GetLocationId()[1]) == 0)
                    location.Text = "Ethiopia";
                else
                    location.Text = userInfo.GetUserInfo(Convert.ToInt32(_presenter.GetLocationId()[2])).RegionName;
            }
            else
            {
                location.Text = userInfo.GetUserInfo(Convert.ToInt32(_presenter.GetLocationId()[2])).RegionName + "  --  " + userInfo.GetUserInfo(Convert.ToInt32(_presenter.GetLocationId()[2])).SiteName;
            }
        }
        protected void lnkChangepassword_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/ChangePassword.aspx?{0}=1", AppConstants.TABID));
        }
}
}
