﻿using System;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.Services;
using Chai.Maintenance.CoreDomain;
using System.Web.UI;
using Chai.Maintenance.DataAccess;
using Chai.Maintenance.Shared;
namespace Chai.Maintenance.Modules.Shell.MasterPages
{
    public partial class ModuleMaster : BaseMaster, IModuleMasterView
    {
        private ModuleMasterPresenter _presenter;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            base.CheckTransferdMessage();

            _presenter.OnViewLoaded();
            GetUserInfo();
            UserRole();
    
        }
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            base.Message += new MessageEventHandler(ModuleMaster_Message);
            //new MessageEventHandler(ModuleMaster_Message);
        }

        protected void ModuleMaster_Message(object sender, Chai.Maintenance.Shared.Events.MessageEventArgs e)
        {
            BaseMessageControl ctr = (BaseMessageControl)Page.LoadControl("~/Shared/Controls/RMessageBox.ascx");
            ctr.Message = e.Message;
            this.plhMessage.Controls.Add(ctr);
        }

        [CreateNew]
        public ModuleMasterPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        void GetUserInfo()
        {
            
            UserInfoDao userInfo = new UserInfoDao();
            User.Text =  "|  " + userInfo.GetUserInfo(Convert.ToInt32(_presenter.GetLocationId()[2])).UserName;
            if (Convert.ToInt32(_presenter.GetLocationId()[0]) == 2)
            {
                if (Convert.ToInt32(_presenter.GetLocationId()[1]) == 0)
                    location.Text = "Ethiopia";
                else
                    location.Text = userInfo.GetUserInfo(Convert.ToInt32(_presenter.GetLocationId()[2])).RegionName;
            }
            else
            {
                location.Text = userInfo.GetUserInfo(Convert.ToInt32(_presenter.GetLocationId()[2])).RegionName + "  --  " + userInfo.GetUserInfo(Convert.ToInt32(_presenter.GetLocationId()[2])).SiteName;
            }
        }
        #region IModuleMasterView Members

        public void LoadModuleComponent()
        {
            try
            {
                if (!base.ActiveNode.ViewAllowed(base.CurrentUser))
                {
                    throw new AccessForbiddenException("Access was Forbidden.");
                }

                if (base.ActiveTab != null)
                {
                    Response.Write(base.ActiveTab.ToString());
                }
                //if (base.ActiveModule != null)
                //{
                //    foreach (ModuleComponent mc in base.ActiveModule.ModuleComponents)
                //    {
                //        Control placeholder = this.FindControl(mc.PlaceHolder);

                //        BaseModuleControl ctr = (BaseModuleControl)this.LoadControl(mc.Path);
                //        ctr.Module = base.ActiveModule;
                //        ctr.BaseMaster = this;
                //        ctr.Title = mc.Title;
                //        placeholder.Controls.Add(ctr);
                //    }
                //}
            }
            catch (Exception exp)
            {
            }
        }

        #endregion
       
        protected void lnkChangepassword_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/ChangePassword.aspx?{0}=1", AppConstants.TABID));
        }
        protected void lnkAdmin_Click(object sender, EventArgs e)
        {
            this.Page.Response.Redirect(string.Format("~/Admin/Default.aspx?{0}=0", Chai.Maintenance.Shared.AppConstants.TABID));
        }
        private void UserRole()
        {
            foreach (UserRole userroles in CurrentUser.UserRoles)
            {
                if (userroles.Role.Name.Contains("Administrator"))
                {
                    lnkAdmin.Visible = true;
                    break;
                }
                else
                {
                    lnkAdmin.Visible = false;
                }

            }

        }
}
}
