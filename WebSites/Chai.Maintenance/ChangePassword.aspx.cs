﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Chai.Maintenance.DataAccess;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;

public partial class ChangePassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ChangePasswordPushButton_Click(object sender, EventArgs e)
    {
        Chai.Maintenance.CoreDomain.User user = HttpContext.Current.User.Identity as Chai.Maintenance.CoreDomain.User;
       // TextBox txtCurrentuser = null; //= CurrentPassword.Text;// ChangePassword1.ChangePasswordTemplateContainer.FindControl("CurrentPassword") as TextBox;
       // TextBox txtNewuser=null;// = ChangePassword1.ChangePasswordTemplateContainer.FindControl("NewPassword") as TextBox;
        
        UserServices userservices = new UserServices();
        string Encryptedcurrentuser = Encryption.StringToMD5Hash(CurrentPassword.Text);
        try
        {
            if (Encryptedcurrentuser == user.Password)
            {
                user.Password = Chai.Maintenance.CoreDomain.User.HashPassword(NewPassword.Text);
                userservices.SaveOrUpdateUser(user);
                Master.ShowMessage(new AppMessage("You successfully Change your password", Chai.Maintenance.Enums.RMessageType.Info));
                
            }
            else
            {
                Master.ShowMessage(new AppMessage("Error: Please Enter the Correct Old password" , Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        catch (Exception ex)
        {
            Master.ShowMessage(new AppMessage("Error: " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
        }
    }
    protected void CancelPushButton0_Click(object sender, EventArgs e)
    {
        Response.Redirect(String.Format("~/Default.aspx?{0}=1",AppConstants.TABID));
    }
    protected void CancelPushButton0_Click1(object sender, EventArgs e)
    {
        Response.Redirect("~/UserLogIn.aspx");
    }
}
