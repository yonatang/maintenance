﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Nodes.aspx.cs" Inherits="Chai.Maintenance.Modules.Admin.Views.Nodes"
    Title="Nodes" MasterPageFile="~/Shared/AdminMaster.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="MenuContent" runat="Server">
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" runat="Server">
       <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Nodes</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">   
                     <label class="select">
                    <asp:DropDownList ID="ddlViewtype" runat="server">
       </asp:DropDownList><i></i></label>
       </section></div></fieldset>
       <footer>
       <asp:HyperLink ID="hplNewnode" Cssclass="btn btn-primary" runat="server">Register New Node</asp:HyperLink>
       <asp:Button ID="butFiliter" runat="server" Text="List" Cssclass="btn btn-primary" 
                                 onclick="butFiliter_Click" />
              
                    
                </footer>
        </div>
                                </div>
                                </div>
        <asp:GridView ID="grvNodes" runat="server" AutoGenerateColumns="False" 
                         CssClass="table table-striped table-bordered table-hover" 
                         PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
            CellPadding="3" ForeColor="#333333" GridLines="Horizontal" Width="100%" 
           OnRowDataBound="grvNodes_RowDataBound" PageSize="25" AllowPaging="True" 
                         onpageindexchanging="grvNodes_PageIndexChanging">
            
            <Columns>
                <asp:TemplateField HeaderText="Folder">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem,"FolderPath") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Node Title">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem,"Title") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Categorys">
                    <ItemTemplate>
                       <%# DataBinder.Eval(Container.DataItem,"ViewType") %>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Image">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem,"ImagePath") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem,"Description") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            
            <PagerStyle  Cssclass="paginate_button active" HorizontalAlign="Center" />
        </asp:GridView>
        </div>
</asp:Content>
