﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Shared.Settings;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Enums;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Admin.Views
{
    public partial class UserEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IUserEditView
    {
        private UserEditPresenter _presenter;
        private IList<Region> _regionlist;
        //private static IList<UserLocation> userlocationList;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
                BindUserControls();
                BindRoles();
                Bindddlregion();
                ddlSite.DataSource = _presenter.GetSite(Convert.ToInt32(ddlRegion.SelectedValue));
                ddlSite.DataBind();
            }
            this._presenter.OnViewLoaded();
            

            if (ChkIsExternalUser.Checked)
            {
                ddlVendor.Visible = true;
                lblVendor.Visible = true;
                inlinevendor.Visible = true;
                ddlRegion.Visible = false;
                ddlSite.Visible = false;
                inlineregion.Visible = false;
                inlinesite.Visible = false;
                txtFirstname.Visible = false;
                txtLastname.Visible = false;
                lblFirstname.Visible = false;
                lblLastname.Visible = false;
                lblRegion.Visible = false;
                lblSite.Visible = false;
                RfvFirstname.Visible = false;
                RfvLastname.Visible = false;
                rfvRegion.Visible = false;

            }
            else
            {
                RfvVendor.Visible = false;
                ddlVendor.Visible = false;
                inlinevendor.Visible = false;
                ddlRegion.Visible = true;
                ddlSite.Visible = true;
                inlineregion.Visible = true;
                inlinesite.Visible = true;
                txtFirstname.Visible = true;
                txtLastname.Visible = true;
                lblFirstname.Visible = true;
                lblLastname.Visible = true;
                lblRegion.Visible = true;
                lblSite.Visible = true;
                lblVendor.Visible = false;
            }
            // userlocationList=_presenter.CurrentUser.UserLocations;
        }

        [CreateNew]
        public UserEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void Bindddlregion()
        {
           // _regionlist = _presenter.GetRegion();
            _regionlist = _presenter.GetRegions();
            ddlRegion.DataSource = _regionlist;
            ddlRegion.DataBind();

        }
        private void BindUserControls()
        {
            if (!_presenter.CurrentUser.IsNew())
            {
                this.txtUsername.Visible = false;
                this.lblUsername.Text = _presenter.CurrentUser.UserName;
                this.lblUsername.Visible = true;
                this.rfvUsername.Enabled = false;
            }
            else
            {
                this.txtUsername.Text = _presenter.CurrentUser.UserName;
                this.txtUsername.Visible = true;
                this.lblUsername.Visible = false;
                this.rfvUsername.Enabled = true;
            }
            this.txtFirstname.Text = _presenter.CurrentUser.FirstName;
            this.txtLastname.Text = _presenter.CurrentUser.LastName;
            this.txtEmail.Text = _presenter.CurrentUser.Email;
            this.chkActive.Checked = _presenter.CurrentUser.IsActive;
            this.ChkIsExternalUser.Checked = _presenter.CurrentUser.IsExternalUser;
            this.ddlVendor.SelectedValue = _presenter.CurrentUser.VendorId.ToString();
            this.ddlRegion.SelectedValue = _presenter.CurrentUser.RegionId.ToString();
            this.ddlSite.SelectedValue = _presenter.CurrentUser.SiteId.ToString();
            this.btnDelete.Visible = (_presenter.CurrentUser.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Are you sure you want to delete this user?\")");
        }

        private void BindRoles()
        {
            IList<Role> roles = _presenter.GetRoles();
            FilterAnonymousRoles(roles);
            this.rptRoles.ItemDataBound += new RepeaterItemEventHandler(rptRoles_ItemDataBound);
            this.rptRoles.DataSource = roles;
            this.rptRoles.DataBind();
        }

        private void FilterAnonymousRoles(IList<Role> roles)
        {
            int roleCount = roles.Count;
            for (int i = roleCount - 1; i >= 0; i--)
            {
                Role role = roles[i];
                if (role.PermissionLevel == (int)AccessLevel.Anonymous)
                {
                    roles.Remove(role);
                }
            }
        }

        private void SetRoles()
        {
            foreach (UserRole ur in _presenter.CurrentUser.UserRoles)
            {
                ur.IsDirty = true;
            }

            foreach (RepeaterItem ri in rptRoles.Items)
            {
                CheckBox chkRole = (CheckBox)ri.FindControl("chkRole");
                if (chkRole.Checked)
                {
                    int roleId = (int)this.ViewState[ri.ClientID];
                    Role role = _presenter.GetRoleById(roleId);
                    UserRole urole = new UserRole(_presenter.CurrentUser.Id, role);

                    _presenter.CurrentUser.UserRoles.Add(urole);
                }
            }

            string adminRole = UserSettings.GetAdministratorRole;
            if (_presenter.CurrentUser.UserName == UserSettings.GetSuperUser
                && !_presenter.CurrentUser.IsInRole(adminRole))
            {
                throw new Exception(String.Format("The user '{0}' has to have the '{1}' role."
                    , _presenter.CurrentUser.UserName, adminRole));
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            CheckValidation();
            if (Page.IsValid)
            {
                try
                {
                    SetRoles();
                    if (_presenter.CurrentUser.IsNew())
                    {
                        _presenter.SaveOrUpdateUser();
                        Master.TransferMessage(new AppMessage("User created successfully", Chai.Maintenance.Enums.RMessageType.Info));
                        _presenter.RedirectPage(String.Format("~/Admin/UserEdit.aspx?{0}=0&{1}={2}", AppConstants.TABID, AppConstants.USERID, _presenter.CurrentUser.Id));
                    }
                    else
                    {
                        _presenter.SaveOrUpdateUser();
                        Master.ShowMessage(new AppMessage("User saved", Chai.Maintenance.Enums.RMessageType.Info));
                    }
                }
                catch (Exception ex)
                {
                    Master.ShowMessage(new AppMessage("Error: " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
                }
            }
        }

        #region IUserEditView Members

        public string GetUserId
        {
            get { return Request.QueryString[AppConstants.USERID]; }
        }

        public string GetUserName
        {
            get { return this.txtUsername.Text; }
        }

        public string GetFirstName
        {
            get { return this.txtFirstname.Text; }
        }

        public string GetLastName
        {
            get { return this.txtLastname.Text; }
        }

        public string GetEmail
        {
            get { return this.txtEmail.Text; }
        }

        public bool GetIsActive
        {
            get
            {
                return this.chkActive.Checked;
            }
        }

        public string GetPassword
        {
            get
            {
                return this.txtPassword1.Text;
            }
        }
        public bool IsExternaUser
        {
            get { return this.ChkIsExternalUser.Checked; }
        }

        public int VendorId
        {
            get { return Convert.ToInt32(this.ddlVendor.SelectedValue); }
        }

        public int RegionId
        {
            get { return Convert.ToInt32(this.ddlRegion.SelectedValue); }
        }

    public int SiteId
        {
            get { return Convert.ToInt32(this.ddlSite.SelectedValue); }
        }

        #endregion

        protected void rptRoles_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Role role = e.Item.DataItem as Role;
            if (role != null)
            {
                CheckBox chkView = (CheckBox)e.Item.FindControl("chkRole");
                chkView.Checked = this._presenter.CurrentUser.IsInRole(role);

                // Add RoleId to the ViewState with the ClientID of the repeateritem as key.
                this.ViewState[e.Item.ClientID] = role.Id;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }


        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteUser();
                Master.TransferMessage(new AppMessage("User was deleted", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Error: " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        //protected void lnkAdd_Click(object sender, EventArgs e)
        //{
        //    if(userlocationList==null)
        //     userlocationList = _presenter.CurrentUser.UserLocations;

        //     UserLocation userlocations = new UserLocation();
        //     userlocations.RegionId= Convert.ToInt32(ddlRegion.SelectedValue);
        //     userlocations.SiteId = Convert.ToInt32(ddlSite.SelectedValue);
        //     userlocationList.Add(userlocations);
        //     Session["UserLocation"] = userlocationList;
        //     grvLocation.DataSource = userlocationList;
        //     grvLocation.DataBind();
        //}


        //public IList<UserLocation> userlocation
        //{
        //    get { return userlocationList = (IList<UserLocation>)Session["UserLocation"]; }
        //}
        //protected void grvLocation_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ddlRegion.SelectedValue = grvLocation.Rows[grvLocation.SelectedIndex].Cells[0].ToString();
        //    ddlSite.SelectedValue = grvLocation.Rows[grvLocation.SelectedIndex].Cells[1].ToString();
        //}
        //protected void grvLocation_RowEditing(object sender, GridViewEditEventArgs e)
        //{

        //}



        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSite.Items.Count > 0)
            {
                ListItem lst = new ListItem();
                ddlSite.Items.Clear();
                lst.Text = "Select Site";
                lst.Value = "0";
                ddlSite.Items.Add(lst);
            }
            ddlSite.DataSource = _presenter.GetSite(Convert.ToInt32(ddlRegion.SelectedValue));
            ddlSite.DataBind();
            if (ChkIsExternalUser.Checked)
            {
                RfvVendor.ValidationGroup = Convert.ToString(1);

            }
            else
            {
                RfvFirstname.ValidationGroup = Convert.ToString(1);
                RfvLastname.ValidationGroup = Convert.ToString(1);
                rfvRegion.ValidationGroup = Convert.ToString(1);
            }
            
        }
        

       

        protected void CheckValidation()
        { 
        if (ChkIsExternalUser.Checked)
            {
                RfvVendor.ValidationGroup = Convert.ToString(1);

            }
            else
            {
                RfvFirstname.ValidationGroup = Convert.ToString(1);
                RfvLastname.ValidationGroup = Convert.ToString(1);
                rfvRegion.ValidationGroup = Convert.ToString(1);
            }
        }
        }
    }


