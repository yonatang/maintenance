﻿using System;
using System.Web.UI.WebControls;
using Microsoft.Practices.ObjectBuilder;

using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public partial class Nodes : Microsoft.Practices.CompositeWeb.Web.UI.Page, INodesView
    {
        private NodesPresenter _presenter;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
                PopViewType();
                BindNodes();
               
                string url = String.Format("~/Admin/NodeEdit.aspx?{0}=0&{1}=0", AppConstants.TABID, AppConstants.NODEID);
                hplNewnode.NavigateUrl = this.ResolveUrl(url);
            }

            this._presenter.OnViewLoaded();
        }

        [CreateNew]
        public NodesPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void PopViewType()
        {
            string[] pages = Enum.GetNames(typeof(PageViewType));
            for (int i = 0; i < pages.Length; i++)
            {
                ddlViewtype.Items.Add(new ListItem(pages[i], pages[i]));
            }
        }
        private void BindNodes()
        {
            
                PageViewType pagetype = (PageViewType)Enum.Parse(typeof(PageViewType), ddlViewtype.SelectedValue);
                grvNodes.DataSource = _presenter.GetNodes(pagetype);
                grvNodes.DataBind();
           
        }

        protected void grvNodes_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            Node node = e.Row.DataItem as Node;
            if (node != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Admin/NodeEdit.aspx?{0}=0&{1}={2}", AppConstants.TABID, AppConstants.NODEID, node.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
            }
        }
        protected void butFiliter_Click(object sender, EventArgs e)
        {
            BindNodes();
        }
        protected void grvNodes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PageViewType pagetype = (PageViewType)Enum.Parse(typeof(PageViewType), ddlViewtype.SelectedValue);
            grvNodes.PageIndex = e.NewPageIndex;
            grvNodes.DataSource = _presenter.GetNodes(pagetype);
            grvNodes.DataBind();
        }
}
}

