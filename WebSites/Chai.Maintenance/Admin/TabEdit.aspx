﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.Admin.Views.TabEdit"
    Title="TabEdit" MasterPageFile="~/Shared/AdminMaster.master" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="content1" ContentPlaceHolderID="MenuContent" runat="Server">
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" Runat="Server">
		<div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Tab Edit</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>
        <table style="width: 100%">
            <tr>
                <td colspan="2">
                    Action Section</td>
                <td>
                    &nbsp;</td>
                <td colspan="2">
                    Find Section</td>
                <td>
                    &nbsp;</td>
                <td class="inbox-data-from" style="width: 198px">
                    Dropdown Section</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="ddlAction" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="butAddaction" runat="server" Text="Add" 
                        onclick="butAddaction_Click" class="btn btn-primary"/>
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddlFind" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="butAddfind" runat="server" Text="Add" 
                        onclick="butAddfind_Click" class="btn btn-primary"/>
                </td>
                <td class="inbox-data-from" style="width: 198px">
                    <asp:DropDownList ID="ddlDropdown" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="butAdddropdown" runat="server" Text="Add" 
                        onclick="butAdddropdown_Click" class="btn btn-primary"/>
                </td>
            </tr>
             <tr>
                <td rowspan="2" style="width: 205px">
                    <asp:ListBox ID="ltbAction" runat="server" Height="350px" Width="200px">
                    </asp:ListBox>
                    </td>
                <td style="height: 9px" valign="bottom">
                    &nbsp;</td>
                <td rowspan="2">
                    <asp:Button ID="butRemoveaction" runat="server" onclick="butRemoveaction_Click" 
                        Text="Remove"  class="btn btn-primary"/>
                    </td>
                <td rowspan="2" style="width: 19px">
                    <asp:ListBox ID="ltbFind" runat="server" Height="350px" Width="200px">
                    </asp:ListBox>
                    </td>
                <td valign="bottom">
                    &nbsp;</td>
                <td rowspan="2">
                    <asp:Button ID="butRemovefind" runat="server" onclick="butRemovefind_Click" 
                        Text="Remove"  class="btn btn-primary"/>
                    </td>
                <td rowspan="2" class="inbox-data-from" style="width: 198px">
                    <asp:ListBox ID="ltbDropdown" runat="server" Height="350px" Width="200px">
                    </asp:ListBox>
                    </td>
                <td rowspan="2">
                    <asp:Button ID="butRemovedropdown" runat="server" 
                        onclick="butRemovedropdown_Click" Text="Remove" Cssclass="btn btn-primary" />
                    </td>
            </tr>
             <tr>
                <td valign="top">
                    &nbsp;</td>
                <td valign="top">
                    &nbsp;</td>
            </tr>
        </table>
        </fieldset></div></div></div></div>
</asp:Content>
