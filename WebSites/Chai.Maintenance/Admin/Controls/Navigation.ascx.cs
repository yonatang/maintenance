﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Admin.Views;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public partial class Navigation : Microsoft.Practices.CompositeWeb.Web.UI.UserControl, INavigationView
    {
        private  NavigationPresenter _presenter;
   
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

                hplMaintenance.NavigateUrl = Page.ResolveUrl(String.Format("~/Admin/TabEdit.aspx?{0}=0&{1}={2}", AppConstants.TABID, AppConstants.NODEID, (int)TabType.Maintenance));
                hplSparepartInventory.NavigateUrl = Page.ResolveUrl(String.Format("~/Admin/TabEdit.aspx?{0}=0&{1}={2}", AppConstants.TABID, AppConstants.NODEID, (int)TabType.SparepartInventory));
                hplConfiguration.NavigateUrl = Page.ResolveUrl(String.Format("~/Admin/TabEdit.aspx?{0}=0&{1}={2}", AppConstants.TABID, AppConstants.NODEID, (int)TabType.Configuration));
                hplReports.NavigateUrl = Page.ResolveUrl(String.Format("~/Admin/TabEdit.aspx?{0}=0&{1}={2}", AppConstants.TABID, AppConstants.NODEID, (int)TabType.Report));
                hplHelp.NavigateUrl = Page.ResolveUrl(String.Format("~/Admin/TabEdit.aspx?{0}=0&{1}={2}", AppConstants.TABID, AppConstants.NODEID, (int)TabType.Help));
            }

            this._presenter.OnViewLoaded();
        }

        [CreateNew]
        public NavigationPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        
    }
}

