﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Navigation.ascx.cs" Inherits="Chai.Maintenance.Modules.Admin.Views.Navigation" %>

<ul>
					<li>
						<a href="#" title="Admin"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Admin</span></a>
                        <ul>
                           <li>
								<a href="#">Tabs</a>
                                  <ul>
                                    <li> <asp:hyperlink id="hplMaintenance" runat="server">Maintenance</asp:hyperlink></li>
                                    <li> <asp:hyperlink id="hplSparepartInventory" runat="server">SparepartInventory</asp:hyperlink></li>
		                            <li> <asp:hyperlink id="hplConfiguration" runat="server">Configuration</asp:hyperlink></li>
		                            <li> <asp:hyperlink id="hplReports" runat="server">Report</asp:hyperlink></li>
                              		<li> <asp:hyperlink id="hplHelp" runat="server">Help</asp:hyperlink></li>
                                  </ul>
							</li>
                           <li><asp:hyperlink id="hplNodes" navigateurl="../Nodes.aspx" runat="server">Manage Nodes</asp:hyperlink></li>
   
	                       <li><asp:hyperlink id="hplRoles" navigateurl="../Roles.aspx" runat="server">Manage roles</asp:hyperlink></li>
    
		                   <li><asp:hyperlink id="hplUsers" navigateurl="../Users.aspx" runat="server">Manage users</asp:hyperlink></li>
                        </ul>
                        </li>

		 
    </ul>
 


      



