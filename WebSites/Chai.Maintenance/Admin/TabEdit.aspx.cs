﻿using System;
using System.Web.UI.WebControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.Enums;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public partial class TabEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, ITabEditView
    {
        private TabEditPresenter _presenter;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
                PopNodesToDdl();
                PopActionList();
                PopFindList();
                PopDropdown();
            }
            this._presenter.OnViewLoaded();
        }

        [CreateNew]
        public TabEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        private void PopNodesToDdl()
        {
            ddlAction.Items.Clear();
            ddlFind.Items.Clear();
            foreach (Node node in _presenter.GetNodes((PageViewType)2))
            {
                if (!_presenter.CurrentTab.NodeWasAddedToActionsection(node.Id))
                    ddlAction.Items.Add(new ListItem(node.Title, node.Id.ToString()));
            }

            foreach (Node node in _presenter.GetNodes((PageViewType)1))
            {
                if (!_presenter.CurrentTab.NodeWasAddedToFindsection(node.Id))
                    ddlFind.Items.Add(new ListItem(node.Title, node.Id.ToString()));
            }

            //foreach (Node node in _presenter.GetNodes())
            //{
            //    if (!_presenter.CurrentTab.NodeWasAddedToDropdownsection(node.Id))
            //        ddlAction.Items.Add(new ListItem(node.Title, node.Id.ToString()));
            //}
        }

        private void PopActionList()
        {
            ltbAction.Items.Clear();
            foreach (TabNode tn in _presenter.CurrentTab.ActionSectionNodes)
            {
                ltbAction.Items.Add(new ListItem(tn.Node.Title, tn.Id.ToString()));
            }

        }

        private void PopFindList()
        {
            ltbFind.Items.Clear();
            foreach (TabNode tn in _presenter.CurrentTab.FindSectionNodes)
            {
                ltbFind.Items.Add(new ListItem(tn.Node.Title, tn.Id.ToString()));
            }

        }

        private void PopDropdown()
        {
            ltbDropdown.Items.Clear();
            foreach (TabNode tn in _presenter.CurrentTab.DropdownSectionNodes)
            {
                ltbDropdown.Items.Add(new ListItem(tn.Node.Title, tn.Id.ToString()));
            }
        }

        protected void butAddaction_Click(object sender, EventArgs e)
        {
            try
            {
                TabNode tnode = new TabNode();
                tnode.Section = MenuSectionType.Action;
                tnode.Tab = _presenter.CurrentTab.Tab;
                tnode.Node = _presenter.GetNode(int.Parse(ddlAction.SelectedValue));

                _presenter.CurrentTab.ActionSectionNodes.Add(tnode);
                _presenter.SaveOrUpdateTab();
                PopNodesToDdl();
                PopActionList();
                Master.ShowMessage(new AppMessage("Tab was saved seccessfully", RMessageType.Info));
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to save Tab. " + ex.Message, RMessageType.Error));
            }

        }
        protected void butAddfind_Click(object sender, EventArgs e)
        {
            try
            {
                TabNode tnode = new TabNode();
                tnode.Section = MenuSectionType.Find;
                tnode.Tab = _presenter.CurrentTab.Tab;
                tnode.Node = _presenter.GetNode(int.Parse(ddlFind.SelectedValue));

                _presenter.CurrentTab.FindSectionNodes.Add(tnode);
                _presenter.SaveOrUpdateTab();

                PopNodesToDdl();
                PopFindList();
                Master.ShowMessage(new AppMessage("Tab was saved seccessfully", RMessageType.Info));
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to save Tab. " + ex.Message, RMessageType.Error));
            }
        }
        protected void butAdddropdown_Click(object sender, EventArgs e)
        {
            try
            {
                TabNode tnode = new TabNode();
                tnode.Section = MenuSectionType.Dropdown;
                tnode.Tab = _presenter.CurrentTab.Tab;
                tnode.Node = _presenter.GetNode(int.Parse(ddlDropdown.SelectedValue));

                _presenter.CurrentTab.ActionSectionNodes.Add(tnode);
                _presenter.SaveOrUpdateTab();
                PopNodesToDdl();
                PopDropdown();
                Master.ShowMessage(new AppMessage("Tab was saved seccessfully", RMessageType.Info));
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to save Tab. " + ex.Message, RMessageType.Error));
            }
        }
        protected void butRemoveaction_Click(object sender, EventArgs e)
        {
            int id = int.Parse(ltbAction.SelectedValue);
            TabNode tnode = _presenter.CurrentTab.GetTabNodeFromAction(id);

            if (tnode != null)
                tnode.IsDirty = true;
            try
            {
                _presenter.SaveOrUpdateTab();
                PopNodesToDdl();
                PopActionList();
                Master.ShowMessage(new AppMessage("Tab was saved seccessfully", RMessageType.Info));
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to save Tab. " + ex.Message, RMessageType.Error));
            }
        }
        protected void butRemovefind_Click(object sender, EventArgs e)
        {
            int id = int.Parse(ltbFind.SelectedValue);
            TabNode tnode = _presenter.CurrentTab.GetTabNodeFromFind(id);

            if (tnode != null)
                tnode.IsDirty = true;
            try
            {
                _presenter.SaveOrUpdateTab();
                PopNodesToDdl();
                PopFindList();
                Master.ShowMessage(new AppMessage("Tab was saved seccessfully", RMessageType.Info));
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to save Tab. " + ex.Message, RMessageType.Error));
            }
        }
        protected void butRemovedropdown_Click(object sender, EventArgs e)
        {
            int id = int.Parse(ltbDropdown.SelectedValue);
            TabNode tnode = _presenter.CurrentTab.GetTabNodeFromDropdown(id);

            if (tnode != null)
                tnode.IsDirty = true;
            try
            {
                _presenter.SaveOrUpdateTab();
                PopNodesToDdl();
                PopDropdown();
                Master.ShowMessage(new AppMessage("Tab was saved seccessfully", RMessageType.Info));
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to save Tab. " + ex.Message, RMessageType.Error));
            }
        }

        #region ITabEditView Members

        public string GetTabId
        {
            get { return Request.QueryString[AppConstants.TABID]; }
        }

        public string GetNodeId
        {
            get { return Request.QueryString[AppConstants.NODEID]; }
        }

        #endregion
        protected void btnUp_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (ltbFind.SelectedIndex != 0)
                {
                    string value;
                    int index = ltbFind.SelectedIndex - 1;
                    value = ltbFind.SelectedItem.Text;
                    ltbFind.Items.Remove(value);
                    ltbFind.Items.Insert(index, value);
                    ltbFind.Focus();


                }
                else
                {
                    Master.ShowMessage(new AppMessage("Item Is in the TOP Position", RMessageType.Info));
                   
                }
            }
            catch
            {
                Master.ShowMessage(new AppMessage("Please Select One Item from List", RMessageType.Info));

                
            }
 
 


        }
        protected void btnDown_Click(object sender, EventArgs e)
        {

        }
}
}

