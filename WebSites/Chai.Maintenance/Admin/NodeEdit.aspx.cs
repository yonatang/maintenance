﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.Shared;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Enums;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public partial class NodeEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, INodeEditView
    {
        private NodeEditPresenter _presenter;

        protected void Page_Load(object sender, EventArgs e)
        {  
            
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
                btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");
            }
            this._presenter.OnViewLoaded();
        }

        [CreateNew]
        public NodeEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
       
        public string GetTabId
        {
            get{return Request.QueryString[AppConstants.TABID];}
        }

        public string GetNodeId
        {
            get{ return Request.QueryString[AppConstants.NODEID];}
        }
        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.SaveOrUpdateNode();
                Master.ShowMessage(new AppMessage("Node was saved seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
            }
            catch (Exception ex)
            {
              
                Master.ShowMessage(new AppMessage("Erro: Unable to save node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelIt();
        }
        
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
            _presenter.DeleteIt();
            Master.ShowMessage(new AppMessage("Node was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }

        #region INodeEditView Members

        public void BindNode()
        {
            PopAspxPages();
            PopViewType();

            if (_presenter.CurrentNode.Id > 0)
            {
                this.txtTitle.Text = _presenter.CurrentNode.Title;
                this.txtDescription.Text = _presenter.CurrentNode.Description;
                this.txtFolderpath.Text = _presenter.CurrentNode.FolderPath;
                this.txtImagePath.Text = _presenter.CurrentNode.ImagePath;
                ddlAspxpage.SelectedValue = _presenter.CurrentNode.Id.ToString();
                ddlCategory.SelectedValue = _presenter.CurrentNode.ViewType.ToString();
                ddlAspxpage.Enabled = false;
                btnDelete.Visible = true;
            }
            else
            {
                btnDelete.Visible = false;
            }
        }

        private void PopAspxPages()
        {
            string[] pages = Enum.GetNames(typeof(AspxPageType));
            for (int i = 0; i < pages.Length; i++)
            {
                AspxPageType at = (AspxPageType)Enum.Parse(typeof(AspxPageType), pages[i]);

                ddlAspxpage.Items.Add(new ListItem(pages[i], ((int)at).ToString()));
            }
        }

        private void PopViewType()
        {
            string[] pages = Enum.GetNames(typeof(PageViewType));
            for (int i = 0; i < pages.Length; i++)
            {
                ddlCategory.Items.Add(new ListItem(pages[i], pages[i]));
            }
        }

        public void BindRoles()
        {
            this.rptRoles.DataSource = _presenter.GetRoles;
            this.rptRoles.DataBind();
        }

        #endregion
      
        public void SetRoles(Node node)
        {
            foreach (NodePermission np in node.NodePermissions)
            {
                np.IsDirty = true;
            }
            
            foreach (RepeaterItem ri in rptRoles.Items)
            {
                CheckBox chkView = (CheckBox)ri.FindControl("chkViewAllowed");
                CheckBox chkEdit = (CheckBox)ri.FindControl("chkEditAllowed");
				if (chkView.Checked || chkEdit.Checked)
				{
                    NodePermission np = new NodePermission();
                    np.NodeId = node.Id;
                    np.Role = _presenter.GetRole((int)ViewState[ri.ClientID]);
                    np.ViewAllowed = chkView.Checked;
                    np.EditAllowed = chkEdit.Checked;

                    node.NodePermissions.Add(np);
                }
            }
        }

        protected void rptRoles_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Role role = e.Item.DataItem as Role;
            if (role != null)
            {
                CheckBox chkView = (CheckBox)e.Item.FindControl("chkViewAllowed");
                if (_presenter.CurrentNode != null)
                    chkView.Checked = this._presenter.CurrentNode.ViewAllowed(role);

                CheckBox chkEdit = (CheckBox)e.Item.FindControl("chkEditAllowed");
                if (_presenter.CurrentNode != null)
                    chkEdit.Checked = this._presenter.CurrentNode.EditAllowed(role);

                this.ViewState[e.Item.ClientID] = role.Id;
            }

        }

        #region INodeEditView Members


        public string GetAspxPageId
        {
            get { return ddlAspxpage.SelectedValue; }
        }

        public string GetViewType
        {
            get { return ddlCategory.SelectedValue; }
        }

        public string GetTitle
        {
            get { return txtTitle.Text; }
        }

        public string GetDescription
        {
            get { return txtDescription.Text; }
        }

        public string GetFolderPath
        {
            get { return txtFolderpath.Text; }
        }

        public string GetImagePath
        {
            get { return txtImagePath.Text; }
        }

        #endregion
    }
}

