﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.Admin.Views.UserEdit"
    Title="UserEdit" MasterPageFile="~/Shared/AdminMaster.master" %>

<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="content1" ContentPlaceHolderID="MenuContent" runat="Server">
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" runat="Server">
<div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>User</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">   
    
                    <label class="label"> User name</label> 
                    <label class="input">
    
                   
               
                    <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ControlToValidate="txtUsername"
                        Display="Dynamic" ErrorMessage="User name is required" 
                        SetFocusOnError="True" ValidationGroup="1"></asp:RequiredFieldValidator>
                    <cc1:ValidatorCalloutExtender ID="rfvUsername_ValidatorCalloutExtender"
                        runat="server" Enabled="True" TargetControlID="rfvUsername" Width="300px">
                    </cc1:ValidatorCalloutExtender>
                    <asp:Label ID="lblUsername" runat="server" Visible="False"></asp:Label></label></section></div>
                <div class="row">
									<section class="col col-6">   
                     <label class="checkbox">
                    <asp:CheckBox ID="ChkIsExternalUser" runat="server" Text="Is External User" AutoPostBack="True" /><i></i></label></section></div>
                <div class="row">
									<section class="col col-6">
                                                      
                    <asp:Label ID="lblVendor" runat="server" Text="Vendor" Visible="False"></asp:Label>
                <label class="select"> 
                    <asp:DropDownList ID="ddlVendor" runat="server" CausesValidation="True" 
                        Visible="False">
                        <asp:ListItem Value="0">Select Vendor</asp:ListItem>
                        <asp:ListItem Value="1">Mosea</asp:ListItem>
                    </asp:DropDownList><i runat="server" id="inlinevendor" visible="false"></i>
                    <asp:RequiredFieldValidator ID="RfvVendor" runat="server" 
                        ControlToValidate="ddlVendor" Display="Dynamic" ErrorMessage="Vendor Required" 
                        InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator></label></section></div>
                <div class="row">
									<section class="col col-6">
                                    
                    <asp:Label ID="lblFirstname" runat="server" Text="First name"></asp:Label>
                <label class="input">
                    <asp:TextBox ID="txtFirstname" runat="server" 
                        CausesValidation="True"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RfvFirstname" runat="server" 
                        ControlToValidate="txtFirstname" Display="Dynamic" 
                        ErrorMessage="First Name Required" SetFocusOnError="True"></asp:RequiredFieldValidator></label></section></div>
                <div class="row">
									<section class="col col-6">
                    <asp:Label ID="lblLastname" runat="server" Text="Last name"></asp:Label>
                 <label class="input">
                    <asp:TextBox ID="txtLastname" runat="server"
                        CausesValidation="True"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RfvLastname" runat="server" 
                        ControlToValidate="txtLastname" Display="Dynamic" 
                        ErrorMessage="Last Name Required" SetFocusOnError="True"></asp:RequiredFieldValidator></label></section></div>
                 <div class="row">
									<section class="col col-6">
                <asp:Label ID="lblRegion" runat="server" Text="Region"></asp:Label>
               <label class="select">
                    <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                        AutoPostBack="True" CausesValidation="True" DataTextField="RegionName" 
                        DataValueField="Id" onselectedindexchanged="ddlRegion_SelectedIndexChanged" 
                       >
                        <asp:ListItem Value="-1">Select Region</asp:ListItem>
                    </asp:DropDownList><i runat="server" id="inlineregion" visible="false"></i>
                    <asp:RequiredFieldValidator ID="rfvRegion" runat="server" 
                        ControlToValidate="ddlRegion" Display="Dynamic" ErrorMessage="Region Required" 
                        InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator></label></section></div>
               <div class="row">
									<section class="col col-6">
                    <asp:Label ID="lblSite" runat="server" Text="Site"></asp:Label>
               <label class="select">
                    <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                        DataTextField="Name" DataValueField="Id" >
                        <asp:ListItem Value="0">Select Site</asp:ListItem>
                    </asp:DropDownList><i runat="server" id="inlinesite" visible="false"></i></label></section></div>
                   
                <div class="row">
									<section class="col col-6">
                    <asp:Label ID="Label1" runat="server" Text="Email"></asp:Label>
                    
                <label class="input">
                    <asp:TextBox ID="txtEmail" runat="server" ></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                        Display="Dynamic" ErrorMessage="Invalid Email Address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        SetFocusOnError="True" ValidationGroup="1"></asp:RegularExpressionValidator>
                    <cc1:ValidatorCalloutExtender ID="RegularExpressionValidator1_ValidatorCalloutExtender"
                        runat="server" Enabled="True" TargetControlID="RegularExpressionValidator1" Width="300px">
                    </cc1:ValidatorCalloutExtender></label></section></div>
               <div class="row">
									<section class="col col-6">
                  
                    
                <label class="checkbox">
                                    
                    <asp:CheckBox ID="chkActive" runat="server" Text="Is Active" Checked="True"></asp:CheckBox><i></i></label></section></div>
                 <div class="row">
									<section class="col col-6">
                    <asp:Label ID="Label3" runat="server" Text="Password" ></asp:Label>
                    
                <label class="input">
                                    
                    <asp:TextBox ID="txtPassword1" type="password" runat="server" TextMode="Password"
                       ></asp:TextBox></label></section></div>
                  <div class="row">
									<section class="col col-6">
                    <asp:Label ID="Label4" runat="server" Text="Confirm password"></asp:Label>
                    
                <label class="input">
                                    
                    <asp:TextBox ID="txtPassword2" type="password" runat="server" TextMode="Password"
                        ></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword1"
                        ControlToValidate="txtPassword2" Display="Dynamic" ErrorMessage="The password you typed donot match. Please retype the password"
                        SetFocusOnError="True" ValidationGroup="1"></asp:CompareValidator>
                    <cc1:ValidatorCalloutExtender ID="CompareValidator1_ValidatorCalloutExtender" runat="server"
                        Enabled="True" TargetControlID="CompareValidator1" Width="300px">
                    </cc1:ValidatorCalloutExtender></label></section></div>
                   </fieldset> 
          <h4>
            Roles</h4>      
    <div class="table-responsive">
       
        <table class="table table-striped table-bordered table-hover">
            <asp:Repeater ID="rptRoles" runat="server">
                <HeaderTemplate>
                    <tr>
                        <th>
                            Role
                        </th>
                        <th>
                        </th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%# DataBinder.Eval(Container.DataItem, "Name") %>
                        </td>
                        <td style="text-align: center">
                            <asp:CheckBox ID="chkRole" runat="server"></asp:CheckBox>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    <footer>
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" 
            ValidationGroup="1" CommandName="Save" class="btn btn-primary"></asp:Button>
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="False"
            OnClick="btnCancel_Click" class="btn btn-primary"></asp:Button>
        <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click"  Visible="False" class="btn btn-primary"></asp:Button>
    </footer></div></div></div></div>
</asp:Content>
