﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class VendorEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IVendorEditView
    {

        private VendorEditPresenter _presenter;
        private Vendor _Vendor;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();


            if (Id != 0)
            {
                if (!Page.IsPostBack)
                {
                    BindVendorControls();
                }
            }


        }
        [CreateNew]
        public VendorEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindVendorControls()
        {

            this.txtVendor.Text = _Vendor.Name;

            this.btnDelete.Visible = (_Vendor.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _Vendor.Name = txtVendor.Text;

                SaveVendor();

            }
        }
        private void SaveVendor()
        {
            try
            {
                if (_Vendor.IsNew())
                {
                    _presenter.SaveOrUpdateVendor(_Vendor);
                    Master.TransferMessage(new AppMessage("Vendor saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateVendor(_Vendor);
                    Master.ShowMessage(new AppMessage("Vendor saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {

            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteVendor(_Vendor.Id);
                Master.ShowMessage(new AppMessage("Node was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }

        #region IVendorEditView Members

        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["vendorId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["vendorId"]);
                else
                    return 0;

            }
        }

        Vendor IVendorEditView._Vendor
        {
           
            get
            {
                return _Vendor;
            }
            set
            {
                _Vendor = value;
            }
        }

        #endregion
    }
}