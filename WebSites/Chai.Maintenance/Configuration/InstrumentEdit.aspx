﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="InstrumentEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.InstrumentEdit" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <div class="alert alert-block alert-success">
  <h4 class="alert-heading">Site Information</h4>
	<p>
                <asp:Label ID="lblSiteName" runat="server" Text="Site Name" CssClass="label"></asp:Label>
           
                <asp:Label ID="lblResultName" runat="server" CssClass="label"></asp:Label>
            
                <asp:Label ID="lblSiteCode" runat="server" Text="Site Code" CssClass="label"></asp:Label>
                <asp:Label ID="lblresultCode" runat="server"  CssClass="label"></asp:Label>
        </p>
</div>     
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        HeaderText="Error" ValidationGroup="1" class="alert alert-danger fade in"/>
    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Instrument</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
    
             
                <asp:Label ID="lblInstrumentName" runat="server" Text="Instrument Name" 
                    CssClass="label"></asp:Label>
                 <label class="select">
                <asp:DropDownList ID="ddlInsName" runat="server" 
                    AppendDataBoundItems="True" DataTextField="InstrumentName" 
                    DataValueField="Id" CssClass="textbox" style="margin-left: 0px">
                    <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                    <asp:ListItem Value="1">AUTOLAB 18 and SABA</asp:ListItem>
                </asp:DropDownList><i></i>
                <asp:RequiredFieldValidator ID="RfvInstrumentName" runat="server" 
                    ControlToValidate="ddlInsName" Display="Dynamic" 
                    ErrorMessage="Select Instrument Name" InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator></label>
                    </section>
                                    <section class="col col-6"> 
                <asp:Label ID="lblManufacturer" runat="server" CssClass="label" 
                    Text="Manufacturer"></asp:Label>
           <label class="select">
                <asp:DropDownList ID="ddlManufacturer" runat="server" 
                    AppendDataBoundItems="True" CssClass="textbox" DataTextField="Name" 
                    DataValueField="Id">
                    <asp:ListItem Value="0">Select Manufacturer</asp:ListItem>
                </asp:DropDownList><i></i></label>
           </section>
                                    
            </div>
            <div class="row">
									<section class="col col-6">   
                                    
                <asp:Label ID="lblLotNumber" runat="server" Text="Lot Number" CssClass="label"></asp:Label>
        <label class="input">
                <asp:TextBox ID="txtLotNumber" runat="server" CssClass="textbox"></asp:TextBox></label>
                </section>
                                    <section class="col col-6">  
                <asp:Label ID="lblVendor" runat="server" CssClass="label" Text="Vendor"></asp:Label>
             <label class="select">
                <asp:DropDownList ID="ddlVendor" runat="server" AppendDataBoundItems="True" 
                    CssClass="textbox" DataTextField="Name" DataValueField="Id" 
                    onselectedindexchanged="ddlVendor_SelectedIndexChanged">
                    <asp:ListItem Value="0">Select Vendor</asp:ListItem>
                </asp:DropDownList><i></i></label>
               </section>
                                    
                
            </div>
             <div class="row">
                                    <section class="col col-6">   
                <asp:Label ID="lblSerialNo" runat="server" Text="Serial No." CssClass="label"></asp:Label>
                <label class="input">
                <asp:TextBox ID="txtSerialNo" runat="server" CssClass="textbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvSerialno" runat="server" 
                    ControlToValidate="txtSerialNo" Display="Dynamic" 
                    ErrorMessage="Serial Number Required" ValidationGroup="1">*</asp:RequiredFieldValidator></label>
            </section>
                                    <section class="col col-6"> 
               <label></label>
               <label class="checkbox">
                <asp:CheckBox ID="chkContract" runat="server" Text=""/><i></i>Is Under Service Contract</label>
            </section>

								
           </div>
           <div class="row">	
                                    <section class="col col-6">  
                <asp:Label ID="lblPreventiveMaintenancePeriod" runat="server" CssClass="label" 
                    Text="Preventive Maintenance Period"></asp:Label>
                <label class="select">
                <asp:DropDownList ID="ddlPeriod" runat="server" CssClass="textbox">
                    <asp:ListItem Value="&quot;&quot;">Select Period</asp:ListItem>
                    <asp:ListItem Value="1Year">1 Year</asp:ListItem>
                    <asp:ListItem Value="6Month">6 Month</asp:ListItem>
                    <asp:ListItem Value="3Month">3 Month</asp:ListItem>
                    <asp:ListItem Value="1Month">1 Month</asp:ListItem></asp:DropDownList><i></i></label>
            
           </section>
                                    <section class="col col-6">   

                <asp:Label ID="lblContractWith_Id" runat="server" Text="Contract With" 
                    CssClass="label"></asp:Label>
                                 <label class="select">
                <asp:DropDownList ID="ddlContractWith" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Vendor</asp:ListItem>
                </asp:DropDownList><i></i></label>
               </section>
									
                                    
               </div>
               <div class="row">
									<section class="col col-6">  
                <asp:Label ID="lblWarranityExpireDate" runat="server" CssClass="label" 
                    Text="Warranity Expire Date"></asp:Label>
               <label class="input">
               <i class="icon-append fa fa-calendar"></i>
                <asp:TextBox ID="Calendar2" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox>
                
                <asp:RegularExpressionValidator ID="REVWExpireDate" runat="server" 
                    ControlToValidate="Calendar2" 
                    ErrorMessage="Warraniity Expire Date Is Not Valid" 
                    ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                    ValidationGroup="1">*</asp:RegularExpressionValidator></label>
           </section>
                                    <section class="col col-6">  
                <asp:Label ID="lblInstrallationDate" runat="server" CssClass="label" 
                    Text="Installation Date"></asp:Label>
           <label class="input">
           <i class="icon-append fa fa-calendar"></i>
           <asp:TextBox ID="Calendar1" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox>
               
                <asp:RegularExpressionValidator ID="REVInstallationDate" runat="server" 
                    ControlToValidate="Calendar1" ErrorMessage="Installation Date Is Not Valid" 
                    ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                    ValidationGroup="1">*</asp:RegularExpressionValidator></label>
            </section>
            </div>
             <div class="row">
                                    <section class="col col-6">  
               <label></label>
              <label class="checkbox">
                <asp:CheckBox ID="chkIsFunctional" runat="server" Text=""/><i></i>Is Functional</label>
            </section>
									<section class="col col-6">  
                <asp:Label ID="lblLastPreventiveMaintenanceDate" runat="server" 
                    CssClass="label" Text="Last Preventive Maintenance Date"></asp:Label>
                  <label class="input">
                  <i class="icon-append fa fa-calendar"></i>
                  <asp:TextBox ID="Calendar3" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox>
                  <asp:RegularExpressionValidator ID="REVLPMDate" runat="server" 
                    ControlToValidate="Calendar3" 
                    ErrorMessage="Last Preventive Maintenance Date Is Not Valid" 
                    ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                    ValidationGroup="1">*</asp:RegularExpressionValidator></label>
            </section>
            </div>
            </fieldset>
             <footer>
                <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" 
                    ValidationGroup="1" Cssclass="btn btn-primary"/>
                <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" 
                    Text="Cancel" Cssclass="btn btn-primary"/>
                <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" 
                    Text="Delete" Cssclass="btn btn-primary"/>
           </footer>
      
                                </div>
                                </div>
                                </div>

                       
 <asp:GridView ID="grvInstrumentList" runat="server" AutoGenerateColumns="False"
            CellPadding="1" ForeColor="#333333" GridLines="Horizontal" Width="100%" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
            EnableModelValidation="True" 
            onselectedindexchanged="grvInstrumentList_SelectedIndexChanged" 
        DataKeyNames="Id" AllowPaging="True" 
        onpageindexchanging="grvInstrumentList_PageIndexChanging">
        
        <Columns>
            <asp:BoundField DataField="InstrumentName" HeaderText="Instrument Name" />
            <asp:BoundField DataField="LotNumber" HeaderText="Lot Number" />
            <asp:BoundField DataField="SerialNo" HeaderText="Serial No." />
            <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
            <asp:CommandField SelectText="Edit" ShowSelectButton="True" />
        </Columns>
        <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
        
    </asp:GridView>
    </div>
</asp:Content>

