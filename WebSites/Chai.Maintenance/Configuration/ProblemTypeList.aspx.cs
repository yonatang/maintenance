﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class ProblemTypeList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IProblemTypeListView
    {
        private ProblemTypeListPresenter _presenter;
        private IList<ProblemType> _problemType = null;
        private int _problemTypeId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();

            grvProblemTypeList.DataSource = _problemType;
            grvProblemTypeList.DataBind();
        }
        [CreateNew]
        public ProblemTypeListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        protected void grvProblemTypeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvProblemTypeList.PageIndex = e.NewPageIndex;
            grvProblemTypeList.DataSource = _problemType;
            grvProblemTypeList.DataBind();
        }
        protected void grvProblemTypeList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ProblemType problemType = e.Row.DataItem as ProblemType;
            if (problemType != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/ProblemTypeEdit.aspx?{0}=4&ProblemTypeId={1}", AppConstants.TABID, problemType.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
                _problemTypeId = problemType.Id;
            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetProblemTypes(ProblemType);
        }

        public int ProblemTypeId
        {
            get { throw new NotImplementedException(); }
        }

        public IList<ProblemType> problemTypelist
        {
            set { _problemType = value; }
        }

        public string ProblemType
        {
            get { return txtProblemtype.Text; }
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("ProblemTypeEdit.aspx?{0}=4", AppConstants.TABID));
        }
}
}