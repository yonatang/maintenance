﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="SparepartTypeList.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.SparepartTypeList" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
     <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Find Sparepart Type</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">       
         
                        <asp:Label ID="lblFindby" runat="server" Text="Find By" CssClass="label"></asp:Label>
                        <label class="select">
                        <asp:DropDownList ID="ddlSelectBy" runat="server" 
                            onselectedindexchanged="ddlSelectBy_SelectedIndexChanged" 
                            AutoPostBack="True" CssClass="textbox">
                            <asp:ListItem Value="0">Select Criteria</asp:ListItem>
                            <asp:ListItem Value="1">Sparepart Name</asp:ListItem>
                            <asp:ListItem Value="2">Sparepart Part No.</asp:ListItem>
                            <asp:ListItem Value="3">Instrument</asp:ListItem>
                        </asp:DropDownList><i></i></label>
                        </section>
                        <section class="col col-6">
                    
                        <asp:Label ID="lblValue" runat="server" Text="Value" CssClass="label" Visible="False"></asp:Label>
                    <label class="input">
                        <asp:TextBox ID="txtValue" runat="server" Visible="False" CssClass="textbox"></asp:TextBox></label>
                      </section>
                      </div>
                      <div class="row">
                     <section class="col col-6">  
                     <asp:Label ID="lblinstrument" runat="server" Text="Instrument" CssClass="label" 
                              Visible="False"></asp:Label>
                     <label class="select">
                        <asp:DropDownList ID="ddlValue" runat="server" 
                            DataTextField="InstrumentName" DataValueField="Id" 
                            AppendDataBoundItems="True" Visible="False" >
                            <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                        </asp:DropDownList><i id="inlineinstrument" runat="server" visible="False"></i></label>
                        </section>
                        
                        </div>
                          
                          </fieldset>
                          <footer>
                        <asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click" Cssclass="btn btn-primary">Add New Sparepart Type</asp:LinkButton>
                        <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" Cssclass="btn btn-primary" />
                      </footer>
                     </div>
                                </div>
                                </div>
            <asp:GridView ID="grvSparepartTypeList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True" 
                    ForeColor="#333333" GridLines="Horizontal" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    onrowdatabound="grvRegionList_RowDataBound" 
                Width="100%" AllowPaging="True" 
                onpageindexchanging="grvSparepartTypeList_PageIndexChanging">
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <Columns>
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instruments" />
                    <asp:BoundField DataField="Name" HeaderText="Sparepart Name" />
                    <asp:BoundField DataField="PartNumber" HeaderText="Part Number" />
                    <asp:BoundField DataField="UOMName" HeaderText="UoM" />
                    <asp:BoundField DataField="ReorderQty" HeaderText="Reorder Qty" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle  Cssclass="paginate_button active" HorizontalAlign="Center" />
                
           
            </asp:GridView>
            
           
            </div>
                 
            
       
</asp:Content>

