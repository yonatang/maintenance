﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="EditSite.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.EditSite" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
<div class="alert alert-block alert-success">
		<h4 class="alert-heading">Region Information</h4>
	<p>
                    
   
                <asp:Label ID="lblRegionName" runat="server" Text="Region Name" 
                    CssClass="label"></asp:Label>
          
                <asp:Label ID="lblGetRegionName" runat="server" CssClass="label"></asp:Label>
           
                <asp:Label ID="lblRegionCode" runat="server" Text="Region Code" 
                    CssClass="label"></asp:Label>
           
                <asp:Label ID="lblGetRegionCode" runat="server" CssClass="label"></asp:Label>
            
    </p>
</div>
  

    <asp:ValidationSummary ID="ValidationSummarySite" runat="server" 
        HeaderText="Error" ValidationGroup="1" class="alert alert-danger fade in"/>
    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Sparepart Type</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
            <asp:Label ID="lblSiteName" runat="server" Font-Bold="False" 
                CssClass="label"
                Text="Site Name"></asp:Label>
            <label class="input">
            <asp:TextBox ID="txtSiteName" runat="server" ></asp:TextBox>
            <cc1:FilteredTextBoxExtender ID="txtSiteName_FilteredTextBoxExtender" 
                runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters" 
                TargetControlID="txtSiteName" ValidChars="/_ ">
            </cc1:FilteredTextBoxExtender>
            <asp:RequiredFieldValidator ID="RqvSiteName" runat="server" 
                ControlToValidate="txtSiteName" ErrorMessage="Site Name Required" 
                ValidationGroup="1">*</asp:RequiredFieldValidator></label>
                </section>
            <section class="col col-6">   
            <asp:Label ID="lblAddress" runat="server" Font-Bold="False" 
                CssClass="label"
                Text="Address"></asp:Label>
        <label class="input">
            <asp:TextBox ID="txtAddress" runat="server" ></asp:TextBox></label>
        </section>
        </div>
        <div class="row">
									<section class="col col-6">   
            <asp:Label ID="lblSiteCode" runat="server" Font-Bold="False" 
                CssClass="label"
                Text="Site Code"></asp:Label>
        <label class="input">
            <asp:TextBox ID="txtSiteCode" runat="server" ></asp:TextBox></label>
        </section>
        	<section class="col col-6">   
            <asp:Label ID="lblTel1" runat="server" Font-Bold="False" 
                CssClass="label"
                Text="Telephone1"></asp:Label>
            <label class="input">
            <asp:TextBox ID="txtTel1" runat="server" ValidationGroup="1" ></asp:TextBox>
            <cc1:MaskedEditExtender ID="txtTel1_MaskedEditExtender" runat="server" 
                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                Mask="251999999999" MaskType="Number" TargetControlID="txtTel1">
            </cc1:MaskedEditExtender></label>
            </section>
            </div>
            <div class="row">
									<section class="col col-6">   
            <asp:Label ID="lblCity" runat="server" Font-Bold="False" 
                CssClass="label"              

                Text="City"></asp:Label>
            <label class="input">
            <asp:TextBox ID="txtCity" runat="server" ></asp:TextBox>
            <cc1:FilteredTextBoxExtender ID="txtCity_FilteredTextBoxExtender" 
                runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters" 
                TargetControlID="txtCity" ValidChars=" /">
            </cc1:FilteredTextBoxExtender></label>
           </section>
           <section class="col col-6">   
            <asp:Label ID="lblTelephone2" runat="server" Font-Bold="False" 
                CssClass="label"
                Text="Telephone2"></asp:Label>
            <label class="input">
            <asp:TextBox ID="txttel2" runat="server" ValidationGroup="1" ></asp:TextBox>
            <cc1:MaskedEditExtender ID="txttel2_MaskedEditExtender" runat="server" 
                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                Mask="+251999999999" MaskType="Number" TargetControlID="txttel2">
            </cc1:MaskedEditExtender></label>
             </section>
             </div>
             <div class="row">
									<section class="col col-6">   
            <asp:Label ID="lblSiteType" runat="server" Font-Bold="False" 
                CssClass="label"
                Text="Site Type"></asp:Label>
            <label class="select">
            <asp:DropDownList ID="ddlSiteType" runat="server" 
                AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                >
                <asp:ListItem Value="0">Select Site Type</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RfvSiteType" runat="server" 
                ControlToValidate="ddlSiteType" ErrorMessage="Site Type Required" 
                InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator><i></i></label>
                </section>
            <section class="col col-6">   
            <asp:Label ID="lblEmail" runat="server" Font-Bold="False" 
                                Text="Email" CssClass="label"></asp:Label>
            <label class="input">
            <asp:TextBox ID="txtEmail" runat="server" ></asp:TextBox></label>
            </section>
            </div>
            </fieldset>
               <footer>
            <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" 
                ValidationGroup="1" CommandName="Save" Cssclass="btn btn-primary"/>
       
            <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" 
                Text="Delete" Cssclass="btn btn-primary"/>
        
            <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click1" 
                Text="Cancel" Cssclass="btn btn-primary"/>
     </footer>
      
                                </div>
                                </div>
                                </div>
                                
         
 <asp:GridView ID="grvRegionList" runat="server" AutoGenerateColumns="False"
            CellPadding="1" ForeColor="#333333" GridLines="Horizontal" Width="100%" CssClass="table table-striped table-bordered table-hover"  PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
            EnableModelValidation="True" 
               onselectedindexchanged="grvRegionList_SelectedIndexChanged" 
        DataKeyNames="Id" AllowPaging="True" 
        onpageindexchanging="grvRegionList_PageIndexChanging">
         <Columns>
            <asp:BoundField DataField="Name" HeaderText="Site Name" />
            <asp:BoundField DataField="Code" HeaderText="Site Code" />
            <asp:BoundField DataField="City" HeaderText="City" />
            <asp:BoundField DataField="Telephone1" HeaderText="Telephone 1" />
            <asp:BoundField DataField="Telephone2" HeaderText="Telephone 2" />
            <asp:CommandField SelectText="Edit" ShowSelectButton="True" />
        </Columns>
         <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
        
        
    </asp:GridView>
    </div>

</asp:Content>

