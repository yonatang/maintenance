﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="UnitOfMeasurmentList.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.UnitOfMeasurmentList" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
 
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="DefaultContent" Runat="Server">
 <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Find Unit Of Measurment</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">       
                                      <asp:Label ID="Label2" runat="server" Text="Unit Of Measurment" CssClass="label"></asp:Label>
                                      <label class="input">
                                        <asp:TextBox ID="txtUOM" runat="server"></asp:TextBox>
                                             </label>
                                   </section>
                          </div>
                          </fieldset>
                          <footer>
        <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click" Cssclass="btn btn-primary">Add New Unit of Measurment</asp:LinkButton>
        <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click" Cssclass="btn btn-primary" />
    
    </footer>
                     </div>
                                </div>
                                </div>
    
    <asp:GridView ID="grvUOMList" runat="server" CellPadding="3" 
        EnableModelValidation="True" ForeColor="#333333" GridLines="Horizontal" 
                         CssClass="table table-striped table-bordered table-hover" 
                         PagerStyle-Cssclass="paginate_button active" AlternatingRowStyle-CssClass=""
        Width="100%" AutoGenerateColumns="False" 
        onrowdatabound="grvUOMList_RowDataBound" style="text-align: left" AllowPaging="True" 
                         onpageindexchanging="grvUOMList_PageIndexChanging">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Unit Of Measurment" />
            <asp:BoundField DataField="Description" HeaderText="Description" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink ID="hplEdit" runat="server" Text="Edit"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
       
        <PagerStyle  Cssclass="paginate_button active" HorizontalAlign="Center" />
        
       
    </asp:GridView>
     </div>
   
                
               
</asp:Content>