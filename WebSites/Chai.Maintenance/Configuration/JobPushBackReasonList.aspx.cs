﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class JobPushBackReasonList : Microsoft.Practices.CompositeWeb.Web.UI.Page,
                                                 IJobPushBackReasonListView
    {
        private JobPushBackReasonListPresenter _presenter;
        IList<JobPushBackReason> jobPushBackReason = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();

            grvReasonList.DataSource = jobPushBackReason;
            grvReasonList.DataBind();
        }
        [CreateNew]
        public JobPushBackReasonListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        protected void grvReasonList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            JobPushBackReason jobPushBackReason = e.Row.DataItem as JobPushBackReason;
            if (jobPushBackReason != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/JobPushBackReasonEdit.aspx?{0}=4&jobPushBackReasonId={1}", AppConstants.TABID, jobPushBackReason.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
          
            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetJobPushBackReasons(value);
        }
        #region IJobPushBackReasonListView Members

        public int JobPushBackReasonId
        {
            get { throw new NotImplementedException(); }
        }

        IList<JobPushBackReason> IJobPushBackReasonListView.JobPushBackReasonList
        {
            set { jobPushBackReason = value; }
        }

        public string value
        {
            get { return txtReason.Text; }
        }

        #endregion
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("JobPushBackReasonEdit.aspx?{0}=4",AppConstants.TABID));
        }
        protected void grvReasonList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvReasonList.PageIndex = e.NewPageIndex;
            grvReasonList.DataSource = jobPushBackReason;
            grvReasonList.DataBind();
        }
}
}