﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="InstrumentTypeList.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.InstrumentTypeList" %>


<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Find Instrument Type</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">       
         
                       
                        <asp:Label ID="lblFindby" runat="server" Text="Find By" CssClass="label"></asp:Label>
                   <label class="select">
                        <asp:DropDownList ID="ddlSelectBy" runat="server" 
                            onselectedindexchanged="ddlSelectBy_SelectedIndexChanged" 
                            AutoPostBack="True" >
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="1">Instrument Catagory</asp:ListItem>
                            <asp:ListItem Value="2">Instrument Name</asp:ListItem>
                        </asp:DropDownList><i></i></label>
                        </section>
                        <section class="col col-6">
                        <asp:Label ID="lblValue" runat="server" Text="Value" CssClass="label" Visible="False"></asp:Label>
                    <label class="input">
                        <asp:TextBox ID="txtValue" runat="server" Visible="False" ></asp:TextBox></label>
                        </section>
                      </div>
                      <div class="row">
                     <section class="col col-6">  
                      <asp:Label ID="lblCatagory" runat="server" Text="Instrument Category" CssClass="label" 
                              Visible="False"></asp:Label>
                       <label class="select">
                        <asp:DropDownList ID="ddlCatagory" runat="server" Visible="False" 
                            DataTextField="Name" DataValueField="Id" AppendDataBoundItems="True" 
                            >
                            <asp:ListItem Value="0">Select Category</asp:ListItem>
                        </asp:DropDownList><i id="inlineCatagory" runat="server" visible="False"></i></label>
                     </section>
                        
                        </div>
                          
                          </fieldset>
                          <footer>
                  
         <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click" Cssclass="btn btn-primary">Add New Instrument Type</asp:LinkButton>
              <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" Cssclass="btn btn-primary" />
           </footer>
                     </div>
                                </div>
                                </div>
            <asp:GridView ID="grvInstrumentType" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True" 
                    ForeColor="#333333" GridLines="Horizontal"  CssClass="table table-striped table-bordered table-hover" 
                 PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    onrowdatabound="grvInstrumentType_RowDataBound" Width="100%" 
                 onselectedindexchanged="grvInstrumentType_SelectedIndexChanged" AllowPaging="True" 
                         onpageindexchanging="grvInstrumentType_PageIndexChanging">
       
              
                <Columns>
                    <asp:BoundField DataField="Name" 
                        HeaderText="Instrument Category" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument Name" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField SelectText="Download Service Manual" 
                        ShowSelectButton="True" />
                </Columns>
                <PagerStyle Cssclass="paginate_button active" HorizontalAlign="Center" />
              
                
               
            </asp:GridView>
         
           
        </div>               
                  
</asp:Content>

