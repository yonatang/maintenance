﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="UnitOfMeasurmentEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.UnitOfMeasurmentEdit" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">



    <asp:ValidationSummary ID="vlsummery" runat="server" ValidationGroup="1" 
        HeaderText="Error Message" class="alert alert-danger fade in"/>
    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Sparepart Type</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
  
    
        <asp:Label ID="lblUOM" runat="server" Text="Unit Of Measurement" 
            CssClass="label"></asp:Label>
        <label class="input">
        <asp:TextBox ID="txtUOM" runat="server" ValidationGroup="1" ></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            Display="Dynamic" ErrorMessage="Unit Of Measurement Required" 
            ValidationGroup="1" ControlToValidate="txtUOM">*</asp:RequiredFieldValidator></label>
       </section>
       <section class="col col-6">   
        <asp:Label ID="Label3" runat="server" Text="Description" CssClass="label"></asp:Label>
         <label class="textarea">
        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" 
           ></asp:TextBox></label>
                     </section>
             </div>
             </fieldset>
             <footer>
    <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" 
        ValidationGroup="1" Cssclass="btn btn-primary"/>
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
        onclick="btnCancel_Click" Cssclass="btn btn-primary"/>
    <asp:Button ID="btnDelete" runat="server" Text="Delete" 
        onclick="btnDelete_Click" Cssclass="btn btn-primary"/>

        </footer>
      
                                </div>
                                </div>
                                </div>
                                </div>
    

</asp:Content>