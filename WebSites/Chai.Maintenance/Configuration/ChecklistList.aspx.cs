﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;

namespace Chai.Maintenance.Modules.Configuration.View
{
    public partial class ChecklistList : Page, IChecklistTaskListView
    {
        private ChecklistTaskListPresenter _presenter;
        private IList<ChecklistTask> _checklistTask = null;
        IList<InstrumentType> _instrumentTypeList;
        private int _checklistTaskId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();

            grvChecklist.DataSource = _checklistTask;
            grvChecklist.DataBind();

            BindInstrumentType();
        }
        [CreateNew]
        public ChecklistTaskListPresenter Presenter
        {
            get { return this._presenter; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
      void BindInstrumentType()
      {
          if (!IsPostBack)
          {
              _instrumentTypeList = _presenter.GetInstrumenttype();
              ddlInstrument.DataSource = _instrumentTypeList;
              ddlInstrument.DataBind();
          }
      }

        protected void grvChecklist_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvChecklist.PageIndex = e.NewPageIndex;
            grvChecklist.DataSource = _checklistTask;
            grvChecklist.DataBind();
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetChecklistTask(Task, InstrumentLookupId);
        }

        public int ChecklistId
        {
            get { throw new NotImplementedException(); }
        }

        public IList<ChecklistTask> ChecklistTask
        {
            set { _checklistTask = value; }
        }

        public string Task
        {
            get { return txtTask.Text; }
        }

        public int InstrumentLookupId
        {
            get { return Convert.ToInt32(ddlInstrument.SelectedValue); }
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/Configuration/ChecklistEdit.aspx?{0}=4",AppConstants.TABID));
        }
        protected void grvChecklist_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ChecklistTask _checklistTask = e.Row.DataItem as ChecklistTask;
            if (_checklistTask != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/ChecklistEdit.aspx?{0}=4&ChecklistId={1}", AppConstants.TABID, _checklistTask.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
                _checklistTaskId = _checklistTask.Id;
            }

        }
}
}