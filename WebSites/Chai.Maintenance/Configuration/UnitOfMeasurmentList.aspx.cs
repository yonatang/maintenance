﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class UnitOfMeasurmentList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IUnitOfMeasurmentListView
    {
        private UnitOfMeasurmentListPresenter _presenter;
        IList<UnitOfMeasurment> _unitOfMeasurment = null;
        int _unitOfMeasurmentId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();

            grvUOMList.DataSource = _unitOfMeasurment;
            grvUOMList.DataBind();
        }

        [CreateNew]
        public UnitOfMeasurmentListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }




        protected void grvUOMList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            UnitOfMeasurment unitOfMeasurment = e.Row.DataItem as UnitOfMeasurment;
            if (unitOfMeasurment != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/UnitOfMeasurmentEdit.aspx?{0}=4&unitOfMeasurmentId={1}", AppConstants.TABID, unitOfMeasurment.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
                _unitOfMeasurmentId = unitOfMeasurment.Id;
            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetUnitOfMeasurments(value);
        }

        #region IUnitOfMeasurmentListView Members

        public int UnitOfMeasurmentId
        {
            get { throw new NotImplementedException(); }
        }

        public IList<UnitOfMeasurment> unitOfMeasurmentlist
        {
            set { _unitOfMeasurment = value; }
        }

        public string value
        {
            get { return txtUOM.Text; }
        }

        #endregion
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/Configuration/UnitOfMeasurmentEdit.aspx?{0}=4", AppConstants.TABID));
        }
        protected void grvUOMList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvUOMList.PageIndex = e.NewPageIndex;
            grvUOMList.DataSource = _unitOfMeasurment;
            grvUOMList.DataBind();
        }
}
}