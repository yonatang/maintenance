﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class UnitOfMeasurmentEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IUnitOfMeasurmentEditView
    {
        private UnitOfMeasurmentEditPresenter _presenter;
        private UnitOfMeasurment _unitOfMeasurment;
       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();


            if (Id != 0)
            {
                if (!Page.IsPostBack)
                {
                    BindRegionControls();
                }
            }


        }

        [CreateNew]
        public UnitOfMeasurmentEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        private void BindRegionControls()
        {

            this.txtUOM.Text = _unitOfMeasurment.Name;
            this.txtDescription.Text = _unitOfMeasurment.Description;
            this.btnDelete.Visible = (_unitOfMeasurment.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }

        UnitOfMeasurment IUnitOfMeasurmentEditView._UnitOfMeasurment
        {
            get { return _unitOfMeasurment; }
            set { _unitOfMeasurment = value; }
        }
        protected void  btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _unitOfMeasurment.Name = txtUOM.Text;
                _unitOfMeasurment.Description = txtDescription.Text;
                SaveUnitOfMeasurment();

            }	        
        }

        private void SaveUnitOfMeasurment()
        {
            try
            {
                if (_unitOfMeasurment.IsNew())
                {
                    _presenter.SaveOrUpdateUnitOfMeasurment(_unitOfMeasurment);
                    Master.TransferMessage(new AppMessage("Unit Of Measurment saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateUnitOfMeasurment(_unitOfMeasurment);
                    Master.ShowMessage(new AppMessage("Unit Of Measurment saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {
                // Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }




        #region IUnitOfMeasurmentEditView Members

        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["unitOfMeasurmentId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["unitOfMeasurmentId"]);
                else
                    return 0;

            }
        }

        #endregion
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteUnitOfMeasurment(_unitOfMeasurment.Id);
                Master.ShowMessage(new AppMessage("Node was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
}
}

