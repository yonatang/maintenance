﻿ 
<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ConsumableList.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.View.ConsumableList" %>

 
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
   <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Find Consumable</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">       
      
                    <asp:Label ID="Label2" runat="server" Text="Consumable Name" CssClass="label"></asp:Label>
             <label class="input">
                    <asp:TextBox ID="txtConsumableName" runat="server"></asp:TextBox></label>
                    </section>
                          </div>
                          </fieldset>
                          <footer>

                     <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click" Cssclass="btn btn-primary">Add New Consumable</asp:LinkButton>
                    <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click" Cssclass="btn btn-primary"/>
            </footer>
        </div>
                                </div>
                                </div>
        <asp:GridView ID="grvConsumbaleList" runat="server" AutoGenerateColumns="False" 
            CellPadding="3" EnableModelValidation="True" ForeColor="#333333" 
            GridLines="Horizontal" onrowdatabound="grvConsumbaleList_RowDataBound" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
            style="text-align: left" Width="100%" 
             onpageindexchanging="grvConsumbaleList_PageIndexChanging" 
             AllowPaging="True">
         
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Consumables" />
                <asp:BoundField DataField="Description" HeaderText="Description" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplEdit" runat="server" Text="Edit"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            
            <PagerStyle  Cssclass="paginate_button active" HorizontalAlign="Center" />
           
        </asp:GridView>
   
   
                
              
</asp:Content>

