﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class RegionList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IRegionListView
    {
        private RegionListPresenter _presenter;
        IList<Region> _region = null;
        User _user;
        int _regionId;
        System.Web.UI.Control editsite;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();
            //EditSite1.SiteRegionId = _regionId;
            grvRegionList.DataSource = _region;
            grvRegionList.DataBind();

            if (_user.RegionId == 1000)
            {
                lnkAddNew.Visible = true;
            }
        }

        [CreateNew]
        public RegionListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        public int RegionId
        {
            get { throw new NotImplementedException(); }
        }

        public System.Collections.Generic.IList<CoreDomain.Configuration.Region> regionlist
        {
            set { _region = value; }
        }


        public int findby
        {
            get { return Convert.ToInt32(ddlSelectBy.SelectedValue); }
        }

        public string value
        {
            get { return txtValue.Text; }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetRegions(findby, value);
        }
        protected void grvRegionList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Region region = e.Row.DataItem as Region;
            if (region != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/RegionEdit.aspx?{0}=4&{1}={2}", AppConstants.TABID, AppConstants.REGIONID, region.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
                _regionId = region.Id;
            }



        }

        protected void grvRegionList_SelectedIndexChanged(object sender, EventArgs e)
        {

            Response.Redirect(String.Format("~/Configuration/EditSite.aspx?{0}=4&RegionId={1}&RegionName={2}&RegionCode={3}", AppConstants.TABID, Convert.ToString(grvRegionList.SelectedDataKey["Id"]), Convert.ToString(grvRegionList.SelectedDataKey["RegionName"]), Convert.ToString(grvRegionList.SelectedDataKey["RegionCode"])));



        }


        protected void Page_PreInit(object sender, EventArgs e)
        {
            editsite = LoadControl("EditSite.ascx");
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/Configuration/RegionEdit.aspx?{0}=4", AppConstants.TABID));
        }


        public User user
        {
            get
            {
                return _user;
            }
            set
            {
                _user = value;
            }
        }
        protected void grvRegionList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvRegionList.PageIndex = e.NewPageIndex;
            grvRegionList.DataSource = _region;
            grvRegionList.DataBind();
        }
}
}