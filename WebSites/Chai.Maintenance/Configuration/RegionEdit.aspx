﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="RegionEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.RegionEdit" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    
    <asp:ValidationSummary ID="vlsummery" runat="server" HeaderText="Error Message" class="alert alert-danger fade in"
        ValidationGroup="1" />
        
        <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Region Form </h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">
									
                                        <asp:Label ID="lblRegionName" runat="server" Text="Region Name" class="label"></asp:Label>
                                        <label class="input">
                                           <asp:TextBox ID="txtRegionName" runat="server"></asp:TextBox>
                                           <asp:RequiredFieldValidator ID="rfvRegion" runat="server" ErrorMessage="Region Name Required" ValidationGroup="1" ControlToValidate="txtRegionName" Display="Dynamic">*</asp:RequiredFieldValidator>
                                       </label>
                                    </section>
                                    <section class="col col-6">
                                        <asp:Label ID="lblRegionCode" runat="server" class="label">Region Code</asp:Label>
                                          <label class="input">
                                             <asp:TextBox ID="txtRegionCode" runat="server"></asp:TextBox></label>
                                    </section>
                                    </div>
                                    <div class="row">
                                  <section class="col col-6">
                                 <asp:Label ID="lblMapId" runat="server" Text="Map Id" class="label"></asp:Label>
                                   <label class="input">
                                  <asp:TextBox ID="txtmapId" runat="server" ></asp:TextBox></label>
                                 </section>
                                 </div>
                                 </fieldset>
                                 <footer>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" ValidationGroup="1" Cssclass="btn btn-primary" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Cssclass="btn btn-primary" onclick="btnCancel_Click" />
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" Cssclass="btn btn-primary" onclick="btnDelete_Click" />
                                </footer>
                                 </div>
                                </div>
                                </div>
                                </div>
</asp:Content>

