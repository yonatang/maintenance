﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class ManufacturerList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IManufacturerListView
    {
        private ManufacturerListPresenter _presenter;
        IList<Manufacturer> _Manufacturer = null;
        int _ManufacturerId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();

            grvManufacturerList.DataSource = _Manufacturer;
            grvManufacturerList.DataBind();
        }

        [CreateNew]
        public ManufacturerListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }




        protected void grvManufacturerList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Manufacturer manufacturer = e.Row.DataItem as Manufacturer;
            if (manufacturer != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/ManufacturerEdit.aspx?{0}=4&manufacturerId={1}", AppConstants.TABID, manufacturer.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
                _ManufacturerId = manufacturer.Id;
            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetManufacturers(value);
        }

        #region IManufacturerListView Members

        public int ManufacturerId
        {
            get { throw new NotImplementedException(); }
        }

        public IList<Manufacturer> manufacturerlist
        {
            set { _Manufacturer = value; }
        }

        public string value
        {
            get { return txtManufacturer.Text; }
        }

        #endregion
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/Configuration/ManufacturerEdit.aspx?{0}=4", AppConstants.TABID));
        }
        protected void grvManufacturerList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvManufacturerList.PageIndex = e.NewPageIndex;
            grvManufacturerList.DataSource = _Manufacturer;
            grvManufacturerList.DataBind();
        }
}
}