﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;
using System.IO;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class InstrumentTypeList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IInstrumentTypeListView
    {
        InstrumentTypeListPresenter _presenter;
        IList<InstrumentType> _instrumentlist;
        IList<InstrumentCategory> _InstCategory;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
                BindCategory();
            }
            this._presenter.OnViewLoaded();
            grvInstrumentType.DataSource = _instrumentlist;
            grvInstrumentType.DataBind();
           
        }
        [CreateNew]
        public InstrumentTypeListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        private void BindCategory()
        {
            _InstCategory = _presenter.GetInstrumentCatgory();
            ddlCatagory.DataSource = _InstCategory;
            ddlCatagory.DataBind();

        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetInstrumentTypes(findby, value, ddlvalue);
        }
        
        protected void grvInstrumentType_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            InstrumentType instrumenttype = e.Row.DataItem as InstrumentType;
            if (instrumenttype != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
               
                string url = String.Format("~/Configuration/InstrumentTypeEdit.aspx?{0}=4&InstrumentTypeId={1}", AppConstants.TABID, instrumenttype.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
                
            }

        }

        public int InstrumentTypeId
        {
            get { throw new NotImplementedException(); }
        }

        public IList<InstrumentType> InstrumentTypelist
        {
            set { _instrumentlist = value; }
        }

        public int findby
        {
            get { return Convert.ToInt32(ddlSelectBy.SelectedValue); }
        }

        public string value
        {
            get { return txtValue.Text; }
        }

        public int ddlvalue
        {
            get {
                if (ddlCatagory.SelectedValue != null)
                    return Convert.ToInt32(ddlCatagory.SelectedValue);
                else
                    return 0;
            }
        }
        protected void ddlSelectBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlSelectBy.SelectedValue) == 1)
            {
                ddlCatagory.Visible = true;
                txtValue.Visible = false;
                lblValue.Visible = false;
                ddlCatagory.Visible = true;
                inlineCatagory.Visible = true;
                lblCatagory.Visible = true;
            }
            else if (Convert.ToInt32(ddlSelectBy.SelectedValue) == 2)
            {
                lblValue.Visible = true;
                txtValue.Visible = true;
                ddlCatagory.Visible = false;
                inlineCatagory.Visible = false;
                lblCatagory.Visible = false;
            }

        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("InstrumentTypeEdit.aspx?{0}=4",AppConstants.TABID));
        }
        protected void grvInstrumentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            InstrumentType instrumenttype = _instrumentlist[grvInstrumentType.SelectedIndex];
            
            if (instrumenttype.FileName != "")
            {
                string Filename = instrumenttype.FileName;

                int FileSize = instrumenttype.FileSize;
                Byte[] FileData = instrumenttype.InstrumentService;// new Byte[FileSize];

               
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                context.Response.Clear();
                context.Response.ClearHeaders();
                context.Response.ClearContent();
                context.Response.AppendHeader("content-length", FileData.Length.ToString());
                context.Response.ContentType = GetMimeTypeByFileName(Filename);
                context.Response.AppendHeader("content-disposition", "attachment; filename=" + Filename);
                context.Response.BinaryWrite(FileData);
                            
                context.ApplicationInstance.CompleteRequest();
            }
        }

        public string GetMimeTypeByFileName(string sFileName)
        {
            string sMime = "application/octet-stream";

            string sExtension = System.IO.Path.GetExtension(sFileName);
            if (!string.IsNullOrEmpty(sExtension))
            {
                sExtension = sExtension.Replace(".", "");
                sExtension = sExtension.ToLower();

                if (sExtension == "xls" || sExtension == "xlsx")
                {
                    sMime = "application/ms-excel";
                }
                else if (sExtension == "doc" || sExtension == "docx")
                {
                    sMime = "application/msword";
                }
                else if (sExtension == "ppt" || sExtension == "pptx")
                {
                    sMime = "application/ms-powerpoint";
                }
                else if (sExtension == "rtf")
                {
                    sMime = "application/rtf";
                }
                else if (sExtension == "zip")
                {
                    sMime = "application/zip";
                }
                else if (sExtension == "mp3")
                {
                    sMime = "audio/mpeg";
                }
                else if (sExtension == "bmp")
                {
                    sMime = "image/bmp";
                }
                else if (sExtension == "gif")
                {
                    sMime = "image/gif";
                }
                else if (sExtension == "jpg" || sExtension == "jpeg")
                {
                    sMime = "image/jpeg";
                }
                else if (sExtension == "png")
                {
                    sMime = "image/png";
                }
                else if (sExtension == "tiff" || sExtension == "tif")
                {
                    sMime = "image/tiff";
                }
                else if (sExtension == "txt")
                {
                    sMime = "text/plain";
                }
            }

            return sMime;
        }
        protected void grvInstrumentType_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvInstrumentType.PageIndex = e.NewPageIndex;
            grvInstrumentType.DataSource = _instrumentlist;
            grvInstrumentType.DataBind();
        }
}
}