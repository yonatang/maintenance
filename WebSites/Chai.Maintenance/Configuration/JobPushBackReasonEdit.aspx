﻿ 
<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="JobPushBackReasonEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.JobPushBackReasonEdit" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <asp:ValidationSummary ID="vlsummery" runat="server" ValidationGroup="1" class="alert alert-danger fade in"
        HeaderText="Error Message" />
     
     <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Pushback Reason </h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
  
    

  
        <asp:Label ID="lblReason" runat="server" Text="Reason" CssClass="label"></asp:Label>
       <label class="textarea">

   
        <asp:TextBox ID="txtReason" runat="server" ValidationGroup="1" Height="59px" 
            TextMode="MultiLine" ></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            Display="Dynamic" ErrorMessage="Reason Required" 
            ValidationGroup="1" ControlToValidate="txtReason">*</asp:RequiredFieldValidator>
        </label>
                  </section>
                  </div>
    </fieldset>
    <footer>
    <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="1" 
        onclick="btnSave_Click" Cssclass="btn btn-primary"/>
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
        onclick="btnCancel_Click" Cssclass="btn btn-primary"/>
    <asp:Button ID="btnDelete" runat="server" Text="Delete" 
        onclick="btnDelete_Click" Cssclass="btn btn-primary"/>
        </footer>
      
                                </div>
                                </div>
                                </div>
                                </div>

</asp:Content>

