﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="InstrumentList.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.InstrumentList" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
     <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Find Instrument</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">       
                   <asp:Label ID="lblFindby" runat="server" Text="Find By" CssClass="label"></asp:Label>
                     <label class="select">
                        <asp:DropDownList ID="ddlSelectBy" runat="server"  
                            AutoPostBack="True" onselectedindexchanged="ddlSelectBy_SelectedIndexChanged"> 
                                          
                            <asp:ListItem Value="0">Select By</asp:ListItem>
                            <asp:ListItem Value="1">Serial No.</asp:ListItem>
                            <asp:ListItem Value="2">Lot Number</asp:ListItem>
                            <asp:ListItem Value="3">Instrument Name</asp:ListItem>
                            <asp:ListItem Value="4">Site</asp:ListItem>
                        </asp:DropDownList><i></i></label>
                   </section>
                   <section class="col col-6">
                        <asp:Label ID="lblValue" runat="server" Text="Value" CssClass="label" Visible="false"></asp:Label>
                 <label class="input">
                        <asp:TextBox ID="txtValue" runat="server" Visible="False" ></asp:TextBox></label>
                     </section>
                      </div>
                       <div class="row">
                     <section class="col col-6">  
                        <asp:Label ID="lblinstrument" runat="server" Text="Instrument" CssClass="label" Visible="false"></asp:Label>
                        <label class="select">
                        <asp:DropDownList ID="ddlInstrumentName" runat="server" DataTextField="InstrumentName" 
                            DataValueField="Id" Visible="False" 
                            AppendDataBoundItems="True" >
                            <asp:ListItem Value="0">Select Instrument Name</asp:ListItem>
                        </asp:DropDownList><i id="inlineInstrument" runat="server" visible="False"></i></label>
                          </section>
                        <section class="col col-6">  
                        <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label" Visible="false"></asp:Label>
                        <label class="select">
                      
                        <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                            onselectedindexchanged="ddlRegion_SelectedIndexChanged" Visible="False" 
                            >
                            <asp:ListItem Value="0">Select Region</asp:ListItem>
                        </asp:DropDownList><i id="inlineRegion" runat="server" visible="False"></i></label>
                        <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label" Visible="false"></asp:Label>
                        <label class="select">
                        <asp:DropDownList ID="ddlSite" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                            Visible="False">
                            <asp:ListItem Value="0">Select Site</asp:ListItem>
                        </asp:DropDownList><i id="inlinesite" runat="server" visible="False"></i></label>
                        </section>
                        </div>
                          </fieldset>
                          <footer>
                                           
                        <asp:LinkButton ID="lnkNew" runat="server" onclick="lnkNew_Click" Cssclass="btn btn-primary">Add New Instrument</asp:LinkButton>
                        <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" Cssclass="btn btn-primary"/>
                     </footer>
                     </div>
                                </div>
                                </div>
              
            
            <asp:GridView ID="grvInstrumentList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True" 
                    ForeColor="#333333" GridLines="Horizontal" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    onrowdatabound="grvInstrumentList_RowDataBound" 
                Width="100%" AllowPaging="True" 
                onpageindexchanging="grvInstrumentList_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument Name" />
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="LotNumber" HeaderText="Lot Number" />
                    <asp:BoundField DataField="SerialNo" HeaderText="SerialNo." />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
              <PagerStyle Cssclass="paginate_button active" HorizontalAlign="Center" />
                
             
            </asp:GridView>
       
        </div>
                
</asp:Content>

