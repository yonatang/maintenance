﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class JobPushBackReasonEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IJobPushBackReasonEditView
    {
        private JobPushBackReasonEditPresenter _presenter;
        private JobPushBackReason _jobPushBackReason;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();


            if (Id != 0)
            {
                if (!Page.IsPostBack)
                {
                    BindControls();
                }
            }
        }
        [CreateNew]
        public JobPushBackReasonEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindControls()
        {

            this.txtReason.Text = _jobPushBackReason.Reason;

            this.btnDelete.Visible = (_jobPushBackReason.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _jobPushBackReason.Reason = txtReason.Text;

                SaveJobPushBackReason();

            }
        }
        private void SaveJobPushBackReason()
        {
            try
            {
                if (_jobPushBackReason.IsNew())
                {
                    _presenter.SaveOrUpdateJobPushBackReason(_jobPushBackReason);
                    Master.TransferMessage(new AppMessage("Reason saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateJobPushBackReason(_jobPushBackReason);
                    Master.ShowMessage(new AppMessage("Reason saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteJobPushBackReason(_jobPushBackReason.Id);
                Master.ShowMessage(new AppMessage("Node was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }

        #region IJobPushBackReasonEditView Members

        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["jobPushBackReasonId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["jobPushBackReasonId"]);
                else
                    return 0;

            }
        }
        public JobPushBackReason JobPushBackReason
        {
            get
            {
                return _jobPushBackReason;
            }
            set
            {
                _jobPushBackReason = value;
            }
        }

        #endregion
}
}