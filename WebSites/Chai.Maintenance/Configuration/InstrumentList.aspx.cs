﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class InstrumentList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IInstrumentListView
    {
        private InstrumentListPresenter _presenter;
        IList<Instrument> _instrumentList;
        IList<Site> _siteList;
        IList<Region> _regionlist;
        IList<InstrumentType> _instrumentTypeList;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
               this._presenter.OnViewInitialized();
               BindRegion();
            }
            this._presenter.OnViewLoaded();
            
            BindInstrumentType();
           
        }
        [CreateNew]
        public InstrumentListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindRegion()
        {
          _regionlist =  _presenter.GetRegionList();
          ddlRegion.DataSource = _regionlist;
          ddlRegion.DataBind();
        }
        private void BindInstrumentType()
        {
            _instrumentTypeList = _presenter.GetInstrumenType();
            ddlInstrumentName.DataSource = _instrumentTypeList;
            ddlInstrumentName.DataBind();
        }
        public int InstrumentId
        {
            get { return Convert.ToInt32(ddlInstrumentName.SelectedValue); }
        }

        public IList<Instrument> instrumentlist
        {
            set { _instrumentList = value; }
        }

        public int findby
        {
            get { return Convert.ToInt32(ddlSelectBy.SelectedValue); }
        }

        public string value
        {
            get { return txtValue.Text; }
        }

        public int ddlsiteId
        {
            get { return Convert.ToInt32(ddlSite.SelectedValue); }
        }

        public int ddlinstrumentId
        {
            get { return Convert.ToInt32(ddlInstrumentName.SelectedValue); }
        }

        public IList<Site> SiteList
        {
            set { _siteList = value; }
        }

        public IList<InstrumentType> InstrumentNameList
        {
            set { _instrumentTypeList = value; }
        }
        
        protected void grvInstrumentList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Instrument instrument = e.Row.DataItem as Instrument;
            if (instrument != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/InstrumentEdit.aspx?{0}=4&InstrumentId={1}&SiteId={2}", AppConstants.TABID, instrument.Id,instrument.SiteId);

                hpl.NavigateUrl = this.ResolveUrl(url);

            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
          _presenter.GetInstruments(Convert.ToInt32(ddlSelectBy.SelectedValue),txtValue.Text,Convert.ToInt32(ddlInstrumentName.SelectedValue),Convert.ToInt32(ddlSite.SelectedValue));
          grvInstrumentList.DataSource = _instrumentList;
          grvInstrumentList.DataBind();
        }
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            _siteList = _presenter.GetSiteList(Convert.ToInt32(ddlRegion.SelectedValue));
            if (ddlSite.Items.Count > 0)
            {
                ddlSite.Items.Clear();
            }
                ListItem lst = new ListItem();
                lst.Text = "Select Site";
                lst.Value = "0";
                ddlSite.Items.Add(lst);
            

            
            ddlSite.DataSource = _siteList;
            ddlSite.DataBind();
        }
        protected void ddlSelectBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlSelectBy.SelectedValue) == 1 || Convert.ToInt32(ddlSelectBy.SelectedValue) == 2)
            {
                lblValue.Visible = true;
                txtValue.Visible = true;
                lblinstrument.Visible = false;
                ddlInstrumentName.Visible = false;
                inlineInstrument.Visible = false;
                lblRegion.Visible = false;
                ddlRegion.Visible = false;
                inlineRegion.Visible = false;
                lblSite.Visible = false;
                ddlSite.Visible = false;
                inlinesite.Visible = false;
            }
            else if (Convert.ToInt32(ddlSelectBy.SelectedValue) == 3)
            {
                lblValue.Visible = false;
                txtValue.Visible = false;
                lblinstrument.Visible = true;
                ddlInstrumentName.Visible = true;
                inlineInstrument.Visible = true;
                lblRegion.Visible = false;
                ddlRegion.Visible = false;
                inlineRegion.Visible = false;
                lblSite.Visible = false;
                ddlSite.Visible = false;
                inlinesite.Visible = false;
            }
            else if (Convert.ToInt32(ddlSelectBy.SelectedValue) == 4)
            {
                lblValue.Visible = false;
                txtValue.Visible = false;
                lblinstrument.Visible = false;
                ddlInstrumentName.Visible = false;
                inlineInstrument.Visible = false;
                lblRegion.Visible = true;
                ddlRegion.Visible = true;
                inlineRegion.Visible = true;
                lblSite.Visible = true;
                ddlSite.Visible = true;
                inlinesite.Visible = true;
            }
        }
        protected void lnkNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/Configuration/SiteList.aspx?{0}=4", AppConstants.TABID));
        }
        protected void grvInstrumentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            _presenter.GetInstruments(Convert.ToInt32(ddlSelectBy.SelectedValue), txtValue.Text, Convert.ToInt32(ddlInstrumentName.SelectedValue), Convert.ToInt32(ddlSite.SelectedValue));
            grvInstrumentList.PageIndex = e.NewPageIndex;
            grvInstrumentList.DataSource = _instrumentList;
            grvInstrumentList.DataBind();
        }
}
}