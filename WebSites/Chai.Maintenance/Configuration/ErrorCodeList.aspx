﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ErrorCodeList.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.ErrorCodeList" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
  <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Find Error Code</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">       

                    <asp:Label ID="Label2" runat="server" Text="Error Code" CssClass="label"></asp:Label>
                    <label class="input">
                    <asp:TextBox ID="txtErrorCode" runat="server"></asp:TextBox></label>
                    </section>
                <section class="col col-6">       
                    <asp:Label ID="Label4" runat="server" Text="Problem Type" CssClass="label"></asp:Label>
                <label class="select">
                    <asp:DropDownList ID="ddlProblemType" runat="server" 
                        AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                        >
                        <asp:ListItem Value="0">Select Problem Type</asp:ListItem>
                    </asp:DropDownList><i></i></label>
                    </section>
                     </div>
                          
                          </fieldset>
                          <footer>
                      <asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click" Cssclass="btn btn-primary">Add New Error Code</asp:LinkButton>
                    <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click" Cssclass="btn btn-primary"/>
                </footer>
                     </div>
                                </div>
                                </div>
       
        <asp:GridView ID="grvErrorCodeList" runat="server" AutoGenerateColumns="False" 
            CellPadding="3" EnableModelValidation="True" ForeColor="#333333" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass="alt"
            GridLines="Horizontal" onrowdatabound="grvErrorCodeList_RowDataBound" 
            style="text-align: left" Width="100%" 
            onpageindexchanging="grvErrorCodeList_PageIndexChanging" 
            AllowPaging="True">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="ProblemTypeName" HeaderText="Problem Type" />
                <asp:BoundField DataField="Name" HeaderText="Error Code" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplEdit" runat="server" Text="Edit"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            
            <PagerStyle ForeColor="White" HorizontalAlign="Center" />
        
           
        </asp:GridView>


   

  
           
</asp:Content>


