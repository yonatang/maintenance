﻿ 

<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ManufacturerList.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.ManufacturerList" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
 

<asp:Content ID="Content1" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Find Manufacturer</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">       
          
        
       
                    <asp:Label ID="Label2" runat="server" Text="Manufacturer" CssClass="label"></asp:Label>
               <label class="input">
                    <asp:TextBox ID="txtManufacturer" runat="server" CssClass="textbox"></asp:TextBox>
                                 </label>
                                   </section>
                          </div>
                          </fieldset>
                          <footer>
                     <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click" Cssclass="btn btn-primary">Add New Manufacture</asp:LinkButton>
                    <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click" Cssclass="btn btn-primary"/>
                  
                   </footer>
                     </div>
                                </div>
                                </div>
       
   
    
    <asp:GridView ID="grvManufacturerList" runat="server" CellPadding="3" 
        EnableModelValidation="True" ForeColor="#333333" GridLines="Horizontal" 
                         CssClass="table table-striped table-bordered table-hover"  
                         PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
        Width="100%" AutoGenerateColumns="False" 
        onrowdatabound="grvManufacturerList_RowDataBound" AllowPaging="True" 
                         onpageindexchanging="grvManufacturerList_PageIndexChanging">
       
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Manufacturer" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink ID="hplEdit" runat="server" Text="Edit"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
       
        <PagerStyle Cssclass="paginate_button active" HorizontalAlign="Center" />
       
     
    </asp:GridView>
    
   </div>
                 
        
</asp:Content>