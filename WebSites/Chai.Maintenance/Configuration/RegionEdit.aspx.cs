﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{

    public partial class RegionEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IEditRegionView
{
    private RegionEditPresenter _presenter;
    private Region _region;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this._presenter.OnViewInitialized();
           
            
        }
        this._presenter.OnViewLoaded();

       
        if (Id != 0)
        {
            if (!Page.IsPostBack)
            {
                BindRegionControls();
            }
        }
        
       
    }
    [CreateNew]
    public RegionEditPresenter Presenter
    {
        get
        {
            return this._presenter;
        }
        set
        {
            if (value == null)
                throw new ArgumentNullException("value");

            this._presenter = value;
            this._presenter.View = this;
        }
    }
    private void BindRegionControls()
    {
        
            this.txtRegionName.Text = _region.RegionName;
            this.txtRegionCode.Text = _region.RegionCode;
            this.txtmapId.Text = _region.Mapid;
            this.btnDelete.Visible = (_region.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");

        
    }
   
      public string GetRegionName
    {
        get { return this.txtRegionName.Text; }
    }

    public string GetRegionCode
    {
        get { return this.txtRegionCode.Text; }
    }
    public string GetMapId
    {
        get { return this.txtmapId.Text; }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (this.IsValid)
        {
            _region.RegionName = txtRegionName.Text;
            _region.RegionCode = txtRegionCode.Text;
            _region.Mapid = txtmapId.Text;
            SaveRegion();
            
        }	         
    }
    private void SaveRegion()
    {
        try
        {
            if (_region.IsNew())
            {
                _presenter.SaveOrUpdateRegion(_region);
               Master.TransferMessage(new AppMessage("Region saved", RMessageType.Info));
                _presenter.CancelPage();
            }
            else
            {
                _presenter.SaveOrUpdateRegion(_region);
                Master.ShowMessage(new AppMessage("Region saved", RMessageType.Info));
                _presenter.CancelPage();
            }
        }
        catch (Exception ex)
        {
           // Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        _presenter.CancelPage();
    }

    public int Id
    {
        get
        {
            if (Convert.ToInt32(Request.QueryString["RegionId"]) != 0)
                return Convert.ToInt32(Request.QueryString["RegionId"]);
            else
                return 0;
        
        }
    }
    
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            _presenter.DeleteRegion(_region.Id);
            Master.ShowMessage(new AppMessage("Node was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
            _presenter.CancelPage();
        }
        catch (Exception ex)
        {
            Master.ShowMessage(new AppMessage("Erro: Unable to delete node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
        }
    }
    


    public CoreDomain.Configuration.Region region
    {
        
        set
        {
            _region= value;
        }
    }
}

}