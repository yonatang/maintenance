﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{

    public partial class SiteList : Microsoft.Practices.CompositeWeb.Web.UI.Page,ISiteListView
    {
        private SiteListPresenter _presenter;
        IList<Site> _SiteList;
        IList<Region> _RegionList;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
                BindRegion();
            }
            this._presenter.OnViewLoaded();
            grvSiteList.DataSource = _SiteList;
            grvSiteList.DataBind();
           
        }

        private void BindRegion()
        {
            ddlRegion.DataSource = _presenter.GetRegion();
            ddlRegion.DataBind();
        }
        [CreateNew]
        public SiteListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        protected void grvSiteList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Site site = e.Row.DataItem as Site;
            if (site != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/EditSite.aspx?{0}=4&SiteId={1}&RegionId={2}", AppConstants.TABID, site.Id,site.RegionId);

                hpl.NavigateUrl = this.ResolveUrl(url);
                
            }



        }

       
        public int SiteId
        {
            get { throw new NotImplementedException(); }
        }

        public System.Collections.Generic.IList<Site> sitelist
        {
            set { _SiteList = value; }
        }

        public int findby
        {
            get { return Convert.ToInt32(ddlSelectBy.SelectedValue); }
        }

        public string value
        {
            get { return txtValue.Text; }
        }

        public int ddlValue
        {
            get { return Convert.ToInt32(ddlRegion.SelectedValue); }
        }
        protected void ddlSelectBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlSelectBy.SelectedValue) == 1)
            {
                txtValue.Visible = false;
                ddlRegion.Visible = true;
                lblValue.Visible = false;
                lblRegion.Visible = true;
                inlineRegion.Visible = true;
            }
            else
            {
                txtValue.Visible = true;
                ddlRegion.Visible = false;
                lblValue.Visible = true;
                lblRegion.Visible = false;
                inlineRegion.Visible = false;
            }

        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
           _SiteList = _presenter.GetSites(Convert.ToInt32(ddlSelectBy.SelectedValue), txtValue.Text, Convert.ToInt32(ddlRegion.SelectedValue));
            grvSiteList.DataSource = _SiteList;
            grvSiteList.DataBind();
        }


        public IList<Region> RegionList
        {
            set { _RegionList = value; }
        }
        protected void lnkNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/Configuration/RegionList.aspx?{0}=4",AppConstants.TABID));
        }
        protected void grvSiteList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvSiteList.PageIndex = e.NewPageIndex;
            grvSiteList.DataSource = _SiteList;
            grvSiteList.DataBind();
        }
        
        protected void grvSiteList_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView gv = sender as GridView;

            Response.Redirect(String.Format("InstrumentEdit.aspx?SiteId={0}&SiteName={1}&SiteCode={2}&{3}=4", Convert.ToInt32(gv.SelectedDataKey["Id"]), _SiteList[gv.SelectedIndex].Name, _SiteList[gv.SelectedIndex].Code, AppConstants.TABID));
            grvSiteList.Enabled = false;
        }
        protected void grvSiteList_PageIndexChanging1(object sender, GridViewPageEventArgs e)
        {
            grvSiteList.PageIndex = e.NewPageIndex;
            grvSiteList.DataSource = _SiteList;
            grvSiteList.DataBind();
        }
}
}