﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="SiteList.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.SiteList" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
       <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Find Site</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">       
         
                        <asp:Label ID="lblFindby" runat="server" Text="Find By" CssClass="label"></asp:Label>
                    <label class="select">
                        <asp:DropDownList ID="ddlSelectBy" runat="server" 
                            AutoPostBack="True" 
                            onselectedindexchanged="ddlSelectBy_SelectedIndexChanged" CssClass="textbox">
                            <asp:ListItem Value="0">Select By</asp:ListItem>
                            <asp:ListItem Value="1">Region</asp:ListItem>
                            <asp:ListItem Value="2">Site Name</asp:ListItem>
                            <asp:ListItem Value="3">Site Code</asp:ListItem>
                        </asp:DropDownList><i></i></label>
                    </section>
                        <section class="col col-6">
                        <asp:Label ID="lblValue" runat="server" Text="Value" CssClass="label" Visible="False"></asp:Label>
                    <label class="input">
                        <asp:TextBox ID="txtValue" runat="server" Visible="False" CssClass="textbox"></asp:TextBox></label>
                     </section>
                      </div>
                      <div class="row">
                     <section class="col col-6">  
                     <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label" Visible="False"></asp:Label>
                              <label class="select">
                        <asp:DropDownList ID="ddlRegion" runat="server" DataTextField="RegionName" 
                            DataValueField="Id" Visible="False" 
                            AppendDataBoundItems="True" CssClass="textbox">
                            <asp:ListItem Value="-1">Select Region</asp:ListItem>
                        </asp:DropDownList><i id="inlineRegion" runat="server" visible="False"></i></label>
                          </section>
                        
                        </div>
                          
                          </fieldset>
                          <footer>
                         <asp:LinkButton ID="lnkNew" runat="server" onclick="lnkNew_Click" Cssclass="btn btn-primary">Add New Site</asp:LinkButton>
                        <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" Cssclass="btn btn-primary"/>
                  </footer>
                     </div>
                                </div>
                                </div>
            
             <asp:GridView ID="grvSiteList" runat="server" DataKeyNames="Id" AllowPaging="True" 
                 AlternatingRowStyle-CssClass="" AutoGenerateColumns="False" CellPadding="3" 
                 CssClass="table table-striped table-bordered table-hover"
                 EnableModelValidation="True" GridLines="Horizontal" 
                 onrowdatabound="grvSiteList_RowDataBound" 
                 onselectedindexchanged="grvSiteList_SelectedIndexChanged" 
                 PagerStyle-CssClass="paginate_button active" style="text-align: left" 
                         onpageindexchanging="grvSiteList_PageIndexChanging1" >
                 <Columns>
                     <asp:BoundField DataField="Name" HeaderText="Name" />
                    <asp:BoundField DataField="Code" HeaderText="Code" />
                    <asp:BoundField DataField="City" HeaderText="City" />
                    <asp:BoundField DataField="Telephone1" HeaderText="Telephone 1" />
                    <asp:BoundField DataField="Telephone2" HeaderText="Telephone 2" />
                    <asp:BoundField DataField="Email" HeaderText="Email" />
                     <asp:TemplateField>
                         <ItemTemplate>
                             <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField></asp:TemplateField>
                     <asp:CommandField SelectText="Add Instrument" ShowSelectButton="True" />
                 </Columns>
                 <PagerStyle Cssclass="paginate_button active" HorizontalAlign="Center" />
              
             </asp:GridView>
           </div>
                        
                   
</asp:Content>

