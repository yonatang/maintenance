﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;
namespace Chai.Maintenance.Modules.Configuration.Views
{
public partial class SparepartTypeList : Microsoft.Practices.CompositeWeb.Web.UI.Page, ISparepartTypeListView
{
    private SparepartTypeListPresenter _presenter;
    IList<SparepartType> _spareparttypelist = null;
    int _spareparttypeId;
    IList<InstrumentType> _instrumenttypelist;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this._presenter.OnViewInitialized();
            BindInstrumentType();
        }
        this._presenter.OnViewLoaded();
        grvSparepartTypeList.DataSource = _spareparttypelist;
        grvSparepartTypeList.DataBind();
        
    }
    private void BindInstrumentType()
    {
      _instrumenttypelist = _presenter.GetInstrumentList();
      ddlValue.DataSource = _instrumenttypelist;
      ddlValue.DataBind();
     
     
    }
    [CreateNew]
    public SparepartTypeListPresenter Presenter
    {
        get
        {
            return this._presenter;
        }
        set
        {
            if (value == null)
                throw new ArgumentNullException("value");

            this._presenter = value;
            this._presenter.View = this;
        }
    }
    protected void btnFind_Click(object sender, EventArgs e)
    {
        _presenter.GetSparepartTypes(Convert.ToInt32(ddlSelectBy.SelectedValue), txtValue.Text, Convert.ToInt32(ddlValue.SelectedValue));

    }
    protected void grvRegionList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        SparepartType region = e.Row.DataItem as SparepartType;
        if (region != null)
        {
            HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
            string url = String.Format("~/Configuration/SparepartTypEdit.aspx?{0}=4&SparepartTypeId={1}", AppConstants.TABID, region.Id);

            hpl.NavigateUrl = this.ResolveUrl(url);
            
        }
    }



    public int SpareparttypeId
    {
        get { throw new NotImplementedException(); }
    }

    public IList<SparepartType> SparepartTypelist
    {
        set { _spareparttypelist = value; }
    }

    public int findby
    {
        get { return Convert.ToInt32(ddlSelectBy.SelectedValue);}
    }

    public string value
    {
        get { return txtValue.Text; }
    }

    public int ddlvalue
    {

        get
        {
            //if (ddlValue.SelectedValue != null)
                return Convert.ToInt32(ddlValue.SelectedValue);
            
        }
    }
    protected void ddlSelectBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ddlSelectBy.SelectedValue) == 1 || Convert.ToInt32(ddlSelectBy.SelectedValue) == 2)
        {
            txtValue.Visible = true;
            ddlValue.Visible = false;
            lblValue.Visible = true;
            lblinstrument.Visible = false;
            inlineinstrument.Visible = false;
        }
        if (Convert.ToInt32(ddlSelectBy.SelectedValue) == 3)
        {
            txtValue.Visible = false;
            ddlValue.Visible = true;
            lblValue.Visible = false;
            lblinstrument.Visible = true;
            inlineinstrument.Visible = true;
        }

    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect(String.Format("~/Configuration/SparepartTypEdit.aspx?{0}=4", AppConstants.TABID));
    }
    protected void grvSparepartTypeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvSparepartTypeList.PageIndex = e.NewPageIndex;
        grvSparepartTypeList.DataSource = _spareparttypelist;
        grvSparepartTypeList.DataBind();
    }
}
}