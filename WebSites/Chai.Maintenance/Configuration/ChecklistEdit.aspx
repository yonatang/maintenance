﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ChecklistEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.View.ChecklistEdit" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <asp:ValidationSummary ID="vlsummery" runat="server" HeaderText="Error Message" class="alert alert-danger fade in"
        ValidationGroup="1" />
  <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Check list Form </h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">
        <asp:Label ID="Label2" runat="server" Text="Instrument" CssClass="label"></asp:Label>
       <label class="select">
        <asp:DropDownList ID="ddlInstrument" runat="server" AppendDataBoundItems="True" 
            DataTextField="InstrumentName" DataValueField="Id" >
            <asp:ListItem Value="0">Select Instrument</asp:ListItem>
        </asp:DropDownList><i></i>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ControlToValidate="ddlInstrument" ErrorMessage="Select Instrument" 
            ValidationGroup="1">*</asp:RequiredFieldValidator></label>
      </section>
      <section class="col col-6">
        <asp:Label ID="lblConsumable" runat="server" Text="Checklist" CssClass="label"></asp:Label>
       <label class="textarea">
        <asp:TextBox ID="txtChecklist" runat="server" ValidationGroup="1" 
             TextMode="MultiLine" 
            ></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            Display="Dynamic" ErrorMessage="Checklist Required" 
            ValidationGroup="1" ControlToValidate="txtChecklist">*</asp:RequiredFieldValidator></label>
      </section>
      </div>
      </fieldset>
   <footer>
    
    <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" Cssclass="btn btn-primary" ValidationGroup="1" />
    <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" Cssclass="btn btn-primary" Text="Cancel" />
    <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" Cssclass="btn btn-primary" Text="Delete" />
              </footer>
                                 </div>
                                </div>
                                </div>
                                </div>
</asp:Content>

