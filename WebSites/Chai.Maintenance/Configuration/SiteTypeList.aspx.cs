﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class SiteTypeList : Microsoft.Practices.CompositeWeb.Web.UI.Page, ISiteTypeListView
    {
        private SiteTypeListPresenter _presenter;
        IList<SiteType> _siteType = null;
        int _siteTypeId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();

            grvSiteTypeList.DataSource = _siteType;
            grvSiteTypeList.DataBind();
        }

        [CreateNew]
        public SiteTypeListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }




        protected void grvSiteTypeList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SiteType siteType = e.Row.DataItem as SiteType;
            if (siteType != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/SiteTypeEdit.aspx?{0}=4&siteTypeId={1}", AppConstants.TABID, siteType.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
                _siteTypeId = siteType.Id;
            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetSiteTypes(value);
        }

        #region ISiteTypeListView Members

        public int SiteTypeId
        {
            get { throw new NotImplementedException(); }
        }

        public IList<SiteType> siteTypelist
        {
            set { _siteType = value; }
        }

        public string value
        {
            get { return txtSiteType.Text; }
        }

        #endregion
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/Configuration/SiteTypeEdit.aspx?{0}=4", AppConstants.TABID));
        }
        protected void grvSiteTypeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvSiteTypeList.PageIndex = e.NewPageIndex;
            grvSiteTypeList.DataSource = _siteType;
            grvSiteTypeList.DataBind();
        }
}
}