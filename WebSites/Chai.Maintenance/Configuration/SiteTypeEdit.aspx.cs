﻿using System;
using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class SiteTypeEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, ISiteTypeEditView
    {

        private SiteTypeEditPresenter _presenter;
        private SiteType _SiteType;

      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();


            if (Id != 0)
            {
                if (!Page.IsPostBack)
                {
                    BindRegionControls();
                }
            }


        }
        [CreateNew]
        public SiteTypeEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindRegionControls()
        {

            this.txtSiteType.Text = _SiteType.Name;

            this.btnDelete.Visible = (_SiteType.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            
            if (this.IsValid)
            {
                _SiteType.Name = txtSiteType.Text;

                SaveSiteType();

            }
        }
        private void SaveSiteType()
        {
            try
            {
                if (_SiteType.IsNew())
                {
                    _presenter.SaveOrUpdateSiteType(_SiteType);
                    Master.TransferMessage(new AppMessage("Site Type saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateSiteType(_SiteType);
                    Master.ShowMessage(new AppMessage("Site Type saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {

            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteSiteType(_SiteType.Id);
                Master.ShowMessage(new AppMessage("Node was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }

        #region ISiteTypeEditView Members

        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["siteTypeId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["siteTypeId"]);
                else
                    return 0;

            }
        }

        SiteType ISiteTypeEditView._SiteType
        {
           
            get
            {
                return _SiteType;
            }
            set
            {
                _SiteType = value;
            }
        }

        #endregion
    }
}