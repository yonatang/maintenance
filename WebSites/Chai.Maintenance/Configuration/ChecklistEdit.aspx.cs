﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Chai.Maintenance.Modules.Configuration.Views;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.View
{
    public partial class ChecklistEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IChecklistTaskEditView
    {
        private ChecklistTaskEditPresenter _presenter;
        private ChecklistTask _checklistTask;
        IList<InstrumentType> _instrumentTypeList;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();



            if (Id != 0)
                if (!Page.IsPostBack)
                    BindControls();
            BindInstrumentType();
        }
        [CreateNew]
        public ChecklistTaskEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        void BindInstrumentType()
        {
            if (!IsPostBack)
            {
                _instrumentTypeList = _presenter.GetInstrumenttype();
                ddlInstrument.DataSource = _instrumentTypeList;
                ddlInstrument.DataBind();

            }
        }
        private void BindControls()
        {

            this.txtChecklist.Text = _checklistTask.Name;
            this.ddlInstrument.SelectedValue = _checklistTask.InstrumentLookupId.ToString();

            this.btnDelete.Visible = (_checklistTask.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }
        private void SaveChecklist()
        {
            try
            {
                if (_checklistTask.IsNew())
                {
                    _presenter.SaveOrUpdateChecklistTask(_checklistTask);
                    Master.TransferMessage(new AppMessage("Checklist saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateChecklistTask(_checklistTask);
                    Master.ShowMessage(new AppMessage("Checklist saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {

            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _checklistTask.Name = txtChecklist.Text;
                _checklistTask.InstrumentLookupId = Convert.ToInt32(ddlInstrument.SelectedValue);
                SaveChecklist();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteChecklistTask(_checklistTask.Id);
                Master.ShowMessage(new AppMessage("Node was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Error: Unable to delete node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }

        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ChecklistId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ChecklistId"]);
                else
                    return 0;

            }
        }

        public ChecklistTask ChecklistTask
        {
            get
            {
                return _checklistTask;
            }
            set
            {
                _checklistTask = value;
            }
        }
    }
}