﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Chai.Maintenance.Modules.Configuration.Views;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.View
{
    public partial class ConsumableEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IConsumableEditView
    {

        private ConsumableEditPresenter _presenter;
        private Consumables _consumables;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();



            if (Id != 0)
                if (!Page.IsPostBack)
                    BindControls();
 
        }

        [CreateNew]
        public ConsumableEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindControls()
        {

            this.txtConsumable.Text = _consumables.Name;
            this.txtDesc.Text = _consumables.Description;

            this.btnDelete.Visible = (_consumables.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }

        #region interface

        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["consumbaleId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["consumbaleId"]);
                else
                    return 0;

            }
        }

        public Consumables Consumables
        {
            get
            {
                return _consumables;
            }
            set
            {
                _consumables = value;
            }
        }
        #endregion


        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _consumables.Name = txtConsumable.Text;
                _consumables.Description = txtDesc.Text;
                SaveConsumable();
            }
        }
        private void SaveConsumable()
        {
            try
            {
                if (_consumables.IsNew())
                {
                    _presenter.SaveOrUpdateConsumable(_consumables);
                    Master.TransferMessage(new AppMessage("Consumables saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateConsumable(_consumables);
                    Master.ShowMessage(new AppMessage("Consumables saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {

            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteConsumable(_consumables.Id);
                Master.ShowMessage(new AppMessage("Node was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
}
}
