﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;

namespace Chai.Maintenance.Modules.Configuration.View
{
    public partial class ConsumableList : Page, IConsumableListView
    {
        private ConsumableListPresenter _presenter;
        private IList<Consumables> _consumable = null;
        private int _consumableId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();

            grvConsumbaleList.DataSource = _consumable;
            grvConsumbaleList.DataBind();
        }

        [CreateNew]
        public ConsumableListPresenter Presenter
        {
            get { return this._presenter; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        #region interface

        public int ConsumableId
        {
            get { throw new NotImplementedException(); }
        }

        public IList<Consumables> Consumablelist
        {
            set { _consumable = value; }
        }

        public string Value
        {
            get { return txtConsumableName.Text; }
        }

        #endregion
        protected void grvConsumbaleList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Consumables _consumables = e.Row.DataItem as Consumables;
            if (_consumables != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/ConsumableEdit.aspx?{0}=4&consumbaleId={1}", AppConstants.TABID, _consumables.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
                _consumableId = _consumables.Id;
            }
        }
        protected void grvConsumbaleList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvConsumbaleList.PageIndex = e.NewPageIndex;
            grvConsumbaleList.DataSource = _consumable;
            grvConsumbaleList.DataBind();
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetConsumables(Value);
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("ConsumableEdit.aspx?{0}=4",AppConstants.TABID));
        }
}

}