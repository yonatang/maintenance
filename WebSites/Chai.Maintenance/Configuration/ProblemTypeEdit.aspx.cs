﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class ProblemTypeEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IProblemTypeEditView
    {
        private ProblemTypeEditPresenter _presenter;
        private ProblemType _problemType;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();


            if (Id != 0)
                if (!Page.IsPostBack)
                    BindControls();
        }
        [CreateNew]
        public ProblemTypeEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindControls()
        {
            this.txtProblemType.Text = _problemType.Name;

            this.btnDelete.Visible = (_problemType.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");
        }
        private void SaveProblemType()
        {
            try
            {
                if (_problemType.IsNew())
                {
                    _presenter.SaveOrUpdateProblemType(_problemType);
                    Master.TransferMessage(new AppMessage("Problem Type saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateProblemType(_problemType);
                    Master.ShowMessage(new AppMessage("Problem Type saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {

            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _problemType.Name = txtProblemType.Text;
                SaveProblemType();
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteProblemType(_problemType.Id);
                Master.ShowMessage(new AppMessage("Node was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }

        public int Id
        {
            get 
            {
                if (Convert.ToInt32(Request.QueryString["ProblemTypeId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ProblemTypeId"]);
                else
                    return 0;
            }
        }

        public ProblemType _ProblemType
        {
            get
            {
                return _problemType;
            }
            set
            {
                _problemType = value;
            }
        }
    }
}