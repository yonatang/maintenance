﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="SparepartTypEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.SparepartTypEdit" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>


<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <asp:ValidationSummary ID="SparepartErrorSummary" runat="server" 
        HeaderText="Error" Height="67px" ValidationGroup="1" class="alert alert-danger fade in"/>
    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Sparepart Type</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
  
                <asp:Label ID="lblCatagory" runat="server" Text="Instrument" CssClass="label"></asp:Label>
            <label class="select">
                <asp:DropDownList ID="ddlCatagory" runat="server" 
                    AppendDataBoundItems="True" DataTextField="InstrumentName" 
                    DataValueField="Id" >
                    <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                </asp:DropDownList><i></i>

                <asp:RequiredFieldValidator ID="rfvcatagory" runat="server" 
                    ControlToValidate="ddlCatagory" Display="Dynamic" 
                    ErrorMessage="Catagory Required" InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator></label>
                </section>
               <section class="col col-6">   
                <asp:Label ID="lblUom" runat="server" Text="Unit of Measurment" 
                    CssClass="label"></asp:Label>
           <label class="select">
                <asp:DropDownList ID="ddlUom" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    >
                    <asp:ListItem Value="0">Select Unit Of Meaurement</asp:ListItem>
                </asp:DropDownList><i></i></label>
          </section>
          </div>
          <div class="row">
									<section class="col col-6">   
                <asp:Label ID="lblName" runat="server" Text="Sparepart Name" CssClass="label"></asp:Label>
            <label class="input">
                <asp:TextBox ID="txtName" runat="server" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvName" runat="server" 
                    ControlToValidate="txtName" ErrorMessage="Sparepart Name Required " 
                    ValidationGroup="1">*</asp:RequiredFieldValidator></label>
           </section>
           <section class="col col-6">   
                <asp:Label ID="lblReorderQty" runat="server" Text="Reorder Qty" 
                    CssClass="label"></asp:Label>
            <label class="input">
                <asp:TextBox ID="txtQty" runat="server" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvQty" runat="server" 
                    ControlToValidate="txtQty" ErrorMessage="Reorder Qty Required" 
                    ValidationGroup="1">*</asp:RequiredFieldValidator></label>
          </section>
          </div>
           <div class="row">
           <section class="col col-6"> 
                <asp:Label ID="lblpartNo" runat="server" Text="Part Number" CssClass="label"></asp:Label>
            <label class="input">
                <asp:TextBox ID="txtpartno" runat="server" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvPartNo" runat="server" 
                    ControlToValidate="txtpartno" Display="Dynamic" 
                    ErrorMessage="Part Number Required" ValidationGroup="1">*</asp:RequiredFieldValidator></label>
                    </section>
            <section class="col col-6"> 
                <asp:Label ID="lblDescription" runat="server" Text="Description" 
                    CssClass="label"></asp:Label>
        <label class="textarea">
                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" ></asp:TextBox></label>
             </section>
             </div>
             </fieldset>
             <footer>
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" 
                    ValidationGroup="1" Cssclass="btn btn-primary"/>
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    onclick="btnCancel_Click" Cssclass="btn btn-primary"/>
                <asp:Button ID="btnDelete" runat="server" Text="Delete" 
                    onclick="btnDelete_Click" Cssclass="btn btn-primary"/>
           </footer>
      
                                </div>
                                </div>
                                </div>
                                </div>

</asp:Content>

