﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class ErrorCodeList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IErrorCodeListView
    {
        private ErrorCodeListPresenter _presenter;
        IList<ErrorCode> _errorCode = null;
        private int _errorCodeId;
        IList<ProblemType> _problemtype;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();

            grvErrorCodeList.DataSource = _errorCode;
            grvErrorCodeList.DataBind();
            BindProblemType();
        }
        [CreateNew]
        public ErrorCodeListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindProblemType()
        {
            if (!IsPostBack)
            {
                _problemtype = _presenter.GetProblemType();
                ddlProblemType.DataSource = _problemtype;
                ddlProblemType.DataBind();
            }
        }
        protected void grvErrorCodeList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            ErrorCode errorCode = e.Row.DataItem as ErrorCode;
            if (errorCode != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/ErrorCodeEdit.aspx?{0}=4&ErrorCodeId={1}", AppConstants.TABID, errorCode.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
                _errorCodeId = errorCode.Id;
            }
        }
        protected void grvErrorCodeList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvErrorCodeList.PageIndex = e.NewPageIndex;
            grvErrorCodeList.DataSource = _errorCode;
            grvErrorCodeList.DataBind();
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetErrorCodes(ErrorCode, ProblemTypeId);
        }

        public int ErrorCodeId
        {
            get { throw new NotImplementedException(); }
        }

        public IList<ErrorCode> errorCodelist
        {
            set { _errorCode = value; }
        }

        public string ErrorCode
        {
            get { return txtErrorCode.Text; }
        }
        public int ProblemTypeId
        {

            get { return Convert.ToInt32(ddlProblemType.SelectedValue); }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("ErrorCodeEdit.aspx?{0}=4",AppConstants.TABID));
        }
}
}