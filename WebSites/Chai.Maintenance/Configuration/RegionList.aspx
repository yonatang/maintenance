﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="RegionList.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.RegionList" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register src="EditSite.ascx" tagname="EditSite" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
<div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Find Region</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">
                                     <Label  class="label">Find By</Label>
                                     <label class="select">
                                           <asp:DropDownList ID="ddlSelectBy" runat="server">
                                           <asp:ListItem Value="0">Select</asp:ListItem>
                                           <asp:ListItem Value="1">Region Name</asp:ListItem>
                                           <asp:ListItem Value="2">Region Code</asp:ListItem>
                                          </asp:DropDownList><i></i></label> 
                                          </section>
                        
                             <section class="col col-6">      
                                     <Label  class="label">Value</Label>
                                     <label class="input">
                                    <asp:TextBox ID="txtValue" runat="server" ></asp:TextBox></label>
                            </section>
                             
                            </div>
                                </fieldset>
                                 <footer>
                                 <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click" Cssclass="btn btn-primary" Visible="False">Add New Region</asp:LinkButton>
                                    <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" Cssclass="btn btn-primary"/>
                                     
                            </footer>
                            </div>
                                </div>
                                </div>
                                <asp:GridView ID="grvRegionList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id,RegionName,RegionCode" EnableModelValidation="True" 
                    GridLines="Horizontal" 
                         CssClass="table table-striped table-bordered table-hover" 
                         PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    onrowdatabound="grvRegionList_RowDataBound" style="text-align: left" AllowPaging="True" 
                    onselectedindexchanged="grvRegionList_SelectedIndexChanged" 
                         onpageindexchanging="grvRegionList_PageIndexChanging">
                    
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Id" />
                        <asp:BoundField DataField="RegionName" HeaderText="Region Name" />
                        <asp:BoundField DataField="RegionCode" HeaderText="Region Code" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField SelectText="Add Site" ShowSelectButton="True" />
                    </Columns>
                    
                    <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
                    
                </asp:GridView>
                                </div>
              

                

</asp:Content>

