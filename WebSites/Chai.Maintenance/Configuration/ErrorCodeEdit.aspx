﻿ 

<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ErrorCodeEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.ErrorCodeEdit" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <asp:ValidationSummary ID="vlsummery" runat="server" HeaderText="Error Message" class="alert alert-danger fade in"
        ValidationGroup="1" />
    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Error Code</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">
				
        <asp:Label ID="lblVendor" runat="server" Text="Error Code Name" 
            CssClass="label"></asp:Label>
         <label class="input">
        <asp:TextBox ID="txtErrorCode" runat="server" ValidationGroup="1" 
            ></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            Display="Dynamic" ErrorMessage="Error Code Name Required" 
            ValidationGroup="1" ControlToValidate="txtErrorCode">*</asp:RequiredFieldValidator></label>
      </section>
       <section class="col col-6">
        <asp:Label ID="Label2" runat="server" Text="Problem Type" CssClass="label"></asp:Label>
         <label class="select">
        <asp:DropDownList ID="ddlproblemType" runat="server" 
            AppendDataBoundItems="True" ValidationGroup="1" 
            DataTextField="Name" DataValueField="Id" >
            <asp:ListItem Value="0">Select Problem Type</asp:ListItem>
        </asp:DropDownList><i></i>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ErrorMessage="Select Problem Type" ValidationGroup="1" 
            ControlToValidate="ddlproblemType">*</asp:RequiredFieldValidator></label>
       </section>
    </div>
       </fieldset>
       <footer>
    
     <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" 
        ValidationGroup="1" Cssclass="btn btn-primary"/>
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
        onclick="btnCancel_Click" Cssclass="btn btn-primary"/>
    <asp:Button ID="btnDelete" runat="server" Text="Delete" 
        onclick="btnDelete_Click" Cssclass="btn btn-primary"/>
  </footer>
      
                                </div>
                                </div>
                                </div>
</asp:Content>
