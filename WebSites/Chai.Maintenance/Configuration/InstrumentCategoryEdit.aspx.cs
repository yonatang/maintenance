﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class InstrumentCategoryEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IInstrumentCategoryEditView
    {
        private InstrumentCategoryEditPresenter _presenter;
        private InstrumentCategory _instrumentCategory;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();


            if (Id != 0)
            {
                if (!Page.IsPostBack)
                {
                    BindRegionControls();
                }
            }


        }

        [CreateNew]
        public InstrumentCategoryEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        private void BindRegionControls()
        {

            this.txtInstCategory.Text = _instrumentCategory.Name;
            this.txtDescription.Text = _instrumentCategory.Description;
            this.btnDelete.Visible = (_instrumentCategory.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }

        InstrumentCategory IInstrumentCategoryEditView._InstrumentCategory
        {
            get { return _instrumentCategory; }
            set { _instrumentCategory = value; }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _instrumentCategory.Name = txtInstCategory.Text;
                _instrumentCategory.Description = txtDescription.Text;
                SaveInstrumentCategory();

            }
        }

        private void SaveInstrumentCategory()
        {
            try
            {
                if (_instrumentCategory.IsNew())
                {
                    _presenter.SaveOrUpdateInstrumentCategory(_instrumentCategory);
                    Master.TransferMessage(new AppMessage("Instrument Category saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateInstrumentCategory(_instrumentCategory);
                    Master.ShowMessage(new AppMessage("Instrument Category saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }




        #region IInstrumentCategoryEditView Members

        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["instrumentCategoryId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["instrumentCategoryId"]);
                else
                    return 0;

            }
        }

        #endregion
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteInstrumentCategory(_instrumentCategory.Id);
                Master.ShowMessage(new AppMessage("Node was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }

        #region IInstrumentCategoryEditView Members


        public InstrumentCategory _InstrumentCategory
        {
            get
            {
                return _instrumentCategory;
            }
            set
            {
                _instrumentCategory = value;
            }
        }

        #endregion


        protected void btnCancel_Click1(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
}
}

