﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="InstrumentCategoryList.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.InstrumentCategoryList" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

     <asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
 <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Find Instrument Category</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">       
                     <asp:Label ID="Label2" runat="server" Text="Instrument Category" 
                         CssClass="label"></asp:Label>
                 <label class="input">
                     <asp:TextBox ID="txtInstCat" runat="server" ></asp:TextBox></label>
                       </section>
                          </div>
                          </fieldset>
                          <footer>
                       <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click" Cssclass="btn btn-primary">Add New Instrument Catagory</asp:LinkButton>
                     <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click" Cssclass="btn btn-primary"/>
                 </footer>
                     </div>
                                </div>
                                </div>
    <asp:GridView ID="grvInstCatList" runat="server" CellPadding="2" 
        EnableModelValidation="True" ForeColor="#333333" GridLines="Horizontal"  
                         CssClass="table table-striped table-bordered table-hover"  
                         PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
        Width="100%" AutoGenerateColumns="False" 
        onrowdatabound="grvInstCatList_RowDataBound" style="text-align: left" AllowPaging="True" 
                         onpageindexchanging="grvInstCatList_PageIndexChanging">
        
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Instrument Category" />
            <asp:BoundField DataField="Description" HeaderText="Description" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink ID="hplEdit" runat="server" Text="Edit"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        
         <PagerStyle  Cssclass="paginate_button active" HorizontalAlign="Center" />
    </asp:GridView>
    
     </div>
    
               
           
 </asp:Content>