﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class ManufacturerEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IManufacturerEditView
    {
        private ManufacturerEditPresenter _presenter;
        private Manufacturer _manufacturer;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();


            if (Id != 0)
            {
                if (!Page.IsPostBack)
                {
                    BindControls();
                }
            }


        }

        [CreateNew]
        public ManufacturerEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        private void BindControls()
        {

            this.txtManufacturer.Text = _manufacturer.Name;

            this.btnDelete.Visible = (_manufacturer.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }

        Manufacturer IManufacturerEditView._Manufacturer
        {
            get { return _manufacturer; }
            set { _manufacturer = value; }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _manufacturer.Name = txtManufacturer.Text;
                
                SaveManufacturer();

            }
        }

        private void SaveManufacturer()
        {
            try
            {
                if (_manufacturer.IsNew())
                {
                    _presenter.SaveOrUpdateManufacturer(_manufacturer);
                    Master.TransferMessage(new AppMessage("Manufacturer saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateManufacturer(_manufacturer);
                    Master.ShowMessage(new AppMessage("Manufacturer saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {
               Master.ShowMessage(new AppMessage(ex.Message,RMessageType.Error));
            }
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }




        #region IManufacturerEditView Members

        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["manufacturerId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["manufacturerId"]);
                else
                    return 0;

            }
        }
        public Manufacturer _Manufacturer
        {
            get
            {
                return _manufacturer;
            }
            set
            {
                _manufacturer = value;
            }
        }

        #endregion
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteManufacturer(_manufacturer.Id);
                Master.ShowMessage(new AppMessage("Node was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }




 
}
}

