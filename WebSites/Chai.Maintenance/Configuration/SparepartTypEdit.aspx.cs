﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class SparepartTypEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, ISparepartTypeEditView
    {

        private SparepartTypeEditPresenter _presenter;
        private SparepartType _spareparttype;
        private IList<InstrumentType> _InstrumentType;
        private IList<UnitOfMeasurment> _UnitOfMeasurment;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();


            if (Id != 0)
            {
                if (!Page.IsPostBack)
                {
                    BindRegionControls();
                }
            }
            BindInstrumentLookUp();
            BindUnitOfMeasurment();
        
        }
        [CreateNew]
        public SparepartTypeEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindInstrumentLookUp()
        {
            _InstrumentType = _presenter.GetInstrumentTypeList();
            ddlCatagory.DataSource = _InstrumentType;
            ddlCatagory.DataBind();


        }
        private void BindUnitOfMeasurment()
        {
            _UnitOfMeasurment = _presenter.GetUnitOfMeasurmentList();
            ddlUom.DataSource = _UnitOfMeasurment;
            ddlUom.DataBind();


        }
        private void BindRegionControls()
        {

            this.txtName.Text = _spareparttype.Name;
            this.ddlCatagory.SelectedValue = _spareparttype.InstrumentlookupId.ToString();
            this.txtpartno.Text = _spareparttype.PartNumber;
            this.ddlUom.SelectedValue = _spareparttype.Uom.ToString();
            this.txtQty.Text = _spareparttype.ReorderQty.ToString();
            this.txtDescription.Text = _spareparttype.Description;
            this.btnDelete.Visible = (_spareparttype.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }
        private void SaveSparepartType()
        {
            try
            {
                if (_spareparttype.IsNew())
                {
                    _presenter.SaveOrUpdateSparepartType(_spareparttype);
                    Master.TransferMessage(new AppMessage("Sparepart Type saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateSparepartType(_spareparttype);
                    Master.ShowMessage(new AppMessage("Sparepart Type saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                 _spareparttype.Name = this.txtName.Text;
                 _spareparttype.InstrumentlookupId=Convert.ToInt32(this.ddlCatagory.SelectedValue) ;
                 _spareparttype.PartNumber=this.txtpartno.Text;
               _spareparttype.Uom = Convert.ToInt32(this.ddlUom.SelectedValue);
               _spareparttype.ReorderQty = Convert.ToInt32(this.txtQty.Text);
               _spareparttype.Description=this.txtDescription.Text;
               SaveSparepartType();

            }	 

        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteSparepartType(_spareparttype.Id);
                Master.ShowMessage(new AppMessage("Sparepart Type was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete Sparepart Type . " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }

        }

        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["SparepartTypeId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["SparepartTypeId"]);
                else
                    return 0;

            }
        }

        public string Name
        {
            get { return txtName.Text; }
        }

        public int InstrumentlookupId
        {
            get { return Convert.ToInt32(ddlCatagory.SelectedValue); }
        }

        public string PartNumber
        {
            get { return txtpartno.Text; }
        }

        public string UOM
        {
            get { return ddlUom.SelectedValue; }
        }

        public int ReorderQty
        {
            get { return Convert.ToInt32(txtQty.Text); }
        }

        public string Description
        {
            get { return txtDescription.Text; }
        }

        public SparepartType spareparttype
        {
            set { _spareparttype = value; }
        }
    }
}