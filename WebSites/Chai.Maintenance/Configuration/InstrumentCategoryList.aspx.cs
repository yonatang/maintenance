﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class InstrumentCategoryList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IInstrumentCategoryListView
    {
        private InstrumentCategoryListPresenter _presenter;
        IList<InstrumentCategory> _instrumentCategory = null;
        int _instrumentCategoryId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();

            grvInstCatList.DataSource = _instrumentCategory;
            grvInstCatList.DataBind();
        }

        [CreateNew]
        public InstrumentCategoryListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }




        protected void grvInstCatList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            InstrumentCategory instrumentCategory = e.Row.DataItem as InstrumentCategory;
            if (instrumentCategory != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/InstrumentCategoryEdit.aspx?{0}=4&instrumentCategoryId={1}", AppConstants.TABID, instrumentCategory.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
                _instrumentCategoryId = instrumentCategory.Id;
            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetInstrumentCategorys(value);
        }

        #region IInstrumentCategoryListView Members

        public int InstrumentCategoryId
        {
            get { throw new NotImplementedException(); }
        }

        public IList<InstrumentCategory> instrumentCategorylist
        {
            set { _instrumentCategory = value; }
        }

        public string value
        {
            get { return txtInstCat.Text; }
        }

        #endregion
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/Configuration/InstrumentCategoryEdit.aspx?{0}=4", AppConstants.TABID));
        }
        protected void grvInstCatList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvInstCatList.PageIndex = e.NewPageIndex;
            grvInstCatList.DataSource = _instrumentCategory;
            grvInstCatList.DataBind();
        }
}
}