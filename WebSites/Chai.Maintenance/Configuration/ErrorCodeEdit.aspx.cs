﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class ErrorCodeEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IErrorCodeEditView
    {

        private ErrorCodeEditPresenter _presenter;
        private ErrorCode _errorCode;
        IList<ProblemType> _problemtype;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();


            if (Id != 0)
                if (!Page.IsPostBack)
                    BindControls();


            BindProblemType();

        }
        [CreateNew]
        public ErrorCodeEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindProblemType()
        {
            if (!IsPostBack)
            {
                _problemtype = _presenter.GetProblemType();
                ddlproblemType.DataSource = _problemtype;
                ddlproblemType.DataBind();
            }
        }
        private void BindControls()
        {
            this.txtErrorCode.Text = _errorCode.Name;
            this.ddlproblemType.SelectedValue = _errorCode.ProblemTypeId.ToString();
            this.btnDelete.Visible = (_errorCode.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");
        }
        private void SaveErrorCode()
        {
            try
            {
                if (_errorCode.IsNew())
                {
                    _presenter.SaveOrUpdateErrorCode(_errorCode);
                    Master.TransferMessage(new AppMessage("Error Code saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateErrorCode(_errorCode);
                    Master.ShowMessage(new AppMessage("Error Code saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _errorCode.Name = txtErrorCode.Text;
                _errorCode.ProblemTypeId = Convert.ToInt32(ddlproblemType.SelectedValue);
                SaveErrorCode();

            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteErrorCode(_errorCode.Id);
                Master.ShowMessage(new AppMessage("Node was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }

        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ErrorCodeId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ErrorCodeId"]);
                else
                    return 0;

            }
        }

        public ErrorCode _ErrorCode
        {
            get
            {
                return _errorCode;
            }
            set
            {
                _errorCode = value;
            }
        }
    }
}