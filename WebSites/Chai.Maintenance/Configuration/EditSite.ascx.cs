﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Modules.Configuration;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.Configuration.Views
{

    public partial class EditSite : Microsoft.Practices.CompositeWeb.Web.UI.UserControl, IEditSiteView
    {
        private SiteEditPresenter _presenter;
        private Site _site;
        private IList<Site> _siteList;
        private int _siteId;
        public static int regionId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();

            grvRegionList.DataSource = _siteList;
            grvRegionList.DataBind();


            if (SiteId != 0)
            {
                if (!Page.IsPostBack)
                {
                    BindRegionControls();
                }
            }

        }
        public int SiteRegionId
        {

            set
            {
                regionId = value;

            }
        }
        [CreateNew]
        public SiteEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }


        Site IEditSiteView._site
        {
            get { return _site; }
            set { _site = value; }
        }

        public int RegionId
        {
            get
            {
                if (regionId != 0)
                    return regionId;
                else
                    return 0;

            }
        }
        private void BindRegionControls()
        {

            txtSiteName.Text = _site.Name;
            txtSiteCode.Text = _site.Code;
            txtCity.Text = _site.City;
            ddlSiteType.SelectedValue = _site.typeId.ToString();
            txtAddress.Text = _site.Address;
            txtTel1.Text = _site.Telephone1;
            txttel2.Text = _site.Telephone2;
            txtEmail.Text = _site.Email;
            this.btnDelete.Visible = (_site.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            _site.Name = txtSiteName.Text;
            _site.RegionId = RegionId;
            _site.Code = txtSiteCode.Text;
            _site.City = txtCity.Text;
            _site.typeId = Convert.ToInt32(ddlSiteType.SelectedValue);
            _site.Address = txtAddress.Text;
            _site.Telephone1 = txtTel1.Text;
            _site.Telephone2 = txttel2.Text;
            _site.Email = txtEmail.Text;
            SaveSite();


        }

        private void SaveSite()
        {
            try
            {
                if (_site.IsNew())
                {
                    _presenter.SaveOrUpdateSite(_site);
                    //Master.TransferMessage(new AppMessage("Region saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateSite(_site);
                    //Master.ShowMessage(new AppMessage("Region saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {
                // Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteSite(_site);
                //Master.ShowMessage(new AppMessage("Node was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                //Master.ShowMessage(new AppMessage("Erro: Unable to delete node. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }


        public int SiteId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["SiteId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["SiteId"]);
                else
                    return 0;

            }
            set
            {
                _siteId = value;
            }
        }


        public IList<Site> siteList
        {
            set { _siteList = value; }
        }


        protected void grvRegionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            _siteId = Convert.ToInt32(grvRegionList.SelectedDataKey["Id"]);
        }


        int IEditSiteView.RegionId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["RegionId"]) != null)
                    return Convert.ToInt32(Request.QueryString["RegionId"]);
                else
                    return 0;

            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string RegionName
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["RegioName"]) != null)
                    return Convert.ToString(Request.QueryString["RegioName"]);
                else
                    return "";

            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string RegionCode
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["RegionCode"]) != null)
                    return Convert.ToString(Request.QueryString["RegionCode"]);
                else
                    return "";

            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}