﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class AutoNumberList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IAutoNumberListView
    {
        private AutoNumberListPresenter _presenter;
        IList<AutoNumber> _AutoNumber = null;
        int _AutoNumberId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();

            grvAutoNumberList.DataSource = _AutoNumber;
            grvAutoNumberList.DataBind();
        }

        [CreateNew]
        public AutoNumberListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindPage()
        {

        }
        protected void grvAutoNumberList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            AutoNumber _AutoNumber = e.Row.DataItem as AutoNumber;
            if (_AutoNumber != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/AutoNumberEdit.aspx?{0}=4&AutoNumberId={1}", AppConstants.TABID, _AutoNumber.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
                _AutoNumberId = _AutoNumber.Id;
            }
        }

        public int AutoNumberId
        {
            get { throw new NotImplementedException(); }
        }

        public IList<AutoNumber> _AutoNumberlist
        {
            set { _AutoNumber = value; }
        }

        public int pageId
        {
            get { return Convert.ToInt32(ddlPage.SelectedValue); }
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {

        }
}
}