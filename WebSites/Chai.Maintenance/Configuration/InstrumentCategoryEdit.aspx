﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="InstrumentCategoryEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.InstrumentCategoryEdit" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <asp:ValidationSummary ID="vlsummery" runat="server" ValidationGroup="1" class="alert alert-danger fade in"
        HeaderText="Error Message" />
     <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Instrument Category Form </h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
    
  
        <asp:Label ID="lblInstCat" runat="server" Text="Instrument Category" 
            CssClass="label"></asp:Label>
        <label class="input">
        <asp:TextBox ID="txtInstCategory" runat="server" ValidationGroup="1" 
          ></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            Display="Dynamic" ErrorMessage="Instrument Category Required" 
            ValidationGroup="1" ControlToValidate="txtInstCategory">*</asp:RequiredFieldValidator></label>
         </section>
       <section class="col col-6">
         
        <asp:Label ID="Label3" runat="server" Text="Description" CssClass="label"></asp:Label>
          <label class="textarea">
        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" 
            ></asp:TextBox></label>
        </section>
       </div>
       </fieldset>
       <footer>
    
    <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="1" 
        onclick="btnSave_Click" Cssclass="btn btn-primary" />
    <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
        onclick="btnCancel_Click1" Cssclass="btn btn-primary"/>
    <asp:Button ID="btnDelete" runat="server" Text="Delete" 
        onclick="btnDelete_Click" Cssclass="btn btn-primary"/>
         </footer>
      
                                </div>
                                </div>
                                </div>
                                </div>
</asp:Content>
