﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Modules.Configuration;
using System.Collections.Generic;
using System.IO;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class InstrumentEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IInstrumentEditView
    {
        private InstrumentEditPresenter _presenter;
        private Instrument _instrument;
        private IList<Instrument> _instrumentlist;
        private static int _instrumentid;
        public static int siteid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
                BindManufacturer();
                BindVendor();
                BindContractWith();
                BindInstrumentName();

            }

            this._presenter.OnViewLoaded();

            grvInstrumentList.DataSource = _instrumentlist;
            grvInstrumentList.DataBind();

            if (!Page.IsPostBack)
            {
                _instrumentid = 0;
            }

            if (InstrumentId != 0 && !Page.IsPostBack)
            {

                BindRegionControls();

            }

            lblResultName.Text = SiteName;
            lblresultCode.Text = SiteCode;
          
        }

        private void BindManufacturer()
        {
            ddlManufacturer.DataSource = _presenter.GetManufacturer();
            ddlManufacturer.DataBind();
        }
        private void BindVendor()
        {
            ddlVendor.DataSource = _presenter.GetVendor();
            ddlVendor.DataBind();
        }
        private void BindContractWith()
        {
            ddlContractWith.DataSource = _presenter.GetContractWith();
            ddlContractWith.DataBind();
        }
        private void BindInstrumentName()
        {
            ddlInsName.DataSource = _presenter.GetInstrumentType();
            ddlInsName.DataBind();
        }
        [CreateNew]
        public InstrumentEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }





        public Instrument instrument
        {
            
                 get { return _instrument; }
            set { _instrument = value; }
            
              
        }

        public IList<Instrument> instrumentList
        {
            set { _instrumentlist = value; }
        }

        public int InstrumentId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["InstrumentId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["InstrumentId"]);
                else
                    return _instrumentid;
            }
            set
            {
                _instrumentid = value;
            }
        }

        public int SiteId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["SiteId"]) != 0)
                {
                    siteid = Convert.ToInt32(Request.QueryString["SiteId"]);
                    return siteid;
                }
                else
                    return 0;
            }
            set
            {
                siteid = value;
            }
        }
       
        public string SiteName
        {
            get
            {
                if (Request.QueryString["SiteName"] != string.Empty)
                {

                    lblResultName.Text = Request.QueryString["SiteName"];
                    return lblResultName.Text;
                }
                else
                    return string.Empty;
            }
            set
            {
                lblResultName.Text = value;
            }
        }

        public string SiteCode
        {
            get
            {
                if (Request.QueryString["SiteCode"] != string.Empty)
                {
                    lblresultCode.Text = Request.QueryString["SiteCode"];
                    return lblresultCode.Text;
                }
                else
                    return string.Empty;

            }
            set
            {
                lblresultCode.Text = value;
            }
        }
        private void SaveSite()
        {
            try
            {
                if (_instrument.IsNew())
                {
                    _presenter.SaveOrUpdateInstrument(_instrument);
                    Master.ShowMessage(new AppMessage("Instrument saved", RMessageType.Info));
                    _instrumentlist.Add(_instrument);
                    grvInstrumentList.DataSource = _instrumentlist;
                    grvInstrumentList.DataBind();
                    ClearRegionControls();
                }
                else
                {
                    _presenter.SaveOrUpdateInstrument(_instrument);
                    Master.ShowMessage(new AppMessage("Instrument saved", RMessageType.Info));
                    _presenter.OnViewLoaded();
                    grvInstrumentList.DataSource = _instrumentlist;
                    grvInstrumentList.DataBind();
                    ClearRegionControls();
                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        private void BindRegionControls()
        {
            _presenter.OnViewLoaded();
             ddlInsName.SelectedValue = _instrument.nameId.ToString();
             txtLotNumber.Text= _instrument.LotNumber ;
             txtSerialNo.Text=_instrument.SerialNo ;
             ddlManufacturer.SelectedValue =_instrument.ManufacturerId.ToString();
             Calendar1.Text = _instrument.InstallationDate.ToString();
             ddlVendor.SelectedValue=_instrument.VendorId.ToString();
             SiteId=_instrument.SiteId;
             Calendar2.Text = _instrument.WarranityExpireDate.ToString();
            ddlPeriod.SelectedItem.Text= _instrument.PreventiveMaintenancePeriod ;
            ddlContractWith.SelectedValue=_instrument.ContractwithId.ToString() ;
            chkIsFunctional.Checked = _instrument.IsFunctional;
            chkContract.Checked = _instrument.UnderServiceContract;
            this.btnDelete.Visible = (_instrument.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }
        private void ClearRegionControls()
        {

            ddlInsName.SelectedValue = Convert.ToString(0);
            txtLotNumber.Text = "";
            txtSerialNo.Text = "";
            ddlManufacturer.SelectedValue = Convert.ToString(0);
            Calendar1.Text = "";
            ddlVendor.SelectedValue = Convert.ToString(0);
            Calendar2.Text = ""; 
            ddlContractWith.SelectedValue = Convert.ToString(0);
            chkIsFunctional.Checked =false;
            chkContract.Checked = false;
            _presenter.OnViewInitialized();


        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            _presenter.OnViewLoaded();
            _instrument.nameId = Convert.ToInt32(ddlInsName.SelectedValue);
            _instrument.SiteId = SiteId;
            _instrument.LotNumber= txtLotNumber.Text;
            _instrument.SerialNo = txtSerialNo.Text;
            _instrument.ManufacturerId = Convert.ToInt32(ddlManufacturer.SelectedValue);
            _instrument.InstallationDate = Calendar1.Text != "" ? Convert.ToDateTime(Calendar1.Text) : _instrument.InstallationDate;
            _instrument.VendorId = Convert.ToInt32(ddlVendor.SelectedValue);
            _instrument.SiteId = SiteId;
            _instrument.WarranityExpireDate = Calendar2.Text != "" ? Convert.ToDateTime(Calendar2.Text) : _instrument.WarranityExpireDate;
            _instrument.PreventiveMaintenancePeriod = ddlPeriod.SelectedItem.Text;
            _instrument.ContractwithId = Convert.ToInt32(ddlContractWith.SelectedValue);
            _instrument.IsFunctional = chkIsFunctional.Checked;
            _instrument.UnderServiceContract = chkContract.Checked;
            _instrument.InstrumentName = ddlInsName.SelectedItem.Text;
            _instrument.LastPreventiveMaintenanceDate = Calendar3.Text != "" ? Convert.ToDateTime(Calendar3.Text) : _instrument.LastPreventiveMaintenanceDate;
            
            SaveSite();
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteInstrument(_instrument.Id);
                Master.ShowMessage(new AppMessage("Instrument was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.OnViewLoaded();
                grvInstrumentList.DataSource = _instrumentlist;
                grvInstrumentList.DataBind();
                InstrumentId = 0;
                //_presenter.NewSite();
                ClearRegionControls();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete Instrument. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }

        protected void grvInstrumentList_SelectedIndexChanged(object sender, EventArgs e)
        {
            _instrumentid = Convert.ToInt32(grvInstrumentList.SelectedDataKey["Id"]);
            _presenter.GetInstrumentById(_instrumentid);
            BindRegionControls();
        }
        protected void grvInstrumentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvInstrumentList.PageIndex = e.NewPageIndex;
            grvInstrumentList.DataSource = _instrumentlist;
            grvInstrumentList.DataBind();
        }
        protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
       
}
}