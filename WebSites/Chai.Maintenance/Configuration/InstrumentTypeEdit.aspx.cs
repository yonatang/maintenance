﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using System.Collections.Generic;
using System.IO;
namespace Chai.Maintenance.Modules.Configuration.Views
{

    public partial class InstrumentTypeEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IInstrumentTypeEditView
    {
        InstrumentTypeEditPresenter _presenter;
        private InstrumentType _instrumenttype;
        IList<InstrumentCategory> _instrumentCategorylist;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();


            if (Id != 0)
            {
                if (!Page.IsPostBack)
                {
                    BindInstrumentTypeControls();
                }
            }
            BindInstrumentCategory();
        }
        private void BindInstrumentCategory()
        {
            _instrumentCategorylist = _presenter.GetInstrumentCategory();
            ddlCatagory.DataSource = _instrumentCategorylist;

            ddlCatagory.DataBind();
        }
        [CreateNew]
        public InstrumentTypeEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindInstrumentTypeControls()
        {

            this.ddlCatagory.SelectedValue = _instrumenttype.InstrumentCatagoryId.ToString();
            this.txtName.Text = _instrumenttype.InstrumentName;
            this.btnDelete.Visible = (_instrumenttype.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }
        public int Id
        {
            get 
        {
            if (Convert.ToInt32(Request.QueryString["InstrumentTypeId"]) != 0)
                return Convert.ToInt32(Request.QueryString["InstrumentTypeId"]);
            else
                return 0;
        
        } 
        }

        public int GetInstrumentCatagoryId
        {
            get { return Convert.ToInt32(ddlCatagory.SelectedValue); }
        }

        public string GetInstrumentname
        {
            get { return txtName.Text; }
        }

        public InstrumentType instrumenttype
        {
            set { _instrumenttype = value; }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _instrumenttype.InstrumentCatagoryId = Convert.ToInt32(this.ddlCatagory.SelectedValue);
                _instrumenttype.InstrumentName = this.txtName.Text;
                if (FileUploadmnuservice.HasFile)
                {
                    _instrumenttype.FileName = FileUploadmnuservice.FileName;
                    _instrumenttype.ContentType = FileUploadmnuservice.PostedFile.ContentType;
                    _instrumenttype.FileSize = FileUploadmnuservice.PostedFile.ContentLength;
                }
                _instrumenttype.InstrumentService = CreateAttachment();
                SaveInstrumentType();

            }	 
        }
        private byte[] CreateAttachment()
        {
            Byte[] FileData;
            if (FileUploadmnuservice.HasFile)
            {
                String Filename = FileUploadmnuservice.FileName;
                String ContentType = FileUploadmnuservice.PostedFile.ContentType;
                int FileSize = FileUploadmnuservice.PostedFile.ContentLength;
                
                FileData = new Byte[FileSize];
                FileUploadmnuservice.FileContent.Read(FileData, 0, FileSize);

                return FileData;
            }
            else 
            {
                return null;
            }
        }
        private void SaveInstrumentType()
        {
            try
            {
                if (_instrumenttype.IsNew())
                {
                    _presenter.SaveOrUpdateInstrumentType(_instrumenttype);
                    Master.TransferMessage(new AppMessage("Instrument Type saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateInstrumentType(_instrumenttype);
                    Master.ShowMessage(new AppMessage("Instrument Type saved", RMessageType.Info));
                    _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteInstrumentType(_instrumenttype.Id);
                Master.ShowMessage(new AppMessage("Instrument Type was deleted seccessfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.CancelPage();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete Instrument Type. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
}
}