﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using System.Collections.Generic;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Practices.CompositeWeb.Web.UI;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public partial class VendorList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IVendorListView
    {
        private VendorListPresenter _presenter;
        IList<Vendor> _vendor = null;
        int _vendorId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();

            grvVendorList.DataSource = _vendor;
            grvVendorList.DataBind();
        }

        [CreateNew]
        public VendorListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }




        protected void grvVendorList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Vendor vendor = e.Row.DataItem as Vendor;
            if (vendor != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/Configuration/VendorEdit.aspx?{0}=4&vendorId={1}", AppConstants.TABID, vendor.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);
                _vendorId = vendor.Id;
            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetVendors(value);
        }

        #region IVendorListView Members

        public int VendorId
        {
            get { throw new NotImplementedException(); }
        }

        public IList<Vendor> vendorlist
        {
            set { _vendor = value; }
        }

        public string value
        {
            get { return txtvendorName.Text; }
        }

        #endregion

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/Configuration/VendorEdit.aspx?{0}=4", AppConstants.TABID));
        }
        protected void grvVendorList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvVendorList.PageIndex = e.NewPageIndex;
            grvVendorList.DataSource = _vendor;
            grvVendorList.DataBind();
        }
}
}