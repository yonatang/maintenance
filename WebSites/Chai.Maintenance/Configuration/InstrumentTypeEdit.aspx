﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="InstrumentTypeEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.InstrumentTypeEdit" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    
    <asp:ValidationSummary ID="ValidationSummary1" runat="server"  class="alert alert-danger fade in"
        ValidationGroup="1" />
   <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Instrument Type Form </h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
    
                <asp:Label ID="lblCatagoryId" runat="server" Text="Instrument Catagory" 
                    CssClass="label"></asp:Label>
            <label class="select">
                <asp:DropDownList ID="ddlCatagory" runat="server" 
                    DataTextField="Name" DataValueField="Id" AppendDataBoundItems="True" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Instrument Catagory</asp:ListItem>
                </asp:DropDownList><i></i>

                <asp:RequiredFieldValidator ID="RfvCatagory" runat="server" 
                    ControlToValidate="ddlCatagory" Display="Dynamic" 
                    ErrorMessage="Catagory Required" InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator></label>
                </section>
                <section class="col col-6">   
                <asp:Label ID="lblInstrumentName" runat="server" Text="Instrument Name" 
                    CssClass="label"></asp:Label>
                 <label class="input">
                <asp:TextBox ID="txtName" runat="server" 
                    style="margin-left: 3px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txtName" Display="Dynamic" ErrorMessage="name Required" 
                    ValidationGroup="1">*</asp:RequiredFieldValidator></label>
                  </section>
                  </div>
                  <div class="row">
			        <section class="col col-6"> 
                  <asp:Label ID="Label1" runat="server" Text="Service Manual"></asp:Label>
                    <label class="input">
                  <asp:FileUpload ID="FileUploadmnuservice" runat="server" /></label>
                  </section>
                  </div>
                  </fieldset>
                   <footer>
                <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" 
                    ValidationGroup="1" Cssclass="btn btn-primary"/>
                &nbsp;
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    onclick="btnCancel_Click" Cssclass="btn btn-primary"/>
          
                <asp:Button ID="btnDelete" runat="server" Text="Delete" 
                    onclick="btnDelete_Click" Cssclass="btn btn-primary" />
            </footer>
      
                                </div>
                                </div>
                                </div>
                                </div>
</asp:Content>

