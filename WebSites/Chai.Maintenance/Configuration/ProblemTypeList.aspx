﻿ 
<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ProblemTypeList.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.Views.ProblemTypeList" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Find Problem Type</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">       
                    <asp:Label ID="Label4" runat="server" Text="Problem Type" CssClass="label"></asp:Label>
                 <label class="input">
                    <asp:TextBox ID="txtProblemtype" runat="server" ></asp:TextBox></label>
             </section>
                          </div>
                          </fieldset>
                          <footer>
                              <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click" Cssclass="btn btn-primary">Add New Problem Type</asp:LinkButton>
                    <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click" Cssclass="btn btn-primary" />
              
          </footer>
        </div>
                                </div>
                                </div>

        <asp:GridView ID="grvProblemTypeList" runat="server" AutoGenerateColumns="False" 
            CellPadding="3" EnableModelValidation="True" ForeColor="#333333" 
            GridLines="Horizontal" onrowdatabound="grvProblemTypeList_RowDataBound"  CssClass="table table-striped table-bordered table-hover"  PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
            style="text-align: left" Width="100%" 
            onpageindexchanging="grvProblemTypeList_PageIndexChanging" 
             AllowPaging="True">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Problem Type" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplEdit" runat="server" Text="Edit"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
           
            <PagerStyle Cssclass="paginate_button active" HorizontalAlign="Center" />
            
         
        </asp:GridView>
    
  </div>
            
         
</asp:Content>
