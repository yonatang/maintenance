﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Modules.Configuration;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.Configuration.Views
{

    public partial class EditSite : Microsoft.Practices.CompositeWeb.Web.UI.Page, IEditSiteView
    {
        private SiteEditPresenter _presenter;
        private Site _site;
        private IList<Site> _siteList;
        private IList<SiteType> _SiteType;
        private static int _siteId;
        public static int regionId = 0;
        private bool issave = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
                BindSiteType();

            }

            this._presenter.OnViewLoaded();

            grvRegionList.DataSource = _siteList;
            grvRegionList.DataBind();
            if (!Page.IsPostBack)
            {
                _siteId = 0;
            }            
            
                if (SiteId != 0 && !Page.IsPostBack)
                {

                    BindRegionControls();

                }
            
            lblGetRegionName.Text = RegionName;
            lblGetRegionCode.Text = RegionCode;
            

        }
        
        [CreateNew]
        public SiteEditPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindSiteType()
        {
            _SiteType = _presenter.GetSiteType();
            ddlSiteType.DataSource = _SiteType;
            ddlSiteType.DataBind();
            ListItem lst = new ListItem();
            lst.Text = "Select Site Type";
            lst.Value = "0";
            ddlSiteType.Items.Add(lst);


        }

        Site IEditSiteView._site
        {
            get { return _site; }
            set { _site = value; }
        }

       
        private void BindRegionControls()
        {
            _presenter.OnViewLoaded();
            txtSiteName.Text = _site.Name;
            txtSiteCode.Text = _site.Code;
            txtCity.Text = _site.City;
            ddlSiteType.SelectedValue = _site.typeId.ToString();
            txtAddress.Text = _site.Address;
            txtTel1.Text = _site.Telephone1;
            txttel2.Text = _site.Telephone2;
            txtEmail.Text = _site.Email;
            this.btnDelete.Visible = (_site.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }
        private void ClearRegionControls()
        {
           
            txtSiteName.Text = "";
            txtSiteCode.Text = "";
            txtCity.Text = "";
            ddlSiteType.SelectedValue = Convert.ToString(0);
            txtAddress.Text = "";
            txtTel1.Text = "";
            txttel2.Text = "";
            txtEmail.Text = "";
          _presenter.OnViewInitialized();
            

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            _presenter.OnViewLoaded();
            _site.Name = txtSiteName.Text;
            _site.RegionId = RegionId;
            _site.Code = txtSiteCode.Text;
            _site.City = txtCity.Text;
            _site.typeId = Convert.ToInt32(ddlSiteType.SelectedValue);
            _site.Address = txtAddress.Text;
            _site.Telephone1 = txtTel1.Text;
            _site.Telephone2 = txttel2.Text;
            _site.Email = txtEmail.Text;
            _site.RegionId = RegionId;
            SaveSite();


        }

        private void SaveSite()
        {
            try
            {
                if (_site.IsNew())
                {
                    _presenter.SaveOrUpdateSite(_site);
                    Master.ShowMessage(new AppMessage("Site saved", RMessageType.Info));
                    _siteList.Add(_site);
                    grvRegionList.DataSource = _siteList;
                    grvRegionList.DataBind();
                    ClearRegionControls();
                    //_presenter.CancelPage();
                }
                else
                {
                    _presenter.SaveOrUpdateSite(_site);
                    _presenter.OnViewLoaded();
                    grvRegionList.DataSource = _siteList;
                    grvRegionList.DataBind();
                    ClearRegionControls();
                    Master.ShowMessage(new AppMessage("Site saved", RMessageType.Info));
                   
                   // _presenter.CancelPage();
                }
            }
            catch (Exception ex)
            {
                 Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteSite(_site);
                Master.ShowMessage(new AppMessage("Site was deleted successfully", Chai.Maintenance.Enums.RMessageType.Info));
                _presenter.OnViewLoaded();
                grvRegionList.DataSource = _siteList;
                grvRegionList.DataBind();
                SiteId = 0;
                //_presenter.NewSite();
                ClearRegionControls();
                
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Erro: Unable to delete Site. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }


        public int SiteId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["SiteId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["SiteId"]);
                else
                    return _siteId;

            }
            set
            {
                _siteId = value;
            }
        }
        public string RegionName
        {
            get
            {
                if (Request.QueryString["RegionName"] != string.Empty)
                {

                    return Request.QueryString["RegionName"];
                }
                else
                    return string.Empty;

            }
            set
            {
                lblGetRegionName.Text = value;
            }
   
        }
        public string RegionCode
        {
            get
            {
                if (Request.QueryString["RegionCode"] != string.Empty)
                {

                    return Request.QueryString["RegionCode"];
                }
                else
                    return string.Empty;

            }
            set {
                lblGetRegionCode.Text = value;
            }

        }
        public int RegionId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["RegionId"]) != 0)
                {
                    regionId = Convert.ToInt32(Request.QueryString["RegionId"]);
                    return regionId;
                }
                else
                    return -1;

            }
            set { regionId = value; }

           
        }

        public IList<Site> siteList
        {
            set { _siteList = value; }
        }


        protected void grvRegionList_SelectedIndexChanged(object sender, EventArgs e)
        {

            _siteId = Convert.ToInt32(grvRegionList.SelectedDataKey["Id"]);
            _presenter.GetSiteById(_siteId);
            BindRegionControls();
        }
        protected void btnCancel_Click1(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }

        protected void grvRegionList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvRegionList.PageIndex = e.NewPageIndex;
            grvRegionList.DataSource = _siteList;
            grvRegionList.DataBind();
        }
}
}