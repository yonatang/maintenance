﻿ 

<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ChecklistList.aspx.cs" Inherits="Chai.Maintenance.Modules.Configuration.View.ChecklistList" %>

 
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
  <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                     <header>
					        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					        <h2>Find Check List</h2>				
				    </header>
                    <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                           <fieldset>					
								<div class="row">
									<section class="col col-6">       
                    <asp:Label ID="Label3" runat="server" Text="Instrument" CssClass="label"></asp:Label>
               <label class="select">
                    <asp:DropDownList ID="ddlInstrument" runat="server" AppendDataBoundItems="True" 
                        AutoPostBack="True" DataTextField="InstrumentName" 
                        DataValueField="Id" >
                        <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                    </asp:DropDownList><i></i></label>
                     </section>
                          </div>
                          </fieldset>
                          <footer>
                     <asp:LinkButton ID="lnkAddNew" runat="server" onclick="lnkAddNew_Click"  Cssclass="btn btn-primary">Add New Task</asp:LinkButton>
                    <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click"  Cssclass="btn btn-primary"
                        Visible="false" />
                </footer>
        </div>
                                </div>
                                </div>
                    <asp:TextBox ID="txtTask" runat="server" Visible="False" ></asp:TextBox>
                    <asp:Label ID="Label2" runat="server" Text="Task" Visible="False"></asp:Label>
                
    
       
        <asp:GridView ID="grvChecklist" runat="server" AutoGenerateColumns="False" 
            CellPadding="3" EnableModelValidation="True" ForeColor="#333333" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
            GridLines="Horizontal" 
            style="text-align: left" Width="100%" 
             AllowPaging="True" 
             onpageindexchanging="grvChecklist_PageIndexChanging" 
             onrowdatabound="grvChecklist_RowDataBound">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Task" />
                <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplEdit" runat="server" Text="Edit"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            
            <PagerStyle  Cssclass="paginate_button active" HorizontalAlign="Center" />
        
        </asp:GridView>
    
   
   
              
           
</asp:Content>

