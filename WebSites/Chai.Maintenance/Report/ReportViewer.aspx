﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ReportViewer.aspx.cs" Inherits="Chai.Maintenance.Modules.Report.Views.ReportViewer" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>

 
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">

     <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Select Criteria</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
   
      
                <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
                <label class="select">
                <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                    AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                    onselectedindexchanged="ddlRegion_SelectedIndexChanged" >
                    <asp:ListItem Value="-1">Select Region</asp:ListItem>
                </asp:DropDownList><i></i></label>
                </section>
                <section class="col col-6">   
                <asp:Label ID="lblSiteType" runat="server" Text="Site Type" CssClass="label"></asp:Label>
                 <label class="select">
                <asp:DropDownList ID="ddlSiteType" runat="server" AppendDataBoundItems="True" 
                    DataTextField="Name" DataValueField="Id" >
                    <asp:ListItem Value="0">Select Site Type</asp:ListItem>
                </asp:DropDownList><i></i></label>
                </section>
                </div>
                <div class="row">
									<section class="col col-6">   
                <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                <label class="select">
                <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                    DataTextField="Name" DataValueField="Id" >
                    <asp:ListItem Value="0">Select Site</asp:ListItem>
                </asp:DropDownList><i></i></label>
                </section>
                <section class="col col-6"> 
                
                <asp:Label ID="lblManufacturer" runat="server" Text="Manufacturer" 
                    CssClass="label"></asp:Label>
            <label class="select">
                <asp:DropDownList ID="ddlManufacturer" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    >
                    <asp:ListItem Value="0">Select Manufacturer</asp:ListItem>
                </asp:DropDownList><i></i></label>
               </section>
               </div>
               <div class="row">
									<section class="col col-6">   
                <asp:Label ID="lblInstrumentCatagory" runat="server" Text="Instrument Catagory" 
                    CssClass="label"></asp:Label>
           <label class="select">
                <asp:DropDownList ID="ddlInstrumentCatagory" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    >
                    <asp:ListItem Value="0">Select Instrument Catagory</asp:ListItem>
                </asp:DropDownList><i></i></label>
                </section>
                <section class="col col-6">   
            
                <asp:Label ID="Label1" runat="server" Text="Consumable" CssClass="label"></asp:Label>
            <label class="select">
                <asp:DropDownList ID="ddlConsumables" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                   >
                    <asp:ListItem Value="0">Select Consumable</asp:ListItem>
                </asp:DropDownList><i></i></label>
                </section>
            </div>
            <div class="row">
									<section class="col col-6">   
                <asp:Label ID="lblDateFrom" runat="server" Text="Date From" CssClass="label"></asp:Label>
                <label class="input">
                   <i class="icon-append fa fa-calendar"></i>
                     <asp:TextBox ID="CalDateFrom"  runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"
                         ></asp:TextBox>
                   
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" 
                            runat="server" ControlToValidate="CalDateFrom" ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator></label>
           
                            </section>
                            
           <section class="col col-6"> 
                <asp:Label ID="lblDateTo" runat="server" Text="Date To" CssClass="label"></asp:Label>
             <label class="input">
                   <i class="icon-append fa fa-calendar"></i>
                     <asp:TextBox ID="CalDateTo" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"
                         ></asp:TextBox>
                   
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                            runat="server" ControlToValidate="CalDateTo" ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator></label></section>
                            
                            </div>
                        </fieldset>	   
                          <footer> 
            <asp:Button ID="btnView" runat="server" onclick="btnView_Click" Text="View" Cssclass="btn btn-primary" />
                    
           
   </footer>
      
                                </div>
                                </div>
                                </div>
    
      </div>   
       
           
               <rsweb:ReportViewer ID="Viewer" runat="server" Width="100%"  DocumentMapWidth="100%"></rsweb:ReportViewer>
        </asp:Content>

