﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.CoreDomain.Configuration;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.UI;
namespace Chai.Maintenance.Modules.Report.Views
{
    public partial class ReportViewer : Microsoft.Practices.CompositeWeb.Web.UI.Page, IReportViewerView
    {
        ReportViewerPresenter _presenter;
        User _user;
        string[] ReportName;
        string Path = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                BindRegion();
                BindSiteType();
                BindInstrumentCatagory();
                BindManufacturer();
                BindConsumables(); 
                
            }
            _presenter.OnViewLoaded();

            string Report = Request.QueryString["ReportName"].ToString();
            ReportName = Report.Split('?');
           
            Path = ConfigurationManager.AppSettings["Reportpath"].ToString();
        }
        [CreateNew]
        public ReportViewerPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        protected void BindRegion()
        {
            ddlRegion.DataSource = _presenter.GetRegion();
            ddlRegion.DataBind();
        }
        protected void BindSiteType()
        {
            ddlSiteType.DataSource = _presenter.GetSiteType();
            ddlSiteType.DataBind();
        }
        protected void BindInstrumentCatagory()
        {
            ddlInstrumentCatagory.DataSource = _presenter.GetInstrumentCatgory();
            ddlInstrumentCatagory.DataBind();
        }
        protected void BindManufacturer()
        {
            ddlManufacturer.DataSource = _presenter.GetManufacturer();
            ddlManufacturer.DataBind();
        
        }
        protected void BindConsumables()
        {
            ddlConsumables.DataSource = _presenter.GetConsumables();
            ddlConsumables.DataBind();

        }

        private void LoadRegionReport()
        {

            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
            Viewer.ProcessingMode = ProcessingMode.Local;
            Viewer.LocalReport.ReportPath = path;
            var datasource = _presenter.GetregionReport();
            Viewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", datasource.Tables[0]));
            Viewer.LocalReport.Refresh();
        }
        private void LoadSiteReport()
        {           
           
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));

            var datasource = _presenter.GetSiteReport(Convert.ToInt32(ddlRegion.SelectedValue),_user.RegionId,Convert.ToInt32(ddlSiteType.SelectedValue));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);          
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var SiteTypeId = Convert.ToInt32(ddlSiteType.SelectedValue);
            var param1 = new ReportParameter("RegionId", RegionId.ToString());
            var param2 = new ReportParameter("userregionId", UserRegionId.ToString());
            var param3 = new ReportParameter("SiteTypeId", SiteTypeId.ToString());
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            Viewer.LocalReport.SetParameters(parameters);
            
        }
        private void LoadSiteContactReport()
        {

            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
            var datasource = _presenter.GetSiteContactReport(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToInt32(ddlSite.SelectedValue));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var SiteId = Convert.ToInt32(ddlSite.SelectedValue);
            var param1 = new ReportParameter("RegionId", RegionId.ToString());
            var param2 = new ReportParameter("userregionId", UserRegionId.ToString());
            var param3 = new ReportParameter("SiteId", SiteId.ToString());
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            Viewer.LocalReport.SetParameters(parameters);
           

        }
        private void LoadInstrumentReport()
        {

            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
            var datasource = _presenter.GetInstrumentReport(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToInt32(ddlSite.SelectedValue));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var SiteId = Convert.ToInt32(ddlSite.SelectedValue);
            var param1 = new ReportParameter("RegionId", RegionId.ToString());
            var param2 = new ReportParameter("userregionId", UserRegionId.ToString());
            var param3 = new ReportParameter("SiteId", SiteId.ToString());
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            Viewer.LocalReport.SetParameters(parameters);
           

        }
        private void LoadUpcomingCurativeReport()
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }

            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
            var datasource = _presenter.GetUpcomeingCurativeReport(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToInt32(ddlSite.SelectedValue),Convert.ToInt32(ddlInstrumentCatagory.SelectedValue),Convert.ToDateTime(CalDateFrom.Text),Convert.ToDateTime(CalDateTo.Text));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var SiteId = Convert.ToInt32(ddlSite.SelectedValue);
            var InstrumentCatagoryId = Convert.ToInt32(ddlInstrumentCatagory.SelectedValue);
            var DateFrom = CalDateFrom.Text;
            var DateTo = CalDateTo.Text;
            var param1 = new ReportParameter("regionId", RegionId.ToString());
            var param2 = new ReportParameter("userRegionId", UserRegionId.ToString());
            var param3 = new ReportParameter("siteId", SiteId.ToString());
            var param4 = new ReportParameter("instrumentCategoryId", InstrumentCatagoryId.ToString());
            var param5 = new ReportParameter("ScheduledDateFrom", DateFrom);
            var param6 = new ReportParameter("ScheduledDateTo", DateTo);
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            parameters.Add(param4);
            parameters.Add(param5);
            parameters.Add(param6);
            Viewer.LocalReport.SetParameters(parameters);
           

        }
        private void LoadUpcomingPreventiveReport()
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
            var datasource = _presenter.GetUpcomeingPreventiveReport(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToInt32(ddlSite.SelectedValue), Convert.ToInt32(ddlInstrumentCatagory.SelectedValue), Convert.ToDateTime(CalDateFrom.Text), Convert.ToDateTime(CalDateTo.Text));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var SiteId = Convert.ToInt32(ddlSite.SelectedValue);
            var InstrumentCatagoryId = Convert.ToInt32(ddlInstrumentCatagory.SelectedValue);
            var DateFrom = CalDateFrom.Text;
            var DateTo = CalDateTo.Text;
            var param1 = new ReportParameter("regionId", RegionId.ToString());
            var param2 = new ReportParameter("userRegionId", UserRegionId.ToString());
            var param3 = new ReportParameter("siteId", SiteId.ToString());
            var param4 = new ReportParameter("instrumentCategoryId", InstrumentCatagoryId.ToString());
            var param5 = new ReportParameter("ScheduledDateFrom", DateFrom);
            var param6 = new ReportParameter("ScheduledDateTo", DateTo);
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            parameters.Add(param4);
            parameters.Add(param5);
            parameters.Add(param6);
            Viewer.LocalReport.SetParameters(parameters);
           

        }
        private void LoadTransferReport()
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
            var datasource = _presenter.GetTransferReport(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToInt32(ddlSite.SelectedValue), Convert.ToInt32(ddlInstrumentCatagory.SelectedValue), Convert.ToDateTime(CalDateFrom.Text), Convert.ToDateTime(CalDateTo.Text));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var SiteId = Convert.ToInt32(ddlSite.SelectedValue);
            var InstrumentCatagoryId = Convert.ToInt32(ddlInstrumentCatagory.SelectedValue);
            var DateFrom = CalDateFrom.Text;
            var DateTo = CalDateTo.Text;
            var param1 = new ReportParameter("regionId", RegionId.ToString());
            var param2 = new ReportParameter("userRegionId", UserRegionId.ToString());
            var param3 = new ReportParameter("siteId", SiteId.ToString());
            var param4 = new ReportParameter("instrumentCategoryId", InstrumentCatagoryId.ToString());
            var param5 = new ReportParameter("transferedDateFrom", DateFrom);
            var param6 = new ReportParameter("transferedDateTo", DateTo);
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            parameters.Add(param4);
            parameters.Add(param5);
            parameters.Add(param6);
            Viewer.LocalReport.SetParameters(parameters);
           

        }
        private void LoadDisposalReport()
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
            
            var datasource = _presenter.GetDisposalReport(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToInt32(ddlSite.SelectedValue), Convert.ToInt32(ddlInstrumentCatagory.SelectedValue), Convert.ToDateTime(CalDateFrom.Text), Convert.ToDateTime(CalDateTo.Text));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var SiteId = Convert.ToInt32(ddlSite.SelectedValue);
            var InstrumentCatagoryId = Convert.ToInt32(ddlInstrumentCatagory.SelectedValue);
            var DateFrom = CalDateFrom.Text;
            var DateTo = CalDateTo.Text;
            var param1 = new ReportParameter("regionId", RegionId.ToString());
            var param2 = new ReportParameter("userRegionId", UserRegionId.ToString());
            var param3 = new ReportParameter("siteId", SiteId.ToString());
            var param4 = new ReportParameter("instrumentCategoryId", InstrumentCatagoryId.ToString());
            var param5 = new ReportParameter("disposedDateFrom", DateFrom);
            var param6 = new ReportParameter("disposedDateTo", DateTo);
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            parameters.Add(param4);
            parameters.Add(param5);
            parameters.Add(param6);
            Viewer.LocalReport.SetParameters(parameters);
           

        }
        private void LoadPerformedCurativeMaintenanceReport()
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
            
            var datasource = _presenter.GetPerformedCurativeMaintenance(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToInt32(ddlSite.SelectedValue), Convert.ToInt32(ddlInstrumentCatagory.SelectedValue), Convert.ToDateTime(CalDateFrom.Text), Convert.ToDateTime(CalDateTo.Text));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var SiteId = Convert.ToInt32(ddlSite.SelectedValue);
            var InstrumentCatagoryId = Convert.ToInt32(ddlInstrumentCatagory.SelectedValue);
            var DateFrom = CalDateFrom.Text;
            var DateTo = CalDateTo.Text;
            var param1 = new ReportParameter("regionId", RegionId.ToString());
            var param2 = new ReportParameter("userRegionId", UserRegionId.ToString());
            var param3 = new ReportParameter("siteId", SiteId.ToString());
            var param4 = new ReportParameter("instrumentCategoryId", InstrumentCatagoryId.ToString());
            var param5 = new ReportParameter("ScheduledDateFrom", DateFrom);
            var param6 = new ReportParameter("ScheduledDateTo", DateTo);
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            parameters.Add(param4);
            parameters.Add(param5);
            parameters.Add(param6);
            Viewer.LocalReport.SetParameters(parameters);
            

        }
        private void LoadPerformedPreventiveMaintenanceReport()
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
           
            var datasource = _presenter.GetPerformedPreventiveMaintenance(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToInt32(ddlSite.SelectedValue), Convert.ToInt32(ddlInstrumentCatagory.SelectedValue), Convert.ToDateTime(CalDateFrom.Text), Convert.ToDateTime(CalDateTo.Text));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var SiteId = Convert.ToInt32(ddlSite.SelectedValue);
            var InstrumentCatagoryId = Convert.ToInt32(ddlInstrumentCatagory.SelectedValue);
            var DateFrom = CalDateFrom.Text;
            var DateTo = CalDateTo.Text;
            var param1 = new ReportParameter("regionId", RegionId.ToString());
            var param2 = new ReportParameter("userRegionId", UserRegionId.ToString());
            var param3 = new ReportParameter("siteId", SiteId.ToString());
            var param4 = new ReportParameter("instrumentCategoryId", InstrumentCatagoryId.ToString());
            var param5 = new ReportParameter("ScheduledDateFrom", DateFrom);
            var param6 = new ReportParameter("ScheduledDateTo", DateTo);
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            parameters.Add(param4);
            parameters.Add(param5);
            parameters.Add(param6);
            Viewer.LocalReport.SetParameters(parameters);
            

        }
        private void LoadNonFunctionalInstrumentsReport()
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
            var datasource = _presenter.GetNonFunctionalInstrumentsBySiteANDInstrumentCategory(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToInt32(ddlSite.SelectedValue), Convert.ToInt32(ddlInstrumentCatagory.SelectedValue));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var SiteId = Convert.ToInt32(ddlSite.SelectedValue);
            var InstrumentCatagoryId = Convert.ToInt32(ddlInstrumentCatagory.SelectedValue);
            var param1 = new ReportParameter("regionId", RegionId.ToString());
            var param2 = new ReportParameter("userRegionId", UserRegionId.ToString());
            var param3 = new ReportParameter("siteId", SiteId.ToString());
            var param4 = new ReportParameter("instrumentCategoryId", InstrumentCatagoryId.ToString());
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            parameters.Add(param4);
            
            Viewer.LocalReport.SetParameters(parameters);
            

        }
        private void LoadInstrumentProblemByManufacturerReport()
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
           
            var datasource = _presenter.GetInstrumentProblemByManufacturer(_user.RegionId, Convert.ToInt32(ddlInstrumentCatagory.SelectedValue),Convert.ToInt32(ddlManufacturer.SelectedValue));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            
            var UserRegionId = _user.RegionId;
            var manufactureId = Convert.ToInt32(ddlManufacturer.SelectedValue);
            var InstrumentCatagoryId = Convert.ToInt32(ddlInstrumentCatagory.SelectedValue);
            
            var param2 = new ReportParameter("userRegionId", UserRegionId.ToString());
            var param4 = new ReportParameter("instrumentCategoryId", InstrumentCatagoryId.ToString());
            var param3 = new ReportParameter("manufacturerId", manufactureId.ToString());
            var parameters = new List<ReportParameter>();
            
            parameters.Add(param2);
            parameters.Add(param4);
            parameters.Add(param3);
            
            Viewer.LocalReport.SetParameters(parameters);
           

        }
        private void LoadQuarterCurativeReport()
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
            
            var datasource = _presenter.GetQuarterCurativeReport(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId,Convert.ToDateTime(CalDateFrom.Text), Convert.ToDateTime(CalDateTo.Text));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var DateFrom = CalDateFrom.Text;
            var DateTo = CalDateTo.Text;
            var param1 = new ReportParameter("RegionId", RegionId.ToString());
            var param2 = new ReportParameter("userregionId", UserRegionId.ToString());
            var param5 = new ReportParameter("DateFrom", DateFrom);
            var param6 = new ReportParameter("DateTo", DateTo);
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param6);
            parameters.Add(param5);
            Viewer.LocalReport.SetParameters(parameters);
           
        }
        private void LoadQuarterPreventiveReport()
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
            var datasource = _presenter.GetQuarterPreventiveReport(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToDateTime(CalDateFrom.Text), Convert.ToDateTime(CalDateTo.Text));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var DateFrom = CalDateFrom.Text;
            var DateTo = CalDateTo.Text;
            var param1 = new ReportParameter("RegionId", RegionId.ToString());
            var param2 = new ReportParameter("userregionId", UserRegionId.ToString());
            var param5 = new ReportParameter("DateFrom", DateFrom);
            var param6 = new ReportParameter("DateTo", DateTo);
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param6);
            parameters.Add(param5);
            Viewer.LocalReport.SetParameters(parameters);
           

        }
        private  void LoadEscalatedCurativeMaintenances()
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));

            var datasource = _presenter.GetEscalatedCurativeMaintenances(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToInt32(ddlSite.SelectedValue), Convert.ToInt32(ddlInstrumentCatagory.SelectedValue), Convert.ToDateTime(CalDateFrom.Text), Convert.ToDateTime(CalDateTo.Text));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var SiteId = Convert.ToInt32(ddlSite.SelectedValue);
            var InstrumentCatagoryId = Convert.ToInt32(ddlInstrumentCatagory.SelectedValue);
            var DateFrom = CalDateFrom.Text;
            var DateTo = CalDateTo.Text;
            var param1 = new ReportParameter("regionId", RegionId.ToString());
            var param2 = new ReportParameter("userRegionId", UserRegionId.ToString());
            var param3 = new ReportParameter("siteId", SiteId.ToString());
            var param4 = new ReportParameter("instrumentCategoryId", InstrumentCatagoryId.ToString());
            var param5 = new ReportParameter("EscalatedDateFrom", DateFrom);
            var param6 = new ReportParameter("EscalatedDateTo", DateTo);
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            parameters.Add(param4);
            parameters.Add(param5);
            parameters.Add(param6);
            Viewer.LocalReport.SetParameters(parameters);
        }
        private void LoadNotifiedConsumables()
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));

            var datasource = _presenter.GetNotifiedConsumables(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToInt32(ddlSite.SelectedValue), Convert.ToInt32(ddlConsumables.SelectedValue));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var regionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var userRegionId = _user.RegionId;
            var siteId = Convert.ToInt32(ddlSite.SelectedValue);
            var consumableId = Convert.ToInt32(ddlConsumables.SelectedValue);

            var param1 = new ReportParameter("regionId", regionId.ToString());
            var param2 = new ReportParameter("userRegionId", userRegionId.ToString());
            var param3 = new ReportParameter("siteId", siteId.ToString());
            var param4 = new ReportParameter("consumableId", consumableId.ToString());
 
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            parameters.Add(param4);
           
            Viewer.LocalReport.SetParameters(parameters);
        }
        private void LoadInstrumentsUnderContract()
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
            var datasource = _presenter.GetInstrumentUnderContract(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToInt32(ddlSite.SelectedValue));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var SiteId = Convert.ToInt32(ddlSite.SelectedValue);
            var param1 = new ReportParameter("RegionId", RegionId.ToString());
            var param2 = new ReportParameter("userregionId", UserRegionId.ToString());
            var param3 = new ReportParameter("SiteId", SiteId.ToString());
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            Viewer.LocalReport.SetParameters(parameters);


        }

        private void LoadCurativeMaintenanceTracking()/////////////////////////////////////////////
        {
            if (CalDateFrom.Text == "")
            {
                CalDateFrom.Text = "1/1/1990";
            }
            if (CalDateTo.Text == "")
            {
                CalDateTo.Text = "1/1/9999";
            }
            var path = Server.MapPath(Convert.ToString(Path + ReportName[0] + ".rdlc"));
            var datasource = _presenter.GetCurativeMaintenanceTracking(Convert.ToInt32(ddlRegion.SelectedValue), _user.RegionId, Convert.ToInt32(ddlSite.SelectedValue));
            ReportDataSource s = new ReportDataSource("DataSet1", datasource.Tables[0]);
            Viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(s);
            Viewer.LocalReport.ReportPath = path;
            var RegionId = Convert.ToInt32(ddlRegion.SelectedValue);
            var UserRegionId = _user.RegionId;
            var SiteId = Convert.ToInt32(ddlSite.SelectedValue);
            var param1 = new ReportParameter("RegionId", RegionId.ToString());
            var param2 = new ReportParameter("userregionId", UserRegionId.ToString());
            var param3 = new ReportParameter("SiteId", SiteId.ToString());
            var parameters = new List<ReportParameter>();
            parameters.Add(param1);
            parameters.Add(param2);
            parameters.Add(param3);
            Viewer.LocalReport.SetParameters(parameters);


        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            if (ReportName[0] == "RRegion")
            {
                LoadRegionReport();
            }
            else if (ReportName[0] == "RSite")
            {
                LoadSiteReport();
            }
            else if (ReportName[0] == "RSiteContact")
            {
                LoadSiteContactReport();
            }
            else if (ReportName[0] == "RInstrument")
            {
                LoadInstrumentReport();
            }
            else if (ReportName[0] == "RUpcomingCurativeMaintenance")
            {
                LoadUpcomingCurativeReport();
            }
            else if (ReportName[0] == "RUpcomingPreventiveMaintenance")
            {
                LoadUpcomingPreventiveReport();
            }
            else if (ReportName[0] == "RTransferedInstruments")
            {
                LoadTransferReport();
            }
            else if (ReportName[0] == "RDisposedInstruments")
            {
                LoadDisposalReport();
            }
            else if (ReportName[0] == "RPerformedCurativeMaintenance")
            {
                LoadPerformedCurativeMaintenanceReport();
            }
            else if (ReportName[0] == "RPerformedPreventiveMaintenance")
            {
                LoadPerformedPreventiveMaintenanceReport();
            }
            else if (ReportName[0] == "RNonFunctionalInstrument")
            {
                LoadNonFunctionalInstrumentsReport();
            }
            else if (ReportName[0] == "RInstrumentProblemByManufacturer")
            {
                LoadInstrumentProblemByManufacturerReport();
            }
            else if (ReportName[0] == "RQuarterCurative")
            {
                LoadQuarterCurativeReport();
            }
            else if (ReportName[0] == "RQuarterPreventive")
            {
                LoadQuarterPreventiveReport();
            }
            else if(ReportName[0] == "REscalatedCurativeMaintenances")
            {
                LoadEscalatedCurativeMaintenances();
            }
            else if (ReportName[0] == "RNotifiedConsumables")
            {
                LoadNotifiedConsumables();
            }
            else if (ReportName[0] == "RInstrumentsUnderContract")
            {
                LoadInstrumentsUnderContract();
            }
            else if (ReportName[0] == "RCurativeMaintenanceTracking")
            {
                LoadCurativeMaintenanceTracking();
            }
        }

        public new User User
        {
            set { _user = value; }
        }
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            IList<Site> _siteList;
            _siteList = _presenter.GetSiteList(Convert.ToInt32(ddlRegion.SelectedValue));
            if (ddlSite.Items.Count > 0)
            {
                ddlSite.Items.Clear();
            }
            ListItem lst = new ListItem();
            lst.Text = "Select Site";
            lst.Value = "0";
            ddlSite.Items.Add(lst);



            ddlSite.DataSource = _siteList;
            ddlSite.DataBind();
        }
}
}