﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ItemBalance.aspx.cs" Inherits="Chai.Maintenance.Modules.SparepartInventory.Views.ItemBalance" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Item Balance</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
        
     
        
                <asp:Label ID="lblInstrument" runat="server" Text="Instrument" CssClass="label"></asp:Label>
           <label class="select">
                <asp:DropDownList ID="ddlInstrument" runat="server" 
                    AppendDataBoundItems="True" AutoPostBack="True" DataTextField="InstrumentName" 
                    DataValueField="Id" 
                    onselectedindexchanged="ddlInstrument_SelectedIndexChanged" CssClass="textbox">
                    <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                </asp:DropDownList><i></i></label></section>
           <section class="col col-6">  
                <asp:Label ID="lblSparepart" runat="server" Text="Sparepart" CssClass="label"></asp:Label>
           <label class="select">
                <asp:DropDownList ID="ddlSparepart" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Sparepart</asp:ListItem>
                </asp:DropDownList><i></i></label></section></div></fieldset>
           <footer>
                <asp:Button ID="btnView" runat="server" onclick="btnView_Click" Text="View" Cssclass="btn btn-primary"/>
           </footer></div></div></div>
    
  
        
           
              
            <asp:GridView ID="grvItemBalanceList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="0" DataKeyNames="Id" EnableModelValidation="True" 
             CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    ForeColor="#333333" GridLines="Horizontal" 
                Width="100%">
                
                <Columns>
                    <asp:BoundField DataField="SparepartName" HeaderText="Sparepart Name" />
                    <asp:BoundField DataField="Qty" HeaderText="Stock Qty" />
                </Columns>
                <PagerStyle ForeColor="White" HorizontalAlign="Center" />
            </asp:GridView>
            </div>
  
  </asp:Content>

