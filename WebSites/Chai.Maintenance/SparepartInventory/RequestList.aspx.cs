﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public partial class RequestList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IRequestListView
    {
        private RequestListPresenter _presenter;
        private IList<Request> _request;
        private User _user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();
        }
        [CreateNew]
        public RequestListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        
        protected void ddlSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
        
            if (Convert.ToInt32(ddlSelect.SelectedValue) == 2)
            {
                
                CalDateFrom.Visible = true;
                CalDateTo.Visible = true;
                
                txtvalue.Visible = false;
                lblDateFrom.Visible = true;
                lblDateTo.Visible = true;
                lblValue.Visible = false;

            }
            else
            {
                CalDateFrom.Visible = false;
                CalDateTo.Visible = false;
                
                txtvalue.Visible = true;
                lblDateFrom.Visible = false;
                lblDateTo.Visible = false;
                lblValue.Visible = true;
            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetRequestList(Convert.ToInt32(ddlSelect.SelectedValue), txtvalue.Text, CalDateFrom.Text, CalDateTo.Text,_user.RegionId,_user.Id);
            grvRequestList.DataSource = _request;
            grvRequestList.DataBind();
        }

        public new IList<Request> Requests
        {
            set { _request = value; }
        }
        protected void grvRequestList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem != null)
            {
                Request request = e.Row.DataItem as Request;
                if (request != null)
                {
                    HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                    string url = String.Format("~/SparepartInventory/RequestEdit.aspx?{0}=3&RequestId={1}", AppConstants.TABID, request.Id);

                    hpl.NavigateUrl = this.ResolveUrl(url);

                }

                if (request.ApprovalStatus == "Approved" || request.ApprovalStatus == "Rejected")
                {

                    e.Row.Enabled = false;
                  
                }
            }
        }

        
        protected void lnkNew_Click(object sender, EventArgs e)
        {
           Response.Redirect(String.Format("~/SparepartInventory/RequestEdit.aspx?{0}=3", AppConstants.TABID));
        }
        protected void grvRequestList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
              grvRequestList.PageIndex = e.NewPageIndex;
              grvRequestList.DataSource = _request;
              grvRequestList.DataBind();

        }


        public User user
        {
            set { _user = value; }
        }
    }
}