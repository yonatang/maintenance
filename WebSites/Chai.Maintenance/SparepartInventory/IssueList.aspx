﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="IssueList.aspx.cs" Inherits="Chai.Maintenance.Modules.SparepartInventory.Views.IssueList" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Find Issue</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
        
     
                    <asp:Label ID="lblSelectCriteria" runat="server" Text="Find By" 
                        CssClass="label"></asp:Label>
                <label class="select">
                    <asp:DropDownList ID="ddlSelect" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlSelect_SelectedIndexChanged" CssClass="textbox">
                        <asp:ListItem Value="0">Select Criteria</asp:ListItem>
                        <asp:ListItem Value="1">Issue No.</asp:ListItem>
                        <asp:ListItem Value="2">Date</asp:ListItem>
                    </asp:DropDownList><i></i></label></section>
               <section class="col col-6"> 
                    <asp:Label ID="lblValue" runat="server" Text="Value" Visible="False" 
                        CssClass="label"></asp:Label>
               <label class="input">
                    <asp:TextBox ID="txtvalue" runat="server" Visible="False" 
                    Wrap="False" CssClass="textbox"></asp:TextBox></label></section></div>
               <div class="row">
				<section class="col col-6">
                    <asp:Label ID="lblDateFrom" runat="server" Text="Date From" Visible="False" 
                        CssClass="label"></asp:Label>
                 <label class="input">
                   <i class="icon-append fa fa-calendar"></i>
                     <asp:TextBox ID="CalDateFrom"  runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"
                         ></asp:TextBox>
                   
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" 
                            runat="server" ControlToValidate="CalDateFrom" ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator></label>
               </section>
               <section class="col col-6">
                    <asp:Label ID="lblDateTo" runat="server" Text="Date To" Visible="False" 
                        CssClass="label"></asp:Label>
                <label class="input">
                   <i class="icon-append fa fa-calendar"></i>
                     <asp:TextBox ID="CalDateTo" runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"
                         ></asp:TextBox>
                   
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                            runat="server" ControlToValidate="CalDateTo" ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator></label>
              </section></div></fieldset>
                 <footer> 
                    <asp:Button ID="btnFind" runat="server" Text="Find" Width="54px" 
                    onclick="btnFind_Click" ValidationGroup="1" Cssclass="btn btn-primary"/>
             </footer>
      
                                </div>
                                </div>
                                </div>
  
            <asp:GridView ID="grvIssueList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id,Approver,IssuedFor" EnableModelValidation="True" 
                    ForeColor="#333333" GridLines="Horizontal" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                    onrowdatabound="grvIssueList_RowDataBound" 
                Width="100%" AllowPaging="True" 
          onpageindexchanging="grvIssueList_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="IssueNo" HeaderText="Issue No" />
                    <asp:BoundField DataField="IssueDate" HeaderText="Issue Date" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                 <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:GridView>
  </div>
</asp:Content>

