﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="BeginningBalance.aspx.cs" Inherits="Chai.Maintenance.Modules.SparepartInventory.Views.BeginningBalance" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>


<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
   <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Begining Balance</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
        
  
       
                <asp:Label ID="lblInstrument" runat="server" Text="Instrument" CssClass="label"></asp:Label>
            <label class="select">
                <asp:DropDownList ID="ddlInstrumentforList" runat="server" 
                    AppendDataBoundItems="True" AutoPostBack="True" DataTextField="InstrumentName" 
                    DataValueField="Id" 
                    onselectedindexchanged="ddlInstrumentforList_SelectedIndexChanged" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                </asp:DropDownList><i></i></label></section><
           <section class="col col-6">   
                <asp:Label ID="lblSparepart" runat="server" Text="Sparepart" CssClass="label"></asp:Label>
            <label class="select">
                <asp:DropDownList ID="ddlSparepartforList" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    >
                    <asp:ListItem Value="0">Select Sparepart</asp:ListItem>
                </asp:DropDownList><i></i></label></section></div></fieldset>
                <footer>
                <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" Cssclass="btn btn-primary" />
           
   </footer>
      
                                </div>
                                </div>
                                </div>
    
  
    
        <asp:DataGrid ID="dgItemDetail" runat="server" AutoGenerateColumns="False" 
            CellPadding="0" ShowFooter="True" Width="99%" ForeColor="#333333" 
            CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass="" 
             HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White"
            oncancelcommand="dgItemDetail_CancelCommand" 
            ondeletecommand="dgItemDetail_DeleteCommand" 
            oneditcommand="dgItemDetail_EditCommand" 
            onitemcommand="dgItemDetail_ItemCommand" 
            onitemdatabound="dgItemDetail_ItemDataBound" 
            onupdatecommand="dgItemDetail_UpdateCommand" BorderColor="#003366" 
            BorderStyle="Solid" BorderWidth="1px" GridLines="None" AllowPaging="True" 
             onpageindexchanged="dgItemDetail_PageIndexChanged">
            <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:TemplateColumn HeaderText="Instrument Name">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlInstrument" runat="server" AppendDataBoundItems="True" 
                                    Width="150px" onselectedindexchanged="ddlInstrument_SelectedIndexChanged" 
                                    DataTextField="InstrumentName" DataValueField="Id">
                            <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RfvInstrument" runat="server" 
                                    ControlToValidate="ddlInstrument" ErrorMessage="Instrument Required" 
                                    InitialValue="0" ValidationGroup="3">*</asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlFInstrument" runat="server" 
                            AppendDataBoundItems="True" AutoPostBack="True" DataTextField="InstrumentName" 
                            DataValueField="Id" onselectedindexchanged="ddlFInstrument_SelectedIndexChanged" Width="200px">
                           
                            <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RfvFInstrument" runat="server" 
                                    ControlToValidate="ddlFInstrument" Display="Dynamic" 
                                    ErrorMessage="Instrument Required" InitialValue="0" 
                            ValidationGroup="2">*</asp:RequiredFieldValidator>
                    </FooterTemplate>
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Sparepart Name">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlSparepart" runat="server" AppendDataBoundItems="True" 
                                    ValidationGroup="3" Width="150px" DataTextField="Name" 
                            DataValueField="Id">
                            <asp:ListItem Value="0">Select Sparepart</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RfvSparepart" runat="server" 
                                    ControlToValidate="ddlSparepart" ErrorMessage="Sparepart Required" 
                                    InitialValue="0" SetFocusOnError="True" ValidationGroup="3">*</asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlFSparepart" runat="server" AppendDataBoundItems="True"  
                                    ValidationGroup="2" Width="150px" DataTextField="Name" DataValueField="Id" 
                                    EnableViewState="true">
                            <asp:ListItem Value="0">Select Sparepart</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RfvFSparepart" runat="server" 
                                    ControlToValidate="ddlFSparepart" ErrorMessage="Sparepart Required" 
                                    InitialValue="0" SetFocusOnError="True" ValidationGroup="2">*</asp:RequiredFieldValidator>
                    </FooterTemplate>
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "SparepartName")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Qty">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtQty" runat="server" ValidationGroup="3" Width="100px" 
                                    Text =' <%# DataBinder.Eval(Container.DataItem, "Qty")%>'></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="txtQty_FilteredTextBoxExtender" runat="server" 
                                    Enabled="True" FilterType="Numbers" TargetControlID="txtQty">
                        </cc2:FilteredTextBoxExtender>
                        <asp:RequiredFieldValidator ID="RfvQty" runat="server" 
                                    ErrorMessage="Qty Required" InitialValue="0" ValidationGroup="3" 
                                    ControlToValidate="txtQty">*</asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtFQty" runat="server" ValidationGroup="2" Width="100px" 
                                    EnableViewState="true" ></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="txtFQty_FilteredTextBoxExtender" 
                                    runat="server" Enabled="True" FilterType="Numbers" 
                                    TargetControlID="txtFQty">
                        </cc2:FilteredTextBoxExtender>
                        <asp:RequiredFieldValidator ID="RfvFQty" runat="server" 
                                    ErrorMessage="Qty Required" InitialValue="0" ValidationGroup="2" 
                                    ControlToValidate="txtFQty" Display="Dynamic">*</asp:RequiredFieldValidator>
                    </FooterTemplate>
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Qty")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <EditItemTemplate>
                        <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="Update" 
                                    ValidationGroup="3">Update</asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="lnkDelete" runat="server" 
                            CommandName="Delete">Delete</asp:LinkButton>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:LinkButton ID="lnkAddNew" runat="server" CommandName="AddNew" 
                                    ValidationGroup="2">Add New</asp:LinkButton>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" 
                                    Text="Delete" />
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
                       
           <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
        
        </asp:DataGrid>
 
</asp:Content>

