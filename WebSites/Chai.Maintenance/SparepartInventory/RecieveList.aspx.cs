﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public partial class RecieveList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IRecieveListView
    {
        private RecieveListPresenter _presenter;
        private IList<Recieve> _recieve;
        private User _user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();
           // grvRecieveList.DataSource = _recieve;
            //grvRecieveList.DataBind();
           
        }
        [CreateNew]
        public RecieveListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        protected void grvRecieveList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Recieve recieve = e.Row.DataItem as Recieve;
            if (recieve != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/SparepartInventory/RecieveEdit.aspx?{0}=3&RecieveId={1}", AppConstants.TABID, recieve.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);

            }
        }
        protected void ddlSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlSelect.SelectedValue) == 5)
            {
                CalDateFrom.Visible = true;
                CalDateTo.Visible = true;
                
                txtvalue.Visible = false;
                lblDateFrom.Visible = true;
                lblDateTo.Visible = true;
                lblValue.Visible = false;
            }
            else
            {
                CalDateFrom.Visible = false;
                CalDateTo.Visible = false;
                
                txtvalue.Visible = true;
                lblDateFrom.Visible = false;
                lblDateTo.Visible = false;
                lblValue.Visible = true;
            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
           _presenter.GetRecieveList(Convert.ToInt32(ddlSelect.SelectedValue), txtvalue.Text, CalDateFrom.Text, CalDateTo.Text, _user.RegionId);
          grvRecieveList.DataSource = _recieve;
          grvRecieveList.DataBind();
        }

        IList<Recieve> IRecieveListView._recieve
        {
            set { _recieve = value; }
        }




        public User user
        {
            set { _user = value; }
        }
        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/SparepartInventory/RecieveEdit.aspx?{0}=3", AppConstants.TABID));
           
        }
        protected void grvRecieveList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvRecieveList.PageIndex = e.NewPageIndex;
            grvRecieveList.DataSource = _recieve;
            grvRecieveList.DataBind();
        }
}
}