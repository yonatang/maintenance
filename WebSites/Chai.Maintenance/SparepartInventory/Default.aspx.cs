﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Chai.Maintenance.Modules.Configuration.Views;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public partial class Default : Microsoft.Practices.CompositeWeb.Web.UI.Page, IDefaultView
    {
        private DefaultViewPresenter _presenter;
        private User _user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();
            BindIssuedChart();
            BindRequestedChart();
        }

        [CreateNew]
        public DefaultViewPresenter Presenter
        {
            set
            {
                this._presenter = value;
                this._presenter.View = this;
            }
        }
        void BindIssuedChart()
        {
            DateTime datefrom = Convert.ToDateTime("1/1/1753");

            DateTime dateTo = DateTime.Now;

            if (this.Calendar1.Text != "")
                datefrom = Convert.ToDateTime(this.Calendar1.Text);

            if (this.Calendar2.Text != "")
                dateTo = Convert.ToDateTime(this.Calendar2.Text);

            chartIssue.DataSource = _presenter.GetMostIssuedSparepartsbyInstrumentsChart(datefrom,
                                                dateTo);
            chartIssue.DataBind();
            chartIssue.Series[0]["PointWidth"] = "0.1";
            chartIssue.Series[0].YValueMembers = "NoOfIssuedSparepartsByInstruments";
            chartIssue.Series[0].XValueMember = "InstrumentName";



        }
        void BindRequestedChart()
        {
            DateTime datefrom = Convert.ToDateTime("1/1/1753");

            DateTime dateTo = DateTime.Now;

            if (this.Calendar1.Text != "")
                datefrom = Convert.ToDateTime(this.Calendar1.Text);

            if (this.Calendar2.Text != "")
                dateTo = Convert.ToDateTime(this.Calendar2.Text);

            chartRequest.DataSource = _presenter.GetMostRequestedSparepartsbyInstrumentsChart(datefrom,
                                                dateTo);
            chartRequest.DataBind();
            chartRequest.Series[0]["PointWidth"] = "0.1";
            chartRequest.Series[0].YValueMembers = "NoOfIssuedSparepartsByInstruments";
            chartRequest.Series[0].XValueMember = "InstrumentName";



        }
        public User user
        {
            set { _user = value; }
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            DateTime datefrom = Convert.ToDateTime("1/1/1753");

            DateTime dateTo = DateTime.Now;

            if (this.Calendar1.Text != "")
                datefrom = Convert.ToDateTime(this.Calendar1.Text);

            if (this.Calendar2.Text != "")
                dateTo = Convert.ToDateTime(this.Calendar2.Text);

            chartIssue.Series[0]["PointWidth"] = "0.1";
            chartIssue.DataSource = _presenter.GetMostIssuedSparepartsbyInstrumentsChart(datefrom,
                                                dateTo);
            chartIssue.DataBind();

            chartIssue.Series[0].YValueMembers = "NoOfIssuedSparepartsByInstruments";
            chartIssue.Series[0].XValueMember = "InstrumentName";
            //
            //DateTime datefrom = Convert.ToDateTime("1/1/1753");

            //DateTime dateTo = DateTime.Now;

            //if (this.Calendar1.Text != "")
            //    datefrom = Convert.ToDateTime(this.Calendar1.Text);

            //if (this.Calendar2.Text != "")
            //    dateTo = Convert.ToDateTime(this.Calendar2.Text);

            chartRequest.DataSource = _presenter.GetMostRequestedSparepartsbyInstrumentsChart(datefrom,
                                                dateTo);
            chartRequest.DataBind();
            chartRequest.Series[0]["PointWidth"] = "0.1";
            chartRequest.Series[0].YValueMembers = "NoOfIssuedSparepartsByInstruments";
            chartRequest.Series[0].XValueMember = "InstrumentName";

        }
}
}
