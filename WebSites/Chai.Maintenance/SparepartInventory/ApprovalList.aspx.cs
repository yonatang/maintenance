﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{

    public partial class ApprovalList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IApprovalListView
    {
        private ApprovalListPresenter _presenter;
        private IList<Approval> _approval;
        private User _user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();
        }
        [CreateNew]
        public ApprovalListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        protected void grvApprovalList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Approval approval = e.Row.DataItem as Approval;
            if (approval != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/SparepartInventory/ApprovalEdit.aspx?{0}=3&ApprovalId={1}", AppConstants.TABID, approval.Id);

                hpl.NavigateUrl = this.ResolveUrl(url);

            }
           
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetApprovalList(Convert.ToInt32(ddlSelect.SelectedValue), txtvalue.Text, CalDateFrom.Text, CalDateTo.Text, _user.RegionId, _user.Id);
            grvApprovalList.DataSource = _approval;
            grvApprovalList.DataBind();
        }
        protected void ddlSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlSelect.SelectedValue) == 2)
            {

                CalDateFrom.Visible = true;
                CalDateTo.Visible = true;
                
                txtvalue.Visible = false;
                lblDateFrom.Visible = true;
                lblDateTo.Visible = true;
                lblValue.Visible = false;
            }
            else
            {
                CalDateFrom.Visible = false;
                CalDateTo.Visible = false;
              
                txtvalue.Visible = true;
                lblDateFrom.Visible = false;
                lblDateTo.Visible = false;
                lblValue.Visible = true;
            }
        }

        public IList<Approval> approval
        {
            set { _approval = value; }
        }

        public User user
        {
            set { _user = value; }
        }
        protected void grvApprovalList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvApprovalList.PageIndex = e.NewPageIndex;
            grvApprovalList.DataSource = _approval;
            grvApprovalList.DataBind();
        }
}
}