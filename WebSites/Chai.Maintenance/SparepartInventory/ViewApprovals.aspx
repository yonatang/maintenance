﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ViewApprovals.aspx.cs" Inherits="Chai.Maintenance.Modules.SparepartInventory.Views.ViewApprovals" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
     <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>View Approval</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
           
                    <asp:Label ID="lblSelectCriteria" runat="server" Text="Find By" 
                        CssClass="label"></asp:Label>
                <label class="select">
                    <asp:DropDownList ID="ddlSelect" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlSelect_SelectedIndexChanged" CssClass="textbox">
                        <asp:ListItem Value="0">Select Criteria</asp:ListItem>
                        <asp:ListItem Value="1">Approval No.</asp:ListItem>
                        <asp:ListItem Value="2">Date</asp:ListItem>
                        <asp:ListItem Value="3">Requester</asp:ListItem>
                    </asp:DropDownList><i></i></label>
              </section>
              </div>
              <div class="row">
              <section class="col col-6"> 
                    <asp:Label ID="lblValue" runat="server" Text="Value" Visible="False" 
                        CssClass="label"></asp:Label>
                 <label class="input">
                    <asp:TextBox ID="txtvalue" runat="server" Visible="False" 
                    Wrap="False" CssClass="textbox"></asp:TextBox></label>
            </section>
             <section class="col col-6"> 
                    <asp:Label ID="lblRequester" runat="server" Text="Requester" Visible="False" 
                        CssClass="label"></asp:Label>
                <label class="select">
                    <asp:DropDownList ID="ddlRequester" runat="server" DataTextField="FullName" 
                        DataValueField="Id" Visible="False">
                        <asp:ListItem Value="0">Select Requester</asp:ListItem>
                    </asp:DropDownList><i id="inlineapprover" runat="server"></i></label>
               </section>
               </div>
                <div class="row">
              <section class="col col-6"> 
                    <asp:Label ID="lblDateFrom" runat="server" Text="Date From" Visible="False" 
                        CssClass="label"></asp:Label>
                <label class="input">
                        <asp:TextBox ID="Calendar1" runat="server" CssClass="textbox" 
                        Visible="False"></asp:TextBox>
                        <cc2:CalendarExtender ID="Calendar1_CalendarExtender" runat="server" 
                            Enabled="True" PopupButtonID="Img" TargetControlID="Calendar1">
                        </cc2:CalendarExtender>
                        <asp:Image ID="Img" runat="server" Visible="false"
                            ImageUrl="~/Images/Calendar_scheduleHS.png" />
                    <asp:RegularExpressionValidator ID="REVDateFrom" runat="server" 
                        ControlToValidate="Calendar1" Display="Dynamic" 
                        ErrorMessage="Date From Is Not Valid" 
                        ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                        ValidationGroup="1">*</asp:RegularExpressionValidator></label>
               </section>
               <section class="col col-6">
                    <asp:Label ID="lblDateTo" runat="server" Text="Date To" Visible="False" 
                        CssClass="label"></asp:Label>
               <label class="input">
                        <asp:TextBox ID="Calendar2" runat="server" CssClass="textbox" 
                        Visible="False"></asp:TextBox>
                        <cc2:CalendarExtender ID="Calendar2_CalendarExtender" runat="server" 
                            Enabled="True" PopupButtonID="Img1" TargetControlID="Calendar2">
                        </cc2:CalendarExtender>
                        <asp:Image ID="Img1" runat="server" Visible="false"
                            ImageUrl="~/Images/Calendar_scheduleHS.png" />
                        <asp:RegularExpressionValidator ID="REVDateTo" runat="server" 
                        ControlToValidate="Calendar2" Display="Dynamic" 
                        ErrorMessage="Date To Is Not Valid" 
                        ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                        ValidationGroup="1">*</asp:RegularExpressionValidator></label>
                </section>
                </div>
                </fieldset>
                  <footer>
                    <asp:Button ID="btnFind" runat="server" Text="Find" Width="54px" 
                    onclick="btnFind_Click" ValidationGroup="1" Cssclass="btn btn-primary"/>
                </footer>
      
                                </div>
                                </div>
                                </div>
  
  
            <asp:GridView ID="grvApprovalList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id,RequestedBy,Approver" EnableModelValidation="True" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass="" 
                    ForeColor="#333333" GridLines="Horizontal" 
                Width="100%" 
          onselectedindexchanged="grvApprovalList_SelectedIndexChanged" 
          AllowPaging="True" onpageindexchanging="grvApprovalList_PageIndexChanging">
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <Columns>
                    <asp:BoundField DataField="ApprovalNo" HeaderText="Approval No" />
                    <asp:BoundField DataField="ApprovalDate" HeaderText="Approval Date" />
                    <asp:BoundField DataField="ApprovalStatus" HeaderText="Approval Status" />
                    <asp:CommandField SelectText="Issue" ShowSelectButton="True" />
                </Columns>
                 <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
                
            </asp:GridView>
  
 
</asp:Content>

