﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{

    public partial class IssueEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IIssueEditView
    {
        private IssuePresneter _presenter;
        private Issue _issue;
        private IList<IssueItemDetail> _issueDetail;
        private User _requester;
        private User _approver;
        private User _user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();
            lblapproverresult.Text = _approver.FullName;
            lbltoolkeeperresult.Text = _user.FullName;
            if (_requester.FullName != null)
            {
                lblIssuedforesult.Text = _requester.FullName;
            }
            else 
            {
                lblIssuedforesult.Text = _requester.VendorName;
            }
            if (_issue.IsPosted == true)
            {
                btnSave.Enabled = false;
                dgItemDetail.Enabled = false;
                btnDelete.Enabled = false;
            }
            if (!IsPostBack)
            {
                BindIssueControls();
                BindItemDetails();

            }
            if (_issue.Id == -1)

                txtIssueNo.Text = AutoNumber();
        }

        private string AutoNumber()
        {
            string _AutoNumber = "";
            string _prefix = "";
            int _sequence = 0;
            _sequence = _presenter.GetAutoNumberByPageId((int)AspxPageType.Issue).Sequence + 1;
            _prefix = _presenter.GetAutoNumberByPageId((int)AspxPageType.Issue).Prefix;
            _AutoNumber = _prefix + " - " + _sequence;
            return _AutoNumber;
        }
        [CreateNew]
        public IssuePresneter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["IssuedId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["IssuedId"]);
                return 0;
            }
        }

        public int ApprovedId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ApprovalId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ApprovalId"]);
                return 0;
            }
        }
        public int RequesterId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["Requester"]) != 0)
                    return Convert.ToInt32(Request.QueryString["Requester"]);
                return _issue.IssuedFor;
            }
        }
        public int approver
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["Approver"]) != 0)
                    return Convert.ToInt32(Request.QueryString["Approver"]);
                return _issue.Approver;
            }
        }
        public User Requester
        {
            set { _requester=value; }
        }

        public User Approver
        {
            set { _approver = value; }
        }
        public User user
        {
            set { _user = value; }
        }
        public Issue issue
        {
            get
            {
                return _issue;
            }
            set
            {
                _issue = value; 
            }
        }

        private void BindInstrument(DropDownList ddlInstrument)
        {
            ddlInstrument.DataSource = _presenter.GetInstrumentName();
            ddlInstrument.DataBind();
        }

        private void BindSparepart(DropDownList ddlSparepart, int InsId)
        {
            ddlSparepart.DataSource = _presenter.GetSparepart(InsId);
            ddlSparepart.DataBind();
        }
        private void BindIssueControls()
        {

            this.txtIssueNo.Text = _issue.IssueNo;
            this.CalIssueDate.Text = _issue.IssueDate.ToString();

            this.btnDelete.Visible = (_issue.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }
        private void SaveIssue()
        {
            try
            {
                if (_issue.IsNew())
                {
                    if (_issue.ItemDetail.Count != 0)
                    {
                        _presenter.SaveorUpdateIssue(_issue);

                        Master.ShowMessage(new AppMessage("Issue Info. saved", RMessageType.Info));


                    }
                    else
                    {
                        Master.ShowMessage(new AppMessage("You Must Add Sparepart Detail", RMessageType.Error));

                    }
                }
                else
                {
                    if (_issue.ItemDetail.Count != 0)
                    {
                        _presenter.SaveorUpdateIssue(_issue);
                        Master.ShowMessage(new AppMessage("Approval Info. saved", RMessageType.Info));


                    }
                    else
                    {
                        Master.ShowMessage(new AppMessage("You Must Add Sparepart Detail", RMessageType.Error));

                    }
                }
            }

            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.IsValid)
            {
                _issue.IssueNo = this.txtIssueNo.Text;
                _issue.IssueDate = Convert.ToDateTime(this.CalIssueDate.Text);
                _issue.ApprovalId = ApprovedId;
                _issue.IssuedFor = RequesterId;
                _issue.Approver = approver;
                _issue.ToolkeeperId = _user.Id;
                _issue.RegionId = _user.RegionId;
                SaveIssue();


            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            _presenter.CancelPage();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _presenter.DeleteIssue(_issue.Id);
                Master.ShowMessage(new AppMessage("Issue Info. was deleted successfully", Chai.Maintenance.Enums.RMessageType.Info));

            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Error: Unable to delete Issue Info. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
        #region ItemDetail
        private void BindItemDetails()
        {
            dgItemDetail.DataSource = _issue.ItemDetail;
            dgItemDetail.DataBind();
        }
        protected void dgItemDetail_CancelCommand(object source, DataGridCommandEventArgs e)
        {
            this.dgItemDetail.EditItemIndex = -1;
            BindItemDetails();
        }
        protected void dgItemDetail_DeleteCommand(object source, DataGridCommandEventArgs e)
        {



        }
        protected void dgItemDetail_EditCommand(object source, DataGridCommandEventArgs e)
        {
            this.dgItemDetail.EditItemIndex = e.Item.ItemIndex;
            BindItemDetails();
        }
        protected void dgItemDetail_ItemCommand(object source, DataGridCommandEventArgs e)
        {

        }
        protected void dgItemDetail_ItemDataBound(object sender, DataGridItemEventArgs e)
        {


            if (e.Item.ItemType == ListItemType.Footer)
            {



            }
            else
            {


                if (_issue.ItemDetail != null)
                {
                    LinkButton lbt = e.Item.FindControl("lnkDelete") as LinkButton;
                    if (lbt != null)
                        lbt.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this Recieve Detail?');");




                }




            }




        }
        protected void dgItemDetail_UpdateCommand(object source, DataGridCommandEventArgs e)
        {
            IssueItemDetail ItemDetail = _issue.ItemDetail[e.Item.DataSetIndex];
            try
            {

                TextBox txtApprovalqty = e.Item.FindControl("txtIssuedQty") as TextBox;
                ItemDetail.IssuedQty = Convert.ToInt32(txtApprovalqty.Text);
                _issue.ItemDetail.Insert(Convert.ToInt32(e.Item.DataSetIndex), ItemDetail);
                _issue.ItemDetail.RemoveAt(e.Item.DataSetIndex);
                Master.ShowMessage(new AppMessage("Sparepart Detail  Updated successfully.", Chai.Maintenance.Enums.RMessageType.Info));
                dgItemDetail.EditItemIndex = -1;
                BindItemDetails();
            }
            catch (Exception ex)
            {
                Master.ShowMessage(new AppMessage("Error: Unable to Update Sparepart Detail item Detail. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }




        }

        #endregion



        public IList<IssueItemDetail> _issueitemdetail
        {
            get { return _issueDetail;  }
            set { _issueDetail = value; }
        }



      













        protected void btnPrint_Click(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/SparepartInventory/PrintIssue.aspx?{0}=3&IssueId={1}&Approver={2}&Requester={3}", AppConstants.TABID, issue.Id,issue.Approver,issue.IssuedFor));
        }
}
}