﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{

    public partial class ViewApprovals : Microsoft.Practices.CompositeWeb.Web.UI.Page, IApprovalListView
    {
        private ApprovalListPresenter _presenter;
        private IList<Approval> _approval;
        private User _user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();
            BindRequester();
        }
        [CreateNew]
        public ApprovalListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        private void BindRequester()
        {
            ddlRequester.DataSource = _presenter.GetUsers(_user.RegionId);
            ddlRequester.DataBind();
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetApprovalforIssue(Convert.ToInt32(ddlSelect.SelectedValue), txtvalue.Text, Calendar1.Text, Calendar2.Text,_user.RegionId,Convert.ToInt32(ddlRequester.SelectedValue));
            grvApprovalList.DataSource = _approval;
            grvApprovalList.DataBind();
        }
        protected void ddlSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlSelect.SelectedValue) == 2)
            {
                Calendar1.Visible = true;
                Calendar2.Visible = true;
                Img.Visible = true;
                Img1.Visible = true;
                txtvalue.Visible = false;
                lblDateFrom.Visible = true;
                lblDateTo.Visible = true;
                lblValue.Visible = false;
                lblRequester.Visible = false;
                ddlRequester.Visible = false;
                inlineapprover.Visible = false;
            }
            else if (Convert.ToInt32(ddlSelect.SelectedValue) == 3)
            {
                Calendar1.Visible = false;
                Calendar2.Visible = false;
                Img.Visible = false;
                Img1.Visible = false;
                txtvalue.Visible = false;
                lblDateFrom.Visible = false;
                lblDateTo.Visible = false;
                lblValue.Visible = false;
                lblRequester.Visible = true;
                ddlRequester.Visible = true;
                inlineapprover.Visible = true;
            }
            else
            {
                Calendar1.Visible = false;
                Calendar2.Visible = false;
                Img.Visible = false;
                Img1.Visible = false;
                txtvalue.Visible = true;
                lblDateFrom.Visible = false;
                lblDateTo.Visible = false;
                lblValue.Visible = true;
                lblRequester.Visible = false;
                ddlRequester.Visible = false;
                inlineapprover.Visible = false;
            }
        }
        protected void grvApprovalList_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/SparepartInventory/IssueEdit.aspx?{0}=3&ApprovalId={1}&Requester={2}&Approver={3}", AppConstants.TABID, Convert.ToInt32(grvApprovalList.SelectedDataKey["Id"]), Convert.ToInt32(grvApprovalList.SelectedDataKey["RequestedBy"]), Convert.ToInt32(grvApprovalList.SelectedDataKey["Approver"])));
        }

        public IList<Approval> approval
        {
            set { _approval = value; }
        }

        public User user
        {
            set { _user = value; }
        }
       
        protected void grvApprovalList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvApprovalList.PageIndex = e.NewPageIndex;
            grvApprovalList.DataSource = _approval;
            grvApprovalList.DataBind();
        }
}
}