﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="IssueEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.SparepartInventory.Views.IssueEdit" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
            HeaderText="Error" ValidationGroup="1" Height="68px" class="alert alert-danger fade in"/>
       <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Issue</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   

                        <asp:Label ID="lblIssueNo" runat="server" Text="Issue No." CssClass="label"></asp:Label>
                    <label class="input">
                        <asp:TextBox ID="txtIssueNo" runat="server" 
                             Enabled="False"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RfvIssueNo" runat="server" 
                            ControlToValidate="txtIssueNo" Display="Dynamic" 
                            ErrorMessage="Request No. Required" ValidationGroup="1">*</asp:RequiredFieldValidator>
                    </label></section>
                    <section class="col col-6">  
                        <asp:Label ID="lblapprover" runat="server" Text="Approver" CssClass="label"></asp:Label>
                   <label class="input">
                        <asp:TextBox ID="lblapproverresult" runat="server" ReadOnly="true"
                           ></asp:TextBox></label></section></div>
                      <div class="row">
									<section class="col col-6">   

                        <asp:Label ID="lblIssueDate" runat="server" Text="Issue Date" CssClass="label"></asp:Label>
              <label class="input">
                   <i class="icon-append fa fa-calendar"></i>
                     <asp:TextBox ID="CalIssueDate"  runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"
                         ></asp:TextBox>
                   
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" 
                            runat="server" ControlToValidate="CalIssueDate" ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator></label></section>
                      <section class="col col-6"> 
                        <asp:Label ID="lblIssuedfor" runat="server" Text="Requester" CssClass="label"></asp:Label>
                    <label class="input">
                        <asp:TextBox ID="lblIssuedforesult" runat="server" 
                          ></asp:TextBox></label>
                       </section></div>
                       <div class="row">
									<section class="col col-6">   
                        <asp:Label ID="lblToolKeeper" runat="server" Text="Tool Keeper" 
                            CssClass="label"></asp:Label>
                   <label class="input">
                        <asp:TextBox ID="lbltoolkeeperresult" runat="server"
                           ></asp:TextBox></label></section></div></fieldset>
                       
            <asp:DataGrid ID="dgItemDetail" runat="server" AutoGenerateColumns="False" 
                BorderColor="#003366" BorderStyle="Solid" BorderWidth="1px" CellPadding="0" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                DataKeyField="Id" ForeColor="#333333" GridLines="None" 
                oncancelcommand="dgItemDetail_CancelCommand" 
                ondeletecommand="dgItemDetail_DeleteCommand" 
                oneditcommand="dgItemDetail_EditCommand" 
                onitemcommand="dgItemDetail_ItemCommand" 
                onitemdatabound="dgItemDetail_ItemDataBound" 
                onupdatecommand="dgItemDetail_UpdateCommand" ShowFooter="True" 
                Width="100%">
               
                <Columns>
                    <asp:TemplateColumn HeaderText="Instrument Name">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Sparepart Name">
                        <EditItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "SparepartName")%>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "SparepartName")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Approved Qty">
                       
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "ApprovedQty")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Stock Qty">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "StockQty")%>
                        </ItemTemplate>
                       
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Issue Qty">
                        <EditItemTemplate>
                            &nbsp;
                            <table align="left" style="width: 56%; margin-left: 3px;">
                                <tr>
                                    <td class="editTextBox" style="width: 83px">
                                        <asp:Label ID="lblStockQty" runat="server" Text="Stock Qty"></asp:Label>
                                    </td>
                                    <td class="editDropDown" style="width: 81px">
                                        <asp:TextBox ID="txtStockQty" runat="server" Enabled="False" Height="16px" 
                                            Text='<%# DataBinder.Eval(Container.DataItem, "StockQty")%>' 
                                            ValidationGroup="3" Width="67px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:CompareValidator ID="CvissueQty" runat="server" 
                                            ControlToCompare="txtStockQty" ControlToValidate="txtIssuedQty" 
                                            Display="Dynamic" ErrorMessage="Issued Qty Must be Less Than Stock Qty" 
                                            Operator="LessThan" SetFocusOnError="True" ValidationGroup="3" 
                                            ValueToCompare="txtStockQty" Type="Integer"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="editTextBox" style="width: 83px">
                                        <asp:Label ID="lblApprovedQty" runat="server" Text="Approved Qty"></asp:Label>
                                    </td>
                                    <td class="editDropDown" style="width: 81px">
                                        <asp:TextBox ID="txtApprovedQty" runat="server" Enabled="False" Height="16px" 
                                            Width="67px" Text='<%# DataBinder.Eval(Container.DataItem, "ApprovedQty")%>' 
                                            ValidationGroup="3"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="RfvQty" runat="server" 
                                            ControlToValidate="txtIssuedQty" ErrorMessage="Qty Required" InitialValue="0" 
                                            ValidationGroup="3">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="editTextBox" style="width: 83px">
                                        <asp:Label ID="lblIssueQty" runat="server" Text="Issue Qty"></asp:Label>
                                    </td>
                                    <td class="editDropDown" style="width: 81px">
                                        <asp:TextBox ID="txtIssuedQty" runat="server" Height="16px" 
                                            Text=' <%# DataBinder.Eval(Container.DataItem, "IssuedQty")%>' 
                                            ValidationGroup="3" Width="67px"></asp:TextBox>
                                        <cc2:FilteredTextBoxExtender ID="txtIssuedQty_FilteredTextBoxExtender" 
                                            runat="server" FilterType="Numbers" TargetControlID="txtIssuedQty">
                                        </cc2:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:CompareValidator ID="CvApprovedQty" runat="server" 
                                            ControlToCompare="txtApprovedQty" ControlToValidate="txtIssuedQty" 
                                            Display="Dynamic" 
                                            ErrorMessage="Issued Qty must be Less Than or Equal to Approved Qty" 
                                            Operator="LessThanEqual" ValueToCompare="txtApprovedQty" Type="Integer" 
                                            ValidationGroup="3"></asp:CompareValidator>
                                    </td>
                                </tr>
                            </table>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "IssuedQty")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <EditItemTemplate>
                            <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="Update" 
                                ValidationGroup="3">Update</asp:LinkButton>
                            &nbsp;
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                            &nbsp;
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                 <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
            </asp:DataGrid>
                <footer>
                          <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Issue" 
                    ValidationGroup="1"  Cssclass="btn btn-primary"/>
                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Cssclass="btn btn-primary"
                    onclick="btnCancel_Click" Text="Cancel" />
                <asp:Button ID="btnPrint" runat="server" onclick="btnPrint_Click" Cssclass="btn btn-primary"
                    Text="Print" />
                <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" Cssclass="btn btn-primary"
                    Text="Delete" />
         </footer>
      
                                </div>
                                </div>
                                </div>  
    </div>
   
              
          
</asp:Content>

