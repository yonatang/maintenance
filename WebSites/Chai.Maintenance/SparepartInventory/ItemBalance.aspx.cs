﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{


    public partial class ItemBalance : Microsoft.Practices.CompositeWeb.Web.UI.Page, IItemBalanceView
    {
        private ItemBalancePresenter _presenter;
        private IList<StockQty> _itembalance;
        private User _user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
                 BindSparepart();
            }
            this._presenter.OnViewLoaded();
            
        }

        private void BindSparepart()
        {
           ddlInstrument.DataSource = _presenter.GetInstrumentName();
            ddlInstrument.DataBind();
        }
        [CreateNew]
        public ItemBalancePresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        protected void ddlInstrument_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlSparepart.Items.Count > 0)
            {
                ListItem lst = new ListItem();
                ddlSparepart.Items.Clear();
                lst.Text = "Select Sparepart";
                lst.Value = "0";
                ddlSparepart.Items.Add(lst);
            }
            ddlSparepart.DataSource = _presenter.GetSparepart(Convert.ToInt32(ddlInstrument.SelectedValue));
            ddlSparepart.DataBind();
        }
        protected void btnView_Click(object sender, EventArgs e)
        {
            _itembalance = _presenter.GetItembalance(Convert.ToInt32(ddlInstrument.SelectedValue), Convert.ToInt32(ddlSparepart.SelectedValue),_user.RegionId);
            grvItemBalanceList.DataSource = _itembalance;
            grvItemBalanceList.DataBind();
        }

        public User user
        {
            set { _user = value; }
        }
    }
}