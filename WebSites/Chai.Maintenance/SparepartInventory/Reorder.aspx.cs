﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{


    public partial class Reorder : Microsoft.Practices.CompositeWeb.Web.UI.Page, IReorderView
    {
        private ReorderPresenter _presenter;
        private IList<StockQty> _itembalance;
        private User _user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();
            grvItemBalanceList.DataSource = _itembalance;
            grvItemBalanceList.DataBind();
          
        }
        [CreateNew]
        public ReorderPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        public IList<StockQty> _stockqty
        {
            set { _itembalance = value; }
        }


        public User user
        {
            set { _user = value; }
        }
    }
}