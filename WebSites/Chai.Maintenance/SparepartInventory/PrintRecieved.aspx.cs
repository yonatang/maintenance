﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{

public partial class PrintRecieved :  Microsoft.Practices.CompositeWeb.Web.UI.Page, IRecieveEditView
{
    private RecievePresenter _presenter;
    private  Recieve _recieve;
    User _user;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this._presenter.OnViewInitialized();
           btnPrint.Attributes.Add("onclick", "javascript:Clickheretoprint('divprint'); return false;");
           BindJS();
        }
      
        this._presenter.OnViewLoaded();
        BindRecieveControls();
        BindItemDetails();
      
    }
     [CreateNew]
    public RecievePresenter Presenter
    {
        get
        {
            return this._presenter;
        }
        set
        {
            if (value == null)
                throw new ArgumentNullException("value");

            this._presenter = value;
            this._presenter.View = this;
        }
    }

     private void BindRecieveControls()
     {

         this.lblRecieptNoresult.Text = _recieve.ReceiptNo;
         this.lblRecievedDateresult.Text = _recieve.ReceiveDate.ToShortDateString();
         this.lblLetterNoresult.Text = _recieve.LetterNo;
         this.lblPurchaseNoresult.Text = _recieve.PurchaseNo;
       
         this.lblSourceofFinanceresult.Text = _recieve.SourceOfFinanance;
         this.lblToolKeeperresult.Text = _user.FullName;
       


     }
     private void BindItemDetails()
     {
         dgItemDetail.DataSource = _recieve.ItemDetail;
         dgItemDetail.DataBind();
     }
    public int  Id
{
     get
        {
            if (Convert.ToInt32(Request.QueryString["RecieveId"]) != 0)
                return Convert.ToInt32(Request.QueryString["RecieveId"]);
            else
                return 0;
           } 
}

public Recieve  recieve
{
	  get 
	{ 
		return _recieve;
	}
	  set 
	{ 
		_recieve = value;
	}
}

public int  ddlInstrumentId
{
    get { return 0; }
}

public User  user
{
    set { _user = value; }
    }
protected void BindJS()
{
    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Maintenancescripts", String.Format("<script language=\"JavaScript\" src=\"http://localhost/maintenance/Maintenance.js\"></script>\n"));
}


}
}