﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
public partial class RecieveEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IRecieveEditView
{
    private RecievePresenter _presenter;
    private  Recieve _recieve;
    User _user;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this._presenter.OnViewInitialized();
           

        }
        this._presenter.OnViewLoaded();
        if (_recieve.Id > 0)
        {
            btnPrint.Enabled = true;
        }
        lblToolKeeperName.Text = _user.FullName;
        if (_recieve.Id > 0 && _recieve.IsPosted == false)
        {
            btnPost.Enabled = true;
        }
        if (_recieve.IsPosted == true)
        {
            
            btnDelete.Enabled = false;
            btnPost.Enabled = false;
            dgItemDetail.Enabled = false;
        }
        if (_recieve.SourceOfFinanance != "Purchased")
        {
            txtPurchaseNo.Enabled = false;
            txtTendorNo.Enabled = false;
            txtLetterNo.Enabled = true;
            txtSourcedesc.Enabled = true;

        }
        else
        {
            txtPurchaseNo.Enabled = true;
            txtTendorNo.Enabled = true;
            txtLetterNo.Enabled = false;
            txtSourcedesc.Enabled = false;
        }
        if (!IsPostBack)
        { 
            BindRecieveControls();
            BindItemDetails();
            Bindvendor();
        }
        
    }
    [CreateNew]
    public RecievePresenter Presenter
    {
        get
        {
            return this._presenter;
        }
        set
        {
            if (value == null)
                throw new ArgumentNullException("value");

            this._presenter = value;
            this._presenter.View = this;
        }
    }
    private void Bindvendor()
    {
        
        ddlVendor.DataSource = _presenter.GetVendor();
        ddlVendor.DataBind();
    }
    private void BindInstrument(DropDownList ddlInstrument)
    {
        ddlInstrument.DataSource = _presenter.GetInstrumentName();
        ddlInstrument.DataBind();
    }
    private void BindManufacturer(DropDownList ddlManufacturer)
    {
        ddlManufacturer.DataSource = _presenter.getManufacturer();
        ddlManufacturer.DataBind();
    }
    private void BindSparepart(DropDownList ddlSparepart,int InsId)
    {
        ddlSparepart.DataSource = _presenter.GetSparepart(InsId);
        ddlSparepart.DataBind();
    }
    private void BindRecieveControls()
    {

        this.txtRecieptNo.Text = _recieve.ReceiptNo;
        this.CalRecievedDateDate.Text = _recieve.ReceiveDate.ToString();
        this.txtVendorAgent.Text = _recieve.VendorAgent;
        this.txtSourcedesc.Text = _recieve.SourceDescription;
        this.txtLetterNo.Text = _recieve.LetterNo;
        this.txtPurchaseNo.Text = _recieve.PurchaseNo;
        this.txtTendorNo.Text = _recieve.TendorNo;
        this.ddlSourceofFinance.SelectedValue = _recieve.SourceOfFinanance;
        this.ddlVendor.SelectedValue = _recieve.VendorId.ToString();
        
        this.btnDelete.Visible = (_recieve.Id > 0);
        this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


    }
   
    public int Id
    {
        get
        {
            if (Convert.ToInt32(Request.QueryString["RecieveId"]) != 0)
                return Convert.ToInt32(Request.QueryString["RecieveId"]);
            else
                return 0;
           }
    }

    public CoreDomain.SparepartInventory.Recieve recieve
    {
        get
        {
            return _recieve;
        }
        set
        {
            _recieve = value;
        }
    }

    public int ddlInstrumentId
    {
        get { return 0; }
    }
    private void SaveRecieve()
    {
        try
        {
            if (_recieve.IsNew())
            {
                if (_recieve.ItemDetail.Count != 0)
                {
                    _presenter.SaveorUpdateRecieve(_recieve);
                    Master.ShowMessage(new AppMessage("Recieve Info. saved", RMessageType.Info));
                    btnPost.Enabled = true;
                   
                }
                else
                {
                    Master.ShowMessage(new AppMessage("You Must Add Sparepart Detail", RMessageType.Error));
                
                }
            }
            else
            {
                if (_recieve.ItemDetail.Count != 0)
                {
                    _presenter.SaveorUpdateRecieve(_recieve);
                    Master.ShowMessage(new AppMessage("Recieve Info. saved", RMessageType.Info));
                    btnPost.Enabled = true;
                  
                }
                else
                {
                    Master.ShowMessage(new AppMessage("You Must Add Sparepart Detail", RMessageType.Error));

                }
            }
        }
        catch (Exception ex)
        {
            Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (this.IsValid)
        {
           _recieve.ReceiptNo=this.txtRecieptNo.Text ;
           _recieve.ReceiveDate = Convert.ToDateTime(this.CalRecievedDateDate.Text);
           _recieve.VendorAgent= this.txtVendorAgent.Text  ;
            _recieve.SourceDescription=this.txtSourcedesc.Text ;
            _recieve.LetterNo=this.txtLetterNo.Text  ;
            _recieve.PurchaseNo=this.txtPurchaseNo.Text ;
            _recieve.TendorNo=this.txtTendorNo.Text  ;
            _recieve.SourceOfFinanance = this.ddlSourceofFinance.SelectedValue;
            _recieve.VendorId =Convert.ToInt32(this.ddlVendor.SelectedValue);
            _recieve.ToolkeeperId = _user.Id;
            _recieve.RegionId = _user.RegionId;
             SaveRecieve();

             btnPrint.Enabled = true;
             btnPost.Enabled = true;
        }	    
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        _presenter.CancelPage();
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            _presenter.DeleteRecieve(_recieve.Id);
            Master.ShowMessage(new AppMessage("Recieve Info. was deleted successfully", Chai.Maintenance.Enums.RMessageType.Info));
            
        }
        catch (Exception ex)
        {
            Master.ShowMessage(new AppMessage("Error: Unable to delete Recieve Info. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
        }
    }
    protected void btnPost_Click(object sender, EventArgs e)
    {
        try
        {
           
            if (_recieve.ItemDetail.Count != 0)
            {
                _presenter.PostItems(_recieve.ItemDetail, _recieve.RegionId,_recieve.Id);
                Master.ShowMessage(new AppMessage("Sparepart Qty successfully Posted", Chai.Maintenance.Enums.RMessageType.Info));
                btnPost.Enabled = false;
            }
            else 
            
            {
                Master.ShowMessage(new AppMessage("There is no Sparepart to Post", Chai.Maintenance.Enums.RMessageType.Info));
               
            }
        }
        catch (Exception ex)
        {
            Master.ShowMessage(new AppMessage("Error: Unable to Post Sparepart Qty. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
        }

    }
    #region ItemDetail
    private void BindItemDetails()
    {
        dgItemDetail.DataSource = _recieve.ItemDetail;
        dgItemDetail.DataBind();
    }
    protected void dgItemDetail_CancelCommand(object source, DataGridCommandEventArgs e)
    {
        this.dgItemDetail.EditItemIndex = -1;
        BindItemDetails();
    }
    protected void dgItemDetail_DeleteCommand(object source, DataGridCommandEventArgs e)
    {
        
       
        try
        {
            LinkButton lbt = e.Item.FindControl("lnkDelete") as LinkButton;
            if (lbt != null)
                lbt.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this Recieve Detail?');");
            
            if (_recieve.ItemDetail[e.Item.DataSetIndex].Id != 0)
            {
                _presenter.DeleteRecieveDetail(_recieve.ItemDetail[e.Item.DataSetIndex].Id);
            }
            _recieve.ItemDetail.Remove(_recieve.ItemDetail[Convert.ToInt32(e.Item.DataSetIndex)]);
            BindItemDetails();

            Master.ShowMessage(new AppMessage("Sparepart item Detail was Removed successfully", Chai.Maintenance.Enums.RMessageType.Info));
        }
        catch(Exception ex)
        {
            Master.ShowMessage(new AppMessage("Error: Unable to delete Sparepart item Detail. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
        }
    }
    protected void dgItemDetail_EditCommand(object source, DataGridCommandEventArgs e)
    {
        this.dgItemDetail.EditItemIndex = e.Item.ItemIndex;
        
        BindItemDetails();
    }
    protected void dgItemDetail_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "AddNew")
        {
            try
            {
                RecieveItemDetail ItemDetail = new RecieveItemDetail();
                DropDownList ddlInstrument = e.Item.FindControl("ddlFInstrument") as DropDownList;
                ItemDetail.InstrumentId = int.Parse(ddlInstrument.SelectedValue);
                ItemDetail.InstrumentName = ddlInstrument.SelectedItem.Text;
                DropDownList ddlSparepart = e.Item.FindControl("ddlFSparepart") as DropDownList;
                ItemDetail.SparepartId = int.Parse(ddlSparepart.SelectedValue);
                ItemDetail.SparepartName = ddlSparepart.SelectedItem.Text;
                DropDownList ddlmanufacturer = e.Item.FindControl("ddlFManufacturer") as DropDownList;
                ItemDetail.ManfacturerId = int.Parse(ddlmanufacturer.SelectedValue);
                ItemDetail.Manufacturer = ddlmanufacturer.SelectedItem.Text;
                TextBox txtqty = e.Item.FindControl("txtFQty") as TextBox;
                ItemDetail.Qty = Convert.ToInt32(txtqty.Text);
                TextBox txtUnitPrice = e.Item.FindControl("txtFUnitPrice") as TextBox;
                ItemDetail.UnitPrice = Convert.ToDecimal(txtUnitPrice.Text);
                ItemDetail.TotalPrice = Convert.ToDecimal((Convert.ToInt32(txtqty.Text) * Convert.ToDecimal(txtUnitPrice.Text)));
                _recieve.ItemDetail.Add(ItemDetail);
                Master.ShowMessage(new AppMessage("Sparepart Detail  added successfully.", Chai.Maintenance.Enums.RMessageType.Info));
                dgItemDetail.EditItemIndex = -1;
                BindItemDetails();
            }
            catch(Exception ex)
            {
                Master.ShowMessage(new AppMessage("Error: Unable to Add Sparepart item Detail. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
    }
    protected void dgItemDetail_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
       

        if (e.Item.ItemType == ListItemType.Footer)
        {
            DropDownList ddlinstrument = e.Item.FindControl("ddlFInstrument") as DropDownList;
            BindInstrument(ddlinstrument);
            DropDownList ddlmanufacturer = e.Item.FindControl("ddlFManufacturer") as DropDownList;
            BindManufacturer(ddlmanufacturer);
            DropDownList ddlSparepart = e.Item.FindControl("ddlFSparepart") as DropDownList;
            //BindSparepart(ddlSparepart);
            if (Convert.ToInt32(ddlinstrument.SelectedValue) != 0)
            {
                if (ddlSparepart.Items.Count > 0)
                {
                    ddlSparepart.Items.Clear();
                }
                ListItem lst = new ListItem();
                lst.Text = "Select Sparepart";
                lst.Value = "0";
                ddlSparepart.DataSource = _presenter.GetSparepart(Convert.ToInt32(ddlinstrument.SelectedValue));
                ddlSparepart.DataBind();
            }
        }
            else
            {
                

                    if (_recieve.ItemDetail != null)
                    {
                        LinkButton lbt = e.Item.FindControl("lnkDelete") as LinkButton;
                        if (lbt != null)
                            lbt.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this Recieve Detail?');");

                        DropDownList ddlinstrument = e.Item.FindControl("ddlInstrument") as DropDownList;
                        if (ddlinstrument != null)
                        {
                            BindInstrument(ddlinstrument);
                            if (_recieve.ItemDetail[e.Item.DataSetIndex].InstrumentId != null)
                            {
                                ListItem liI = ddlinstrument.Items.FindByValue(_recieve.ItemDetail[e.Item.DataSetIndex].InstrumentId.ToString());
                                if (liI != null)
                                    liI.Selected = true;
                            }

                        }
                        DropDownList ddlSparepart = e.Item.FindControl("ddlSparepart") as DropDownList;
                        if (ddlSparepart != null)
                        {

                            BindSparepart(ddlSparepart, Convert.ToInt32(ddlinstrument.SelectedValue));
                            if (_recieve.ItemDetail[e.Item.DataSetIndex].SparepartId != null)
                            {
                                ListItem lis = ddlSparepart.Items.FindByValue(_recieve.ItemDetail[e.Item.DataSetIndex].SparepartId.ToString());
                                if (lis != null)
                                    lis.Selected = true;
                            }
                        }
                        
                        DropDownList ddlmanufacturer = e.Item.FindControl("ddlManufacturer") as DropDownList;
                        if (ddlmanufacturer != null)
                        {
                            BindManufacturer(ddlmanufacturer);
                            if (_recieve.ItemDetail[e.Item.DataSetIndex].ManfacturerId != null)
                            {
                                ListItem li = ddlmanufacturer.Items.FindByValue(_recieve.ItemDetail[e.Item.DataSetIndex].ManfacturerId.ToString());
                                if (li != null)
                                    li.Selected = true;
                            }
                        }
                    
                }
                    
            }
        
        
    }
    protected void dgItemDetail_UpdateCommand(object source, DataGridCommandEventArgs e)
    {
        RecieveItemDetail ItemDetail  = _recieve.ItemDetail[e.Item.DataSetIndex];
         try
          {
              DropDownList ddlInstrument = e.Item.FindControl("ddlInstrument") as DropDownList;
              ItemDetail.InstrumentId = int.Parse(ddlInstrument.SelectedValue);
              ItemDetail.InstrumentName = ddlInstrument.SelectedItem.Text;
              DropDownList ddlSparepart = e.Item.FindControl("ddlSparepart") as DropDownList;
              ItemDetail.SparepartId = int.Parse(ddlSparepart.SelectedValue);
              ItemDetail.SparepartName = ddlSparepart.SelectedItem.Text;
              DropDownList ddlmanufacturer = e.Item.FindControl("ddlManufacturer") as DropDownList;
              ItemDetail.ManfacturerId = int.Parse(ddlmanufacturer.SelectedValue);
              ItemDetail.Manufacturer = ddlmanufacturer.SelectedItem.Text;
              TextBox txtqty = e.Item.FindControl("txtQty") as TextBox;
              ItemDetail.Qty = Convert.ToInt32(txtqty.Text);
              TextBox txtUnitPrice = e.Item.FindControl("txtUnitPrice") as TextBox;
              ItemDetail.UnitPrice = Convert.ToDecimal(txtUnitPrice.Text);
              ItemDetail.TotalPrice = Convert.ToDecimal((Convert.ToInt32(txtqty.Text) * Convert.ToDecimal(txtUnitPrice.Text)));
             
              _recieve.ItemDetail.Insert(Convert.ToInt32(e.Item.DataSetIndex), ItemDetail);
              _recieve.ItemDetail.RemoveAt(e.Item.DataSetIndex);
              Master.ShowMessage(new AppMessage("Sparepart Detail  Updated successfully.", Chai.Maintenance.Enums.RMessageType.Info));
              dgItemDetail.EditItemIndex = -1;
              BindItemDetails();
          }
          catch (Exception ex)
          {
              Master.ShowMessage(new AppMessage("Error: Unable to Update Sparepart Detail item Detail. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
          }

        

        
    }

    #endregion


    protected void ddlFInstrument_SelectedIndexChanged(object sender, EventArgs e)
    {
                 
          DropDownList ddlInstrument = (DropDownList)sender;
          DataGridItem dgFooter = (DataGridItem)ddlInstrument.Parent.Parent;
          DropDownList ddlSparepart = (DropDownList)dgFooter.FindControl("ddlFSparepart");
      
        if (ddlSparepart.Items.Count > 0)
        {
            ddlSparepart.Items.Clear();
        }
        ListItem lst = new ListItem();
        lst.Text = "Select Sparepart";
        lst.Value = "0";
        ddlSparepart.Items.Add(lst);
        ddlSparepart.DataSource = _presenter.GetSparepart(Convert.ToInt32(ddlInstrument.SelectedValue));
        ddlSparepart.DataBind();
    }
    protected void ddlInstrument_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DropDownList ddlInstrument = dgItemDetail.FindControl("ddlInstrument") as DropDownList;
        
        //DropDownList ddlSparepart = dgItemDetail.FindControl("ddlSparepart") as DropDownList;
        //if (ddlSparepart.Items.Count > 0)
        //{
        //    ddlSparepart.Items.Clear();
        //}
        //ListItem lst = new ListItem();
        //lst.Text = "Select Spareprt";
        //lst.Value = "0";
        //ddlSparepart.DataSource = _presenter.GetSparepart(Convert.ToInt32(ddlInstrument.SelectedValue));
        //ddlSparepart.DataBind();

    }
    protected void ddlFInstrument_DataBinding(object sender, EventArgs e)
    {

    }

    protected void ddlSourceofFinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSourceofFinance.SelectedItem.Text != "Purchased")
        {
            txtPurchaseNo.Enabled = false;
            txtTendorNo.Enabled = false;
            txtLetterNo.Enabled = true;
            txtSourcedesc.Enabled = true;

        }
        else 
        
        {
            txtPurchaseNo.Enabled = true;
            txtTendorNo.Enabled = true;
            txtLetterNo.Enabled = false;
            txtSourcedesc.Enabled = false;
        }
    }


    public User user
    {
        set { _user = value; }
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        if (recieve.Id > 0)
        {
            Response.Redirect(String.Format("~/SparepartInventory/PrintRecieved.aspx?{0}=3&RecieveId={1}", AppConstants.TABID, recieve.Id));
        }
    }
}
}  