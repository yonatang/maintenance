﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
   public partial class BeginningBalance : Microsoft.Practices.CompositeWeb.Web.UI.Page, IBeginingBalanceView
     {
       private BeginingBalancePresenter _presenter;
       private IList<StockQty> _beginingbalancelist;
       private StockQty _beginingbalance;
       private User _user;
       protected void Page_Load(object sender, EventArgs e)
         {
             if (!IsPostBack)
             {
                 _presenter.OnViewInitialized();
                 BindInstrumentforList();
             }
             _presenter.OnViewLoaded();
             if (!IsPostBack)
             {
                 BindItemDetails();
             }
         }
       [CreateNew]
       public BeginingBalancePresenter Presenter
       {
           get
           {
               return this._presenter;
           }
           set
           {
               if (value == null)
                   throw new ArgumentNullException("value");

               this._presenter = value;
               this._presenter.View = this;
           }
       }
       private void BindInstrumentforList()
       {
           ddlInstrumentforList.DataSource = _presenter.GetInstrumentName();
           ddlInstrumentforList.DataBind();
       }
       private void BindInstrument(DropDownList ddlInstrument)
       {
           ddlInstrument.DataSource = _presenter.GetInstrumentName();
           ddlInstrument.DataBind();
       }
       
       private void BindSparepart(DropDownList ddlSparepart, int InsId)
       {
           ddlSparepart.DataSource = _presenter.GetSparepart(InsId);
           ddlSparepart.DataBind();
       }
       #region ItemDetail
       private void BindItemDetails()
       {
           
           dgItemDetail.DataSource = _beginingbalancelist;
           dgItemDetail.DataBind();
       }
       protected void dgItemDetail_CancelCommand(object source, DataGridCommandEventArgs e)
       {
           this.dgItemDetail.EditItemIndex = -1;
           BindItemDetails();
       }
       protected void dgItemDetail_DeleteCommand(object source, DataGridCommandEventArgs e)
       {


           try
           {
               LinkButton lbt = e.Item.FindControl("lnkDelete") as LinkButton;
               if (lbt != null)
                   lbt.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this Recieve Detail?');");

               if (_beginingbalancelist[e.Item.DataSetIndex].Id != 0)
               {
                   _presenter.DeleteStockQty(_beginingbalancelist[e.Item.DataSetIndex].Id);
               }
               _beginingbalancelist.Remove(_beginingbalancelist[Convert.ToInt32(e.Item.DataSetIndex)]);
               BindItemDetails();

               Master.ShowMessage(new AppMessage("Sparepart item Detail was Removed successfully", Chai.Maintenance.Enums.RMessageType.Info));
           }
           catch (Exception ex)
           {
               Master.ShowMessage(new AppMessage("Error: Unable to delete Sparepart item Detail. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
           }
       }
       protected void dgItemDetail_EditCommand(object source, DataGridCommandEventArgs e)
       {
           this.dgItemDetail.EditItemIndex = e.Item.ItemIndex;

           BindItemDetails();
       }
       protected void dgItemDetail_ItemCommand(object source, DataGridCommandEventArgs e)
       {
           if (e.CommandName == "AddNew")
           {
               try
               {
                   StockQty ItemDetail = new StockQty();
                   DropDownList ddlInstrument = e.Item.FindControl("ddlFInstrument") as DropDownList;
                   ItemDetail.InstrumentId = int.Parse(ddlInstrument.SelectedValue);
                   ItemDetail.InstrumentName = ddlInstrument.SelectedItem.Text;
                   DropDownList ddlSparepart = e.Item.FindControl("ddlFSparepart") as DropDownList;
                   ItemDetail.SparepartId = int.Parse(ddlSparepart.SelectedValue);
                   ItemDetail.SparepartName = ddlSparepart.SelectedItem.Text;
                   TextBox txtqty = e.Item.FindControl("txtFQty") as TextBox;
                   ItemDetail.Qty = Convert.ToInt32(txtqty.Text);
                   ItemDetail.RegionId = _user.RegionId;
                   _presenter.SaveorUpdateStockQty(ItemDetail);
                   Master.ShowMessage(new AppMessage("Sparepart Detail  added successfully.", Chai.Maintenance.Enums.RMessageType.Info));
                   dgItemDetail.EditItemIndex = -1;
                   _presenter.OnViewInitialized();
                   _presenter.OnViewLoaded();
                   BindItemDetails();
               }
               catch (Exception ex)
               {
                   Master.ShowMessage(new AppMessage("Error: Unable to Add Sparepart item Detail. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
               }
           }
       }
       protected void dgItemDetail_ItemDataBound(object sender, DataGridItemEventArgs e)
       {


           if (e.Item.ItemType == ListItemType.Footer)
           {
               DropDownList ddlinstrument = e.Item.FindControl("ddlFInstrument") as DropDownList;
               BindInstrument(ddlinstrument);
               DropDownList ddlSparepart = e.Item.FindControl("ddlFSparepart") as DropDownList;
               //BindSparepart(ddlSparepart);
               if (Convert.ToInt32(ddlinstrument.SelectedValue) != 0)
               {
                   if (ddlSparepart.Items.Count > 0)
                   {
                       ddlSparepart.Items.Clear();
                   }
                   ListItem lst = new ListItem();
                   lst.Text = "Select Sparepart";
                   lst.Value = "0";
                   ddlSparepart.DataSource = _presenter.GetSparepart(Convert.ToInt32(ddlinstrument.SelectedValue));
                   ddlSparepart.DataBind();
               }
           }
           else
           {


               if (_beginingbalancelist != null)
               {
                   LinkButton lbt = e.Item.FindControl("lnkDelete") as LinkButton;
                   if (lbt != null)
                       lbt.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this Recieve Detail?');");

                   DropDownList ddlinstrument = e.Item.FindControl("ddlInstrument") as DropDownList;
                   if (ddlinstrument != null)
                   {
                       BindInstrument(ddlinstrument);
                       if (_beginingbalancelist[e.Item.DataSetIndex].InstrumentId != 0)
                       {
                           ListItem liI = ddlinstrument.Items.FindByValue(_beginingbalancelist[e.Item.DataSetIndex].InstrumentId.ToString());
                           if (liI != null)
                               liI.Selected = true;
                       }

                   }
                   DropDownList ddlSparepart = e.Item.FindControl("ddlSparepart") as DropDownList;
                   if (ddlSparepart != null)
                   {

                       BindSparepart(ddlSparepart, Convert.ToInt32(ddlinstrument.SelectedValue));
                       if (_beginingbalancelist[e.Item.DataSetIndex].SparepartId != 0)
                       {
                           ListItem lis = ddlSparepart.Items.FindByValue(_beginingbalancelist[e.Item.DataSetIndex].SparepartId.ToString());
                           if (lis != null)
                               lis.Selected = true;
                       }
                   }

                }

           }


       }
       protected void dgItemDetail_UpdateCommand(object source, DataGridCommandEventArgs e)
       {
           StockQty ItemDetail = _beginingbalancelist[e.Item.DataSetIndex];
           try
           {
               DropDownList ddlInstrument = e.Item.FindControl("ddlInstrument") as DropDownList;
               ItemDetail.InstrumentId = int.Parse(ddlInstrument.SelectedValue);
               ItemDetail.InstrumentName = ddlInstrument.SelectedItem.Text;
               DropDownList ddlSparepart = e.Item.FindControl("ddlSparepart") as DropDownList;
               ItemDetail.SparepartId = int.Parse(ddlSparepart.SelectedValue);
               ItemDetail.SparepartName = ddlSparepart.SelectedItem.Text;
               TextBox txtqty = e.Item.FindControl("txtQty") as TextBox;
               ItemDetail.Qty = Convert.ToInt32(txtqty.Text);
               _presenter.SaveorUpdateStockQty(ItemDetail);
               //_beginingbalancelist.Insert(Convert.ToInt32(e.Item.DataSetIndex),ItemDetail);
               //_beginingbalancelist.RemoveAt(e.Item.DataSetIndex);
               Master.ShowMessage(new AppMessage("Sparepart Detail  Updated successfully.", Chai.Maintenance.Enums.RMessageType.Info));
               dgItemDetail.EditItemIndex = -1;
               BindItemDetails();
           }
           catch (Exception ex)
           {
               Master.ShowMessage(new AppMessage("Error: Unable to Update Sparepart Detail item Detail. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
           }




       }

       protected void ddlFInstrument_SelectedIndexChanged(object sender, EventArgs e)
       {
           DropDownList ddlInstrument = (DropDownList)sender;
           DataGridItem dgFooter = (DataGridItem)ddlInstrument.Parent.Parent;
           DropDownList ddlSparepart = (DropDownList)dgFooter.FindControl("ddlFSparepart");

           if (ddlSparepart.Items.Count > 0)
           {
               ddlSparepart.Items.Clear();
           }
           ListItem lst = new ListItem();
           lst.Text = "Select Sparepart";
           lst.Value = "0";
           ddlSparepart.Items.Add(lst);
           ddlSparepart.DataSource = _presenter.GetSparepart(Convert.ToInt32(ddlInstrument.SelectedValue));
           ddlSparepart.DataBind();
       }
    
       protected void ddlInstrument_SelectedIndexChanged(object sender, EventArgs e)
       {
           //DropDownList ddlInstrument = dgItemDetail.FindControl("ddlInstrument") as DropDownList;

           //DropDownList ddlSparepart = dgItemDetail.FindControl("ddlSparepart") as DropDownList;
           //if (ddlSparepart.Items.Count > 0)
           //{
           //    ddlSparepart.Items.Clear();
           //}
           //ListItem lst = new ListItem();
           //lst.Text = "Select Spareprt";
           //lst.Value = "0";
           //ddlSparepart.DataSource = _presenter.GetSparepart(Convert.ToInt32(ddlInstrument.SelectedValue));
           //ddlSparepart.DataBind();

       }
       protected void ddlFInstrument_DataBinding(object sender, EventArgs e)
       {

       }
       #endregion
       public IList<StockQty> beginingbalanceList
       {
           set {  _beginingbalancelist = value; }
       }

       public StockQty beginingbalance
       {
           set { _beginingbalance = value; }
       }


       public User user
       {
           set { _user = value; }
       }


       protected void btnFind_Click(object sender, EventArgs e)
       {
           _presenter.GetListOfStockQty(Convert.ToInt32(ddlInstrumentforList.SelectedValue), Convert.ToInt32(ddlSparepartforList.SelectedValue), _user.RegionId);
           dgItemDetail.DataSource = _beginingbalancelist;
           dgItemDetail.DataBind();
       }
       protected void ddlInstrumentforList_SelectedIndexChanged(object sender, EventArgs e)
       {
           if (ddlSparepartforList.Items.Count > 0)
           {
               ListItem lst = new ListItem();
               ddlSparepartforList.Items.Clear();
               lst.Text = "Select Sparepart";
               lst.Value = "0";
               ddlSparepartforList.Items.Add(lst);
           }
           _presenter.GetListOfStockQty(Convert.ToInt32(ddlInstrumentforList.SelectedValue), Convert.ToInt32(ddlSparepartforList.SelectedValue), _user.RegionId);
           ddlSparepartforList.DataSource = _presenter.GetSparepart(Convert.ToInt32(ddlInstrumentforList.SelectedValue));
           ddlSparepartforList.DataBind();
       }
       protected void dgItemDetail_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
       {
           dgItemDetail.CurrentPageIndex = e.NewPageIndex;
           dgItemDetail.DataSource = _beginingbalancelist;
           dgItemDetail.DataBind();
       }
}
}