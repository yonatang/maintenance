﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="Reorder.aspx.cs" Inherits="Chai.Maintenance.Modules.SparepartInventory.Views.Reorder" %>


<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Reorder Qty</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
                                    </section>
                                    </div>
                                    </fieldset>
                                    
        <asp:GridView ID="grvItemBalanceList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="0" DataKeyNames="Id" EnableModelValidation="True" 
                    ForeColor="#333333" GridLines="Horizontal" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active"
                Width="97%">
            
            <Columns>
                <asp:BoundField DataField="SparepartName" HeaderText="Sparepart Name" />
                <asp:BoundField DataField="ReorderQty" HeaderText="Reorder Qty" />
                <asp:BoundField DataField="Qty" HeaderText="Stock Qty" />
            </Columns>
            <PagerStyle ForeColor="White" HorizontalAlign="Center" />
        </asp:GridView>
    </div></div></div>
</asp:Content>

