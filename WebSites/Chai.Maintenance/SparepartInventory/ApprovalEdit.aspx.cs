﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public partial class ApprovalEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page,IApprovalEditView
    {
        private ApprovalPresneter _presenter;
        private Approval _approval;
        private IList<ApprovalItemDetail> _approvalDetail;
        private User _user;
        private User _requester;
   protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this._presenter.OnViewInitialized();
            

        }
        this._presenter.OnViewLoaded();
       
        lblapproverresult.Text = _user.FullName;
        if (_requester != null)
            lblRequestresult.Text = _requester.FullName;
        else
            lblRequestresult.Text = "-";
        if (!IsPostBack)
        {
              BindApprovalControls();
              BindItemDetails();
          
        }
        if (_approval.RejectedReason == "Rejected")
        {
            lblRejectedReason.Visible = false;
            txtrejectedreason.Visible = false;
        }
        if (_approval.Id == -1)

            txtapprovalNo.Text = AutoNumber();
        }
   private string AutoNumber()
   {
       string _AutoNumber = "";
       string _prefix = "";
       int _sequence = 0;
       _sequence = _presenter.GetAutoNumberByPageId((int)AspxPageType.Approval).Sequence + 1;
       _prefix = _presenter.GetAutoNumberByPageId((int)AspxPageType.Approval).Prefix;
       _AutoNumber = _prefix + " - " + _sequence;
       return _AutoNumber;
   }
   [CreateNew]
   public ApprovalPresneter Presenter
   {
       get
       {
           return this._presenter;
       }
       set
       {
           if (value == null)
               throw new ArgumentNullException("value");

           this._presenter = value;
           this._presenter.View = this;
       }
   }



   public int Id
   {
       get
       {
           if (Convert.ToInt32(Request.QueryString["ApprovalId"]) != 0)
               return Convert.ToInt32(Request.QueryString["ApprovalId"]);
           return 0;
       }
   }

   public int requestId
   {
       get
       {
           if (Convert.ToInt32(Request.QueryString["RequestId"]) != 0)
               return Convert.ToInt32(Request.QueryString["RequestId"]);
           return 0;
       }
   }
   public int RequestedBy
   {
       get
       {
           if (Convert.ToInt32(Request.QueryString["RequestedBy"]) != 0)
               return Convert.ToInt32(Request.QueryString["RequestedBy"]);
           return 0;
       }
   }
   public Approval approval
   {
       get
       {
           return _approval;
       }
       set
       {
           _approval = value; ;
       }
   }

   private void BindInstrument(DropDownList ddlInstrument)
   {
       ddlInstrument.DataSource = _presenter.GetInstrumentName();
       ddlInstrument.DataBind();
   }

   private void BindSparepart(DropDownList ddlSparepart, int InsId)
   {
       ddlSparepart.DataSource = _presenter.GetSparepart(InsId);
       ddlSparepart.DataBind();
   }
   private void BindApprovalControls()
   {

       this.txtapprovalNo.Text = _approval.ApprovalNo;
       this.CalApprovalDate.Text = _approval.ApprovalDate.ToString();
       this.txtrejectedreason.Text = _approval.RejectedReason;
       this.ddlStatus.SelectedValue = _approval.ApprovalStatus;
       this.btnDelete.Visible = (_approval.Id > 0);
       this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


   }
   private void SaveApproval()
   {
       try
       {
           if (_approval.IsNew())
           {
               if (_approval.ItemDetail.Count != 0)
               {
                   _presenter.SaveorUpdateApproval(_approval);
                   Master.ShowMessage(new AppMessage("Approval Info. saved", RMessageType.Info));


               }
               else
               {
                   Master.ShowMessage(new AppMessage("You Must Add Sparepart Detail", RMessageType.Error));

               }
           }
           else
           {
               if (_approval.ItemDetail.Count != 0)
               {
                   _presenter.SaveorUpdateApproval(_approval);
                   Master.ShowMessage(new AppMessage("Approval Info. saved", RMessageType.Info));


               }
               else
               {
                   Master.ShowMessage(new AppMessage("You Must Add Sparepart Detail", RMessageType.Error));

               }
           }
       }

       catch (Exception ex)
       {
           Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
       }
   }
   protected void btnSave_Click(object sender, EventArgs e)
   {
       if (this.IsValid)
       {
           _approval.ApprovalNo = this.txtapprovalNo.Text;
           _approval.ApprovalStatus = this.ddlStatus.SelectedValue;
           _approval.ApprovalDate = Convert.ToDateTime(this.CalApprovalDate.Text);
           _approval.RejectedReason = this.txtrejectedreason.Text;
           _approval.RequestedBy = RequestedBy;
           _approval.RegionId = _user.RegionId;
           _approval.Approver = _user.Id;
           _approval.RequestId = requestId;
           SaveApproval();


       }
   }
   protected void btnCancel_Click(object sender, EventArgs e)
   {
       _presenter.CancelPage();
   }
   protected void btnDelete_Click(object sender, EventArgs e)
   {
       try
       {
           _presenter.DeleteApproval(_approval.Id);
           Master.ShowMessage(new AppMessage("Approval Info. was deleted successfully", Chai.Maintenance.Enums.RMessageType.Info));

       }
       catch (Exception ex)
       {
           Master.ShowMessage(new AppMessage("Error: Unable to delete Approval Info. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
       }
   }
   #region ItemDetail
   private void BindItemDetails()
   {
       dgItemDetail.DataSource = _approval.ItemDetail;
       dgItemDetail.DataBind();
   }
   protected void dgItemDetail_CancelCommand(object source, DataGridCommandEventArgs e)
   {
       this.dgItemDetail.EditItemIndex = -1;
       BindItemDetails();
   }
   protected void dgItemDetail_DeleteCommand(object source, DataGridCommandEventArgs e)
   {


      
   }
   protected void dgItemDetail_EditCommand(object source, DataGridCommandEventArgs e)
   {
       this.dgItemDetail.EditItemIndex = e.Item.ItemIndex;
       BindItemDetails();
   }
   protected void dgItemDetail_ItemCommand(object source, DataGridCommandEventArgs e)
   {
      
   }
   protected void dgItemDetail_ItemDataBound(object sender, DataGridItemEventArgs e)
   {


       if (e.Item.ItemType == ListItemType.Footer)
       {
           
          
          
       }
       else
       {


           if (_approval.ItemDetail != null)
           {
               LinkButton lbt = e.Item.FindControl("lnkDelete") as LinkButton;
               if (lbt != null)
                   lbt.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this Recieve Detail?');");

              
               

               }
              



           }

       


   }
   protected void dgItemDetail_UpdateCommand(object source, DataGridCommandEventArgs e)
   {
       ApprovalItemDetail ItemDetail = _approval.ItemDetail[e.Item.DataSetIndex];
       try
       {

           TextBox txtApprovalqty = e.Item.FindControl("txtApprovedQty") as TextBox;
           ItemDetail.ApprovedQty = Convert.ToInt32(txtApprovalqty.Text);
           _approval.ItemDetail.Insert(Convert.ToInt32(e.Item.DataSetIndex), ItemDetail);
           _approval.ItemDetail.RemoveAt(e.Item.DataSetIndex);
           Master.ShowMessage(new AppMessage("Sparepart Detail  Updated successfully.", Chai.Maintenance.Enums.RMessageType.Info));
           dgItemDetail.EditItemIndex = -1;
           BindItemDetails();
       }
       catch (Exception ex)
       {
           Master.ShowMessage(new AppMessage("Error: Unable to Update Sparepart Detail item Detail. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
       }




   }

   #endregion



   public IList<ApprovalItemDetail> _approvaldetail
   {
       get { return _approvaldetail; }
       set { _approvalDetail = value; }
   }
   protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
   {
       if (ddlStatus.SelectedItem.Text == "Rejected")
       {
           lblRejectedReason.Visible = true;
           txtrejectedreason.Visible = true;
       }
       else
       {
           lblRejectedReason.Visible = false;
           txtrejectedreason.Visible = false;
       
       }
   }





   public User user
   {
       set { _user = value; }
   }


   public User Requester
   {
       set { _requester = value; }
   }
    }

    }
