﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public partial class RequestEdit : Microsoft.Practices.CompositeWeb.Web.UI.Page, IRequestEditView
    {
        private RequestPresneter _presenter;
        private Request _request;
        private User _user;      
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();
            if (_user.FullName != null)
                lblRequestresult.Text = _user.FullName;
            else
                lblRequestresult.Text = _user.VendorName;
            if (!IsPostBack)
            {
                BindRecieveControls();
                BindItemDetails();
            }
            if (_request.Id == -1)
                txtRequestNo.Text = AutoNumber();

        }
        [CreateNew]
       
        public RequestPresneter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private string AutoNumber()
        {
            string _AutoNumber = "";
            string _prefix = "";
            int _sequence = 0;
            _sequence = _presenter.GetAutoNumberByPageId((int)AspxPageType.Request).Sequence + 1;
            _prefix = _presenter.GetAutoNumberByPageId((int)AspxPageType.Request).Prefix;
            _AutoNumber = _prefix + " - " + _sequence;
            return _AutoNumber;
        }
        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["RequestId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["RequestId"]);
                else
                    return 0;
            }
        }

        public Request request
        {
            get
            {
                return _request;
            }
            set
            {
                _request = value;
            }
        }
       
        private void BindInstrument(DropDownList ddlInstrument)
        {
            ddlInstrument.DataSource = _presenter.GetInstrumentName();
            ddlInstrument.DataBind();
        }
       
        private void BindSparepart(DropDownList ddlSparepart, int InsId)
        {
            ddlSparepart.DataSource = _presenter.GetSparepart(InsId);
            ddlSparepart.DataBind();
        }
        private void BindRecieveControls()
        {

            this.txtRequestNo.Text = _request.RequestNo;
            this.Calendar1.Text = _request.RequestDate.ToString();
            this.btnDelete.Visible = (_request.Id > 0);
            this.btnDelete.Attributes.Add("onclick", "return confirm(\"Ary you sure?\")");


        }
        private void SaveRequest()
            {
        try
        {
            if (_request.IsNew())
            {
                if (_request.ItemDetail.Count != 0)
                {
                    _presenter.SaveorUpdateRequest(_request);
                    Master.ShowMessage(new AppMessage("Request Info. saved", RMessageType.Info));
                   
                   
                }
                else
                {
                    Master.ShowMessage(new AppMessage("You Must Add Sparepart Detail", RMessageType.Error));
                
                }
            }
            else
            {
                if (_request.ItemDetail.Count != 0)
                {
                    _presenter.SaveorUpdateRequest(_request);
                    Master.ShowMessage(new AppMessage("Request Info. saved", RMessageType.Info));
                 
                  
                }
                else
                {
                    Master.ShowMessage(new AppMessage("You Must Add Sparepart Detail", RMessageType.Error));

                }
            }
        }

        catch (Exception ex)
        {
            Master.ShowMessage(new AppMessage(ex.Message, RMessageType.Error));
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (this.IsValid)
        {

            _request.RequestNo = this.txtRequestNo.Text;
            _request.RequestDate = Convert.ToDateTime(this.Calendar1.Text);
            _request.RequestedBy = _user.Id;
            _request.Region_Id = _user.RegionId;
            SaveRequest();
            

        }	    
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        _presenter.CancelPage();
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        try
        {
            _presenter.DeleteRequest(_request.Id);
            Master.ShowMessage(new AppMessage("Request Info. was deleted successfully", Chai.Maintenance.Enums.RMessageType.Info));
            
        }
        catch (Exception ex)
        {
            Master.ShowMessage(new AppMessage("Error: Unable to delete Request Info. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
        }
    }
    
    #region ItemDetail
    private void BindItemDetails()
    {
        dgItemDetail.DataSource = _request.ItemDetail;
        dgItemDetail.DataBind();
    }
    protected void dgItemDetail_CancelCommand(object source, DataGridCommandEventArgs e)
    {
        this.dgItemDetail.EditItemIndex = -1;
        BindItemDetails();
    }
    protected void dgItemDetail_DeleteCommand(object source, DataGridCommandEventArgs e)
    {
        
       
        try
        {
            LinkButton lbt = e.Item.FindControl("lnkDelete") as LinkButton;
            if (lbt != null)
                lbt.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this Recieve Detail?');");

            if (_request.ItemDetail[e.Item.DataSetIndex].Id != 0)
            {
                _presenter.DeleteRequestDetail(_request.ItemDetail[e.Item.DataSetIndex].Id);
            }
            _request.ItemDetail.Remove(_request.ItemDetail[Convert.ToInt32(e.Item.DataSetIndex)]);
            BindItemDetails();

            Master.ShowMessage(new AppMessage("Sparepart item Detail was Removed successfully", Chai.Maintenance.Enums.RMessageType.Info));
        }
        catch(Exception ex)
        {
            Master.ShowMessage(new AppMessage("Error: Unable to delete Sparepart item Detail. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
        }
    }
    protected void dgItemDetail_EditCommand(object source, DataGridCommandEventArgs e)
    {
        this.dgItemDetail.EditItemIndex = e.Item.ItemIndex;
        
        BindItemDetails();
    }
    protected void dgItemDetail_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "AddNew")
        {
            try
            {
                RequestItemDetail ItemDetail = new RequestItemDetail();
                DropDownList ddlInstrument = e.Item.FindControl("ddlFInstrument") as DropDownList;
                ItemDetail.InstrumentId = int.Parse(ddlInstrument.SelectedValue);
                ItemDetail.InstrumentName = ddlInstrument.SelectedItem.Text;
                DropDownList ddlSparepart = e.Item.FindControl("ddlFSparepart") as DropDownList;
                ItemDetail.SparepartId = int.Parse(ddlSparepart.SelectedValue);
                ItemDetail.SparepartName = ddlSparepart.SelectedItem.Text;
               // DropDownList ddlProblem = e.Item.FindControl("ddlFProblem") as DropDownList;
               // ItemDetail.ProblemId = int.Parse(ddlProblem.SelectedValue);
               // ItemDetail.ProblemNumber = ddlProblem.SelectedItem.Text;
                TextBox txtqty = e.Item.FindControl("txtFQty") as TextBox;
                ItemDetail.Qty = Convert.ToInt32(txtqty.Text);
                _request.ItemDetail.Add(ItemDetail);
                Master.ShowMessage(new AppMessage("Sparepart Detail  added successfully.", Chai.Maintenance.Enums.RMessageType.Info));
                dgItemDetail.EditItemIndex = -1;
                BindItemDetails();
            }
            catch(Exception ex)
            {
                Master.ShowMessage(new AppMessage("Error: Unable to Add Sparepart item Detail. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
            }
        }
    }
    protected void dgItemDetail_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
       

        if (e.Item.ItemType == ListItemType.Footer)
        {
            DropDownList ddlinstrument = e.Item.FindControl("ddlFInstrument") as DropDownList;
            BindInstrument(ddlinstrument);
                    
            DropDownList ddlSparepart = e.Item.FindControl("ddlFSparepart") as DropDownList;
            //BindSparepart(ddlSparepart);
            if (Convert.ToInt32(ddlinstrument.SelectedValue) != 0)
            {
                if (ddlSparepart.Items.Count > 0)
                {
                    ddlSparepart.Items.Clear();
                }
                ListItem lst = new ListItem();
                lst.Text = "Select Sparepart";
                lst.Value = "0";
                ddlSparepart.DataSource = _presenter.GetSparepart(Convert.ToInt32(ddlinstrument.SelectedValue));
                ddlSparepart.DataBind();
            }
        }
            else
            {
                

                    if (_request.ItemDetail != null)
                    {
                        LinkButton lbt = e.Item.FindControl("lnkDelete") as LinkButton;
                        if (lbt != null)
                            lbt.Attributes.Add("onclick", "javascript:return confirm('Are you sure you want to delete this Recieve Detail?');");

                        DropDownList ddlinstrument = e.Item.FindControl("ddlInstrument") as DropDownList;
                        if (ddlinstrument != null)
                        {
                            BindInstrument(ddlinstrument);
                            if (_request.ItemDetail[e.Item.DataSetIndex].InstrumentId != null)
                            {
                                ListItem liI = ddlinstrument.Items.FindByValue(_request.ItemDetail[e.Item.DataSetIndex].InstrumentId.ToString());
                                if (liI != null)
                                    liI.Selected = true;
                            }

                        }
                        DropDownList ddlSparepart = e.Item.FindControl("ddlSparepart") as DropDownList;
                        if (ddlSparepart != null)
                        {

                            BindSparepart(ddlSparepart, Convert.ToInt32(ddlinstrument.SelectedValue));
                            if (_request.ItemDetail[e.Item.DataSetIndex].SparepartId != null)
                            {
                                ListItem lis = ddlSparepart.Items.FindByValue(_request.ItemDetail[e.Item.DataSetIndex].SparepartId.ToString());
                                if (lis != null)
                                    lis.Selected = true;
                            }
                        }
                        
                    
                    
                }
                    
            }
        
        
    }
    protected void dgItemDetail_UpdateCommand(object source, DataGridCommandEventArgs e)
    {
        RequestItemDetail ItemDetail = _request.ItemDetail[e.Item.DataSetIndex];
         try
          {
              DropDownList ddlInstrument = e.Item.FindControl("ddlInstrument") as DropDownList;
              ItemDetail.InstrumentId = int.Parse(ddlInstrument.SelectedValue);
              ItemDetail.InstrumentName = ddlInstrument.SelectedItem.Text;
              DropDownList ddlSparepart = e.Item.FindControl("ddlSparepart") as DropDownList;
              ItemDetail.SparepartId = int.Parse(ddlSparepart.SelectedValue);
              ItemDetail.SparepartName = ddlSparepart.SelectedItem.Text;
             //DropDownList ddlProblem = e.Item.FindControl("ddlProblem") as DropDownList;
              //ItemDetail.ProblemId = int.Parse(ddlProblem.SelectedValue);
              //ItemDetail.ProblemNumber = ddlProblem.SelectedItem.Text;
              TextBox txtqty = e.Item.FindControl("txtQty") as TextBox;
              ItemDetail.Qty = Convert.ToInt32(txtqty.Text);
             
             
              _request.ItemDetail.Insert(Convert.ToInt32(e.Item.DataSetIndex), ItemDetail);
              _request.ItemDetail.RemoveAt(e.Item.DataSetIndex);
              Master.ShowMessage(new AppMessage("Sparepart Detail  Updated successfully.", Chai.Maintenance.Enums.RMessageType.Info));
              dgItemDetail.EditItemIndex = -1;
              BindItemDetails();
          }
          catch (Exception ex)
          {
              Master.ShowMessage(new AppMessage("Error: Unable to Update Sparepart Detail item Detail. " + ex.Message, Chai.Maintenance.Enums.RMessageType.Error));
          }

        

        
    }

    #endregion


    protected void ddlFInstrument_SelectedIndexChanged(object sender, EventArgs e)
    {
                 
          DropDownList ddlInstrument = (DropDownList)sender;
          DataGridItem dgFooter = (DataGridItem)ddlInstrument.Parent.Parent;
          DropDownList ddlSparepart = (DropDownList)dgFooter.FindControl("ddlFSparepart");
      
        if (ddlSparepart.Items.Count > 0)
        {
            ddlSparepart.Items.Clear();
        }
        ListItem lst = new ListItem();
        lst.Text = "Select Sparepart";
        lst.Value = "0";
        ddlSparepart.Items.Add(lst);
        ddlSparepart.DataSource = _presenter.GetSparepart(Convert.ToInt32(ddlInstrument.SelectedValue));
        ddlSparepart.DataBind();
    }
    protected void ddlInstrument_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DropDownList ddlInstrument = dgItemDetail.FindControl("ddlInstrument") as DropDownList;
        
        //DropDownList ddlSparepart = dgItemDetail.FindControl("ddlSparepart") as DropDownList;
        //if (ddlSparepart.Items.Count > 0)
        //{
        //    ddlSparepart.Items.Clear();
        //}
        //ListItem lst = new ListItem();
        //lst.Text = "Select Spareprt";
        //lst.Value = "0";
        //ddlSparepart.DataSource = _presenter.GetSparepart(Convert.ToInt32(ddlInstrument.SelectedValue));
        //ddlSparepart.DataBind();

    }
    protected void ddlFInstrument_DataBinding(object sender, EventArgs e)
    {

    }
        protected void dgItemDetail_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        public User user
        {
            set { _user = value; }
        }
    }
}