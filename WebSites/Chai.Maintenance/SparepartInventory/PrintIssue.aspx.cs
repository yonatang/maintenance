﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{

    public partial class PrintIssue : Microsoft.Practices.CompositeWeb.Web.UI.Page, IIssueEditView
    {
        private IssuePresneter _presenter;
        private Issue _issue;
        private User _user;
        private User _requester;
        private User _approver;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();
                btnPrint.Attributes.Add("onclick", "javascript:Clickheretoprint('divprint'); return false;");
                BindJS();

            }
            this._presenter.OnViewLoaded();
            BindIssueControls();
            BindItemDetails();
        }
        [CreateNew]
        public IssuePresneter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        public int Id
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["IssueId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["IssueId"]);
                return 0;
            }
        }

        public int ApprovedId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["ApprovalId"]) != 0)
                    return Convert.ToInt32(Request.QueryString["ApprovalId"]);
                return 0;
            }
        }

        public int RequesterId
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["Requester"]) != 0)
                    return Convert.ToInt32(Request.QueryString["Requester"]);
                return _issue.IssuedFor;
            }
        }

        public int approver
        {
            get
            {
                if (Convert.ToInt32(Request.QueryString["Approver"]) != 0)
                    return Convert.ToInt32(Request.QueryString["Approver"]);
                return _issue.Approver;
            }
        }

        public User Requester
        {
            set { _requester = value; }
        }

        public User Approver
        {
            set { _approver = value; }
        }

        public User user
        {
            set { _user = value; }
        }

        public Issue issue
        {
            get
            {
                return _issue;
            }
            set
            {
                _issue = value; 
            }
        }
        private void BindIssueControls()
        {

            this.lblIssueNoresult.Text = _issue.IssueNo;
            this.lblIssueDateresult.Text = _issue.IssueDate.ToShortDateString();
            this.lblapproverresult.Text = _approver.FullName;
            this.lblIssuedforresult.Text = _requester.FullName;
            this.lblToolKeeperresult.Text = _user.FullName;

        }
        private void BindItemDetails()
        {
            dgItemDetail.DataSource = _issue.ItemDetail;
            dgItemDetail.DataBind();
        }
        public IList<IssueItemDetail> _issueitemdetail
        {
            get
            {
                return null;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        protected void BindJS()
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "EIDscripts", String.Format("<script language=\"JavaScript\" src=\"http://localhost/maintenance/Maintenance.js\"></script>\n"));
        }
        protected void btnPrint_Click(object sender, EventArgs e)
        {

        }
}
}