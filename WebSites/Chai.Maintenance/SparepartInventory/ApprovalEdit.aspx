﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Shared/ModuleMaster.master" AutoEventWireup="true" CodeFile="ApprovalEdit.aspx.cs" Inherits="Chai.Maintenance.Modules.SparepartInventory.Views.ApprovalEdit" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>

<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
            HeaderText="Error" ValidationGroup="1" class="alert alert-danger fade in"/>
        <div class="jarviswidget" id="wid-id-8" data-widget-editbutton="false" data-widget-custombutton="false">
                  <header>
					<span class="widget-icon"> <i class="fa fa-edit"></i> </span>
					<h2>Approval</h2>				
				</header>
                 <div>								
					<div class="jarviswidget-editbox"></div>	
						<div class="widget-body no-padding">
                         <div class="smart-form">
                    <fieldset>					
								<div class="row">
									<section class="col col-6">   
                        <asp:Label ID="lblApprovalNo" runat="server" Text="Approval No." 
                            CssClass="label"></asp:Label>
                     <label class="input">
                        <asp:TextBox ID="txtapprovalNo" runat="server"
                             Enabled="False"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RfvRequestNo" runat="server" 
                            ControlToValidate="txtapprovalNo" Display="Dynamic" 
                            ErrorMessage="Request No. Required" ValidationGroup="1">*</asp:RequiredFieldValidator></label>
                            </section>
                    <section class="col col-6">  
                        <asp:Label ID="lblRequestBy" runat="server" Text="Requested By" 
                            CssClass="label"></asp:Label>
                   <label class="input">
                        <asp:TextBox ID="lblRequestresult" runat="server" ReadOnly="true"
                            ></asp:TextBox></label></section></div>
                       <div class="row">
									<section class="col col-6">
                        <asp:Label ID="lblApprovalStatus" runat="server" CssClass="label" 
                            Text="Approval Status"></asp:Label>
                    <label class="select">
                        <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" 
                            CssClass="textbox" onselectedindexchanged="ddlStatus_SelectedIndexChanged">
                            <asp:ListItem Value="0">Select Status</asp:ListItem>
                            <asp:ListItem>Approved</asp:ListItem>
                            <asp:ListItem>Rejected</asp:ListItem>
                        </asp:DropDownList><i></i>
                        <asp:RequiredFieldValidator ID="RfvStatus" runat="server" 
                            ControlToValidate="ddlStatus" Display="Dynamic" ErrorMessage="Status Required" 
                            InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator></label>
                   </section>
                   <section class="col col-6">
                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="label"></asp:Label>
                     <label class="input">
                        <asp:TextBox ID="lblapproverresult" runat="server"   ReadOnly="true"
                            ></asp:TextBox></label></section>
                      </div>
                      <div class="row">
									<section class="col col-6">
                        <asp:Label ID="lblApprovalDate" runat="server" CssClass="label" 
                            Text="Approval Date"></asp:Label>
                       <label class="input">
                   <i class="icon-append fa fa-calendar"></i>
                     <asp:TextBox ID="CalApprovalDate"  runat="server" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"
                         ></asp:TextBox>
                   
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" 
                            runat="server" ControlToValidate="CalApprovalDate" ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator></label></section>
                    <section class="col col-6">
                        <asp:Label ID="lblRejectedReason" runat="server" Text="Rejected Reason" 
                            Visible="False" CssClass="label"></asp:Label>
                   <label class="textarea">
                        <asp:TextBox ID="txtrejectedreason" runat="server"  
                            TextMode="MultiLine" Visible="False" ></asp:TextBox></label></section></div></fieldset>
                            <footer>
                             <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Approve" 
                            ValidationGroup="1" Width="72px" Cssclass="btn btn-primary"/>
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Cssclass="btn btn-primary"
                            onclick="btnCancel_Click" Text="Cancel"/>
                        <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" 
                            Text="Delete" Width="59px" Cssclass="btn btn-primary"/>
 </footer>
      
                                </div>
                                </div>
                                </div>
                  
            <asp:DataGrid ID="dgItemDetail" runat="server" AutoGenerateColumns="False"
                BorderColor="#003366" BorderStyle="Solid" BorderWidth="1px" CellPadding="0" CssClass="table table-striped table-bordered table-hover" PagerStyle-CssClass="paginate_button active" AlternatingRowStyle-CssClass=""
                DataKeyField="Id" ForeColor="#333333" GridLines="None" 
                oncancelcommand="dgItemDetail_CancelCommand" 
                ondeletecommand="dgItemDetail_DeleteCommand" 
                oneditcommand="dgItemDetail_EditCommand" 
                onitemcommand="dgItemDetail_ItemCommand" 
                onitemdatabound="dgItemDetail_ItemDataBound" 
                onupdatecommand="dgItemDetail_UpdateCommand" ShowFooter="True" Width="99%">
                
                <Columns>
                    <asp:TemplateColumn HeaderText="Instrument Name">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Sparepart Name">
                        <EditItemTemplate>
                             <%# DataBinder.Eval(Container.DataItem, "SparepartName")%>
                        </EditItemTemplate>
                        <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "SparepartName")%>
                            </ItemTemplate>
                    </asp:TemplateColumn>
                    
                    <asp:TemplateColumn HeaderText="Requested Qty">
                        <EditItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "RequestedQty")%>
                        </EditItemTemplate>
                        <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "RequestedQty")%>
                            </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Approved Qty">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtApprovedQty" runat="server" 
                                Text=' <%# DataBinder.Eval(Container.DataItem, "ApprovedQty")%>' ValidationGroup="3" 
                                Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="txtApprovedQty_FilteredTextBoxExtender" 
                                runat="server" Enabled="True" FilterType="Numbers" 
                                TargetControlID="txtApprovedQty">
                            </cc2:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="RfvQty" runat="server" 
                                ControlToValidate="txtApprovedQty" ErrorMessage="Qty Required" InitialValue="0" 
                                ValidationGroup="3">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "ApprovedQty")%>
                            </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <EditItemTemplate>
                            <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="Update" 
                                ValidationGroup="3">Update</asp:LinkButton>
                            &nbsp;
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                            &nbsp;
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                
               <PagerStyle CssClass="paginate_button active"  HorizontalAlign="Center" />
                          </asp:DataGrid>
           
               </div>        
                  
</asp:Content>

