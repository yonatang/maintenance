﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{


    public partial class IssueList : Microsoft.Practices.CompositeWeb.Web.UI.Page, IIssueListView
    {
        private IssueListPresenter _presenter;
        private IList<Issue> _issue;
        private User _user;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();

            }
            this._presenter.OnViewLoaded();
        }
        [CreateNew]
        public IssueListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetIssueList(Convert.ToInt32(ddlSelect.SelectedValue), txtvalue.Text, CalDateFrom.Text, CalDateTo.Text, _user.RegionId);
            grvIssueList.DataSource = _issue;
            grvIssueList.DataBind();
        }
        protected void ddlSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlSelect.SelectedValue) == 2)
            {

                CalDateFrom.Visible = true;
                CalDateTo.Visible = true;
               
                txtvalue.Visible = false;
                lblDateFrom.Visible = true;
                lblDateTo.Visible = true;
                lblValue.Visible = false;
            }
            else
            {
                CalDateFrom.Visible = false;
                CalDateTo.Visible = false;
               
                txtvalue.Visible = true;
                lblDateFrom.Visible = false;
                lblDateTo.Visible = false;
                lblValue.Visible = true;
            }
        }
       
        protected void grvIssueList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Issue issue = e.Row.DataItem as Issue;
            if (issue != null)
            {
                HyperLink hpl = e.Row.FindControl("hplEdit") as HyperLink;
                string url = String.Format("~/SparepartInventory/IssueEdit.aspx?{0}=3&IssuedId={1}&approver={2}&Requester={3}", AppConstants.TABID, issue.Id, issue.Approver, issue.IssuedFor);

                hpl.NavigateUrl = this.ResolveUrl(url);

            }
        }
        protected void grvIssueList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvIssueList.PageIndex = e.NewPageIndex;
            grvIssueList.DataSource = _issue;
            grvIssueList.DataBind();
        }

        public IList<Issue> issue
        {
            set { _issue = value; }
        }

        public User user
        {
            set { _user = value; }
        }
    }
}