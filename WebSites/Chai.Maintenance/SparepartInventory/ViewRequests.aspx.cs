﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Modules.Shell;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.SparepartInventory;
using System.Collections.Generic;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public partial class ViewRequests : Microsoft.Practices.CompositeWeb.Web.UI.Page, IRequestListView
    {
        private RequestListPresenter _presenter;
        private IList<Request> _request;
        private User _user;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this._presenter.OnViewInitialized();


            }
            this._presenter.OnViewLoaded();
            BindRequester();
        }
        [CreateNew]
        public RequestListPresenter Presenter
        {
            get
            {
                return this._presenter;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._presenter = value;
                this._presenter.View = this;
            }
        }
        private void BindRequester()
        {
            ddlRequester.DataSource = _presenter.GetUsers(_user.RegionId);
            ddlRequester.DataBind();
        }
        protected void ddlSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(ddlSelect.SelectedValue) == 2)
            {
                Calendar1.Visible = true;
                Calendar2.Visible = true;
                Img.Visible = true;
                Img1.Visible = true;
                txtvalue.Visible = false;
                lblDateFrom.Visible = true;
                lblDateTo.Visible = true;
                lblValue.Visible = false;
                ddlRequester.Visible = false;
                lblRequester.Visible = false;
                inlineRequester.Visible = false;
            }
            else if (Convert.ToInt32(ddlSelect.SelectedValue) == 3)
            {
                Calendar1.Visible = false;
                Calendar2.Visible = false;
                Img.Visible = false;
                Img1.Visible = false;
                txtvalue.Visible = false;
                lblDateFrom.Visible = false;
                lblDateTo.Visible = false;
                lblValue.Visible = false;
                lblRequester.Visible = true;
                ddlRequester.Visible = true;
                inlineRequester.Visible = true;
            }
            else
            {
                Calendar1.Visible = false;
                Calendar2.Visible = false;
                Img.Visible = false;
                Img1.Visible = false;
                txtvalue.Visible = true;
                lblDateFrom.Visible = false;
                lblDateTo.Visible = false;
                lblValue.Visible = true;
                ddlRequester.Visible = false;
                lblRequester.Visible = false;
                inlineRequester.Visible = false;
            }
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
            _presenter.GetRequestListforApprovals(Convert.ToInt32(ddlSelect.SelectedValue), txtvalue.Text, Calendar1.Text, Calendar2.Text, _user.RegionId, Convert.ToInt32(ddlRequester.SelectedValue));
            grvRequestList.DataSource = _request;
            grvRequestList.DataBind();
        }

        public new IList<Request> Requests
        {
            set { _request = value; }
        }
       

        protected void grvRequestList_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect(String.Format("~/SparepartInventory/ApprovalEdit.aspx?{0}=3&RequestId={1}&RequestedBy={2}", AppConstants.TABID, Convert.ToInt32(grvRequestList.SelectedDataKey["Id"]), Convert.ToInt32(grvRequestList.SelectedDataKey["RequestedBy"])));

            //this.ResolveUrl(url);
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {

        }


        public User user
        {
            set { _user = value; }
        }
        protected void grvRequestList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem != null)
            {
                Request request = e.Row.DataItem as Request;
                if (request != null)
                {
                    if (request.ApprovalStatus == "Approved" || request.ApprovalStatus == "Rejected")
                    {

                        e.Row.Enabled = false;

                    }
                }
            }
        }
        protected void grvRequestList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvRequestList.PageIndex = e.NewPageIndex;
            grvRequestList.DataSource = _request;
            grvRequestList.DataBind();
        }
}
}