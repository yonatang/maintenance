﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits ="Chai.Maintenance.Modules.SparepartInventory.Views.Default"
    Title="Default" MasterPageFile="~/Shared/ModuleMaster.master" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>
<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" Runat="Server">
		<h1>
            Sparepart Inventory</h1>
        <table style="width: 100%" border="0">
            <tr>
                <td align="center" colspan="2">
            <asp:Label ID="Label1" runat="server" style="font-weight: 700; font-size: small;" 
                        Text="Date From"></asp:Label>
            &nbsp;
                    <cc1:Calendar ID="Calendar1" runat="server" Height="54px" />
&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label2" runat="server" style="font-weight: 700; font-size: small;" 
                        Text="Date To"></asp:Label>
            &nbsp;&nbsp;&nbsp;
                    <cc1:Calendar ID="Calendar2" runat="server" Height="61px" />
                    <asp:Button ID="btnView" runat="server" onclick="btnView_Click" Text="View" />
                </td>
            </tr>
            <tr>
                <td style="width: 100%" align="center">
            <asp:CHART ID="chartIssue" runat="server" BackColor="211, 223, 240" 
                BackGradientStyle="TopBottom" BorderColor="#1A3B69" 
                BorderlineDashStyle="Solid" BorderWidth="2px" Height="296px" 
                ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)" IsSoftShadows="False" 
                Width="410px" BorderlineColor="WhiteSmoke" BorderlineWidth="0">
                <Titles>
                    <asp:Title Name="Issued Spareparts for each Instruments" Text="Issued Spareparts for each Instrument">
                    </asp:Title>
                </Titles>
                <borderskin SkinStyle="Emboss" />
                <series>
                    <asp:Series ChartArea="Area1" 
                        Name="Series3" IsValueShownAsLabel="True">
                    </asp:Series>
                </series>
                <chartareas>
                    <asp:ChartArea BackColor="Transparent" BackGradientStyle="TopBottom" 
                        BackSecondaryColor="Transparent" BorderColor="64, 64, 64, 64" Name="Area1" 
                        ShadowColor="Transparent">
                        <axisy2>
                            <MajorGrid Enabled="False" />
                            <MajorTickMark Enabled="False" />
                        </axisy2>
                        <axisx2>
                            <MajorGrid Enabled="False" />
                            <MajorTickMark Enabled="False" />
                        </axisx2>
                        <area3dstyle IsClustered="False" IsRightAngleAxes="False" PointGapDepth="900" 
                            Rotation="162" WallWidth="25" />
                        <axisy LineColor="64, 64, 64, 64" LabelAutoFitMinFontSize="8" 
                            Title="No Issued Spareparts by Instrument">
                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                            <MajorGrid LineColor="192, 192, 255" />
                            <MajorTickMark Enabled="False" />
                        </axisy>
                        <axisx LineColor="64, 64, 64, 64" Interval="1" IsLabelAutoFit="False" 
                            Title="Instrument">
                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Angle="90" />
                            <MajorGrid LineColor="192, 192, 255" />
                            <MinorGrid LineColor="LightGray" LineDashStyle="DashDotDot" />
                            <MajorTickMark Enabled="False" />
                        </axisx>
                    </asp:ChartArea>
                </chartareas>
            </asp:CHART>
                </td>
                <td align="center">
            <asp:CHART ID="chartRequest" runat="server" BackColor="211, 223, 240" 
                BackGradientStyle="TopBottom" BorderColor="#1A3B69" 
                BorderlineDashStyle="Solid" BorderWidth="2px" Height="296px" 
                ImageLocation="~/TempImages/ChartPic_#SEQ(300,3)" IsSoftShadows="False" 
                Width="436px" BorderlineColor="WhiteSmoke" BorderlineWidth="0">
                 <Titles>
                    <asp:Title Name="Issued Spareparts for each Instruments" Text="Requested Spareparts for each Instrument">
                    </asp:Title>
                </Titles>
                <borderskin SkinStyle="Emboss" />
                <series>
                    <asp:Series ChartArea="Area1" 
                        Name="Series3" IsValueShownAsLabel="True">
                    </asp:Series>
                </series>
                <chartareas>
                    <asp:ChartArea BackColor="Transparent" BackGradientStyle="TopBottom" 
                        BackSecondaryColor="Transparent" BorderColor="64, 64, 64, 64" Name="Area1" 
                        ShadowColor="Transparent">
                        <axisy2>
                            <MajorGrid Enabled="False" />
                            <MajorTickMark Enabled="False" />
                        </axisy2>
                        <axisx2>
                            <MajorGrid Enabled="False" />
                            <MajorTickMark Enabled="False" />
                        </axisx2>
                        <area3dstyle IsClustered="False" IsRightAngleAxes="False" PointGapDepth="900" 
                            Rotation="162" WallWidth="25" />
                        <axisy LineColor="64, 64, 64, 64" LabelAutoFitMinFontSize="8" 
                            Title="No Of Requested Spareparts by Instruments">
                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
                            <MajorGrid LineColor="192, 192, 255" />
                            <MajorTickMark Enabled="False" />
                        </axisy>
                        <axisx LineColor="64, 64, 64, 64" Interval="1" IsLabelAutoFit="False" 
                            Title="Instrument">
                            <LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" Angle="90" />
                            <MajorGrid LineColor="192, 192, 255" />
                            <MinorGrid LineColor="LightGray" LineDashStyle="DashDotDot" />
                            <MajorTickMark Enabled="False" />
                        </axisx>
                    </asp:ChartArea>
                </chartareas>
            </asp:CHART>
                </td>
            </tr>
        </table>
        <br />
</asp:Content>

