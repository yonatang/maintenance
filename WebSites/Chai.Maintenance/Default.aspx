﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="ShellDefault" MasterPageFile="~/Shared/HomeMaster.master" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <script src="FusionCharts/FusionCharts.js" type="text/javascript"></script>
    <script src="FusionMaps/FusionMaps.js" type="text/javascript"></script>
  				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> Dashboard <span>></span></h1>
					</div>
   
    <article class="col-sm-12 sortable-grid ui-sortable">
							<!-- new widget -->
							
							<!-- end widget -->

						<div class="jarviswidget jarviswidget-sortable" id="wid-id-0" data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" role="widget" style="">
							
								<header role="heading">
									<span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span>
									<h2>Charts</h2>

									<ul class="nav nav-tabs pull-right in" id="myTab">
										<li class="active">
											<a data-toggle="tab" href="#s1"><span class="hidden-mobile hidden-tablet">Notified Problems By Region</span></a>
										</li>

										<li class="">
											<a data-toggle="tab" href="#s2"><span class="hidden-mobile hidden-tablet">Notified Problems By Instrument</span></a>
										</li>

										<li>
											<a data-toggle="tab" href="#s3"> <span class="hidden-mobile hidden-tablet">Problems By Manufacturer</span></a>
										</li>
                                        <li>
											<a data-toggle="tab" href="#s4"><span class="hidden-mobile hidden-tablet">Problems By Problem Type</span></a>
										</li>
									</ul>

								<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

								<!-- widget div-->
								<div class="no-padding" role="content">
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">

										test
									</div>
									<!-- end widget edit box -->

									<div class="widget-body">
										<!-- content -->
										<div id="myTabContent" class="tab-content">
											<div class="tab-pane fade padding-10 no-padding-bottom active in" id="s1">
                                            <div class="widget-body-toolbar bg-color-white">
                                            <div class="form-inline" role="form">
                                            <div class="form-group">
															<label class="sr-only" for="s123">Show From</label>
                                                            <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
															<asp:TextBox runat="server" id="datefrom1" placeholder="Date From" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox></label>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                                 ControlToValidate="datefrom1" 
                                                                 ErrorMessage="*" 
                                                                 ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                                                                 ValidationGroup="2" style="font-size: small">*</asp:RegularExpressionValidator>
														</div>
														<div class="form-group">
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
															<asp:TextBox runat="server" id="DateTto1" placeholder="Date To" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox></label>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                                              ControlToValidate="DateTto1" 
                                                              ErrorMessage="*" 
                                                              ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                                                              ValidationGroup="2" style="font-size: small">*</asp:RegularExpressionValidator>
                                                              <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-xs btn-primary"
                                                               ValidationGroup="2">View</asp:LinkButton>
														</div>
                                                        </div>
                                            
												</div>
												<div class="row no-space">
												<asp:Literal ID="ltrNotifiedProblems" runat="server"></asp:Literal>
												</div>

												<div class="show-stat-microcharts">
													<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

														<div class="easy-pie-chart txt-color-orangeDark easyPieChart" data-percent="33" data-pie-size="50" style="width: 50px; height: 50px; line-height: 50px;">
															<span >80</span>
														<canvas width="50" height="50"></canvas></div>
														<span class="easy-pie-title"> Tot. Instrument  </span>
													
														<div class="sparkline txt-color-greenLight hidden-sm hidden-md pull-right" data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent"><canvas width="70" height="33" style="display: inline-block; width: 70px; height: 33px; vertical-align: top;"></canvas></div>
													</div>
													<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
														<div class="easy-pie-chart txt-color-greenLight easyPieChart" data-percent="78.9" data-pie-size="50" style="width: 50px; height: 50px; line-height: 50px;">
															<span >13</span>
														<canvas width="50" height="50"></canvas></div>
														<span class="easy-pie-title"> Not Functioning </span>
														
														<div class="sparkline txt-color-blue hidden-sm hidden-md pull-right" data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent"><canvas width="70" height="33" style="display: inline-block; width: 70px; height: 33px; vertical-align: top;"></canvas></div>
													</div>
													<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
														<div class="easy-pie-chart txt-color-darken easyPieChart" data-percent="36" data-pie-size="50" style="width: 50px; height: 50px; line-height: 50px;">
															<span>7</span>
														<canvas width="50" height="50"></canvas></div>
														<span class="easy-pie-title"> Need Preventive Main.</span>
														
														<div class="sparkline txt-color-red hidden-sm hidden-md pull-right" data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent"><canvas width="70" height="33" style="display: inline-block; width: 70px; height: 33px; vertical-align: top;"></canvas></div>
													</div>
													<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
														<div class="easy-pie-chart txt-color-darken easyPieChart" data-percent="36" data-pie-size="50" style="width: 50px; height: 50px; line-height: 50px;">
															<span>3</span>
														<canvas width="50" height="50"></canvas></div>
														<span class="easy-pie-title"> Disposed </span>
														
														<div class="sparkline txt-color-red hidden-sm hidden-md pull-right" data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent"><canvas width="70" height="33" style="display: inline-block; width: 70px; height: 33px; vertical-align: top;"></canvas></div>
													</div>
												</div>

											</div>
											<!-- end s1 tab pane -->

											<div class="tab-pane fade " id="s2">
												<div class="widget-body-toolbar bg-color-white">
                                            <div class="form-inline" role="form">
                                            <div class="form-group">
															<label class="sr-only" for="s123">Show From</label>
                                                            <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
															<asp:TextBox runat="server" id="txtDatefrom2" placeholder="Date From" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox></label>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                                                 ControlToValidate="txtDatefrom2" 
                                                                 ErrorMessage="*" 
                                                                 ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                                                                 ValidationGroup="2" style="font-size: small">*</asp:RegularExpressionValidator>
														</div>
														<div class="form-group">
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
															<asp:TextBox runat="server" id="txtDateTo2" placeholder="Date To" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox></label>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                                                              ControlToValidate="txtDateTo2" 
                                                              ErrorMessage="*" 
                                                              ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                                                              ValidationGroup="2" style="font-size: small">*</asp:RegularExpressionValidator>
                                                              <asp:LinkButton ID="lnkView2" runat="server" CssClass="btn btn-xs btn-primary"
                                                               ValidationGroup="2">View</asp:LinkButton>
														</div>
                                                        </div>
                                            
												</div>
                                                <div class="row no-space">
									               <asp:Literal ID="ltrFrequentlyMaindIns" runat="server"></asp:Literal>
                                                   </div>
												</div>
												
											
											<!-- end s2 tab pane -->

											<div class="tab-pane fade" id="s3">

												<div class="widget-body-toolbar bg-color-white">

									               <div class="form-inline" role="form">
                                            <div class="form-group">
															<label class="sr-only" for="s123">Show From</label>
                                                            <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
															<asp:TextBox runat="server" id="txtDatefrom3" placeholder="Date From" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox></label>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" 
                                                                 ControlToValidate="txtDatefrom3" 
                                                                 ErrorMessage="*" 
                                                                 ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                                                                 ValidationGroup="2" style="font-size: small">*</asp:RegularExpressionValidator>
														</div>
														<div class="form-group">
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
															<asp:TextBox runat="server" id="txtDateTo3" placeholder="Date To" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox></label>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
                                                              ControlToValidate="txtDateTo3" 
                                                              ErrorMessage="*" 
                                                              ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                                                              ValidationGroup="2" style="font-size: small">*</asp:RegularExpressionValidator>
                                                              <asp:LinkButton ID="lnkView3" runat="server" CssClass="btn btn-xs btn-primary"
                                                               ValidationGroup="2">View</asp:LinkButton>
														</div>
                                                        </div>
							

												</div>
                                                <div class="row no-space">
												<asp:Literal ID="ltrFrequentProblemNotificationByManu" runat="server"></asp:Literal></div>
											</div>
                                            <div class="tab-pane fade" id="s4">
                                            	<div class="widget-body-toolbar bg-color-white" >
                                                      <div class="form-inline" role="form">
                                                        <div class="form-group">
															<label class="sr-only" for="s123">Show From</label>
                                                            <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
															<asp:TextBox runat="server" id="txtDatefrom4" placeholder="Date From" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox></label>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" 
                                                                 ControlToValidate="txtDatefrom4" 
                                                                 ErrorMessage="*" 
                                                                 ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                                                                 ValidationGroup="2" style="font-size: small">*</asp:RegularExpressionValidator>
														</div>
														<div class="form-group">
                                                        <label class="input">
                                                            <i class="icon-append fa fa-calendar"></i>
															<asp:TextBox runat="server" id="txtDateTo4" placeholder="Date To" CssClass="form-control datepicker" data-dateformat="mm/dd/yy"></asp:TextBox></label>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" 
                                                              ControlToValidate="txtDateTo4" 
                                                              ErrorMessage="*" 
                                                              ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                                                              ValidationGroup="2" style="font-size: small">*</asp:RegularExpressionValidator>
                                                              <asp:LinkButton ID="lnkView4" runat="server" CssClass="btn btn-xs btn-primary"
                                                               ValidationGroup="2">View</asp:LinkButton>
														</div>
                                                      </div>

													

                                             <div class="row no-space">
												<asp:Literal ID="ltrFrequentlyReportedProblemTypes" runat="server"></asp:Literal></div>
											</div>
											<!-- end s3 tab pane -->
                                            </div>
										</div>

										<!-- end content -->
									</div>

								</div>
								<!-- end widget div -->
							</div>
    </article>
    <article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">


						<div class="jarviswidget jarviswidget-sortable" id="wid-id-2" data-widget-colorbutton="false" data-widget-editbutton="false" role="widget" style="">

								

								<header role="heading"><div class="jarviswidget-ctrls" role="menu"></div>
									<span class="widget-icon"> <i class="fa fa-map-marker"></i> </span>
									<h2>Regional Summary / Hover on each State/Region to view summary</h2>
									<div class="widget-toolbar hidden-mobile" role="menu">
										
									</div>
								</header>

								<!-- widget div-->
								<div role="content">
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<div>
											<label>Title:</label>
											<input type="text">
										</div>
									</div>
									<!-- end widget edit box -->

									<div class="widget-body no-padding">
										<!-- content goes here -->

										
                                        <asp:Literal ID="ltrmap" runat="server"></asp:Literal>
										

										<!-- end content -->

									</div>

								</div>
								<!-- end widget div -->
							</div>
							</article>
 <article class="col-sm-12 col-md-12 col-lg-6 sortable-grid ui-sortable">
                            <div class="jarviswidget jarviswidget-color-blue jarviswidget-sortable" id="wid-id-4" data-widget-editbutton="false" data-widget-colorbutton="false" role="widget" style="">

								<!-- widget options:
								usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

								data-widget-colorbutton="false"
								data-widget-editbutton="false"
								data-widget-togglebutton="false"
								data-widget-deletebutton="false"
								data-widget-fullscreenbutton="false"
								data-widget-custombutton="false"
								data-widget-collapsed="true"
								data-widget-sortable="false"

								-->

								<header role="heading"><div class="jarviswidget-ctrls" role="menu">   <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a></div>
									<span class="widget-icon"> <i class="fa fa-check txt-color-white"></i> </span>
									<h2> ToDo's </h2>
									<!-- <div class="widget-toolbar">
									add: non-hidden - to disable auto hide

									</div>-->
								<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

								<!-- widget div-->
								<div role="content">
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<div>
											<label>Title:</label>
											<input type="text">
										</div>
									</div>
									<!-- end widget edit box -->

									<div class="widget-body no-padding smart-form">
										<!-- content goes here -->
										<h5 class="todo-group-title"><i class="fa fa-warning"></i> Critical Tasks (<small class="num-of-tasks">1</small>)</h5>
										<ul id="sortable1" class="todo ui-sortable">
											<li>
												<span class="handle"> <label class="checkbox">
														<input type="checkbox" name="checkbox-inline">
														<i></i> </label> </span>
												<p>
													<strong> #17</strong> - Reported Problems [<a href="javascript:void(0);" class="font-xs">More Details</a>]
													
												</p>
											</li>
										</ul>
										<h5 class="todo-group-title"><i class="fa fa-exclamation"></i> Important Tasks (<small class="num-of-tasks">3</small>)</h5>
										<ul id="sortable2" class="todo ui-sortable">
											<li>
												<span class="handle"> <label class="checkbox">
														<input type="checkbox" name="checkbox-inline">
														<i></i> </label> </span>
												<p>
													<strong>#13</strong> - Assigned Jobs [<a href="javascript:void(0);" class="font-xs">More Details</a>] 
												</p>
											</li>
											<li>
												<span class="handle"> <label class="checkbox">
														<input type="checkbox" name="checkbox-inline">
														<i></i> </label> </span>
												<p>
													<strong>#8</strong> - Sparpart Approvals <small>(review)</small>
													
												</p>
											</li>
											<li>
												<span class="handle"> <label class="checkbox">
														<input type="checkbox" name="checkbox-inline">
														<i></i> </label> </span>
												<p>
													<strong>#8</strong> - Sparpart Issuance
													
												</p>
											</li>
										</ul>

								
										<!-- end content -->
									</div>

								</div>
								<!-- end widget div -->
							</div>
                            </article>
                            </div>
</asp:Content>