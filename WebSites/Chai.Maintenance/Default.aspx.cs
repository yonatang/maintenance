using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Chai.Maintenance.Modules.Shell.Views;
using Microsoft.Practices.ObjectBuilder;
using System.Collections;
using System.Text;
using Chai.Maintenance.Shared;
using Chai.EID.Shared.FusionUtil;

public partial class ShellDefault : Microsoft.Practices.CompositeWeb.Web.UI.Page, IDefaultView
{
    private DefaultViewPresenter _presenter;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this._presenter.OnViewInitialized();
        }
        this._presenter.OnViewLoaded();
        MapData();
        BindNotifiedProblemsChart();
        BindFrequentlyMaintainedInstrumentsChart();
        BindFrequentProblemNotificationOnInstrumentsByManufacturerChart();
        BindFrequentlyReportedProblemTypesChart();
    }

    [CreateNew]
    public DefaultViewPresenter Presenter
    {
        get
		{
			return this._presenter;
		}
        set
        {
            if(value == null)
                throw new ArgumentNullException("value");

            this._presenter = value;
            this._presenter.View = this;
        }
    }
    public void MapData()
    {
        IList result = _presenter.GetMapStatisticReport();
        //string[] Provisionalcolor = {"996633","663300","FF9933","FFCC99","CCFF99","99CC99","CC9900","CCCCCC","999999","333333","336633","333366","CC0033","999933","6699FF","336666","339999","336633","66FF99","CCCC33",};
        string[] Provisionalcolor = { "996633", "663300", "FF9933", "FFCC99", "CCFF99", "99CC99", "CC9900", "CCCCCC", "999999", "333333", "336633", "66FF99", "CCCC33" };
        //for (int i = 0; i < Provisionalcolor.Length; i++)
        //{
        int i = 0;
        foreach (object[] pr in result)
        {
            if (pr[4].ToString() != "")
            {
                pr.SetValue(Provisionalcolor[i], 5);
                i++;
            }
        }
        StringBuilder strXML = new StringBuilder();

        //Initialize <map> element
        strXML.Append("<map caption='Regional Summary' showCanvasBorder='0' bgalpha='0' showLabels='1' borderColor = 'FFFFFF' bgColor='FFFFFF' fillColor='CCCC33' includeNameInLabels='1' fillAlpha='80' showlevel='0' fillAlpha='80' showlevel='1' legendPosition='Bottom' toolTipBgColor='000000' showToolTipShadow='1' baseFontColor='FFFFFF'>");

        // Open data element that will store map data
        strXML.Append("<data>");

        // Use Data from array for each entity
        foreach (object[] pr in result)
        {

            //  Set each map <entity> id and value
            strXML.AppendFormat("<entity id='{0}' value='{1}' color='" + pr[5] + "' toolText='{2}' link='Default.aspx?mapid= " + pr[4] + "&Name=" + pr[3] + "'/>", pr[4], 3, pr[3] + "{br}" + "No. Site:- " + pr[1].ToString() + "{br}" + "No. Instrument:- " + pr[0].ToString() + "{br}" + "Not Functional Ins. :- " + pr[2].ToString());

            //strXML.AppendFormat("<color='"+FF5904+"'>")
        }

        // close data element
        strXML.Append("</data>");
        //close map element
        strXML.Append("</map>");


        string country = TechnicalConfig.GetConfiguration()["Country"].ToString();
        string mapHTML = FusionMaps.RenderMap("FusionMaps/FCMap_" + country + ".swf", "", strXML.ToString(), "mapid", "530", "450", false, false);

        //embed the chart rendered as HTML into Literal - WorldPopulationMap
        ltrmap.Text = mapHTML;
    }
    void BindNotifiedProblemsChart()
    {

        IList result = _presenter.GetNotifiedProblemsChart(datefrom1.Text, DateTto1.Text);


        StringBuilder xmlData = new StringBuilder();

        //StringBuilder xmlSeriesfour = new StringBuilder();
        xmlData.Append("<chart caption='Notified Problems By Region' xAxisName='Region' yAxisName='No Of Notified Problems' paletteColors='0075c2' baseFontColor ='333333' baseFont='Helvetica Neue,Arial' captionFontSize='14' subcaptionFontSize='14' subcaptionFontBold='0' showBorder='0' bgColor='ffffff'  showShadow='0'  canvasBgColor='ffffff' canvasBorderAlpha='7' valueFontColor='7994D2' placeValuesInside='1' rotateValues='1' plotgradientcolor='' useRoundEdges='0' showPlotBorder='0' showAlternateHGridColor='0' showAlternateVGridColor='0' valueFontSize='10' divlineAlpha='20' divlineColor='999999' divlineThickness='0' divLineIsDashed='1' divLineDashLen='1' divLineGapLen='1' showHoverEffect='1' showXAxisLine='1' xAxisLineThickness='1' xAxisLineColor='7994D2' labelDisplay='Rotate' slantLabels='1'>");
        //Iterate through the data	
        foreach (object[] pr in result)
        {

            xmlData.AppendFormat("<set label='{0}' value='{1}' color='7994D2'/>", pr[1], pr[0]);


        }


        xmlData.Append("</chart>");





        ltrNotifiedProblems.Text = FusionCharts.RenderChart("FusionCharts/Column2D.swf", "", xmlData.ToString(), "NotifiedProblems", "1080", "300", false, false);

    }
    void BindFrequentlyMaintainedInstrumentsChart()
    {
        IList result = _presenter.GetFrequentlyMaintainedInstrumentsChart(txtDatefrom2.Text, txtDateTo2.Text);

        StringBuilder xmlData = new StringBuilder();

        //StringBuilder xmlSeriesfour = new StringBuilder();
        xmlData.Append("<chart caption='Notified Problems By Instrument' xAxisName='Instrument' yAxisName='No Of Problems' paletteColors='0075c2' baseFontColor ='333333' baseFont='Helvetica Neue,Arial' captionFontSize='14' subcaptionFontSize='14' subcaptionFontBold='0' showBorder='0' bgColor='ffffff'  showShadow='0'  canvasBgColor='ffffff' canvasBorderAlpha='7' valueFontColor='7994D2' placeValuesInside='1' rotateValues='1' plotgradientcolor='' useRoundEdges='0' showPlotBorder='0' showAlternateHGridColor='0' showAlternateVGridColor='0' valueFontSize='10' divlineAlpha='20' divlineColor='999999' divlineThickness='0' divLineIsDashed='1' divLineDashLen='1' divLineGapLen='1' showHoverEffect='1' showXAxisLine='1' xAxisLineThickness='1' xAxisLineColor='7994D2' labelDisplay='Rotate' slantLabels='1'>");
        //Iterate through the data	
        foreach (object[] pr in result)
        {

            xmlData.AppendFormat("<set label='{0}' value='{1}' color='7994D2'/>", pr[1], pr[0]);


        }


        xmlData.Append("</chart>");





        ltrFrequentlyMaindIns.Text = FusionCharts.RenderChart("FusionCharts/Column2D.swf", "", xmlData.ToString(), "frequntlymaintaned", "1080", "300", false, false);




    }

    void BindFrequentProblemNotificationOnInstrumentsByManufacturerChart()
    {

        IList result = _presenter.GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(txtDatefrom3.Text, txtDateTo3.Text);

        StringBuilder xmlData = new StringBuilder();

        //StringBuilder xmlSeriesfour = new StringBuilder();
        xmlData.Append("<chart caption='Problems By Manufacturer' xAxisName='Manufacturer' yAxisName='No Of Notified problem' paletteColors='0075c2' baseFontColor ='333333' baseFont='Helvetica Neue,Arial' captionFontSize='14' subcaptionFontSize='14' subcaptionFontBold='0' showBorder='0' bgColor='ffffff'  showShadow='0'  canvasBgColor='ffffff' canvasBorderAlpha='7' valueFontColor='7994D2' placeValuesInside='1' rotateValues='1' plotgradientcolor='' useRoundEdges='0' showPlotBorder='0' showAlternateHGridColor='0' showAlternateVGridColor='0' valueFontSize='10' divlineAlpha='20' divlineColor='999999' divlineThickness='0' divLineIsDashed='1' divLineDashLen='1' divLineGapLen='1' showHoverEffect='1' showXAxisLine='1' xAxisLineThickness='1' xAxisLineColor='7994D2' labelDisplay='Rotate' slantLabels='1'>");
        //Iterate through the data	
        foreach (object[] pr in result)
        {

            xmlData.AppendFormat("<set label='{0}' value='{1}' color='7994D2'/>", pr[1], pr[0]);


        }


        xmlData.Append("</chart>");





        ltrFrequentProblemNotificationByManu.Text = FusionCharts.RenderChart("FusionCharts/Column2D.swf", "", xmlData.ToString(), "FrequentProblemNotificationByManu", "1080", "300", false, false);



    }

    void BindFrequentlyReportedProblemTypesChart()
    {
        IList result = _presenter.GetRFrequentlyReportedProblemTypesChart(txtDatefrom4.Text, txtDateTo4.Text);

        StringBuilder xmlData = new StringBuilder();

        //StringBuilder xmlSeriesfour = new StringBuilder();
        xmlData.Append("<chart caption='Problems By Problem Type' xAxisName='Problem Type' yAxisName='No Of Problems' paletteColors='0075c2' baseFontColor ='333333' baseFont='Helvetica Neue,Arial' captionFontSize='14' subcaptionFontSize='14' subcaptionFontBold='0' showBorder='0' bgColor='ffffff'  showShadow='0'  canvasBgColor='ffffff' canvasBorderAlpha='7' valueFontColor='7994D2' placeValuesInside='1' rotateValues='1' plotgradientcolor='' useRoundEdges='0' showPlotBorder='0' showAlternateHGridColor='0' showAlternateVGridColor='0' valueFontSize='10' divlineAlpha='20' divlineColor='999999' divlineThickness='0' divLineIsDashed='1' divLineDashLen='1' divLineGapLen='1' showHoverEffect='1' showXAxisLine='1' xAxisLineThickness='1' xAxisLineColor='7994D2' labelDisplay='Rotate' slantLabels='1'>");
        //Iterate through the data	
        foreach (object[] pr in result)
        {

            xmlData.AppendFormat("<set label='{0}' value='{1}' color='7994D2'/>", pr[1], pr[0]);


        }


        xmlData.Append("</chart>");





        ltrFrequentlyReportedProblemTypes.Text = FusionCharts.RenderChart("FusionCharts/Column2D.swf", "", xmlData.ToString(), "FrequentlyReportedProblemTypes", "1080", "300", false, false);


    }
}
