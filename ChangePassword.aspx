﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="ChangePassword, App_Web_smodhl5n" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
     <asp:Panel ID="Panel2" runat="server" CssClass="group">
    
   <%-- <asp:ChangePassword ID="ChangePassword1" runat="server" BackColor="#F7F6F3" 
             BorderColor="#E6E2D8" BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" 
             Font-Names="Verdana" Font-Size="0.8em">
        <CancelButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" 
            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" 
            ForeColor="#284775" />
        <ChangePasswordButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" 
            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" 
            ForeColor="#284775" />
        <ChangePasswordTemplate>
            <table cellpadding="1" cellspacing="0" style="border-collapse:collapse;">
                <tr>
                    <td>
                        <table cellpadding="0">
                            <tr>
                                <td align="center" colspan="2">
                                    Change Your Password</td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="CurrentPasswordLabel" runat="server" 
                                        AssociatedControlID="CurrentPassword">Password:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" 
                                        ControlToValidate="CurrentPassword" ErrorMessage="Password is required." 
                                        ToolTip="Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="NewPasswordLabel" runat="server" 
                                        AssociatedControlID="NewPassword">New Password:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="NewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" 
                                        ControlToValidate="NewPassword" ErrorMessage="New Password is required." 
                                        ToolTip="New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Label ID="ConfirmNewPasswordLabel" runat="server" 
                                        AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" 
                                        ControlToValidate="ConfirmNewPassword" 
                                        ErrorMessage="Confirm New Password is required." 
                                        ToolTip="Confirm New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:CompareValidator ID="CpVconfirm" runat="server" 
                                        ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" 
                                        ErrorMessage="Your passwords do not match up!" SetFocusOnError="True" 
                                        ValidationGroup="ChangePassword1"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="color:Red;">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="ChangePasswordPushButton" runat="server" 
                                        CommandName="ChangePassword" onclick="ChangePasswordPushButton_Click" 
                                        Text="Change Password" ValidationGroup="ChangePassword1" />
                                </td>
                                <td>
                                    <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" 
                                        CommandName="Cancel" Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
            </table>
        </ChangePasswordTemplate>
        <ContinueButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" 
            BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" 
            ForeColor="#284775" />
        <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
        <PasswordHintStyle Font-Italic="True" ForeColor="#888888" />
        <TextBoxStyle Font-Size="0.8em" />
        <TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" Font-Size="0.9em" 
            ForeColor="White" />
    </asp:ChangePassword>--%>
         <br />
         <table style="border: 1px solid #E6E2D8; background-color: #F7F6F3; font-family: verdana; font-size: 0.8px;">
             <tr>
                 <td>
                     <table cellpadding="0">
                         <tr>
                             <td align="center" colspan="2">
                                 Change Your Password</td>
                         </tr>
                         <tr>
                             <td align="right">
                                 <asp:Label ID="CurrentPasswordLabel0" runat="server" 
                                     AssociatedControlID="CurrentPassword">Password:</asp:Label>
                             </td>
                             <td>
                                 <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password"></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="CurrentPasswordRequired0" runat="server" 
                                     ControlToValidate="CurrentPassword" ErrorMessage="Password is required." 
                                     ToolTip="Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                             </td>
                         </tr>
                         <tr>
                             <td align="right">
                                 <asp:Label ID="NewPasswordLabel0" runat="server" 
                                     AssociatedControlID="NewPassword">New Password:</asp:Label>
                             </td>
                             <td>
                                 <asp:TextBox ID="NewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="NewPasswordRequired0" runat="server" 
                                     ControlToValidate="NewPassword" ErrorMessage="New Password is required." 
                                     ToolTip="New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                             </td>
                         </tr>
                         <tr>
                             <td align="right">
                                 <asp:Label ID="ConfirmNewPasswordLabel0" runat="server" 
                                     AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label>
                             </td>
                             <td>
                                 <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired0" runat="server" 
                                     ControlToValidate="ConfirmNewPassword" 
                                     ErrorMessage="Confirm New Password is required." 
                                     ToolTip="Confirm New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                             </td>
                         </tr>
                         <tr>
                             <td align="center" colspan="2">
                                 <asp:CompareValidator ID="CpVconfirm0" runat="server" 
                                     ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" 
                                     ErrorMessage="Your passwords do not match up!" SetFocusOnError="True" 
                                     ValidationGroup="ChangePassword1"></asp:CompareValidator>
                             </td>
                         </tr>
                         <tr>
                             <td align="center" colspan="2" style="color:Red;">
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td align="right">
                                 <asp:Button ID="ChangePasswordPushButton0" runat="server" 
                                     CommandName="ChangePassword" onclick="ChangePasswordPushButton_Click" 
                                     Text="Change Password" ValidationGroup="ChangePassword1" />
                             </td>
                             <td>
                                 <asp:Button ID="CancelPushButton0" runat="server" CausesValidation="False" 
                                     CommandName="Cancel" Text="Cancel" onclick="CancelPushButton0_Click" />
                             </td>
                         </tr>
                     </table>
                 </td>
             </tr>
         </table>
     </asp:Panel>
</asp:Content>

