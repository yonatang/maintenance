﻿using System;
using System.Security;
using System.Security.Principal;

namespace Chai.Maintenance.CoreDomain
{
    public class ChaiPrincipal: IPrincipal
    {
        private User _user;

		/// <summary>
		/// 
		/// </summary>
		public IIdentity Identity
		{
			get { return this._user; }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="role"></param>
		/// <returns></returns>
		public bool IsInRole(string role)
		{
			foreach (UserRole roleObject in this._user.UserRoles)
			{
				if (roleObject.Role.Name.Equals(role))
					return true;
			}
			return false;
		}

		/// <summary>
		/// Default constructor. An instance of an authenticated user is required when creating this principal.
		/// </summary>
		/// <param name="user"></param>
		public ChaiPrincipal(User user)
		{
			if (user != null && user.IsAuthenticated)
			{
				this._user = user;
			}
			else
			{
				throw new SecurityException("Cannot create a principal without a valid user");
			}
		}

    }
}
