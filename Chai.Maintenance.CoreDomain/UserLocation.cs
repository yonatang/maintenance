﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Shared;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.CoreDomain
{
    public class UserLocation:BaseEntity,IBaseEntity
    {
        #region Business Properties and Methods

        //declare members
        private int _id = 0;
        private int _userId = 0;
        private int _regionId = 0;
        private int _siteId = 0;

       

        public int UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                if (!_userId.Equals(value))
                {
                    _userId = value;
                }
            }
        }

        public int RegionId
        {
            get
            {
                return _regionId;
            }
            set
            {
                if (!_regionId.Equals(value))
                {
                    _regionId = value;
                   
                }
            }
        }

        public int SiteId
        {
            get
            {
                return _siteId;
            }
            set
            {
                if (!_siteId.Equals(value))
                {
                    _siteId = value;
                   
                }
            }
        }

      

        #endregion //Business Properties and Methods
    }
}
