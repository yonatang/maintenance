﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{
   public class MaintenanceHistory : BaseEntity, IBaseEntity
    {
        //From Problem Table
        private string _problemNumber = string.Empty;
        private string _description = string.Empty;
        private string _problemType = string.Empty;
        private string _errorCode = string.Empty;
        private DateTime _dateReported = DateTime.Now;

        //From Curative Maintenance Table
        private string _enginerName = string.Empty;
        private DateTime _maintainedDate = DateTime.Now;
        private string _enginerComment = string.Empty;
        private string _causeOfFailure = string.Empty;
        private string _actionTaken = string.Empty;
        private string _maintainedParts = string.Empty;


        public string ProblemNumber
        {
            get { return _problemNumber; }
            set { _problemNumber = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string ProblemType
        {
            get { return _problemType; }
            set { _problemType = value; }
        }

        public string ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }

        public DateTime DateReported
        {
            get { return _dateReported; }
            set { _dateReported = value; }
        }

        public string EnginerName
        {
            get { return _enginerName; }
            set { _enginerName = value; }
        }

        public string CauseOfFailure
        {
            get { return _causeOfFailure; }
            set { _causeOfFailure = value; }
        }

        public string MaintainedParts
        {
            get { return _maintainedParts; }
            set { _maintainedParts = value; }
        }

        public string ActionTaken
        {
            get { return _actionTaken; }
            set { _actionTaken = value; }
        }

        public string EnginerComment
        {
            get { return _enginerComment; }
            set { _enginerComment = value; }
        }

        public DateTime MaintainedDate
        {
            get { return _maintainedDate; }
            set { _maintainedDate = value; }
        }

    }
}
