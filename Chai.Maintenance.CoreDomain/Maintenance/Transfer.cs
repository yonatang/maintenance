﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{

    public class Transfer : BaseEntity, IBaseEntity
    {
        private int _instrumentId = 0;
        private int _TransferToSiteId = 0;
        private int _CurrentSiteId = 0;
        private int _TransferedById = 0;
        private int _NewUserId = 0;
        private string _Reason = string.Empty;
        private DateTime _Date = DateTime.Now;
        private string _InstrumentName = string.Empty;
        private string _NewSite = string.Empty;
        private string _OldSite = string.Empty;
        public int InstrumentId
        {
            get
            {
                return _instrumentId;
            }
            set
            {
                if (!_instrumentId.Equals(value))
                {
                    _instrumentId = value;

                }
            }
        }

        public int TransferToSiteId
        {
            get
            {
                return _TransferToSiteId;
            }
            set
            {
                if (!_TransferToSiteId.Equals(value))
                {
                    _TransferToSiteId = value;

                }
            }
        }
        public string InstrumentName
        {
            get
            {
                return _InstrumentName;
            }
            set
            {

                _InstrumentName = value;


            }
        }
        public string NewSite
        {
            get
            {
                return _NewSite;
            }
            set
            {
                _NewSite = value;
            }
        }
        public string OldSite
        {
            get
            {
                return _OldSite;
            }
            set
            {
                _OldSite = value;
            }
        }
        public int CurrentSiteId
        {
            get
            {
                return _CurrentSiteId;
            }
            set
            {
                if (!_CurrentSiteId.Equals(value))
                {
                    _CurrentSiteId = value;

                }
            }
        }

        public int TransferedById
        {
            get
            {
                return _TransferedById;
            }
            set
            {
                if (!_TransferedById.Equals(value))
                {
                    _TransferedById = value;

                }
            }
        }

        public int NewUserId
        {
            get
            {
                return _NewUserId;
            }
            set
            {
                if (!_NewUserId.Equals(value))
                {
                    _NewUserId = value;

                }
            }
        }

        public string Reason
        {
            get
            {
                return _Reason;
            }
            set
            {

                _Reason = value;


            }
        }

        public DateTime Date 
        {
            get
            {
                return _Date;
            }
            set
            {
                _Date = value;
            }
        }
    }
}
