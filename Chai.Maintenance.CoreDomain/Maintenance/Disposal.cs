﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{

    public class Disposal : BaseEntity, IBaseEntity
    {
        private int _instrumentId = 0;
        private int _DisposedByID = 0;
        private string _Reason = string.Empty;
        private DateTime _Date = DateTime.Now;
        private string _InstrumentName = string.Empty;
        private string _SiteName = string.Empty;
        public int InstrumentId
        {
            get
            {
                return _instrumentId;
            }
            set
            {
                if (!_instrumentId.Equals(value))
                {
                    _instrumentId = value;

                }
            }
        }
        public int DisposedByID
        {
            get
            {
                return _DisposedByID;
            }
            set
            {
                if (!_DisposedByID.Equals(value))
                {
                    _DisposedByID = value;

                }
            }
        }
        public string Reason
        {
            get
            {
                return _Reason;
            }
            set
            {

                _Reason = value;


            }
        }
        public string InstrumentName
        {
            get
            {
                return _InstrumentName;
            }
            set
            {

                _InstrumentName = value;


            }
        }
        public string SiteName
        {
            get
            {
                return _SiteName;
            }
            set
            {

                _SiteName = value;


            }
        }
        public DateTime Date
        {
            get
            {
                return _Date;
            }
            set
            {
                _Date = value;
            }
        }
    }
}
