﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{

    public class Spareparts : BaseEntity, IBaseEntity
    {

       
        private int _curativeMaintenanceId = 0;
        private int _preventiveMaintenanceId = 0;
        private int _sparepartId = 0;
      
        private int _quantity = 0;

        private string _sparePartName = string.Empty;

        public int CurativeMaintenanceId
        {
            get
            {
                
                return _curativeMaintenanceId;
            }
            set
            {
               
                if (!_curativeMaintenanceId.Equals(value))
                {
                    _curativeMaintenanceId = value;
                    
                }
            }
        }

        public int PreventiveMaintenanceId
        {
            get
            {
                
                return _preventiveMaintenanceId;
            }
            set
            {
               
                if (!_preventiveMaintenanceId.Equals(value))
                {
                    _preventiveMaintenanceId = value;
                    
                }
            }
        }

        public int SparepartId
        {
            get
            {
               
                return _sparepartId;
            }
            set
            {
                
                if (!_sparepartId.Equals(value))
                {
                    _sparepartId = value;
                    
                }
            }
        }

        

        public int Quantity
        {
            get
            {
              
                return _quantity;
            }
            set
            {
                
                if (!_quantity.Equals(value))
                {
                    _quantity = value;
                    
                }
            }
        }


        public string SparePartName
        {
            get { return _sparePartName; }
            set { _sparePartName = value; }
        }
    }
}
