﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{

    public class EscalatedJobs : BaseEntity, IBaseEntity
    {

        private int _instrumentId = 0;
        private int _problemId = 0;
        private int _scheduleId = 0;
        private int _escalatedFromRegionId = 0;
        private int _enginerEscalateReasonId = 0;
        private string _status = string.Empty;

        private string _RegionName = string.Empty;
        private string _InstrumentName = string.Empty;
        private string _ProblemNumber = string.Empty;
        private string _SiteName = string.Empty;
        private string _EnginerName = string.Empty;
        private string _SerialNumber = string.Empty;


        public int InstrumentId
        {
            get
            {
                return _instrumentId;
            }
            set
            {
                if (!_instrumentId.Equals(value))
                {
                    _instrumentId = value;

                }
            }
        }
        public int ProblemId
        {
            get
            {
                return _problemId;
            }
            set
            {
                if (!_problemId.Equals(value))
                {
                    _problemId = value;

                }
            }
        }
        public int ScheduleId
        {
            get
            {
                return _scheduleId;
            }
            set
            {
                if (!_scheduleId.Equals(value))
                {
                    _scheduleId = value;

                }
            }
        }
        public int EscalatedFromRegionId
        {
            get
            {
                return _escalatedFromRegionId;
            }
            set
            {
                if (!_escalatedFromRegionId.Equals(value))
                {
                    _escalatedFromRegionId = value;

                }
            }
        }
        public int EnginerEscalateReasonId
        {
            get
            {
                return _enginerEscalateReasonId;
            }
            set
            {
                if (!_enginerEscalateReasonId.Equals(value))
                {
                    _enginerEscalateReasonId = value;

                }
            }
        }
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {

                _status = value;


            }
        }


        public string RegionName
        {
            get{return _RegionName;}
            set{_RegionName = value;}
        }
        public string InstrumentName
        {
            get { return _InstrumentName; }
            set { _InstrumentName = value; }
        }
        public string ProblemNumber
        {
            get { return _ProblemNumber; }
            set { _ProblemNumber = value; }
        }
        public string SiteName
        {
            get { return _SiteName; }
            set { _SiteName = value; }
        }
        public string EnginerName
        {
            get { return _EnginerName; }
            set { _EnginerName = value; }
        }
        public string SerialNo
        {
            get { return _SerialNumber; }
            set { _SerialNumber = value; }
        }
    }
}
