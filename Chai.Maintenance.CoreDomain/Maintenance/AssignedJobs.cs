﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{

    public class AssignedJobs : BaseEntity, IBaseEntity
    {
        private string _serialNo = string.Empty;
        private DateTime _installationDate = DateTime.Now;
        private DateTime _warranityExpireDate = DateTime.Now;
        private string _preventiveMaintenancePeriod = string.Empty;
        private bool _isFunctional = false;
        private string _sitename = string.Empty;
        private string _instrumentname = string.Empty;
        private DateTime _lastPreventiveMaintenanceDate = DateTime.Now;

        private int _ProblemId = 0;

        private string _problemNumber = string.Empty;
        private string _ProblemDescription = string.Empty;
        private DateTime _scheduledDateFrom = DateTime.Now;
        private DateTime _scheduledDateTo = DateTime.Now;
        private string _siteName = string.Empty;
        private string _schedulerFullName = string.Empty;
        private int _instrumentId = 0;
        private string _problemType = string.Empty;
        private string _errorCode = string.Empty;
        private string _PreventiveScheduleStatus = string.Empty;
        private string _type = string.Empty;
        private int _ScheduleID = 0;

        public string SiteName
        {
            get { return _sitename; }
            set { _sitename = value; }
        }

        public string InstrumentName
        {
            get { return _instrumentname; }
            set { _instrumentname = value; }
        }

        public string SerialNo
        {
            get
            {
                return _serialNo;
            }
            set
            {

                _serialNo = value;


            }
        }

        public DateTime InstallationDate
        {
            get
            {
                return _installationDate;
            }
            set
            {
                _installationDate = value;
            }
        }

        public DateTime WarranityExpireDate
        {
            get
            {
                return _warranityExpireDate;
            }
            set
            {
                _warranityExpireDate = value;
            }
        }

        public DateTime LastPreventiveMaintenanceDate
        {
            get
            {
                return _lastPreventiveMaintenanceDate;
            }
            set
            {
                _lastPreventiveMaintenanceDate = value;
            }
        }

        public string PreventiveMaintenancePeriod
        {
            get
            {
                return _preventiveMaintenancePeriod;
            }
            set
            {

                _preventiveMaintenancePeriod = value;


            }
        }
        public int ScheduleID
        {
            get
            {
                return _ScheduleID;
            }
            set
            {
                if (!_ScheduleID.Equals(value))
                {
                    _ScheduleID = value;

                }
            }
        }
        public bool IsFunctional
        {
            get
            {
                return _isFunctional;
            }
            set
            {

                _isFunctional = value;


            }
        }

        public string ProblemNumber
        {
            get
            {
                return _problemNumber;
            }
            set
            {

                _problemNumber = value;


            }
        }

        public string ProblemDescription
        {
            get
            {
                return _ProblemDescription;
            }
            set
            {

                _ProblemDescription = value;


            }
        }

        public DateTime ScheduledDateFrom
        {
            get
            {
                return _scheduledDateFrom;
            }
            set
            {
                _scheduledDateFrom = value;
            }
        }

        public DateTime ScheduledDateTo
        {
            get
            {
                return _scheduledDateTo;
            }
            set
            {
                _scheduledDateTo = value;
            }
        }

        public string SchedulerFullName
        {
            get
            {
                return _schedulerFullName;
            }
            set
            {

                _schedulerFullName = value;


            }
        }

        public int InstrumentId
        {
            get
            {
                return _instrumentId;
            }
            set
            {
                if (!_instrumentId.Equals(value))
                {
                    _instrumentId = value;

                }
            }
        }
        public int ProblemId
        {
            get
            {
                return _ProblemId;
            }
            set
            {
                if (!_ProblemId.Equals(value))
                {
                    _ProblemId = value;

                }
            }
        }
        public string ProblemType
        {
            get { return _problemType; }
            set { _problemType = value; }
        }

        public string ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }

        public string Type
        {
            get
            {
                if (_ProblemId == 0)
                    return "Prevetive";
                else
                    return "Curative";
            }
            //set
            //{
            //    _type = value;
            //}
        }
        public string PreventiveScheduleStatus
        {
            get
            {
                return _PreventiveScheduleStatus;
            }
            set
            {

                _PreventiveScheduleStatus = value;


            }
        }

    }
}