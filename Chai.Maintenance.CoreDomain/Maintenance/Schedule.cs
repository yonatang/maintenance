﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{

    public class Schedule : BaseEntity, IBaseEntity
    {
        private int _id = 0;
        private string _description = string.Empty;
        private int _instrumentId = 0;
        private int _siteId = 0;
       
     
        private string _InstrumentName = string.Empty;
        private string _siteName = string.Empty;


        private DateTime _scheduledDateFrom = DateTime.Now;
        private DateTime _scheduledDateTo = DateTime.Now;
        private int _scheduleForId = 0;
        private int _problemId = 0;
    
        private bool _isExternal = false;
        private DateTime _date = DateTime.Now;
        
        private int _enginerId = 0;
        private string _FirstName = string.Empty;
        private string _LastName = string.Empty;
        private string _VendorName = string.Empty;
        private string _pushedReason = string.Empty;
        private int _userId = 0;
        private string _PreventiveScheduleStatus = string.Empty;
        private string _CurativeScheduleStatus = string.Empty;
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (!_id.Equals(value))
                {
                    _id = value;

                }
            }
        }
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {

                _description = value;


            }
        } 
        public string InstrumentName
        {
            get
            {
                return _InstrumentName;
            }
            set
            {

                _InstrumentName = value;


            }
        }
        public string SiteName
        {
            get
            {
                return _siteName;
            }
            set
            {

                _siteName = value;


            }
        }
        public int InstrumentId
        {
            get
            {
                return _instrumentId;
            }
            set
            {
                if (!_instrumentId.Equals(value))
                {
                    _instrumentId = value;

                }
            }
        }
        public int SiteId
        {
            get
            {
                return _siteId;
            }
            set
            {
                if (!_siteId.Equals(value))
                {
                    _siteId = value;

                }
            }
        }
        public DateTime ScheduledDateFrom
        {
            get
            {
                return _scheduledDateFrom;
            }
            set
            {
                _scheduledDateFrom = value;
            }
        }
        public DateTime ScheduledDateTo
        {
            get
            {
                return _scheduledDateTo;
            }
            set
            {
                _scheduledDateTo = value;
            }
        }
        public int ScheduleForId
        {
            get
            {
                return _scheduleForId;
            }
            set
            {
                if (!_scheduleForId.Equals(value))
                {
                    _scheduleForId = value;

                }
            }
        }
        public int ProblemId
        {
            get
            {
                return _problemId;
            }
            set
            {
                if (!_problemId.Equals(value))
                {
                    _problemId = value;

                }
            }
        }
        public DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
            }
        }
        public int EnginerId
        {
            get
            {
                return _enginerId;
            }
            set
            {
                if (!_enginerId.Equals(value))
                {
                    _enginerId = value;

                }
            }
        }
        public bool IsExternal
        {
            get
            {
                return _isExternal;
            }
            set
            {

                _isExternal = value;


            }
        }
        public int UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                if (!_userId.Equals(value))
                {
                    _userId = value;

                }
            }
        }
        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
             
        }
        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
            }

        }
        public string FullName
        {
            get
            {
                if (_VendorName == null)
                    return _FirstName + "  " + _LastName;
                else
                   return _VendorName;
            }

        }
        public string VendorName
        {
            get
            {
                return _VendorName;
            }
            set
            {
                _VendorName = value;
            }

        }
        public string PreventiveScheduleStatus
        {
            get
            {
                return _PreventiveScheduleStatus;
            }
            set
            {

                _PreventiveScheduleStatus = value;


            }
        }
        public string CurativeScheduleStatus
        {
            get
            {
                return _CurativeScheduleStatus;
            }
            set
            {

                _CurativeScheduleStatus = value;


            }
        }
        public string Type
        {
            get
            {
                if (_problemId == 0)
                    return "Prevetive";
                else
                    return "Curative";
            }
           
        }

        public string PushedReason
        {
            get
            {
                return _pushedReason;
            }
            set
            {

                _pushedReason = value;


            }
        }
    }
}
