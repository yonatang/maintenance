﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{

    public class Problem : BaseEntity, IBaseEntity
    {
        private int _id = 0;
        private string _problemNumber = string.Empty;
        private string _description = string.Empty;
        private int _instrumentId = 0;
        private int _siteId = 0;
        private int _userId = 0;
        private DateTime _dateReported = DateTime.Now;
        private string _status = string.Empty;
        private int _problemtypeId = 0;
        private int _errorCodeId = 0;

        private string _problemTypeName = string.Empty;
        private string _errorCodeName = string.Empty;

        private string _instrumentName = string.Empty;
        private DateTime _installationDate = DateTime.Now;
        private string _serialNumber = string.Empty;
        private string _manufacturerName = string.Empty;
        private string _siteName = string.Empty;
        private DateTime _warantyExpireDate = DateTime.Now;


        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (!_id.Equals(value))
                {
                    _id = value;

                }
            }
        }

        public string ProblemNumber
        {
            get
            {
                return _problemNumber;
            }
            set
            {

                _problemNumber = value;


            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {

                _description = value;


            }
        }

        public int InstrumentId
        {
            get
            {
                return _instrumentId;
            }
            set
            {
                if (!_instrumentId.Equals(value))
                {
                    _instrumentId = value;

                }
            }
        }

        public int SiteId
        {
            get
            {
                return _siteId;
            }
            set
            {
                if (!_siteId.Equals(value))
                {
                    _siteId = value;

                }
            }
        }

        public int UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                if (!_userId.Equals(value))
                {
                    _userId = value;

                }
            }
        }

        public DateTime DateReported
        {
            get
            {
                return _dateReported;
            }
            set
            {
                _dateReported = value;
            }
        }

        public string Status
        {
            get
            {
                return _status;
            }
            set
            {

                _status = value;


            }
        }

        public int ProblemtypeId
        {
            get
            {
                return _problemtypeId;
            }
            set
            {
                if (!_problemtypeId.Equals(value))
                {
                    _problemtypeId = value;

                }
            }
        }

        public int ErrorCodeId
        {
            get
            {
                return _errorCodeId;
            }
            set
            {
                if (!_errorCodeId.Equals(value))
                {
                    _errorCodeId = value;

                }
            }
        }

        public string ProblemTypeName
        {
            get { return _problemTypeName; }
            set { _problemTypeName = value; }
        }
        public string ErrorCodeName
        {
            get { return _errorCodeName; }
            set { _errorCodeName = value; }
        }



        public string InstrumentName
        {
            get { return _instrumentName; }
            set { _instrumentName = value; }
        }
        public DateTime InstallationDate
        {
            get { return _installationDate; }
            set { _installationDate = value; }
        }
        public string SerialNo
        {
            get { return _serialNumber; }
            set { _serialNumber = value; }
        }
        //public string ManufacturerName
        //{
        //    get { return _manufacturerName; }
        //    set { _manufacturerName = value; }
        //}
        public string SiteName
        {
            get { return _siteName; }
            set { _siteName = value; }
        }
        public DateTime WarranityExpireDate
        {
            get { return _warantyExpireDate; }
            set { _warantyExpireDate = value; }
        }
        
    }
}
