﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{

    public class Confirmation : BaseEntity, IBaseEntity
    {

        private int _instrumentId = 0;
        private int _problemId = 0;
        private string _userComments = string.Empty;
        private bool _isResolved = false;
        private DateTime _Date = DateTime.Now;


        public int InstrumentId
        {
            get
            {
                return _instrumentId;
            }
            set
            {
                if (!_instrumentId.Equals(value))
                {
                    _instrumentId = value;

                }
            }
        }
        public int ProblemId
        {
            get
            {
                return _problemId;
            }
            set
            {
                if (!_problemId.Equals(value))
                {
                    _problemId = value;

                }
            }
        }
        public string UserComments
        {
            get
            {
                return _userComments;
            }
            set
            {

                _userComments = value;


            }
        }
        public bool IsResolved
        {
            get
            {
                return _isResolved;
            }
            set
            {

                _isResolved = value;


            }
        }
        public DateTime Date
        {
            get
            {
                return _Date;
            }
            set
            {
                _Date = value;
            }
        }
    }
}
