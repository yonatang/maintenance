﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{

    public class CurativeMaintenance : BaseEntity, IBaseEntity
    {
    
        private string _descriptionOfEqptFailure = string.Empty;
        private string _failureCode = string.Empty;
        private string _causeOfEqptFailure = string.Empty;
        private string _maintainedParts = string.Empty;
        private string _curativeActionTaken = string.Empty;
        private bool _performanceTestsDone = false;
        private bool _equipmentFullyFunctional = false;
        private string _engineerComments = string.Empty;
        private bool _followUpRequired = false;
        private DateTime _visitCompletionDate = DateTime.Now;
        private int _instrumentId = 0;
        private int _problemId = 0;
        private DateTime _date = DateTime.Now;
        private int _enginerId = 0;
        private int _scheduleid = 0;
        private string _sitename = string.Empty;
        private string _instrumentname = string.Empty;
        private string _problemType = string.Empty;
        private IList<Spareparts> _spares = new List<Spareparts>();
        public string SiteName
        {
            get { return _sitename; }
            set { _sitename = value; }
        }
        public string ProblemType
        {
            get { return _problemType; }
            set { _problemType = value; }
        }
        public string InstrumentName
        {
            get { return _instrumentname; }
            set { _instrumentname = value; }
        }
        public string DescriptionOfEqptFailure
        {
            get
            {
          
                return _descriptionOfEqptFailure;
            }
            set
            {
               
                if (value == null) value = string.Empty;
                if (!_descriptionOfEqptFailure.Equals(value))
                {
                    _descriptionOfEqptFailure = value;
    
                }
            }
        }

        public string FailureCode
        {
            get
            {
                
                return _failureCode;
            }
            set
            {
               
                if (value == null) value = string.Empty;
                if (!_failureCode.Equals(value))
                {
                    _failureCode = value;
                    
                }
            }
        }

        public string CauseOfEqptFailure
        {
            get
            {
             
                return _causeOfEqptFailure;
            }
            set
            {
                
                if (value == null) value = string.Empty;
                if (!_causeOfEqptFailure.Equals(value))
                {
                    _causeOfEqptFailure = value;
                   
                }
            }
        }

        public string MaintainedParts
        {
            get
            {
              
                return _maintainedParts;
            }
            set
            {
               
                if (value == null) value = string.Empty;
                if (!_maintainedParts.Equals(value))
                {
                    _maintainedParts = value;
                   
                }
            }
        }

        public string CurativeActionTaken
        {
            get
            {
               
                return _curativeActionTaken;
            }
            set
            {
              
                if (value == null) value = string.Empty;
                if (!_curativeActionTaken.Equals(value))
                {
                    _curativeActionTaken = value;
                    
                }
            }
        }

        public bool PerformanceTestsDone
        {
            get
            {
                
                return _performanceTestsDone;
            }
            set
            {
              
                if (!_performanceTestsDone.Equals(value))
                {
                    _performanceTestsDone = value;
                    
                }
            }
        }

        public bool EquipmentFullyFunctional
        {
            get
            {
               
                return _equipmentFullyFunctional;
            }
            set
            {
                
                if (!_equipmentFullyFunctional.Equals(value))
                {
                    _equipmentFullyFunctional = value;
                   
                }
            }
        }

        public string EngineerComments
        {
            get
            {
               
                return _engineerComments;
            }
            set
            {
                
                if (value == null) value = string.Empty;
                if (!_engineerComments.Equals(value))
                {
                    _engineerComments = value;
                   
                }
            }
        }

        public bool FollowUpRequired
        {
            get
            {
                
                return _followUpRequired;
            }
            set
            {
               
                if (!_followUpRequired.Equals(value))
                {
                    _followUpRequired = value;
                   
                }
            }
        }

        

        public DateTime VisitCompletionDate
        {
            get
            {

                return _visitCompletionDate;
            }
            set
            {
                
                if (!_visitCompletionDate.Equals(value))
                {
                    _visitCompletionDate = value;
                    
                }
            }
        }

        public int InstrumentId
        {
            get
            {
               
                return _instrumentId;
            }
            set
            {
                
                if (!_instrumentId.Equals(value))
                {
                    _instrumentId = value;
                  
                }
            }
        }
        public int Scheduleid
        {
            get
            {

                return _scheduleid;
            }
            set
            {

                if (!_scheduleid.Equals(value))
                {
                    _scheduleid = value;
                  
                }
            }
        }
        public int ProblemId
        {
            get
            {
                 
                return _problemId;
            }
            set
            {
               
                if (!_problemId.Equals(value))
                {
                    _problemId = value;
                    
                }
            }
        }

        

        public DateTime Date
        {
            get
            {
                 
                return _date;
            }
            set
            {
          
            
                if (!_date.Equals(value))
                {
                    _date = value;
        
                }
            }
        }

        public int EnginerId
        {
            get
            {
                
                return _enginerId;
            }
            set
            {
               
                if (!_enginerId.Equals(value))
                {
                    _enginerId = value;
                    
                }
            }
        }

        public IList<Spareparts> Sapres
        {
            get { return _spares; }
            set { _spares = value; }

        }
       

    }
}
