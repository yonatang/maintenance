﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{

    public class PreventiveMaintenance : BaseEntity, IBaseEntity
    {



        private int _ScheduleId = 0;
       
        private string _preventiveActionTaken = string.Empty;
        private bool _performanceTestsDone = false;
        private bool _equipmentFullyFunctional = false;
        private string _engineerComments = string.Empty;
        private bool _followUpRequired = false;
        private DateTime _NextVistScheduledDate = DateTime.Now;
        private int _instrumentId = 0;
       
        private DateTime _date = DateTime.Now;
        private int _enginerId = 0;
        private string _DailyIQC = string.Empty;
        private string _WeeklyIQC = string.Empty;
        private string _MonthlyIQC = string.Empty;
        private string _sitename = string.Empty;
        private string _instrumentname = string.Empty;

        private IList<Spareparts> _spares = new List<Spareparts>();
        public string SiteName
        {
            get { return _sitename; }
            set { _sitename = value; }
        }

        public string InstrumentName
        {
            get { return _instrumentname; }
            set { _instrumentname = value; }
        }
        public string DailyIQC
        {
            get
            {

                return _DailyIQC;
            }
            set
            {

                if (value == null) value = string.Empty;
                if (!_DailyIQC.Equals(value))
                {
                    _DailyIQC = value;

                }
            }
        }
        public string WeeklyIQC
        {
            get
            {

                return _WeeklyIQC;
            }
            set
            {

                if (value == null) value = string.Empty;
                if (!_WeeklyIQC.Equals(value))
                {
                    _WeeklyIQC = value;

                }
            }
        }
        public string MonthlyIQC
        {
            get
            {

                return _MonthlyIQC;
            }
            set
            {

                if (value == null) value = string.Empty;
                if (!_MonthlyIQC.Equals(value))
                {
                    _MonthlyIQC = value;

                }
            }
        }


        public string PreventiveActionTaken
        {
            get
            {

                return _preventiveActionTaken;
            }
            set
            {

                if (value == null) value = string.Empty;
                if (!_preventiveActionTaken.Equals(value))
                {
                    _preventiveActionTaken = value;

                }
            }
        }

        public bool PerformanceTestsDone
        {
            get
            {

                return _performanceTestsDone;
            }
            set
            {

                if (!_performanceTestsDone.Equals(value))
                {
                    _performanceTestsDone = value;

                }
            }
        }

        public bool EquipmentFullyFunctional
        {
            get
            {

                return _equipmentFullyFunctional;
            }
            set
            {

                if (!_equipmentFullyFunctional.Equals(value))
                {
                    _equipmentFullyFunctional = value;

                }
            }
        }

        public string EngineerComments
        {
            get
            {

                return _engineerComments;
            }
            set
            {

                if (value == null) value = string.Empty;
                if (!_engineerComments.Equals(value))
                {
                    _engineerComments = value;

                }
            }
        }

        public bool FollowUpRequired
        {
            get
            {

                return _followUpRequired;
            }
            set
            {

                if (!_followUpRequired.Equals(value))
                {
                    _followUpRequired = value;

                }
            }
        }



        public DateTime NextVistScheduledDate
        {
            get
            {

                return _NextVistScheduledDate;
            }
            set
            {

                if (!_NextVistScheduledDate.Equals(value))
                {
                    _NextVistScheduledDate = value;

                }
            }
        }

        public int InstrumentId
        {
            get
            {

                return _instrumentId;
            }
            set
            {

                if (!_instrumentId.Equals(value))
                {
                    _instrumentId = value;

                }
            }
        }

        public DateTime Date
        {
            get
            {

                return _date;
            }
            set
            {


                if (!_date.Equals(value))
                {
                    _date = value;

                }
            }
        }

        public int EnginerId
        {
            get
            {

                return _enginerId;
            }
            set
            {

                if (!_enginerId.Equals(value))
                {
                    _enginerId = value;

                }
            }
        }
        public int ScheduleId
        {
            get
            {

                return _ScheduleId;
            }
            set
            {

                if (!_ScheduleId.Equals(value))
                {
                    _ScheduleId = value;

                }
            }
        }
        public IList<Spareparts> Sapres
        {
            get { return _spares; }
            set { _spares = value; }

        }


    }
}
