﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{

    public class EscalateReason : BaseEntity, IBaseEntity
    {
        private int _instrumentId = 0;
        private int _scheduleId = 0;
        private int _problemId = 0;
        private int _enginerId = 0;
        private int _reasonId = 0;
        private DateTime _date = DateTime.Now;

        private string _siteName = string.Empty;
        private string _instrumentName = string.Empty;
        private string _problemNumber = string.Empty;
        public string InstrumentName
        {
            get
            {
                return _instrumentName;
            }
            set
            {
                _instrumentName = value;
            }
        }
        public string ProblemNumber
        {
            get
            {
                return _problemNumber;
            }
            set
            {
                _problemNumber = value;
            }
        }

        public string _Reason = string.Empty;
        public string Reason
        {
            get
            {
                return _Reason;
            }
            set
            {
                _Reason = value;
            }
        }
        public string SiteName
        {
            get
            {
                return _siteName;
            }
            set
            {
                _siteName = value;
            }
        }
        
        public int InstrumentId
        {
            get
            {
                return _instrumentId;
            }
            set
            {
                if (!_instrumentId.Equals(value))
                {
                    _instrumentId = value;

                }
            }
        }
        public int ScheduleId
        {
            get
            {
                return _scheduleId;
            }
            set
            {
                if (!_scheduleId.Equals(value))
                {
                    _scheduleId = value;

                }
            }
        }
        public int ProblemId
        {
            get
            {
                return _problemId;
            }
            set
            {
                if (!_problemId.Equals(value))
                {
                    _problemId = value;

                }
            }
        }
        public int EnginerId
        {
            get
            {
                return _enginerId;
            }
            set
            {
                if (!_enginerId.Equals(value))
                {
                    _enginerId = value;

                }
            }
        }
        public int ReasonId
        {
            get
            {
                return _reasonId;
            }
            set
            {

                if (!_reasonId.Equals(value))
                {
                    _reasonId = value;

                }


            }
        }

        public DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
            }
        }
    }
}
