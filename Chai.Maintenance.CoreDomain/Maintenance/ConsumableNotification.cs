﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{
    public class ConsumableNotification : BaseEntity, IBaseEntity
    {
        private int _consumableId = 0;
        private int _siteId = 0;
        private int _notifiedById = 0;
        private DateTime _date = DateTime.Now;
        private string _remark = string.Empty;
        private string _consumableName = string.Empty;
        private string _siteName = string.Empty;
        private string _notifiedBy = string.Empty;
        public int ConsumableId
        {
            get
            {
                return _consumableId;
            }
            set
            {
                if (!_consumableId.Equals(value))
                {
                    _consumableId = value;

                }
            }
        }
        public int SiteId
        {
            get
            {
                return _siteId;
            }
            set
            {
                if (!_siteId.Equals(value))
                {
                    _siteId = value;

                }
            }
        }
        public int NotifiedById
        {
            get
            {
                return _notifiedById;
            }
            set
            {
                if (!_notifiedById.Equals(value))
                {
                    _notifiedById = value;

                }
            }
        }
        public DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
            }
        }
        public string Remark
        {
            get
            {
                return _remark;
            }
            set
            {

                _remark = value;


            }
        }
        public string ConsumableName
        {
            get
            {
                return _consumableName;
            }
            set
            {

                _consumableName = value;


            }
        }
        public string SiteName
        {
            get
            {
                return _siteName;
            }
            set
            {

                _siteName = value;


            }
        }
        public string NotifiedBy
        {
            get
            {
                return _notifiedBy;
            }
            set
            {

                _notifiedBy = value;


            }
        }
    }
}
