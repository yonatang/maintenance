﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Maintenance
{

    public class PreventiveMaintenanceChecklist : BaseEntity, IBaseEntity
    {
       
        private int _taskId = 0;
        private bool _isCompleted = false;
        private string _remarks = string.Empty;
        private int _preventiveMaintenanceId = 0;
        private string _instrumentName = string.Empty;
        private string _taskName = string.Empty;


        public string TaskName
        {
            get { return _taskName; }
            set { _taskName = value; }
        }
        public string InstrumentName
        {
            get { return _instrumentName; }
            set { _instrumentName = value; }
        }
        public string Remarks
        {
            get
            {

                return _remarks;
            }
            set
            {

                if (value == null) value = string.Empty;
                if (!_remarks.Equals(value))
                {
                    _remarks = value;

                }
            }
        }
        public bool IsCompleted
        {
            get
            {

                return _isCompleted;
            }
            set
            {

                if (!_isCompleted.Equals(value))
                {
                    _isCompleted = value;

                }
            }
        }
       
        public int TaskId
        {
            get
            {

                return _taskId;
            }
            set
            {

                if (!_taskId.Equals(value))
                {
                    _taskId = value;

                }
            }
        }
        public int PreventiveMaintenanceId
        {
            get
            {

                return _preventiveMaintenanceId;
            }
            set
            {

                if (!_preventiveMaintenanceId.Equals(value))
                {
                    _preventiveMaintenanceId = value;

                }
            }
        }
    }
}
