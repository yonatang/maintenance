﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Configuration
{
   
    public class SparepartType :BaseEntity, IBaseEntity
    {
        #region Business Properties and Methods

        //declare members
        private int _id = 0;
        private string _name = string.Empty;
        private int _instrumentlookupId = 0;
        private string _partNumber = string.Empty;
        private int _uom = 0;
        private int _reorderQty = 0;
        private string _description = string.Empty;

        private string _uomString = string.Empty;
        private string _instrumentCategoryString = string.Empty;


        public SparepartType()
        { 
        
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_name.Equals(value))
                {
                    _name = value;
                   
                }
            }
        }

        public int InstrumentlookupId
        {
            get
            {
                return _instrumentlookupId;
            }
            set
            {
                if (!_instrumentlookupId.Equals(value))
                {
                    _instrumentlookupId = value;
                    
                }
            }
        }

        public string PartNumber
        {
            get
            {
                return _partNumber;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_partNumber.Equals(value))
                {
                    _partNumber = value;
                    
                }
            }
        }

        public int Uom
        {
            get
            {
                return _uom;
            }
            set
            {
               
                if (!_uom.Equals(value))
                {
                    _uom = value;
                   
                }
            }
        }

        public int ReorderQty
        {
            get
            {
                return _reorderQty;
            }
            set
            {
                if (!_reorderQty.Equals(value))
                {
                    _reorderQty = value;
                
                }
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_description.Equals(value))
                {
                    _description = value;
                 
                }
            }
        }
        public string UOMName
        {
            get
            {
                return _uomString;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_uomString.Equals(value))
                {
                    _uomString = value;

                }
            }
        }
        public string InstrumentName
        {
            get
            {
                return _instrumentCategoryString;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_instrumentCategoryString.Equals(value))
                {
                    _instrumentCategoryString = value;

                }
            }
        }
       
       
        #endregion //Business Properties and Methods

        
    }
}
