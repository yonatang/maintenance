﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Configuration
{
            
	public class Instrument :BaseEntity,IBaseEntity
	{
		

		
		private int _siteId = 0;
		private int _nameId = 0;
		private string _lotNumber = string.Empty;
		private string _serialNo = string.Empty;
        private DateTime _installationDate = DateTime.Now;
		private int _manufacturerId = 0;
		private int _vendorId = 0;
		private DateTime _warranityExpireDate = DateTime.Now;
		private bool _underServiceContract = false;
		private int _contractwithId = 0;
		private string _preventiveMaintenancePeriod = string.Empty;
		private bool _isFunctional = false;
		private int _installbyId = 0;
		private int _status = 0;
        private string _sitename = string.Empty;
        private string _instrumentname = string.Empty;
		private DateTime _lastPreventiveMaintenanceDate = DateTime.Now;
        
        private string _manufacturerName = string.Empty;
        private string _regionName = string.Empty;
	    private string _contractedWith = string.Empty;
		public int SiteId
		{
			get
			{
				return _siteId;
			}
			set
			{
				if (!_siteId.Equals(value))
				{
					_siteId = value;
					
				}
			}
		}
        
        public string ContractedWith
        {
            get { return _contractedWith; }
            set { _contractedWith = value; }
        }
        public string SiteName
        {
            get { return _sitename; }
            set { _sitename=value;}
        }
        public string InstrumentName
        {
            get { return _instrumentname; }
            set { _instrumentname = value; }
        }
        public string ManufacturerName
        {
            get { return _manufacturerName; }
            set { _manufacturerName = value; }
        }
        public string RegionName
        {
            get { return _regionName; }
            set { _regionName = value; }
        }
		public int nameId
		{
			get
			{
				return _nameId;
			}
			set
			{
				
					_nameId = value;
					
				
			}
		}

		public string LotNumber
		{
			get
			{
				return _lotNumber;
			}
			set
			{
				
					_lotNumber = value;
					
				
			}
		}

		public string SerialNo
		{
			get
			{
				return _serialNo;
			}
			set
			{
				
					_serialNo = value;
					
				
			}
		}

		public DateTime InstallationDate
		{
			get
			{
				return _installationDate.Date;
			}
            set
            {
                _installationDate = value;
            }
		}

		
		public int ManufacturerId
		{
			get
			{
				return _manufacturerId;
			}
			set
			{
				
					_manufacturerId = value;
					
				
			}
		}

		public int VendorId
		{
			get
			{
				return _vendorId;
			}
			set
			{
				
					_vendorId = value;
					
				
			}
		}

		public DateTime WarranityExpireDate
		{
			get
			{
				return _warranityExpireDate.Date;
			}
            set
            {
                _warranityExpireDate = value;
            }
		}

		

		public bool UnderServiceContract
		{
			get
			{
				return _underServiceContract;
			}
			set
			{
				
					_underServiceContract = value;

				
			}
		}

		public int ContractwithId
		{
			get
			{
				return _contractwithId;
			}
			set
			{
				
					_contractwithId = value;
					
				
			}
		}

		public string PreventiveMaintenancePeriod
		{
			get
			{
				return _preventiveMaintenancePeriod;
			}
			set
			{
				
					_preventiveMaintenancePeriod = value;
					
				
			}
		}

		public bool IsFunctional
		{
			get
			{
				return _isFunctional;
			}
			set
			{
				
					_isFunctional = value;
					
				
			}
		}

		public int InstallbyId
		{
			get
			{
				return _installbyId;
			}
			set
			{
				
					_installbyId = value;
					
				
			}
		}

		public int Status
		{
			get
			{
				return _status;
			}
			set
			{
				
					_status = value;
					
				
			}
		}

		public DateTime LastPreventiveMaintenanceDate
		{
			get
			{
				return _lastPreventiveMaintenanceDate.Date;
			}
            set
            {
                _lastPreventiveMaintenanceDate = value;
            }
		}

		
        }
    }
		

		

		

		

		