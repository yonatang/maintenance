﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Configuration
{
        
	public class Site : BaseEntity, IBaseEntity
	{
		#region Business Properties and Methods

		//declare members
		
		private int _regionId = 0;
		private string _name = string.Empty;
		private string _code = string.Empty;
		private string _city = string.Empty;
		private int _typeId = 0;
		private string _telephone1 = string.Empty;
		private string _telephone2 =  string.Empty;

		private string _address = string.Empty;
		private string _email = string.Empty;
		private int _labtechnicianuserId = 0;
		private bool _status = false;

		
		public int RegionId
		{
			get
			{
				return _regionId;
			}
			set
			{
				
					_regionId = value;
					
				
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				
					_name = value;
					
				
			}
		}

		public string Code
		{
			get
			{
				return _code;
			}
			set
			{
				
					_code = value;
					
				
			}
		}

		public string City
		{
			get
			{
				return _city;
			}
			set
			{
				
					_city = value;
					
				
			}
		}

		public int typeId
		{
			get
			{
				return _typeId;
			}
			set
			{
				
					_typeId = value;
					
				
			}
		}

		public string Telephone1
		{
			get
			{
				return _telephone1;
			}
			set
			{
				
					_telephone1 = value;
					
				
			}
		}

		public string Telephone2
		{
			get
			{
				return _telephone2;
			}
			set
			{
				
					_telephone2 = value;
					
				
			}
		}

		public string Address
		{
			get
			{
				return _address;
			}
			set
			{
				
					_address = value;
					
				
			}
		}

		public string Email
		{
			get
			{
				return _email;
			}
			set
			{
				
					_email = value;
					
				
			}
		}

		public int labtechnicianuserId
		{
			get
			{
				return _labtechnicianuserId;
			}
			set
			{
				
					_labtechnicianuserId = value;
				
			}
		}

		public bool Status
		{
			get
			{
				return _status;
			}
			set
			{
				
					_status = value;
					
				
			}
		}
 
		

		#endregion //Business Properties and Methods
        public Site()
        { }
    }
}
