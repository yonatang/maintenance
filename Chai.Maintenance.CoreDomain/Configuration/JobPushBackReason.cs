﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Configuration
{
    public class JobPushBackReason : BaseEntity, IBaseEntity
    {

    
 
        private string _reason = string.Empty;





        public string Reason
        {
            get
            {
                return _reason;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_reason.Equals(value))
                {
                    _reason = value;

                }
            }
        }

        
    }
}
