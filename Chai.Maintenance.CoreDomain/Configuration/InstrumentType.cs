﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Configuration
{
   public class InstrumentType:BaseEntity,IBaseEntity
    {

        #region Business Properties and Methods

        //declare members

        private int _InstrumentCatagoryId = 0;
        private string _Instrumentname = string.Empty;
        private byte[] _instrumentservice ;
        private string _Name = string.Empty;
        private string _filename = string.Empty;
        private string _contenttype = string.Empty;
        private int _filesize = 0;
        public int InstrumentCatagoryId
        {
            get
            {
                return _InstrumentCatagoryId;
            }
            set
            {
                if (!_InstrumentCatagoryId.Equals(value))
                {
                    _InstrumentCatagoryId = value;

                }
            }
        }
        public byte[] InstrumentService
        {
            get
            {
                return _instrumentservice;
            }
            set
            {


                _instrumentservice = value;

            }
        }

        public string InstrumentName
        {
            get
            {
                return _Instrumentname;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_Instrumentname.Equals(value))
                {
                    _Instrumentname = value;

                }
            }
        }
        public string FileName
        {
            get
            {
                return _filename;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_filename.Equals(value))
                {
                    _filename = value;

                }
            }
        }
        public string ContentType
        {
            get
            {
                return _contenttype;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_contenttype.Equals(value))
                {
                    _contenttype = value;

                }
            }
        }
        public int FileSize
        {
            get
            {
                return _filesize;
            }
            set
            {
                if (!_filesize.Equals(value))
                {
                    _filesize = value;

                }
            }
        }
        public string Name
        {
            get
            {
                return _Name;
            }
        set
        {
            _Name = value;
        }
           
             
        }


        #endregion //Business Properties and Methods
    }
}
