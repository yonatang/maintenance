﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Configuration
{
    public class Region :BaseEntity, IBaseEntity
    {
        #region Member Variables
        protected string _regionname;
        protected string _regioncode;
        protected string _mapid;
        #endregion
        #region Constructor
        public Region()
        { }
        public Region(string regionname,string regioncode)
        {
            _regionname = regionname;
            _regioncode = regioncode;
        
        }
        #endregion
        #region Public Properties

        public string RegionName
        {
            get { return _regionname; }
            set
            {
                if (value != null && value.Length > 32)
                    throw new ArgumentOutOfRangeException("Invalid value for Name", value, value.ToString());
                _regionname = value;
            }
        }
        public string RegionCode
        {
            get { return _mapid; }
            set
            {
                if (value != null && value.Length > 32)
                    throw new ArgumentOutOfRangeException("Invalid value for Name", value, value.ToString());
                _mapid = value;
            }
        }
        public string Mapid
        {
            get { return _mapid; }
            set
            {
                if (value != null && value.Length > 32)
                    throw new ArgumentOutOfRangeException("Invalid value for Name", value, value.ToString());
                _mapid = value;
            }
        
        }
        #endregion
    }
}
