﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.CoreDomain.Configuration
{
    public class AutoNumber : BaseEntity, IBaseEntity
    {

        private int _id = 0;
        private int _pageID = 0;
        private string _prefix = string.Empty;
        private int _sequence = 0;
        private string _status = string.Empty;
        private string _PageString = string.Empty;
        
        public string PageString
        {
            get
            {
                if (_pageID == (int)AspxPageType.ProblemNotification)
                    return "Problem Notification";
                else
                    return "";
            }

        }
        public int PageId
        {
            get
            {
              
                return _pageID;
            }
            set
            {
                if (!_pageID.Equals(value))
                {
                    _pageID = value;
                 
                }
            }
        }

        public string Prefix
        {
            get
            {      
                //string[] PrefixChar;
                //PrefixChar = _prefix.Split('/');
                //_prefix = PrefixChar[0];
                return _prefix;
            }
            set
            {
                if (!_prefix.Equals(value))
                {
                    _prefix = value;
                }
            }
        }

        public int Sequence
        {
            get
            {
                return _sequence;
            }
            set
            {
                if (!_sequence.Equals(value))
                {
                    _sequence = value;
                }
            }
        }

        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                if (!_status.Equals(value))
                {
                    _status = value;
                }
            }
        }

    }
}
