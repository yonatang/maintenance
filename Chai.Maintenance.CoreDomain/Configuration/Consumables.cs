﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Configuration
{
    public class Consumables : BaseEntity, IBaseEntity
    {
         #region Member Variables
        private string _name = string.Empty;
        private string _description = string.Empty;
        #endregion
        #region Constructor
        public Consumables()
        { }
       
        #endregion
        #region Public Properties

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_name.Equals(value))
                {
                    _name = value;

                }
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_description.Equals(value))
                {
                    _description = value;

                }
            }
        }


        #endregion
    }
}
