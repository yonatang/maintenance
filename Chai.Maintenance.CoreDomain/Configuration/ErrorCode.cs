﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Configuration
{
    public class ErrorCode : BaseEntity, IBaseEntity
    {
        #region Member Variables
        private string _name = string.Empty;
        private int _problemtypeId = 0;
        private string _problemTypeName = string.Empty;
        #endregion
        #region Constructor
        public ErrorCode()
        { }
         
        #endregion
        #region Public Properties

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_name.Equals(value))
                {
                    _name = value;

                }
            }
        }

        public int ProblemTypeId
        {
            get
            {
                return _problemtypeId;
            }
            set
            {
                if (!_problemtypeId.Equals(value))
                {
                    _problemtypeId = value;

                }
            }
        }
        public string ProblemTypeName
        {
            get{return _problemTypeName; }
            set{_problemTypeName = value;}
        }

        #endregion
    }
}
