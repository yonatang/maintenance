﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Configuration
{
    public class Manufacturer : BaseEntity, IBaseEntity
    {
        #region Member Variables
        protected string _name;
 
        #endregion
        #region Constructor
        public Manufacturer()
        { }
        public Manufacturer(string name)
        {
            _name = name;
 

        }
        #endregion
        #region Public Properties

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 32)
                    throw new ArgumentOutOfRangeException("Invalid value for Name", value, value.ToString());
                _name = value;
            }
        }
     

        #endregion
    }
}