﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Configuration
{
    public class UnitOfMeasurment :BaseEntity, IBaseEntity
    {
        #region Member Variables
        protected string _name;
        protected string _description;
        #endregion
        #region Constructor
        public UnitOfMeasurment()
        { }
        public UnitOfMeasurment(string name, string description)
        {
            _name = name;
            _description = description;
        
        }
        #endregion
        #region Public Properties

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 32)
                    throw new ArgumentOutOfRangeException("Invalid value for Name", value, value.ToString());
                _name = value;
            }
        }
        public string Description
        {
            get { return _description; }
            set
            {
                //if (value != null && value.Length > 32)
                //    throw new ArgumentOutOfRangeException("Invalid value for Description", value, value.ToString());
                _description = value;
            }
        }

        #endregion
    }
}