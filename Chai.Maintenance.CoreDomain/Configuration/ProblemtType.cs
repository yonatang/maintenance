﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Configuration
{
    public class ProblemType : BaseEntity, IBaseEntity
    {
        #region Member Variables
        private string _name = string.Empty;
 
        #endregion
        #region Constructor
        public ProblemType()
        { }
       
        #endregion
        #region Public Properties

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_name.Equals(value))
                {
                    _name = value;

                }
            }
        }

        


        #endregion
    }
}
