﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.Configuration
{
    public class ChecklistTask : BaseEntity, IBaseEntity
    {
        #region Member Variables
        private string _name = string.Empty;
        private int _instrumentLookupId = 0;
        private string _instrumentName = string.Empty;
        #endregion
        #region Constructor
        public ChecklistTask()
        { }
         
        #endregion
        #region Public Properties
        public int InstrumentLookupId
        {
            get
            {

                return _instrumentLookupId;
            }
            set
            {

                if (!_instrumentLookupId.Equals(value))
                {
                    _instrumentLookupId = value;

                }
            }
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value == null) value = string.Empty;
                if (!_name.Equals(value))
                {
                    _name = value;

                }
            }
        }

        public string InstrumentName
        {
            get
            {
                return _instrumentName;
            }
            set
            {
              
                    _instrumentName = value;
 
            }
        }

        #endregion
    }
}
