﻿using System;
using System.Collections.Generic;
//using Chai.Maintenance.CoreDomain.Accounting;

namespace Chai.Maintenance.CoreDomain
{
    public class BaseEntity :IBaseEntity
    {
        private int _id = -1;
        private Guid _guid;
        private bool _isDirty;

        public BaseEntity()
        {
            this._isDirty = false;
        }

        public BaseEntity(Guid guid)
            : this()
        {
            this._guid = guid;
        }

        #region IBaseEntity Members

        public virtual Type GetEntityType()
        {
            throw new NotImplementedException();
        }

        public int Id
        {
            get { return this._id; }
            set { this._id = value; }
        }

        public bool IsNew()
        {
            return this._id <= 0;
        }

        public bool IsDirty
        {
            get { return this._isDirty; }
            set { this._isDirty = value; }
        }

        public System.Guid Guid
        {
            get { return this._guid; }
            set { this._guid = value; }
        }

        #endregion
    }
}
