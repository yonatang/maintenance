
using System;
using System.Collections.Generic;
using System.Security.Principal;
using Chai.Maintenance.Enums;

namespace Chai.Maintenance.CoreDomain
{
    public class Node : BaseEntity, IBaseEntity
    {
        #region Member Variables

        private PageViewType _viewType;
        private string _title;
        protected string _folderPath;
        protected string _imagePath;
        protected string _description;
        protected IList<NodePermission> _nodePermissions;
    
        #endregion

        #region Constructors

        public Node() { }

        public Node(PageViewType viewType, string title, string folderPath, string imagePath, string description)
        {
            this._viewType = viewType;
            this._title = title;
            this._folderPath = folderPath;
            this._imagePath = imagePath;
            this._description = description;
        }

        #endregion

        #region Public Properties

        public AspxPageType PageType
        {
            get { return (AspxPageType)Enum.ToObject(typeof(AspxPageType), Id); }
        }

        public PageViewType ViewType
        {
            get { return _viewType; }
            set { _viewType = value; }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                if (value != null && value.Length > 64)
                    throw new ArgumentOutOfRangeException("Invalid value for Title", value, value.ToString());
                _title = value;
            }
        }

        public string FolderPath
        {
            get { return _folderPath; }
            set
            {
                if (value != null && value.Length > 255)
                    throw new ArgumentOutOfRangeException("Invalid value for FolderPath", value, value.ToString());
                _folderPath = value;
            }
        }

        public string NodeUrl
        {
            get
            {
                string result = String.Format("~/{0}/{1}.aspx", FolderPath, PageType);

                string page = PageType.ToString();
                string[] p = page.Split('_');
                if (p.Length >=2)
                { 
                    page = p[0];

                    result = String.Format("~/{0}/{1}.aspx?ReportName={2}", FolderPath, page, p[1]);
                   
                }
                

                return result;
            }
        }

        public string ImagePath
        {
            get { return _imagePath; }
            set
            {
                if (value != null && value.Length > 255)
                    throw new ArgumentOutOfRangeException("Invalid value for ImagePath", value, value.ToString());
                _imagePath = value;
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (value != null && value.Length > 128)
                    throw new ArgumentOutOfRangeException("Invalid value for Description", value, value.ToString());
                _description = value;
            }
        }

        public IList<NodePermission> NodePermissions
        {
            get
            {
                if (_nodePermissions == null)
                {
                    _nodePermissions = new List<NodePermission>();
                }
                return _nodePermissions;
            }
            set { _nodePermissions = value; }
        }

        #endregion

        public virtual bool AnonymousViewAllowed
        {
            get
            {
                foreach (NodePermission np in NodePermissions)
                {
                    if (Array.IndexOf(np.Role.Permissions, AccessLevel.Anonymous) > -1)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public bool ViewAllowed(IIdentity user)
        {
            User u = user as User;
            if (this.AnonymousViewAllowed)
            {
                return true;
            }
            else if (u != null)
            {
                return u.CanView(this);
            }
            else
            {
                return false;
            }
        }

        public virtual bool ViewAllowed(Role role)
        {
            foreach (NodePermission np in this.NodePermissions)
            {
                if (np.Role.Id == role.Id && np.ViewAllowed)
                {
                    return true;
                }
            }
            return false;
        }

        public virtual bool EditAllowed(Role role)
        {
            foreach (NodePermission np in this.NodePermissions)
            {
                if (np.Role.Id == role.Id && np.EditAllowed)
                {
                    return true;
                }
            }
            return false;
        }
   }
}
