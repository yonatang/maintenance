﻿using System;
using System.Collections.Generic;

using Chai.Maintenance.Enums;

namespace Chai.Maintenance.CoreDomain
{
    public class MenuTab
    {
        private TabType _tabtype;

        private IList<TabNode> _actionSectionNodes;
        private IList<TabNode> _findSectionNodes;
        private IList<TabNode> _dropdownSectionNodes;

        private MenuTab()
        {
        }

        public MenuTab(int tabid)
            : this((TabType)Enum.ToObject(typeof(TabType), tabid))
        {
        }

        public MenuTab(TabType tab)
        {
            this._tabtype = tab;
            this._actionSectionNodes = new List<TabNode>();
            this._findSectionNodes = new List<TabNode>();
            this._dropdownSectionNodes = new List<TabNode>();
        }
        
        public TabType Tab
        {
            get { return _tabtype; }
        }

        public string TabName
        {
            get
            {
                return _tabtype.ToString().Replace('_', ' ');
            }
        }
        
        public IList<string> LeftMenuSections()
        {
            IList<string> list = new List<string>();
            if (_tabtype != TabType.Report)
            {
                list.Add(MenuSectionType.Action.ToString());
                list.Add(MenuSectionType.Find.ToString());
            }
            else
            {
                list.Add(MenuSectionType.Action.ToString());
                list.Add(MenuSectionType.Find.ToString());
            }
            return list;
        }

        public IList<TabNode> ActionSectionNodes
        {
            get { return _actionSectionNodes; }
            set { _actionSectionNodes = value; }
        }
        
        public IList<TabNode> FindSectionNodes
        {
            get { return _findSectionNodes; }
            set { _findSectionNodes = value; }
        }

        public IList<TabNode> DropdownSectionNodes
        {
            get { return _dropdownSectionNodes; }
            set { _dropdownSectionNodes = value; }
        }

        public bool NodeWasAddedToActionsection(int nodeid)
        {
            foreach (TabNode tn in ActionSectionNodes)
            {
                if (tn.Node.Id == nodeid)
                    return true;
            }
            return false;
        }

        public bool NodeWasAddedToFindsection(int nodeid)
        {
            foreach (TabNode tn in FindSectionNodes)
            {
                if (tn.Node.Id == nodeid)
                    return true;
            }
            return false;
        }

        public bool NodeWasAddedToDropdownsection(int nodeid)
        {
            foreach (TabNode tn in DropdownSectionNodes)
            {
                if (tn.Node.Id == nodeid)
                    return true;
            }
            return false;
        }

        public TabNode GetTabNodeFromAction(int id)
        {
            TabNode tnode = null;
            foreach (TabNode tn in ActionSectionNodes)
            {
                if (tn.Id == id)
                    tnode = tn;
            }
            return tnode;
        }

        public TabNode GetTabNodeFromFind(int id)
        {
            TabNode tnode = null;
            foreach (TabNode tn in FindSectionNodes)
            {
                if (tn.Id == id)
                    tnode = tn;
            }
            return tnode;
        }

        public TabNode GetTabNodeFromDropdown(int id)
        {
            TabNode tnode = null;
            foreach (TabNode tn in DropdownSectionNodes)
            {
                if (tn.Id == id)
                    tnode = tn;
            }
            return tnode;
        }
    }
}
