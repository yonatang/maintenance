
using System;
using System.Collections;
using Chai.Maintenance.Enums;

namespace Chai.Maintenance.CoreDomain
{
    public class TabNode: BaseEntity, IBaseEntity
    {
        #region Member Variables

        protected TabType _tabType;
        protected MenuSectionType _section;
        protected int _position;
        protected Node _node;
        private int _nodeid;
        #endregion

        #region Constructors

        public TabNode() { }

        public TabNode(TabType tabtype, MenuSectionType section, int position, Node node)
        {
            this._tabType = tabtype;
            this._section = section;
            this._position = position;
            this._node = node;
        }

        #endregion

        #region Public Properties

        public TabType Tab
        {
            get { return _tabType; }
            set { _tabType = value; }
        }
        public int TabId
        {
            get
            {
                return (int)_tabType;
            }
        }
        public MenuSectionType Section
        {
            get { return _section; }
            set
            {
                _section = value;
            }
        }

        public int Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public int NodeId
        {
            get { return _nodeid; }
            set { _nodeid = value; }
        }
        public Node Node
        {
            get { return _node; }
            set { _node = value; }
        }
        #endregion

    }
}
