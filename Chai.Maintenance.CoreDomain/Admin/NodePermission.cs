
using System;

namespace Chai.Maintenance.CoreDomain
{ 
	public class NodePermission : BaseEntity, IBaseEntity
	{
		
		private int _nodeId;
		private bool _viewAllowed = false;
		private bool _editAllowed = false;
        private Role _role = null;

        public NodePermission()
        {
        }

		public int NodeId
		{
			get
			{
				return _nodeId;
			}
            set
            {
                _nodeId = value;
            }
        }

        public Role Role
        {
            get 
            {
                return _role; 
            }
            set 
            {
                _role = value;
            }
        }

		public bool ViewAllowed
		{
			get
			{
				return _viewAllowed;
			}
            set
            {
                _viewAllowed = value;
            }
		}

		public bool EditAllowed
		{
			get
			{
				return _editAllowed;
			}
            set
            {
                _editAllowed = value;
            }
        }

     
	}
}
