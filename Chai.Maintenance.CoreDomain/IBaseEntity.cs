﻿using System;

namespace Chai.Maintenance.CoreDomain
{
    public interface IBaseEntity
    {
        //virtual Type GetEntityType();
        int Id { get; set; }
        bool IsNew();
        bool IsDirty { get; set; }
        System.Guid Guid { get; set; }
 
    }
}
