﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.SparepartInventory
{
    public class RequestItemDetail : BaseEntity,IBaseEntity
    {
        #region Business Properties and Methods

        //declare members
      
        private int _instrumentId = 0;
        private int _requestId = 0;
        private int _sparepartId = 0;
        private int _qty = 0;
        private int _problemId = 0;
        private string _InstrumentName = string.Empty;
        private string _SparepartName = string.Empty;
        private string _problemnumber = string.Empty;
       

        public int InstrumentId
        {
            get
            {
               
                return _instrumentId;
            }
            set
            {
              
                if (!_instrumentId.Equals(value))
                {
                    _instrumentId = value;
                 
                }
            }
        }

        public int RequestId
        {
            get
            {
               
                return _requestId;
            }
            set
            {
             
                if (!_requestId.Equals(value))
                {
                    _requestId = value;
                
                }
            }
        }

        public int SparepartId
        {
            get
            {
               
                return _sparepartId;
            }
            set
            {
              
                if (!_sparepartId.Equals(value))
                {
                    _sparepartId = value;
                 
                }
            }
        }

        public int Qty
        {
            get
            {
              
                return _qty;
            }
            set
            {

                if (!_qty.Equals(value))
                {
                    _qty = value;
                }
                 
            }
        }

        public int ProblemId
        {
            get
            {
             
                return _problemId;
            }
            set
            {
            
                if (!_problemId.Equals(value))
                {
                    _problemId = value;
                 
                }
            }
        }
        public string ProblemNumber
        {
            get
            {

                return _problemnumber;
            }
            set
            {

                
                    _problemnumber = value;

               
            }
        }
        public string InstrumentName
        {
            get
            {

                return _InstrumentName;
            }
            set
            {


                _InstrumentName = value;


            }
        }
        public string SparepartName
        {
            get
            {

                return _SparepartName;
            }
            set
            {


                _SparepartName = value;


            }
        }
        #endregion //Business Properties and Methods
    }
}
