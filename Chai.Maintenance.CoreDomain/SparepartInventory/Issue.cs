﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.SparepartInventory
{
   public class Issue : BaseEntity,IBaseEntity
    {
        #region Business Properties and Methods

        //declare members
      
        private string _issueNo = string.Empty;
        private DateTime _issueDate = DateTime.Now;
        private int _approvalId = 0;
        private int _toolkeeperId = 0;
        private int _issuedFor = 0;
        private int _regionId = 0;
        private string _toolkeepername = string.Empty;
        private string _issuedforname = string.Empty;
        private string _regionName = string.Empty;
        private int _approver = 0;
        private bool _isposted = false;
        private IList<IssueItemDetail> _itemdetail = new List<IssueItemDetail>();
        public string IssueNo
        {
            get
            {
             
                return _issueNo;
            }
            set
            {
             
                if (value == null) value = string.Empty;
                if (!_issueNo.Equals(value))
                {
                    _issueNo = value;
                
                }
            }
        }

        public DateTime IssueDate
        {
            get
            {
             
                return _issueDate.Date;
            }
            set
            {
                _issueDate = value;
            }
        }
        public bool IsPosted
        {
            get { return _isposted; }
            set { _isposted = value; }
        }
        

        public int ApprovalId
        {
            get
            {
              
                return _approvalId;
            }
            set
            {
               
                if (!_approvalId.Equals(value))
                {
                    _approvalId = value;
                
                }
            }
        }
        public int Approver
        {
            get
            {

                return _approver;
            }
            set
            {

                if (!_approver.Equals(value))
                {
                    _approver = value;

                }
            }
        }

        public int ToolkeeperId
        {
            get
            {
            
                return _toolkeeperId;
            }
            set
            {
            
                if (!_toolkeeperId.Equals(value))
                {
                    _toolkeeperId = value;
                  
                }
            }
        }

        public int IssuedFor
        {
            get
            {
              
                return _issuedFor;
            }
            set
            {
              
                if (!_issuedFor.Equals(value))
                {
                    _issuedFor = value;
                 
                }
            }
        }

        public int RegionId
        {
            get
            {
               
                return _regionId;
            }
            set
            {
               
                if (!_regionId.Equals(value))
                {
                    _regionId = value;
               
                }
            }
        }
        public string ToolKeeperName
        {
            get { return _toolkeepername; }
            set { _toolkeepername = value; } 
          
        }
        public string issuedforName
        {
            get { return _issuedforname; }
            set { _issuedforname = value; }
        }
        public string RegionName
        {
            get { return _regionName; }
            set { _regionName = value; }
        }

        public IList<IssueItemDetail> ItemDetail
        {
            get {
                return _itemdetail;
            }
            set { _itemdetail = value; }
        }
        #endregion //Business Properties and Methods
    }
}
