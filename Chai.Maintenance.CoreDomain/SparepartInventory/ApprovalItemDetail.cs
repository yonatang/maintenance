﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.SparepartInventory
{
    public class ApprovalItemDetail :BaseEntity,IBaseEntity
    {
        #region Business Properties and Methods

        //declare members
       
        private int _approvalId = 0;
        private int _insrumentId = 0;
        private int _sparepartId = 0;
        private int _requestedQty = 0;
        private int _approvedQty = 0;
        private string _instrumentName = string.Empty;
        private string _sparepartName = string.Empty;
        private int _Stockqty = 0;
        public int ApprovalId
        {
            get
            {
               
                return _approvalId;
            }
            set
            {
               
                if (!_approvalId.Equals(value))
                {
                    _approvalId = value;
                 
                }
            }
        }

        public int InsrumentId
        {
            get
            {
              
                return _insrumentId;
            }
            set
            {
               
                if (!_insrumentId.Equals(value))
                {
                    _insrumentId = value;
                                }
            }
        }

        public int SparepartId
        {
            get
            {
              
                return _sparepartId;
            }
            set
            {
            
                if (!_sparepartId.Equals(value))
                {
                    _sparepartId = value;
             
                }
            }
        }

        public int RequestedQty
        {
            get
            {
               
                return _requestedQty;
            }
            set
            {
               
                if (!_requestedQty.Equals(value))
                {
                    _requestedQty = value;
                 
                }
            }
        }

        public int ApprovedQty
        {
            get
            {
               
                return _approvedQty;
            }
            set
            {
              
                if (!_approvedQty.Equals(value))
                {
                    _approvedQty = value;
                
                }
            }
        }
        public int StockQty
        {
            get
            {

                return _Stockqty;

            }
            set { _Stockqty = value; }
        }
        public string InstrumentName
        {
            get { return _instrumentName; }
            set { _instrumentName = value; }
           
        }
        public string SparepartName
        {
            get { return _sparepartName; }
            set { _sparepartName = value; }

        }

        #endregion //Business Properties and Methods
    }
}
