﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.SparepartInventory
{
  
    public class RecieveItemDetail :BaseEntity,IBaseEntity
    {
        #region Business Properties and Methods

        //declare members
        private int _id = 0;
        private int _receiveId = 0;
        private int _manfacturerId = 0;
        private int _sparepartId = 0;
        private int _qty = 0;
        private decimal _unitPrice = 0;
        private decimal _totalPrice = 0;
        private int _instrumentid = 0;
        private string _manufacturer = string.Empty;
        private string _sparepartname = string.Empty;
        private string _instrumentname = string.Empty;

        public int ReceiveId
        {
            get
            {
               
                return _receiveId;
            }
            set
            {
              
                if (!_receiveId.Equals(value))
                {
                    _receiveId = value;
                  
                }
            }
        }

        public int ManfacturerId
        {
            get
            {
               
                return _manfacturerId;
            }
            set
            {
                
                if (!_manfacturerId.Equals(value))
                {
                    _manfacturerId = value;
                   
                }
            }
        }
        public int InstrumentId
        {
            get
            {

                return _instrumentid;
            }
            set
            {

                if (!_instrumentid.Equals(value))
                {
                    _instrumentid = value;

                }
            }
        }
        public int SparepartId
        {
            get
            {
               
                return _sparepartId;
            }
            set
            {
               
                if (!_sparepartId.Equals(value))
                {
                    _sparepartId = value;
                  
                }
            }
        }

        public int Qty
        {
            get
            {
            
                return _qty;
            }
            set
            {
              
                if (!_qty.Equals(value))
                {
                    _qty = value;
                  
                }
            }
        }

        public decimal UnitPrice
        {
            get
            {
               
                return _unitPrice;
            }
            set
            {
             
                if (!_unitPrice.Equals(value))
                {
                    _unitPrice = value;
               
                }
            }
        }

        public decimal TotalPrice
        {
            get
            {
              
                return _totalPrice;
            }
            set
            {
              
                if (!_totalPrice.Equals(value))
                {
                    _totalPrice = value;
                  
                }
            }
        }
        public string InstrumentName
        {
            get {
                return _instrumentname;
            }
            set { 
                _instrumentname = value;
            }
        }

        public string SparepartName
        {
            get {
                return _sparepartname;
            
            }
            set {
                _sparepartname = value;
            }
        }
        public string Manufacturer
        {
            get {
                return _manufacturer;
            }
            set {
                _manufacturer = value;
            }
        
        }
        #endregion //Business Properties and Methods

    }
}
