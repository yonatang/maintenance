﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.SparepartInventory
{
    public class StockQty:BaseEntity,IBaseEntity
    {
        #region Business Properties and Methods

        //declare members
        
        private int _regionId = 0;
        private int _instrumentId = 0;
        private int _sparepartId = 0;
        private int _qty = 0;
        private int _reorderqty = 0;
        private string _sparepartname = string.Empty;
        private string _instrumentname = string.Empty;
        public int RegionId
        {
            get
            {
               
                return _regionId;
            }
            set
            {
               
                if (!_regionId.Equals(value))
                {
                    _regionId = value;
                   
                }
            }
        }

        public int SparepartId
        {
            get
            {
                
                return _sparepartId;
            }
            set
            {
                
                if (!_sparepartId.Equals(value))
                {
                    _sparepartId = value;
                   
                }
            }
        }
        public int InstrumentId
        {
            get
            {

                return _instrumentId;
            }
            set
            {

                if (!_instrumentId.Equals(value))
                {
                    _instrumentId = value;

                }
            }
        }
        public int Qty
        {
            get
            {
              
                return _qty;
            }
            set
            {
                
                if (!_qty.Equals(value))
                {
                    _qty = value;
                
                }
            }
        }
        public int ReorderQty
        {
            get
            {

                return _reorderqty;
            }
            set
            {

                if (!_reorderqty.Equals(value))
                {
                    _reorderqty = value;

                }
            }
        }
        public string SparepartName
        {
            get { return _sparepartname; }
            set { _sparepartname = value; }

        
        }
        public string InstrumentName
        {
            get
            {
                return _instrumentname;
            }
            set
            {
                _instrumentname = value;
            }
        }


        #endregion //Business Properties and Methods
    }
}
