﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.SparepartInventory
{
    public class IssueItemDetail : BaseEntity,IBaseEntity
    {
        #region Business Properties and Methods
        
        //declare members
       
        private int _issueId = 0;
        private int _instrumentId = 0;
        private int _sparepartId = 0;
        private int _approvedQty = 0;
        private int _issuedQty = 0;
        private string _instrumentname = string.Empty;
        private string _sparepartname = string.Empty;
        private int _Stockqty = 0;

        public int IssueId
        {
            get
            {
               
                return _issueId;
            }
            set
            {
               
                if (!_issueId.Equals(value))
                {
                    _issueId = value;
                 
                }
            }
        }

        public int InstrumentId
        {
            get
            {
             
                return _instrumentId;
            }
            set
            {
               
                if (!_instrumentId.Equals(value))
                {
                    _instrumentId = value;
                  
                }
            }
        }

        public int SparepartId
        {
            get
            {
               
                return _sparepartId;
            }
            set
            {
             
                if (!_sparepartId.Equals(value))
                {
                    _sparepartId = value;
                 
                }
            }
        }

        public int ApprovedQty
        {
            get
            {
              
                return _approvedQty;
            }
            set
            {
              
                if (!_approvedQty.Equals(value))
                {
                    _approvedQty = value;
                   
                }
            }
        }

        public int IssuedQty
        {
            get
            {
              
                return _issuedQty;
            }
            set
            {
               
                if (!_issuedQty.Equals(value))
                {
                    _issuedQty = value;
                   
                }
            }
        }
        public int StockQty
        {
            get {

                return _Stockqty;

            }
            set { _Stockqty = value; }
        }
        public string InstrumentName
        {
            get { return _instrumentname; }
            set { _instrumentname = value; }

        }
        public string SparepartName
        {
            get { return _sparepartname; }
            set { _sparepartname = value; }

        }
     

        #endregion //Business Properties and Methods
    }
}
