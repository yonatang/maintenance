﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.SparepartInventory
{
   public class Request :BaseEntity,IBaseEntity
    {
        #region Business Properties and Methods

        //declare members
       
        private string _requestNo = string.Empty;
        private DateTime _requestDate = DateTime.Now;
        private int _requestedBy = 0;
        private string _approvalStatus = "Pending";
        private int _region_Id = 0;
        private string _rejectedreason;
        private IList<RequestItemDetail> _itemdetail = new List<RequestItemDetail>();
        
        public string RequestNo
        {
            get
            {
              
                return _requestNo;
            }
            set
            {
                
                if (value == null) value = string.Empty;
                if (!_requestNo.Equals(value))
                {
                    _requestNo = value;
                  
                }
            }
        }

        public DateTime RequestDate
        {
            get
            {
               
                return _requestDate.Date;
            }
            set {
                _requestDate = value;
            }
        }

        public string RejectedReason
        {
            get { return _rejectedreason; }
            set { _rejectedreason = value; }
        }

        public int RequestedBy
        {
            get
            {
              
                return _requestedBy;
            }
            set
            {
               
                if (!_requestedBy.Equals(value))
                {
                    _requestedBy = value;
                  
                }
            }
        }
        public int Region_Id
        {
            get
            {

                return _region_Id;
            }
            set
            {

                if (!_region_Id.Equals(value))
                {
                    _region_Id = value;

                }
            }
        }

        public string ApprovalStatus
        {
            get
            {
             
                return _approvalStatus;
            }
            set
            {
              
                if (value == null) value = string.Empty;
                if (!_approvalStatus.Equals(value))
                {
                    _approvalStatus = value;
                  
                }
            }
        }

        public IList<RequestItemDetail> ItemDetail
        {
            get { return _itemdetail; }
            set { _itemdetail = value; }

        }
        #endregion //Business Properties and Methods
    }
}
