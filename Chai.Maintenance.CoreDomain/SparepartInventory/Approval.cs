﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.SparepartInventory
{
    public class Approval:BaseEntity,IBaseEntity
    
    {
        #region Business Properties and Methods

        //declare members
      
        private string _approvalNo = string.Empty;
        private DateTime _approvalDate = DateTime.Now;
        private int _requestId = 0;
        private int _requestedBy = 0;
        private int _approver = 0;
        private string _approvalStatus = string.Empty;
        private int _regionId = 0;
        private string _approvername = string.Empty;
        private string _requestedbyname = string.Empty;
        private string _regionname = string.Empty;
        private string _rejectedreason;
        private IList<ApprovalItemDetail> _itemdetail = new List<ApprovalItemDetail>();
        public string ApprovalNo
        {
            get
            {
               
                return _approvalNo;
            }
            set
            {
                
                if (value == null) value = string.Empty;
                if (!_approvalNo.Equals(value))
                {
                    _approvalNo = value;
                  
                }
            }
        }

        public DateTime ApprovalDate
        {
            get
            {
              
                return _approvalDate.Date;
            }
            set
            {
                _approvalDate = value;
            }
        }

     

        public int RequestId
        {
            get
            {
          
                return _requestId;
            }
            set
            {
             
                if (!_requestId.Equals(value))
                {
                    _requestId = value;
              
                }
            }
        }

        public int RequestedBy
        {
            get
            {
             
                return _requestedBy;
            }
            set
            {
          
                if (!_requestedBy.Equals(value))
                {
                    _requestedBy = value;
             
                }
            }
        }

        public int Approver
        {
            get
            {
             
                return _approver;
            }
            set
            {
             
                if (!_approver.Equals(value))
                {
                    _approver = value;
                 
                }
            }
        }

        public string ApprovalStatus
        {
            get
            {
              
                return _approvalStatus;
            }
            set
            {
              
                if (value == null) value = string.Empty;
                if (!_approvalStatus.Equals(value))
                {
                    _approvalStatus = value;
                
                }
            }
        }

        public int RegionId
        {
            get
            {
             
                return _regionId;
            }
            set
            {
              
                if (!_regionId.Equals(value))
                {
                    _regionId = value;
                 
                }
            }
        }
        public IList<ApprovalItemDetail> ItemDetail
        {
            get
            {
                return _itemdetail;
            }
            set
            {
                _itemdetail = value;
            }
        }
        public string RejectedReason
        {
            get { return _rejectedreason; }
            set { _rejectedreason = value; }
        }
        public string RegionName
        {
            get { return _regionname; }
            set { _regionname = value; }
        }
        public string ApproverName
        {
            get { return _approvername; }
            set { _approvername = value; }
        
        }
        public string RequestedByName
        {
            get { return _requestedbyname; }
            set { _requestedbyname = value; }
         
        }
        #endregion //Business Properties and Methods
    }
}
