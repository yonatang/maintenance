﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain.SparepartInventory
{
    
    public class Recieve :BaseEntity,IBaseEntity
    {
        #region Business Properties and Methods

        //declare members
       
        private string _receiptNo = string.Empty;
        private DateTime _receiveDate = DateTime.Now.Date;
        private int _vendorId = 0;
        private string _vendorAgent = string.Empty;
        private string _sourceOfFinanance = string.Empty;
        private string _purchaseNo = string.Empty;
        private string _tendorNo = string.Empty;
        private string _letterNo = string.Empty;
        private string _sourceDescription = " ";
        private int _toolkeeperId = 0;
        private int _regionId = 0;
        private bool _isPosted = false;
        private string _regionname = string.Empty;
        private string _toolkeeperName = string.Empty;
        private IList<RecieveItemDetail> _itemdetail = new List<RecieveItemDetail>();
        
        public string ReceiptNo
        {
            get
            {
                
                return _receiptNo;
            }
            set
            {
              
                if (value == null) value = string.Empty;
                if (!_receiptNo.Equals(value))
                {
                    _receiptNo = value;
                   
                }
            }
        }

        public DateTime ReceiveDate
        {
            get
            {
               
                return _receiveDate;
            }
            set
            {
                
             
                if (!_receiveDate.Equals(value))
                {
                    _receiveDate = value;
                 
                }
            }
        }

       

        public int VendorId
        {
            get
            {
               
                return _vendorId;
            }
            set
            {
               
                if (!_vendorId.Equals(value))
                {
                    _vendorId = value;
                 
                }
            }
        }

        public string VendorAgent
        {
            get
            {
               
                return _vendorAgent;
            }
            set
            {
             
                if (value == null) value = string.Empty;
                if (!_vendorAgent.Equals(value))
                {
                    _vendorAgent = value;
                  
                }
            }
        }

        public string SourceOfFinanance
        {
            get
            {
             
                return _sourceOfFinanance;
            }
            set
            {
              
                if (value == null) value = string.Empty;
                if (!_sourceOfFinanance.Equals(value))
                {
                    _sourceOfFinanance = value;
                   
                }
            }
        }

        public string PurchaseNo
        {
            get
            {
               
                return _purchaseNo;
            }
            set
            {
              
                if (value == null) value = string.Empty;
                if (!_purchaseNo.Equals(value))
                {
                    _purchaseNo = value;
                   
                }
            }
        }

        public string TendorNo
        {
            get
            {
               
                return _tendorNo;
            }
            set
            {
               
                if (value == null) value = string.Empty;
                if (!_tendorNo.Equals(value))
                {
                    _tendorNo = value;
                  
                }
            }
        }

        public string LetterNo
        {
            get
            {
              
                return _letterNo;
            }
            set
            {
               
                if (value == null) value = string.Empty;
                if (!_letterNo.Equals(value))
                {
                    _letterNo = value;
                  
                }
            }
        }

        public string SourceDescription
        {
            get
            {
               
                return _sourceDescription;
            }
            set
            {
              
                if (value == null) value = string.Empty;
                if (!_sourceDescription.Equals(value))
                {
                    _sourceDescription = value;
                 
                }
            }
        }

        public int ToolkeeperId
        {
            get
            {
              
                return _toolkeeperId;
            }
            set
            {
               
                if (!_toolkeeperId.Equals(value))
                {
                    _toolkeeperId = value;
                 
                }
            }
        }

        public int RegionId
        {
            get
            {
              
                return _regionId;
            }
            set
            {
              
                if (!_regionId.Equals(value))
                {
                    _regionId = value;
                                   }
            }
        }

        public bool IsPosted
        {
            get
            {
               
                return _isPosted;
            }
            set
            {
              
                if (!_isPosted.Equals(value))
                {
                    _isPosted = value;
                 
                }
            }
        }
        public IList<RecieveItemDetail> ItemDetail
        {
            get { return _itemdetail; }
            set { _itemdetail = value; }
            
        }
        public string ToolKeepereName
        {
            get { return _toolkeeperName; }
            set { _toolkeeperName = value; }
        }
        public string RegionName
        {
            get { return _regionname; }
            set { _regionname = value; }
        }
        
        #endregion //Business Properties and Methods

    }
}
