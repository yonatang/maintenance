
using System;
using System.Collections;


namespace Chai.Maintenance.CoreDomain
{
    public class UserRole: BaseEntity, IBaseEntity
    {
        #region Member Variables

        protected int _userId;
        protected Role _role;

        #endregion

        #region Constructors

        public UserRole() { }

        public UserRole(int userId, Role role)
        {
            this._userId = userId;
            this._role = role;
        }

        #endregion

        #region Public Properties
               
        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public Role Role
        {
            get { return _role; }
            set { _role = value; }
        }

        #endregion
    }
}

