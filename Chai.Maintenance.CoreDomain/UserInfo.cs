﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.CoreDomain
{
    public class UserInfo : BaseEntity, IBaseEntity
    {
        private string _regionName = string.Empty;
        private int _regionId = 0;
        private string _siteName = string.Empty;
        private string _userFullName = string.Empty;
        private string _userName = string.Empty;
        public string SiteName
        {
            get { return _siteName; }
            set { _siteName = value; }
        }
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        public string UserFullName
        {
            get { return _userFullName; }
            set { _userFullName = value; }
        }

        public string RegionName
        {
            get { return _regionName; }
            set { _regionName = value; }
        }

        public int RegionId
        {
            get { return _regionId; }
            set { _regionId = value; }
        }

    }
}