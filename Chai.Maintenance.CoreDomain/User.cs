
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Shared;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.CoreDomain
{
    public class User : BaseEntity, IBaseEntity, IIdentity
    {
        #region Member Variables

        protected string _userName;
        protected string _password;
        protected string _firstName;
        protected string _lastName;
        protected string _email;
        protected bool _isActive;
        protected bool _isexternaluser;
        protected int _regionid=-1;
        protected int _siteid;
        protected int _vendorid;
        protected DateTime? _lastLogin;
        protected string _lastIp;
        protected DateTime _dateCreated = DateTime.Now;
        protected DateTime _dateModified = DateTime.Now;
        private bool _isAuthenticated = false;
        private string _VendorName = string.Empty;

        private AccessLevel[] _permissions = new AccessLevel[0];
        protected IList<UserRole> _roles = new List<UserRole>();
        protected IList<UserLocation> _userlocation = new List<UserLocation>();

        #endregion

        #region Constructors

        public User()
        {

        }

        public User(string userName, string password, string firstName, string lastName, string email, bool isActive,
                    DateTime lastLogin, string lastIp, DateTime dateCreated, DateTime dateModified, bool isexternaluser,
                    int regionid, int siteid, int vendorid)
        {
            this._userName = userName;
            this._password = password;
            this._firstName = firstName;
            this._lastName = lastName;
            this._email = email;
            this._isActive = isActive;
            this._lastLogin = lastLogin;
            this._lastIp = lastIp;
            this._dateCreated = dateCreated;
            this._dateModified = dateModified;
            this._isexternaluser = isexternaluser;
            this._regionid = regionid;
            this._siteid = siteid;
            this._vendorid = vendorid;
        }

        #endregion

        #region Public Properties

        public string UserName
        {
            get { return _userName; }
            set
            {
                if (value != null && value.Length > 50)
                    throw new ArgumentOutOfRangeException("Invalid value for UserName", value, value.ToString());
                _userName = value;
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("Invalid value for Password", value, value.ToString());
                _password = value;
            }
        }

        public string FirstName
        {
            get { return _firstName; }
            set
            {
                if (value != null && value.Length > 64)
                    throw new ArgumentOutOfRangeException("Invalid value for FirstName", value, value.ToString());
                _firstName = value;
            }
        }

        public string LastName
        {
            get { return _lastName; }
            set
            {
                if (value != null && value.Length > 64)
                    throw new ArgumentOutOfRangeException("Invalid value for LastName", value, value.ToString());
                _lastName = value;
            }
        }

        public string FullName
        {
            get
            {
                if (this._firstName != null && this._firstName != String.Empty
                    && this._lastName != null && this._lastName != String.Empty)
                {
                    return this._firstName + " " + this._lastName;
                }
                else
                {
                    return this._userName;
                }
            }
        }

        public string Email
        {
            get { return _email; }
            set
            {
                if (value != null && value.Length > 100)
                    throw new ArgumentOutOfRangeException("Invalid value for Email", value, value.ToString());
                _email = value;
            }
        }

        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public bool IsExternalUser
        {

            get { return _isexternaluser; }
            set { _isexternaluser = value; }

        }

        public int RegionId
        {
            get { return _regionid; }
            set { _regionid = value; }
        }

        public int SiteId
        {
            get { return _siteid; }
            set { _siteid = value; }
        }

        public int VendorId
        {
            get { return _vendorid; }
            set { _vendorid = value; }
        }

        public DateTime? LastLogin
        {
            get { return _lastLogin; }
            set { _lastLogin = value; }
        }

        public string LastIp
        {
            get { return _lastIp; }
            set
            {
                if (value != null && value.Length > 40)
                    throw new ArgumentOutOfRangeException("Invalid value for LastIp", value, value.ToString());
                _lastIp = value;
            }
        }

        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value; }
        }

        public DateTime DateModified
        {
            get { return _dateModified; }
            set { _dateModified = value; }
        }

        public string VendorName
        {
            get { return _VendorName; }
            set { _VendorName = value; }

        }

        public IList<UserRole> UserRoles
        {
            get { return _roles; }
            set { _roles = value; }
        }

        public IList<UserLocation> UserLocations
        {

            get { return _userlocation; }
            set { _userlocation = value; }
        }

        public AccessLevel[] Permissions
        {
            get
            {
                if (this._permissions.Length == 0)
                {
                    ArrayList permissions = new ArrayList();
                    foreach (UserRole role in UserRoles)
                    {
                        foreach (AccessLevel permission in role.Role.Permissions)
                        {
                            if (permissions.IndexOf(permission) == -1)
                                permissions.Add(permission);
                        }
                    }
                    this._permissions = (AccessLevel[]) permissions.ToArray(typeof (AccessLevel));
                }
                return this._permissions;
            }
        }

        public bool HasPermission(AccessLevel permission)
        {
            return Array.IndexOf(this.Permissions, permission) > -1;
        }

        public bool CanView(Node node)
        {
            foreach (NodePermission p in node.NodePermissions)
            {
                if (p.ViewAllowed && IsInRole(p.Role))
                {
                    return true;
                }
            }
            return false;
        }

        public bool CanEdit(Node node)
        {
            foreach (NodePermission p in node.NodePermissions)
            {
                if (p.EditAllowed && IsInRole(p.Role))
                {
                    return true;
                }
            }
            return false;
        }

        public static string HashPassword(string password)
        {
            if (ValidatePassword(password))
            {
                return Encryption.StringToMD5Hash(password);
            }
            else
            {
                throw new ArgumentException("Invalid password");
            }
        }

        public static bool ValidatePassword(string password)
        {
            return (password.Length >= 5);
        }

        public bool IsInRole(Role roleToCheck)
        {
            foreach (UserRole role in this.UserRoles)
            {
                if (role.Role.Id == roleToCheck.Id && role.Role.Name == roleToCheck.Name)
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsInRole(string roleName)
        {
            foreach (UserRole role in this.UserRoles)
            {
                if (role.Role.Name == roleName)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion


        #region IIdentity Members

        public string AuthenticationType
        {
            get { return "RamcsAuthentication"; }
        }

        public bool IsAuthenticated
        {
            get { return this._isAuthenticated; }
            set { this._isAuthenticated = value; }
        }

        public string Name
        {
            get
            {
                if (this._isAuthenticated)
                    return base.Id.ToString();
                else
                    return "";
            }
        }

        #endregion



    }
}
