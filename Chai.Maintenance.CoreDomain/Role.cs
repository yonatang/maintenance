
using System;
using System.Collections;
using System.Text;
using Chai.Maintenance.Enums;

namespace Chai.Maintenance.CoreDomain
{
    public class Role: BaseEntity, IBaseEntity
    {
        #region Member Variables

        protected string _name;
        protected int _permissionLevel;
        protected IList _userRoles;
        private AccessLevel[] _permissions;

        #endregion

        #region Constructors

        public Role() 
        {
            this._permissionLevel = -1;
        }

        public Role(string name, int permissionLevel)
        {
            this._name = name;
            this._permissionLevel = permissionLevel;
        }

        #endregion

        #region Public Properties

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != null && value.Length > 32)
                    throw new ArgumentOutOfRangeException("Invalid value for Name", value, value.ToString());
                _name = value;
            }
        }

        public int PermissionLevel
        {
            get { return _permissionLevel; }
            set
            {
                _permissionLevel = value; 
                TranslatePermissionLevelToAccessLevels();
            }
        }

        public virtual AccessLevel[] Permissions
        {
            get { return this._permissions; }
        }

        public virtual string PermissionsString
        {
            get { return GetPermissionsAsString(); }
        }

        public virtual bool HasPermission(AccessLevel permission)
		{
			return Array.IndexOf(this.Permissions, permission) > -1;
		}

		private void TranslatePermissionLevelToAccessLevels()
		{
			ArrayList permissions = new ArrayList();
			AccessLevel[] accessLevels = (AccessLevel[])Enum.GetValues(typeof(AccessLevel));

			foreach (AccessLevel accesLevel in accessLevels)
			{
				if ((this.PermissionLevel & (int)accesLevel) == (int)accesLevel)
				{
					permissions.Add(accesLevel);
				}
			}
			this._permissions = (AccessLevel[])permissions.ToArray(typeof(AccessLevel));
		}

		private string GetPermissionsAsString()
		{
			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < this._permissions.Length; i++)
			{
				AccessLevel accessLevel = this._permissions[i];
				sb.Append(accessLevel.ToString());
				if (i < this._permissions.Length - 1)
				{
					sb.Append(", ");
				}
			}

			return sb.ToString();
		}        
        
        #endregion

    }
}
