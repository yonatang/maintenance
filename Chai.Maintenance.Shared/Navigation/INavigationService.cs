﻿using System;
using System.Collections.Generic;

namespace Chai.Maintenance.Shared.Navigation
{
    public interface INavigationService
    {
        void Navigate(string view);
    }
}
