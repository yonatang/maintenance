﻿using System;
using System.Collections.Generic;

namespace Chai.Maintenance.Shared.Settings
{
    /// <summary>
    /// Technical parameters.
    /// </summary
    public static class TechnicalSettings
    {
        public const string VERSION   = "1.0.0.*";
        public const string COMPANY   = "CHAI Ethiopia";
        public const string PRODUCT   = "Chai Maintenance Suite";
        public const string COPYRIGHT = "CHAI 2011";

        public static string SoftwareVersion
        {
            get { return string.Format("v{0}", VERSION); }
        }

        public static string Copyright
        {
            get { return COPYRIGHT; }
        }

        public static string Product
        {
            get { return PRODUCT; }
        }

        public static string Company
        {
            get { return COMPANY; }
        }

        public static string DatabaseServerName
        {
            get { return TechnicalConfig.GetConfiguration()["DATABASE_SERVER_NAME"]; }
        }

        public static string DatabaseName
        {
            get { return TechnicalConfig.GetConfiguration()["DATABASE_NAME"]; }
        }

        public static string DatabasePassword
        {
            get { return  TechnicalConfig.GetConfiguration()["DATABASE_PASSWORD"]; }
        }

        public static string DatabaseLoginName
        {
            get { return TechnicalConfig.GetConfiguration()["DATABASE_LOGIN_NAME"]; }
        }

        public static int DatabaseTimeout
        {
            get { return 100;}// int.Parse(TechnicalConfig.GetConfiguration()["DATABASE_TIMEOUT"]); }
        }
    }

}
