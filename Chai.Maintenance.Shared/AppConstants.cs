﻿using System;
using System.Collections.Generic;

namespace Chai.Maintenance.Shared
{

    public static class AppConstants
    {
        public const string TABID =  "tabid";
        public const string NODEID = "nodeid";
        public const string USERID = "userid";
        public const string ROLEID = "roleid";
        public const string ACCOUNTID = "accid";
        public const string ACCOUNTTYPE = "acctype";
        public const string TAXCODEID = "tcodeid";
        public const string REGIONID = "regionid";
    }
}
