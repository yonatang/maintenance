﻿<%@ control language="C#" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Shell.Views.NodeNavigation, App_Web_yhv4hnub" %>
    <style>
    .portlet-content li
    {
          border-bottom: 1px dashed #BBBBBB;
    margin: 0 0px 0 0px;
    padding: 5px 0;
   list-style: none outside none;
    }
    </style>
<asp:Repeater ID="rptPanel" runat="server" 
    onitemdatabound="rptPanel_ItemDataBound">
    <ItemTemplate>
      <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
		<div class="portlet-header ui-widget-header ui-corner-top">
                        <asp:Literal ID="ltrTitle" runat="server" />
                        </div>

		<div class="portlet-content">
                  
                <asp:PlaceHolder ID="plhNode" runat="server"></asp:PlaceHolder>
           </div>
        </div>
    </ItemTemplate>
</asp:Repeater>

