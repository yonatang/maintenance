﻿<%@ control language="C#" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Shell.Views.RMessageBox, App_Web_yhv4hnub" %>

<asp:Panel ID="panMessage" runat="server" >
    <asp:Image ID="imgIcon" runat="server" ImageAlign="AbsMiddle" />
    <asp:Literal ID="ltrMessage" runat="server" />
</asp:Panel>

