﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;
namespace Chai.Maintenance.Services.Maintenance
{
   public  class CurativeMaintenanceServices
    {
        private CurativeMaintenanceDao _CurativeMaintenanceDao;

        private SparepartsServices _SparepartsServices;

        public CurativeMaintenanceServices()
        {
            _CurativeMaintenanceDao = new CurativeMaintenanceDao();
        }

        public void SaveOrUpdateCurativeMaintenance(CurativeMaintenance CurativeMaintenance)
        {
            if (CurativeMaintenance.Id <= 0)
            {
                _CurativeMaintenanceDao.Save(CurativeMaintenance);
                _SparepartsServices = new SparepartsServices();
                foreach (Spareparts spares in CurativeMaintenance.Sapres)
                {
                    _SparepartsServices.SaveOrUpdateSpareparts(spares, CurativeMaintenance.Id, "Curative");
                }
            }
            else
            {
                _CurativeMaintenanceDao.Update(CurativeMaintenance);
                _SparepartsServices = new SparepartsServices();
                foreach (Spareparts spares in CurativeMaintenance.Sapres)
                {

                    _SparepartsServices.SaveOrUpdateSpareparts(spares, CurativeMaintenance.Id, "Curative");
                   
                }
            }
        }

        public CurativeMaintenance GetCurativeMaintenanceById(int curativeId)
        {
            return _CurativeMaintenanceDao.GetCurativeMaintenanceById(curativeId);
        }

        public void DeleteCurativeMaintenance(int curativeId)
        {
            _CurativeMaintenanceDao.Delete(curativeId);
        }

        public IList<CurativeMaintenance> GetAllCurativeMaintenance(string problemNumber, int InstrumentId)
        {
            return _CurativeMaintenanceDao.GetListOfCurativeMaintenances(problemNumber, InstrumentId);
        }


        public IList<CurativeMaintenance> GetCurativeMaintetanceBySiteInsCategoryId(int SiteId, int InstrumentCat, int userId)
        {
            return _CurativeMaintenanceDao.GetCurativeMaintetanceBySiteInsCategoryId(SiteId, InstrumentCat, userId);
        }
       
    }
}
