﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;

namespace Chai.Maintenance.Services.Maintenance
{
    public class EscalatedJobsServices
    {
        private EscalatedJobsDao _escalatedJobsDao;
        private EscalatedJobs _escalatedJobs;
        public EscalatedJobsServices()
        {
            _escalatedJobsDao = new EscalatedJobsDao();
        }

        public IList<EscalatedJobs> GetEscalatedJobsByRegionSite(int regionId, int siteId)
        {
            return _escalatedJobsDao.GetEscalatedJobsByRegionSite(regionId, siteId);
        }
       
         
    }
}
