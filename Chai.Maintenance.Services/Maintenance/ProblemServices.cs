﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;

namespace Chai.Maintenance.Services.Maintenance
{
    public class ProblemServices
    {
        private ProblemDao _problemdao;
        private Problem _problem;
        public ProblemServices()
        {
            _problemdao = new ProblemDao();
        }
        public void NewProblem()
        {

        }
        public void SaveOrUpdateProblem(Problem problem, bool NotFunctional)
        {
            if (problem.Id <= 0)
            {
                _problemdao.Save(problem, NotFunctional);
            }
            else
            {
                _problemdao.Update(problem);
            }
        }

        public Problem GetProblemById(int problemId)
        {
            return _problemdao.GetProblemById(problemId);
        }

        public void DeleteProblem(int problemId)
        {
            _problemdao.Delete(problemId);
        }

        public IList<Problem> GetAllProblems(int siteid, int problemId)
        {
            return null;
        }
        public IList<Problem> GetProblemForCurativeSchedule(int regionId, int siteId, int InstrumentCategoryId, int ProblemType)
        {
            return _problemdao.GetProblemForCurativeSchedule(regionId, siteId, InstrumentCategoryId, ProblemType);
        }

        public IList<Problem> GetProblemBySiteInsCategoryId(int regionId, int SiteId, int InstrumentCat, int ProblemType)
        {
            return _problemdao.GetProblemBySiteInsCategoryId(regionId, SiteId, InstrumentCat, ProblemType);
        }

        public Problem GetInstrumentById(int problemId)
        {
            return _problemdao.GetProblemById(problemId);
        }
       
    }
}
