﻿ 
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;

namespace Chai.Maintenance.Services.Maintenance
{
    public class EscalateReasonServices
    {
        private EscalateReasonDao _escalateReasonDao;
 
        public EscalateReasonServices()
        {
            _escalateReasonDao = new EscalateReasonDao();
        }
        public void NewEscalateReason()
        {

        }
        public void SaveOrUpdateEscalateReason(EscalateReason escalateReason)
        {
            if (escalateReason.Id <= 0)
            {
                _escalateReasonDao.Save(escalateReason);
            }
            else
            {
                _escalateReasonDao.Update(escalateReason);
            }
        }

        public EscalateReason GetEscalateReasonById(int escalateReasonId)
        {
            return _escalateReasonDao.GetEscalateReasonById(escalateReasonId);
        }

        public void DeleteEscalateReason(int escalateReasonId)
        {
            _escalateReasonDao.Delete(escalateReasonId);
        }

        public IList<EscalateReason> GetEscalateReasonBySiteInsCategoryId(int siteId, int instrumentCat, int enginerId)
        {
            return _escalateReasonDao.GetEscalateReasonBySiteInsCategoryId(siteId, instrumentCat, enginerId);
        }

        public EscalateReason GetEscalateReasonByProblemIdANDScheduleId(int problemId, int scheduleId)
        {
            return _escalateReasonDao.GetEscalateReasonByProblemIdANDScheduleId(problemId, scheduleId);
        }
    }
}
