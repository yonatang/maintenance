﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;

namespace Chai.Maintenance.Services.Maintenance
{
    public class ConsumableNotificationServices
    {
        private ConsumableNotificationDao _consumableNotificationDao;
        private ConsumableNotification _consumableNotification;
        public ConsumableNotificationServices()
        {
            _consumableNotificationDao = new ConsumableNotificationDao();
        }
        public void NewConsumableNotification()
        {

        }
        public void SaveOrUpdateConsumableNotification(ConsumableNotification _consumableNotification)
        {
            if (_consumableNotification.Id <= 0)
                _consumableNotificationDao.Save(_consumableNotification);
            else
                _consumableNotificationDao.Update(_consumableNotification);
        }
        public ConsumableNotification GetConsumableNotificationById(int consumableNotificationId)
        {
            return _consumableNotificationDao.GetConsumableNotificationById(consumableNotificationId);
        }
        public void DeleteConsumableNotification(int consumableNotificationId)
        {
            _consumableNotificationDao.Delete(consumableNotificationId);
        }
        public IList<ConsumableNotification> GetConsumableNotificationBySiteConsumableId(int siteId, int consumableId, int notifiedbyId)
        {
            return _consumableNotificationDao.GetConsumableNotificationBySiteConsumableId(siteId, consumableId, notifiedbyId);
        }
    }
}
