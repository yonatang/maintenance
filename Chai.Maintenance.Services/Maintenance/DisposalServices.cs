﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;

namespace Chai.Maintenance.Services.Maintenance
{
    public class DisposalServices
    {
        private DisposalDao _DisposalDao;
       
        public DisposalServices()
        {
            _DisposalDao = new DisposalDao();
        }
        public void NewDisposal()
        {

        }
        public void SaveOrUpdateDisposal(Disposal _Disposal)
        {
            if (_Disposal.Id <= 0)
            {
                _DisposalDao.Save(_Disposal);
            }
            else
            {
                _DisposalDao.Update(_Disposal);
            }
        }

        public Disposal GetDisposalById(int DisposalID)
        {
            return _DisposalDao.GetDisposalById(DisposalID);
        }

        public void DeleteDisposal(int DisposalID)
        {
            _DisposalDao.Delete(DisposalID);
        }

        public IList<Disposal> GetDisposalBySiteInsCategoryId(int SiteId, int InstrumentCat, int userId)
        {
            return _DisposalDao.GetDisposalBySiteInsCategoryId(SiteId, InstrumentCat, userId);
        }
    }
}
