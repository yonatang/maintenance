﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;

namespace Chai.Maintenance.Services.Maintenance
{
    public class AssignedJobsServices
    {
        private AssignedJobsDao _assignedJobsDao;
        private AssignedJobs _assignedJobs;
        public AssignedJobsServices()
        {
            _assignedJobsDao = new AssignedJobsDao();
        }

        public IList<AssignedJobs> GetAssignedJobs(string searchBy, int userId, int siteid)
        {
            return _assignedJobsDao.GetAssignedJobs(searchBy, userId, siteid);

        }
       
         
    }
}
