﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;
namespace Chai.Maintenance.Services.Maintenance
{
    public class PreventiveMaintenanceServices
    {
        private PreventiveMaintenanceDao _PreventiveMaintenanceDao;

        private SparepartsServices _SparepartsServices;

        public PreventiveMaintenanceServices()
        {
            _PreventiveMaintenanceDao = new PreventiveMaintenanceDao();
        }

        public void SaveOrUpdatePreventiveMaintenance(PreventiveMaintenance PreventiveMaintenance)
        {
            if (PreventiveMaintenance.Id <= 0)
            {
                _PreventiveMaintenanceDao.Save(PreventiveMaintenance);
                _SparepartsServices = new SparepartsServices();
                foreach (Spareparts spares in PreventiveMaintenance.Sapres)
                {
                    _SparepartsServices.SaveOrUpdateSpareparts(spares, PreventiveMaintenance.Id, "Prev");
                }
            }
            else
            {
                _PreventiveMaintenanceDao.Update(PreventiveMaintenance);
                _SparepartsServices = new SparepartsServices();
                foreach (Spareparts spares in PreventiveMaintenance.Sapres)
                {

                    _SparepartsServices.SaveOrUpdateSpareparts(spares, PreventiveMaintenance.Id, "Prev");

                }
            }
        }

        public PreventiveMaintenance GetPreventiveMaintenanceById(int preventiveId)
        {
            return _PreventiveMaintenanceDao.GetPreventiveMaintenanceById(preventiveId);
        }

        public void DeletePreventiveMaintenance(int curativeId)
        {
            _PreventiveMaintenanceDao.Delete(curativeId);
        }

        public IList<PreventiveMaintenance> GetAllPreventiveMaintenance(int InstrumentId)
        {
            return _PreventiveMaintenanceDao.GetListOfPreventiveMaintenance(InstrumentId);
        }
        public IList<PreventiveMaintenance> GetPreventiveMaintetanceBySiteInsCategoryId(int SiteId, int InstrumentCat, int userId)
        {
            return _PreventiveMaintenanceDao.GetPreventiveMaintetanceBySiteInsCategoryId(SiteId, InstrumentCat, userId);///////////////////
        }
    }
}
