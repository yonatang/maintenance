﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;

namespace Chai.Maintenance.Services.Maintenance
{
    public class PreventiveMaintenanceChecklistServices
    {
        private PreventiveMaintenanceChecklistDao _checklistDao;
        private PreventiveMaintenanceChecklist _checklist;
        public PreventiveMaintenanceChecklistServices()
        {
            _checklistDao = new PreventiveMaintenanceChecklistDao();
        }
        public void NewPreventiveMaintenanceChecklist()
        {

        }
        public void SaveOrUpdatePreventiveMaintenanceChecklist(PreventiveMaintenanceChecklist _checklist)
        {
            if (_checklist.Id <= 0)
            {
                _checklistDao.Save(_checklist);
            }
            else
            {
                _checklistDao.Update(_checklist);
            }
        }

        public PreventiveMaintenanceChecklist GetPreventiveMaintenanceChecklistById(int checklistId)
        {
            return _checklistDao.GetPreventiveMaintenanceChecklistById(checklistId);
        }

        public void DeletePreventiveMaintenanceChecklist(int checklistId)
        {
            _checklistDao.Delete(checklistId);
        }

       
    }
}
