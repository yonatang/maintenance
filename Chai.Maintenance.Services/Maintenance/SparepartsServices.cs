﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;

namespace Chai.Maintenance.Services.Maintenance
{
    public class SparepartsServices
    {
        private SparepartsDao _SparepartsDao;
        private Spareparts _spare;


        public SparepartsServices()
        {
            _SparepartsDao = new SparepartsDao();
        }

        public void SaveOrUpdateSpareparts(Spareparts _spare, int parentId, string Type)
        {
            if (_spare.Id <= 0)
            {
                _SparepartsDao.Save(_spare, parentId, Type);
            }
            else
            {
                _SparepartsDao.Update(_spare);
            }
        }

        public Spareparts GetSparepartsById(int Sparepartsid)
        {
            return _SparepartsDao.GetSparepartsById(Sparepartsid);
        }

        public void DeleteSpareparts(int SparepartsId)
        {
            _SparepartsDao.Delete(SparepartsId);
        }

        public IList<Spareparts> GetAllRecieve(int curativeid)
        {
            return _SparepartsDao.GetListOfSpareparts(curativeid);
        }
       
    }
}
