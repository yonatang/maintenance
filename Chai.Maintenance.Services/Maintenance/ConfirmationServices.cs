﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;

namespace Chai.Maintenance.Services.Maintenance
{
    public class ConfirmationServices
    {
        private ConfirmationDao _ConfirmationDao;
        private Confirmation _Confirmation;
        public ConfirmationServices()
        {
            _ConfirmationDao = new ConfirmationDao();
        }
        public void NewConfirmation()
        {

        }
        public void SaveOrUpdateConfirmation(Confirmation _Confirmation, string searchBy,int ScheduleId)
        {
            if (_Confirmation.Id <= 0)
            {
                _ConfirmationDao.Save(_Confirmation, searchBy, ScheduleId);
            }
            else
            {
                _ConfirmationDao.Update(_Confirmation);
            }
        }
        public void DeleteConfirmation(int ConfirmationId)
        {
            _ConfirmationDao.Delete(ConfirmationId);
        }

        public IList<Confirmation> GetAllConfirmation(int siteid)
        {
            //return _ConfirmationDao.GetAllProblem(siteid);
            return null;
        }
       
        public Confirmation GetConfirmationById(int ConfirmationId)
        {
            return _ConfirmationDao.GetConfirmationById(ConfirmationId);
        }
        //public IList<Instrument> GetInstrumentByRegionSiteInst(int siteId, int instrumentId)
        //{
        //    return _instrumentdao.GetInstrumentByRegionSiteInst(siteId, instrumentId);
        //}
        //public IList<Instrument> GetInstrumentBysiteID(int siteId)
        //{
        //    return _instrumentdao.GetInstrumentBySiteID(siteId);
        //}

        
    }
}
