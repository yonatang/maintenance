﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;

namespace Chai.Maintenance.Services.Maintenance
{
    public class ScheduleServices
    {
        private ScheduleDao _Scheduledao;
        private Schedule _Schedule;
        public ScheduleServices()
        {
            _Scheduledao = new ScheduleDao();
        }
        public void NewSchedule()
        {

        }
        public void SaveOrUpdateSchedule(Schedule _Schedule, string type, int escalateId)
        {
            if (_Schedule.Id <= 0)
            {
                _Scheduledao.Save(_Schedule, type, escalateId);
            }
            else
            {
                _Scheduledao.Update(_Schedule);
            }
        }

        public Schedule GetScheduleById(int ScheduleID)
        {
            return _Scheduledao.GetScheduleById(ScheduleID);
        }
        public IList<Schedule> GetScheduleBySiteInsCategoryId(int InstrumentCat, string type, int userId)
        {
            return _Scheduledao.GetScheduleBySiteInsCategoryId(InstrumentCat, type, userId);
        }


        public IList<Schedule> GetSchedulesByEnginerId(int enginerId, string maintenanceType)
        {
            return _Scheduledao.GetSchedulesByEnginerId(enginerId, maintenanceType);
        }


        public void DeleteSchedule(int ScheduleID)
        {
            _Scheduledao.Delete(ScheduleID);
        }

    
    }
}
