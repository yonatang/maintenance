﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.DataAccess.Maintenance;

namespace Chai.Maintenance.Services.Maintenance
{
    public class TransferServices
    {
        private TransferDao _transferdao;
        private Transfer _transfer;
        public TransferServices()
        {
            _transferdao = new TransferDao();
        }
        public void NewTransfer()
        {

        }
        public void SaveOrUpdateTransfer(Transfer transfer)
        {
            if (transfer.Id <= 0)
            {
                _transferdao.Save(transfer);
            }
            else
            {
                _transferdao.Update(transfer);
            }
        }

        public Transfer GeTransferById(int TransferId)
        {
            return _transferdao.GetTransferById(TransferId);
        }

        public void DeleteTransfer(int TransferId)
        {
            _transferdao.Delete(TransferId);
        }

        public IList<Transfer> GetAllTransfers(int siteid)
        {
            //return _problemdao.GetAllProblem(siteid, problemId);
            return null;
        }
        public IList<Transfer> GetTransferBySiteInsCategoryId(int UserId, int InstrumentCat)
        {
            return _transferdao.GetTransferBySiteInsCategoryId(UserId, InstrumentCat);
        }
        
        //public IList<Instrument> GetInstrumentByRegionSiteInst(int siteId, int instrumentId)
        //{
        //    return _instrumentdao.GetInstrumentByRegionSiteInst(siteId, instrumentId);
        //}
        //public IList<Instrument> GetInstrumentBysiteID(int siteId)
        //{
        //    return _instrumentdao.GetInstrumentBySiteID(siteId);
        //}

        
    }
}
