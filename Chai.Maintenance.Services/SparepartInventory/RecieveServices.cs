﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.DataAccess.SparepartInventory;
namespace Chai.Maintenance.Services.SparepartInventory
{
   public  class RecieveServices
    {
        private RecieveDao _recieveDao;
        private Recieve _recieve;
        private RecieveItemDetailServices _recievedetailService;
        
        public RecieveServices()
        {
            _recieveDao = new RecieveDao();
        }

        public void SaveOrUpdateRecieve(Recieve recieve)
        {
            if (recieve.Id <= 0)
            {
                _recieveDao.Save(recieve);
                _recievedetailService = new RecieveItemDetailServices();
                foreach(RecieveItemDetail detail in recieve.ItemDetail)
                {
                    _recievedetailService.SaveOrUpdateRecieve(detail,recieve.Id);
                }
            }
            else
            {
                _recieveDao.Update(recieve);
                _recievedetailService = new RecieveItemDetailServices();
                foreach (RecieveItemDetail detail in recieve.ItemDetail)
                {
                   
                        _recievedetailService.SaveOrUpdateRecieve(detail, recieve.Id);
                   
                }
            }
        }

        public Recieve GetRecieveById(int receiveid)
        {
            return _recieveDao.GetRecieveById(receiveid);
        }

        public void DeleteRecieve(int recieveId)
        {
            _recieveDao.Delete(recieveId);
        }

        public IList<Recieve> GetAllRecieve(int selectby ,string value,string datefrom, string dateto,int regionId)
        {
            return _recieveDao.GetListOfRecieves(selectby, value, datefrom, dateto, regionId);
        }
       
    }
}
