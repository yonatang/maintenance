﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.DataAccess.SparepartInventory;

namespace Chai.Maintenance.Services.SparepartInventory
{
    public class ApprovalServices
    {
        private ApprovalDao _approvalDao;
        private Approval _approval;
        private ApprovalItemDetailServices _approvaldetailService;
        private RequestItemdetailServices _requesteddetailService;
        public ApprovalServices()
        {
            _approvalDao = new ApprovalDao();
        }

        public void SaveOrUpdateApproval(Approval approval)
        {
            if (approval.Id <= 0)
            {
                _approvalDao.Save(approval);
                _approvaldetailService = new ApprovalItemDetailServices();
                foreach (ApprovalItemDetail detail in approval.ItemDetail)
                {
                    _approvaldetailService.SaveOrUpdateApprovalDetail(detail, approval.Id);
                }
            }
            else
            {
                _approvalDao.Update(approval);
                _approvaldetailService = new ApprovalItemDetailServices();
                foreach (ApprovalItemDetail detail in approval.ItemDetail)
                {

                    _approvaldetailService.SaveOrUpdateApprovalDetail(detail, approval.Id);
                   
                }
            }
        }

        public Approval GetApprovalById(int approvalid)
        {
            return _approvalDao.GetApprovalById(approvalid);
        }

        public void DeleteApproval(int approvalid)
        {
            _approvalDao.Delete(approvalid);
        }

        public IList<Approval> GetAllApproval(int selectby, string value, string datefrom, string dateto,int regionId,int userId)
        {
            return _approvalDao.GetApprovals(selectby, value, datefrom, dateto,regionId,userId);
        }
        public IList<Approval> GetAllApprovalforIssue(int selectby, string value, string datefrom, string dateto, int regionId, int userId)
        {
            return _approvalDao.GetApprovalsforIssue(selectby, value, datefrom, dateto, regionId, userId);
        }
        public IList<RequestItemDetail> GetListofRequestedItems(int requestid)
        {
            _requesteddetailService = new RequestItemdetailServices();
            return _requesteddetailService.GetAllRequestDetial(requestid);
        }

    }
}
