﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.DataAccess.SparepartInventory;


namespace Chai.Maintenance.Services.SparepartInventory
{
    public class RequestItemdetailServices 
    {
        private RequestItemDetailDao _requestitemdetailDao;
        private RequestItemDetail _request;


        public RequestItemdetailServices()
        {
            _requestitemdetailDao = new RequestItemDetailDao();
        }

        public void SaveOrUpdateRequest(RequestItemDetail requestdetail, int requestid)
        {
            if (requestdetail.Id <= 0)
            {
                _requestitemdetailDao.Save(requestdetail, requestid);
            }
            else
            {
                _requestitemdetailDao.Update(requestdetail);
            }
        }

        public RequestItemDetail GetRequestById(int requestdetailid)
        {
            return _requestitemdetailDao.GetRequestItemDetailById(requestdetailid);
        }

        public void DeleteRecieveDetail(int recievedetailId)
        {
            _requestitemdetailDao.Delete(recievedetailId);
        }

        public IList<RequestItemDetail> GetAllRequestDetial(int requstid)
        {
            return _requestitemdetailDao.GetListOfRequestDetails(requstid);
        }
       
    }
}
