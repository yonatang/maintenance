﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.DataAccess.SparepartInventory;

namespace Chai.Maintenance.Services.SparepartInventory
{
   public  class RequestServices
    {
        private RequestDao _requestDao;
        private Request _request;
        private RequestItemdetailServices _requestdetailservices;

        public RequestServices()
        {
            _requestDao = new RequestDao();
        }

        public void SaveOrUpdateRequest(Request request)
        {
            if (request.Id <= 0)
            {
                _requestDao.Save(request);
                _requestdetailservices = new RequestItemdetailServices();
                foreach (RequestItemDetail detail in request.ItemDetail)
                {
                    _requestdetailservices.SaveOrUpdateRequest(detail, request.Id);
                }
            }
            else
            {
                _requestDao.Update(request);
                _requestdetailservices = new RequestItemdetailServices();
                foreach (RequestItemDetail detail in request.ItemDetail)
                {

                    _requestdetailservices.SaveOrUpdateRequest(detail, request.Id);
                   
                }
            }
        }

        public Request GetRequestById(int requestid)
        {
            return _requestDao.GetRequestById(requestid);
        }

        public void DeleteRequest(int requestId)
        {
            _requestDao.Delete(requestId);
        }

        public IList<Request> GetAllRequest(int selectby ,string value,string datefrom, string dateto,int regionid,int userId)
        {
            return _requestDao.GetRequests(selectby, value, datefrom, dateto, regionid, userId);
        }
        public IList<Request> GetAllRequestforapprovals(int selectby, string value, string datefrom, string dateto, int regionid, int userId)
        {
            return _requestDao.GetRequestsforApproval(selectby, value, datefrom, dateto, regionid, userId);
        }
    }
}
