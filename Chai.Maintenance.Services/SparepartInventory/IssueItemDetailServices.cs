﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.DataAccess.SparepartInventory;
namespace Chai.Maintenance.Services.SparepartInventory
{
    public class IssueItemDetailServices
    {
        private IssueItemDetailDao _issueitemdetailDao;
        private IssueItemDetail _issuedetail;


        public IssueItemDetailServices()
        {
            _issueitemdetailDao = new IssueItemDetailDao();
        }

        public void SaveOrUpdateIssueDetail(IssueItemDetail issuedetail, int issueid,int regionId)
        {
            if (issuedetail.Id <= 0)
            {
                //_issueitemdetailDao.Save(issuedetail, issueid, regionId);
            }
            else
            {
                _issueitemdetailDao.Update(issuedetail);
            }
        }

        public IssueItemDetail GetIssueById(int issuedetailid)
        {
            return _issueitemdetailDao.GetIssueItemDetailById(issuedetailid);
        }

        public void DeleteIssueDetail(int issuedetailId)
        {
            _issueitemdetailDao.Delete(issuedetailId);
        }

        public IList<IssueItemDetail> GetAllIssue(int issueid)
        {
            return _issueitemdetailDao.GetListOfIssueDetails(issueid);
        }
       
    }
}
