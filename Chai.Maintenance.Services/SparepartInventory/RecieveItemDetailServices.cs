﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.DataAccess.SparepartInventory;

namespace Chai.Maintenance.Services.SparepartInventory
{
    public class RecieveItemDetailServices
    {
        private RecieveItemDetailDao _recieveitemdetailDao;
        private RecieveItemDetail _recieve;


        public RecieveItemDetailServices()
        {
            _recieveitemdetailDao = new RecieveItemDetailDao();
        }

        public void SaveOrUpdateRecieve(RecieveItemDetail recievedetail,int receiveid)
        {
            if (recievedetail.Id <= 0)
            {
                _recieveitemdetailDao.Save(recievedetail, receiveid);
            }
            else
            {
                _recieveitemdetailDao.Update(recievedetail);
            }
        }

        public RecieveItemDetail GetRecieveById(int receivedetailid)
        {
            return _recieveitemdetailDao.GetRecieveItemDetailById(receivedetailid);
        }

        public void DeleteRecieveDetail(int recievedetailId)
        {
            _recieveitemdetailDao.Delete(recievedetailId);
        }

        public IList<RecieveItemDetail> GetAllRecieve(int recieveid)
        {
            return _recieveitemdetailDao.GetListOfRecieves(recieveid);
        }
        public void PostItems(IList<RecieveItemDetail> ItemDetail,int regionId,int recieveId)
        {
            _recieveitemdetailDao.PostItems(ItemDetail, regionId, recieveId);
        }
    }
}
