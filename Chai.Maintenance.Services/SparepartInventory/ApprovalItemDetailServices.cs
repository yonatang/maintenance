﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.DataAccess.SparepartInventory;

namespace Chai.Maintenance.Services.SparepartInventory
{
    public class ApprovalItemDetailServices
    {
        private ApprovalItemdetailDao _approvalitemdetailDao;
        private ApprovalItemDetail _approvaldetail;


        public ApprovalItemDetailServices()
        {
            _approvalitemdetailDao = new ApprovalItemdetailDao();
        }

        public void SaveOrUpdateApprovalDetail(ApprovalItemDetail approvaldetail, int approvalid)
        {
            if (approvaldetail.Id <= 0)
            {
                _approvalitemdetailDao.Save(approvaldetail, approvalid);
            }
            else
            {
                _approvalitemdetailDao.Update(approvaldetail);
            }
        }

        public ApprovalItemDetail GetApprovalDetailById(int approvaldetailid)
        {
            return _approvalitemdetailDao.GetApprovalItemDetailById(approvaldetailid);
        }

        public void DeleteApprovalDetail(int approvaldetailid)
        {
            _approvalitemdetailDao.Delete(approvaldetailid);
        }

        public IList<ApprovalItemDetail> GetAllApprovalDetail(int approvalid,int regionId)
        {
            return _approvalitemdetailDao.GetListOfApprovalDetails(approvalid,regionId);
        }
        
    }
}
