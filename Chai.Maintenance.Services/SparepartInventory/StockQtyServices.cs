﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.DataAccess.SparepartInventory;

namespace Chai.Maintenance.Services.SparepartInventory
{
   public class StockQtyServices
    {
        private StockQtyDao _stockQtyDao;
        private StockQty _stockQty;
        
        public StockQtyServices()
        {
            _stockQtyDao = new StockQtyDao();
        }
        public void NewStockQty()
        {
            _stockQty = new StockQty();
        }
        public StockQty GetStockQtyById(int region_Id,int Sarepart_Id)
        {
            return _stockQtyDao.GetStockQtyBy(region_Id, Sarepart_Id);
        }
        public IList<StockQty> GetStockQty(int instrumnetId,int sparepartId,int regionId)
        {
            return _stockQtyDao.GetStockQtys(instrumnetId, sparepartId, regionId);
        }
        public IList<StockQty> GetReorderSparepart(int regionId)
        {
            return _stockQtyDao.GetReorderSparepart(regionId);
        }
        public void SaveorUpdateStockQty(StockQty stockqty)
        {
            if (stockqty.Id <= 0)
            {
                _stockQtyDao.Save(stockqty);
            }
            else
            {
                _stockQtyDao.Update(stockqty);
            }
        }
        public void DeleteStockQty(int Id)
        {
            _stockQtyDao.Delete(Id);
        }

    }
}
