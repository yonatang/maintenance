﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.DataAccess.SparepartInventory;

namespace Chai.Maintenance.Services.SparepartInventory
{
    public class IssueServices
    {
         private IssueDao _issueDao;
        private Issue _issue;
        private IssueItemDetailServices _issuedetailService;
        private ApprovalItemDetailServices _approvaldetailService;
        public IssueServices()
        {
            _issueDao = new IssueDao();
        }

        public void SaveOrUpdateIssue(Issue issue)
        {
            if (issue.Id <= 0)
            {
                _issueDao.Save(issue);
                
            }
            else
            {
                _issueDao.Update(issue);
             
            }
        }

        public Issue GetIssueById(int issueid,int regionId)
        {
            return _issueDao.GetIssueById(issueid, regionId);
        }

        public void DeleteIssue(int issueid)
        {
            _issueDao.Delete(issueid);
        }

        public IList<Issue> GetAllissue(int selectby, string value, string datefrom, string dateto,int regionId)
        {
            return _issueDao.GetIssues(selectby, value, datefrom, dateto, regionId);
        }
        public IList<ApprovalItemDetail> GetListofApprovedItems(int approvedid,int regionId)
        {
            _approvaldetailService = new ApprovalItemDetailServices();
            return _approvaldetailService.GetAllApprovalDetail(approvedid,regionId);
        }
       
    }
}
