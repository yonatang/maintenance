﻿using System;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.DataAccess;

namespace Chai.Maintenance.Services
{
    public class UserLocationServices
    {
        private UserLocationDao _locationdao;

        public UserLocationServices()
        {
            _locationdao = new UserLocationDao();
        }

        public void SaveOrUpdateUserLocation(IList<UserLocation> userlocation,int userid)
        {
            foreach (UserLocation location in userlocation)
            {
                if (location.Id <= 0)
                {
                    _locationdao.Save(location, userid);
                }
                else
                {
                    _locationdao.Update(location);
                }
            }
        }

        public UserLocation GeUserLocationById(int locationid)
        {
            return _locationdao.GetUserLocationById(locationid);
        }

        public void DeleteUserLocation(int userlocationid)
        {
            _locationdao.Delete(userlocationid);
        }

        public IList<UserLocation> GetAllUSerLocations(int findby,int userid,int regionid,int siteid)
        {
            return _locationdao.GetListOfUserLocation(findby, userid, regionid, siteid);
        }
        public IList<UserLocation> GetAllUSerLocationsByUserId(int userid)
        {
            return _locationdao.GetListOfUserLocationbyUserIdLocation(userid);
        }
    }
}
