﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;

namespace Chai.Maintenance.Services.Configuration
{
    public class InstrumentCategoryServices
    {
        private InstrumentCategoryDao _instrumentCategoryDao;
        private InstrumentCategory _manufacturer;


        public InstrumentCategoryServices()
        {
            _instrumentCategoryDao = new InstrumentCategoryDao();
        }
        public void InstrumentCategory()
        {
           
        }
        public void SaveOrUpdateInstrumentCategory(InstrumentCategory instrumentCategory)
        {
            if (instrumentCategory.Id <= 0)
            {
                _instrumentCategoryDao.Save(instrumentCategory);
            }
            else
            {
                _instrumentCategoryDao.Update(instrumentCategory);
            }
        }

        public InstrumentCategory GetInstrumentCategoryById(int instrumentCategoryid)
        {
            return _instrumentCategoryDao.GetInstrumentCategoryById(instrumentCategoryid);
        }

        public void DeleteInstrumentCategory(int instrumentCategoryid)
        {
            _instrumentCategoryDao.Delete(instrumentCategoryid);
        }

        public IList<InstrumentCategory> GetAllInstrumentCategorys(string value)
        {
            return _instrumentCategoryDao.GetListOfInstrumentCategorys(value);
        }
    }
}
