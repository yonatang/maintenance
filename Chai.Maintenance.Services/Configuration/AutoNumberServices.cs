﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;

namespace Chai.Maintenance.Services.Configuration
{
    public class AutoNumberServices
    {
        private AutoNumberDao _autoNumberDao;
        private AutoNumber _manufacturer;


        public AutoNumberServices()
        {
            _autoNumberDao = new AutoNumberDao();
        }
        public void AutoNumber()
        {
          
        }
        public void SaveOrUpdateAutoNumber(AutoNumber autoNumber)
        {
            if (autoNumber.Id <= 0)
            {
                _autoNumberDao.Save(autoNumber);
            }
            else
            {
                _autoNumberDao.Update(autoNumber);
            }
        }

        public AutoNumber GetAutoNumberByPageId(int PageId)
        {
            return _autoNumberDao.GetAutoNumberByPageId(PageId);
        }

        public void DeleteAutoNumber(int autoNumberId)
        {
            _autoNumberDao.Delete(autoNumberId);
        }

        public IList<AutoNumber> GetAllAutoNumbers(int pageId)
        {
            return _autoNumberDao.GetListOfAutoNumber(pageId);
        }
    }
}
