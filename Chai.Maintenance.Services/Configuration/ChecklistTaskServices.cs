﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;

namespace Chai.Maintenance.Services.Configuration
{
    public class ChecklistTaskServices
    {
        private ChecklistTaskDao checklistTaskDao;


        public ChecklistTaskServices()
        {
            checklistTaskDao = new ChecklistTaskDao();
        }
        public void ChecklistTask()
        {
            
        }
        public void SaveOrUpdateChecklistTask(ChecklistTask checklistTask)
        {
            if (checklistTask.Id <= 0)
            {
                checklistTaskDao.Save(checklistTask);
            }
            else
            {
                checklistTaskDao.Update(checklistTask);
            }
        }

        public ChecklistTask GetChecklistTaskById(int checklistTaskId)
        {
            return checklistTaskDao.GetChecklistTaskById(checklistTaskId);
        }

        public void DeleteChecklistTask(int checklistTaskId)
        {
            checklistTaskDao.Delete(checklistTaskId);
        }

        public IList<ChecklistTask> GetAllChecklistTask(string task, int instrumentLookupId)
        {
            return checklistTaskDao.GetListOfChecklistTask(task, instrumentLookupId);
        }
    }
}

