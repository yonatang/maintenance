﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;

namespace Chai.Maintenance.Services.Configuration
{
    public class ConsumableServices
    {
        private ConsumablesDao consumablesDao;


        public ConsumableServices()
        {
            consumablesDao = new ConsumablesDao();
        }
        public void Consumables()
        {
            
        }
        public void SaveOrUpdateConsumables(Consumables consumables)
        {
            if (consumables.Id <= 0)
            {
                consumablesDao.Save(consumables);
            }
            else
            {
                consumablesDao.Update(consumables);
            }
        }

        public Consumables GetConsumablesById(int vendorid)
        {
            return consumablesDao.GetConsumablesById(vendorid);
        }

        public void DeleteConsumables(int consumableId)
        {
            consumablesDao.Delete(consumableId);
        }

        public IList<Consumables> GetAllConsumables(string value)
        {
            return consumablesDao.GetListOfConsumables(value);
        }
    }
}

