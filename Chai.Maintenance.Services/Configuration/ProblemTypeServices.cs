﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;

namespace Chai.Maintenance.Services.Configuration
{
    public class ProblemTypeServices
    {
        private ProblemTypeDao _problemTypeDao;
        private ProblemType _problemType;


        public ProblemTypeServices()
        {
            _problemTypeDao = new ProblemTypeDao();
        }
        public void ProblemType()
        {
          
        }
        public void SaveOrUpdateProblemType(ProblemType problemType)
        {
            if (problemType.Id <= 0)
            {
                _problemTypeDao.Save(problemType);
            }
            else
            {
                _problemTypeDao.Update(problemType);
            }
        }

        public ProblemType GetProblemTypeById(int problemTypeid)
        {
            return _problemTypeDao.GetProblemTypeById(problemTypeid);
        }

        public void DeleteProblemType(int problemTypeid)
        {
            _problemTypeDao.Delete(problemTypeid);
        }

        public IList<ProblemType> GetAllProblemTypes(string value)
        {
            return _problemTypeDao.GetListOfProblemType(value);
        }
    }
}
