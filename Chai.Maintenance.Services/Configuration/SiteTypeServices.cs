﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;

namespace Chai.Maintenance.Services.Configuration
{
    public class SiteTypeServices
    {
        private SiteTypeDao _siteTypeDao;
        private SiteType _siteType;


        public SiteTypeServices()
        {
            _siteTypeDao = new SiteTypeDao();
        }
        public void SiteType()
        {
         
        }
        public void SaveOrUpdateSiteType(SiteType siteType)
        {
            if (siteType.Id <= 0)
            {
                _siteTypeDao.Save(siteType);
            }
            else
            {
                _siteTypeDao.Update(siteType);
            }
        }

        public SiteType GetSiteTypeById(int siteTypeId)
        {
            return _siteTypeDao.GetSiteTypeById(siteTypeId);
        }

        public void DeleteSiteType(int siteTypeId)
        {
            _siteTypeDao.Delete(siteTypeId);
        }

        public IList<SiteType> GetAllSiteTypes(string value)
        {
            return _siteTypeDao.GetListOfSiteType(value);
        }
    }
}
