﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;
namespace Chai.Maintenance.Services.Configuration
{
   public class InstrumentTypeServices
    {
         private InstrumentTypeDao _instrumenttypedao;
         private InstrumentType _region;
        public InstrumentTypeServices()
        {
            _instrumenttypedao = new InstrumentTypeDao();
        }
        public void NewInstrumentType()
        {
           //_region.
        }
        public void SaveOrUpdateInstrumentType(InstrumentType instrumenttype)
        {
            if (instrumenttype.Id <= 0)
            {
                _instrumenttypedao.Save(instrumenttype);                
            }
            else
            {
                _instrumenttypedao.Update(instrumenttype);
            }
        }

        public InstrumentType GetnstrumentTypeById(int instrumenttypeid)
        {
            return _instrumenttypedao.GetInstrumentTypeById(instrumenttypeid);
        }

        public void DeleteInstrumentType(int instrumenttypeid)
        {
            _instrumenttypedao.Delete(instrumenttypeid);
        }

        public IList<InstrumentType> GetAllInstrumentTypes(int findby, string value,int ddlvalue)
        {
            return _instrumenttypedao.GetListOfInstrumentType(findby, value,ddlvalue);
        }
    }
}
