﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;

namespace Chai.Maintenance.Services.Configuration
{
    public class ErrorCodeServices
    {
        private ErrorCodeDao _errorCodeDao;
        private ErrorCode _errorCodee;


        public ErrorCodeServices()
        {
            _errorCodeDao = new ErrorCodeDao();
        }
        public void ErrorCode()
        {

        }
        public void SaveOrUpdateErrorCode(ErrorCode ErrorCode)
        {
            if (ErrorCode.Id <= 0)
            {
                _errorCodeDao.Save(ErrorCode);
            }
            else
            {
                _errorCodeDao.Update(ErrorCode);
            }
        }

        public ErrorCode GetErrorCodeById(int ErrorCodeid)
        {
            return _errorCodeDao.GetErrorCodeById(ErrorCodeid);
        }
        public IList<ErrorCode> GetErrorCodeListByProblemTypeID(int ProblemTypeID)
        {
            return _errorCodeDao.GetErrorCodeListByProblemTypeID(ProblemTypeID);
        }
        
        public void DeleteErrorCode(int ErrorCodeid)
        {
            _errorCodeDao.Delete(ErrorCodeid);
        }

        public IList<ErrorCode> GetAllErrorCodes(string errorCode, int problemType)
        {
            return _errorCodeDao.GetListOfErrorCodes(errorCode, problemType);
        }
    }
}
