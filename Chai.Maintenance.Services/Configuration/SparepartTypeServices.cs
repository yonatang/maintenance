﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;

namespace Chai.Maintenance.Services.Configuration
{
    public class SparepartTypeServices
    {
         private SparepartTypeDao _spareparttypedao;
         private SparepartType _spareparttype;
         public SparepartTypeServices()
        {
            _spareparttypedao = new SparepartTypeDao();
        }
        public void NewSparepartType()
        {
           //_region.
        }
        public void SaveOrUpdateSparepartType(SparepartType sparepart)
        {
            if (sparepart.Id <= 0)
            {
                _spareparttypedao.Save(sparepart);                
            }
            else
            {
                _spareparttypedao.Update(sparepart);
            }
        }

        public SparepartType GetSparepartTypeById(int spareparttypeid)
        {
            return _spareparttypedao.GetSparepartTypeById(spareparttypeid);
        }

        public void DeleteSaprepartType(int regionid)
        {
            _spareparttypedao.Delete(regionid);
        }

        public IList<SparepartType> GetAllSparepartTypes(int findby, string value,int ddlvalue)
        {
            return _spareparttypedao.GetListOfSparepartType(findby, value, ddlvalue);
        }
    }
}
