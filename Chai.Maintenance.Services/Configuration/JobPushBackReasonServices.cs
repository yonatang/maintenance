﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;

namespace Chai.Maintenance.Services.Configuration
{
    public class JobPushBackReasonServices
    {
        private JobPushBackReasonDao _jobPushBackReasonDao;
        private JobPushBackReason _jobPushBackReason;


        public JobPushBackReasonServices()
        {
            _jobPushBackReasonDao = new JobPushBackReasonDao();
        }
        public void JobPushBackReason()
        {
            //_region.
        }
        public void SaveOrUpdateJobPushBackReason(JobPushBackReason jobPushBackReason)
        {
            if (jobPushBackReason.Id <= 0)
            {
                _jobPushBackReasonDao.Save(jobPushBackReason);
            }
            else
            {
                _jobPushBackReasonDao.Update(jobPushBackReason);
            }
        }

        public JobPushBackReason GetJobPushBackReasonById(int jobPushBackReasonId)
        {
            return _jobPushBackReasonDao.GetJobPushBackReasonById(jobPushBackReasonId);
        }

        public void DeleteJobPushBackReason(int jobPushBackReasonId)
        {
            _jobPushBackReasonDao.Delete(jobPushBackReasonId);
        }

        public IList<JobPushBackReason> GetAllJobPushBackReasons(string value)
        {
            return _jobPushBackReasonDao.GetJobPushBackReason(value);
        }
    }
}
