﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;

namespace Chai.Maintenance.Services.Configuration
{
    public class ManufacturerServices
    {
        private ManufacturerDao _manufacturerDao;
        private Manufacturer _manufacturer;


        public ManufacturerServices()
        {
            _manufacturerDao = new ManufacturerDao();
        }
        public void Manufacturer()
        {
            //_region.
        }
        public void SaveOrUpdateManufacturer(Manufacturer manufacturer)
        {
            if (manufacturer.Id <= 0)
            {
                _manufacturerDao.Save(manufacturer);
            }
            else
            {
                _manufacturerDao.Update(manufacturer);
            }
        }

        public Manufacturer GetManufacturerById(int manufacturerid)
        {
            return _manufacturerDao.GetManufacturerById(manufacturerid);
        }

        public void DeleteManufacturer(int manufacturerid)
        {
            _manufacturerDao.Delete(manufacturerid);
        }

        public IList<Manufacturer> GetAllManufacturers(string value)
        {
            return _manufacturerDao.GetListOfManufacturer(value);
        }
    }
}
