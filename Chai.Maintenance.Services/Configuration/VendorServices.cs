﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;

namespace Chai.Maintenance.Services.Configuration
{
    public class VendorServices
    {
        private VendorDao _vendorDao;
        private Vendor _vendor;


        public VendorServices()
        {
            _vendorDao = new VendorDao();
        }
        public void Vendor()
        {
            
        }
        public void SaveOrUpdateVendor(Vendor vendor)
        {
            if (vendor.Id <= 0)
            {
                _vendorDao.Save(vendor);
            }
            else
            {
                _vendorDao.Update(vendor);
            }
        }

        public Vendor GetVendorById(int Vendorid)
        {
            return _vendorDao.GetVendorById(Vendorid);
        }

        public void DeleteVendor(int vendorId)
        {
            _vendorDao.Delete(vendorId);
        }

        public IList<Vendor> GetAllVendor(string value)
        {
            return _vendorDao.GetListOfVendor(value);
        }
    }
}
