﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;
namespace Chai.Maintenance.Services.Configuration
{
    public class RegionServices
    {
         private RegionDao _regiondao;
         private Region _region;
         public RegionServices()
         {
             _regiondao = new RegionDao();
         }
        public void NewRegion()
        {
           //_region.
        }
        public void SaveOrUpdateRegion(Region region)
        {
            if (region.Id <= 0)
            {
                _regiondao.Save(region);                
            }
            else
            {
                _regiondao.Update(region);
            }
        }

        public Region GetRegionById(int regionid)
        {
            return _regiondao.GetRegionById(regionid);
        }

      
        public void DeleteRegion(int regionid)
        {
            _regiondao.Delete(regionid);
        }

        public IList<Region> GetAllRegions(int findby, string value,int regionId)
        {
            return _regiondao.GetListOfRegion(findby, value, regionId);
        }
        public IList<Region> GetAllListOfRegion()
        {
            return _regiondao.GetAllListOfRegion();
        }
    }
}
