﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;

namespace Chai.Maintenance.Services.Configuration
{
    public class UnitOfMeasurmentServices
    {
        private UnitOfMeasurmentDao _unitOfMeasurmentDao;
        private UnitOfMeasurment _unitOfMeasurment;


        public UnitOfMeasurmentServices()
         {
             _unitOfMeasurmentDao = new UnitOfMeasurmentDao();
         }
        public void UnitOfMeasurment()
        {
           //_region.
        }
        public void SaveOrUpdateUnitOfMeasurment(UnitOfMeasurment unitOfMeasurment)
        {
            if (unitOfMeasurment.Id <= 0)
            {
                _unitOfMeasurmentDao.Save(unitOfMeasurment);                
            }
            else
            {
                _unitOfMeasurmentDao.Update(unitOfMeasurment);
            }
        }

        public UnitOfMeasurment GetUnitOfMeasurmentById(int unitOfMeasurmentid)
        {
            return _unitOfMeasurmentDao.GetUnitOfMeasurmentById(unitOfMeasurmentid);
        }

        public void DeleteUnitOfMeasurment(int unitOfMeasurmentid)
        {
            _unitOfMeasurmentDao.Delete(unitOfMeasurmentid);
        }

        public IList<UnitOfMeasurment> GetAllUnitOfMeasurments(string value)
        {
            return _unitOfMeasurmentDao.GetListOfUnitOfMeasurment(value);
        }
    }
}
