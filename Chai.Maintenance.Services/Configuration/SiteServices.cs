﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;
namespace Chai.Maintenance.Services.Configuration
{
   public class SiteServices
    {
        private SiteDao _sitedao;
        private Site _site;
        public SiteServices()
        {
            _sitedao = new SiteDao();
        }
        public void NewSite()
        {
           //_region.
        }
        public void SaveOrUpdateRegion(Site site)
        {
            if (site.Id <= 0)
            {
                _sitedao.Save(site);                
            }
            else
            {
                _sitedao.Update(site);
            }
        }

        public Site GetSiteById(int regionid)
        {
            return _sitedao.GetSiteById(regionid);
        }

        public void DeleteSite(Site site)
        {
             _sitedao.Delete(site);
        }

        public IList<Site> GetAllSites(int findby, string value,int regionId)
        {
            return _sitedao.GetListOfSite(findby, value, regionId);
        }

        public IList<Site> GetSiteByRegionID(int regionId)
        {
           return _sitedao.GetSiteByRegionID(regionId);
        }

        public Site GetRegionIDBySiteId(int siteid)
        {

            return _sitedao.GetRegionIDBySiteId(siteid);
        }
    }
}
