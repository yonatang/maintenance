﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.DataAccess.Configuration;

namespace Chai.Maintenance.Services.Configuration
{
    public class InstrumentServices
    {
      private InstrumentDao _instrumentdao;
      private Instrument _instrument;
        public InstrumentServices()
        {
            _instrumentdao = new InstrumentDao();
        }
        public void NewSite()
        {
           
        }
        public void SaveOrUpdateInstrument(Instrument instrument)
        {
            if (instrument.Id <= 0)
            {
                _instrumentdao.Save(instrument);                
            }
            else
            {
                _instrumentdao.Update(instrument);
            }
        }
        public void SaveTransferedInstrument(Instrument instrument, int OldInstrumentId, int NewSiteId)
        {
            _instrumentdao.SaveTransferedInstrument(instrument, OldInstrumentId, NewSiteId);                
        }
        
        public Instrument GetInstrumentById(int instrumentid)
        {
            return _instrumentdao.GetInstrumentById(instrumentid);
        }

        public void DeleteInstrument(int instrumentid)
        {
            _instrumentdao.Delete(instrumentid);
        }

        public IList<Instrument> GetAllInstruments(int findby, string value,int siteid,int instrumentid,int regionId)
        {
            return _instrumentdao.GetListOfInstrument(findby, value, siteid, instrumentid, regionId);
        }
        public IList<Instrument> GetInstrumentByRSIIG(int regionId,int siteId, int instrumentId, int instrumentCategoryID, int functionality)
        {
            return _instrumentdao.GetInstrumentByRSIIG(regionId, siteId, instrumentId, instrumentCategoryID, functionality);
        }
        public IList<Instrument> GetInstrumentBysiteID(int siteId)
        {
            return _instrumentdao.GetInstrumentBySiteID(siteId);
        }

        public IList<Instrument> GetInstrumentForPreventiveMaint(int regionId, int siteId, int instrumentCategoryId, string LastPrevMainDate)
        {
            return _instrumentdao.GetInstrumentForPreventiveMaint(regionId, siteId, instrumentCategoryId, LastPrevMainDate);
        }
        public IList<Instrument> GetInstrumentForTransfer(int regionId, int siteId, int instrumentCategoryId)
        {
            return _instrumentdao.GetInstrumentForTransfer(regionId, siteId, instrumentCategoryId);
        }
    }
}
