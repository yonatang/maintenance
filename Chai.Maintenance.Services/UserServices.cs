﻿using System;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.DataAccess;

namespace Chai.Maintenance.Services
{
    public class UserServices
    {
        private UserDao _userDao;
        public UserServices()
        {
            _userDao = new UserDao();
        }

        public void SaveOrUpdateUser(User user)
        {
            if (user.Id <= 0)
            {
                if (_userDao.GetUserIdByUsername(user.UserName) != null)
                    throw new Exception("user name alerady exists");
                _userDao.Save(user);
            }
            else
            {
                _userDao.Update(user);
            }
        }
        public void DeleteUser(User user)
        {
            _userDao.Delete(user);
        }

        public User GetUser(int userid)
        {
            return _userDao.GetUserById(userid);
        }

        public User GetUserByUsernameAndPassword(string username, string password)
        {
            return _userDao.GetUserByUsernameAndPassword(username, password);
        }
        public IList<User> GetAllUsers()
        {
            return _userDao.GetListOfAllUsers();
        }

        public IList<User> GetExternalUsers()
        {
            return _userDao.GetExternalUsers();
        }
        public IList<User> GetEnginersList(int regionId)
        {
            return _userDao.GetEnginersList(regionId);
        }
        public IList<User> GetUserList(int regionId)
        {
            return _userDao.GetUserList(regionId);
        }
        public IList<User> SearchUsers(string username)
        {
            return _userDao.SearchUsers(username);
        }
        public IList<User> GetUserBySiteID(int SiteId)
        {
            return _userDao.GetUserBySiteID(SiteId);
        }
        public User GetRequester(int userid)
        {
            return _userDao.GetRequesterById(userid);
        }
        public User GetApprover(int userid)
        {
            return _userDao.GetApproverById(userid);
        }
       
        public IList<User> GetUsers(int regionId)
        {
            return _userDao.GetUsers(regionId);
        }
        
    }
}
