﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Chai.Maintenance.DataAccess.Report;
using System.Collections;

namespace Chai.Maintenance.Services.Report
{
    public class ReportServices
    {
        ReportDao reportdao;
         
        public ReportServices()
         {
             reportdao = new ReportDao();
         }
         public DataSet GetRegionReport()
         
         {
             return reportdao.GetRegionReport();
         }
         public DataSet GetSiteReport(int regionId, int userregionId, int siteTypeId)
         {

             return reportdao.GetSiteReport(regionId, userregionId, siteTypeId);
         }
         public DataSet GetSiteContactReport(int regionId, int userregionId, int siteId)
         {
             return reportdao.GetSiteContactReport(regionId, userregionId, siteId);
         }
         public DataSet GetInstrumentReport(int regionId, int userregionId, int siteId)
         {

             return reportdao.GetInstrumentReport(regionId, userregionId, siteId);
         }
         public DataSet GetUpcomeingPreventiveReport(int regionId, int userregionId, int siteId,int instrumentcatagoryId,DateTime datefrom,DateTime dateto)
         {

             return reportdao.GetUpcomeingPreventiveReport(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
         }
         public DataSet GetUpcomeingCurativeReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
         {

             return reportdao.GetUpcomeingCurativeReport(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
         }
         public DataSet GetTransferReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
         {

             return reportdao.GetTransferReport(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
         }
         public DataSet GetDisposalReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
         {

             return reportdao.GetDisposalReport(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
         }
         public DataSet GetPerformedPreventiveMaintenance(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
         {

             return reportdao.GetPerformedPreventiveMaintenance(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
         }
         public DataSet GetPerformedCurativeMaintenance(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
         {

             return reportdao.GetPerformedCurativeMaintenance(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
         }
         public DataSet GetNonFunctionalInstrumentsBySiteANDInstrumentCategory(int regionId, int userregionId, int siteId, int instrumentcatagoryId)
         {

             return reportdao.GetNonFunctionalInstrumentsBySiteANDInstrumentCategory(regionId, userregionId, siteId, instrumentcatagoryId);
         }
         public DataSet GetInstrumentProblemByManufacturer(int userregionId,int instrumentcatagoryId,int manufactureid)
         {

             return reportdao.GetInstrumentProblemByManufacturer(userregionId, instrumentcatagoryId, manufactureid);
         }
         public DataSet GetQuarterPreventiveReport(int regionId, int userregionId, DateTime datefrom, DateTime dateto)
         {
             return reportdao.GetQuarterPreventiveReport(regionId, userregionId, datefrom, dateto);
         }
         public DataSet GetQuarterCurativeReport(int regionId, int userregionId, DateTime datefrom, DateTime dateto)
         {
             return reportdao.GetQuarterCurativeReport(regionId, userregionId, datefrom, dateto);
         }

         public DataSet GetEscalatedCurativeMaintenances(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
         {
             return reportdao.GetEscalatedCurativeMaintenances(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
         }

         public DataSet GetNotifiedConsumables(int regionId, int userregionId, int siteId, int consumableId)
         {
             return reportdao.GetNotifiedConsumables(regionId, userregionId, siteId, consumableId);
         }

         public DataSet GetNotifiedProblemsChart(DateTime datefrom, DateTime dateto)
         {
             return reportdao.GetNotifiedProblemsChart(datefrom, dateto);

         }
         public DataSet GetMostIssuedSparepartsbyInstrumentsChart(DateTime datefrom, DateTime dateto)
         {
             return reportdao.GetMostIssuedSparepartsbyInstrumentsChart(datefrom, dateto);
         }
         public DataSet GetMostRequestedSparepartsbyInstrumentsChart(DateTime datefrom, DateTime dateto)
         {
             return reportdao.GetMostRequestedSparepartsbyInstrumentsChart(datefrom, dateto);
         }

         public DataSet GetFrequentlyMaintainedInstrumentsChart(DateTime datefrom, DateTime dateto)
         {
             return reportdao.GetFrequentlyMaintainedInstrumentsChart(datefrom, dateto);
         }
         public DataSet GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(DateTime datefrom, DateTime dateto)
         {
             return reportdao.GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(datefrom, dateto);
         }
         public DataSet GetRFrequentlyReportedProblemTypesChart(DateTime datefrom, DateTime dateto)
         {
             return reportdao.GetRFrequentlyReportedProblemTypesChart(datefrom, dateto);
         }

         public DataSet GetInstrumentsUnderContract(int regionId, int userRegionId, int siteId)
         {
             return reportdao.GetInstrumentsUnderContract(regionId, userRegionId, siteId);
         }

         public DataSet GetCurativeMaintenanceTracking(int regionId, int userRegionId, int siteId)
         {
             return reportdao.GetCurativeMaintenanceTracking(regionId, userRegionId, siteId);
         }
         public IList GetMapStatisticReport()
         {
             return reportdao.GetMapStatisticReport();
         }
         public IList GetNotifiedProblemsChart(string datefrom, string dateto)
         {
             return reportdao.GetNotifiedProblemsChart(datefrom, dateto);

         }
        
         public IList GetFrequentlyMaintainedInstrumentsChart(string datefrom, string dateto)
         {
             return reportdao.GetFrequentlyMaintainedInstrumentsChart(datefrom, dateto);
         }
         public IList GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(string datefrom, string dateto)
         {
             return reportdao.GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(datefrom, dateto);
         }
         public IList GetRFrequentlyReportedProblemTypesChart(string datefrom, string dateto)
         {
             return reportdao.GetRFrequentlyReportedProblemTypesChart(datefrom, dateto);
         }

    }
   
}
