﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.DBConnection;
using Chai.Maintenance.DataAccess.Admin;
using Chai.Maintenance.Enums;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.Services.Admin
{
    public class NodeServices
    {
        private NodeDao _nodeDao;
        public NodeServices()
        {
            _nodeDao = new NodeDao();
        }
        public Node GetNodeById(int nodeid)
        {
            return _nodeDao.GetNodeById(nodeid);
        }

        public void SaveNode(Node node)
        {
            if (node == null)
                throw new Exception("");

            SqlTransaction sqltransaction = ConnectionManager.GetInstance().GetSqlTransaction();
            try
            {
                _nodeDao.Save(node, sqltransaction);
                sqltransaction.Commit();
            }
            catch (Exception ex)
            {
                sqltransaction.Rollback();
                throw ex;
            }
        }

        public void UpdateNode(Node node)
        {
            SqlTransaction sqltransaction = ConnectionManager.GetInstance().GetSqlTransaction();
            try
            {
                _nodeDao.Update(node, sqltransaction);
                sqltransaction.Commit();
            }
            catch (Exception)
            {
                sqltransaction.Rollback();
                throw;
            }
        }

        public void DeleteNode(Node node)
        {
            _nodeDao.Delete(node);
        }

        public IList<Node> GetListOfAllNodes()
        {
            return _nodeDao.GetListOfAllNodes();
        }

        public IList<Node> GetListOfNodes(PageViewType vtype)
        {
            return _nodeDao.GetListOfNodes(vtype);
        }
    }
}
