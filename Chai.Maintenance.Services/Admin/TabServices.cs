﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.DBConnection;
using Chai.Maintenance.DataAccess.Admin;
using Chai.Maintenance.Enums;
using Chai.Maintenance.CoreDomain;


namespace Chai.Maintenance.Services.Admin
{
    public class TabServices
    {
        private TabDao _tabDao;

        public TabServices()
        {
            _tabDao = new TabDao();
        }

        public MenuTab GetMenuTab(TabType ttype)
        {
            return _tabDao.GetMenuTab(ttype);
        }
        
        public MenuTab GetMenuTab(int tabid)
        {
            return _tabDao.GetMenuTab(tabid);
        }
        
        public void SaveOrUpdateMenuTab(MenuTab mtab)
        {
            SqlTransaction sqltransaction = ConnectionManager.GetInstance().GetSqlTransaction();
            try
            {
                _tabDao.SaveOrUpdateMenuTab(mtab, sqltransaction);
                sqltransaction.Commit();
            }
            catch (Exception)
            {
                sqltransaction.Rollback();
                throw;
            }
        }

        public TabNode GetTabNodeById(int tnodeid)
        {
            return _tabDao.GetTabNodeById(tnodeid);
        }

        public void SaveOrUpdateTabNode(TabNode tnode)
        {
            if (tnode == null)
                throw new Exception("");

            //todo: validation

            if (tnode.IsNew())
            {
                AddTabNode(tnode);
            }
            else
            {
                UpdateTabNode(tnode);
            }

        }

        private void AddTabNode(TabNode tnode)
        {
            SqlTransaction sqltransaction = ConnectionManager.GetInstance().GetSqlTransaction();
            try
            {
                _tabDao.Save(tnode, sqltransaction);
                sqltransaction.Commit();
            }
            catch (Exception)
            {
                sqltransaction.Rollback();
                throw;
            }
        }

        private void UpdateTabNode(TabNode tnode)
        {
            SqlTransaction sqltransaction = ConnectionManager.GetInstance().GetSqlTransaction();
            try
            {
                _tabDao.Update(tnode, sqltransaction);
                sqltransaction.Commit();
            }
            catch (Exception)
            {
                sqltransaction.Rollback();
                throw;
            }
        }

        public void DeleteTabNode(TabNode tnode)
        {
            SqlTransaction sqltransaction = ConnectionManager.GetInstance().GetSqlTransaction();
            try
            {
                _tabDao.Delete(tnode, sqltransaction);
                sqltransaction.Commit();
            }
            catch (Exception)
            {
                sqltransaction.Rollback();
                throw;
            }
        }

        public IList<TabNode> GetListOfAllTabNodes(TabType ttype, MenuSectionType section)
        {
            return _tabDao.GetListOfTabNode(ttype, section);
        }
    }
}
