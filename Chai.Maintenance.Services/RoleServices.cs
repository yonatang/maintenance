﻿using System;
using System.Collections.Generic;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.DataAccess;

namespace Chai.Maintenance.Services
{
    public class RoleServices
    {
        private RoleDao _roledao;
        
        public RoleServices()
        {
            _roledao = new RoleDao();
        }

        public void SaveOrUpdateRole(Role role)
        {
            if (role.Id <= 0)
            {
                _roledao.Save(role);                
            }
            else
            {
                _roledao.Update(role);
            }
        }

        public Role GetRoleById(int roleid)
        {
            return _roledao.GetRoleById(roleid);
        }

        public void DeleteRole(Role role)
        {
            _roledao.Delete(role);
        }

        public IList<Role> GetAllRoles()
        {
            return _roledao.GetListOfRole();
        }
    }
}
