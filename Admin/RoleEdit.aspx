﻿
<%@ page language="C#" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Admin.Views.RoleEdit, App_Web_mefkcnti" title="RoleEdit" masterpagefile="~/Shared/AdminMaster.master" stylesheettheme="Default" %>
    <%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
 
<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" Runat="Server">
            <div class="group">
				<h4>General</h4>
				<table>
					<tr>
						<td style="WIDTH: 120px">Name</td>
						<td><asp:textbox id="txtName" runat="server" width="200px"></asp:textbox>
							<asp:requiredfieldvalidator id="rfvName" runat="server" errormessage="Name is required" cssclass="validator" display="Dynamic" enableclientscript="False" controltovalidate="txtName"></asp:requiredfieldvalidator>
						</td>
					</tr>
					<tr>
						<td>Permissions</td>
						<td><asp:checkboxlist id="cblRoles" runat="server" repeatlayout="Flow"></asp:checkboxlist>

						</td>
					</tr>
				</table>
			</div>
			<br/>
			<div>
			<asp:button id="btnSave" runat="server" text="Save" onclick="btnSave_Click"></asp:button>
			<asp:Button id="btnCancel" runat="server" Text="Cancel" causesvalidation="false" onclick="btnCancel_Click"></asp:Button>
			<asp:Button id="btnDelete" runat="server" Text="Delete" causesvalidation="false" 
                    onclick="btnDelete_Click"></asp:Button>
			</div>
</asp:Content>
