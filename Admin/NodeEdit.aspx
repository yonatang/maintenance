﻿<%@ page language="C#" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Admin.Views.NodeEdit, App_Web_mefkcnti" title="NodeEdit" masterpagefile="~/Shared/AdminMaster.master" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" runat="Server">

    <p>
        Manage the properties of the node (page). Use the buttons on the bottom of the page
        to save or delete the page.</p>
    <div class="group">
        <h4>
            General</h4>
        <table>
            <tr>
                <td style="width: 100px">
                    ASPX Page</td>
                <td>
                    <asp:DropDownList ID="ddlAspxpage" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Category</td>
                <td>
                    <asp:DropDownList ID="ddlCategory" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Node title
                </td>
                <td>
                    <asp:TextBox ID="txtTitle" runat="server" Width="300px"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfvTitle" runat="server" ErrorMessage="Title is required" Display="Dynamic"
                        CssClass="validator" ControlToValidate="txtTitle" EnableClientScript="False"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Description</td>
                <td>
                    <asp:TextBox ID="txtDescription" runat="server" Width="300px" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Folder Path</td>
                <td>
                    <asp:TextBox ID="txtFolderpath" runat="server" Width="300px"></asp:TextBox>
                    <asp:RequiredFieldValidator
                        ID="rfvTitle0" runat="server" ErrorMessage="Title is required" Display="Dynamic"
                        CssClass="validator" ControlToValidate="txtFolderpath" 
                        EnableClientScript="False"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Image path</td>
                <td>
                    <asp:TextBox ID="txtImagePath" runat="server" Width="300px" ></asp:TextBox>
                </td>
            </tr>
            </table>
    </div>
    <div class="group">
        <h4>
            Authorization</h4>
        <table class="tbl">
            <asp:Repeater ID="rptRoles" runat="server" 
                onitemdatabound="rptRoles_ItemDataBound">
                <HeaderTemplate>
                    <tr>
                        <th>
                            Role
                        </th>
                        <th>
                            View allowed
                        </th>
                        <th>
                            Edit allowed
                        </th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%# DataBinder.Eval(Container.DataItem, "Name") %>
                        </td>
                        <td style="text-align: center">
                            <asp:CheckBox ID="chkViewAllowed" runat="server"></asp:CheckBox>
                        </td>
                        <td style="text-align: center">
                            <asp:CheckBox ID="chkEditAllowed" runat="server"></asp:CheckBox>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
        </div>
    <div>
        <asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click"></asp:Button>
        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" 
            Text="Cancel" onclick="btnCancel_Click"></asp:Button>
        <asp:Button ID="btnDelete" runat="server" CausesValidation="False" 
            Text="Delete" onclick="btnDelete_Click" Enabled="False"></asp:Button>
        <cc1:ConfirmButtonExtender ID="btnDelete_ConfirmButtonExtender" runat="server" 
            ConfirmText="Are you sure" Enabled="True" TargetControlID="btnDelete">
        </cc1:ConfirmButtonExtender>
   </div>
</asp:Content>
