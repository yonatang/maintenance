﻿
<%@ page language="C#" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Admin.Views.Roles, App_Web_mefkcnti" title="Roles" masterpagefile="~/Shared/AdminMaster.master" stylesheettheme="Default" %>
   <%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" Runat="Server">
        <table class="tbl" width="100%">
				<asp:repeater id="rptRoles" runat="server" 
                    onitemdatabound="rptRoles_ItemDataBound">
					<headertemplate>
						<tr>
						    <th></th>
							<th>Role name</th>
							<th>Permissionlevel(s)</th>
							<th></th>
						</tr>
					</headertemplate>
					<itemtemplate>
						<tr>
						    <td><asp:Image Width="14" Height="12" ImageAlign="Middle" runat="server" ID="imgRole" /></td>
							<td><%# DataBinder.Eval(Container.DataItem, "Name") %></td>
							<td><asp:label id="lblPermissions" runat="server"></asp:label></td>
							<td>
								<asp:hyperlink id="hplEdit" runat="server">Edit</asp:hyperlink>
							</td>
						</tr>
					</itemtemplate>
				</asp:repeater>
			</table>
			<br/>
			<div>
				<asp:button id="btnNew" runat="server" text="Add new role" 
                    onclick="btnNew_Click"></asp:button>
			</div>
</asp:Content>
