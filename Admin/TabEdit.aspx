﻿
<%@ page language="C#" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Admin.Views.TabEdit, App_Web_mefkcnti" title="TabEdit" masterpagefile="~/Shared/AdminMaster.master" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" Runat="Server">
		<h1>TabEdit</h1>
        <br />
        <table style="width: 100%">
            <tr>
                <td colspan="2">
                    Action Section</td>
                <td>
                    &nbsp;</td>
                <td colspan="2">
                    Find Section</td>
                <td>
                    &nbsp;</td>
                <td>
                    Dropdown Section</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="ddlAction" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="butAddaction" runat="server" Text="Add" 
                        onclick="butAddaction_Click" />
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddlFind" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="butAddfind" runat="server" Text="Add" 
                        onclick="butAddfind_Click" />
                </td>
                <td>
                    <asp:DropDownList ID="ddlDropdown" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="butAdddropdown" runat="server" Text="Add" 
                        onclick="butAdddropdown_Click" />
                </td>
            </tr>
             <tr>
                <td rowspan="2" style="width: 205px">
                    <asp:ListBox ID="ltbAction" runat="server" Height="350px" Width="200px">
                    </asp:ListBox>
                    </td>
                <td style="height: 9px" valign="bottom">
                    &nbsp;</td>
                <td rowspan="2">
                    <asp:Button ID="butRemoveaction" runat="server" onclick="butRemoveaction_Click" 
                        Text="Remove" Width="47px" />
                    </td>
                <td rowspan="2" style="width: 19px">
                    <asp:ListBox ID="ltbFind" runat="server" Height="350px" Width="200px">
                    </asp:ListBox>
                    </td>
                <td valign="bottom">
                    &nbsp;</td>
                <td rowspan="2">
                    <asp:Button ID="butRemovefind" runat="server" onclick="butRemovefind_Click" 
                        Text="Remove" Width="47px" />
                    </td>
                <td rowspan="2">
                    <asp:ListBox ID="ltbDropdown" runat="server" Height="350px" Width="200px">
                    </asp:ListBox>
                    </td>
                <td rowspan="2">
                    <asp:Button ID="butRemovedropdown" runat="server" 
                        onclick="butRemovedropdown_Click" Text="Remove" Width="47px" />
                    </td>
            </tr>
             <tr>
                <td valign="top">
                    &nbsp;</td>
                <td valign="top">
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>
