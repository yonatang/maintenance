﻿<%@ control language="C#" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Admin.Views.Navigation, App_Web_gqopnwqf" %>


 <style>
    .portlet-content li
    {
          border-bottom: 1px dashed #BBBBBB;
    margin: 0 0px 0 0px;
    padding: 5px 0;
   list-style: none outside none;
    }
    </style>

<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
    <div class="portlet-header ui-widget-header ui-corner-top">
		Tabs
    </div>
	<div class="portlet-content">
    <ul>
		<li></li><asp:hyperlink id="hplMaintenance" runat="server">Maintenance</asp:hyperlink><br />
		<asp:hyperlink id="hplSparepartInventory" runat="server">SparepartInventory</asp:hyperlink><br />
		<asp:hyperlink id="hplConfiguration" runat="server">Configuration</asp:hyperlink><br />
		<asp:hyperlink id="hplReports" runat="server">Report</asp:hyperlink><br />
     
		<asp:hyperlink id="hplHelp" runat="server">Help</asp:hyperlink><br />
    </ul>
    </div>
</div>












<br/> 
<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"> 
<div class="portlet-header ui-widget-header ui-corner-top">Nodes</div>
<div class="portlet-content">
	<ul>
       <li></li><asp:hyperlink id="hplNodes" navigateurl="../Nodes.aspx" runat="server">Manage Nodes</asp:hyperlink>
	</ul>
    </div>
    </div>
    <br/>


<br/>

<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"> 
<div class="portlet-header ui-widget-header ui-corner-top">Roles</div>
<div class="portlet-content">
	<ul>
       <li></li>
	<asp:hyperlink id="hplRoles" navigateurl="../Roles.aspx" runat="server">Manage roles</asp:hyperlink>
    </ul>
</div>
</div>

<br/>

<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"> 
<div class="portlet-header ui-widget-header ui-corner-top">Users</div>
<div class="portlet-content">
	<ul>
       <li></li><asp:hyperlink id="hplUsers" navigateurl="../Users.aspx" runat="server">Manage users</asp:hyperlink>
       </ul>
       </div>
</div><br />



