﻿<%@ page language="C#" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Admin.Views.Users, App_Web_mefkcnti" title="Users" masterpagefile="~/Shared/AdminMaster.master" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" runat="Server">
    <div class="group">
        <h4>
            Find users</h4>
        Username
        <asp:TextBox ID="txtUsername" runat="server" ></asp:TextBox>
        <asp:Button ID="btnFind" runat="server" Text="Find" onclick="btnFind_Click"></asp:Button>
    </div>
    <asp:Panel ID="pnlResults" runat="server" CssClass="group">
        <h4>
            Search results</h4>
<asp:GridView ID="grvUser" runat="server" AllowPaging="True" 
            AutoGenerateColumns="False" CellPadding="3" ForeColor="#333333" 
            GridLines="Horizontal" Width="100%"  OnRowDataBound="grvUser_RowDataBound"          
             onpageindexchanging="grvUser_PageIndexChanging"  PageSize="10">
            <PagerSettings FirstPageImageUrl="~/Images/arrow_beg.gif" 
                LastPageImageUrl="~/Images/arrow_end.gif" 
                NextPageImageUrl="~/Images/arrow_right.gif" 
                PreviousPageImageUrl="~/Images/arrow_left.gif" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <Columns>
                <asp:TemplateField HeaderText="Username">
                <ItemTemplate>                
                <asp:Label ID="lblUsername" runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"Username") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Firstname">
                <ItemTemplate>                
                <asp:Label ID="lblFirstname" runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"firstname") %>'></asp:Label>
                </ItemTemplate>                
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lastname">
                <ItemTemplate>                
                <asp:Label ID="lblLastname" runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"lastname") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Email">
                <ItemTemplate>                
                <asp:Label ID="lblEmail" runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"email") %>'></asp:Label>
                </ItemTemplate>                
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Last login date">
                <ItemTemplate>                
                <asp:Label ID="lblLastlogin" runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"LastLogin") %>'></asp:Label>
                </ItemTemplate>                
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Last login from">
                <ItemTemplate>                
                <asp:Label ID="lblloginfrom" runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"LastIp") %>'></asp:Label>
                </ItemTemplate>                
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status">
                <ItemTemplate>               
                <asp:Label ID="lblStatus" runat ="server" Text ='<%# DataBinder.Eval(Container.DataItem,"IsActive") %>'></asp:Label>
                </ItemTemplate>                
                </asp:TemplateField>
                <asp:TemplateField>
                <ItemTemplate>
                <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    </asp:Panel>
    <div>
        <asp:Button ID="btnNew" runat="server" Text="Add new user" 
            onclick="btnNew_Click"></asp:Button>
    </div>
</asp:Content>
