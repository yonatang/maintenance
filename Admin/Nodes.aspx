﻿<%@ page language="C#" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Admin.Views.Nodes, App_Web_mefkcnti" title="Nodes" masterpagefile="~/Shared/AdminMaster.master" stylesheettheme="Default" %>

<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" runat="Server">
       <h3>Nodes</h3>
        <table width="100%">
            <tr>
                <td width="145">
                    <asp:DropDownList ID="ddlViewtype" runat="server" Width="100px">
       </asp:DropDownList>
       <asp:Button ID="butFiliter" runat="server" Text="List" /></td>
                <td>
                    <asp:HyperLink ID="hplNewnode" runat="server">Register New Node</asp:HyperLink>
                </td>
            </tr>
       </table>
        <asp:GridView ID="grvNodes" runat="server" AutoGenerateColumns="False"
            CellPadding="3" ForeColor="#333333" GridLines="Horizontal" Width="100%" 
           OnRowDataBound="grvNodes_RowDataBound" PageSize="10">
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <Columns>
                <asp:TemplateField HeaderText="Folder">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem,"FolderPath") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Node Title">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem,"Title") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Categorys">
                    <ItemTemplate>
                       <%# DataBinder.Eval(Container.DataItem,"ViewType") %>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Image">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem,"ImagePath") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem,"Description") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>

</asp:Content>
