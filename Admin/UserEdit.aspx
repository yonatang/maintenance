﻿<%@ page language="C#" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Admin.Views.UserEdit, App_Web_mefkcnti" title="UserEdit" masterpagefile="~/Shared/AdminMaster.master" stylesheettheme="Default" %>

<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" runat="Server">

    <div class="group">
        <h4>
            General</h4>
        <table>
            <tr>
                <td style="width: 174px">
                    User name
                </td>
                <td>
                    <asp:TextBox ID="txtUsername" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ControlToValidate="txtUsername"
                        Display="Dynamic" ErrorMessage="User name is required" 
                        SetFocusOnError="True" ValidationGroup="1"></asp:RequiredFieldValidator>
                    <cc1:ValidatorCalloutExtender ID="rfvUsername_ValidatorCalloutExtender"
                        runat="server" Enabled="True" TargetControlID="rfvUsername" Width="300px">
                    </cc1:ValidatorCalloutExtender>
                    <asp:Label ID="lblUsername" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 174px">
                    Is External User</td>
                <td>
                    <asp:CheckBox ID="ChkIsExternalUser" runat="server" AutoPostBack="True" />
                </td>
            </tr>
            <tr>
                <td style="width: 174px">
                    <asp:Label ID="lblVendor" runat="server" Text="Vendor" Visible="False"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlVendor" runat="server" CausesValidation="True" 
                        Visible="False" Width="200px">
                        <asp:ListItem Value="0">Select Vendor</asp:ListItem>
                        <asp:ListItem Value="1">Mosea</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RfvVendor" runat="server" 
                        ControlToValidate="ddlVendor" Display="Dynamic" ErrorMessage="Vendor Required" 
                        InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 174px">
                    <asp:Label ID="lblFirstname" runat="server" Text="First name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFirstname" runat="server" Width="200px" 
                        CausesValidation="True"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RfvFirstname" runat="server" 
                        ControlToValidate="txtFirstname" Display="Dynamic" 
                        ErrorMessage="First Name Required" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 174px">
                    &nbsp;<asp:Label ID="lblLastname" runat="server" Text="Last name"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtLastname" runat="server" Width="200px" 
                        CausesValidation="True"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RfvLastname" runat="server" 
                        ControlToValidate="txtLastname" Display="Dynamic" 
                        ErrorMessage="Last Name Required" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 174px">
                    &nbsp;<asp:Label ID="lblRegion" runat="server" Text="Region"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                        AutoPostBack="True" CausesValidation="True" DataTextField="RegionName" 
                        DataValueField="Id" onselectedindexchanged="ddlRegion_SelectedIndexChanged" 
                        Width="200px">
                        <asp:ListItem Value="-1">Select Region</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvRegion" runat="server" 
                        ControlToValidate="ddlRegion" Display="Dynamic" ErrorMessage="Region Required" 
                        InitialValue="-1" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 174px">
                    &nbsp;<asp:Label ID="lblSite" runat="server" Text="Site"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                        DataTextField="Name" DataValueField="Id" Width="200px">
                        <asp:ListItem Value="0">Select Site</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 174px">
                    Email
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" Width="200px"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                        Display="Dynamic" ErrorMessage="Invalid Email Address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        SetFocusOnError="True" ValidationGroup="1"></asp:RegularExpressionValidator>
                    <cc1:ValidatorCalloutExtender ID="RegularExpressionValidator1_ValidatorCalloutExtender"
                        runat="server" Enabled="True" TargetControlID="RegularExpressionValidator1" Width="300px">
                    </cc1:ValidatorCalloutExtender>
                </td>
            </tr>
            <tr>
                <td style="width: 174px">
                    Active
                </td>
                <td>
                    <asp:CheckBox ID="chkActive" runat="server" Checked="True"></asp:CheckBox>
                </td>
            </tr>
            <tr>
                <td style="width: 174px; height: 25px;">
                    Password
                </td>
                <td style="height: 25px">
                    <asp:TextBox ID="txtPassword1" type="password" runat="server" TextMode="Password"
                        Width="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 174px; height: 25px;">
                    Confirm password
                </td>
                <td style="height: 25px">
                    <asp:TextBox ID="txtPassword2" type="password" runat="server" TextMode="Password"
                        Width="200px"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword1"
                        ControlToValidate="txtPassword2" Display="Dynamic" ErrorMessage="The password you typed donot match. Please retype the password"
                        SetFocusOnError="True" ValidationGroup="1"></asp:CompareValidator>
                    <cc1:ValidatorCalloutExtender ID="CompareValidator1_ValidatorCalloutExtender" runat="server"
                        Enabled="True" TargetControlID="CompareValidator1" Width="300px">
                    </cc1:ValidatorCalloutExtender>
                </td>
            </tr>
        </table>
    </div>
    <div class="group">
        <h4>
            Roles</h4>
        <table class="tbl">
            <asp:Repeater ID="rptRoles" runat="server">
                <HeaderTemplate>
                    <tr>
                        <th>
                            Role
                        </th>
                        <th>
                        </th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%# DataBinder.Eval(Container.DataItem, "Name") %>
                        </td>
                        <td style="text-align: center">
                            <asp:CheckBox ID="chkRole" runat="server"></asp:CheckBox>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    <div>
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" 
            ValidationGroup="1" CommandName="Save"></asp:Button>
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="False"
            OnClick="btnCancel_Click"></asp:Button>
        <asp:Button ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click"  Visible="False"></asp:Button>
    </div>
</asp:Content>
