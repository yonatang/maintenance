using System;
using System.Collections.Generic;
//using System.Threading;
using System.Web;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.CompositeWeb.Utility;
using Microsoft.Practices.ObjectBuilder;

using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services.Admin;
using Chai.Maintenance.Shared.Navigation;
using Chai.Maintenance.Services;


namespace Chai.Maintenance.Modules.Admin
{
    public class AdminController
    {
        private IHttpContextLocatorService _httpContextLocatorService;
        private INavigationService _navigationService;
        private RoleServices _roleServices;
        private ISessionStateLocatorService _sessionstate;
        private UserLocationServices _userlocationservices;
        //private NodeServices _nodeServices;
        private UserServices _userservices;
        [InjectionConstructor]
        public AdminController([ServiceDependency] IHttpContextLocatorService httpContextLocatorService,
            [ServiceDependency] INavigationService navigationService, [ServiceDependency] ISessionStateLocatorService sessionstate)
        {
            _httpContextLocatorService = httpContextLocatorService;
            _navigationService = navigationService;
            _sessionstate = sessionstate;
        }

        public User CurrentUser
        {
            get { return _httpContextLocatorService.GetCurrentContext().User.Identity as User; }
        }

        public void Navigate(string to)
        {
            _navigationService.Navigate(to);
        }
        public User GetUser
        { 
            get{
                User _user = _sessionstate.GetSessionState()["User"] as User;
                    if(_user !=null)
                return _user;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["User"] = value;
            }
        }
        public User GetRequester
        {
            get
            {
                User _user = _sessionstate.GetSessionState()["User"] as User;
                if (_user != null)
                    return _user;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["User"] = value;
            }
        }
        public User GetApprover
        {
            get
            {
                User _user = _sessionstate.GetSessionState()["User"] as User;
                if (_user != null)
                    return _user;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["User"] = value;
            }
        }
        public IList<Role> GetRoles
        {
            get
            {
                if (_roleServices == null)
                    _roleServices = new RoleServices();

                return _roleServices.GetAllRoles();
            }
        }

        public Role GetRoleById(int roleid)
        {
            if (_roleServices == null)
                _roleServices = new RoleServices();

            return _roleServices.GetRoleById(roleid);
        }
        public void DeleteRole(Role role)
        {
            if (_roleServices == null)
                _roleServices = new RoleServices();
            _roleServices.DeleteRole(role);
        }

        public void SaveOrUpdateRole(Role role)
        {
            if (_roleServices == null)
                _roleServices = new RoleServices();
            _roleServices.SaveOrUpdateRole(role);
        }
        public User GetRequesterByUserId(int userId)
        {
            if (_userservices == null)
                _userservices = new UserServices();
            return _userservices.GetRequester(userId);
        }
        public void GetApproverByUserId(int userId)
        {
            if (_userservices == null)
                _userservices = new UserServices();
            this.GetApprover = _userservices.GetApprover(userId);
        }
        public IList<User> GetUsers(int regionId)
        {
            if (_userservices == null)
                _userservices = new UserServices();
           return _userservices.GetUsers(regionId);
        }
        public User GetUserListByIDForTransfer(int userId)
        {
            if (_userservices == null)
                _userservices = new UserServices();
            return _userservices.GetUser(userId);
        }
        
        public IList<User> GetUserBySiteID(int SiteId)
        {
            if (_userservices == null)
                _userservices = new UserServices();
            return _userservices.GetUserBySiteID(SiteId);
        }

        public IList<User> GetUserList( int regionId)
        {
            if (_userservices == null)
                _userservices = new UserServices();
            return _userservices.GetUserList(regionId);
        }
        public IList<User> GetEnginersList(int regionId)
        {
            if (_userservices == null)
                _userservices = new UserServices();
            return _userservices.GetEnginersList(regionId);
        }
        public IList<User> GetExternalUsers()
        {
            if(_userservices == null)
                _userservices = new UserServices();
            return _userservices.GetExternalUsers();
        }
        #region UserLocation
       
        public IList<UserLocation> UserLocationObject
        {
            get
            {
                IList<UserLocation> _userlocation = _sessionstate.GetSessionState()["UserLocation"] as IList<UserLocation>;

                if (_userlocation != null)
                    return _userlocation;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["UserLocation"] = value;

            }
        }


        public IList<UserLocation> GetUserLocations(int findby,int Userid,int regionid,int siteid)
        {

            if (_userlocationservices == null)
                _userlocationservices = new UserLocationServices();

            return _userlocationservices.GetAllUSerLocations(findby, Userid, regionid, siteid);

        }
        public IList<UserLocation> GetUserLocationByUserId(int Userid)
        {

            if (_userlocationservices == null)
                _userlocationservices = new UserLocationServices();

            return _userlocationservices.GetAllUSerLocationsByUserId(Userid);

        }
        public void GetUserLocationById(int locationid)
        {
            if (_userlocationservices == null)
                _userlocationservices = new UserLocationServices();

           // this.UserLocationObject = _userlocationservices.GeUserLocationById(locationid);
        }

        public void SaveOrUpdateUserLocation(IList<UserLocation> userlocation,int userid)
        {

            if (_userlocationservices == null)
                _userlocationservices = new UserLocationServices();
            _userlocationservices.SaveOrUpdateUserLocation(userlocation, userid);
            this.UserLocationObject = userlocation;
        }
        public void NewRegion()
        {
            //UserLocation userlocation = new UserLocation();
            //this.UserLocationObject = userlocation;
        }
        public void DeleteUserLocation(int locationid)
        {
            if (_userlocationservices == null)
                _userlocationservices = new UserLocationServices();
            _userlocationservices.DeleteUserLocation(locationid);
        }
        #endregion
       
    }
}
