﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration;
namespace Chai.Maintenance.Modules.Admin.Views
{
    public class UserEditPresenter : Presenter<IUserEditView>
    {

        private AdminController _controller;
        private User _user;
        private ConfigurationController _concontroller;
        private UserServices _userService;

        public UserEditPresenter([CreateNew] AdminController controller, [CreateNew] ConfigurationController Concontroller)
        {
            _controller = controller;
            _concontroller = Concontroller;
            _userService = new UserServices();

        }

        public override void OnViewLoaded()
        {
            
        }

        public override void OnViewInitialized()
        {
           
        }

        public User CurrentUser
        {
            get 
            {
                if (_user == null)
                {
                    int id = int.Parse(View.GetUserId);
                    if (id > 0)
                        _user = _userService.GetUser(id);
                    else
                        _user = new User();
                }
                return _user; }
        }

        public Role GetRoleById(int roleid)
        {
            return _controller.GetRoleById(roleid);
        }
        public IList<Role> GetRoles()
        {
            return _controller.GetRoles;
        }

        public void SaveOrUpdateUser()       
        {
            User user = CurrentUser;

            if (user.IsNew())
                user.UserName = View.GetUserName;
            
            user.FirstName = View.GetFirstName;
            user.LastName = View.GetLastName;
            user.Email = View.GetEmail;
            user.IsActive = View.GetIsActive;
            user.DateModified = DateTime.Now;
            user.RegionId = View.RegionId;
            user.SiteId = View.SiteId;
            user.IsExternalUser = View.IsExternaUser;
            user.VendorId = View.VendorId;
            //user.UserLocations = View.userlocation;
            if (View.GetPassword.Length > 0)
            {
                try
                {
                    user.Password = User.HashPassword(View.GetPassword);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            if (user.IsNew() && user.Password == null)
                throw new Exception("Password is required");

            _userService.SaveOrUpdateUser(user);
            //SaveorUpdateUserLocation(CurrentUser.Id);
        }
        //public void SaveorUpdateUserLocation(int userId)
        //{
        //    _controller.SaveOrUpdateUserLocation(View.userlocation,userId);
        //}
        public void DeleteUser()
        {
            if (!CurrentUser.IsNew())
                _userService.DeleteUser(CurrentUser);
        }
        public IList<Region> GetRegion()
        {
           return _concontroller.GetRegions(0, "",0);
        }
        public IList<Region> GetAllListOfRegion()
        {
            return _concontroller.GetAllListOfRegion();
        }
        public IList<Site> GetSite(int RegionId)
        {
          return  _concontroller.GetSiteByRegionID(View.RegionId);
        }
        public IList<Region> GetRegions()
        {
            return _concontroller.GetRegions(0, string.Empty, _controller.CurrentUser.RegionId);
        }
        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Admin/Users.aspx?{0}=0", AppConstants.TABID));
        }
        public void RedirectPage(string url)
        {
            _controller.Navigate(url);
        }
    }
}




