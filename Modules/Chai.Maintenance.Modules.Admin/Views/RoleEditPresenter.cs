﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services.Admin;
using Chai.Maintenance.Shared;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public class RoleEditPresenter : Presenter<IRoleEditView>
    {
        private AdminController _controller;
        private Role _role;
        
        public RoleEditPresenter([CreateNew] AdminController controller)
        {
            _controller = controller;
        }

        public override void OnViewLoaded()
        {
            // TODO: Implement code that will be executed every time the view loads
        }

        public override void OnViewInitialized()
        {
            // TODO: Implement code that will be executed the first time the view loads
        }

        public Role CurrentRole
        {
            get
            {
                if (_role == null)
                {
                    int id = int.Parse(View.GetRoleId);
                    if (id > 0)
                        _role = _controller.GetRoleById(id);
                    else
                        _role = new Role();
                }
                return _role;
            }
        }
        public void DeleteRole()
        {
            _controller.DeleteRole(CurrentRole);
        }
        public void SaveOrUpdateRole()
        {
            _controller.SaveOrUpdateRole(CurrentRole);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Admin/Roles.aspx?{0}=0", AppConstants.TABID));
        }
    }
}




