﻿using System;
using System.Collections.Generic;
using System.Text;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public interface IUserEditView
    {
        string GetUserId { get; }
        string GetUserName { get; }
        string GetFirstName { get; }
        string GetLastName { get; }
        string GetEmail { get; }
        bool GetIsActive { get; }
        string GetPassword { get; }
        bool IsExternaUser { get; }
        int VendorId { get; }
        int RegionId { get; }
        int SiteId { get; }
       //IList<UserLocation> userlocation { get; }
        //IList<Role> Roles { set; }
        
    }
}




