﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.Services.Admin;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Enums;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public class TabEditPresenter : Presenter<ITabEditView>
    {
        private AdminController _controller;
        private NodeServices _nodeService;
        private TabServices _tabService;
        private MenuTab _tab;

        public TabEditPresenter([CreateNew] AdminController controller)
        {
            _controller = controller;
            _nodeService = new NodeServices();
            _tabService = new TabServices();
        }

        public override void OnViewLoaded()
        {
        }

        public override void OnViewInitialized()
        {
        }

        public IList<Node> GetNodes(PageViewType pt)
        {
            return _nodeService.GetListOfNodes(pt);
        }

        public IList<Node> GetNodes()
        {
            return _nodeService.GetListOfAllNodes();
        }
        
        public Node GetNode(int nodeid)
        {
            return _nodeService.GetNodeById(nodeid);
        }

        public MenuTab CurrentTab
        {
            get
            {
                if (_tab == null)
                {
                    int id = int.Parse(View.GetNodeId);
                    _tab = _tabService.GetMenuTab(id);

                    if (_tab == null)
                        _tab = new MenuTab(id);
                }
                return _tab;
            }
        }

        
        public void SaveOrUpdateTab()
        {
            _tabService.SaveOrUpdateMenuTab(CurrentTab);
        }
    }
}




