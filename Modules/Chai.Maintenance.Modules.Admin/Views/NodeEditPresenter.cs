﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services.Admin;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Shared;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public class NodeEditPresenter : Presenter<INodeEditView>
    {
        private AdminController _controller;
        private Node _node;
        private NodeServices _nodeServices;

        public NodeEditPresenter([CreateNew] AdminController controller)
        {
            _controller = controller;
            _nodeServices = new NodeServices();
        }

        public override void OnViewLoaded()
        {
        }

        public override void OnViewInitialized()
        {
            View.BindNode();
            View.BindRoles();
        }

        public IList<Role> GetRoles
        {
            get { return _controller.GetRoles; }
        }
                
        public Node CurrentNode
        {
            get
            {
                if (_node == null)
                {
                    int id = int.Parse(View.GetNodeId);
                    if (id > 0)
                        _node = _nodeServices.GetNodeById(id);
                    else
                        _node = new Node();
                }
                return _node;
            }
        }

        public void SaveOrUpdateNode()
        {
            Node node = CurrentNode;
                               
            View.SetRoles(node);

            node.Title = View.GetTitle;
            node.Description = View.GetDescription;
            node.FolderPath = View.GetFolderPath;
            node.ImagePath = View.GetImagePath;
            node.ViewType = (PageViewType)Enum.Parse(typeof(PageViewType), View.GetViewType);

            if (node.IsNew())
            {
                node.Id = int.Parse( View.GetAspxPageId);
                _nodeServices.SaveNode(node);
            }
            else
            {
                _nodeServices.UpdateNode(node);
            }
            
            if (int.Parse(View.GetNodeId) <= 0)
            {
                string url = String.Format("~/Admin/NodeEdit.aspx?{0}=0&{1}={2}", AppConstants.TABID, AppConstants.NODEID, node.Id);
                _controller.Navigate(url);
            }
        }
        
        public void CancelIt()
        {  
            string url = String.Format("~/Admin/Nodes.aspx?{0}=0", AppConstants.TABID);
            _controller.Navigate(url);
        }

        public void DeleteIt()
        {
            Node node = CurrentNode;
            if (node != null && !node.IsNew())
            {
                _nodeServices.DeleteNode(node);
            }
        }

        public Role GetRole(int roleid)
        {
            return _controller.GetRoleById(roleid);
        }
    }
}




