﻿using System;
using System.Collections.Generic;
using System.Text;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public interface INodeEditView
    {
        string GetTabId { get; }
        string GetNodeId { get; }
        string GetAspxPageId { get; }
        string GetViewType { get; }
        string GetTitle { get; }
        string GetDescription { get; }
        string GetFolderPath { get; }
        string GetImagePath { get; }
        void BindNode();
        void BindRoles();
        void SetRoles(Node node);
    }
}




