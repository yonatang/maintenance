﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public class NavigationPresenter : Presenter<INavigationView>
    {
        private AdminController _controller;

        public NavigationPresenter([CreateNew] AdminController controller)
        {
         		_controller = controller;
        }

        public override void OnViewLoaded()
        {
            // TODO: Implement code that will be executed every time the view loads
        }

        public override void OnViewInitialized()
        {
            // TODO: Implement code that will be executed the first time the view loads
        }

        public User CurrentUser
        {
            get { return _controller.CurrentUser; }
        }

     
    }
}




