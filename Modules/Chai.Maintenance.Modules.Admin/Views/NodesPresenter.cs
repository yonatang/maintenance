﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;

using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services.Admin;
using Chai.Maintenance.Enums;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public class NodesPresenter : Presenter<INodesView>
    {
        private NodeServices _nodeService;

        private AdminController _controller;
        public NodesPresenter([CreateNew] AdminController controller)
        {
            _controller = controller;
            _nodeService = new NodeServices();
        }


        public override void OnViewLoaded()
        {
            // TODO: Implement code that will be executed every time the view loads
        }

        public override void OnViewInitialized()
        {
            // TODO: Implement code that will be executed the first time the view loads
        }

        public IList<Node> GetNodes()
        {
            return _nodeService.GetListOfAllNodes();
        }

        public IList<Node> GetNodes(PageViewType ptype)
        {
            return _nodeService.GetListOfNodes(ptype);
        }
    }
}




