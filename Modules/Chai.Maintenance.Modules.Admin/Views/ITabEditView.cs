﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public interface ITabEditView
    {
        string GetTabId { get; }
        string GetNodeId { get; }
    }
}




