﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Services;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public class UsersPresenter : Presenter<IUsersView>
    {
        private AdminController _controller;
        private UserServices _userService;
 
        public UsersPresenter([CreateNew] AdminController controller)
        {
            _controller = controller;
            _userService = new UserServices();
        }

        public override void OnViewLoaded()
        {
            // TODO: Implement code that will be executed every time the view loads
        }

        public override void OnViewInitialized()
        {
            // TODO: Implement code that will be executed the first time the view loads
        }

        public void AddNewUser()
        {
            string url = String.Format("~/Admin/UserEdit.aspx?{0}=0&{1}=0", AppConstants.TABID, AppConstants.USERID);
            _controller.Navigate(url);
        }

        public IList<User> SearchUser(string username)
        {
            return _userService.SearchUsers(username);
        }
    }
}




