﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;

namespace Chai.Maintenance.Modules.Admin.Views
{
    public class RolesPresenter : Presenter<IRolesView>
    {
        private AdminController _controller;

        public RolesPresenter([CreateNew] AdminController controller)
        {
            _controller = controller;
        }

        public override void OnViewLoaded()
        {
            // TODO: Implement code that will be executed every time the view loads
        }

        public override void OnViewInitialized()
        {
            // TODO: Implement code that will be executed the first time the view loads
        }
       public void AddNewRole()
        {
            string url = String.Format("~/Admin/RoleEdit.aspx?{0}=0&{1}=0", AppConstants.TABID, AppConstants.ROLEID);
            _controller.Navigate(url);

        }

       public IList<Role> GetAllRoles()
       {
           return _controller.GetRoles;
       }
    }
}




