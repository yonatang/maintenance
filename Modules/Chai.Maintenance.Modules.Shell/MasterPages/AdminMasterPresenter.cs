﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;

namespace Chai.Maintenance.Modules.Shell.MasterPages
{
    public class AdminMasterPresenter : Presenter<IAdminMasterView>
    {

        // NOTE: Uncomment the following code if you want ObjectBuilder to inject the module controller
        //       The code will not work in the Shell module, as a module controller is not created by default
        //
        private Chai.Maintenance.Modules.Admin.AdminController _controller;
        public AdminMasterPresenter([CreateNew] Chai.Maintenance.Modules.Admin.AdminController controller)
        {
            _controller = controller;
        }

        public override void OnViewLoaded()
        {
            // TODO: Implement code that will be executed every time the view loads
        }

        public override void OnViewInitialized()
        {
            // TODO: Implement code that will be executed the first time the view loads
        }
        public int[] GetLocationId()
        {
            int[] locationId = { 0, 0, 0 }; // 3rd index holds userid
            locationId[2] = _controller.CurrentUser.Id; // user id

            if (_controller.CurrentUser.SiteId != 0)
            {

                locationId[0] = 1; //site = 1
                locationId[1] = _controller.CurrentUser.SiteId;
                return locationId;

            }
            else
            {
                locationId[0] = 2; //region = 2
                locationId[1] = _controller.CurrentUser.RegionId;
                return locationId;

            }
        }
        // TODO: Handle other view events and set state in the view
    }
}




