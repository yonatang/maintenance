﻿using System;
using System.Web.UI;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb.Web;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services.Admin;
using Chai.Maintenance.Shared.Events;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Enums;


namespace Chai.Maintenance.Modules.Shell
{
    public class BaseMaster : Microsoft.Practices.CompositeWeb.Web.UI.MasterPage
    {
        ShellController _controller;
        public delegate void MessageEventHandler(object sender, MessageEventArgs e);
        public event MessageEventHandler Message;
        private NodeServices _nodeService;

        public BaseMaster()
        {
            _nodeService = new NodeServices();

            if (Context.User == null)
            {
                Context.Response.Redirect("~/UserLogin.aspx");
                //Server.Transfer("~/UserLogin.aspx", false);
            }
            else if (!Context.User.Identity.IsAuthenticated)
            {
                Context.Response.Redirect("~/UserLogin.aspx");
                //Server.Transfer("~/UserLogin.aspx", false);
                //_controller.Navigate();

            }
        }
        
        //protected override void OnInit(EventArgs e)
        //{
            
        //    base.OnInit(e);
        //}

        protected virtual void OnMessage(MessageEventArgs e)
        {
            if (Message != null)
            {
                Message(this, e);
            }
        }

        [CreateNew]
        public ShellController Controller
        {
            get
            { 
                return _controller; 
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value");

                this._controller = value;
            }
        }

        public User CurrentUser
        {
            get
            {
                return HttpContextLocatorService.GetCurrentContext().User.Identity as User;
            }
        }

        public IHttpContextLocatorService HttpContextLocatorService
        {
            get { return Controller.HttpContextLocatorService; }
        }

        public Node ActiveNode
        {
            get
            {
                if (string.IsNullOrEmpty(this.NodeId))
                    return null;

                return _nodeService.GetNodeById(int.Parse(this.NodeId));
            }
        }

        public TabType ActiveTab
        {
            get
            {
                if (TabId != null)
                {
                    return (TabType)Enum.ToObject(typeof(TabType), int.Parse(TabId));
                }
                return TabType.Home;
            }
        }

        public string TabId
        {
            get { return Request.QueryString[AppConstants.TABID]; }
        }

        public string NodeId
        {
            get { return Request.QueryString[AppConstants.NODEID]; }
        }

        public void ShowMessage(Chai.Maintenance.Shared.AppMessage message)
        {
            MessageEventArgs e = new MessageEventArgs(message);
            OnMessage(e);
        }

        public void TransferMessage(Chai.Maintenance.Shared.AppMessage message)
        {
            this.GetRMessaage = message;
        }
        
        protected void CheckTransferdMessage()
        {
            object msgObject =  GetRMessaage;
            if (msgObject != null && (msgObject is Chai.Maintenance.Shared.AppMessage))
            {
                ShowMessage((Chai.Maintenance.Shared.AppMessage)msgObject);
                this.GetRMessaage = null;
            }
        }

        private object GetRMessaage
        {
            get
            {
                return HttpContextLocatorService.GetCurrentContext().Session["RMESSAGE"];
            }
            set
            {
                HttpContextLocatorService.GetCurrentContext().Session["RMESSAGE"] = value;
            }
 
        }
       
    }
    
}
