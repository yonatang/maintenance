﻿using System;
using System.Collections.Generic;
using Chai.Maintenance.Shared;

namespace Chai.Maintenance.Modules.Shell
{
    public class BaseMessageControl : Microsoft.Practices.CompositeWeb.Web.UI.UserControl
    {
        private AppMessage _message;

        public AppMessage Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public BaseMessageControl()
        {
        }
    }
}
