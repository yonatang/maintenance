using System;
using System.Web.SessionState;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb.Web;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared.Navigation;
using System.Data;
using Chai.Maintenance.Services.Report;
using System.Collections;

namespace Chai.Maintenance.Modules.Shell
{
    public class ShellController
    {
        private IHttpContextLocatorService _httpContextLocatorService;
        private INavigationService _navigationService;
        private ReportServices _reportservices;
        [InjectionConstructor]
        public ShellController([ServiceDependency] IHttpContextLocatorService httpContextLocatorService,
           [ServiceDependency] INavigationService navigationService)
        {
             _httpContextLocatorService = httpContextLocatorService;
             _navigationService = navigationService;            
        }

        public IHttpContextLocatorService HttpContextLocatorService
        {
            get { return _httpContextLocatorService; }
        }

        public IHttpContextLocatorService CurrentContext
        {
            get { return _httpContextLocatorService; }
        }

        public User CurrentUser
        {
            get
            {
                return _httpContextLocatorService.GetCurrentContext().User.Identity as User;
            }
        }

        public void Navigate(string url)
        {
            _navigationService.Navigate(url);
        }
        public IList GetMapStatisticReport()
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();

            return _reportservices.GetMapStatisticReport();
        }
        public IList GetNotifiedProblemsChart(string datefrom, string dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetNotifiedProblemsChart(datefrom, dateto);
        }
        
        public IList GetFrequentlyMaintainedInstrumentsChart(string datefrom, string dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetFrequentlyMaintainedInstrumentsChart(datefrom, dateto);
        }
        public IList GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(string datefrom, string dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(datefrom, dateto);
        }
        public IList GetRFrequentlyReportedProblemTypesChart(string datefrom, string dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetRFrequentlyReportedProblemTypesChart(datefrom, dateto);
        }
     
    }
}
