﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Ramcs.Accounting.CoreDomain.Vendors;
using Ramcs.Accounting.CoreDomain;
using Ramcs.Accounting.Services.Vendor;
using Ramcs.Accounting.Enums;

namespace Ramcs.Accounting.Modules.Vendors.Views
{
    public class VendorQuotationListPresenter : Presenter<IVendorQuotationListView>
    {
        private VendorsController _controller;
        public VendorQuotationListPresenter([CreateNew] IRamcs.Accounting.Modules.ShellController controller)
        {
            _controller = controller;
        }

        public override void OnViewLoaded()
        {
            // TODO: Implement code that will be executed every time the view loads
        }

        public override void OnViewInitialized()
        {
            // TODO: Implement code that will be executed the first time the view loads
        }

        // TODO: Handle other view events and set state in the view
    }
}




