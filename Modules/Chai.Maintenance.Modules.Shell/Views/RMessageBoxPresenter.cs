﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;

namespace Chai.Maintenance.Modules.Shell.Views
{
    public class RMessageBoxPresenter : Presenter<IRMessageBoxView>
    {
        public override void OnViewLoaded()
        {
            View.BindMessage();
        }

        public override void OnViewInitialized()
        {
            // TODO: Implement code that will be executed the first time the view loads
        }

     }
}




