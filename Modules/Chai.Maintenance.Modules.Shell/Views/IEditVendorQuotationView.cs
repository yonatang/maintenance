﻿using System;
using System.Collections.Generic;
using System.Text;
using Ramcs.Accounting.CoreDomain;

namespace Ramcs.Accounting.Modules.Shell.Views
{
    public interface IEditVendorQuotationView
    {
        void BindQoutation();
        string GetDocumentID { get; }
        string GetAccountId { get; }
        string QuotationNo { get; }
        string QuoteDate { get; }
        string Name { get; }
        string VatRegNo { get; }
        string TinNo { get; }
        string SubCity { get; }
        string Kebele { get; }
        string HouseNo { get; }
        string TelNo { get; }
        string CustRequestNo { get; }
        string RequestedBy { get; }
        string ValidUntil { get; }
        string Remark { get; }
        Node GetNode { get; }
        void ApplyAuthorizationRules();
    }
}




