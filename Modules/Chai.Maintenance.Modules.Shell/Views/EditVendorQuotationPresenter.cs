﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Ramcs.Accounting.CoreDomain.Customers;
using Ramcs.Accounting.Services.Customer;
using Ramcs.Accounting.CoreDomain.Documents;
using Ramcs.Accounting.CoreDomain;

namespace Ramcs.Accounting.Modules.Vendors.Views
{
    public class EditVendorQuotationPresenter : Presenter<IEditVendorQuotationView>
    {

        private VendorsController _controller;
        public EditVendorQuotationPresenter([CreateNew] VendorsController controller)
        {
            _controller = controller;
        }

        public override void OnViewLoaded()
        {
            // TODO: Implement code that will be executed every time the view loads
        }

        public override void OnViewInitialized()
        {
            // TODO: Implement code that will be executed the first time the view loads
        }

        // TODO: Handle other view events and set state in the view
    }
}




