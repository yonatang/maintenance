﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;

using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services.Admin;

namespace Chai.Maintenance.Modules.Shell.Views
{
    public class ModuleNavigationPresenter : Presenter<IModuleNavigationView>
    {
         private ShellController _controller;
         private TabServices _tabServices;
         public ModuleNavigationPresenter([CreateNew] ShellController controller)
         {
         		_controller = controller;
                _tabServices = new TabServices();
         }

        public override void OnViewLoaded()
        {
            // TODO: Implement code that will be executed every time the view loads
        }

        public override void OnViewInitialized()
        {
            // TODO: Implement code that will be executed the first time the view loads
        }
        public MenuTab Getmenutab(int tabid)
        {
            return _tabServices.GetMenuTab(tabid);
        }
        public User CurrentUser
        {
            get { return _controller.CurrentUser; }
        }
    }
}




