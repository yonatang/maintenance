﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using System.Globalization;

using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Services.Admin;


namespace Chai.Maintenance.Modules.Shell.Views
{
    public class NodeNavigationPresenter : Presenter<INodeNavigationView>
    {
        private ShellController _controller;
        private MenuTab _currentTab;
        private TabNode _currentTabNode;
        private int _tabId;
        private int _nodeId;
        private TabServices _tabServices;

        public NodeNavigationPresenter([CreateNew] ShellController controller)
        {
            _controller = controller;
            _tabServices = new TabServices();
        }

        public override void OnViewLoaded()
        {
            
            Int32.TryParse(View.TabId, NumberStyles.Integer, CultureInfo.CurrentCulture, out _tabId);
            Int32.TryParse(View.NodeId, NumberStyles.Integer, CultureInfo.CurrentCulture, out _nodeId);
            
            View.BindPanel();
        }

        public override void OnViewInitialized()
        {
            // TODO: Implement code that will be executed the first time the view loads
        }

        public User CurrentUser
        {
            get { return _controller.CurrentUser; }
        }

        public MenuTab CurrentTab
        {
            get
            {
                if (_currentTab == null)
                    _currentTab = _tabServices.GetMenuTab(_tabId);
                return _currentTab;
            }
        }

        public TabNode CurrentTabNode
        {
            get 
            {
                if (_currentTabNode == null)
                    _currentTabNode = _tabServices.GetTabNodeById(_nodeId);
                return _currentTabNode;
            }
        }
    }
}




