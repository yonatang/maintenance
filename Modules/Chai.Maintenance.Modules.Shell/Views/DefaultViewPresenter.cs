using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using System.Data;
using System.Collections;

namespace Chai.Maintenance.Modules.Shell.Views
{
    public class DefaultViewPresenter : Presenter<IDefaultView>
    {
        ShellController _shellcontroller;
        public DefaultViewPresenter([CreateNew] ShellController shellcontroller)
        {
            _shellcontroller = shellcontroller;
        }
        public IList GetMapStatisticReport()
        {


            return _shellcontroller.GetMapStatisticReport();
        }
        public IList GetNotifiedProblemsChart(string datefrom, string dateto)
        {
            return _shellcontroller.GetNotifiedProblemsChart(datefrom, dateto);
        }
        public IList GetFrequentlyMaintainedInstrumentsChart(string datefrom, string dateto)
        {

            return _shellcontroller.GetFrequentlyMaintainedInstrumentsChart(datefrom, dateto);
        }
        public IList GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(string datefrom, string dateto)
        {

            return _shellcontroller.GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(datefrom, dateto);
        }
        public IList GetRFrequentlyReportedProblemTypesChart(string datefrom, string dateto)
        {

            return _shellcontroller.GetRFrequentlyReportedProblemTypesChart(datefrom, dateto);
        }
        
    }
}
