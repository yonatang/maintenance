﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.Modules.Shell.Views
{
    public class UserLoginPresenter : Presenter<IUserLoginView>
    {
        private ShellController _controller;
        public UserLoginPresenter([CreateNew] ShellController controller)
        {
            _controller = controller;
        }

        public override void OnViewLoaded()
        {
            // TODO: Implement code that will be executed every time the view loads
        }

        public override void OnViewInitialized()
        {
            // TODO: Implement code that will be executed the first time the view loads
        }
        public bool AuthenticateUser()
        {
            AuthenticationModule am = (AuthenticationModule)_controller.CurrentContext.GetCurrentContext().ApplicationInstance.Modules["AuthenticationModule"];
            if (View.GetUserName.Trim().Length > 0 && View.GetPassword.Trim().Length > 0)
            {
                try
                {
                    if (am.AuthenticateUser(View.GetUserName, View.GetPassword, View.PersistLogin))
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            else
            {
                throw new Exception("USERNAMEPASSWORDMISSING");
            }
        }
        public void Logout()
        {
            AuthenticationModule am = (AuthenticationModule)_controller.HttpContextLocatorService.GetCurrentContext().ApplicationInstance.Modules["AuthenticationModule"];
            am.Logout();
        }

        public User CurrentUser
        {
            get { return _controller.CurrentUser; }
        }

        public bool IsAuthenticated
        {
            get
            {
                if (_controller.CurrentUser != null)
                    return _controller.CurrentUser.IsAuthenticated;
                return false;
            }
        }

        public void RedirectToRowUrl()
        {
            _controller.Navigate(_controller.HttpContextLocatorService.GetCurrentContext().Request.RawUrl);
        }

    }
}




