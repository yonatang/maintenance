﻿using System;
using System.Collections.Generic;
using System.Text;
using Ramcs.Accounting.CoreDomain;

namespace Ramcs.Accounting.Modules.Shell.Views
{
    public interface IVendorQuotationListView
    {
        void BindQuotation();
        void ApplyAuthorizationRules();
    }
}




