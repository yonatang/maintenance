﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chai.Maintenance.Modules.Shell.Views
{
    public interface INodeNavigationView
    {
        string TabId { get; }
        string NodeId { get; }
        string PanelId { get; }
        void BindPanel();
    }
}




