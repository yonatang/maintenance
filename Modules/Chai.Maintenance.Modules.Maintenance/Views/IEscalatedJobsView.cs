﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IEscalatedJobsView
    {

        IList<CoreDomain.Maintenance.EscalatedJobs> EscalatedJobs { set; }
        int SiteId { get; }
        int RegionId { get; }
    }
}
