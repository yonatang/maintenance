﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 

namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IMaintenanceInstrumentListView
    {
        
        int ddlinstrumentId { get; }
        int siteId { get; }
        int regionId { get; }
        int _instrumentID { get; } //parameter from Problem notification page after save, cancel clicked. to keep the selected instrument
        int _siteId { get; } //parameter from Problem notification page after save, cancel clicked. to keep the selected instrument
        IList<Chai.Maintenance.CoreDomain.Configuration.Instrument> instrumentlist { set; }
        int instrumentCategoryID { get; }
        int Functionality { get; }
    }
}
