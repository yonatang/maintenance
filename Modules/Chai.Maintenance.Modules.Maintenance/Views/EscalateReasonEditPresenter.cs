﻿ 

using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.Modules.Maintenance.Views;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Modules.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class EscalateReasonEditPresenter : Presenter<IEscalateReasonEditView>
    {
        private MaintenanceController _controller;
        private ConfigurationController _confgController;
        private EscalateReason _escalateReason;
        public EscalateReasonEditPresenter([CreateNew] MaintenanceController controller, ConfigurationController confgController)
        {
            _controller = controller;
            _confgController = confgController;
        }
        public override void OnViewLoaded()
        {

            View.EscalateReason = _controller.EscalateReasonObject;
        }
        public override void OnViewInitialized()
        {
            if (View.Id != 0)
                _controller.GetEscalateReasonById(View.Id);
            else
                _controller.NewEscalateReason();
        }
        public void SaveOrUpdateEscalateReason(EscalateReason escalateReason)
        {
            _controller.SaveOrUpdateEscalateReason(escalateReason);
        }
        public Instrument GetInstrumentById(int instrumentId)
        {
            return _confgController.GetInstrumentByIdForProblem(instrumentId);
        }
        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Maintenance/Maintenance.aspx?{0}=2", AppConstants.TABID));         
        }
        public void DeleteProblem(int problemId)
        {
            _controller.DeleteProblem(problemId);
        }
       
        public Problem GetProlemById(int problemId)
        {
            return _controller.GetProblemByProblemID(problemId);
        }
        public int[] GetLocationId()
        {
            int[] locationId = { 0, 0, 0 }; // 3rd index holds userid
            locationId[2] = _controller.CurrentUser.Id; // user id

            if (_controller.CurrentUser.SiteId != 0)
            {

                locationId[0] = 1; //site = 1
                locationId[1] = _controller.CurrentUser.SiteId;
                return locationId;

            }
            else
            {
                locationId[0] = 2; //region = 2
                locationId[1] = _controller.CurrentUser.RegionId;
                return locationId;

            }
        }
    }
}
