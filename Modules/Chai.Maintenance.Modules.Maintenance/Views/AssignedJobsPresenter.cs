﻿
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class AssignedJobsPresenter : Presenter<IAssignedJobsView>
    {

         
        private Chai.Maintenance.Modules.Maintenance.MaintenanceController _maintenanceController;
        public AssignedJobsPresenter([CreateNew] Chai.Maintenance.Modules.Maintenance.MaintenanceController maintcontroller)
        {
       
            _maintenanceController = maintcontroller;

        }
        public override void OnViewLoaded()
        {
            View._AssignedJobs = _maintenanceController.GetAssignedJobs("",0,0);

        }

        public override void OnViewInitialized()
        {

        }
        public IList<Chai.Maintenance.CoreDomain.Maintenance.AssignedJobs> GetAssignedJobs(string searchBy, int UserId, int siteid)
        {
            return _maintenanceController.GetAssignedJobs(searchBy, UserId, siteid);
        }
       
        public void NewPage()
        {
            _maintenanceController.Navigate(String.Format("~/Maintenance/AssignedJobs.aspx?{0}=0", AppConstants.TABID));
        }
        public Problem GetProlemById(int problemId)
        {
            return _maintenanceController.GetProblemByProblemID(problemId);
        }
        public int[] GetLocationId()
        {
            int[] locationId = { 0, 0, 0 }; // 3rd index holds userid
            locationId[2] = _maintenanceController.CurrentUser.Id; // user id

            if (_maintenanceController.CurrentUser.SiteId != 0)
            {

                locationId[0] = 1; //site = 1
                locationId[1] = _maintenanceController.CurrentUser.SiteId;
                return locationId;

            }
            else
            {
                locationId[0] = 2; //region = 2
                locationId[1] = _maintenanceController.CurrentUser.RegionId;
                return locationId;

            }
        }
    }
}
