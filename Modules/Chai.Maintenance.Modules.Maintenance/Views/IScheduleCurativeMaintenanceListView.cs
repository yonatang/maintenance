﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IScheduleCurativeMaintenanceListView
    {
        int ProblemId { get; }
        int _problemTypeId { get; }
        int _instCategoryId { get; }
        int _siteId { get; }
        int regionId { get; }
        IList<Problem> problemlist { set; }
   
    }
}
