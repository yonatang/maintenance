﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;

namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IScheduleCurativeMaintenanceView
    {
        int InstrumentId { get; }
        int ProblemId { get; }
        int Id { get; }
        Schedule _Schedule { get; set; }


        int InstrumentIdEdit { get; }
        int ProblemIdEdit { get; }
        int ScheduleIdEdit { get; }

        int Mode { get; }
        int EscalatedInstrumentId { get; }
        int EscalatedProblemId { get; }
        int EscalateId { get; }
    }
}
