﻿using System;
using System.Collections.Generic;
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class EscalatedJobsPresenter : Presenter<IEscalatedJobsView>
    {       
        private Chai.Maintenance.Modules.Maintenance.MaintenanceController _maintenanceController;
        private Chai.Maintenance.Modules.Configuration.ConfigurationController _configurationController;
        public EscalatedJobsPresenter([CreateNew] Chai.Maintenance.Modules.Configuration.ConfigurationController controller, [CreateNew] Chai.Maintenance.Modules.Maintenance.MaintenanceController maintcontroller)
        {
            _configurationController = controller;
            _maintenanceController = maintcontroller;
        }
        public override void OnViewLoaded()
        {
            View.EscalatedJobs = _maintenanceController.GetEscalatedJobsByRegionSite(View.RegionId, View.SiteId);
        }

        public override void OnViewInitialized()
        {

        }
        public void NewPage()/////////////////////////////////////////////
        {
            _maintenanceController.Navigate(String.Format("~/Maintenance/AssignedJobs.aspx?{0}=0", AppConstants.TABID));
        }
        public IList<CoreDomain.Configuration.Region> GetRegionList()
        {
            return _configurationController.GetRegions(0, "", _maintenanceController.CurrentUser.RegionId);
        }
        public IList<CoreDomain.Configuration.Site> GetSiteList(int regionid)
        {
            return _configurationController.GetSiteByRegionID(regionid);
        }
    }
}
