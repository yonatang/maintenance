﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class ConsumableNotificationListPresenter : Presenter<IConsumableNotificationListView>
    {
        private Chai.Maintenance.Modules.Configuration.ConfigurationController _configurationController;
        private Chai.Maintenance.Modules.Maintenance.MaintenanceController _maintenanceController;
        public ConsumableNotificationListPresenter([CreateNew] Chai.Maintenance.Modules.Configuration.ConfigurationController controller, [CreateNew] Chai.Maintenance.Modules.Maintenance.MaintenanceController maintcontroller)
        {
            _configurationController = controller;
            _maintenanceController = maintcontroller;
        }
        public override void OnViewLoaded()
        { 
            View.ConsumableNotificationList = _maintenanceController.GetConsumableNotificationBySiteConsumableId(View.SiteId, View.ConsumablesId, View.UserId);
        }
        public override void OnViewInitialized()
        {

        }
        public IList<Chai.Maintenance.CoreDomain.Configuration.Site> GetSiteList(int regionId)
        {
            return _configurationController.GetSites(0, string.Empty, regionId);
        }
        public IList<Chai.Maintenance.CoreDomain.Configuration.Consumables> GetConsumablesList()
        {
            return _configurationController.GetConsumables(string.Empty);
        }
        public int[] GetLocationId()
        {
            int[] locationId = { 0, 0, 0 }; // 3rd index holds userid
            locationId[2] = _maintenanceController.CurrentUser.Id; // user id

            if (_maintenanceController.CurrentUser.SiteId != 0)
            {

                locationId[0] = 1; //site = 1
                locationId[1] = _maintenanceController.CurrentUser.SiteId;
                return locationId;

            }
            else
            {
                locationId[0] = 2; //region = 2
                locationId[1] = _maintenanceController.CurrentUser.RegionId;
                return locationId;

            }
        }
    }
}
