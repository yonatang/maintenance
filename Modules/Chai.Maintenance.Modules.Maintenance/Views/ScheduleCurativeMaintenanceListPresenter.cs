﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Maintenance.Views;


namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class ScheduleCurativeMaintenanceListPresenter : Presenter<IScheduleCurativeMaintenanceListView>
    {
        private Chai.Maintenance.Modules.Configuration.ConfigurationController _configurationController;
        private Chai.Maintenance.Modules.Maintenance.MaintenanceController _maintenanceController;
        public ScheduleCurativeMaintenanceListPresenter([CreateNew] Chai.Maintenance.Modules.Configuration.ConfigurationController controller, [CreateNew] Chai.Maintenance.Modules.Maintenance.MaintenanceController maintcontroller)
        {
            _configurationController = controller;
            _maintenanceController = maintcontroller;

        }
        

        public override void OnViewLoaded()
        {
            View.problemlist = _maintenanceController.GetProblemForCurativeSchedule(View.regionId, View._siteId, View._instCategoryId, View._problemTypeId);
        }

        public override void OnViewInitialized()
        {

        }


        public IList<Site> GetSiteList(int regionid)
        {
            return _configurationController.GetSiteByRegionID(regionid);
        }
        public IList<Chai.Maintenance.CoreDomain.Configuration.Manufacturer> GetManufacturerList()
        {
            return _configurationController.GetManufacturers(string.Empty);
        }
        public IList<Chai.Maintenance.CoreDomain.Configuration.ProblemType> GetProblemTypeList()
        {
            return _configurationController.GetProblemTypes(string.Empty);
        }
        public IList<Chai.Maintenance.CoreDomain.Configuration.InstrumentCategory> GetInstrumentCategoryList()
        {
            return _configurationController.GetInstrumentCategorys(string.Empty);
        }
        public IList<Chai.Maintenance.CoreDomain.Configuration.Region> GetRegionList()
        {
            return _configurationController.GetRegions(0, string.Empty,_maintenanceController.CurrentUser.RegionId);
        }
        public void GetProblemForCurativeSchedule(int siteId, int InstrumentCategoryId, int ProblemTypeId, int regionId)
        {
            _maintenanceController.GetProblemForCurativeSchedule(regionId, siteId, InstrumentCategoryId, ProblemTypeId);

        }
        public void GetProblembyId(int ProblemId)
        {
            _maintenanceController.GetProblems(0, ProblemId);

        }
        
        public void CancelPage()
        {
            _maintenanceController.Navigate(String.Format("~/Admin/Roles.aspx?{0}=0", AppConstants.TABID));
        }

        public int[] GetLocationId()
        {
            int[] locationId = { 0, 0, 0 }; // 3rd index holds userid
            locationId[2] = _maintenanceController.CurrentUser.Id; // user id

            if (_maintenanceController.CurrentUser.SiteId != 0)
            {

                locationId[0] = 1; //site = 1
                locationId[1] = _maintenanceController.CurrentUser.SiteId;
                return locationId;

            }
            else
            {
                locationId[0] = 2; //region = 2
                locationId[1] = _maintenanceController.CurrentUser.RegionId;
                return locationId;

            }
        }
    }
}
