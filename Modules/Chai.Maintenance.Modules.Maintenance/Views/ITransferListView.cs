﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface ITransferListView
    {
        IList<Transfer> _transferList { set; }
        int _SiteId { get; }
        int _instCategoryId { get; }

        int UserId { get; }
      
    }
}
