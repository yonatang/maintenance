﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IPreventiveMaintenanceChecklistView
    {
        int Id { get; }
        PreventiveMaintenanceChecklist Checklist { get; set; }
        int PreventiveMaintenanceId { get; }

    }
}
