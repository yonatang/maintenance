﻿
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Shared;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class PreventiveMaintenancePresenter : Presenter<IPreventiveMaintenanceView>
    {
        private MaintenanceController _controller;
        private ConfigurationController _confgController;
        public PreventiveMaintenancePresenter([CreateNew] MaintenanceController controller, [CreateNew] ConfigurationController configcontroller)
        {
            this._controller = controller;
            this._confgController = configcontroller;
        }
        public override void OnViewLoaded()
        {

            View.preventive = _controller.PreventiveObject;

        }

        public override void OnViewInitialized()
        {
            if (View.PreventiveIdEdit != 0)
                _controller.GetPreventiveMaintenanceById(View.PreventiveIdEdit);
            else
                _controller.NewPreventiveMaintenance();
        }
        public void SaveOrUpdatePreventiveMaintenance(PreventiveMaintenance preventive)
        {
            _controller.SaveOrUpdatePreventiveMaintenance(preventive);
        }

        public IList<SparepartType> GetSparepartType(int insId)
        {
            return _confgController.GetSparepartTypes(3, "", insId);
        }

        public void CancelPage()//////////////////////////////////////////////////////////////////
        {
            _controller.Navigate(String.Format("~/Maintenance/AssignedJobs.aspx?{0}=3", AppConstants.TABID));
        }
        public void DeletePreventiveMaintenance(int preventiveid)
        {
            _controller.DeletePreventiveMaintenance(preventiveid);
        }
        public void DeleteSpareparts(int spareId)
        {
            _controller.DeletePreventiveMaintenanceSpareparts(spareId);
        }
        public Instrument GetInstrumentByIdForProblem(int instrumentId)
        {
            return _confgController.GetInstrumentByIdForProblem(instrumentId);
        }
        public Problem GetProlemById(int problemId)
        {
            return _controller.GetProblemByProblemID(problemId);
        }
       public IList<ChecklistTask> GetListOfChecklistTask(int nameId)
       {
           return _confgController.GetChecklistTask(string.Empty, nameId);
       }

        public int[] GetLocationId()
        {
            int[] locationId = { 0, 0, 0 }; // 3rd index holds userid
            locationId[2] = _controller.CurrentUser.Id; // user id

            if (_controller.CurrentUser.SiteId != 0)
            {

                locationId[0] = 1; //site = 1
                locationId[1] = _controller.CurrentUser.SiteId;
                return locationId;

            }
            else
            {
                locationId[0] = 2; //region = 2
                locationId[1] = _controller.CurrentUser.RegionId;
                return locationId;

            }
        }
        //public void UpdateStatus(string updateSentFrom, int status, int Id, string type)
        //{
        //    _controller.UpdateStatus(updateSentFrom, status, Id, type);
        //}
    }
}
