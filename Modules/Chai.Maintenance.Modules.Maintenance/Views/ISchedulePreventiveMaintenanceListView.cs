﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface ISchedulePreventiveMaintenanceListView
    {
        int ddlinstrumentCategoryId { get; }
        int siteId { get; }
        int regionId { get; }
        string LastPrevMaindate { get; }
        int InstrumentId { get; }
        IList<Chai.Maintenance.CoreDomain.Configuration.Instrument> instrumentlist { set; }
       
    }
}
