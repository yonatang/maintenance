﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.Modules.Maintenance.Views;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.Modules.Admin;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class PreventiveMaintenanceChecklistPresenter : Presenter<IPreventiveMaintenanceChecklistView>
    {
        private MaintenanceController _controller;
        private ConfigurationController _confgController;
      private AdminController _AdminController;
      public PreventiveMaintenanceChecklistPresenter([CreateNew] MaintenanceController controller, [CreateNew] ConfigurationController configcontroller, [CreateNew] AdminController AdminController)
      {
          this._controller = controller;
          this._confgController = configcontroller;
          this._AdminController = AdminController;
      }

        public override void OnViewLoaded()
        {

            View.Checklist = _controller.PreventiveMaintenanceChecklistObject;
        }
        public override void OnViewInitialized()
        {
            if (View.Id != 0)
                _controller.GetPreventiveMaintenanceChecklistById(View.Id);
            else
                _controller.NewPreventiveMaintenanceChecklist();
        }
        public void SaveOrUpdatePreventiveMaintenanceChecklist(PreventiveMaintenanceChecklist checklist)
        {
            _controller.SaveOrUpdatePreventiveMaintenanceChecklist(checklist);
        }

        public void DeletePreventiveMaintenanceChecklist(int checklistId)
        {
            _controller.DeletePreventiveMaintenanceChecklist(checklistId);
        }
       
        //public IList<Task> GetUserBySiteID(int siteID)
        //{
        //    return _AdminController.GetUserBySiteID(siteID);
        //}
    }
}
