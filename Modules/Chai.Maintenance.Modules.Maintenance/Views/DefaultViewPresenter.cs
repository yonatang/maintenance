﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.Modules.Report;
using Chai.Maintenance.Modules.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class DefaultViewPresenter : Presenter<IDefaultView>
    {
       private ReportController _reportController;
        private MaintenanceController _controller;
        private ConfigurationController _configcontroller;
        public DefaultViewPresenter([CreateNew] MaintenanceController controller,[CreateNew] ConfigurationController configcontroller, [CreateNew] ReportController reportController)
        {
            this._controller = controller;
            this._configcontroller = configcontroller;
            this._reportController = reportController;
        }
        public override void OnViewLoaded()
        {
            View.User = _controller.CurrentUser;
        }
        public DataSet GetNotifiedProblemsChart(DateTime datefrom, DateTime dateto)
        {
            return _reportController.GetNotifiedProblemsChart(datefrom, dateto);
        }
        public DataSet GetFrequentlyMaintainedInstrumentsChart(DateTime datefrom, DateTime dateto)
        {

            return _reportController.GetFrequentlyMaintainedInstrumentsChart(datefrom, dateto);
        }
        public DataSet GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(DateTime datefrom, DateTime dateto)
        {

            return _reportController.GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(datefrom, dateto);
        }
        public DataSet GetRFrequentlyReportedProblemTypesChart(DateTime datefrom, DateTime dateto)
        {

            return _reportController.GetRFrequentlyReportedProblemTypesChart(datefrom, dateto);
        }
        
    }
}
