﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IDisposalView
    {
        int Id { get; }
        Disposal disposal { get; set; }
        int InstrumentId { get; }

        int InstrumentIdEdit { get; }
        int DisposalIdEdit { get; }
    }
}
