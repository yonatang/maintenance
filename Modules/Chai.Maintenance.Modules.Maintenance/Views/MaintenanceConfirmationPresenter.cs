﻿
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.Modules.Maintenance.Views;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Modules.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class MaintenanceConfirmationPresenter : Presenter<IMaintenanceConfirmationView>
    {
        private MaintenanceController _controller;
        private ConfigurationController _confgController;
        private Confirmation _Confirmation;
        public MaintenanceConfirmationPresenter([CreateNew] MaintenanceController controller, ConfigurationController confgController)
        {
            _controller = controller;
            _confgController = confgController;
        }
        public override void OnViewLoaded()
        {

            View._Confirmation = _controller.ConfirmationObject;
        }
        public override void OnViewInitialized()
        {
            if (View.Id != 0)
                _controller.GetConfirmationById(View.Id);
            else
                _controller.NewConfirmation();
        }
        public void SaveOrUpdateConfirmation(Confirmation _Confirmation, string searchBy, int ScheduleId)
        {
            _controller.SaveOrUpdateConfirmation(_Confirmation, searchBy, ScheduleId);
        }
        public Instrument GetInstrumentById(int instrumentId)
        {
            return _confgController.GetInstrumentByIdForProblem(instrumentId);
        }
        public IList<ProblemType> GetProblemTypeList()
        {
            return _confgController.GetProblemTypes(string.Empty);
        }
        public IList<Instrument> GetAllInstrument()
        {
            return _confgController.GetInstruments(0, string.Empty, 0, 0,_controller.CurrentUser.RegionId);
        }
        public IList<ErrorCode> GetErrorCodeListByProblemTypeID(int ProblemTypeID)
        {
            return _confgController.GetErrorCodeListByProblemTypeID(ProblemTypeID);
        }
        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Maintenance/MaintenanceConfirmation.aspx?{0}=2", AppConstants.TABID));         
        }
        public void DeleteProblem(int problemId)
        {
            _controller.DeleteProblem(problemId);
        }
        public IList<Chai.Maintenance.CoreDomain.Maintenance.AssignedJobs> GetAssignedJobs(string searchBy, int UserId, int Siteid)
        {
            return _controller.GetAssignedJobs(searchBy, UserId, Siteid);
        }
        public Problem GetProlemById(int problemId)
        {
            return _controller.GetProblemByProblemID(problemId);
        }
        public int[] GetLocationId()
        {
            int[] locationId = { 0, 0, 0 }; // 3rd index holds userid
            locationId[2] = _controller.CurrentUser.Id; // user id

            if (_controller.CurrentUser.SiteId != 0)
            {

                locationId[0] = 1; //site = 1
                locationId[1] = _controller.CurrentUser.SiteId;
                return locationId;

            }
            else
            {
                locationId[0] = 2; //region = 2
                locationId[1] = _controller.CurrentUser.RegionId;
                return locationId;

            }
        }
        //public void UpdateStatus(string updateSentFrom, int status, int Id, string type)
        //{
        //    _controller.UpdateStatus(updateSentFrom, status, Id, type);
        //}
       
    }
}
