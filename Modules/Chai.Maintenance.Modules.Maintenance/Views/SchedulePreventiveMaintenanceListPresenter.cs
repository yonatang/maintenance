﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Maintenance.Views;


namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class SchedulePreventiveMaintenanceListPresenter : Presenter<ISchedulePreventiveMaintenanceListView>
    {

        private Chai.Maintenance.Modules.Configuration.ConfigurationController _configurationController;
        private Chai.Maintenance.Modules.Maintenance.MaintenanceController _maintenanceController;
        public SchedulePreventiveMaintenanceListPresenter([CreateNew] Chai.Maintenance.Modules.Configuration.ConfigurationController controller, [CreateNew] Chai.Maintenance.Modules.Maintenance.MaintenanceController maintcontroller)
        {
            _configurationController = controller;
            _maintenanceController = maintcontroller;

        }
        

        public override void OnViewLoaded()
        {
         
                View.instrumentlist = _maintenanceController.GetInstrumentForPreventiveMaint(View.regionId, View.siteId,
                                                                                             View.
                                                                                                 ddlinstrumentCategoryId,
                                                                                             View.LastPrevMaindate);
                
        }

        public override void OnViewInitialized()
        {

        }


        public IList<Region> GetRegionList()
        {
            return _configurationController.GetRegions(0, "",_maintenanceController.CurrentUser.RegionId);
        }
        public IList<Site> GetSiteList(int regionid)
        {
            return _configurationController.GetSiteByRegionID(regionid);
        }
        public IList<Chai.Maintenance.CoreDomain.Configuration.InstrumentCategory> GetInstrumentCategoryList()
        {
            return _configurationController.GetInstrumentCategorys(string.Empty);
        }
        public void NewPage()
        {
            _maintenanceController.Navigate(String.Format("~/Admin/InstrumentEdit.aspx?{0}=0", AppConstants.TABID));
        }



        public void GetInstrumentForPreventiveMaint(int regionId, int siteId, int instrumentCategoryId, string LastPrevMainDate)
        {
            _maintenanceController.GetInstrumentForPreventiveMaint(regionId, siteId, instrumentCategoryId, LastPrevMainDate);
        }

        public void GetInstrumentById(int instrumentId)
        {
            _configurationController.GetInstrumentById(instrumentId);
        }
        public int[] GetLocationId()
        {
            int[] locationId = { 0, 0, 0 }; // 3rd index holds userid
            locationId[2] = _maintenanceController.CurrentUser.Id; // user id

            if (_maintenanceController.CurrentUser.SiteId != 0)
            {

                locationId[0] = 1; //site = 1
                locationId[1] = _maintenanceController.CurrentUser.SiteId;
                return locationId;

            }
            else
            {
                locationId[0] = 2; //region = 2
                locationId[1] = _maintenanceController.CurrentUser.RegionId;
                return locationId;

            }
        }
    }
}
