﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.Modules.Maintenance.Views;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.Modules.Admin;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class SchedulePreventiveMaintenancePresenter : Presenter<ISchedulePreventiveMaintenanceView>
    {
        private MaintenanceController _controller;
        private ConfigurationController _confgController;
        private AdminController _adminController;
        private Schedule _Schedule;

        public SchedulePreventiveMaintenancePresenter([CreateNew] MaintenanceController controller, ConfigurationController confgController, AdminController adminController)
        {
            _controller = controller;
            _confgController = confgController;
            _adminController = adminController;
        }
        public override void OnViewLoaded()
        {

            View._Schedule = _controller.ScheduleObject;
        }

        public override void OnViewInitialized()
        {
            if (View.ScheduleIdEdit != 0)
                _controller.GetScheduleById(View.ScheduleIdEdit);
            else
                _controller.NewSchedule();
        }
        public void SaveOrUpdateSchedule(Schedule schedule)
        {
            _controller.SaveOrUpdateSchedule(schedule,"", 0);
        }

        public Instrument GetInstrumentByIdForProblem(int instrumentId)
        {
            return _confgController.GetInstrumentByIdForProblem(instrumentId);
        }
       

        public void CancelPage(int _instrumentId)
        {
            _controller.Navigate(String.Format("~/Maintenance/SchedulePreventiveMaintenanceList.aspx?{0}=2&InstrumentId?{1}=0", AppConstants.TABID, _instrumentId));

        }
        public void DeleteSchedule(int ScheduleId)
        {
            _controller.DeleteSchedule(ScheduleId);
        }
        public IList<User> GetExternalUserList()
        {

            return _adminController.GetExternalUsers();
        }
        public IList<User> GetUserList()
        {
            return _adminController.GetUserList(_controller.CurrentUser.RegionId);
        }
        public IList<User> GetEnginersList()
        {
            return _adminController.GetEnginersList(_controller.CurrentUser.RegionId);
        }
        public IList<Chai.Maintenance.CoreDomain.Configuration.Site> GetSiteList(int regionId)
        {
            return _confgController.GetSites(0, string.Empty, regionId);
        }
        public IList<Chai.Maintenance.CoreDomain.Configuration.Region> GetRegionList()
        {
            return _confgController.GetRegions(0, string.Empty,_controller.CurrentUser.RegionId);
        }

        public int[] GetLocationId()
        {
            int[] locationId = { 0, 0, 0 }; // 3rd index holds userid
            locationId[2] = _controller.CurrentUser.Id; // user id

            if (_controller.CurrentUser.SiteId != 0)
            {

                locationId[0] = 1; //site = 1
                locationId[1] = _controller.CurrentUser.SiteId;
                return locationId;

            }
            else
            {
                locationId[0] = 2; //region = 2
                locationId[1] = _controller.CurrentUser.RegionId;
                return locationId;

            }
        }
        public IList<Schedule> GetSchedulesByEnginerId(int enginerId)
        {
            return _controller.GetSchedulesByEnginerId(enginerId, "Preventive");
        }


    }
}
