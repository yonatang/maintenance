﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
   public interface ISchedulesListView
    {
       IList<Schedule> _scheduleList { set; }
       int _SiteId { get; }
       int _instCategoryId { get; }
 
       string _type { get; }
       int SiteId { get; }
       int UserId { get; }
    }
}
