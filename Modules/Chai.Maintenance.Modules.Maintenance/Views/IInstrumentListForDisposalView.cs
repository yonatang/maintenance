﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IInstrumentListForDisposalView
    {
        IList<Chai.Maintenance.CoreDomain.Configuration.Instrument> instrumentlist { set; }
        int RegionId { get; }
        int SiteId { get; }
        int InstrumentCatId { get; }
    }
}
