﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IPreventiveMaintenanceListView
    {

        IList<PreventiveMaintenance> _PreventiveMaintenanceList { set; }
        int _SiteId { get; } //parameter from edit Preventive mainteanance page after save, cancel clicked. to keep the selected prev Maintenance
        int _InstrumentCatId { get; }
        int _userId { get; }
        int SiteId { get; } //Current Site Id whome is searching the Maintenance
    }
}
