﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IConsumableNotificationEditView
    {
        ConsumableNotification _consumable { get; set; }
        int ConsumableNotificationIdEdit { get; }
    }
}