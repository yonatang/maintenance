﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface ICurativeMaintenanceListView
    {
        IList<CurativeMaintenance> _CurativeMaintenanceList { set; }
        int _SiteId { get; } //parameter from edit Curative mainteanance page after save, cancel clicked. to keep the selected Curative Maintenance
        int _InstrumentCatId { get; }
        int userId { get; }
        int SiteId { get; } //Current Site Id whome is searching the Maintenance
    }
}
