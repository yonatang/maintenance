﻿
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Shared;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class CurativeMaintenancePresenter : Presenter<ICurativeMaintenanceView>
    {
         private MaintenanceController _controller;
         private ConfigurationController _confgController;
         public CurativeMaintenancePresenter([CreateNew] MaintenanceController controller, [CreateNew] ConfigurationController configcontroller)
          {
             this._controller = controller;
             this._confgController = configcontroller;
          }
         public override void OnViewLoaded()
         {
            
                 View.curative = _controller.CurativeObject;
            
         }

         public override void OnViewInitialized()
         {
             if (View.CurativeIdEdit != 0)
             {
                 _controller.GetCurativeMaintenanceById(View.CurativeIdEdit);
             }
             else
                 _controller.NewCurativeMaintenance();

         }
         public void SaveOrUpdateCurativeMaintenance(CurativeMaintenance curtaive)
         {
             _controller.SaveOrUpdateCurativeMaintenance(curtaive);
         }

         
         
         public IList<SparepartType> GetSparepartType(int insId)
         {
             return _confgController.GetSparepartTypes(3, "", insId);
         }
        
         public void CancelPage()
         {
             _controller.Navigate(String.Format("~/SparepartInventory/RecieveList.aspx?{0}=3", AppConstants.TABID));
         }
         public void DeleteCurativeMaintenance(int curativeid)
         {
             _controller.DeleteCurativeMaintenance(curativeid);
         }
         public void DeleteSpareparts(int spareId)
         {
             _controller.DeleteSpareparts(spareId);
         }
         public Instrument GetInstrumentByIdForProblem(int instrumentId)
         {
             return _confgController.GetInstrumentByIdForProblem(instrumentId);
         }
         public Problem GetProlemById(int problemId)
         {
             return _controller.GetProblemByProblemID(problemId);
         }
         public int[] GetLocationId()
         {
             int[] locationId = { 0, 0, 0 }; // 3rd index holds userid
             locationId[2] = _controller.CurrentUser.Id; // user id

             if (_controller.CurrentUser.SiteId != 0)
             {
                 locationId[0] = 1; //site = 1
                 locationId[1] = _controller.CurrentUser.SiteId;
                 return locationId;
             }
             else
             {
                 locationId[0] = 2; //region = 2
                 locationId[1] = _controller.CurrentUser.RegionId;
                 return locationId;

             }
         }
         //public void UpdateStatus(string updateSentFrom, int status, int Id, string type)
         //{
         //    _controller.UpdateStatus(updateSentFrom, status, Id, type);
         //}
    }
}
