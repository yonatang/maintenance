﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.Modules.Maintenance.Views;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.Modules.Admin;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class ConsumableNotificationEditPresenter : Presenter<IConsumableNotificationEditView>
    {
        private MaintenanceController _controller;
        private ConfigurationController _confgController;
      private AdminController _AdminController;
      public ConsumableNotificationEditPresenter([CreateNew] MaintenanceController controller, [CreateNew] ConfigurationController configcontroller, [CreateNew] AdminController AdminController)
      {
          this._controller = controller;
          this._confgController = configcontroller;
          this._AdminController = AdminController;
      }

        public override void OnViewLoaded()
        {

            View._consumable = _controller.ConsumableNotificationObject;
        }
        public override void OnViewInitialized()
        {
            if (View.ConsumableNotificationIdEdit != 0)
                _controller.GetConsumableNotificationById(View.ConsumableNotificationIdEdit);
            else
                _controller.NewConsumableNotification();
        }
        public void SaveOrUpdateConsumableNotification(ConsumableNotification consumableNotification)
        {
            _controller.SaveOrUpdateConsumableNotification(consumableNotification);
        }

        public IList<Chai.Maintenance.CoreDomain.Configuration.Consumables> GetConsumablesList()
        {
            return _confgController.GetConsumables(string.Empty);
        }
        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Maintenance/ConsumableNotificationList.aspx?{0}=3", AppConstants.TABID));
        }
        public void DeleteConsumableNotification(int consumableNotificationId)
        {
            _controller.DeleteConsumableNotification(consumableNotificationId);
        }
       
        public int[] GetLocationId()
        {
            int[] locationId = { 0, 0, 0 }; // 3rd index holds userid
            locationId[2] = _controller.CurrentUser.Id; // user id

            if (_controller.CurrentUser.SiteId != 0)
            {

                locationId[0] = 1; //site = 1
                locationId[1] = _controller.CurrentUser.SiteId;
                return locationId;

            }
            else
            {
                locationId[0] = 2; //region = 2
                locationId[1] = _controller.CurrentUser.RegionId;
                return locationId;

            }
        }

        public User GetUserList(int userID)
        {
            return _AdminController.GetUserListByIDForTransfer(userID);
        }
        public IList<User> GetUserBySiteID(int siteID)
        {
            return _AdminController.GetUserBySiteID(siteID);
        }
        public IList<Site> GetSiteList(int regionid)
        {
            return _confgController.GetSiteByRegionID(regionid);
        }
        
        
    }
}
