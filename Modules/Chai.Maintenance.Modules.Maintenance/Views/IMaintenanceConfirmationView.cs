﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IMaintenanceConfirmationView
    {
        int Id { get; }
        Confirmation _Confirmation { get; set; }
        int InstrumentId { get; }
        int ProblemId { get; }
    }
}
