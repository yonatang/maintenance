﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.Modules.Maintenance.Views;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.Modules.Admin;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class MaintenanceTransferPresenter : Presenter<IMaintenanceTransferView>
    {
        private MaintenanceController _controller;
        private ConfigurationController _confgController;
        private AdminController _AdminController;
        public MaintenanceTransferPresenter([CreateNew] MaintenanceController controller,  ConfigurationController configcontroller, AdminController AdminController)
        {
            this._controller = controller;
            this._confgController = configcontroller;
            this._AdminController = AdminController;
        }
        public override void OnViewLoaded()
        {

            View.transfer = _controller.TransferObject;
        }
        public override void OnViewInitialized()
        {
            if (View.TransferedIdEdit != 0)
                _controller.GetTransferById(View.TransferedIdEdit);
            else
                _controller.NewTransfer();
        }
        public void SaveOrUpdateTransfer(Transfer Transfer)
        {
            _controller.SaveOrUpdateTransfer(Transfer);
        }
        public IList<Chai.Maintenance.CoreDomain.Configuration.Region> GetRegionList()
        {
            return _confgController.GetRegions(0, string.Empty,_controller.CurrentUser.RegionId);
        }
        public IList<Site> GetSiteList(int regionid)
        {
            return _confgController.GetSiteByRegionID(regionid);
        }
        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Maintenance/InstrumentListForTransfer.aspx?{0}=3", AppConstants.TABID));
        }
        public void DeleteTransfer(int TransferId)
        {
            _controller.DeleteTransfer(TransferId);
        }
        public Instrument GetInstrumentById(int instrumentId)
        {
            return _confgController.GetInstrumentByIdForProblem(instrumentId);
        }
        public int[] GetLocationId()
        {
            int[] locationId = { 0, 0, 0 }; // 3rd index holds userid
            locationId[2] = _controller.CurrentUser.Id; // user id

            if (_controller.CurrentUser.SiteId != 0)
            {

                locationId[0] = 1; //site = 1
                locationId[1] = _controller.CurrentUser.SiteId;
                return locationId;

            }
            else
            {
                locationId[0] = 2; //region = 2
                locationId[1] = _controller.CurrentUser.RegionId;
                return locationId;

            }
        }

        public User GetUserList(int userID)
        {
            return _AdminController.GetUserListByIDForTransfer(userID);
        }
        public IList<User> GetUserBySiteID(int siteID)
        {
            return _AdminController.GetUserBySiteID(siteID);
        }
        //public void UpdateStatus(string updateSentFrom, int status, int Id, string type)
        //{
        //    _controller.UpdateStatus(updateSentFrom, status, Id, type);
        //}

        public void SaveTransferedInstrument(Instrument instrument, int OldInstrumentId, int NewSiteId)
        {
            _confgController.SaveTransferedInstrument(instrument, OldInstrumentId, NewSiteId);
        }
    }
}
