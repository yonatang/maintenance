﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
  public interface IProblemNotificationView
    {
        int InstrumentId { get; }
        int Id { get; }
        Problem _Problem { get; set; }

        int InstrumentIdEdit { get; }
        int ProblemIdEdit { get; }
        int UserIdEdit { get; }
    }
}
