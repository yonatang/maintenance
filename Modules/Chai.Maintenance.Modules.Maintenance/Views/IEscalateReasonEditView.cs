﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IEscalateReasonEditView
    {
        int Id { get; }
        EscalateReason EscalateReason { get; set; }
        int ScheduleId { get; }

    }
}
