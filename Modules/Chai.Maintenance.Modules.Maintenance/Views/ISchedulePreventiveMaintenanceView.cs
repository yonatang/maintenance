﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface ISchedulePreventiveMaintenanceView
    {
        int InstrumentId { get; }
        int InstrumentIdEdit { get; }
        int ScheduleIdEdit { get; }
        int Id { get; }
        Schedule _Schedule { get; set; }
    }
}
