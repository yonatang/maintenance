﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Maintenance;
using Chai.Maintenance.Modules.Maintenance.Views;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Modules.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class ProblemNotificationPresenter : Presenter<IProblemNotificationView>
    {
        private MaintenanceController _controller;
        private ConfigurationController _confgController;
        private Problem _Problem;
        public ProblemNotificationPresenter([CreateNew] MaintenanceController controller, ConfigurationController confgController)
        {
            _controller = controller;
            _confgController = confgController;
        }
        public override void OnViewLoaded()
        {

            View._Problem = _controller.ProblemObject;
        }
        public override void OnViewInitialized()
        {
            if (View.ProblemIdEdit != 0)
                _controller.GetProblemById(View.ProblemIdEdit);
            else
                _controller.NewProblem();
        }
        public void SaveOrUpdateProblem(Problem problem, bool NotFunctional)
        {
            _controller.SaveOrUpdateProblem(problem, NotFunctional);
        }
        public Instrument GetInstrumentByIdForProblem(int instrumentId)
        {
            return _confgController.GetInstrumentByIdForProblem(instrumentId);
        }
        public IList<ProblemType> GetProblemTypeList()
        {
            return _confgController.GetProblemTypes(string.Empty);
        }
        public IList<Instrument> GetAllInstrument()
        {
            return _confgController.GetInstruments(0, string.Empty, 0, 0,_controller.CurrentUser.RegionId);
        }
        public IList<ErrorCode> GetErrorCodeListByProblemTypeID(int ProblemTypeID)
        {
            return _confgController.GetErrorCodeListByProblemTypeID(ProblemTypeID);
        }
        public void CancelPage(int instrumentID, int SiteId)
        {
            _controller.Navigate(String.Format("~/Maintenance/MaintenanceInstrumentList.aspx?{0}=2&instrumentID={1}&SiteID={2}", AppConstants.TABID, instrumentID, SiteId));         
        }
        public void DeleteProblem(int problemId)
        {
            _controller.DeleteProblem(problemId);
        }
        //public void UpdateStatus(string updateSentFrom, int status, int Id, string type)
        //{
        //    _controller.UpdateStatus(updateSentFrom, status, Id, type);
        //}
        public AutoNumber GetAutoNumberByPageId(int pageId)
        {
            return _confgController.GetAutoNumberByPageId(pageId);
        }
       
    }
}
