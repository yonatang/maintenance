﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IConsumableNotificationListView
    {
        IList<ConsumableNotification> ConsumableNotificationList { set; }
        
        int ConsumablesId { get; }
        int UserId { get; }
        int SiteId { get; }  
    }
}
