﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public class MaintenanceInstrumentListPresenter : Presenter<IMaintenanceInstrumentListView>
    {

        private Chai.Maintenance.Modules.Configuration.ConfigurationController _configurationController;
        private Chai.Maintenance.Modules.Maintenance.MaintenanceController _maintenanceController;
        public MaintenanceInstrumentListPresenter([CreateNew] Chai.Maintenance.Modules.Configuration.ConfigurationController controller, [CreateNew] Chai.Maintenance.Modules.Maintenance.MaintenanceController maintcontroller)
        {
            _configurationController = controller;
            _maintenanceController = maintcontroller;

        }
        
      
        public override void OnViewLoaded()
        {
            View.instrumentlist = _maintenanceController.GetInstrumentByRSIIG(View.regionId,View.siteId, View._instrumentID,
                                                                              View.instrumentCategoryID, View.Functionality);
        }

        public override void OnViewInitialized()
        {

        }
        public void GetInstrumentByRSIIG(int regionId, int siteId, int instrumentId, int instrumentCategoryId, int functionality)
        {
            if (View._siteId == 0)
                _maintenanceController.GetInstrumentByRSIIG(regionId,siteId, instrumentId, instrumentCategoryId, functionality);
            else
                _maintenanceController.GetInstrumentByRSIIG(View.regionId, View._siteId, 0, instrumentCategoryId, functionality);
        }

        public IList<Chai.Maintenance.CoreDomain.Configuration.Site> GetSiteList(int regionId)
        {
            return _configurationController.GetSiteByRegionID(regionId);
        }
        public IList<Chai.Maintenance.CoreDomain.Configuration.Region> GetRegionList()
        {
            return _configurationController.GetRegions(0, string.Empty,_maintenanceController.CurrentUser.RegionId);
        }

        public IList<Chai.Maintenance.CoreDomain.Configuration.InstrumentCategory> GetInstrumentCategoryList()
        {
            return _configurationController.GetInstrumentCategorys(string.Empty);
        }

        public IList<Chai.Maintenance.CoreDomain.Configuration.Manufacturer> GetManufacturerList()
        {
            return _configurationController.GetManufacturers(string.Empty);
        }
       
        public int[] GetLocationId()
        {
            int[] locationId = { 0, 0, 0 }; // 3rd index holds userid
            locationId[2] = _maintenanceController.CurrentUser.Id; // user id

            if (_maintenanceController.CurrentUser.SiteId != 0)
            {

                locationId[0] = 1; //site = 1
                locationId[1] = _maintenanceController.CurrentUser.SiteId;
                return locationId;

            }
            else
            {
                locationId[0] = 2; //region = 2
                 locationId[1] = _maintenanceController.CurrentUser.RegionId;
                return locationId;

            }
        }
        public int ReturnRegionId()
        {
            return _maintenanceController.CurrentUser.RegionId;
        }
        public void NewPage()
        {
            _maintenanceController.Navigate(String.Format("~/Admin/InstrumentEdit.aspx?{0}=0", AppConstants.TABID));
        }
        public Instrument GetInstrumentById(int instrumentId)
        {
            return _configurationController.GetInstrumentByIdForProblem(instrumentId);
        }
    }
}
