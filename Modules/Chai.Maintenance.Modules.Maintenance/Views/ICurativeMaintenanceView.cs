﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
  public interface ICurativeMaintenanceView
    {
      int Id { get; }
      CurativeMaintenance curative { get; set; }
      int InstrumentId { get; }
      int ProblemId { get; }
      int ScheduleId { get; }
      int DataMode { get; }


      int CurativeIdEdit { get; }
      int InstrumentIdEdit { get; }
      int ProblemIdEdit { get; }
      int ScheduleIdEdit { get; }
    }
}
