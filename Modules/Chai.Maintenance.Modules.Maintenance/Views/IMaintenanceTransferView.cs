﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IMaintenanceTransferView
    {
        int Id { get; }
        Transfer transfer { get; set; }
        int InstrumentId { get; }
        int CurrentSiteId { get; }

        int InstrumentIdEdit {get;}
        int TransferedIdEdit{get;}
    }
}
