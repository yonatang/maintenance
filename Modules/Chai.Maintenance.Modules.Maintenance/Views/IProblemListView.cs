﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IProblemListView
    {

        IList<Problem> _ProblemList { set; }
        int _SiteId { get; }
        int _InstrumentCatId { get; }
        int regionId { get; }
        int _ProblemType { get; }
        int SiteId { get; }
    }
}
