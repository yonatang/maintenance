﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance.Views
{
    public interface IPreventiveMaintenanceView
    {
        int Id { get; }
        PreventiveMaintenance preventive { get; set; }
        int InstrumentId { get; }
        int ScheduleId { get; }


        int InstrumentIdEdit { get; }
        int PreventiveIdEdit { get; }
        int ScheduleIdEdit { get; }
    }
}
