﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.CompositeWeb.Utility;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Shared.Navigation;
using Chai.Maintenance.Services;
using Chai.Maintenance.Services.Maintenance;
namespace Chai.Maintenance.Modules.Maintenance
{

    public class MaintenanceController
    {
        private IHttpContextLocatorService _httpContextLocatorService;
        private INavigationService _navigationService;
        private  ProblemServices _problemServices;
        private ScheduleServices _scheduleServices;
        private AssignedJobsServices _assignedJobsServices;
        private ISessionStateLocatorService _sessionstate;
        private CurativeMaintenanceServices _curativeServices;
        private PreventiveMaintenanceServices _preventiveServices;
        private SparepartsServices _spareServices;
        private TransferServices _TransferServices;
        private DisposalServices _DisposalServices;
        private ConfirmationServices _ConfirmationServices;
        private ConsumableNotificationServices _consumableNotificationServices;
        private PreventiveMaintenanceChecklistServices _preventiveMaintenanceChecklistServices;
        private Chai.Maintenance.Services.Configuration.InstrumentServices _instrumentservices;
        private EscalatedJobsServices _escalatedJobsServices;
        private EscalateReasonServices _escalateReasonServices;
        [InjectionConstructor]
        public MaintenanceController([ServiceDependency] IHttpContextLocatorService httpContextLocatorService,
            [ServiceDependency] INavigationService navigationService, [ServiceDependency] ISessionStateLocatorService sessionstate)
        {
            _httpContextLocatorService = httpContextLocatorService;
            _navigationService = navigationService;
            _sessionstate = sessionstate;
        }

        public User CurrentUser
        {
            get { return _httpContextLocatorService.GetCurrentContext().User.Identity as User; }
        }
        public void Navigate(string to)
        {
            _navigationService.Navigate(to);
        }
        #region Instrument List
        public IList<Chai.Maintenance.CoreDomain.Configuration.Instrument> GetInstrumentByRSIIG(int regionId, int siteId, int instrumentId, int instrumentCategoryId, int functionality)
        {
          
             
            if (_instrumentservices == null)
                _instrumentservices = new Chai.Maintenance.Services.Configuration.InstrumentServices();

            return _instrumentservices.GetInstrumentByRSIIG(regionId, siteId, instrumentId, instrumentCategoryId, functionality);

        }

        #endregion

        #region Problem
        public IList<Problem> GetProblemBySiteInsCategoryId(int regionId, int SiteId, int InstrumentCat, int ProblemType)
        {
            if (_problemServices == null)
                _problemServices = new ProblemServices();

            return _problemServices.GetProblemBySiteInsCategoryId(regionId, SiteId, InstrumentCat, ProblemType);
        }
        public Problem ProblemObject
        {
            get
            {
                Problem _Problem = _sessionstate.GetSessionState()["Problem"] as Problem;

                if (_Problem != null)
                    return _Problem;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["Problem"] = value;

            }
        }


        public IList<Problem> GetProblems(int siteid, int problemId)
        {

            if (_problemServices == null)
                _problemServices = new ProblemServices();

            return _problemServices.GetAllProblems(siteid, problemId);

        }
        
        public IList<Problem> GetProblemForCurativeSchedule(int regionId, int siteId, int InstrumentCategoryId, int ProblemType)
        {
            if (_problemServices == null)
                _problemServices = new ProblemServices();

            return _problemServices.GetProblemForCurativeSchedule(regionId, siteId, InstrumentCategoryId, ProblemType);
        }
        public void GetProblemById(int Problemid)
        {
            if (_problemServices == null)
                _problemServices = new ProblemServices();

            this.ProblemObject = _problemServices.GetProblemById(Problemid);
        }
        public Problem GetProblemByProblemID(int problemId)
        {
            if (_problemServices == null)
                _problemServices = new ProblemServices();
            return _problemServices.GetProblemById(problemId);
        }
        public void SaveOrUpdateProblem(Problem Problem, bool NotFunctional)
        {

            if (_problemServices == null)
                _problemServices = new ProblemServices();
            _problemServices.SaveOrUpdateProblem(Problem, NotFunctional);
            this.ProblemObject = Problem;
        }
        public void NewProblem()
        {
            Problem Problem = new Problem();
            this.ProblemObject = Problem;
        }
        public void DeleteProblem(int Problemid)
        {
            if (_problemServices == null)
                _problemServices = new ProblemServices();
            _problemServices.DeleteProblem(Problemid);
        }
        #endregion

        #region   Schedule

        public Schedule ScheduleObject
        {
            get
            {
                Schedule _Schedule = _sessionstate.GetSessionState()["Schedule"] as Schedule;

                if (_Schedule != null)
                    return _Schedule;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["Schedule"] = value;

            }
        }


        //public IList<Schedule> GetSchedules(int siteid, int scheduleId)
        //{

        //    if (_scheduleServices == null)
        //        _scheduleServices = new ScheduleServices();

        //    return _scheduleServices.GetAllSchedules(siteid, scheduleId);

        //}

        public IList<Schedule> GetScheduleBySiteInsCategoryId(int InstrumentCat, string type, int userId)
        {
            if (_scheduleServices == null)
                _scheduleServices = new ScheduleServices();

            return _scheduleServices.GetScheduleBySiteInsCategoryId(InstrumentCat, type, userId);
        }
        
        
        public void GetScheduleById(int ScheduleId)
        {
            if (_scheduleServices == null)
                _scheduleServices = new ScheduleServices();

            this.ScheduleObject = _scheduleServices.GetScheduleById(ScheduleId);
        }

        public void SaveOrUpdateSchedule(Schedule _Schedule, string type, int escalateId)
        {

            if (_scheduleServices == null)
                _scheduleServices = new ScheduleServices();
            _scheduleServices.SaveOrUpdateSchedule(_Schedule, type, escalateId);
            this.ScheduleObject = _Schedule;
        }
        public void NewSchedule()
        {
            Schedule Schedule = new Schedule();
            this.ScheduleObject = Schedule;
        }
        public void DeleteSchedule(int ScheduleId)
        {
            if (_scheduleServices == null)
                _scheduleServices = new ScheduleServices();
            _scheduleServices.DeleteSchedule(ScheduleId);
        }
        #endregion

        #region Assigned Jobs
        public IList<AssignedJobs> GetAssignedJobs(string searchBy, int UserId, int siteid)
        {

            if (_assignedJobsServices == null)
                _assignedJobsServices = new AssignedJobsServices();

            return _assignedJobsServices.GetAssignedJobs(searchBy, UserId, siteid);

        }
         #endregion
        #region Escalated Jobs
        public IList<EscalatedJobs> GetEscalatedJobsByRegionSite(int regionId, int siteId)
        {

            if (_escalatedJobsServices == null)
                _escalatedJobsServices = new EscalatedJobsServices();

            return _escalatedJobsServices.GetEscalatedJobsByRegionSite(regionId, siteId);

        }
        #endregion
        #region Curative Maintenance
        public CurativeMaintenance CurativeObject
        {
            get
            {
                CurativeMaintenance _curative = _sessionstate.GetSessionState()["CurativeMaintenance"] as CurativeMaintenance;

                if (_curative != null)
                    return _curative;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["CurativeMaintenance"] = value;

            }
        }


        public IList<CurativeMaintenance> GetCurativeMaintenances(string ProlemNumber, int InstrumentID)
        {

            if (_curativeServices == null)
                _curativeServices = new CurativeMaintenanceServices();

            return _curativeServices.GetAllCurativeMaintenance(ProlemNumber, InstrumentID);

        }

        public void GetCurativeMaintenanceById(int curativeId)
        {
            if (_curativeServices == null)
                _curativeServices = new CurativeMaintenanceServices();

            this.CurativeObject = _curativeServices.GetCurativeMaintenanceById(curativeId);
        }

        public void SaveOrUpdateCurativeMaintenance(CurativeMaintenance curative)
        {

            if (_curativeServices == null)
                _curativeServices = new CurativeMaintenanceServices();
            _curativeServices.SaveOrUpdateCurativeMaintenance(curative);
            this.CurativeObject = curative;
        }
        public void NewCurativeMaintenance()
        {
            CurativeMaintenance curative = new CurativeMaintenance();
            this.CurativeObject = curative;
        }
        public void DeleteCurativeMaintenance(int CurativeId)
        {
            if (_curativeServices == null)
                _curativeServices = new CurativeMaintenanceServices();
            _curativeServices.DeleteCurativeMaintenance(CurativeId);
        }
        public void DeleteSpareparts(int Id)
        {
            _spareServices = new SparepartsServices();
            _spareServices.DeleteSpareparts(Id);
        }
        public IList<CurativeMaintenance> GetCurativeMaintetanceBySiteInsCategoryId(int SiteId, int InstrumentCat, int userId)
         {
             if (_curativeServices == null)
                 _curativeServices = new CurativeMaintenanceServices();

             return _curativeServices.GetCurativeMaintetanceBySiteInsCategoryId(SiteId, InstrumentCat, userId);
         }
        #endregion
        

        #region Update status


        //public void UpdateStatus(string updateSentFrom, int status, int Id, string type)
        //{
        //    if (_problemServices == null)
        //        _problemServices = new ProblemServices();
        //    _problemServices.UpdateStatus(updateSentFrom, status, Id, type);
        //}
        #endregion

        #region Preventive Maintenance List
         public IList<Chai.Maintenance.CoreDomain.Configuration.Instrument> GetInstrumentForPreventiveMaint(int regionId, int siteId, int instrumentCategoryId, string LastPrevMainDate)
        { 
            if (_instrumentservices == null)
                _instrumentservices = new Chai.Maintenance.Services.Configuration.InstrumentServices();

            return _instrumentservices.GetInstrumentForPreventiveMaint(regionId, siteId, instrumentCategoryId, LastPrevMainDate);

         }

        
        #endregion

        #region Transfer List

      
         public IList<Chai.Maintenance.CoreDomain.Configuration.Instrument> GetInstrumentForTransfer(int regionId, int siteId, int instrumentCategoryId)
         {
             if (_instrumentservices == null)
                 _instrumentservices = new Chai.Maintenance.Services.Configuration.InstrumentServices();

             return _instrumentservices.GetInstrumentForTransfer(regionId, siteId, instrumentCategoryId);

         }
         #endregion

        #region Preventive Maintenance
         public PreventiveMaintenance PreventiveObject
         {
             get
             {
                 PreventiveMaintenance _Preventive = _sessionstate.GetSessionState()["PreventiveMaintenance"] as PreventiveMaintenance;

                 if (_Preventive != null)
                     return _Preventive;
                 return null;
             }
             set
             {
                 _sessionstate.GetSessionState()["PreventiveMaintenance"] = value;

             }
         }


         public IList<PreventiveMaintenance> GetPreventiveMaintenance(int InstrumentID)
         {

             if (_preventiveServices == null)
                 _preventiveServices = new PreventiveMaintenanceServices();

             return _preventiveServices.GetAllPreventiveMaintenance(InstrumentID);

         }

         public void GetPreventiveMaintenanceById(int preventiveId)
         {
             if (_preventiveServices == null)
                 _preventiveServices = new PreventiveMaintenanceServices();

             this.PreventiveObject = _preventiveServices.GetPreventiveMaintenanceById(preventiveId);
         }

         public void SaveOrUpdatePreventiveMaintenance(PreventiveMaintenance preventive)
         {

             if (_preventiveServices == null)
                 _preventiveServices = new PreventiveMaintenanceServices();
             _preventiveServices.SaveOrUpdatePreventiveMaintenance(preventive);
             this.PreventiveObject = preventive;
         }
         public void NewPreventiveMaintenance()
         {
             PreventiveMaintenance preventive = new PreventiveMaintenance();
             this.PreventiveObject = preventive;
         }
         public void DeletePreventiveMaintenance(int curativeId)
         {
             if (_preventiveServices == null)
                 _preventiveServices = new PreventiveMaintenanceServices();
             _preventiveServices.DeletePreventiveMaintenance(curativeId);
         }
         public void DeletePreventiveMaintenanceSpareparts(int Id)
         {
             _spareServices = new SparepartsServices();
             _spareServices.DeleteSpareparts(Id);
         }
         public IList<PreventiveMaintenance> GetPreventiveMaintetanceBySiteInsCategoryId(int SiteId, int InstrumentCat, int userId)
         {
             if (_preventiveServices == null)
                 _preventiveServices = new PreventiveMaintenanceServices();

             return _preventiveServices.GetPreventiveMaintetanceBySiteInsCategoryId(SiteId, InstrumentCat, userId);
         }
         #endregion

        #region Transfer

         public Transfer TransferObject
         {
             get
             {
                 Transfer _Transfer = _sessionstate.GetSessionState()["Transfer"] as Transfer;

                 if (_Transfer != null)
                     return _Transfer;
                 return null;
             }
             set
             {
                 _sessionstate.GetSessionState()["Transfer"] = value;

             }
         }


        

         public IList<Transfer> GetTransfer(int siteId)
         {
             if (_TransferServices == null)
                 _TransferServices = new TransferServices();

             return _TransferServices.GetAllTransfers(siteId);
         }
         public void GetTransferById(int Transferid)
         {
             if (_TransferServices == null)
                 _TransferServices = new TransferServices();

             this.TransferObject = _TransferServices.GeTransferById(Transferid);
         }

         public void SaveOrUpdateTransfer(Transfer _Transfer)
         {

             if (_TransferServices == null)
                 _TransferServices = new TransferServices();
             _TransferServices.SaveOrUpdateTransfer(_Transfer);
             this.TransferObject = _Transfer;
         }
         public void NewTransfer()
         {
             Transfer Transfer = new Transfer();
             this.TransferObject = Transfer;
         }
         public void DeleteTransfer(int Transferid)
         {
             if (_TransferServices == null)
                 _TransferServices = new TransferServices();
             _TransferServices.DeleteTransfer(Transferid);
         }
         public IList<Transfer> GetTransferBySiteInsCategoryId(int UserId, int InstrumentCat)
         {
             if (_TransferServices == null)
                 _TransferServices = new TransferServices();

             return _TransferServices.GetTransferBySiteInsCategoryId(UserId, InstrumentCat);
         }
         #endregion

        #region Disposal

         public Disposal DisposalObject
         {
             get
             {
                 Disposal _Disposal = _sessionstate.GetSessionState()["Disposal"] as Disposal;

                 if (_Disposal != null)
                     return _Disposal;
                 return null;
             }
             set
             {
                 _sessionstate.GetSessionState()["Disposal"] = value;

             }
         }




         //public IList<Disposal> GetDisposal(int siteId)
         //{
         //    if (_DisposalServices == null)
         //        _DisposalServices = new DisposalServices();

         //    return _DisposalServices.GetDisposalById(siteId);
         //}
         public void GetDisposalById(int Disposalid)
         {
             if (_DisposalServices == null)
                 _DisposalServices = new DisposalServices();

             this.DisposalObject = _DisposalServices.GetDisposalById(Disposalid);
         }

         public void SaveOrUpdateDisposal(Disposal _Disposal)
         {

             if (_DisposalServices == null)
                 _DisposalServices = new DisposalServices();
             _DisposalServices.SaveOrUpdateDisposal(_Disposal);
             this.DisposalObject = _Disposal;
         }
         public void NewDisposal()
         {
             Disposal Disposal = new Disposal();
             this.DisposalObject = Disposal;
         }
         public void DeleteDisposal(int Disposalid)
         {
             if (_DisposalServices == null)
                 _DisposalServices = new DisposalServices();
             _DisposalServices.DeleteDisposal(Disposalid);
         }
         public IList<Disposal> GetDisposalBySiteInsCategoryId(int SiteId, int InstrumentCat, int userId)
         {
             if (_DisposalServices == null)
                 _DisposalServices = new DisposalServices();

             return _DisposalServices.GetDisposalBySiteInsCategoryId(SiteId, InstrumentCat, userId);
         }
        
         #endregion

        #region Confirmation

         public Confirmation ConfirmationObject
         {
             get
             {
                 Confirmation _Confirmation = _sessionstate.GetSessionState()["Confirmation"] as Confirmation;

                 if (_Confirmation != null)
                     return _Confirmation;
                 return null;
             }
             set
             {
                 _sessionstate.GetSessionState()["Confirmation"] = value;

             }
         }




        
         public void GetConfirmationById(int ConfirmationId)
         {
             if (_ConfirmationServices == null)
                 _ConfirmationServices = new ConfirmationServices();

             this.ConfirmationObject = _ConfirmationServices.GetConfirmationById(ConfirmationId);
         }

         public void SaveOrUpdateConfirmation(Confirmation _Confirmation,string searchBy, int ScheduleId)
         {

             if (_ConfirmationServices == null)
                 _ConfirmationServices = new ConfirmationServices();
             _ConfirmationServices.SaveOrUpdateConfirmation(_Confirmation, searchBy, ScheduleId);
             this.ConfirmationObject = _Confirmation;
         }
         public void NewConfirmation()
         {
             Confirmation _Confirmation = new Confirmation();
             this.ConfirmationObject = _Confirmation;
         }
         public void DeleteConfirmation(int Confirmationid)
         {
             if (_ConfirmationServices == null)
                 _ConfirmationServices = new ConfirmationServices();
             _ConfirmationServices.DeleteConfirmation(Confirmationid);
         }
         #endregion

         #region Consumbale Notification

         public ConsumableNotification ConsumableNotificationObject
         {
             get
             {
                 ConsumableNotification _consumableNotification = _sessionstate.GetSessionState()["ConsumableNotification"] as ConsumableNotification;

                 if (_consumableNotification != null)
                     return _consumableNotification;
                 return null;
             }
             set
             {
                 _sessionstate.GetSessionState()["ConsumableNotification"] = value;

             }
         }

         public void GetConsumableNotificationById(int consumableNotificationId)
         {
             if (_consumableNotificationServices == null)
                 _consumableNotificationServices = new ConsumableNotificationServices();

             this.ConsumableNotificationObject = _consumableNotificationServices.GetConsumableNotificationById(consumableNotificationId);
         }

         public void SaveOrUpdateConsumableNotification(ConsumableNotification consumableNotification)
         {

             if (_consumableNotificationServices == null)
                 _consumableNotificationServices = new ConsumableNotificationServices();
             _consumableNotificationServices.SaveOrUpdateConsumableNotification(consumableNotification);
             this.ConsumableNotificationObject = consumableNotification;
         }
         public void NewConsumableNotification()
         {
             ConsumableNotification _consumableNotification = new ConsumableNotification();
             this.ConsumableNotificationObject = _consumableNotification;
         }
         public void DeleteConsumableNotification(int consumableNotificationId)
         {
             if (_consumableNotificationServices == null)
                 _consumableNotificationServices = new ConsumableNotificationServices();
             _consumableNotificationServices.DeleteConsumableNotification(consumableNotificationId);
         }
         public IList<ConsumableNotification> GetConsumableNotificationBySiteConsumableId(int siteId, int consumableId, int notifiedbyId)
         {
             if (_consumableNotificationServices == null)
                 _consumableNotificationServices = new ConsumableNotificationServices();

             return _consumableNotificationServices.GetConsumableNotificationBySiteConsumableId(siteId, consumableId, notifiedbyId);
         }

         #endregion


         #region Preventive Maintenance Checklist

         public PreventiveMaintenanceChecklist PreventiveMaintenanceChecklistObject
         {
             get
             {
                 PreventiveMaintenanceChecklist _checklist = _sessionstate.GetSessionState()["PreventiveMaintenanceChecklist"] as PreventiveMaintenanceChecklist;

                 if (_checklist != null)
                     return _checklist;
                 return null;
             }
             set
             {
                 _sessionstate.GetSessionState()["PreventiveMaintenanceChecklist"] = value;

             }
         }

         public void GetPreventiveMaintenanceChecklistById(int checklistId)
         {
             if (_preventiveMaintenanceChecklistServices == null)
                 _preventiveMaintenanceChecklistServices = new PreventiveMaintenanceChecklistServices();

             this.PreventiveMaintenanceChecklistObject = _preventiveMaintenanceChecklistServices.GetPreventiveMaintenanceChecklistById(checklistId);
         }

         public void SaveOrUpdatePreventiveMaintenanceChecklist(PreventiveMaintenanceChecklist checklist)
         {

             if (_preventiveMaintenanceChecklistServices == null)
                 _preventiveMaintenanceChecklistServices = new PreventiveMaintenanceChecklistServices();
             _preventiveMaintenanceChecklistServices.SaveOrUpdatePreventiveMaintenanceChecklist(checklist);
             this.PreventiveMaintenanceChecklistObject = checklist;
         }
         public void NewPreventiveMaintenanceChecklist()
         {
             PreventiveMaintenanceChecklist checklist = new PreventiveMaintenanceChecklist();
             this.PreventiveMaintenanceChecklistObject = checklist;
         }
         public void DeletePreventiveMaintenanceChecklist(int checklistId)
         {
             if (_preventiveMaintenanceChecklistServices == null)
                 _preventiveMaintenanceChecklistServices = new PreventiveMaintenanceChecklistServices();
             _preventiveMaintenanceChecklistServices.DeletePreventiveMaintenanceChecklist(checklistId);
         }
         #endregion



         #region Escalate Reason

         public EscalateReason EscalateReasonObject
         {
             get
             {
                 EscalateReason escalateReason = _sessionstate.GetSessionState()["EscalateReason"] as EscalateReason;

                 if (escalateReason != null)
                     return escalateReason;
                 return null;
             }
             set
             {
                 _sessionstate.GetSessionState()["EscalateReason"] = value;

             }
         }

         public void GetEscalateReasonById(int escalateReasonId)
         {
             if (_escalateReasonServices == null)
                 _escalateReasonServices = new EscalateReasonServices();

             this.EscalateReasonObject = _escalateReasonServices.GetEscalateReasonById(escalateReasonId);
         }

         public void SaveOrUpdateEscalateReason(EscalateReason escalateReason)
         {
             if (_escalateReasonServices == null)
                 _escalateReasonServices = new EscalateReasonServices();
             _escalateReasonServices.SaveOrUpdateEscalateReason(escalateReason);
             this.EscalateReasonObject = escalateReason;
         }

         public void NewEscalateReason()
         {
             EscalateReason escalateReason = new EscalateReason();
             this.EscalateReasonObject = escalateReason;
         }
         public void DeleteEscalateReason(int escalateReasonId)
         {
             if (_escalateReasonServices == null)
                 _escalateReasonServices = new EscalateReasonServices();
             _escalateReasonServices.DeleteEscalateReason(escalateReasonId);
         }
         public EscalateReason GetEscalateReasonByProblemIdANDScheduleId(int problemId, int scheduleId)
         {
             if (_escalateReasonServices == null)
                 _escalateReasonServices = new EscalateReasonServices();
             return _escalateReasonServices.GetEscalateReasonByProblemIdANDScheduleId(problemId, scheduleId);
         }
         #endregion

         public IList<Schedule> GetSchedulesByEnginerId(int enginerId, string maintenanceType)
         {
             if (_scheduleServices == null)
                 _scheduleServices = new ScheduleServices();

             return _scheduleServices.GetSchedulesByEnginerId(enginerId, maintenanceType);
         }
        
   
    }
}
