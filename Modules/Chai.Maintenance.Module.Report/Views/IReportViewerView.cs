﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.Modules.Report.Views
{
    public interface IReportViewerView
    {
        User User { set; }
    }
}
