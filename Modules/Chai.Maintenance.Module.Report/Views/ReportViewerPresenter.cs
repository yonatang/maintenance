﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using System.Data;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Report.Views
{
    public class ReportViewerPresenter : Presenter<IReportViewerView>
    {
        private ReportController _controller;
        private ConfigurationController _configcontroller;
        public ReportViewerPresenter([CreateNew] ReportController controller,[CreateNew] ConfigurationController configcontroller)
        {
            this._controller = controller;
            this._configcontroller = configcontroller;
        }
        public override void OnViewLoaded()
        {
            View.User = _controller.CurrentUser;
        }

        public override void OnViewInitialized()
        {
           
        }
        public DataSet GetregionReport()
        {
            return _controller.GetRegionReport();

        }
        public DataSet GetSiteReport(int regionId, int userregionId, int siteTypeId)
        {
            return _controller.GetSiteReport(regionId, userregionId, siteTypeId);
        }
        public DataSet GetSiteContactReport(int regionId, int userregionId, int siteId)
        {

            return _controller.GetSiteContactReport(regionId, userregionId, siteId);
        }
        public DataSet GetInstrumentReport(int regionId, int userregionId, int siteId)
        {

            return _controller.GetInstrumentReport(regionId, userregionId, siteId);
        }
        public DataSet GetUpcomeingCurativeReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {

            return _controller.GetUpcomeingCurativeReport(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetUpcomeingPreventiveReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {

            return _controller.GetUpcomeingPreventiveReport(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetTransferReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {

            return _controller.GetTransferReport(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetDisposalReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {

            return _controller.GetDisposalReport(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetPerformedPreventiveMaintenance(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {

            return _controller.GetPerformedPreventiveMaintenance(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetPerformedCurativeMaintenance(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {

            return _controller.GetPerformedCurativeMaintenance(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetNonFunctionalInstrumentsBySiteANDInstrumentCategory(int regionId, int userregionId, int siteId, int instrumentcatagoryId)
        {
           
            return _controller.GetNonFunctionalInstrumentsBySiteANDInstrumentCategory(regionId, userregionId, siteId, instrumentcatagoryId);
        }
        public DataSet GetInstrumentProblemByManufacturer(int userregionId, int instrumentcatagoryId, int manufactureid)
        {

            return _controller.GetInstrumentProblemByManufacturer(userregionId, instrumentcatagoryId, manufactureid);
        }
        public DataSet GetQuarterPreventiveReport(int regionId, int userregionId, DateTime datefrom, DateTime dateto)
        {

            return _controller.GetQuarterPreventiveReport(regionId, userregionId, datefrom, dateto);
        }
        public DataSet GetQuarterCurativeReport(int regionId, int userregionId, DateTime datefrom, DateTime dateto)
        {

            return _controller.GetQuarterCurativeReport(regionId, userregionId, datefrom, dateto);
        }
        public DataSet GetEscalatedCurativeMaintenances(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            return _controller.GetEscalatedCurativeMaintenances(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetNotifiedConsumables(int regionId, int userregionId, int siteId, int consumableId)
        {

            return _controller.GetNotifiedConsumables(regionId, userregionId, siteId, consumableId);                                                               
        }
        public DataSet GetInstrumentUnderContract(int regionId, int userregionId, int siteId)
        {
            return _controller.GetInstrumentUnderContract(regionId, userregionId, siteId);          
        }
        public DataSet GetCurativeMaintenanceTracking(int regionId, int userregionId, int siteId)
        {
            return _controller.GetCurativeMaintenanceTracking(regionId, userregionId, siteId);
        }
        public IList<SiteType> GetSiteType()
        {
            return _configcontroller.GetSiteTypes("");
        }
        public IList<Region> GetRegion()
        {
            return _configcontroller.GetRegions(0, string.Empty, _controller.CurrentUser.RegionId);
        }
        public IList<Site> GetSiteList(int regionid)
        {
            return _configcontroller.GetSiteByRegionID(regionid);
        }
        public IList<InstrumentCategory> GetInstrumentCatgory()
        {
            return _configcontroller.GetInstrumentCategorys("");
            
        }
        public IList<Manufacturer> GetManufacturer()
        {
            return _configcontroller.GetManufacturers("");
        }
        public  IList<Consumables> GetConsumables()
        {
            return _configcontroller.GetConsumables("");
        }
    }
}
