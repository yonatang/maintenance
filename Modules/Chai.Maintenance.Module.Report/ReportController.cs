﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.CompositeWeb.Utility;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.Services.Report;
using Chai.Maintenance.Shared.Navigation;
using Chai.Maintenance.Services;
using Chai.Maintenance.CoreDomain;
using System.Data;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Services.Configuration;
using System.Collections;
namespace Chai.Maintenance.Modules.Report
{
    public class ReportController
    
    {
        private IHttpContextLocatorService _httpContextLocatorService;
        private INavigationService _navigationService;
        private RoleServices _roleServices;
        private ISessionStateLocatorService _sessionstate;
        private UserLocationServices _userlocationservices;
        private UserServices _userservices;
        private ReportServices _reportservices;
      
        [InjectionConstructor]
        public ReportController([ServiceDependency] IHttpContextLocatorService httpContextLocatorService,
            [ServiceDependency] INavigationService navigationService)
        {
            _httpContextLocatorService = httpContextLocatorService;
            _navigationService = navigationService;
            
        }

        public User CurrentUser
        {
            get { return _httpContextLocatorService.GetCurrentContext().User.Identity as User; }
        }

        public void Navigate(string to)
        {
            _navigationService.Navigate(to);
        }

        public DataSet GetRegionReport()
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetRegionReport();
        }
        public DataSet GetSiteReport(int regionId,int userregionId,int sitetypeId)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetSiteReport(regionId, userregionId, sitetypeId);
        }
        public DataSet GetSiteContactReport(int regionId, int userregionId, int siteId)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetSiteContactReport(regionId, userregionId, siteId);
        }
        public DataSet GetInstrumentReport(int regionId, int userregionId, int siteId)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetInstrumentReport(regionId, userregionId, siteId);
        }
        public DataSet GetUpcomeingCurativeReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetUpcomeingCurativeReport(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetUpcomeingPreventiveReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetUpcomeingPreventiveReport(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetTransferReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetTransferReport(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetDisposalReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetDisposalReport(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetPerformedPreventiveMaintenance(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetPerformedPreventiveMaintenance(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetPerformedCurativeMaintenance(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetPerformedCurativeMaintenance(regionId, userregionId, siteId, instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetNonFunctionalInstrumentsBySiteANDInstrumentCategory(int regionId, int userregionId, int siteId, int instrumentcatagoryId)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetNonFunctionalInstrumentsBySiteANDInstrumentCategory(regionId, userregionId, siteId, instrumentcatagoryId);
        }
        public DataSet GetInstrumentProblemByManufacturer(int userregionId, int instrumentcatagoryId, int manufactureid)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetInstrumentProblemByManufacturer(userregionId, instrumentcatagoryId, manufactureid);
        }
        public DataSet GetQuarterPreventiveReport(int regionId, int userregionId, DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetQuarterPreventiveReport(regionId, userregionId, datefrom, dateto);
        }
        public DataSet GetQuarterCurativeReport(int regionId, int userregionId, DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetQuarterCurativeReport(regionId, userregionId, datefrom, dateto);
        }

        public DataSet GetEscalatedCurativeMaintenances(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetEscalatedCurativeMaintenances(regionId, userregionId,siteId,instrumentcatagoryId, datefrom, dateto);
        }
        public DataSet GetNotifiedConsumables(int regionId, int userregionId, int siteId, int consumableId)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetNotifiedConsumables(regionId, userregionId, siteId, consumableId);
        }

        public DataSet GetNotifiedProblemsChart(DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetNotifiedProblemsChart(datefrom, dateto);
        }
        public DataSet GetMostIssuedSparepartsbyInstrumentsChart(DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetMostIssuedSparepartsbyInstrumentsChart(datefrom, dateto);
        }
        public DataSet GetMostRequestedSparepartsbyInstrumentsChart(DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetMostRequestedSparepartsbyInstrumentsChart(datefrom, dateto);
        }


        public DataSet GetFrequentlyMaintainedInstrumentsChart(DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetFrequentlyMaintainedInstrumentsChart(datefrom, dateto);
        }
        public DataSet GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(datefrom, dateto);
        }
        public DataSet GetRFrequentlyReportedProblemTypesChart(DateTime datefrom, DateTime dateto)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();
            return _reportservices.GetRFrequentlyReportedProblemTypesChart(datefrom, dateto);
        }

        public DataSet GetInstrumentUnderContract(int regionId, int userregionId, int siteId)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();

            return _reportservices.GetInstrumentsUnderContract(regionId, userregionId, siteId);
        }

        public DataSet GetCurativeMaintenanceTracking(int regionId, int userregionId, int siteId)
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();

            return _reportservices.GetCurativeMaintenanceTracking(regionId, userregionId, siteId);
        }
        public IList GetMapStatisticReport()
        {
            if (_reportservices == null)
                _reportservices = new ReportServices();

            return _reportservices.GetMapStatisticReport();
        }
    }
}
