﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.CompositeWeb.Utility;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Services.Configuration;
using Chai.Maintenance.Shared.Navigation;
using Chai.Maintenance.Services;

namespace Chai.Maintenance.Modules.Configuration
{

    public class ConfigurationController

    {
        private IHttpContextLocatorService _httpContextLocatorService;
        private INavigationService _navigationService;
        private RegionServices _regionservice;
        private SiteServices _siteservice;
        private InstrumentTypeServices _instrumenttypeservice;
        private SparepartTypeServices _spareparttypeservices;
        private InstrumentServices _instrumentservices;
        private ProblemTypeServices _problemTypeServices;
        private ErrorCodeServices _errorCodeServices;
        private UnitOfMeasurmentServices _UnitOfMeasurmentServices;
        private ManufacturerServices _ManufacturerServices;
        private VendorServices _VendorServices;
        private SiteTypeServices _SiteTypeServices;
        private InstrumentCategoryServices _InstrumentCategoryServices;
        private ISessionStateLocatorService _sessionstate;
        private AutoNumberServices _autoNumberServices;
        private ConsumableServices _consumableServices;
        private ChecklistTaskServices _checklistTaskServices;
        private JobPushBackReasonServices _jobPushBackReasonServices;
        [InjectionConstructor]
        public ConfigurationController([ServiceDependency] IHttpContextLocatorService httpContextLocatorService,
                                       [ServiceDependency] INavigationService navigationService,
                                       [ServiceDependency] ISessionStateLocatorService sessionstate)
        {
            _httpContextLocatorService = httpContextLocatorService;
            _navigationService = navigationService;
            _sessionstate = sessionstate;
        }

        public User CurrentUser
        {
            get { return _httpContextLocatorService.GetCurrentContext().User.Identity as User; }
        }

        public void Navigate(string to)
        {
            _navigationService.Navigate(to);
        }

        #region Region

        public Region RegionObject
        {
            get

            {
                Region _region = _sessionstate.GetSessionState()["Region"] as Region;

                if (_region != null)
                    return _region;
                return null;
            }
            set { _sessionstate.GetSessionState()["Region"] = value; }
        }


        public IList<Region> GetRegions(int findby, string value, int regionId)
        {

            if (_regionservice == null)
                _regionservice = new RegionServices();

            return _regionservice.GetAllRegions(findby, value, regionId);

        }
        public IList<Region> GetAllListOfRegion()
        {

            if (_regionservice == null)
                _regionservice = new RegionServices();

            return _regionservice.GetAllListOfRegion();

        }

        public void GetRegionById(int regionid)
        {
            if (_regionservice == null)
                _regionservice = new RegionServices();

            this.RegionObject = _regionservice.GetRegionById(regionid);
        }
       
        public void SaveOrUpdateRegion(Region region)
        {

            if (_regionservice == null)
                _regionservice = new RegionServices();
            _regionservice.SaveOrUpdateRegion(region);
            this.RegionObject = region;
        }

        public void NewRegion()
        {
            Region region = new Region();
            this.RegionObject = region;
        }

        public void DeleteRegion(int regionid)
        {
            if (_regionservice == null)
                _regionservice = new RegionServices();
            _regionservice.DeleteRegion(regionid);
        }

        #endregion

        #region Site

        public Site SiteObject
        {
            get
            {
                Site _site = _sessionstate.GetSessionState()["Site"] as Site;

                if (_site != null)
                    return _site;
                return null;
            }
            set { _sessionstate.GetSessionState()["Site"] = value; }
        }

        public IList<Site> SiteListObject
        {
            get
            {
                IList<Site> _site = _sessionstate.GetSessionState()["Site"] as IList<Site>;

                if (_site != null)
                    return _site;
                return null;
            }
            set { _sessionstate.GetSessionState()["Site"] = value; }
        }

        public IList<Site> GetSites(int findby, string value, int regionid)
        {

            if (_siteservice == null)
                _siteservice = new SiteServices();

            return _siteservice.GetAllSites(findby, value, regionid);

        }

        public void GetSiteById(int siteid)
        {
            if (_siteservice == null)
                _siteservice = new SiteServices();

            this.SiteObject = _siteservice.GetSiteById(siteid);
        }

        public void SaveOrUpdateSite(Site site)
        {

            if (_siteservice == null)
                _siteservice = new SiteServices();
            _siteservice.SaveOrUpdateRegion(site);
            this.SiteObject = site;
        }

        public void NewSite()
        {
            Site site = new Site();
            this.SiteObject = site;
        }

        public void DeleteSite(Site site)
        {
        
            if (_siteservice == null)
                _siteservice = new SiteServices();
           _siteservice.DeleteSite(site);
          // this.SiteObject = site;
        }

        public IList<Site> GetSiteByRegionID(int regionId)
        {
            if (_siteservice == null)
                _siteservice = new SiteServices();
            return _siteservice.GetSiteByRegionID(regionId);
        }
        public Site GetRegionIDBySiteId(int siteid)
        {
            if (_siteservice == null)
                _siteservice = new SiteServices();
            return _siteservice.GetRegionIDBySiteId(siteid);
        }
        #endregion

        #region InstrumentType

        public InstrumentType InstrumentTypeObject
        {
            get
            {
                InstrumentType _instrumenttype = _sessionstate.GetSessionState()["InstrumentType"] as InstrumentType;

                if (_instrumenttype != null)
                    return _instrumenttype;
                return null;
            }
            set { _sessionstate.GetSessionState()["InstrumentType"] = value; }
        }


        public IList<InstrumentType> GetInstrumentTypes(int findby, string value, int ddlvalue)
        {

            if (_instrumenttypeservice == null)
                _instrumenttypeservice = new InstrumentTypeServices();

            return _instrumenttypeservice.GetAllInstrumentTypes(findby, value, ddlvalue);

        }

        //public void GetInstrumentTypeById(int regionid)
        //{
        //    if (_instrumenttypeservice == null)
        //        _instrumenttypeservice = new InstrumentTypeServices();

        //    this.RegionObject = _regionservice.GetRegionById(regionid);
        //}
        public void GetInstrumentTypeById(int instrumenttypeid)
        {
            if (_instrumenttypeservice == null)
                _instrumenttypeservice = new InstrumentTypeServices();

            this.InstrumentTypeObject = _instrumenttypeservice.GetnstrumentTypeById(instrumenttypeid);
        }

        public void SaveOrUpdateInstrumentType(InstrumentType instrumenttype)
        {

            if (_instrumenttypeservice == null)
                _instrumenttypeservice = new InstrumentTypeServices();
            _instrumenttypeservice.SaveOrUpdateInstrumentType(instrumenttype);
            this.InstrumentTypeObject = instrumenttype;
        }

        public void NewInstrumenType()
        {
            InstrumentType instrumenttype = new InstrumentType();
            this.InstrumentTypeObject = instrumenttype;
        }

        public void DeleteInstrumentType(int instrumenttypeid)
        {
            if (_instrumenttypeservice == null)
                _instrumenttypeservice = new InstrumentTypeServices();
            _instrumenttypeservice.DeleteInstrumentType(instrumenttypeid);
        }

        #endregion

        #region SparepartType

        public SparepartType SparepartTypeObject
        {
            get
            {
                SparepartType _spareparttype = _sessionstate.GetSessionState()["SparepartType"] as SparepartType;

                if (_spareparttype != null)
                    return _spareparttype;
                return null;
            }
            set { _sessionstate.GetSessionState()["SparepartType"] = value; }
        }


        public IList<SparepartType> GetSparepartTypes(int findby, string value, int ddlvalue)
        {

            if (_spareparttypeservices == null)
                _spareparttypeservices = new SparepartTypeServices();

            return _spareparttypeservices.GetAllSparepartTypes(findby, value, ddlvalue);

        }

        public void GetSparepartTypeById(int spareparttypeid)
        {
            if (_spareparttypeservices == null)
                _spareparttypeservices = new SparepartTypeServices();

            this.SparepartTypeObject = _spareparttypeservices.GetSparepartTypeById(spareparttypeid);
        }

        public void SaveOrUpdateSparepartType(SparepartType spareparttype)
        {

            if (_spareparttypeservices == null)
                _spareparttypeservices = new SparepartTypeServices();
            _spareparttypeservices.SaveOrUpdateSparepartType(spareparttype);
            this.SparepartTypeObject = spareparttype;
        }

        public void NewSparepartType()
        {
            SparepartType spareparttype = new SparepartType();
            this.SparepartTypeObject = spareparttype;
        }

        public void DeleteSparepartType(int sparparttypeid)
        {
            if (_spareparttypeservices == null)
                _spareparttypeservices = new SparepartTypeServices();
            _spareparttypeservices.DeleteSaprepartType(sparparttypeid);
        }

        #endregion

        #region Instrument

        public Instrument InstrumentObject
        {
            get
            {
                Instrument _instrument = _sessionstate.GetSessionState()["Instrument"] as Instrument;

                if (_instrument != null)
                    return _instrument;
                return null;
            }
            set { _sessionstate.GetSessionState()["Instrument"] = value; }
        }

        public IList<Instrument> InstrumentListObject
        {
            get
            {
                IList<Instrument> _instrument = _sessionstate.GetSessionState()["Instrument"] as IList<Instrument>;

                if (_instrument != null)
                    return _instrument;
                return null;
            }
            set { _sessionstate.GetSessionState()["Site"] = value; }
        }

        public IList<Instrument> GetInstruments(int findby, string value, int instrumentnameid, int siteid,int regionId)
        {

            if (_instrumentservices == null)
                _instrumentservices = new InstrumentServices();

            return _instrumentservices.GetAllInstruments(findby, value, instrumentnameid, siteid, regionId);

        }

        public void GetInstrumentById(int instrumentid)
        {
            if (_instrumentservices == null)
                _instrumentservices = new InstrumentServices();

            this.InstrumentObject = _instrumentservices.GetInstrumentById(instrumentid);
        }

        public Instrument GetInstrumentByIdForProblem(int instrumentid)
        {
            if (_instrumentservices == null)
                _instrumentservices = new InstrumentServices();
            return _instrumentservices.GetInstrumentById(instrumentid);
        }

        public void SaveOrUpdateInstrument(Instrument instrument)
        {

            if (_instrumentservices == null)
                _instrumentservices = new InstrumentServices();
            _instrumentservices.SaveOrUpdateInstrument(instrument);
            this.InstrumentObject = instrument;
        }

        public void SaveTransferedInstrument(Instrument instrument, int OldInstrumentId, int NewSiteId)
        {

            if (_instrumentservices == null)
                _instrumentservices = new InstrumentServices();
            _instrumentservices.SaveTransferedInstrument(instrument, OldInstrumentId, NewSiteId);
            // this.InstrumentObject = instrument;
        }

        public void NewInstrument()
        {
            Instrument instrument = new Instrument();
            this.InstrumentObject = instrument;
        }

        public void DeleteInstrument(int instrumentid)
        {
            if (_instrumentservices == null)
                _instrumentservices = new InstrumentServices();
            _instrumentservices.DeleteInstrument(instrumentid);
        }

        public IList<Instrument> GetInstrumentBySiteID(int siteId)
        {
            if (_instrumentservices == null)
                _instrumentservices = new InstrumentServices();
            return _instrumentservices.GetInstrumentBysiteID(siteId);
        }

        #endregion

        #region UnitOfMeasurment

        public UnitOfMeasurment UnitOfMeasurmentObject
        {
            get
            {
                UnitOfMeasurment _UnitOfMeasurment =
                    _sessionstate.GetSessionState()["UnitOfMeasurment"] as UnitOfMeasurment;

                if (_UnitOfMeasurment != null)
                    return _UnitOfMeasurment;
                return null;
            }
            set { _sessionstate.GetSessionState()["UnitOfMeasurment"] = value; }
        }


        public IList<UnitOfMeasurment> GetUnitOfMeasurments(string value)
        {

            if (_UnitOfMeasurmentServices == null)
                _UnitOfMeasurmentServices = new UnitOfMeasurmentServices();

            return _UnitOfMeasurmentServices.GetAllUnitOfMeasurments(value);

        }

        public void GetUnitOfMeasurmentById(int UnitOfMeasurmentid)
        {
            if (_UnitOfMeasurmentServices == null)
                _UnitOfMeasurmentServices = new UnitOfMeasurmentServices();

            this.UnitOfMeasurmentObject = _UnitOfMeasurmentServices.GetUnitOfMeasurmentById(UnitOfMeasurmentid);
        }

        public void SaveOrUpdateUnitOfMeasurment(UnitOfMeasurment unitOfMeasurment)
        {

            if (_UnitOfMeasurmentServices == null)
                _UnitOfMeasurmentServices = new UnitOfMeasurmentServices();
            _UnitOfMeasurmentServices.SaveOrUpdateUnitOfMeasurment(unitOfMeasurment);
            this.UnitOfMeasurmentObject = unitOfMeasurment;
        }

        public void NewUnitOfMeasurment()
        {
            UnitOfMeasurment unitOfMeasurment = new UnitOfMeasurment();
            this.UnitOfMeasurmentObject = unitOfMeasurment;
        }

        public void DeleteUnitOfMeasurment(int unitOfMeasurmentid)
        {
            if (_UnitOfMeasurmentServices == null)
                _UnitOfMeasurmentServices = new UnitOfMeasurmentServices();
            _UnitOfMeasurmentServices.DeleteUnitOfMeasurment(unitOfMeasurmentid);
        }

        #endregion

        #region Manufacturer

        public Manufacturer ManufacturerObject
        {
            get
            {
                Manufacturer _Manufacturer = _sessionstate.GetSessionState()["Manufacturer"] as Manufacturer;

                if (_Manufacturer != null)
                    return _Manufacturer;
                return null;
            }
            set { _sessionstate.GetSessionState()["Manufacturer"] = value; }
        }


        public IList<Manufacturer> GetManufacturers(string value)
        {

            if (_ManufacturerServices == null)
                _ManufacturerServices = new ManufacturerServices();

            return _ManufacturerServices.GetAllManufacturers(value);

        }

        public void GetManufacturerById(int Manufacturerid)
        {
            if (_ManufacturerServices == null)
                _ManufacturerServices = new ManufacturerServices();

            this.ManufacturerObject = _ManufacturerServices.GetManufacturerById(Manufacturerid);
        }

        public void SaveOrUpdateManufacturer(Manufacturer Manufacturer)
        {

            if (_ManufacturerServices == null)
                _ManufacturerServices = new ManufacturerServices();
            _ManufacturerServices.SaveOrUpdateManufacturer(Manufacturer);
            this.ManufacturerObject = Manufacturer;
        }

        public void NewManufacturer()
        {
            Manufacturer Manufacturer = new Manufacturer();
            this.ManufacturerObject = Manufacturer;
        }

        public void DeleteManufacturer(int Manufacturerid)
        {
            if (_ManufacturerServices == null)
                _ManufacturerServices = new ManufacturerServices();
            _ManufacturerServices.DeleteManufacturer(Manufacturerid);
        }

        #endregion

        #region Vendor

        public Vendor VendorObject
        {
            get
            {
                Vendor _Vendor = _sessionstate.GetSessionState()["Vendor"] as Vendor;

                if (_Vendor != null)
                    return _Vendor;
                return null;
            }
            set { _sessionstate.GetSessionState()["Vendor"] = value; }
        }


        public IList<Vendor> GetVendors(string value)
        {

            if (_VendorServices == null)
                _VendorServices = new VendorServices();

            return _VendorServices.GetAllVendor(value);

        }

        public void GetVendorById(int vendorid)
        {
            if (_VendorServices == null)
                _VendorServices = new VendorServices();

            this.VendorObject = _VendorServices.GetVendorById(vendorid);
        }

        public void SaveOrUpdateVendor(Vendor Vendor)
        {

            if (_VendorServices == null)
                _VendorServices = new VendorServices();
            _VendorServices.SaveOrUpdateVendor(Vendor);
            this.VendorObject = Vendor;
        }

        public void NewVendor()
        {
            Vendor Vendor = new Vendor();
            this.VendorObject = Vendor;
        }

        public void DeleteVendor(int Vendorid)
        {
            if (_VendorServices == null)
                _VendorServices = new VendorServices();
            _VendorServices.DeleteVendor(Vendorid);
        }

        #endregion

        #region   SiteType

        public SiteType SiteTypeObject
        {
            get
            {
                SiteType _SiteType = _sessionstate.GetSessionState()["SiteType"] as SiteType;

                if (_SiteType != null)
                    return _SiteType;
                return null;
            }
            set { _sessionstate.GetSessionState()["SiteType"] = value; }
        }


        public IList<SiteType> GetSiteTypes(string value)
        {

            if (_SiteTypeServices == null)
                _SiteTypeServices = new SiteTypeServices();

            return _SiteTypeServices.GetAllSiteTypes(value);

        }

        public void GetSiteTypeById(int vendorid)
        {
            if (_SiteTypeServices == null)
                _SiteTypeServices = new SiteTypeServices();

            this.SiteTypeObject = _SiteTypeServices.GetSiteTypeById(vendorid);
        }

        public void SaveOrUpdateSiteType(SiteType SiteType)
        {

            if (_SiteTypeServices == null)
                _SiteTypeServices = new SiteTypeServices();
            _SiteTypeServices.SaveOrUpdateSiteType(SiteType);
            this.SiteTypeObject = SiteType;
        }

        public void NewSiteType()
        {
            SiteType SiteType = new SiteType();
            this.SiteTypeObject = SiteType;
        }

        public void DeleteSiteType(int SiteTypeid)
        {
            if (_SiteTypeServices == null)
                _SiteTypeServices = new SiteTypeServices();
            _SiteTypeServices.DeleteSiteType(SiteTypeid);
        }

        #endregion

        #region   InstrumentCategory

        public InstrumentCategory InstrumentCategoryObject
        {
            get
            {
                InstrumentCategory _InstrumentCategory =
                    _sessionstate.GetSessionState()["InstrumentCategory"] as InstrumentCategory;

                if (_InstrumentCategory != null)
                    return _InstrumentCategory;
                return null;
            }
            set { _sessionstate.GetSessionState()["InstrumentCategory"] = value; }
        }


        public IList<InstrumentCategory> GetInstrumentCategorys(string value)
        {

            if (_InstrumentCategoryServices == null)
                _InstrumentCategoryServices = new InstrumentCategoryServices();

            return _InstrumentCategoryServices.GetAllInstrumentCategorys(value);

        }

        public void GetInstrumentCategoryById(int instrumentCategoryId)
        {
            if (_InstrumentCategoryServices == null)
                _InstrumentCategoryServices = new InstrumentCategoryServices();

            this.InstrumentCategoryObject = _InstrumentCategoryServices.GetInstrumentCategoryById(instrumentCategoryId);
        }

        public void SaveOrUpdateInstrumentCategory(InstrumentCategory instrumentCategory)
        {

            if (_InstrumentCategoryServices == null)
                _InstrumentCategoryServices = new InstrumentCategoryServices();
            _InstrumentCategoryServices.SaveOrUpdateInstrumentCategory(instrumentCategory);
            this.InstrumentCategoryObject = instrumentCategory;
        }

        public void NewInstrumentCategory()
        {
            InstrumentCategory instrumentCategory = new InstrumentCategory();
            this.InstrumentCategoryObject = instrumentCategory;
        }

        public void DeleteInstrumentCategory(int instrumentCategoryId)
        {
            if (_InstrumentCategoryServices == null)
                _InstrumentCategoryServices = new InstrumentCategoryServices();
            _InstrumentCategoryServices.DeleteInstrumentCategory(instrumentCategoryId);
        }

        #endregion

        #region ErrorCode

        public ErrorCode ErrorCodeObject
        {
            get
            {
                ErrorCode _ErrorCode = _sessionstate.GetSessionState()["ErrorCode"] as ErrorCode;

                if (_ErrorCode != null)
                    return _ErrorCode;
                return null;
            }
            set { _sessionstate.GetSessionState()["ErrorCode"] = value; }
        }


        public IList<ErrorCode> GetErrorCodes(string errorCode, int problemType)
        {

            if (_errorCodeServices == null)
                _errorCodeServices = new ErrorCodeServices();

            return _errorCodeServices.GetAllErrorCodes(errorCode, problemType);

        }

        public IList<ErrorCode> GetErrorCodeListByProblemTypeID(int ProblemTypeID)
        {

            if (_errorCodeServices == null)
                _errorCodeServices = new ErrorCodeServices();

            return _errorCodeServices.GetErrorCodeListByProblemTypeID(ProblemTypeID);

        }

        public void GetErrorCodeById(int ErrorCodeid)
        {
            if (_errorCodeServices == null)
                _errorCodeServices = new ErrorCodeServices();

            this.ErrorCodeObject = _errorCodeServices.GetErrorCodeById(ErrorCodeid);
        }

        public void SaveOrUpdateErrorCode(ErrorCode _ErrorCode)
        {

            if (_errorCodeServices == null)
                _errorCodeServices = new ErrorCodeServices();
            _errorCodeServices.SaveOrUpdateErrorCode(_ErrorCode);
            this.ErrorCodeObject = _ErrorCode;
        }

        public void NewErrorCode()
        {
            ErrorCode ErrorCode = new ErrorCode();
            this.ErrorCodeObject = ErrorCode;
        }

        public void DeleteErrorCode(int ErrorCodeid)
        {
            if (_errorCodeServices == null)
                _errorCodeServices = new ErrorCodeServices();
            _errorCodeServices.DeleteErrorCode(ErrorCodeid);
        }

        #endregion

        #region ProblemType

        public ProblemType ProblemTypeObject
        {
            get
            {
                ProblemType _ProblemType = _sessionstate.GetSessionState()["ProblemType"] as ProblemType;

                if (_ProblemType != null)
                    return _ProblemType;
                return null;
            }
            set { _sessionstate.GetSessionState()["ProblemType"] = value; }
        }


        public IList<ProblemType> GetProblemTypes(string value)
        {

            if (_problemTypeServices == null)
                _problemTypeServices = new ProblemTypeServices();

            return _problemTypeServices.GetAllProblemTypes(value);

        }

        public void GetProblemTypeById(int ProblemTypeid)
        {
            if (_problemTypeServices == null)
                _problemTypeServices = new ProblemTypeServices();

            this.ProblemTypeObject = _problemTypeServices.GetProblemTypeById(ProblemTypeid);
        }

        public void SaveOrUpdateProblemType(ProblemType _ProblemType)
        {

            if (_problemTypeServices == null)
                _problemTypeServices = new ProblemTypeServices();
            _problemTypeServices.SaveOrUpdateProblemType(_ProblemType);
            this.ProblemTypeObject = _ProblemType;
        }

        public void NewProblemType()
        {
            ProblemType ProblemType = new ProblemType();
            this.ProblemTypeObject = ProblemType;
        }

        public void DeleteProblemType(int ProblemTypeid)
        {
            if (_problemTypeServices == null)
                _problemTypeServices = new ProblemTypeServices();
            _problemTypeServices.DeleteProblemType(ProblemTypeid);
        }

        #endregion

        #region AutoNumber

        public AutoNumber AutoNumberObject
        {
            get
            {
                AutoNumber _AutoNumber = _sessionstate.GetSessionState()["AutoNumber"] as AutoNumber;

                if (_AutoNumber != null)
                    return _AutoNumber;
                return null;
            }
            set { _sessionstate.GetSessionState()["AutoNumber"] = value; }
        }


        public IList<AutoNumber> GetAutoNumbers(int PageId)
        {

            if (_autoNumberServices == null)
                _autoNumberServices = new AutoNumberServices();

            return _autoNumberServices.GetAllAutoNumbers(PageId);

        }


        public AutoNumber GetAutoNumberByPageId(int pageId)
        {
            if (_autoNumberServices == null)
                _autoNumberServices = new AutoNumberServices();

            return _autoNumberServices.GetAutoNumberByPageId(pageId);
        }

        public void SaveOrUpdateAutoNumber(AutoNumber _AutoNumber)
        {

            if (_autoNumberServices == null)
                _autoNumberServices = new AutoNumberServices();
            _autoNumberServices.SaveOrUpdateAutoNumber(_AutoNumber);
            this.AutoNumberObject = _AutoNumber;
        }

        public void NewAutoNumber()
        {
            AutoNumber _AutoNumber = new AutoNumber();
            this.AutoNumberObject = _AutoNumber;
        }

        public void DeleteAutoNumber(int AutoNumberId)
        {
            if (_autoNumberServices == null)
                _autoNumberServices = new AutoNumberServices();
            _autoNumberServices.DeleteAutoNumber(AutoNumberId);
        }

        #endregion

        #region Consumables

        public Consumables ConsumableObject
        {
            get
            {
                Consumables consumables = _sessionstate.GetSessionState()["Consumables"] as Consumables;

                if (consumables != null)
                    return consumables;
                return null;
            }
            set { _sessionstate.GetSessionState()["Consumables"] = value; }
        }


        public IList<Consumables> GetConsumables(string value)
        {

            if (_consumableServices == null)
                _consumableServices = new ConsumableServices();

            return _consumableServices.GetAllConsumables(value);

        }

        public void GetConsumablesById(int consumablesId)
        {
            if (_consumableServices == null)
                _consumableServices = new ConsumableServices();

            this.ConsumableObject = _consumableServices.GetConsumablesById(consumablesId);
        }

        public void SaveOrUpdateConsumable(Consumables consumable)
        {

            if (_consumableServices == null)
                _consumableServices = new ConsumableServices();
            _consumableServices.SaveOrUpdateConsumables(consumable);
            this.ConsumableObject = consumable;
        }

        public void NewConsumable()
        {
            Consumables consumables = new Consumables();
            this.ConsumableObject = consumables;
        }

        public void DeleteConsumable(int consumableId)
        {
            if (_consumableServices == null)
                _consumableServices = new ConsumableServices();
            _consumableServices.DeleteConsumables(consumableId);
        }

        #endregion

        
        #region  ChecklistTask

        public ChecklistTask ChecklistTaskObject
        {
            get
            {
                ChecklistTask checklistTask = _sessionstate.GetSessionState()["ChecklistTask"] as ChecklistTask;

                if (checklistTask != null)
                    return checklistTask;
                return null;
            }
            set { _sessionstate.GetSessionState()["ChecklistTask"] = value; }
        }


        public IList<ChecklistTask> GetChecklistTask(string task, int instrumentLookupid)
        {

            if (_checklistTaskServices == null)
                _checklistTaskServices = new ChecklistTaskServices();

            return _checklistTaskServices.GetAllChecklistTask(task,instrumentLookupid);

        }

        public void GetChecklistTaskById(int checklistTaskId)
        {
            if (_checklistTaskServices == null)
                _checklistTaskServices = new ChecklistTaskServices();

            this.ChecklistTaskObject = _checklistTaskServices.GetChecklistTaskById(checklistTaskId);
        }

        public void SaveOrUpdateChecklistTask(ChecklistTask checklistTask)
        {

            if (_checklistTaskServices == null)
                _checklistTaskServices = new ChecklistTaskServices();
            _checklistTaskServices.SaveOrUpdateChecklistTask(checklistTask);
            this.ChecklistTaskObject = checklistTask;
        }

        public void NewChecklistTask()
        {
            ChecklistTask checklistTask = new ChecklistTask();
            this.ChecklistTaskObject = checklistTask;
        }

        public void DeletechecklistTask(int checklistTaskId)
        {
            if (_checklistTaskServices == null)
                _checklistTaskServices = new ChecklistTaskServices();
            _checklistTaskServices.DeleteChecklistTask(checklistTaskId);
        }

        #endregion
        #region JobPushBackReason

        public JobPushBackReason JobPushBackReasonObject
        {
            get
            {
                JobPushBackReason _JobPushBackReason = _sessionstate.GetSessionState()["JobPushBackReason"] as JobPushBackReason;

                if (_JobPushBackReason != null)
                    return _JobPushBackReason;
                return null;
            }
            set { _sessionstate.GetSessionState()["JobPushBackReason"] = value; }
        }


        public IList<JobPushBackReason> GetJobPushBackReasons(string value)
        {

            if (_jobPushBackReasonServices == null)
                _jobPushBackReasonServices = new JobPushBackReasonServices();

            return _jobPushBackReasonServices.GetAllJobPushBackReasons(value);

        }

        public void GetJobPushBackReasonById(int jobPushBackReasonId)
        {
            if (_jobPushBackReasonServices == null)
                _jobPushBackReasonServices = new JobPushBackReasonServices();

            this.JobPushBackReasonObject = _jobPushBackReasonServices.GetJobPushBackReasonById(jobPushBackReasonId);
        }

        public void SaveOrUpdateJobPushBackReason(JobPushBackReason jobPushBackReason)
        {

            if (_jobPushBackReasonServices == null)
                _jobPushBackReasonServices = new JobPushBackReasonServices();
            _jobPushBackReasonServices.SaveOrUpdateJobPushBackReason(jobPushBackReason);
            this.JobPushBackReasonObject = jobPushBackReason;
        }

        public void NewJobPushBackReason()
        {
            JobPushBackReason jobPushBackReason = new JobPushBackReason();
            this.JobPushBackReasonObject = jobPushBackReason;
        }

        public void DeleteJobPushBackReason(int jobPushBackReasonId)
        {
            if (_jobPushBackReasonServices == null)
                _jobPushBackReasonServices = new JobPushBackReasonServices();
            _jobPushBackReasonServices.DeleteJobPushBackReason(jobPushBackReasonId);
        }

        #endregion
    }
}
