﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;

namespace Chai.Maintenance.Modules.Configuration.View
{
    public interface IChecklistTaskListView
    {
        int ChecklistId { get; }
        IList<ChecklistTask> ChecklistTask { set; }
        string Task { get; }
        int InstrumentLookupId { get; }
    }
}
