﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
   public interface  IInstrumentCategoryEditView
    {
        int Id { get; }
        InstrumentCategory _InstrumentCategory { get; set; }
    }
}
