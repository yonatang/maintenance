﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
   public interface IEditRegionView
    {
        int Id { get; }
        string GetRegionName { get; }
        string GetRegionCode { get; }
        string GetMapId { get; }
        Region region { set; }
    }
}
