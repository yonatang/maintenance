﻿ 
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface IAutoNumberListView
    {
        int AutoNumberId { get; }
        IList<AutoNumber> _AutoNumberlist { set; }
        int pageId { get; }
    }
}