﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;

namespace Chai.Maintenance.Modules.Configuration.View
{
    public class ChecklistTaskEditPresenter : Presenter<IChecklistTaskEditView>
    {
        private ConfigurationController _controller;
        private Consumables _consumables;

        public ChecklistTaskEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {

            View.ChecklistTask = _controller.ChecklistTaskObject;
        }

        public override void OnViewInitialized()
        {


            if (View.Id != 0)
                _controller.GetChecklistTaskById(View.Id);
            else
                _controller.NewChecklistTask();

        }


        public IList<InstrumentType> GetInstrumenttype()
        {
            return _controller.GetInstrumentTypes(0, string.Empty, 0);
        }
        public void SaveOrUpdateChecklistTask(ChecklistTask checklistTask)
        {
            _controller.SaveOrUpdateChecklistTask(checklistTask);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/ChecklistList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteChecklistTask(int checklistTaskId)
        {
            _controller.DeletechecklistTask(checklistTaskId);
        }
    }
}
