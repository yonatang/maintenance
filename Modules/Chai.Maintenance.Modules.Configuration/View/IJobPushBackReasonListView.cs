﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface IJobPushBackReasonListView
    {
        int JobPushBackReasonId { get; }
        IList<JobPushBackReason> JobPushBackReasonList { set; }
        string value { get; }
    }
}
