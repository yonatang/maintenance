﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class InstrumentTypeEditPresenter:Presenter<IInstrumentTypeEditView>
    {


         private ConfigurationController _controller;
        private InstrumentType _instrumenttype;

        public InstrumentTypeEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;
            

        }

        public override void OnViewLoaded()
        {

            View.instrumenttype = _controller.InstrumentTypeObject;    
        }

        public override void OnViewInitialized()
        {
            
                //int id = _region.Id;
                if (View.Id != 0)
                   _controller.GetInstrumentTypeById(View.Id);
                else
                _controller.NewInstrumenType();
            
        }



        public void SaveOrUpdateInstrumentType(InstrumentType instrumenttype)
        {
            _controller.SaveOrUpdateInstrumentType(instrumenttype);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/InstrumentTypeList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteInstrumentType(int instrumenttypeId)
        {
            _controller.DeleteInstrumentType(instrumenttypeId);
        }
        public IList<InstrumentCategory> GetInstrumentCategory()
        {
            return _controller.GetInstrumentCategorys("");
        }

    }
}
