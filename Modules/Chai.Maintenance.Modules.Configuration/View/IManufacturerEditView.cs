﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface IManufacturerEditView
    {
        int Id { get; }
        Manufacturer _Manufacturer { get; set; }

    }
}
