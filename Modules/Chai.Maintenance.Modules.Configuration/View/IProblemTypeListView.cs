﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface IProblemTypeListView
    {
        int ProblemTypeId { get; }
        IList<ProblemType> problemTypelist { set; }
        string ProblemType { get; }
    }
}
