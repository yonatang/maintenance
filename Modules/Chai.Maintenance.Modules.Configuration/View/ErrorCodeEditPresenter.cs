﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class ErrorCodeEditPresenter : Presenter<IErrorCodeEditView>
    {
        private ConfigurationController _controller;
        private ErrorCode _manufacturer;

        public ErrorCodeEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {

            View._ErrorCode = _controller.ErrorCodeObject;
        }

        public override void OnViewInitialized()
        {


            if (View.Id != 0)
                _controller.GetErrorCodeById(View.Id);
            else
                _controller.NewErrorCode();

        }



        public void SaveOrUpdateErrorCode(ErrorCode ErrorCode)
        {
            _controller.SaveOrUpdateErrorCode(ErrorCode);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/ErrorCodeList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteErrorCode(int ErrorCodeid)
        {
            _controller.DeleteErrorCode(ErrorCodeid);
        }
        public IList<ProblemType> GetProblemType()
        {
            return _controller.GetProblemTypes("");
        }
    }
}
