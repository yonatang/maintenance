﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface ISiteListView
    {
        int SiteId { get; }
        IList<Site> sitelist { set; }
        int findby { get; }
        string value { get; }
        int ddlValue { get; }
        IList<Region> RegionList { set; }

    }
}
