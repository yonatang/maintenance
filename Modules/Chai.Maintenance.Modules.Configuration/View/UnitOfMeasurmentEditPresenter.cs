﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;


namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class UnitOfMeasurmentEditPresenter : Presenter<IUnitOfMeasurmentEditView>
    {
        private ConfigurationController _controller;
        private UnitOfMeasurment _unitOfMeasurment;

        public UnitOfMeasurmentEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {

            View._UnitOfMeasurment = _controller.UnitOfMeasurmentObject;
        }

        public override void OnViewInitialized()
        {

            
            if (View.Id != 0)
                _controller.GetUnitOfMeasurmentById(View.Id);
            else
                _controller.NewUnitOfMeasurment();

        }



        public void SaveOrUpdateUnitOfMeasurment(UnitOfMeasurment unitOfMeasurment)
        {
            _controller.SaveOrUpdateUnitOfMeasurment(unitOfMeasurment);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/UnitOfMeasurmentList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteUnitOfMeasurment(int unitOfMeasurmentid)
        {
            _controller.DeleteUnitOfMeasurment(unitOfMeasurmentid);
        }
    }
}
