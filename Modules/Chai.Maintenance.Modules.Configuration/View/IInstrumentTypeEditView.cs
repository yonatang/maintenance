﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface IInstrumentTypeEditView
    {
        int Id { get; }
        int GetInstrumentCatagoryId { get; }
        string GetInstrumentname { get; }
        InstrumentType instrumenttype { set; }
    }
}
