﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;


namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class RegionEditPresenter : Presenter<IEditRegionView>
    {
        private ConfigurationController _controller;
        private Region _region;
        
        public RegionEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;
            

        }

        public override void OnViewLoaded()
        {

            View.region = _controller.RegionObject;    
        }

        public override void OnViewInitialized()
        {
            
                //int id = _region.Id;
                if (View.Id != 0)
                   _controller.GetRegionById(View.Id);
                else
                _controller.NewRegion();
            
        }
        
        

        public void SaveOrUpdateRegion(Region region)
        {
            _controller.SaveOrUpdateRegion(region);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/RegionList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteRegion(int regionid)
        {
            _controller.DeleteRegion(regionid);
        }
    }
}
