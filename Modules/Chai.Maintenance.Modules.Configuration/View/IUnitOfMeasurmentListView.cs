﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface IUnitOfMeasurmentListView
    {
        int UnitOfMeasurmentId { get; }
        IList<UnitOfMeasurment> unitOfMeasurmentlist { set; }
        string value { get; }
    }
}
