﻿ 
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class InstrumentCategoryEditPresenter : Presenter<IInstrumentCategoryEditView>
    {
        private ConfigurationController _controller;
        

        public InstrumentCategoryEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {

            View._InstrumentCategory = _controller.InstrumentCategoryObject;
        }

        public override void OnViewInitialized()
        {


            if (View.Id != 0)
                _controller.GetInstrumentCategoryById(View.Id);
            else
                _controller.NewInstrumentCategory();

        }



        public void SaveOrUpdateInstrumentCategory(InstrumentCategory instrumentCategory)
        {
            _controller.SaveOrUpdateInstrumentCategory(instrumentCategory);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/InstrumentCategoryList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteInstrumentCategory(int instrumentCategoryId)
        {
            _controller.DeleteInstrumentCategory(instrumentCategoryId);
        }
    }
}
