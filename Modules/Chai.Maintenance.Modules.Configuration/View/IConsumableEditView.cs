﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.View
{
   public interface IConsumableEditView
    {
        int Id { get; }
        Consumables Consumables { get; set; }
    }
}
