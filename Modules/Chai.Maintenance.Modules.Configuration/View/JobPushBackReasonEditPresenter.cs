﻿ 
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class JobPushBackReasonEditPresenter : Presenter<IJobPushBackReasonEditView>
    {
        private ConfigurationController _controller;
        private JobPushBackReason _JobPushBackReason;

        public JobPushBackReasonEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {

            View.JobPushBackReason = _controller.JobPushBackReasonObject;
        }

        public override void OnViewInitialized()
        {


            if (View.Id != 0)
                _controller.GetJobPushBackReasonById(View.Id);
            else
                _controller.NewJobPushBackReason();

        }



        public void SaveOrUpdateJobPushBackReason(JobPushBackReason jobPushBackReason)
        {
            _controller.SaveOrUpdateJobPushBackReason(jobPushBackReason);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/JobPushBackReasonList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteJobPushBackReason(int jobPushBackReasonId)
        {
            _controller.DeleteJobPushBackReason(jobPushBackReasonId);
        }
    }
}
