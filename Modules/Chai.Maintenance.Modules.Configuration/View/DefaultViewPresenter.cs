using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class DefaultViewPresenter : Presenter<IDefaultView>
    {
        private ConfigurationController _controller;

        public DefaultViewPresenter([CreateNew] ConfigurationController controller)
        {
            this._controller = controller;
        }
    }
}
