﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class ProblemTypeEditPresenter : Presenter<IProblemTypeEditView>
    {
        private ConfigurationController _controller;
        private ProblemType _ProblemType;

        public ProblemTypeEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {

            View._ProblemType = _controller.ProblemTypeObject;
        }

        public override void OnViewInitialized()
        {


            if (View.Id != 0)
                _controller.GetProblemTypeById(View.Id);
            else
                _controller.NewProblemType();

        }



        public void SaveOrUpdateProblemType(ProblemType ProblemType)
        {
            _controller.SaveOrUpdateProblemType(ProblemType);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/ProblemTypeList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteProblemType(int ProblemTypeid)
        {
            _controller.DeleteProblemType(ProblemTypeid);
        }
    }
}
