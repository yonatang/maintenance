﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;


namespace Chai.Maintenance.Modules.Configuration.Views
{
   public class InstrumentTypeListPresenter:Presenter<IInstrumentTypeListView>
    {
        private ConfigurationController _controller;
        private InstrumentType _instrumenttype;
        public InstrumentTypeListPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;
            

        }

        public override void OnViewLoaded()
        {
            View.InstrumentTypelist = _controller.GetInstrumentTypes(View.findby,View.value,View.ddlvalue);
        }

        public override void OnViewInitialized()
        {
         
        }
        public void GetInstrumentTypes(int findby, string value,int ddlvalue)
        {
            _controller.GetInstrumentTypes(findby, value,ddlvalue);
        }
        public IList<InstrumentCategory> GetInstrumentCatgory()
        {
            return _controller.GetInstrumentCategorys("");


        }
        public void GetInstrumentById(int Id)
        {
            _controller.GetInstrumentTypeById(Id);
        }
        public void NewPage()
        {
            _controller.Navigate(String.Format("~/Configuration/InstrumentTypeEdit.aspx?{0}=4", AppConstants.TABID));
        }
        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/InstrumentTypeList.aspx?{0}=4", AppConstants.TABID));
        }
    }
}
