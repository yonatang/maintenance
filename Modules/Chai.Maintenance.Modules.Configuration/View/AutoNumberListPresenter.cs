﻿
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;


namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class AutoNumberListPresenter : Presenter<IAutoNumberListView>
    {
        private ConfigurationController _controller;

        public AutoNumberListPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {
            View._AutoNumberlist = _controller.GetAutoNumbers(View.pageId);
        }

        public override void OnViewInitialized()
        {

        }
        public void GetAutoNumbers(int pageId)
        {
            _controller.GetAutoNumbers(pageId);
        }


        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Admin/Roles.aspx?{0}=0", AppConstants.TABID));
        }
    }
}
