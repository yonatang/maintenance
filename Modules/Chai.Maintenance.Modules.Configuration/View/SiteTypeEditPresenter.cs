﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class SiteTypeEditPresenter : Presenter<ISiteTypeEditView>
    {
        private ConfigurationController _controller;
        private SiteType _SiteType;

        public SiteTypeEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {

            View._SiteType = _controller.SiteTypeObject;
        }

        public override void OnViewInitialized()
        {


            if (View.Id != 0)
                _controller.GetSiteTypeById(View.Id);
            else
                _controller.NewSiteType();

        }



        public void SaveOrUpdateSiteType(SiteType siteType)
        {
            _controller.SaveOrUpdateSiteType(siteType);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/SiteTypeList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteSiteType(int SiteTypeid)
        {
            _controller.DeleteSiteType(SiteTypeid);
        }
    }
}
