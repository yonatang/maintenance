﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class InstrumentListPresenter : Presenter<IInstrumentListView>
    {
        private ConfigurationController _controller;
        public InstrumentListPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {
            if(View.findby !=0)
                View.instrumentlist = _controller.GetInstruments(View.findby, View.value, View.ddlinstrumentId, View.ddlsiteId, _controller.CurrentUser.RegionId);
        }

        public override void OnViewInitialized()
        {

        }
        public void GetInstruments(int findby, string value, int ddlinstrumentnameid, int ddlsiteid)
        {
           View.instrumentlist= _controller.GetInstruments(findby, value, ddlinstrumentnameid, ddlsiteid,_controller.CurrentUser.RegionId);
        }
        public IList<Region> GetRegionList()
        {
            return _controller.GetRegions(0, "",_controller.CurrentUser.RegionId);
        }
        public IList<Site> GetSiteList(int regionid)
        {
            return _controller.GetSiteByRegionID(regionid);
        }
        public IList<InstrumentType> GetInstrumenType()
        {
            return _controller.GetInstrumentTypes(0, "", 0);
        }
        public void NewPage()
        {
            _controller.Navigate(String.Format("~/Admin/InstrumentEdit.aspx?{0}=0", AppConstants.TABID));
        }
    }
}
