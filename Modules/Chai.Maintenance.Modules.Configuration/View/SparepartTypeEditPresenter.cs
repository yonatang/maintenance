﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;

namespace Chai.Maintenance.Modules.Configuration.Views
{
   public class SparepartTypeEditPresenter : Presenter<ISparepartTypeEditView>
    {
        private ConfigurationController _controller;
        private SparepartType _spareparttype;

        public SparepartTypeEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;
            

        }
        public override void OnViewLoaded()
        {

            View.spareparttype = _controller.SparepartTypeObject;
        }

        public override void OnViewInitialized()
        {

            //int id = _region.Id;
            if (View.Id != 0)
                _controller.GetSparepartTypeById(View.Id);
            else
                _controller.NewSparepartType();

        }

        public IList<InstrumentType> GetInstrumentTypeList()
        {
            return _controller.GetInstrumentTypes(0,"",0);
        }

        public IList<UnitOfMeasurment> GetUnitOfMeasurmentList()
        {
            return _controller.GetUnitOfMeasurments("");
        }
        public void SaveOrUpdateSparepartType(SparepartType spareparttype)
        {
            _controller.SaveOrUpdateSparepartType(spareparttype);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/SparepartTypeList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteSparepartType(int spareparttypeid)
        {
            _controller.DeleteSparepartType(spareparttypeid);
        }
    }
}
