﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class UnitOfMeasurmentListPresenter : Presenter<IUnitOfMeasurmentListView>
    {
        private ConfigurationController _controller;
        
        public UnitOfMeasurmentListPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {
            View.unitOfMeasurmentlist = _controller.GetUnitOfMeasurments(View.value);
        }

        public override void OnViewInitialized()
        {

        }
        public void GetUnitOfMeasurments(string value)
        {
            _controller.GetUnitOfMeasurments(value);
        }


        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Admin/Roles.aspx?{0}=0", AppConstants.TABID));
        }
    }
}
