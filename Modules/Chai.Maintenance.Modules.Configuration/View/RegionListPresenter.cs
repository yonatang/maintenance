﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;


namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class RegionListPresenter : Presenter<IRegionListView>
    {
        private ConfigurationController _controller;
        private Region _region;
        public RegionListPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;
            

        }

        public override void OnViewLoaded()
        {
           
            View.regionlist = _controller.GetRegions(View.findby,View.value,_controller.CurrentUser.RegionId);
            View.user = _controller.CurrentUser;
        }

        public override void OnViewInitialized()
        {
         
        }
        public void GetRegions(int findby, string value)
        {
            _controller.GetRegions(findby, value,_controller.CurrentUser.RegionId);
        }
 

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Admin/Roles.aspx?{0}=0", AppConstants.TABID));
        }
    }
}
