﻿
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;


namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class SiteTypeListPresenter : Presenter<ISiteTypeListView>
    {
        private ConfigurationController _controller;

        public SiteTypeListPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {
            View.siteTypelist = _controller.GetSiteTypes(View.value);
        }

        public override void OnViewInitialized()
        {

        }
        public void GetSiteTypes(string value)
        {
            _controller.GetSiteTypes(value);
        }


        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Admin/Roles.aspx?{0}=0", AppConstants.TABID));
        }
    }
}
