﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface ISparepartTypeListView
    {
        int SpareparttypeId { get; }
        IList<SparepartType> SparepartTypelist { set; }
        int findby { get; }
        string value { get; }
        int ddlvalue { get; }
    }
}
