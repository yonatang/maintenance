﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;

namespace Chai.Maintenance.Modules.Configuration.Views
{
   public interface IInstrumentListView
    {
        int InstrumentId { get; }
        IList<Instrument> instrumentlist { set; }
        int findby { get; }
        string value { get; }
        int ddlsiteId { get; }
        int ddlinstrumentId { get; }
        IList<Site> SiteList { set; }
        IList<InstrumentType> InstrumentNameList { set; }
    }
}
