﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface ISiteTypeListView
    {
        int SiteTypeId { get; }
        IList<SiteType> siteTypelist { set; }
        string value { get; }
    }
}
