﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class SiteListPresenter: Presenter<ISiteListView>
    {
        private ConfigurationController _controller;
        public SiteListPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;
            

        }

        public override void OnViewLoaded()
        {
            if (_controller.CurrentUser.RegionId == 1000)
            {
                View.sitelist = _controller.GetSites(View.findby, View.value, View.ddlValue);
            }
            else
            {
                View.sitelist = _controller.GetSites(View.findby, View.value, _controller.CurrentUser.RegionId);
            }
        }

        public override void OnViewInitialized()
        {
         
        }
        public IList<Site> GetSites(int findby, string value,int ddlvalue)
        {
           return _controller.GetSites(findby, value, ddlvalue);
        }

        public IList<Region> GetRegion()
        {
          return _controller.GetRegions(0, string.Empty,_controller.CurrentUser.RegionId);
        }
        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Admin/Roles.aspx?{0}=0", AppConstants.TABID));
        }
    }
}
