﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
namespace Chai.Maintenance.Modules.Configuration.Views
{
   public interface IRegionListView
    {
       int RegionId { get; }
       IList<Region> regionlist { set; }
       int findby { get; }
       string value { get; }
       User user { get; set; }
    }
}
