﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class VendorEditPresenter : Presenter<IVendorEditView>
    {
        private ConfigurationController _controller;
        private Vendor _vendor;

        public VendorEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {

            View._Vendor = _controller.VendorObject;
        }

        public override void OnViewInitialized()
        {


            if (View.Id != 0)
                _controller.GetVendorById(View.Id);
            else
                _controller.NewVendor();

        }



        public void SaveOrUpdateVendor(Vendor vendor)
        {
            _controller.SaveOrUpdateVendor(vendor);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/VendorList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteVendor(int vendorid)
        {
            _controller.DeleteVendor(vendorid);
        }
    }
}
