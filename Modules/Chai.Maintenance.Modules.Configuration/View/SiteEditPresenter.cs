﻿using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class SiteEditPresenter: Presenter<IEditSiteView>
    {
        ConfigurationController _controller;
        Site _site;
        IList<Site> _siteList;
        public SiteEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;
            

        }
        public override void OnViewLoaded()
        {
            if (View.RegionId == -1)
            {
                View._site = _controller.SiteObject;
            }
            else
            {
                View.siteList = _controller.GetSiteByRegionID(View.RegionId);
                if (View.SiteId != 0)
                {
                    _controller.GetSiteById(View.SiteId);
                    View._site = _controller.SiteObject;
                }
                else
                {
                    _controller.NewSite();
                    View._site = _controller.SiteObject;
                }
            }
        }

        public override void OnViewInitialized()
        {
            if (View.RegionId != -1 && View.SiteId == 0)
            {
                _controller.GetSiteByRegionID(View.RegionId);
                _controller.NewSite();
            }
            else if (View.SiteId != 0)
            {
                _controller.GetSiteById(View.SiteId);
            }
            else
            {
                View._site = _controller.SiteObject;
            }

        }

        public IList<SiteType> GetSiteType()
        {
            return _controller.GetSiteTypes("");
        }

        public void SaveOrUpdateSite(Site site)
        {
            _controller.SaveOrUpdateSite(site);
        }

        public void CancelPage()
        {
            _controller.Navigate(string.Format("~/Configuration/SiteList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteSite(Site site)
        {
           _controller.DeleteSite(site);
           View._site = _controller.SiteObject;
        }
        
        public void GetSiteById(int siteid)
        {
           _controller.GetSiteById(siteid);
        }
        public void NewSite()
        {
         _controller.NewSite();
         View._site = _controller.SiteObject;
        }
    }
}
