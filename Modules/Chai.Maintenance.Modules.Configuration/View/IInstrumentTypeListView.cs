﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface IInstrumentTypeListView
    {
        int InstrumentTypeId { get; }
        IList<InstrumentType> InstrumentTypelist { set; }
        int findby { get; }
        string value { get; }
        int ddlvalue { get; }
    }
}
