﻿ 
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;

namespace Chai.Maintenance.Modules.Configuration.View
{
    public class ConsumableEditPresenter : Presenter<IConsumableEditView>
    {
        private ConfigurationController _controller;
        private Consumables _consumables;

        public ConsumableEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {

            View.Consumables = _controller.ConsumableObject;
        }

        public override void OnViewInitialized()
        {


            if (View.Id != 0)
                _controller.GetConsumablesById(View.Id);
            else
                _controller.NewConsumable();

        }



        public void SaveOrUpdateConsumable(Consumables consumable)
        {
            _controller.SaveOrUpdateConsumable(consumable);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/ConsumableList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteConsumable(int consumableId)
        {
            _controller.DeleteConsumable(consumableId);
        }
    }
}
