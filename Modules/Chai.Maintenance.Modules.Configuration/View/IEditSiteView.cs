﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface IEditSiteView
    {
        Site _site { get; set; }
        IList<Site> siteList{set;}
        int RegionId { get; set; }
        int SiteId { get; set; }
        string RegionName { get; set; }
        string RegionCode { get; set; }
    }
}
