﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface IErrorCodeListView
    {
        int ErrorCodeId { get; }
        IList<ErrorCode> errorCodelist { set; }
        string ErrorCode { get; }
        int ProblemTypeId { get; }
    }
}
