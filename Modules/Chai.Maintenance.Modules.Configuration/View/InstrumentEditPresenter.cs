﻿using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class InstrumentEditPresenter:Presenter<IInstrumentEditView>
    {
         ConfigurationController _controller;
        Instrument _instrument;
        IList<Instrument> _instrumentlist;
        public InstrumentEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;
            

        }
        public override void OnViewLoaded()
        {
            if (View.SiteId == 0)
                View.instrument = _controller.InstrumentObject;
            else
                View.instrumentList = _controller.GetInstrumentBySiteID(View.SiteId);
            if (View.InstrumentId != 0)
            {
                _controller.GetInstrumentById(View.InstrumentId);
                View.instrument = _controller.InstrumentObject;
            }
            else
            {
                _controller.NewInstrument();
                View.instrument = _controller.InstrumentObject;
            }
        }

        public override void OnViewInitialized()
        {
            if (View.SiteId != 0 && View.InstrumentId == 0)
            {
                _controller.GetInstrumentBySiteID(View.InstrumentId);
                _controller.NewInstrument();
            }
            else if (View.InstrumentId != 0)
            {
                _controller.GetInstrumentById(View.InstrumentId);
            }
            else
            {
                View.instrument = _controller.InstrumentObject;
            }

        }


        public void GetInstrumentById(int instrumentid)
        {
            _controller.GetInstrumentById(instrumentid);
           View.instrument = _controller.InstrumentObject;
        }

        public Instrument GetInstrumentByIdForProblem(int instrumentid)
        {
            return _controller.GetInstrumentByIdForProblem(instrumentid);
       
        }
        public void SaveOrUpdateInstrument(Instrument instrument)
        {
            _controller.SaveOrUpdateInstrument(instrument);
        }
        public IList<Manufacturer> GetManufacturer()
        {
            return _controller.GetManufacturers("");
        }
        public IList<InstrumentType> GetInstrumentType()
        {
            return _controller.GetInstrumentTypes(0,"",0);
        }
        public IList<Vendor> GetVendor()
        {
            return _controller.GetVendors("");
        }
        public IList<Vendor> GetContractWith()
        {
            return _controller.GetVendors("");
        }
        public void CancelPage()
        {
            _controller.Navigate(string.Format("~/Configuration/InstrumentList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteInstrument(int instrumentid)
        {
            _controller.DeleteInstrument(instrumentid);
        }
    }
}
