﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface IErrorCodeEditView
    {
        int Id { get; }
        ErrorCode _ErrorCode { get; set; }

    }
}
