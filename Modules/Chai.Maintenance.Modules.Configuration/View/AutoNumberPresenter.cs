﻿
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;
using System;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class AutoNumberPresenter : Presenter<IAutoNumberView>
    {
        private ConfigurationController _controller;
        private AutoNumber _AutoNumber;

        public AutoNumberPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {

            View._AutoNumber = _controller.AutoNumberObject;
        }

        public override void OnViewInitialized()
        {


            //if (View.Id != 0)
            //    _controller.GetAutoNumberById(View.Id);
            //else
            //    _controller.NewAutoNumber();

        }



        public void SaveOrUpdateAutoNumber(AutoNumber _AutoNumber)
        {
            _controller.SaveOrUpdateAutoNumber(_AutoNumber);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/AutoNumberList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteAutoNumber(int AutoNumberId)
        {
            _controller.DeleteAutoNumber(AutoNumberId);
        }
    }
}
