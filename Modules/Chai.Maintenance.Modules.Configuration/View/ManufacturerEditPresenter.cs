﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;

namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class ManufacturerEditPresenter : Presenter<IManufacturerEditView>
    {
        private ConfigurationController _controller;
        private Manufacturer _manufacturer;

        public ManufacturerEditPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {

            View._Manufacturer = _controller.ManufacturerObject;
        }

        public override void OnViewInitialized()
        {


            if (View.Id != 0)
                _controller.GetManufacturerById(View.Id);
            else
                _controller.NewManufacturer();

        }



        public void SaveOrUpdateManufacturer(Manufacturer manufacturer)
        {
            _controller.SaveOrUpdateManufacturer(manufacturer);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/ManufacturerList.aspx?{0}=4", AppConstants.TABID));
        }

        public void DeleteManufacturer(int manufacturerid)
        {
            _controller.DeleteManufacturer(manufacturerid);
        }
    }
}
