﻿ 
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;


namespace Chai.Maintenance.Modules.Configuration.View
{
    public class ChecklistTaskListPresenter : Presenter<IChecklistTaskListView>
    {
        private ConfigurationController _controller;

        public ChecklistTaskListPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;
        }

        public override void OnViewLoaded()
        {
            View.ChecklistTask = _controller.GetChecklistTask(View.Task, View.InstrumentLookupId);
        }

        public override void OnViewInitialized()
        {

        }
        public void GetChecklistTask(string task, int instrumentLookupId)
        {
            _controller.GetChecklistTask(task, instrumentLookupId);
        }

        public IList<InstrumentType> GetInstrumenttype()
        {
            return _controller.GetInstrumentTypes(0, string.Empty, 0);
        }
        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Admin/Roles.aspx?{0}=0", AppConstants.TABID));
        }
    }
}
