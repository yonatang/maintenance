﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface ISiteTypeEditView
    {
        int Id { get; }
        SiteType _SiteType { get; set; }
        
    }
}
