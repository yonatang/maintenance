﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
    public interface IInstrumentCategoryListView
    {
        int InstrumentCategoryId { get; }
        IList<InstrumentCategory> instrumentCategorylist { set; }
        string value { get; }
    }
}
