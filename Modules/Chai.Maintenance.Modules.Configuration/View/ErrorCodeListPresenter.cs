﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;


namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class ErrorCodeListPresenter : Presenter<IErrorCodeListView>
    {
        private ConfigurationController _controller;

        public ErrorCodeListPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {
            View.errorCodelist = _controller.GetErrorCodes(View.ErrorCode, View.ProblemTypeId);
        }

        public override void OnViewInitialized()
        {

        }
        public void GetErrorCodes(string errorCode, int problemType)
        {
            _controller.GetErrorCodes(errorCode, problemType);
        }

        public IList<ProblemType> GetProblemType()
        {
            return _controller.GetProblemTypes("");
        }
        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Admin/Roles.aspx?{0}=0", AppConstants.TABID));
        }
    }
}
