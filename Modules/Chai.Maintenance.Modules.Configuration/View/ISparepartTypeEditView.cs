﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;

namespace Chai.Maintenance.Modules.Configuration.Views
{
   public interface ISparepartTypeEditView
    {
         int Id {get;}
         string Name { get; }
         int InstrumentlookupId { get; }
         string PartNumber { get; }
         string UOM { get; }
         int ReorderQty { get; }
         string Description { get; }
         SparepartType spareparttype { set; }
    }
}
