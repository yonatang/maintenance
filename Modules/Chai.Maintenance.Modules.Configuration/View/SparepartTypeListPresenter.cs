﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;


namespace Chai.Maintenance.Modules.Configuration.Views
{
    public class SparepartTypeListPresenter : Presenter<ISparepartTypeListView>
    {
         private ConfigurationController _controller;
        private SparepartType _sparepart;
        public SparepartTypeListPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;
            

        }

        public override void OnViewLoaded()
        {
            View.SparepartTypelist = _controller.GetSparepartTypes(View.findby,View.value,View.ddlvalue);
        }

        public override void OnViewInitialized()
        {
         
        }
        public void GetSparepartTypes(int findby, string value,int ddlvalue)
        {
            _controller.GetSparepartTypes(findby, value, ddlvalue);
        }
        public IList<InstrumentType> GetInstrumentList()
        {
           return _controller.GetInstrumentTypes(0, "", 0);
        }
       
        public void NewPage()
        {
            _controller.Navigate(String.Format("~/Configuration/SparepartTypeEdit.aspx?{0}=4", AppConstants.TABID));
        }
        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Configuration/SparepartTypeList.aspx?{0}=4", AppConstants.TABID));
        }
    }
}
