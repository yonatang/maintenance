﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.Modules.Configuration.Views
{
   public interface IInstrumentEditView
    {
        Instrument instrument { get; set; }
        IList<Instrument> instrumentList { set; }
        int InstrumentId { get; set; }
        int SiteId { get; set; }
        string SiteName { get; set; }
        string SiteCode { get; set; }
    }
}
