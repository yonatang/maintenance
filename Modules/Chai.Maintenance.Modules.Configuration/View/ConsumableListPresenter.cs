﻿
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Web;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Services;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Configuration.Views;


namespace Chai.Maintenance.Modules.Configuration.View
{
    public class ConsumableListPresenter : Presenter<IConsumableListView>
    {
        private ConfigurationController _controller;

        public ConsumableListPresenter([CreateNew] ConfigurationController controller)
        {
            _controller = controller;


        }

        public override void OnViewLoaded()
        {
            View.Consumablelist = _controller.GetConsumables(View.Value);
        }

        public override void OnViewInitialized()
        {

        }
        public void GetConsumables(string value)
        {
            _controller.GetConsumables(value);
        }


        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/Admin/Roles.aspx?{0}=0", AppConstants.TABID));
        }
    }
}
