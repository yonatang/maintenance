﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.SparepartInventory.Views.IssueEdit, App_Web_nuwe21ns" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
            HeaderText="Error" ValidationGroup="1" Height="68px" />
        <asp:Panel ID="Panel2" runat="server" CssClass="group" 
            style="left: 0px; top: 0px; width: 813px">
            <h4>
            
                <asp:Label ID="lblIssueInfo" runat="server" Text="Issue Info" CssClass="label"></asp:Label>
            </h4>
            <table style="width: 99%; height: 0px;">
                <tr>
                    <td style="width: 117px">
                        <asp:Label ID="lblIssueNo" runat="server" Text="Issue No." CssClass="label"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtIssueNo" runat="server" Width="200px" Height="19px" 
                            BorderStyle="Inset" Enabled="False"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RfvIssueNo" runat="server" 
                            ControlToValidate="txtIssueNo" Display="Dynamic" 
                            ErrorMessage="Request No. Required" ValidationGroup="1">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Label ID="lblapprover" runat="server" Text="Approver" CssClass="label"></asp:Label>
                    </td>
                    <td>
                        <strong>
                        <asp:Label ID="lblapproverresult" runat="server" BackColor="#CCCCCC" 
                            Width="200px"></asp:Label>
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 117px">
                        <asp:Label ID="lblIssueDate" runat="server" Text="Issue Date" CssClass="label"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Calendar1" runat="server" CssClass="textbox"></asp:TextBox>
                        <asp:Image ID="Img" runat="server" 
                            ImageUrl="~/Images/Calendar_scheduleHS.png" />
                        <cc2:CalendarExtender ID="Calendar1_CalendarExtender" runat="server" 
                            Enabled="True" PopupButtonID="Img" TargetControlID="Calendar1">
                        </cc2:CalendarExtender>
                        <asp:RegularExpressionValidator ID="REVIssueDate" runat="server" 
                            ControlToValidate="Calendar1" Display="Dynamic" 
                            ErrorMessage="Issue Date Is Not Valid" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator>
                    </td>
                    <td>
                        <asp:Label ID="lblIssuedfor" runat="server" Text="Requester" CssClass="label"></asp:Label>
                    </td>
                    <td>
                        <strong>
                        <asp:Label ID="lblIssuedforesult" runat="server" BackColor="#CCCCCC" 
                            Width="200px"></asp:Label>
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 117px">
                        <asp:Label ID="lblToolKeeper" runat="server" Text="Tool Keeper" 
                            CssClass="label"></asp:Label>
                    </td>
                    <td>
                        <strong>
                        <asp:Label ID="lbltoolkeeperresult" runat="server" BackColor="#CCCCCC" 
                            Width="200px"></asp:Label>
                        </strong>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
            </asp:Panel>
        <asp:Panel ID="Panel1" runat="server" CssClass="group" 
            style="left: 0px; top: 0px; width: 819px">
            <h4>
                <asp:Label ID="lblSparepartDetail" runat="server" 
                    Text=" Sparepart Detail" CssClass="label"></asp:Label>
            </h4>
            <asp:DataGrid ID="dgItemDetail" runat="server" AutoGenerateColumns="False" 
                BorderColor="#003366" BorderStyle="Solid" BorderWidth="1px" CellPadding="0" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                DataKeyField="Id" ForeColor="#333333" GridLines="None" 
                oncancelcommand="dgItemDetail_CancelCommand" 
                ondeletecommand="dgItemDetail_DeleteCommand" 
                oneditcommand="dgItemDetail_EditCommand" 
                onitemcommand="dgItemDetail_ItemCommand" 
                onitemdatabound="dgItemDetail_ItemDataBound" 
                onupdatecommand="dgItemDetail_UpdateCommand" ShowFooter="True" 
                Width="100%">
                <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:TemplateColumn HeaderText="Instrument Name">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Sparepart Name">
                        <EditItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "SparepartName")%>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "SparepartName")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Approved Qty">
                       
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "ApprovedQty")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Stock Qty">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "StockQty")%>
                        </ItemTemplate>
                       
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Issue Qty">
                        <EditItemTemplate>
                            &nbsp;
                            <table align="left" style="width: 56%; margin-left: 3px;">
                                <tr>
                                    <td class="editTextBox" style="width: 83px">
                                        <asp:Label ID="lblStockQty" runat="server" Text="Stock Qty"></asp:Label>
                                    </td>
                                    <td class="editDropDown" style="width: 81px">
                                        <asp:TextBox ID="txtStockQty" runat="server" Enabled="False" Height="16px" 
                                            Text='<%# DataBinder.Eval(Container.DataItem, "StockQty")%>' 
                                            ValidationGroup="3" Width="67px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:CompareValidator ID="CvissueQty" runat="server" 
                                            ControlToCompare="txtStockQty" ControlToValidate="txtIssuedQty" 
                                            Display="Dynamic" ErrorMessage="Issued Qty Must be Less Than Stock Qty" 
                                            Operator="LessThan" SetFocusOnError="True" ValidationGroup="3" 
                                            ValueToCompare="txtStockQty" Type="Integer"></asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="editTextBox" style="width: 83px">
                                        <asp:Label ID="lblApprovedQty" runat="server" Text="Approved Qty"></asp:Label>
                                    </td>
                                    <td class="editDropDown" style="width: 81px">
                                        <asp:TextBox ID="txtApprovedQty" runat="server" Enabled="False" Height="16px" 
                                            Width="67px" Text='<%# DataBinder.Eval(Container.DataItem, "ApprovedQty")%>' 
                                            ValidationGroup="3"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator ID="RfvQty" runat="server" 
                                            ControlToValidate="txtIssuedQty" ErrorMessage="Qty Required" InitialValue="0" 
                                            ValidationGroup="3">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="editTextBox" style="width: 83px">
                                        <asp:Label ID="lblIssueQty" runat="server" Text="Issue Qty"></asp:Label>
                                    </td>
                                    <td class="editDropDown" style="width: 81px">
                                        <asp:TextBox ID="txtIssuedQty" runat="server" Height="16px" 
                                            Text=' <%# DataBinder.Eval(Container.DataItem, "IssuedQty")%>' 
                                            ValidationGroup="3" Width="67px"></asp:TextBox>
                                        <cc2:FilteredTextBoxExtender ID="txtIssuedQty_FilteredTextBoxExtender" 
                                            runat="server" FilterType="Numbers" TargetControlID="txtIssuedQty">
                                        </cc2:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:CompareValidator ID="CvApprovedQty" runat="server" 
                                            ControlToCompare="txtApprovedQty" ControlToValidate="txtIssuedQty" 
                                            Display="Dynamic" 
                                            ErrorMessage="Issued Qty must be Less Than or Equal to Approved Qty" 
                                            Operator="LessThanEqual" ValueToCompare="txtApprovedQty" Type="Integer" 
                                            ValidationGroup="3"></asp:CompareValidator>
                                    </td>
                                </tr>
                            </table>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "IssuedQty")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <EditItemTemplate>
                            <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="Update" 
                                ValidationGroup="3">Update</asp:LinkButton>
                            &nbsp;
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                            &nbsp;
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <EditItemStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            </asp:DataGrid>
            <br />
     
    
    <table align="right" style="width: 100%">
        <tr>
            <td style="text-align: right; ">
                <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Issue" 
                    ValidationGroup="1" Width="50px" />
                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" 
                    onclick="btnCancel_Click" Text="Cancel" />
                <asp:Button ID="btnPrint" runat="server" onclick="btnPrint_Click" 
                    Text="Print" />
                <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" 
                    Text="Delete" Width="59px" />
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>

