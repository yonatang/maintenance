﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.SparepartInventory.Views.BeginningBalance, App_Web_nuwe21ns" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
  
  
    
  
   <div Class="section">

    <div cssClass="group">
    <h4>
                    <strong>  <asp:Label ID="lblFindSparepartBy" runat="server" 
                        Text="Find Sparepart By" CssClass="label"></asp:Label></strong></h4>
       <table style="width: 100%">
        <tr>
            <td>
                <asp:Label ID="lblInstrument" runat="server" Text="Instrument" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlInstrumentforList" runat="server" 
                    AppendDataBoundItems="True" AutoPostBack="True" DataTextField="InstrumentName" 
                    DataValueField="Id" 
                    onselectedindexchanged="ddlInstrumentforList_SelectedIndexChanged" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Label ID="lblSparepart" runat="server" Text="Sparepart" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlSparepartforList" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Sparepart</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" />
            </td>
        </tr>
    </table></div></div>
  
  
    
  
     <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
            <asp:Label ID="lblSparepartDetail" runat="server" Text="Sparepart Detail" 
                CssClass="label"></asp:Label>
        </h4>
        <asp:DataGrid ID="dgItemDetail" runat="server" AutoGenerateColumns="False" 
            CellPadding="0" ShowFooter="True" Width="99%" ForeColor="#333333" 
             CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" 
             HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White"
            oncancelcommand="dgItemDetail_CancelCommand" 
            ondeletecommand="dgItemDetail_DeleteCommand" 
            oneditcommand="dgItemDetail_EditCommand" 
            onitemcommand="dgItemDetail_ItemCommand" 
            onitemdatabound="dgItemDetail_ItemDataBound" 
            onupdatecommand="dgItemDetail_UpdateCommand" BorderColor="#003366" 
            BorderStyle="Solid" BorderWidth="1px" GridLines="None" AllowPaging="True" 
             onpageindexchanged="dgItemDetail_PageIndexChanged">
            <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:TemplateColumn HeaderText="Instrument Name">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlInstrument" runat="server" AppendDataBoundItems="True" 
                                    Width="150px" onselectedindexchanged="ddlInstrument_SelectedIndexChanged" 
                                    DataTextField="InstrumentName" DataValueField="Id">
                            <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RfvInstrument" runat="server" 
                                    ControlToValidate="ddlInstrument" ErrorMessage="Instrument Required" 
                                    InitialValue="0" ValidationGroup="3">*</asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlFInstrument" runat="server" 
                            AppendDataBoundItems="True" AutoPostBack="True" DataTextField="InstrumentName" 
                            DataValueField="Id" onselectedindexchanged="ddlFInstrument_SelectedIndexChanged" Width="200px">
                           
                            <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RfvFInstrument" runat="server" 
                                    ControlToValidate="ddlFInstrument" Display="Dynamic" 
                                    ErrorMessage="Instrument Required" InitialValue="0" 
                            ValidationGroup="2">*</asp:RequiredFieldValidator>
                    </FooterTemplate>
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Sparepart Name">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddlSparepart" runat="server" AppendDataBoundItems="True" 
                                    ValidationGroup="3" Width="150px" DataTextField="Name" 
                            DataValueField="Id">
                            <asp:ListItem Value="0">Select Sparepart</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RfvSparepart" runat="server" 
                                    ControlToValidate="ddlSparepart" ErrorMessage="Sparepart Required" 
                                    InitialValue="0" SetFocusOnError="True" ValidationGroup="3">*</asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:DropDownList ID="ddlFSparepart" runat="server" AppendDataBoundItems="True"  
                                    ValidationGroup="2" Width="150px" DataTextField="Name" DataValueField="Id" 
                                    EnableViewState="true">
                            <asp:ListItem Value="0">Select Sparepart</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RfvFSparepart" runat="server" 
                                    ControlToValidate="ddlFSparepart" ErrorMessage="Sparepart Required" 
                                    InitialValue="0" SetFocusOnError="True" ValidationGroup="2">*</asp:RequiredFieldValidator>
                    </FooterTemplate>
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "SparepartName")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Qty">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtQty" runat="server" ValidationGroup="3" Width="100px" 
                                    Text =' <%# DataBinder.Eval(Container.DataItem, "Qty")%>'></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="txtQty_FilteredTextBoxExtender" runat="server" 
                                    Enabled="True" FilterType="Numbers" TargetControlID="txtQty">
                        </cc2:FilteredTextBoxExtender>
                        <asp:RequiredFieldValidator ID="RfvQty" runat="server" 
                                    ErrorMessage="Qty Required" InitialValue="0" ValidationGroup="3" 
                                    ControlToValidate="txtQty">*</asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="txtFQty" runat="server" ValidationGroup="2" Width="100px" 
                                    EnableViewState="true" ></asp:TextBox>
                        <cc2:FilteredTextBoxExtender ID="txtFQty_FilteredTextBoxExtender" 
                                    runat="server" Enabled="True" FilterType="Numbers" 
                                    TargetControlID="txtFQty">
                        </cc2:FilteredTextBoxExtender>
                        <asp:RequiredFieldValidator ID="RfvFQty" runat="server" 
                                    ErrorMessage="Qty Required" InitialValue="0" ValidationGroup="2" 
                                    ControlToValidate="txtFQty" Display="Dynamic">*</asp:RequiredFieldValidator>
                    </FooterTemplate>
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "Qty")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <EditItemTemplate>
                        <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="Update" 
                                    ValidationGroup="3">Update</asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="lnkDelete" runat="server" 
                            CommandName="Delete">Delete</asp:LinkButton>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:LinkButton ID="lnkAddNew" runat="server" CommandName="AddNew" 
                                    ValidationGroup="2">Add New</asp:LinkButton>
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" 
                                    Text="Delete" />
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
                       
           
            <HeaderStyle BackColor="Black" ForeColor="White" />
            <PagerStyle BackColor="Black" CssClass="pgr" ForeColor="White" Height="25px" 
                HorizontalAlign="Center" Mode="NumericPages" />
                       
           
            <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        </asp:DataGrid>
 </asp:Panel>
</asp:Content>

