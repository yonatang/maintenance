﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.SparepartInventory.Views.RecieveEdit, App_Web_nuwe21ns" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
   
    
        <asp:ValidationSummary ID="RecieveSummary" runat="server" HeaderText="Error" 
        ValidationGroup="1" />
       <asp:Panel ID="Panel2" runat="server" CssClass="group" 
         style="left: 0px; top: 0px; width: 833px">
    <h4> 
        <asp:Label ID="lblRecieveInfo" runat="server" Text=" Recieve Info" CssClass="label"></asp:Label>
        </h4>
        <table style="width: 99%; height: 3px;">
        <tr>
            <td style="width: 117px; height: 29px;">
                <asp:Label ID="lblRecieptNo" runat="server" Text="Reciept No." CssClass="label"></asp:Label>
            </td>
            <td style="height: 29px">
                <asp:TextBox ID="txtRecieptNo" runat="server" CssClass="textbox"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RfvRecieptNo" runat="server" 
                    ControlToValidate="txtRecieptNo" Display="Dynamic" 
                    ErrorMessage="Reciept No. Required" ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
            <td style="height: 29px">
                <asp:Label ID="lblSourceofFinance" runat="server" Text="Source of Finance" 
                    CssClass="label"></asp:Label>
            </td>
            <td style="height: 29px; width: 278px;">
                <asp:DropDownList ID="ddlSourceofFinance" runat="server" 
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlSourceofFinance_SelectedIndexChanged" 
                    CssClass="textbox">
                    <asp:ListItem Value="&quot; &quot;">Select Source of Finance</asp:ListItem>
                    <asp:ListItem>Purchased</asp:ListItem>
                    <asp:ListItem>Donation</asp:ListItem>
                    <asp:ListItem>Transefer</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RfvSourceofFinance" runat="server" 
                    ControlToValidate="ddlSourceofFinance" Display="Dynamic" 
                    ErrorMessage="Source of Finance Required" InitialValue="&quot; &quot;" 
                    SetFocusOnError="True" ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 117px; height: 13px;">
                <asp:Label ID="lblRecievedDate" runat="server" Text="Recieved Date" 
                    CssClass="label"></asp:Label>
            </td>
            <td style="height: 13px">
                <asp:TextBox ID="Calendar1" runat="server" CssClass="textbox"></asp:TextBox>
                <cc2:CalendarExtender ID="Calendar1_CalendarExtender" runat="server" 
                    Enabled="True" PopupButtonID="Img" TargetControlID="Calendar1">
                </cc2:CalendarExtender>
                <asp:Image ID="Img" runat="server" 
                    ImageUrl="~/Images/Calendar_scheduleHS.png" />
                <asp:RegularExpressionValidator ID="REVRecievedDate" runat="server" 
                    ControlToValidate="Calendar1" Display="Dynamic" 
                    ErrorMessage="Recieved Date Is Not Valid" 
                    ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                    ValidationGroup="1">*</asp:RegularExpressionValidator>
            </td>
            <td style="height: 13px">
                <asp:Label ID="lblPurchaseNo" runat="server" Text="Purchase No." 
                    CssClass="label"></asp:Label>
            </td>
            <td style="height: 13px; width: 278px;">
                <asp:TextBox ID="txtPurchaseNo" runat="server" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 117px">
                <asp:Label ID="lblVendor" runat="server" Text="Vendor" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlVendor" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Vendor</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RfvVendor" runat="server" 
                    ErrorMessage="Vendor Required" InitialValue="0" ValidationGroup="1" 
                    ControlToValidate="ddlVendor">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Label ID="lblTendorNo" runat="server" Text="Tendor No." CssClass="label"></asp:Label>
            </td>
            <td style="width: 278px">
                <asp:TextBox ID="txtTendorNo" runat="server" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="height: 18px; width: 117px;">
                <asp:Label ID="lblVendorAgent" runat="server" Text="Vendor Agent" 
                    CssClass="label"></asp:Label>
            </td>
            <td style="height: 18px">
                <asp:TextBox ID="txtVendorAgent" runat="server" CssClass="textbox"></asp:TextBox>
            </td>
            <td style="height: 18px">
                <asp:Label ID="lblLetterNo" runat="server" Text="Letter No." CssClass="label"></asp:Label>
            </td>
            <td style="height: 18px; width: 278px;">
                <asp:TextBox ID="txtLetterNo" runat="server" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 117px">
                <asp:Label ID="lblToolKeeper" runat="server" Text="Tool Keeper" 
                    CssClass="label"></asp:Label>
            </td>
            <td>
                <strong>
                <asp:Label ID="lblToolKeeperName" runat="server" BackColor="#CCCCCC" 
                    Width="200px"></asp:Label>
                </strong>
            </td>
            <td>
                <asp:Label ID="lblSourcedescription" runat="server" Text="Source Description" 
                    CssClass="label"></asp:Label>
            </td>
            <td style="width: 278px">
                <asp:TextBox ID="txtSourcedesc" runat="server" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
    </table>
    </asp:Panel>
      <asp:Panel ID="Panel1" runat="server" CssClass="group" 
         style="left: 0px; top: 0px; width: 847px">
   <h4><asp:Label ID="lblSparepartDetail" runat="server" 
            Text="Sparepart Detail" CssClass="label"></asp:Label></h4>
         <asp:DataGrid ID="dgItemDetail" runat="server" AutoGenerateColumns="False" 
            BorderColor="#003366" BorderStyle="Solid" BorderWidth="1px" CellPadding="0" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
            DataKeyField="Id" ForeColor="#333333" GridLines="None" 
            oncancelcommand="dgItemDetail_CancelCommand" 
            ondeletecommand="dgItemDetail_DeleteCommand" 
            oneditcommand="dgItemDetail_EditCommand" 
            onitemcommand="dgItemDetail_ItemCommand" 
            onitemdatabound="dgItemDetail_ItemDataBound" 
            onupdatecommand="dgItemDetail_UpdateCommand" ShowFooter="True" Width="99%">
             <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
             <Columns>
                 <asp:TemplateColumn HeaderText="Instrument Name">
                     <EditItemTemplate>
                         <asp:DropDownList ID="ddlInstrument" runat="server" AppendDataBoundItems="True" 
                             DataTextField="InstrumentName" DataValueField="Id" 
                             onselectedindexchanged="ddlInstrument_SelectedIndexChanged" Width="150px">
                             <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                         </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="RfvInstrument" runat="server" 
                             ControlToValidate="ddlInstrument" ErrorMessage="Instrument Required" 
                             InitialValue="0" ValidationGroup="3">*</asp:RequiredFieldValidator>
                     </EditItemTemplate>
                     <FooterTemplate>
                         <asp:DropDownList ID="ddlFInstrument" runat="server" 
                             AppendDataBoundItems="True" AutoPostBack="True" DataTextField="InstrumentName" 
                             DataValueField="Id" EnableViewState="true" 
                             ondatabinding="ddlFInstrument_DataBinding" 
                             onselectedindexchanged="ddlFInstrument_SelectedIndexChanged" 
                             ValidationGroup="2" Width="150px">
                             <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                         </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="RfvFInstrument" runat="server" 
                             ControlToValidate="ddlFInstrument" Display="Dynamic" 
                             ErrorMessage="Instrument Required" InitialValue="0" ValidationGroup="2">*</asp:RequiredFieldValidator>
                     </FooterTemplate>
                     <ItemTemplate>
                         <%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>
                     </ItemTemplate>
                 </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="Sparepart Name">
                     <EditItemTemplate>
                         <asp:DropDownList ID="ddlSparepart" runat="server" AppendDataBoundItems="True" 
                             DataTextField="Name" DataValueField="Id" ValidationGroup="3" Width="150px">
                             <asp:ListItem Value="0">Select Sparepart</asp:ListItem>
                         </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="RfvSparepart" runat="server" 
                             ControlToValidate="ddlSparepart" ErrorMessage="Sparepart Required" 
                             InitialValue="0" SetFocusOnError="True" ValidationGroup="3">*</asp:RequiredFieldValidator>
                     </EditItemTemplate>
                     <FooterTemplate>
                         <asp:DropDownList ID="ddlFSparepart" runat="server" AppendDataBoundItems="True" 
                             DataTextField="Name" DataValueField="Id" EnableViewState="true" 
                             ValidationGroup="2" Width="150px">
                             <asp:ListItem Value="0">Select Sparepart</asp:ListItem>
                         </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="RfvFSparepart" runat="server" 
                             ControlToValidate="ddlFSparepart" ErrorMessage="Sparepart Required" 
                             InitialValue="0" SetFocusOnError="True" ValidationGroup="2">*</asp:RequiredFieldValidator>
                     </FooterTemplate>
                     <ItemTemplate>
                         <%# DataBinder.Eval(Container.DataItem, "SparepartName")%>
                     </ItemTemplate>
                 </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="Manufacturer">
                     <EditItemTemplate>
                         <asp:DropDownList ID="ddlManufacturer" runat="server" 
                             AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             ValidationGroup="3" Width="150px">
                             <asp:ListItem Value="0">Select Manufacturer</asp:ListItem>
                         </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="RfvManufacturer" runat="server" 
                             ControlToValidate="ddlManufacturer" ErrorMessage="Manufacturer Required" 
                             InitialValue="0" SetFocusOnError="True" ValidationGroup="3">*</asp:RequiredFieldValidator>
                     </EditItemTemplate>
                     <FooterTemplate>
                         <asp:DropDownList ID="ddlFManufacturer" runat="server" 
                             AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             EnableViewState="true" ValidationGroup="2" Width="150px">
                             <asp:ListItem Value="0">Select Manufacturer</asp:ListItem>
                         </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="RfvFManufacturer" runat="server" 
                             ControlToValidate="ddlFManufacturer" Display="Dynamic" 
                             ErrorMessage="Manufacturer Required" InitialValue="0" SetFocusOnError="True" 
                             ValidationGroup="2">*</asp:RequiredFieldValidator>
                     </FooterTemplate>
                     <ItemTemplate>
                         <%# DataBinder.Eval(Container.DataItem, "Manufacturer")%>
                     </ItemTemplate>
                 </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="Qty">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtQty" runat="server" 
                             Text=' <%# DataBinder.Eval(Container.DataItem, "Qty")%>' ValidationGroup="3" 
                             Width="100px"></asp:TextBox>
                         <cc2:FilteredTextBoxExtender ID="txtQty_FilteredTextBoxExtender" runat="server" 
                             Enabled="True" FilterType="Numbers" TargetControlID="txtQty">
                         </cc2:FilteredTextBoxExtender>
                         <asp:RequiredFieldValidator ID="RfvQty" runat="server" 
                             ControlToValidate="txtQty" ErrorMessage="Qty Required" InitialValue="0" 
                             ValidationGroup="3">*</asp:RequiredFieldValidator>
                     </EditItemTemplate>
                     <FooterTemplate>
                         <asp:TextBox ID="txtFQty" runat="server" EnableViewState="true" 
                             ValidationGroup="2" Width="100px"></asp:TextBox>
                         <cc2:FilteredTextBoxExtender ID="txtFQty_FilteredTextBoxExtender" 
                             runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtFQty">
                         </cc2:FilteredTextBoxExtender>
                         <asp:RequiredFieldValidator ID="RfvFQty" runat="server" 
                             ControlToValidate="txtFQty" Display="Dynamic" ErrorMessage="Qty Required" 
                             InitialValue="0" ValidationGroup="2">*</asp:RequiredFieldValidator>
                     </FooterTemplate>
                     <ItemTemplate>
                         <%# DataBinder.Eval(Container.DataItem, "Qty")%>
                     </ItemTemplate>
                 </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="UnitPrice">
                     <EditItemTemplate>
                         <asp:TextBox ID="txtUnitPrice" runat="server" 
                             Text='<%# DataBinder.Eval(Container.DataItem, "UnitPrice")%>' 
                             ValidationGroup="3" Width="100px"></asp:TextBox>
                         <cc2:FilteredTextBoxExtender ID="txtUnitPrice_FilteredTextBoxExtender" 
                             runat="server" Enabled="True" FilterType="Custom, Numbers" 
                             TargetControlID="txtUnitPrice" ValidChars=".">
                         </cc2:FilteredTextBoxExtender>
                         <asp:RequiredFieldValidator ID="RfvunitPrice" runat="server" 
                             ControlToValidate="txtUnitPrice" Display="Dynamic" 
                             ErrorMessage="Unit Price Required" InitialValue="0" SetFocusOnError="True" 
                             ValidationGroup="3">*</asp:RequiredFieldValidator>
                     </EditItemTemplate>
                     <FooterTemplate>
                         <asp:TextBox ID="txtFUnitPrice" runat="server" EnableViewState="true" 
                             ValidationGroup="2" Width="100px"></asp:TextBox>
                         <cc2:FilteredTextBoxExtender ID="txtFUnitPrice_FilteredTextBoxExtender" 
                             runat="server" Enabled="True" FilterType="Custom, Numbers" 
                             TargetControlID="txtFUnitPrice" ValidChars=".">
                         </cc2:FilteredTextBoxExtender>
                         <asp:RequiredFieldValidator ID="RfvFunitPrice" runat="server" 
                             ControlToValidate="txtFUnitPrice" Display="Dynamic" 
                             ErrorMessage="Unit Price Required" InitialValue="0" SetFocusOnError="True" 
                             ValidationGroup="2">*</asp:RequiredFieldValidator>
                     </FooterTemplate>
                     <ItemTemplate>
                         <%# DataBinder.Eval(Container.DataItem, "UnitPrice")%>
                     </ItemTemplate>
                 </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="TotalPrice">
                     <ItemTemplate>
                         <%# DataBinder.Eval(Container.DataItem, "TotalPrice")%>
                     </ItemTemplate>
                 </asp:TemplateColumn>
                 <asp:TemplateColumn>
                     <EditItemTemplate>
                         <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="Update" 
                             ValidationGroup="3">Update</asp:LinkButton>
                         &nbsp;<asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                     </EditItemTemplate>
                     <FooterTemplate>
                         <asp:LinkButton ID="lnkAddNew" runat="server" CommandName="AddNew" 
                             ValidationGroup="2">Add New</asp:LinkButton>
                     </FooterTemplate>
                     <ItemTemplate>
                         <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                         &nbsp;<asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" 
                             Text="Delete" />
                     </ItemTemplate>
                 </asp:TemplateColumn>
             </Columns>
             <EditItemStyle BackColor="#999999" />
             <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
             <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
             <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
             <PagerStyle  ForeColor="White" HorizontalAlign="Center" />
             <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        </asp:DataGrid>
        <table style="width: 708px">
            <tr>
                <td align="right">
                    <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" 
                        style="margin-left: 0px" Text="Save" ValidationGroup="1" Width="50px" />
                    <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" 
                        onclick="btnCancel_Click" Text="Cancel" />
                    <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" 
                        Text="Delete" Width="59px" />
                    <asp:Button ID="btnPrint" runat="server" Enabled="False" 
                        onclick="btnPrint_Click" Text="Print" />
                    <asp:Button ID="btnPost" runat="server" Enabled="False" onclick="btnPost_Click" 
                        Text="Post" Width="47px" />
                </td>
            </tr>
        </table>
        <br />
    </asp:Panel>
    

</asp:Content>

