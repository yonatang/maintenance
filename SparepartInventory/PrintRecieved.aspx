﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.SparepartInventory.Views.PrintRecieved, App_Web_nuwe21ns" stylesheettheme="Default" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
<asp:Panel ID="Panel2" runat="server" CssClass="group" 
         style="left: 0px; top: 0px; width: 842px">
    <h4> </h4>
   <div id="divprint"><fieldset> <table style="width: 100%">
        <tr>
            <td align="center" style="height: 52px; font-size: large;" bgcolor="#999999" 
                colspan="2">
                <strong>EHNRI<br />
                SPAREPART RECIEVED NOTE</strong></td>
        </tr>
        <tr>
            <td align="right" style="width: 853px">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right" style="width: 853px">
                <strong>
                <asp:Label ID="lblRecieptNo" runat="server" Text="Reciept No."></asp:Label>
                </strong>
            </td>
            <td>
                <asp:Label ID="lblRecieptNoresult" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 853px">
                <strong>
                <asp:Label ID="lblRecievedDate" runat="server" Text="Recieved Date"></asp:Label>
                </strong>
            </td>
            <td>
                <asp:Label ID="lblRecievedDateresult" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 853px">
                <strong>
                <asp:Label ID="lblSourceofFinance" runat="server" Text="Source of Finance"></asp:Label>
                </strong>:&nbsp;&nbsp;
                <asp:Label ID="lblSourceofFinanceresult" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>
                <asp:Label ID="lblPurchaseNo" runat="server" Text="Purchase No."></asp:Label>
                </strong>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblPurchaseNoresult" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>
                <asp:Label ID="lblLetterNo" runat="server" Text="Letter No."></asp:Label>
                </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblLetterNoresult" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
         <asp:DataGrid ID="dgItemDetail" runat="server" AutoGenerateColumns="False" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    ShowFooter="false" Width="99%"
                    DataKeyField="Id" Height="106px">
                    <Columns>
                        
                        <asp:TemplateColumn HeaderText="Instrument Name">
                            
                            
                            
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Sparepart Name">
                            
                            
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "SparepartName")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Manufacturer">
                            
                            
                            
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Manufacturer")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Qty">
                            
                            
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Qty")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="UnitPrice">
                            
                            
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "UnitPrice")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TotalPrice">  
                        <ItemTemplate> <%# DataBinder.Eval(Container.DataItem, "TotalPrice")%>
                        </ItemTemplate>
                        </asp:TemplateColumn>
                     
                       
                    </Columns>
                  
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <strong>
                <asp:Label ID="lblToolKeeper" runat="server" Text="Tool Keeper"></asp:Label>
                </strong>&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblToolKeeperresult" runat="server"></asp:Label>
            </td>
        </tr>
    </table></fieldset></div>
    <br />
    <table style="width: 100%">
        <tr>
            <td align="right">
                <asp:Button ID="btnPrint" runat="server" Text="Print" />
            </td>
        </tr>
    </table></asp:Panel>
</asp:Content>

