﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.SparepartInventory.Views.ItemBalance, App_Web_nuwe21ns" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
     <div Class="section">

    <div cssClass="group">
    <h4>
                    <strong>
        <asp:Label ID="lblSelectCriteria" runat="server" Text="Select Criteria" 
            CssClass="label"></asp:Label></strong></h4>
        <table style="width: 96%">
        <tr>
            <td style="width: 98px">
                <asp:Label ID="lblInstrument" runat="server" Text="Instrument" CssClass="label"></asp:Label>
            </td>
            <td style="width: 330px">
                <asp:DropDownList ID="ddlInstrument" runat="server" 
                    AppendDataBoundItems="True" AutoPostBack="True" DataTextField="InstrumentName" 
                    DataValueField="Id" 
                    onselectedindexchanged="ddlInstrument_SelectedIndexChanged" CssClass="textbox">
                    <asp:ListItem Value="0">Select Instrument</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td style="width: 72px">
                <asp:Label ID="lblSparepart" runat="server" Text="Sparepart" CssClass="label"></asp:Label>
            </td>
            <td style="width: 282px">
                <asp:DropDownList ID="ddlSparepart" runat="server" 
                    AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Sparepart</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnView" runat="server" onclick="btnView_Click" Text="View" />
            </td>
        </tr>
    </table></div></div>
     <asp:Panel ID="Panel2" runat="server" CssClass="group" 
         style="left: 0px; top: 0px; width: 826px">
    <h4> 
  
        
            <asp:Label ID="lblItemBalanceList" runat="server" Text="Item Balance List"></asp:Label></h4>
              
            <asp:GridView ID="grvItemBalanceList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="0" DataKeyNames="Id" EnableModelValidation="True" 
             CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    ForeColor="#333333" GridLines="Horizontal" 
                Width="100%">
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <Columns>
                    <asp:BoundField DataField="SparepartName" HeaderText="Sparepart Name" />
                    <asp:BoundField DataField="Qty" HeaderText="Stock Qty" />
                </Columns>
                <PagerStyle ForeColor="White" HorizontalAlign="Center" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            </asp:Panel>
  
  </asp:Content>

