﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.SparepartInventory.Views.ApprovalEdit, App_Web_nuwe21ns" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
            HeaderText="Error" ValidationGroup="1" />
        <br />
       <asp:Panel ID="Panel2" runat="server" CssClass="group">
            <h4>
                <asp:Label ID="lblApproveInfo" runat="server" Text="Approve Info" 
                    CssClass="label"></asp:Label>
            </h4>
            <table style="width: 99%">
                <tr>
                    <td style="width: 117px">
                        <asp:Label ID="lblApprovalNo" runat="server" Text="Approval No." 
                            CssClass="label"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtapprovalNo" runat="server" Width="200px" 
                            BorderStyle="Inset" Enabled="False"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RfvRequestNo" runat="server" 
                            ControlToValidate="txtapprovalNo" Display="Dynamic" 
                            ErrorMessage="Request No. Required" ValidationGroup="1">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <asp:Label ID="lblRequestBy" runat="server" Text="Requested By" 
                            CssClass="label"></asp:Label>
                    </td>
                    <td>
                        <strong>
                        <asp:Label ID="lblRequestresult" runat="server" BackColor="#CCCCCC" 
                            Width="200px"></asp:Label>
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 117px; height: 14px;">
                        <asp:Label ID="lblApprovalStatus" runat="server" CssClass="label" 
                            Text="Approval Status"></asp:Label>
                    </td>
                    <td style="height: 14px">
                        <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" 
                            CssClass="textbox" onselectedindexchanged="ddlStatus_SelectedIndexChanged">
                            <asp:ListItem Value="0">Select Status</asp:ListItem>
                            <asp:ListItem>Approved</asp:ListItem>
                            <asp:ListItem>Rejected</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RfvStatus" runat="server" 
                            ControlToValidate="ddlStatus" Display="Dynamic" ErrorMessage="Status Required" 
                            InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator>
                    </td>
                    <td style="height: 14px">
                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="label"></asp:Label>
                    </td>
                    <td style="height: 14px">
                        <strong>
                        <asp:Label ID="lblapproverresult" runat="server" BackColor="#CCCCCC" 
                            Width="200px"></asp:Label>
                        </strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 117px">
                        <asp:Label ID="lblApprovalDate" runat="server" CssClass="label" 
                            Text="Approval Date"></asp:Label>
                    </td>
                    <td>
                        <cc1:Calendar ID="Calendar1" runat="server" Height="75px" />
                        <asp:RegularExpressionValidator ID="REVApprovalDate" runat="server" 
                            ControlToValidate="Calendar1" Display="Dynamic" 
                            ErrorMessage="Approval Date Is Not Valid" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator>
                    </td>
                    <td>
                        <asp:Label ID="lblRejectedReason" runat="server" Text="Rejected Reason" 
                            Visible="False" CssClass="label"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtrejectedreason" runat="server" Height="35px" 
                            TextMode="MultiLine" Visible="False" CssClass="textboxDescShort"></asp:TextBox>
                    </td>
                </tr>
            </table>
       </asp:Panel>
        <asp:Panel ID="Panel1" runat="server" CssClass="group" 
            style="left: 0px; top: 0px; width: 821px">
            <h4>
                <asp:Label ID="lblSparepartDetail" runat="server" 
                    Text="Requested Sparepart Detail" CssClass="label"></asp:Label>
            </h4>
            <asp:DataGrid ID="dgItemDetail" runat="server" AutoGenerateColumns="False" HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White"
                BorderColor="#003366" BorderStyle="Solid" BorderWidth="1px" CellPadding="0" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                DataKeyField="Id" ForeColor="#333333" GridLines="None" 
                oncancelcommand="dgItemDetail_CancelCommand" 
                ondeletecommand="dgItemDetail_DeleteCommand" 
                oneditcommand="dgItemDetail_EditCommand" 
                onitemcommand="dgItemDetail_ItemCommand" 
                onitemdatabound="dgItemDetail_ItemDataBound" 
                onupdatecommand="dgItemDetail_UpdateCommand" ShowFooter="True" Width="99%">
                <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:TemplateColumn HeaderText="Instrument Name">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Sparepart Name">
                        <EditItemTemplate>
                             <%# DataBinder.Eval(Container.DataItem, "SparepartName")%>
                        </EditItemTemplate>
                        <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "SparepartName")%>
                            </ItemTemplate>
                    </asp:TemplateColumn>
                    
                    <asp:TemplateColumn HeaderText="Requested Qty">
                        <EditItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "RequestedQty")%>
                        </EditItemTemplate>
                        <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "RequestedQty")%>
                            </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Approved Qty">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtApprovedQty" runat="server" 
                                Text=' <%# DataBinder.Eval(Container.DataItem, "ApprovedQty")%>' ValidationGroup="3" 
                                Width="100px"></asp:TextBox>
                            <cc2:FilteredTextBoxExtender ID="txtApprovedQty_FilteredTextBoxExtender" 
                                runat="server" Enabled="True" FilterType="Numbers" 
                                TargetControlID="txtApprovedQty">
                            </cc2:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="RfvQty" runat="server" 
                                ControlToValidate="txtApprovedQty" ErrorMessage="Qty Required" InitialValue="0" 
                                ValidationGroup="3">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "ApprovedQty")%>
                            </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <EditItemTemplate>
                            <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="Update" 
                                ValidationGroup="3">Update</asp:LinkButton>
                            &nbsp;
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                            &nbsp;
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                
                <PagerStyle  ForeColor="White" HorizontalAlign="Center" />
                <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            </asp:DataGrid>
            <table align="right" style="width: 100%">
                <tr>
                    <td style="text-align: right; width: 851px">
                        <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Approve" 
                            ValidationGroup="1" Width="72px" />
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" 
                            onclick="btnCancel_Click" Text="Cancel" />
                        <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" 
                            Text="Delete" Width="59px" />
                    </td>
                </tr>
            </table>
        <br />
       
    </asp:Panel>
    <br />
</asp:Content>

