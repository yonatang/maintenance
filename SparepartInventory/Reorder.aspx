﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.SparepartInventory.Views.Reorder, App_Web_nuwe21ns" stylesheettheme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <asp:Panel ID="Panel1" runat="server" CssClass="group" 
         style="left: 0px; top: 0px; width: 853px">
   <h4>
            <asp:Label ID="lblreorderList" runat="server" Text="Reorder Sparepart List"></asp:Label>
        </h4>
        <asp:GridView ID="grvItemBalanceList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="0" DataKeyNames="Id" EnableModelValidation="True" 
                    ForeColor="#333333" GridLines="Horizontal" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                Width="97%">
            
            <Columns>
                <asp:BoundField DataField="SparepartName" HeaderText="Sparepart Name" />
                <asp:BoundField DataField="ReorderQty" HeaderText="Reorder Qty" />
                <asp:BoundField DataField="Qty" HeaderText="Stock Qty" />
            </Columns>
            <PagerStyle ForeColor="White" HorizontalAlign="Center" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    </asp:Panel>
    <br />
&nbsp;
</asp:Content>

