﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.SparepartInventory.Views.PrintIssue, App_Web_nuwe21ns" stylesheettheme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
   <div id="divprint">
   <fieldset> <table style="width: 100%">
        <tr>
            <td align="center" bgcolor="#CCCCCC" colspan="2" 
                style="height: 58px; font-size: large">
                <strong>EHNRI<br />
                SPAREPART 
                ISSUE NOTE</strong></td>
        </tr>
        <tr>
            <td align="right" style="width: 879px">
                <strong>
                        <asp:Label ID="lblIssueNo" runat="server" Text="Issue No."></asp:Label>
                    </strong>
            </td>
            <td>
                        <asp:Label ID="lblIssueNoresult" runat="server"></asp:Label>
                    </td>
        </tr>
        <tr>
            <td align="right" style="width: 879px">
                <strong>
                        <asp:Label ID="lblIssueDate" runat="server" Text="Issue Date"></asp:Label>
                    </strong>
            </td>
            <td>
                        <asp:Label ID="lblIssueDateresult" runat="server"></asp:Label>
                    </td>
        </tr>
        <tr>
            <td style="width: 879px">
                <strong>
                        <asp:Label ID="lblapprover" runat="server" Text="Approver"></asp:Label>
                    </strong>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        <asp:Label ID="lblapproverresult" runat="server"></asp:Label>
                    </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <strong>
                        <asp:Label ID="lblIssuedfor" runat="server" Text="Requester"></asp:Label>
                    </strong>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblIssuedforresult" runat="server"></asp:Label>
                    </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
         <asp:DataGrid ID="dgItemDetail" runat="server" AutoGenerateColumns="False" 
                    ShowFooter="false" Width="99%"
                    DataKeyField="Id" Height="16px">
                    <Columns>
                        
                        <asp:TemplateColumn HeaderText="Instrument Name">
                            
                            
                            
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "InstrumentName")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Sparepart Name">
                            
                            
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "SparepartName")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                       
                        <asp:TemplateColumn HeaderText="Issued Qty">
                            
                            
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "IssuedQty")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                       
                       
                     
                       
                    </Columns>
                  
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                        &nbsp;</td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                        <strong>
                        <asp:Label ID="lblToolKeeper" runat="server" Text="Tool Keeper"></asp:Label>
                        </strong>&nbsp;:
                        <asp:Label ID="lblToolKeeperresult" runat="server"></asp:Label>
                    </td>
        </tr>
    </table></fieldset></div>

    <br />
    <table style="width: 100%">
        <tr>
            <td align="right">
                <asp:Button ID="btnPrint" runat="server" Text="Print" 
                    onclick="btnPrint_Click" />
            </td>
        </tr>
    </table>

</asp:Content>

