﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.SparepartInventory.Views.ApprovalList, App_Web_nuwe21ns" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MenuContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <div Class="section">

    <div cssClass="group">
    <h4>
                    <strong>
            <asp:Label ID="lblFindBy" runat="server" Text="Find By" CssClass="label"></asp:Label></strong></h4>
                <table style="width: 99%">
            <tr>
                <td style="width: 112px">
                    <asp:Label ID="lblSelectCriteria" runat="server" Text="Find By" 
                        CssClass="label"></asp:Label>
                </td>
                <td style="width: 237px">
                    <asp:DropDownList ID="ddlSelect" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlSelect_SelectedIndexChanged" CssClass="textbox">
                        <asp:ListItem Value="0">Select Criteria</asp:ListItem>
                        <asp:ListItem Value="1">Approval No.</asp:ListItem>
                        <asp:ListItem Value="2">Date</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="editDropDown" style="width: 103px">
                    <asp:Label ID="lblValue" runat="server" Text="Value" Visible="False" 
                        CssClass="label"></asp:Label>
                </td>
                <td style="width: 296px">
                    <asp:TextBox ID="txtvalue" runat="server" Visible="False" 
                    Wrap="False" CssClass="textbox"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 112px">
                    &nbsp;</td>
                <td style="width: 237px">
                    &nbsp;</td>
                <td class="editDropDown" style="width: 103px">
                    <asp:Label ID="lblDateFrom" runat="server" Text="Date From" Visible="False" 
                        CssClass="label"></asp:Label>
                </td>
                <td style="width: 296px">
                        <asp:TextBox ID="Calendar1" runat="server" CssClass="textbox"></asp:TextBox>
                        <cc2:CalendarExtender ID="Calendar1_CalendarExtender" runat="server" 
                            Enabled="True" PopupButtonID="Img" TargetControlID="Calendar1">
                        </cc2:CalendarExtender>
                        <asp:Image ID="Img" runat="server" 
                            ImageUrl="~/Images/Calendar_scheduleHS.png" />
                    <asp:RegularExpressionValidator ID="REVDateFrom" runat="server" 
                        ControlToValidate="Calendar1" Display="Dynamic" 
                        ErrorMessage="Date From Is Not Valid" 
                        ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                        ValidationGroup="1">*</asp:RegularExpressionValidator>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 112px">
                    &nbsp;</td>
                <td style="width: 237px">
                    &nbsp;</td>
                <td class="editDropDown" style="width: 103px">
                    <asp:Label ID="lblDateTo" runat="server" Text="Date To" Visible="False" 
                        CssClass="label"></asp:Label>
                </td>
                <td style="width: 296px">
                        <asp:TextBox ID="Calendar2" runat="server" CssClass="textbox"></asp:TextBox>
                        <cc2:CalendarExtender ID="Calendar2_CalendarExtender" runat="server" 
                            Enabled="True" PopupButtonID="Img1" TargetControlID="Calendar2">
                        </cc2:CalendarExtender>
                        <asp:Image ID="Img1" runat="server" 
                            ImageUrl="~/Images/Calendar_scheduleHS.png" />
                    <asp:RegularExpressionValidator ID="REVDateTo" runat="server" 
                        ControlToValidate="Calendar2" Display="Dynamic" 
                        ErrorMessage="Date To Is Not Valid" 
                        ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                        ValidationGroup="1">*</asp:RegularExpressionValidator>
                </td>
                <td>
                    <asp:Button ID="btnFind" runat="server" Text="Find" Width="54px" 
                    onclick="btnFind_Click" ValidationGroup="1" />
                </td>
            </tr>
        </table>
    </div></div>
   <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4> 
      <asp:Label ID="lblApprovalList" runat="server" Text="Approval List" 
            CssClass="label"></asp:Label></h4>
      
  
            <asp:GridView ID="grvApprovalList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    ForeColor="#333333" GridLines="Horizontal" 
                    onrowdatabound="grvApprovalList_RowDataBound" 
                Width="100%" AllowPaging="True" 
          onpageindexchanging="grvApprovalList_PageIndexChanging">
               
                <Columns>
                    <asp:BoundField DataField="ApprovalNo" HeaderText="Approval No" />
                    <asp:BoundField DataField="ApprovalDate" HeaderText="Approval Date" />
                    <asp:BoundField DataField="ApprovalStatus" HeaderText="Approval Status" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplEdit" runat="server">Edit</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle ForeColor="White" HorizontalAlign="Center" />
                
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
  
  </asp:Panel>
</asp:Content>

