﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain;
using System.Data.SqlClient;
using System.Configuration;
using Chai.Maintenance.DBConnection;
using Chai.Maintenance.Shared;
namespace Chai.Maintenance.DataAccess
{
    public class UserDao: BaseDao
    {


        public IList<User> GetUserBySiteID(int SiteId)
        {
            string sql = " ";

            if (SiteId != 0)
                sql = "SELECT AppUser.*,Vendor.VendorName FROM AppUser " +
                    " Left Join Vendor on AppUser.VendorId=Vendor.Id WHERE SiteId = @SiteId";
          
            IList<User> lstUser = new List<User>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@SiteId", cm, SiteId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstUser.Add(GetUser(dr));
                        }
                    }
                    dr.Close();
                }


            }
            return lstUser;
        }
        public User GetUserById(int userid)
        {
            string connstring = TechnicalConfig.GetConfiguration()["ConnectionString"].ToString();
           // string connstring = "server=192.168.1.6;database=Maintenance;uid=webuser;password=password";
            
            User user;
            string sql = "SELECT AppUser.*,Vendor.VendorName FROM AppUser " +
                    " Left Join Vendor on AppUser.VendorId=Vendor.Id WHERE UserId = @UserId  "

                    + " SELECT  UserRoleId, u.RoleId as Role_Id, PermissionLevel, Name FROM UserRole AS u INNER JOIN Role AS r "
                      + " ON u.RoleId = r.RoleId WHERE  (u.UserId = @UserId)";
            using (SqlConnection cn = new SqlConnection(connstring))
            {
                using (SqlCommand cm = new SqlCommand(sql, cn))
                {
                    cn.Open();
                    DatabaseHelper.InsertInt32Param("@UserId", cm, userid);

                    using (SqlDataReader dr = cm.ExecuteReader())
                    {
                        if (dr != null)
                        {
                            if (dr.HasRows)
                            {
                                dr.Read();
                                user = GetUser(dr);
                                dr.Close();
                                return user;
                            }
                        }
                        
                    }
                    cn.Close();
                }
            }
            return null;
        }
        public User GetRequesterById(int userid)
        {
            string sql = "SELECT UserId,UserName, Password,FirstName, LastName,Email,IsActive,LastLogin,LastIp,DateCreated, DateModified,IsExternalUser,RegionId,SiteId,VendorId,VendorName FROM AppUser Left Join Vendor on AppUser.VendorId=Vendor.Id WHERE UserId = @UserId "
            + " SELECT  UserRoleId, u.RoleId as Role_Id, PermissionLevel, Name FROM UserRole AS u INNER JOIN Role AS r "
            + " ON u.RoleId = r.RoleId WHERE  (u.UserId = @UserId)";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@UserId", cm, userid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetUser(dr);
                        }
                    }
                    dr.Close();
                }
            }
            return null;
        }
        public User GetApproverById(int userid)
        {
            string sql = "SELECT UserId,UserName, Password,FirstName, LastName,Email,IsActive,LastLogin,LastIp,DateCreated, DateModified,IsExternalUser,RegionId,SiteId,VendorId,VendorName FROM AppUser Left Join Vendor on AppUser.VendorId=Vendor.Id WHERE UserId = @UserId "
            + " SELECT  UserRoleId, u.RoleId as Role_Id, PermissionLevel, Name FROM UserRole AS u INNER JOIN Role AS r "
            + " ON u.RoleId = r.RoleId WHERE  (u.UserId = @UserId)";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@UserId", cm, userid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetUser(dr);
                        }
                    }
                    dr.Close();
                }
            }
            return null;
        }
        public User GetUserByUsernameAndPassword(string username, string hashedPassword)
        {
            string sql = "SELECT UserId	FROM AppUser WHERE (UserName = @UserName) AND (Password = @Password)";
            int userid = 0;
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertStringNVarCharParam("@UserName", cm, username);
                DatabaseHelper.InsertStringNVarCharParam("@Password", cm, hashedPassword);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            userid = DatabaseHelper.GetInt32("UserId", dr);
                        }
                    }
                    dr.Close();
                }
            }

            return GetUserById(userid);
        }

        public int? GetUserIdByUsername(string username)
        {
            string sql = "SELECT UserId	FROM AppUser WHERE (UserName = @UserName)";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertStringNVarCharParam("@UserName", cm, username);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return DatabaseHelper.GetInt32("UserId", dr);
                        }
                    }
                    dr.Close();
                }
            }
            return null;
        }
        private static User GetUser(SqlDataReader dr)
        {
            User user = new User
            {
                Id = DatabaseHelper.GetInt32("UserId", dr),
                UserName = DatabaseHelper.GetString("UserName", dr),
                Password = DatabaseHelper.GetString("Password", dr),
                FirstName = DatabaseHelper.GetString("FirstName", dr),
                LastName = DatabaseHelper.GetString("LastName", dr),
                Email = DatabaseHelper.GetString("Email", dr),
                IsActive = DatabaseHelper.GetBoolean("IsActive", dr),
                LastLogin = DatabaseHelper.GetNullAuthorizedDateTime("LastLogin", dr),
                LastIp = DatabaseHelper.GetString("LastIp", dr),
                DateCreated = DatabaseHelper.GetDateTime("DateCreated", dr),
                DateModified = DatabaseHelper.GetDateTime("DateModified", dr),
               IsExternalUser = DatabaseHelper.GetBoolean("IsExternalUser", dr),
                RegionId = DatabaseHelper.GetInt32("RegionId",dr),
                SiteId = DatabaseHelper.GetInt32("SiteId",dr),
                VendorId = DatabaseHelper.GetInt32("VendorId",dr),
                VendorName = DatabaseHelper.GetString("VendorName", dr)
            };

            dr.NextResult();
            while(dr.Read())
            {
                UserRole ur = new UserRole { Id = DatabaseHelper.GetInt32("UserRoleId", dr), UserId = user.Id };
                ur.Role = new Role
                {
                    Id = DatabaseHelper.GetInt32("Role_Id", dr),
                    Name = DatabaseHelper.GetString("Name", dr),
                    PermissionLevel = DatabaseHelper.GetInt32("PermissionLevel", dr)
                };
                user.UserRoles.Add(ur);
            }
            //dr.Close();

            return user;
        }

        private static void SetUser(SqlCommand cm, User user)
        {            
            DatabaseHelper.InsertStringNVarCharParam("@Password", cm, user.Password);
            DatabaseHelper.InsertStringNVarCharParam("@FirstName", cm, user.FirstName);
            DatabaseHelper.InsertStringNVarCharParam("@LastName", cm, user.LastName);
            DatabaseHelper.InsertStringNVarCharParam("@Email", cm, user.Email);
            DatabaseHelper.InsertBooleanParam("@IsActive", cm, user.IsActive);
            DatabaseHelper.InsertDateTimeParam("@LastLogin", cm, user.LastLogin);
            DatabaseHelper.InsertStringNVarCharParam("@LastIp", cm, user.LastIp);            
            DatabaseHelper.InsertDateTimeParam("@DateModified", cm, user.DateModified);
            DatabaseHelper.InsertBooleanParam("@IsExternalUser", cm, user.IsExternalUser);
            DatabaseHelper.InsertInt32Param("@VendorId", cm, user.VendorId);
            DatabaseHelper.InsertInt32Param("@RegionId", cm, user.RegionId);
            DatabaseHelper.InsertInt32Param("@SiteId", cm, user.SiteId);

        }

        public int Save(User user)
        {
            string sql = @"INSERT INTO AppUser(UserName, Password, FirstName, LastName, Email, IsActive, LastLogin, LastIp, DateCreated, DateModified,IsExternalUser,RegionId,SiteId,VendorId)"
            + " VALUES (@UserName, @Password, @FirstName, @LastName, @Email, @IsActive, @LastLogin, @LastIp, @DateCreated, @DateModified,@IsExternalUser,@RegionId,@SiteId,@VendorId)"
            + " SELECT @@identity";
            
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertStringNVarCharParam("@UserName", cm, user.UserName);
                DatabaseHelper.InsertDateTimeParam("@DateCreated", cm, user.DateCreated);
                SetUser(cm, user);
                user.Id = int.Parse(cm.ExecuteScalar().ToString());
                foreach (UserRole ur in user.UserRoles)
                {
                    ur.UserId = user.Id;
                    SaveUserRole(ur);
                }
            }
            return user.Id;
        }

        private void SaveUserRole(UserRole ur)
        {
            string sql = "INSERT INTO UserRole(UserId, RoleId) VALUES (@UserId, @RoleId) SELECT @@identity";
            
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@RoleId", cm, ur.Role.Id);
                DatabaseHelper.InsertInt32Param("@UserId", cm, ur.UserId);
                ur.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(User user)
        {
            string sql = "UPDATE AppUser SET	Password = @Password, FirstName = @FirstName, LastName = @LastName, "
            + " Email = @Email,	IsActive = @IsActive, LastLogin = @LastLogin, LastIp = @LastIp,IsExternalUser=@IsExternalUser,RegionId=@RegionId,SiteId=@SiteId,VendorId=@VendorId, DateModified = @DateModified WHERE	UserId = @UserId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@UserId", cm, user.Id);
                SetUser(cm, user);
                cm.ExecuteNonQuery();
                UpdateUserRole(user);
            }
        }

        private void UpdateUserRole(User user)
        {
            string sql = "DELETE FROM UserRole WHERE UserRoleId = @UserRoleId";

            foreach (UserRole ur in user.UserRoles)
            {
                if (ur.IsDirty && ur.Id > 0)
                {
                    using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
                    {
                        DatabaseHelper.InsertInt32Param("@UserRoleId", cm, ur.Id);
                    }
                }
                else if (ur.IsNew() && !ur.IsDirty)
                {
                    SaveUserRole(ur);
                }                
            }
        }

        public void Delete(User user)
        {
            string sql = "DELETE FROM AppUser WHERE UserId = @UserId";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@UserId", cm, user.Id);
                cm.ExecuteNonQuery();
            }
        }

        public IList<User> GetListOfAllUsers()
        {
            string sql = "SELECT UserId FROM AppUser";
            IList<User> lstuser = new List<User>();
            IList<int> userids = new List<int>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    while (dr.Read())
                        userids.Add(DatabaseHelper.GetInt32("UserId", dr));                       
                }
            }

            foreach (int id in userids)
            {
                lstuser.Add(GetUserById(id));
            }
            return lstuser;
        }

        public IList<User> SearchUsers(string username)
        {
            if (string.IsNullOrEmpty(username))
                return GetListOfAllUsers();

            string sql = String.Format("SELECT UserId FROM AppUser WHERE UserName like '{0}'", username + "%");
            IList<User> lstuser = new List<User>();
            IList<int> userids = new List<int>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    while (dr.Read())
                        userids.Add(DatabaseHelper.GetInt32("UserId", dr));
                }
            }

            foreach (int id in userids)
            {
                lstuser.Add(GetUserById(id));
            }
            return lstuser;
        }


        public IList<User> GetUserList(int regionId)
        {
            string sql = " ";
            sql = "SELECT AppUser.*, Vendor.VendorName FROM AppUser Left Join Vendor on AppUser.VendorId=Vendor.Id " +
                  "where IsExternalUser = @BoolValue and Firstname != '' and LastName != '' and VendorId = 0 and AppUser.RegionId = @regionId";
            IList<User> lstuser = new List<User>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                  DatabaseHelper.InsertBooleanParam("@BoolValue", cm, false);
                DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstuser.Add(GetUserList(dr));
                        }

                    }
                }


            }
            return lstuser;
        }
        public IList<User> GetEnginersList(int regionId)
        {
            string sql = " ";

  
            if (regionId == 1000)
            {
                sql = "SELECT AppUser.*, Vendor.VendorName FROM AppUser Left Join UserRole on UserRole.UserId=AppUser.UserId " +
                            "left join Role on Role.RoleId = UserRole.RoleId " +
                             "Left Join Vendor on AppUser.VendorId=Vendor.Id " +
                            "where IsExternalUser = @BoolValue and Firstname != '' and LastName != '' and " +
                            "(1 = Case When Role.Name like '%' + 'Enginers' + '%' Then 1 END)";
            }
            else
            {
                sql = "SELECT AppUser.*, Vendor.VendorName FROM AppUser Left Join UserRole on UserRole.UserId=AppUser.UserId " +
                            "left join Role on Role.RoleId = UserRole.RoleId " +
                             "Left Join Vendor on AppUser.VendorId=Vendor.Id " +
                            "where IsExternalUser = @BoolValue and Firstname != '' and LastName != '' and " +
                            "(1 = Case When Role.Name like '%' + 'Enginers' + '%' Then 1 END) and AppUser.RegionId = @regionId";
            }
            IList<User> lstuser = new List<User>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {

                DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                DatabaseHelper.InsertBooleanParam("@BoolValue", cm, false);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstuser.Add(GetUserList(dr));
                        }

                    }
                }


            }
            return lstuser;
        }
        public IList<User> GetExternalUsers()
        {
            string sql = "SELECT AppUser.*, Vendor.VendorName FROM AppUser Left Join Vendor on AppUser.VendorId=Vendor.Id where IsExternalUser = @BoolValue and Firstname = '' and LastName = '' and VendorId != 0";
           
            
            IList<User> lstuser = new List<User>();
            IList<int> userids = new List<int>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertBooleanParam("@BoolValue", cm, true);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    while (dr.Read())
                        userids.Add(DatabaseHelper.GetInt32("UserId", dr));
                }
            }

            foreach (int id in userids)
            {
                lstuser.Add(GetUserById(id));
            }
            return lstuser;
        }
        private static User GetUserList(SqlDataReader dr)
        {
            User user = new User
            {
                Id = DatabaseHelper.GetInt32("UserId", dr),
                UserName = DatabaseHelper.GetString("UserName", dr),
                Password = DatabaseHelper.GetString("Password", dr),
                FirstName = DatabaseHelper.GetString("FirstName", dr),
                LastName = DatabaseHelper.GetString("LastName", dr),
                Email = DatabaseHelper.GetString("Email", dr),
                IsActive = DatabaseHelper.GetBoolean("IsActive", dr),
                LastLogin = DatabaseHelper.GetNullAuthorizedDateTime("LastLogin", dr),
                LastIp = DatabaseHelper.GetString("LastIp", dr),
                DateCreated = DatabaseHelper.GetDateTime("DateCreated", dr),
                DateModified = DatabaseHelper.GetDateTime("DateModified", dr),
                IsExternalUser = DatabaseHelper.GetBoolean("IsExternalUser", dr),
                RegionId = DatabaseHelper.GetInt32("RegionId", dr),
                SiteId = DatabaseHelper.GetInt32("SiteId", dr),
                VendorId = DatabaseHelper.GetInt32("VendorId", dr),
                VendorName = DatabaseHelper.GetString("VendorName", dr)
            };
            return user;
        }
        public IList<User> GetUsers(int RegionId)
        {

            string connstring = TechnicalConfig.GetConfiguration()["ConnectionString"].ToString();
            string sql = "SELECT UserId,UserName, Password,FirstName, LastName,Email,IsActive,LastLogin,LastIp,DateCreated, DateModified,IsExternalUser,RegionId,SiteId,VendorId,VendorName FROM AppUser Left Join Vendor on AppUser.VendorId=Vendor.Id WHERE RegionId = @regionId ";

            IList<User> lstuser = new List<User>();
            IList<int> userids = new List<int>();
           using(SqlConnection cn = new SqlConnection(connstring))
           {
               cn.Open();
               using (SqlCommand cm = new SqlCommand(sql, cn))
            {
                DatabaseHelper.InsertInt32Param("@regionId", cm, RegionId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstuser.Add(GetUserList(dr));
                            userids.Add(DatabaseHelper.GetInt32("UserId", dr));

                        }
                    }
              

                }
            }
               cn.Close();
        }




            return lstuser;
        }
    }
}
