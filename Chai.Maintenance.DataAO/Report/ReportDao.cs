﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
namespace Chai.Maintenance.DataAccess.Report
{
    public class ReportDao : BaseDao
    {
        public DataSet GetRegionReport()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SPRRegion";
            //cmd.Parameters.AddWithValue("@regionName");
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetSiteReport(int regionId,int userregionId,int siteTypeId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SiteReport";
            cmd.Parameters.AddWithValue("@RegionId", regionId);
            cmd.Parameters.AddWithValue("@userregionId", userregionId);
            cmd.Parameters.AddWithValue("@SiteTypeId", siteTypeId);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetSiteContactReport(int regionId, int userregionId, int siteId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SiteContactReport";
            cmd.Parameters.AddWithValue("@RegionId", regionId);
            cmd.Parameters.AddWithValue("@userregionId", userregionId);
            cmd.Parameters.AddWithValue("@SiteId", siteId);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public IList GetMapStatisticReport()
        {
            string sql = "";
            sql = " SELECT COUNT(Distinct Instrument.Id) as No_Instrument, COUNT(Distinct Site.Id) as No_Site,COUNT(Case When Instrument.Status = 2 Then 1 End)as Not_Functional,dbo.Region.RegionName,Region.MapId,'' " +
                      " FROM  dbo.Instrument INNER JOIN " +
                       " dbo.Site ON dbo.Instrument.Site_Id = dbo.Site.Id INNER JOIN " +
                        " dbo.Region ON dbo.Site.Region_Id = dbo.Region.Id INNER JOIN " +
                        " dbo.InstrumentLookup ON dbo.Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id " +
                        " Group by dbo.Region.MapId,dbo.Region.RegionName ";
            IList obj = new ArrayList();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {

                
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            object[] values = new object[dr.FieldCount];
                            dr.GetSqlValues(values);
                            obj.Add(values);

                        }
                    }
                }
            }
            return obj;


        }
        public DataSet GetInstrumentReport(int regionId, int userregionId, int siteId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "InstrumentReport";
            cmd.Parameters.AddWithValue("@RegionId", regionId);
            cmd.Parameters.AddWithValue("@userregionId", userregionId);
            cmd.Parameters.AddWithValue("@SiteId", siteId);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetUpcomeingPreventiveReport(int regionId, int userregionId, int siteId,int instrumentcatagoryId,DateTime datefrom,DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RUpcomingPreventiveMaintenance";
            cmd.Parameters.AddWithValue("@regionId", regionId);
            cmd.Parameters.AddWithValue("@userRegionId", userregionId);
            cmd.Parameters.AddWithValue("@siteId", siteId);
            cmd.Parameters.AddWithValue("@instrumentCategoryId", instrumentcatagoryId);
            cmd.Parameters.AddWithValue("@ScheduledDateFrom",datefrom);
            cmd.Parameters.AddWithValue("@ScheduledDateTo",dateto);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetUpcomeingCurativeReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RUpcomingCurativeMaintenance";
            cmd.Parameters.AddWithValue("@regionId", regionId);
            cmd.Parameters.AddWithValue("@userRegionId", userregionId);
            cmd.Parameters.AddWithValue("@siteId", siteId);
            cmd.Parameters.AddWithValue("@instrumentCategoryId", instrumentcatagoryId);
            cmd.Parameters.AddWithValue("@ScheduledDateFrom", datefrom);
            cmd.Parameters.AddWithValue("@ScheduledDateTo", dateto);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetDisposalReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RDisposedInstruments";
            cmd.Parameters.AddWithValue("@regionId", regionId);
            cmd.Parameters.AddWithValue("@userRegionId", userregionId);
            cmd.Parameters.AddWithValue("@siteId", siteId);
            cmd.Parameters.AddWithValue("@instrumentCategoryId", instrumentcatagoryId);
            cmd.Parameters.AddWithValue("@disposedDateFrom", datefrom);
            cmd.Parameters.AddWithValue("@disposedDateTo", dateto);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetTransferReport(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RTransferedInstruments";
            cmd.Parameters.AddWithValue("@regionId", regionId);
            cmd.Parameters.AddWithValue("@userRegionId", userregionId);
            cmd.Parameters.AddWithValue("@siteId", siteId);
            cmd.Parameters.AddWithValue("@instrumentCategoryId", instrumentcatagoryId);
            cmd.Parameters.AddWithValue("@transferedDateFrom", datefrom);
            cmd.Parameters.AddWithValue("@transferedDateTo", dateto);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetPerformedPreventiveMaintenance(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RPerformedPreventiveMaintenance";
            cmd.Parameters.AddWithValue("@regionId", regionId);
            cmd.Parameters.AddWithValue("@userRegionId", userregionId);
            cmd.Parameters.AddWithValue("@siteId", siteId);
            cmd.Parameters.AddWithValue("@instrumentCategoryId", instrumentcatagoryId);
            cmd.Parameters.AddWithValue("@ScheduledDateFrom", datefrom);
            cmd.Parameters.AddWithValue("@ScheduledDateTo", dateto);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetPerformedCurativeMaintenance(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RPerformedCurativeMaintenance";
            cmd.Parameters.AddWithValue("@regionId", regionId);
            cmd.Parameters.AddWithValue("@userRegionId", userregionId);
            cmd.Parameters.AddWithValue("@siteId", siteId);
            cmd.Parameters.AddWithValue("@instrumentCategoryId", instrumentcatagoryId);
            cmd.Parameters.AddWithValue("@ScheduledDateFrom", datefrom);
            cmd.Parameters.AddWithValue("@ScheduledDateTo", dateto);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetNonFunctionalInstrumentsBySiteANDInstrumentCategory(int regionId, int userregionId, int siteId, int instrumentcatagoryId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RNonFunctionalInstrumentsBySiteANDInstrumentCategory";
            cmd.Parameters.AddWithValue("@regionId", regionId);
            cmd.Parameters.AddWithValue("@userRegionId", userregionId);
            cmd.Parameters.AddWithValue("@siteId", siteId);
            cmd.Parameters.AddWithValue("@instrumentCategoryId", instrumentcatagoryId);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetInstrumentProblemByManufacturer(int userregionId,int instrumentcatagoryId,int manufactureid)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RInstrumentProblemByManufacturer";
            cmd.Parameters.AddWithValue("@userRegionId", userregionId);
            cmd.Parameters.AddWithValue("@instrumentCategoryId", instrumentcatagoryId);
            cmd.Parameters.AddWithValue("@manufacturerId", manufactureid);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetQuarterCurativeReport(int regionId, int userregionId, DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "QuarterCurativeReport";
            cmd.Parameters.AddWithValue("@regionId", regionId);
            cmd.Parameters.AddWithValue("@userRegionId", userregionId);           
            cmd.Parameters.AddWithValue("@DateFrom", datefrom);
            cmd.Parameters.AddWithValue("@DateTo", dateto);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetQuarterPreventiveReport(int regionId, int userregionId, DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "QuarterPreventiveReport";
            cmd.Parameters.AddWithValue("@regionId", regionId);
            cmd.Parameters.AddWithValue("@userRegionId", userregionId);
            cmd.Parameters.AddWithValue("@DateFrom", datefrom);
            cmd.Parameters.AddWithValue("@DateTo", dateto);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }

        public DataSet GetEscalatedCurativeMaintenances(int regionId, int userregionId, int siteId, int instrumentcatagoryId, DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "REscalatedCurativeMaintenances";
            cmd.Parameters.AddWithValue("@regionId", regionId);
            cmd.Parameters.AddWithValue("@userRegionId", userregionId);
            cmd.Parameters.AddWithValue("@siteId", siteId);
            cmd.Parameters.AddWithValue("@instrumentCategoryId", instrumentcatagoryId);
            cmd.Parameters.AddWithValue("@EscalatedDateFrom", datefrom);
            cmd.Parameters.AddWithValue("@EscalatedDateTo", dateto);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetNotifiedConsumables(int regionId, int userregionId, int siteId, int consumableId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RNotifiedConsumables";
            cmd.Parameters.AddWithValue("@regionId", regionId);
            cmd.Parameters.AddWithValue("@userRegionId", userregionId);
            cmd.Parameters.AddWithValue("@siteId", siteId);
            cmd.Parameters.AddWithValue("@consumableId", consumableId);
        
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public IList GetNotifiedProblemsChart(string datefrom, string dateto)
        {

            string sql = " select count(Region.Id) AS NoOfNotifiedProblems, Region.RegionName from Problem " +
                         " INNER JOIN Site ON Site.Id = Problem.SiteId" +
                         " INNER join Region ON Region.Id = Site.Region_Id" +
                         " where 1 = Case When  @DateFrom='' AND @DateTo='' Then 1 when Problem.DateReported BETWEEN @DateFrom AND @DateTo Then 1 END  " +
                         " group by Region.RegionName" +
                         " Order by NoOfNotifiedProblems desc";

            IList obj = new ArrayList();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {

                if (datefrom != "" && dateto != "")
                {
                    DatabaseHelper.InsertStringNVarCharParam("@datefrom", cm, datefrom);
                    DatabaseHelper.InsertStringNVarCharParam("@dateto", cm, dateto);

                }
                else
                {
                    DatabaseHelper.InsertStringNVarCharParam("@datefrom", cm, datefrom);
                    DatabaseHelper.InsertStringNVarCharParam("@dateto", cm, dateto);
                }

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            object[] values = new object[dr.FieldCount];
                            dr.GetSqlValues(values);
                            obj.Add(values);

                        }
                    }
                }
            }
            return obj;


        }
        public IList GetFrequentlyMaintainedInstrumentsChart(string datefrom, string dateto)
        {
            string sql = " select count(CurativeMaintenance.InstrumentId) AS NoOfMaintenance, InstrumentLookup.InstrumentName AS Instrument from CurativeMaintenance " +
                        "  INNER JOIN Instrument ON Instrument.Id = CurativeMaintenance.InstrumentId " +
                        "  INNER JOIN InstrumentLookup ON InstrumentLookup.Id = Instrument.InstrumentName_Id" +
                        " where 1 = Case When  @DateFrom='' AND @DateTo='' Then 1 when CurativeMaintenance.Date BETWEEN @DateFrom AND @DateTo Then 1 END  " +
                        "  group by InstrumentLookup.InstrumentName" +
                        " Order by NoOfMaintenance desc";

            IList obj = new ArrayList();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {

                if (datefrom != "" && dateto != "")
                {
                    DatabaseHelper.InsertStringNVarCharParam("@datefrom", cm, datefrom);
                    DatabaseHelper.InsertStringNVarCharParam("@dateto", cm, dateto);

                }
                else
                {
                    DatabaseHelper.InsertStringNVarCharParam("@datefrom", cm, datefrom);
                    DatabaseHelper.InsertStringNVarCharParam("@dateto", cm, dateto);
                }

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            object[] values = new object[dr.FieldCount];
                            dr.GetSqlValues(values);
                            obj.Add(values);

                        }
                    }
                }
            }
            return obj;


        }
        public IList GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(string datefrom, string dateto)
        {

            string sql = " select count(Problem.InstrumentId) AS NoOfNotifiedproblem, Manufacturer.Name AS Manufacturers from Problem " +
                         " INNER JOIN Instrument ON Instrument.Id = Problem.InstrumentId " +
                         " INNER JOIN InstrumentLookup ON InstrumentLookup.Id = Instrument.InstrumentName_Id " +
                         " INNER JOIN Manufacturer ON Manufacturer.Id = Instrument.Manufacturer_Id " +
                         " where 1 = Case When  @DateFrom='' AND @DateTo='' Then 1 when Problem.DateReported BETWEEN @DateFrom AND @DateTo Then 1 END  " +
                         " group by Manufacturer.Name " +
                         " Order by NoOfNotifiedproblem desc ";


            IList obj = new ArrayList();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {

                if (datefrom != "" && dateto != "")
                {
                    DatabaseHelper.InsertStringNVarCharParam("@datefrom", cm, datefrom);
                    DatabaseHelper.InsertStringNVarCharParam("@dateto", cm, dateto);

                }
                else
                {
                    DatabaseHelper.InsertStringNVarCharParam("@datefrom", cm, datefrom);
                    DatabaseHelper.InsertStringNVarCharParam("@dateto", cm, dateto);
                }

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            object[] values = new object[dr.FieldCount];
                            dr.GetSqlValues(values);
                            obj.Add(values);

                        }
                    }
                }
            }
            return obj;

        }
        public IList GetRFrequentlyReportedProblemTypesChart(string datefrom, string dateto)
        {

            string sql = " select count(Problem.Id) AS NoOfProblems, ProblemType.Name AS ProblemType  from Problem  " +
                         " INNER JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id " +
                         " where 1 = Case When  @DateFrom='' AND @DateTo='' Then 1 when Problem.DateReported BETWEEN @DateFrom AND @DateTo Then 1 END  " +
                         " group by ProblemType.Name " +
                         " Order by NoOfProblems desc ";


            IList obj = new ArrayList();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {

                if (datefrom != "" && dateto != "")
                {
                    DatabaseHelper.InsertStringNVarCharParam("@datefrom", cm, datefrom);
                    DatabaseHelper.InsertStringNVarCharParam("@dateto", cm, dateto);

                }
                else
                {
                    DatabaseHelper.InsertStringNVarCharParam("@datefrom", cm, datefrom);
                    DatabaseHelper.InsertStringNVarCharParam("@dateto", cm, dateto);
                }

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            object[] values = new object[dr.FieldCount];
                            dr.GetSqlValues(values);
                            obj.Add(values);

                        }
                    }
                }
            }
            return obj;

        }
        
        public DataSet GetMostIssuedSparepartsbyInstrumentsChart(DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RSparepartInstrumentChart";
            cmd.Parameters.AddWithValue("@DateFrom", datefrom);
            cmd.Parameters.AddWithValue("@DateTo", dateto);


            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetMostRequestedSparepartsbyInstrumentsChart(DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RRequestedSparepartInstrumentChart";
            cmd.Parameters.AddWithValue("@DateFrom", datefrom);
            cmd.Parameters.AddWithValue("@DateTo", dateto);


            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }

        public DataSet GetNotifiedProblemsChart(DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RProblemNotificationsChart";
            cmd.Parameters.AddWithValue("@DateFrom", datefrom);
            cmd.Parameters.AddWithValue("@DateTo", dateto);


            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetFrequentlyMaintainedInstrumentsChart(DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RFrequentlyMaintainedInstrumentsChart";
            cmd.Parameters.AddWithValue("@DateFrom", datefrom);
            cmd.Parameters.AddWithValue("@DateTo", dateto);


            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetFrequentProblemNotificationOnInstrumentsByManufacturerChart(DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RFrequentProblemNotificationOnInstrumentsByManufacturerChart";
            cmd.Parameters.AddWithValue("@DateFrom", datefrom);
            cmd.Parameters.AddWithValue("@DateTo", dateto);


            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetRFrequentlyReportedProblemTypesChart(DateTime datefrom, DateTime dateto)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RFrequentlyReportedProblemTypesChart";
            cmd.Parameters.AddWithValue("@DateFrom", datefrom);
            cmd.Parameters.AddWithValue("@DateTo", dateto);

            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetInstrumentsUnderContract(int regionId, int userRegionId, int siteId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RInstrumentsUnderContract";
            cmd.Parameters.AddWithValue("@RegionId", regionId);
            cmd.Parameters.AddWithValue("@userregionId", userRegionId);
            cmd.Parameters.AddWithValue("@SiteId", siteId);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
        public DataSet GetCurativeMaintenanceTracking(int regionId, int userRegionId, int siteId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DefaultConnection;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RCurativeMaintenanceTracking";
            cmd.Parameters.AddWithValue("@RegionId", regionId);
            cmd.Parameters.AddWithValue("@userregionId", userRegionId);
            cmd.Parameters.AddWithValue("@SiteId", siteId);
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            DefaultConnection.Close();
            return ds;
        }
    }
}
