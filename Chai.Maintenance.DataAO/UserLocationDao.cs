﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.DataAccess
{
    public class UserLocationDao:BaseDao
    {
        public UserLocation GetUserLocationById(int locationid)
        {
            string sql = "SELECT Id, User_Id, Region_Id,Site_Id FROM Location where Id = @locationid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@locationid", cm, locationid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetUserLocation(dr);
                        }
                    }
                    dr.Close();
                }
            }
            return null;
        }

        private static UserLocation GetUserLocation(SqlDataReader dr)
        {
            UserLocation userlocation = new UserLocation
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                UserId =DatabaseHelper.GetInt32("User_Id", dr),
                RegionId = DatabaseHelper.GetInt32("Region_Id", dr),
                SiteId = DatabaseHelper.GetInt32("Site_Id", dr)
            };

            return userlocation;
        }
        private static void SetUserLocation(SqlCommand cm, UserLocation userlocation)
        {
            
            //DatabaseHelper.InsertInt32Param("@User_Id", cm, userlocation.UserId);
            DatabaseHelper.InsertInt32Param("@Region_Id", cm, userlocation.RegionId);
            DatabaseHelper.InsertInt32Param("@Site_Id", cm, userlocation.SiteId);
        }

        public void Save(UserLocation userlocation,int userid)
        {
            string sql = "INSERT INTO Location(User_Id, Region_Id,Site_Id) VALUES (@User_Id, @Region_Id,@Site_Id) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@User_Id", cm, userid);
                SetUserLocation(cm, userlocation);
                userlocation.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(UserLocation userlocation)
        {
            string sql = "Update Location SET User_Id =@User_Id , Region_Id = @Region_Id,Site_Id=@Site_Id where Id = @LocationId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@LocationId", cm, userlocation.Id);
                SetUserLocation(cm, userlocation);
                cm.ExecuteNonQuery();
            }
        }
        
        public void Delete(int locationid)
        {
            string sql = "Delete Location where Id = @locationid";
           
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@locationid", cm, locationid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<UserLocation> GetListOfUserLocation(int findby,int userid,int regionid,int siteid)
        {            
            string sql = "SELECT ID, User_Id, Region_Id,Site_Id FROM UserLocation order by Id";
            IList<UserLocation> lstuserlocation = new List<UserLocation>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstuserlocation.Add(GetUserLocation(dr));
                        }
                    }
                    dr.Close();
                }
            }
            return lstuserlocation;
        }
        public IList<UserLocation> GetListOfUserLocationbyUserIdLocation(int User_Id)
        {
            string sql = "SELECT ID, User_Id, Region_Id,Site_Id FROM UserLocation where User_Id = @User_Id order by Id";
            IList<UserLocation> lstuserlocation = new List<UserLocation>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@User_Id", cm, User_Id);
               
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstuserlocation.Add(GetUserLocation(dr));
                        }
                    }
                    dr.Close();
                }
            }
            return lstuserlocation;
        }
    }
}
