﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;

namespace Chai.Maintenance.DataAccess.Maintenance
{
    public class SparepartsDao : BaseDao
    {
        public Spareparts GetSparepartsById(int spareId)
        {
            string sql = " SELECT Spareparts.*,SparepartType.Name AS SparePartName FROM Spareparts" +
                         " LEFT JOIN SparepartType on Spareparts.SparepartId = SparepartType.Id ";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@spareId", cm, spareId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetSpareparts(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static Spareparts GetSpareparts(SqlDataReader dr)
        {
            Spareparts spare = new Spareparts
            {
                CurativeMaintenanceId = DatabaseHelper.GetInt32("CurativeMaintenanceId", dr),
                PreventiveMaintenanceId = DatabaseHelper.GetInt32("PreventiveMaintenanceId", dr),
                SparepartId = DatabaseHelper.GetInt32("SparepartId", dr),
                Quantity = DatabaseHelper.GetInt32("Quantity", dr)
            };

            return spare;
        }

        private static void SetSpareparts(SqlCommand cm, Spareparts spare)
        {
            DatabaseHelper.InsertInt32Param("CurativeMaintenanceId", cm, spare.CurativeMaintenanceId);
            DatabaseHelper.InsertInt32Param("PreventiveMaintenanceId", cm, spare.PreventiveMaintenanceId);
            DatabaseHelper.InsertInt32Param("SparepartId", cm, spare.SparepartId);
            DatabaseHelper.InsertInt32Param("Quantity", cm, spare.Quantity);
        }

        public void Save(Spareparts spare, int parentId, string type)
        {
            string sql = "";
            if (type == "Prev")
                sql = "INSERT INTO Spareparts(CurativeMaintenanceId, PreventiveMaintenanceId, SparepartId, Quantity) " +
                      " VALUES (0, @parentId, @SparepartId, @Quantity) SELECT @@identity";
            else
                sql = "INSERT INTO Spareparts(CurativeMaintenanceId, PreventiveMaintenanceId, SparepartId, Quantity) " +
                     " VALUES (@parentId, 0, @SparepartId, @Quantity) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@parentId", cm, parentId);
                SetSpareparts(cm, spare);
                spare.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(Spareparts spare)
        {
            string sql = "Update Spareparts SET CurativeMaintenanceId=@CurativeMaintenanceId,PreventiveMaintenanceId=@PreventiveMaintenanceId,SparepartId=@SparepartId,Quantity=@Quantity  where Id = @SparepartId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {

                SetSpareparts(cm, spare);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int spareid)
        {
            string sql = "Delete Spareparts where Id = @spareid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@spareid", cm, spareid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<Spareparts> GetListOfSpareparts(int curativeid)
        {
            string sql;

            sql = " SELECT Spareparts.*,SparepartType.Name as SparePartName FROM Spareparts" +


                    " LEFT JOIN SparepartType on Spareparts.SparepartId = SparepartType.Id where @curativeid  =Spareparts.CurativeMaintenanceId";
            IList<Spareparts> lstSpareparts = new List<Spareparts>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {

                DatabaseHelper.InsertInt32Param("@curativeid", cm, curativeid);


                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstSpareparts.Add(GetSpareparts(dr));
                        }
                    }
                }
            }
            return lstSpareparts;
        }

    }
}
