﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.DataAccess.Maintenance
{
    public class ConfirmationDao : BaseDao
    {
        public Confirmation GetConfirmationById(int ConfirmationId)
        {
            string sql = "SELECT * FROM Confirmation where Id = @DisposalId";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@ConfirmationId", cm, ConfirmationId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetConfirmation(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static Confirmation GetConfirmationlist(SqlDataReader dr)
        {
            Confirmation _Confirmation = new Confirmation
            {
                InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr),
                ProblemId = DatabaseHelper.GetInt32("ProblemId", dr),
                UserComments = DatabaseHelper.GetString("UserComments", dr),
                IsResolved = DatabaseHelper.GetBoolean("IsResolved", dr),
                Date = DatabaseHelper.GetDateTime("Date", dr)
            };
            return _Confirmation;
        }

        private static Confirmation GetConfirmation(SqlDataReader dr)
        {
            Confirmation _Confirmation = new Confirmation
            {
                InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr),
                ProblemId = DatabaseHelper.GetInt32("ProblemId", dr),
                UserComments = DatabaseHelper.GetString("UserComments", dr),
                IsResolved = DatabaseHelper.GetBoolean("IsResolved", dr),
                Date = DatabaseHelper.GetDateTime("Date", dr)
            };
            return _Confirmation;
        }

        private static void SetConfirmation(SqlCommand cm, Confirmation _Confirmation)
        {

            DatabaseHelper.InsertInt32Param("InstrumentId", cm, _Confirmation.InstrumentId);
            DatabaseHelper.InsertInt32Param("ProblemId", cm, _Confirmation.ProblemId);
            DatabaseHelper.InsertStringVarCharParam("UserComments", cm, _Confirmation.UserComments);
            DatabaseHelper.InsertBooleanParam("IsResolved", cm, _Confirmation.IsResolved);
            DatabaseHelper.InsertDateTimeParam("Date", cm, _Confirmation.Date);
           
        }

        public void Save(Confirmation _Confirmation, string searchBy,int ScheduleId)
        {
            string sql = "INSERT INTO Confirmation(InstrumentId, ProblemId, UserComments, IsResolved, Date)"
                         + " VALUES (@InstrumentId, @ProblemId, @UserComments, @IsResolved, @Date) "
                         + " SELECT @@identity";
            ProblemDao _problem = new ProblemDao();
            SqlTransaction tr = DefaultConnection.BeginTransaction();
            try
            {
                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                {
                    SetConfirmation(cm, _Confirmation);
                    if (_Confirmation.IsResolved == true)
                    {
                        if (searchBy == "PreventiveConfirmation")
                            _problem.UpdateStatus("ConfirmationForPreventive", (int)MaintenanceStatus.ConfirmationForPreventive, ScheduleId, "Preventive", tr);
                        else
                        {
                            _problem.UpdateStatus("ConfirmationForCurative", (int)MaintenanceStatus.ConfirmationForCurative, ScheduleId, "Curative", tr);
                            
                        }

                        _problem.UpdateStatus("ConfirmationForInstrument", (int)MaintenanceStatus.ConfirmationForInstrument, _Confirmation.InstrumentId, "", tr);
                        _problem.UpdateStatus("ConfirmationForInstrumentFunctional", Convert.ToInt32(_Confirmation.IsResolved), _Confirmation.InstrumentId, "", tr);
                    }
                    _Confirmation.Id = int.Parse(cm.ExecuteScalar().ToString());
                    tr.Commit();
                }
            }
            catch (SqlException ex)
            {
                tr.Rollback();
            }
        }

        public void Update(Confirmation _Confirmation)
        {
            string sql = "Update Confirmation SET InstrumentId = @InstrumentId, ProblemId = @ProblemId, UserComments = @UserComments, IsResolved = @IsResolved, Date = @Date where Id =@Id";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@Id", cm, _Confirmation.Id);
                SetConfirmation(cm, _Confirmation);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int ConfirmationId)
        {
            string sql = "Delete Confirmation where Id = @ConfirmationId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@ConfirmationId", cm, ConfirmationId);
                cm.ExecuteNonQuery();
            }
        }


    }
}