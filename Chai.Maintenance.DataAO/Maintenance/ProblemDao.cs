﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.DataAccess.Maintenance
{
    public class ProblemDao : BaseDao
    {
        
        public Problem GetProblemById(int problemId)
        {
            string sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id WHERE Problem.Id = @problemId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@problemId", cm, problemId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetProblem(dr);
                        }
                    }
                    dr.Close();
                }
            }
            return null;
        }

        public IList<Problem> GetProblemForCurativeSchedule(int regionId, int siteId, int InstrumentCategoryId, int ProblemType)
        {
            string sql = " ";
            if (siteId == 0 && InstrumentCategoryId == 0 && ProblemType == 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id order by Problem.DateReported desc";
            }
            else if (siteId != 0 && InstrumentCategoryId != 0 && ProblemType != 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @siteId = Problem.SiteId AND @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId AND @ProblemType = Problem.ProblemTypeId  and @RegionId = Site.Region_Id  order by Problem.DateReported desc";
            }
            else if (siteId != 0 && InstrumentCategoryId == 0 && ProblemType == 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @siteId = Problem.SiteId  order by Problem.DateReported desc";
            }
            else if (siteId != 0 && InstrumentCategoryId != 0 && ProblemType == 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @siteId = Problem.SiteId AND @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId  order by Problem.DateReported desc";
            }
            else if (siteId != 0 && InstrumentCategoryId == 0 && ProblemType != 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @siteId = Problem.SiteId AND @ProblemType = Problem.ProblemTypeId  order by Problem.DateReported desc";
            }
            else if (siteId == 0 && InstrumentCategoryId == 0 && ProblemType != 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @ProblemType = Problem.ProblemTypeId  order by Problem.DateReported desc";
            }
            else if (siteId == 0 && InstrumentCategoryId != 0 && ProblemType == 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId  order by Problem.DateReported desc";
            }
            else if (siteId == 0 && InstrumentCategoryId != 0 && ProblemType != 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId AND  @ProblemType = Problem.ProblemTypeId  order by Problem.DateReported desc";
            }
            else if (siteId != 0 && InstrumentCategoryId == 0 && ProblemType == 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @siteId = Problem.SiteId AND @RegionId = Site.Region_Id  order by Problem.DateReported desc";
            }
            else if (siteId == 0 && InstrumentCategoryId == 0 && ProblemType == 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @RegionId = Site.Region_Id  order by Problem.DateReported desc";
            }
            else if (siteId == 0 && InstrumentCategoryId != 0 && ProblemType == 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId AND @RegionId = Site.Region_Id  order by Problem.DateReported desc";
            }
            else if (siteId == 0 && InstrumentCategoryId != 0 && ProblemType != 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId AND @RegionId = Site.Region_Id AND  @ProblemType = Problem.ProblemTypeId  order by Problem.DateReported desc";
            }
            else if (siteId == 0 && InstrumentCategoryId == 0 && ProblemType != 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @RegionId = Site.Region_Id AND  @ProblemType = Problem.ProblemTypeId  order by Problem.DateReported desc";
            }
            else if (siteId != 0 && InstrumentCategoryId == 0 && ProblemType != 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE  @siteId = Problem.SiteId AND @RegionId = Site.Region_Id AND  @ProblemType = Problem.ProblemTypeId  order by Problem.DateReported desc";
            }
            else if (siteId != 0 && InstrumentCategoryId != 0 && ProblemType == 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @siteId = Problem.SiteId AND @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId  and @RegionId = Site.Region_Id  order by Problem.DateReported desc";
            }
            IList<Problem> lstProblem = new List<Problem>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (siteId != 0)
                    DatabaseHelper.InsertInt32Param("@siteId", cm, siteId);
                if (InstrumentCategoryId != 0)
                    DatabaseHelper.InsertInt32Param("@InstrumentCategoryId", cm, InstrumentCategoryId);
                if (ProblemType != 0)
                    DatabaseHelper.InsertInt32Param("@ProblemType", cm, ProblemType);
                if (regionId != 0)
                    DatabaseHelper.InsertInt32Param("@RegionId", cm, regionId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstProblem.Add(GetProblem(dr));
                        }
                    }
                }


            }
            return lstProblem;
        }
        public IList<Problem> GetProblemBySiteInsCategoryId(int regionId, int siteId, int InstrumentCategoryId, int ProblemType)
        {
            string sql = " ";

            if (siteId == 0 && InstrumentCategoryId == 0 && ProblemType == 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id";
            }
            else if (siteId == 0 && InstrumentCategoryId == 0 && ProblemType == 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @RegionId = Site.Region_Id";
            }
            else if (siteId != 0 && InstrumentCategoryId != 0 && ProblemType != 0 && regionId !=0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @siteId = Problem.SiteId AND @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId AND @ProblemType = Problem.ProblemTypeId  and @RegionId = Site.Region_Id";
            }
            else if (siteId != 0 && InstrumentCategoryId == 0 && ProblemType == 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @siteId = Problem.SiteId";
            }
            else if (siteId != 0 && InstrumentCategoryId != 0 && ProblemType == 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @siteId = Problem.SiteId AND @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId ";
            }
            else if (siteId != 0 && InstrumentCategoryId == 0 && ProblemType != 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @siteId = Problem.SiteId AND @ProblemType = Problem.ProblemTypeId";
            }
            else if (siteId == 0 && InstrumentCategoryId == 0 && ProblemType != 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @ProblemType = Problem.ProblemTypeId";
            }
            else if (siteId == 0 && InstrumentCategoryId != 0 && ProblemType == 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId";
            }
            else if (siteId == 0 && InstrumentCategoryId != 0 && ProblemType != 0 && regionId == 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId AND  @ProblemType = Problem.ProblemTypeId";
            }

            else if (siteId != 0 && InstrumentCategoryId == 0 && ProblemType == 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @siteId = Problem.SiteId and @RegionId = Site.Region_Id";
            }
            else if (siteId != 0 && InstrumentCategoryId != 0 && ProblemType == 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @siteId = Problem.SiteId AND @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId and @RegionId = Site.Region_Id";
            }

           else if (siteId != 0 && InstrumentCategoryId == 0 && ProblemType != 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @siteId = Problem.SiteId AND @ProblemType = Problem.ProblemTypeId  and @RegionId = Site.Region_Id";
            }

            else if (siteId == 0 && InstrumentCategoryId != 0 && ProblemType != 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE  @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId AND @ProblemType = Problem.ProblemTypeId  and @RegionId = Site.Region_Id";
            }

            else if (siteId == 0 && InstrumentCategoryId == 0 && ProblemType != 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @ProblemType = Problem.ProblemTypeId  and @RegionId = Site.Region_Id";
            }

            else if (siteId == 0 && InstrumentCategoryId != 0 && ProblemType == 0 && regionId != 0)
            {
                sql = "SELECT ProblemType.Name AS ProblemTypeName, Problem.*, InstrumentLookup.InstrumentName AS InstrumentName, Site.SiteName, "
                     + " Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, ErrorCode.Name AS ErrorCodeName "
                     + " FROM Problem LEFT JOIN ProblemType ON Problem.ProblemTypeId = ProblemType.Id "
                     + " LEFT JOIN ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id "
                     + " LEFT JOIN Instrument ON Problem.InstrumentId = Instrument.Id "
                     + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = dbo.InstrumentLookup.Id "
                     + " LEFT JOIN Site ON Site.Id = Problem.SiteId "
                     + " WHERE @InstrumentCategoryId = InstrumentLookup.InstrumentCategoryId and @RegionId = Site.Region_Id";
            }
            IList<Problem> lstProblem = new List<Problem>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (siteId != 0)
                    DatabaseHelper.InsertInt32Param("@siteId", cm, siteId);
                if (InstrumentCategoryId != 0)
                    DatabaseHelper.InsertInt32Param("@InstrumentCategoryId", cm, InstrumentCategoryId);
                if (ProblemType != 0)
                    DatabaseHelper.InsertInt32Param("@ProblemType", cm, ProblemType);
                if (regionId != 0)
                    DatabaseHelper.InsertInt32Param("@RegionId", cm, regionId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstProblem.Add(GetProblem(dr));
                        }
                    }
                }


            }
            return lstProblem;

        }
        private static Problem GetProblem(SqlDataReader dr)
        {
            Problem problem = new Problem();

            problem.Id = DatabaseHelper.GetInt32("Id", dr);
            problem.ProblemNumber = DatabaseHelper.GetString("ProblemNumber", dr);
            problem.Description = DatabaseHelper.GetString("Description", dr);
            problem.InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr);
            problem.SiteId = DatabaseHelper.GetInt32("SiteId", dr);
            problem.UserId = DatabaseHelper.GetInt32("UserId", dr);
            problem.DateReported = DatabaseHelper.GetDateTime("DateReported", dr);
            problem.Status = DatabaseHelper.GetString("Status", dr);
            problem.ProblemtypeId = DatabaseHelper.GetInt32("ProblemtypeId", dr);
            problem.ErrorCodeId = DatabaseHelper.GetInt32("ErrorCodeId", dr);

            problem.ErrorCodeName = DatabaseHelper.GetString("ErrorCodeName", dr);
            problem.ProblemTypeName = DatabaseHelper.GetString("ProblemTypeName", dr);

            problem.InstrumentName = DatabaseHelper.GetString("InstrumentName", dr);
            problem.InstallationDate = DatabaseHelper.GetDateTime("InstallationDate", dr);
            problem.SerialNo = DatabaseHelper.GetString("SerialNo", dr);
            //problem.ManufacturerName = DatabaseHelper.GetString("ManufacturerName", dr);
            problem.SiteName = DatabaseHelper.GetString("SiteName", dr);
            problem.WarranityExpireDate = DatabaseHelper.GetDateTime("WarranityExpireDate", dr);

            return problem;
        }

        private static void SetProblem(SqlCommand cm, Problem problem)
        {

           
            DatabaseHelper.InsertStringVarCharParam("ProblemNumber", cm, problem.ProblemNumber);
            DatabaseHelper.InsertStringVarCharParam("Description", cm, problem.Description);
            DatabaseHelper.InsertInt32Param("InstrumentId", cm, problem.InstrumentId);
            DatabaseHelper.InsertInt32Param("SiteId", cm, problem.SiteId);
            DatabaseHelper.InsertInt32Param("UserId", cm, problem.UserId);
            DatabaseHelper.InsertDateTimeParam("DateReported", cm, problem.DateReported);
            DatabaseHelper.InsertStringVarCharParam("Status", cm, problem.Status);
            DatabaseHelper.InsertInt32Param("ProblemtypeId", cm, problem.ProblemtypeId);
            DatabaseHelper.InsertInt32Param("ErrorCodeId", cm, problem.ErrorCodeId);
        }

        public void Save(Problem problem, bool NotFunctional)
        {
            string sql = "INSERT INTO Problem(ProblemNumber, Description, InstrumentId, SiteId, UserId, DateReported, Status, ProblemTypeId, ErrorCodeId) " +
                         " VALUES (@ProblemNumber, @Description, @InstrumentId, @SiteId, @UserId, @DateReported, @Status, @ProblemTypeId, @ErrorCodeId) " +
                         " SELECT @@identity";
             SqlTransaction tr = DefaultConnection.BeginTransaction();
             try
             {
                 using (SqlCommand cm = new SqlCommand(sql, DefaultConnection,tr))
                 {
                     SetProblem(cm, problem);

                     UpdateAutoNumber("ProblemNotification", (int)AspxPageType.ProblemNotification, problem.ProblemNumber, tr);
                     if (NotFunctional)
                     {
                         UpdateStatus("ProblemNotification", (int)MaintenanceStatus.ProblemNotification, problem.InstrumentId, "", tr);
                         UpdateStatus("ProblemNotificationChangeInstrumentStatus", Convert.ToInt32(false), problem.InstrumentId, "", tr); // passed false bc instrument is not functional
                     }
                     else
                     {
                         UpdateStatus("ProblemNotification", (int)MaintenanceStatus.ProblemNotificationFuntional, problem.InstrumentId, "", tr);
                         UpdateStatus("ProblemNotificationChangeInstrumentStatus", Convert.ToInt32(true), problem.InstrumentId, "", tr);  
                     }
                     problem.Id = int.Parse(cm.ExecuteScalar().ToString());
                     tr.Commit();
                 }
             }
             catch (SqlException ex)
             {
                 tr.Rollback();
             }
        }

        public void Update(Problem problem)
        {
            string sql = "Update Problem SET ProblemNumber=@ProblemNumber, Description=@Description, InstrumentId=@InstrumentId, SiteId=@SiteId, UserId=@UserId, " +
                                  "DateReported=@DateReported, Status=@Status, ProblemTypeId=@ProblemTypeId, ErrorCodeId=@ErrorCodeId where Id =@Id";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@Id", cm, problem.Id);
                SetProblem(cm, problem);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int problemId)
        {
            string sql = "Delete Problem where Id = @problemId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@problemId", cm, problemId);
                cm.ExecuteNonQuery();
            }
        }

        public void UpdateStatus(string updateSentFrom, int status, int Id, string type, SqlTransaction tr)
        {
            string sql = "";
            if (updateSentFrom == "CurativeMaintenance")
                sql = "Update Problem SET Status=@Status where Id =@Id";
            else if (updateSentFrom == "CurativeMaintenanceInstrumentFuncChangeStatus")
            {
                sql = "Update Instrument SET IsFunctional=@Status where Id =@Id";
            }
            else if (updateSentFrom == "CurativeMaintenanceInstrumentFuncConfirmation")
            {
                sql = "Update Instrument SET Status=@Status where Id =@Id";
            }
            else if (updateSentFrom == "PreventiveMaintenanceForSchedule")
            {
                if (status == 8)
                    sql = "Update Schedule SET PreventiveScheduleStatus='Needs Approval' where Id =@Id";
            }
            else if (updateSentFrom == "PreventiveMaintenanceForSchedulePerformed")
            {
                if (status == 1)
                    sql = "Update Schedule SET PreventiveScheduleStatus='Performed' where Id =@Id";
            }
            else if (updateSentFrom == "CurativeMaintenanceForSchedule")
            {
                if (status == 8)
                    sql = "Update Schedule SET CurativeScheduleStatus='Needs Approval' where Id =@Id";
            }
            else if (updateSentFrom == "Transfer")
                sql = "Update Instrument SET Status=@Status where Id =@Id";

            else if (updateSentFrom == "Disposal")
                sql = "Update Instrument SET Status=@Status where Id =@Id";

            else if (updateSentFrom == "ConfirmationForInstrument")
                sql = "Update Instrument SET Status=@Status where Id =@Id";

            else if (updateSentFrom == "ConfirmationForPreventive" && type == "Preventive")
            {
                if (status == 2)
                    sql = "Update Schedule SET PreventiveScheduleStatus='Performed' where Id =@Id";
            }
            else if (updateSentFrom == "ConfirmationForCurative" && type == "Curative")
            {
                if (status == 2)
                    sql = "Update Schedule SET CurativeScheduleStatus='Performed' where Id =@Id";
            }
            else if (updateSentFrom == "ProblemNotification")
            {
                sql = "Update Instrument SET Status=@Status where Id =@Id";

            }
            else if (updateSentFrom == "ProblemNotificationChangeInstrumentStatus")
            {
                sql = "update Instrument SET IsFunctional=@Status where Id = @Id";
            }
            else if (updateSentFrom == "CurativeSchedule")
                sql = "Update Problem SET Status=@Status where Id =@Id";

            else if (updateSentFrom == "EscalateReason")
            {
                if (status == 5)
                    sql = "Update Schedule SET CurativeScheduleStatus='Pushed' where Id =@Id";
            }
            else if (updateSentFrom == "EscalatedSchedule")
            {
                if (status == 9)
                    sql = "Update Schedule SET CurativeScheduleStatus='Escalated' where Id =@Id";
            }
            else if (updateSentFrom == "CurativeReSchedule") //Changes the status of CurativeMaintenance Schedule to Re-Scheduled
            {
                sql = "Update Schedule SET CurativeScheduleStatus='Re-Scheduled' where Id =@Id";
            }
            else if (updateSentFrom == "EscalatedCurative")
                sql = "Update EscalatedJobs SET Status='Scheduled' where Id =@Id";
            else if (updateSentFrom == "Rescheduling")
                sql = "Update Schedule SET CurativeScheduleStatus='Re-Scheduled' where Id =@Id";

            else if (updateSentFrom == "ConfirmationForInstrumentFunctional")
                sql = "Update Instrument SET IsFunctional=@Status where Id = @Id";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
            {
                DatabaseHelper.InsertInt32Param("@Status", cm, status);
                DatabaseHelper.InsertInt32Param("@Id", cm, Id);
                cm.ExecuteNonQuery();
            }
        }

        public void UpdateAutoNumber(string Updatesentfrom, int PageId, string AutoNumber, SqlTransaction tr)
        {
            string sql = "";
            int Sequence = 0;
            string[] auto = AutoNumber.Split('-');
            Sequence = Convert.ToInt32(auto[1].Trim());

            if (Updatesentfrom == "ProblemNotification")
                sql = "Update AutoNumber SET Sequence=@Sequence where PageId =@PageId";
            else if (Updatesentfrom == "Request")
                sql = "Update AutoNumber SET Sequence=@Sequence where PageId =@PageId";
            else if (Updatesentfrom == "Approval")
                sql = "Update AutoNumber SET Sequence=@Sequence where PageId =@PageId";
            else if (Updatesentfrom == "Issue")
                sql = "Update AutoNumber SET Sequence=@Sequence where PageId =@PageId";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
            {
                DatabaseHelper.InsertInt32Param("@PageId", cm, PageId);
                DatabaseHelper.InsertInt32Param("@Sequence", cm, Sequence);
                cm.ExecuteNonQuery();
            }
        }

    }
}
