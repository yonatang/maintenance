﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.DataAccess.Maintenance
{
    public class PreventiveMaintenanceChecklistDao : BaseDao
    {
        public PreventiveMaintenanceChecklist GetPreventiveMaintenanceChecklistById(int checklistId)
        {
            string sql = "SELECT PreventiveMaintenanceChecklist.*, ChecklistTask.TaskName " +
                         "FROM PreventiveMaintenanceChecklist left JOIN" +
                         "ChecklistTask ON PreventiveMaintenanceChecklist.TaskId = ChecklistTask.Id " +

                         "where PreventiveMaintenanceChecklist.Id = @checklistId";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@checklistId", cm, checklistId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetPreventiveMaintenanceChecklist(dr);
                        }
                    }
                }
            }
            return null;
        }

       

        private static PreventiveMaintenanceChecklist GetPreventiveMaintenanceChecklist(SqlDataReader dr)
        {
            PreventiveMaintenanceChecklist checklist = new PreventiveMaintenanceChecklist
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                
                PreventiveMaintenanceId = DatabaseHelper.GetInt32("PreventiveMaintenanceId", dr),
                TaskId = DatabaseHelper.GetInt32("TaskId", dr),
                Remarks = DatabaseHelper.GetString("Remarks", dr),
                IsCompleted = DatabaseHelper.GetBoolean("IsCompleted", dr),

                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                TaskName = DatabaseHelper.GetString("TaskName", dr)
            };
            return checklist;
        }

        private static void SetPreventiveMaintenanceChecklist(SqlCommand cm, PreventiveMaintenanceChecklist checklist)
        {

           
            DatabaseHelper.InsertInt32Param("PreventiveMaintenanceId", cm, checklist.PreventiveMaintenanceId);
            DatabaseHelper.InsertInt32Param("TaskId", cm, checklist.TaskId);

            DatabaseHelper.InsertStringVarCharParam("Remarks", cm, checklist.Remarks);
            DatabaseHelper.InsertBooleanParam("IsCompleted", cm, checklist.IsCompleted);
           
        }

        public void Save(PreventiveMaintenanceChecklist checklist)
        {
            string sql = "INSERT INTO PreventiveMaintenanceChecklist(PreventiveMaintenanceId, TaskId, Remarks, IsCompleted)"
                         + " VALUES (@PreventiveMaintenanceId, @TaskId, @Remarks, @IsCompleted) "
                         + " SELECT @@identity";

            SqlTransaction tr = DefaultConnection.BeginTransaction();
            try
            {
                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                {
                    SetPreventiveMaintenanceChecklist(cm, checklist);
                    checklist.Id = int.Parse(cm.ExecuteScalar().ToString());
                    tr.Commit();
                }
            }
            catch (SqlException ex)
            {
                tr.Rollback();
            }
        }

        public void Update(PreventiveMaintenanceChecklist checklist)
        {
            string sql = "Update PreventiveMaintenanceChecklist SET PreventiveMaintenanceId = @PreventiveMaintenanceId, TaskId = @TaskId, Remarks = @Remarks, IsCompleted = @IsCompleted where Id =@Id";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@Id", cm, checklist.Id);
                SetPreventiveMaintenanceChecklist(cm, checklist);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int checklistId)
        {
            string sql = "Delete PreventiveMaintenanceChecklist where Id = @checklistId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@checklistId", cm, checklistId);
                cm.ExecuteNonQuery();
            }
        }

    }
}