﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.DataAccess.Maintenance
{
    public class ScheduleDao : BaseDao
    {
        public Schedule GetScheduleById(int ScheduleId)
        {
            string sql = " SELECT Schedule.Id, Schedule.ProblemId, Schedule.SiteId, Schedule.InstrumentId, Schedule.Description, Schedule.ScheduledDateFrom, JobPushBackReason.Reason, "
                         + " Schedule.ScheduledDateTo, Schedule.ScheduleForId, Schedule.IsExternal, Schedule.EnginerId, Schedule.Date, Schedule.UserId, "
                         + " AppUser.FirstName, AppUser.LastName, Vendor.VendorName, InstrumentLookup.InstrumentName, Site.SiteName, Schedule.PreventiveScheduleStatus, Schedule.CurativeScheduleStatus "
                         
                         + " FROM Schedule "

                         + " LEFT JOIN Instrument ON Schedule.InstrumentId= Instrument.Id "
                         + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id "
                         + " LEFT JOIN Site ON Site.Id = Schedule.SiteId "
                         + " LEFT JOIN AppUser ON Schedule.EnginerId = AppUser.UserId "
                         + " LEFT JOIN EscalateReason ON EscalateReason.ScheduleId = Schedule.Id "
                         + " LEFT JOIN JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId "
                         + " LEFT JOIN Vendor ON AppUser.VendorId = Vendor.Id WHERE @ScheduleId = Schedule.Id";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@ScheduleId", cm, ScheduleId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetSchedule(dr);
                        }
                    }
                    dr.Close();
                }
            }
            return null;
        }
        public IList<Schedule> GetScheduleBySiteInsCategoryId(int InstrumentCat, string type, int userId)
        {
            string sql = "";
            if (type == "Curative")
            {
                if (InstrumentCat != 0)
                {
                    sql = " SELECT Schedule.Id, Schedule.ProblemId, Schedule.SiteId, Schedule.InstrumentId, Schedule.Description, Schedule.ScheduledDateFrom, JobPushBackReason.Reason, "
                                 + " Schedule.ScheduledDateTo, Schedule.ScheduleForId, Schedule.IsExternal, Schedule.EnginerId, Schedule.Date, Schedule.UserId, Site.SiteName, "
                                 + " AppUser.FirstName, AppUser.LastName, Vendor.VendorName, InstrumentLookup.InstrumentName, Schedule.PreventiveScheduleStatus, Schedule.CurativeScheduleStatus "

                                 + " FROM Schedule "

                                 + " LEFT JOIN Instrument ON Schedule.InstrumentId= Instrument.Id "
                                 + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id "
                                 + " LEFT JOIN InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id "
                                 + " LEFT JOIN Site ON Site.Id = Schedule.SiteId "
                                 + " LEFT JOIN AppUser ON Schedule.EnginerId = AppUser.UserId "
                                 + " LEFT JOIN EscalateReason ON EscalateReason.ScheduleId = Schedule.Id "
                                 + " LEFT JOIN JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId "
                                 + " LEFT JOIN Vendor ON AppUser.VendorId = Vendor.Id WHERE @userId = Schedule.UserId AND InstrumentCatagory.Id = @InstrumentCat AND Schedule.ProblemId != 0";
                }
                else if (InstrumentCat == 0)
                {
                    sql = " SELECT Schedule.Id, Schedule.ProblemId, Schedule.SiteId, Schedule.InstrumentId, Schedule.Description, Schedule.ScheduledDateFrom, JobPushBackReason.Reason, "
                                + " Schedule.ScheduledDateTo, Schedule.ScheduleForId, Schedule.IsExternal, Schedule.EnginerId, Schedule.Date, Schedule.UserId, Site.SiteName,  "
                                + " AppUser.FirstName, AppUser.LastName, Vendor.VendorName, InstrumentLookup.InstrumentName, Schedule.PreventiveScheduleStatus, Schedule.CurativeScheduleStatus "

                                + " FROM Schedule "

                                + " LEFT JOIN Instrument ON Schedule.InstrumentId= Instrument.Id "
                                + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id "
                                + " LEFT JOIN InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id "
                                + " LEFT JOIN Site ON Site.Id = Schedule.SiteId "
                                + " LEFT JOIN AppUser ON Schedule.EnginerId = AppUser.UserId "
                                + " LEFT JOIN EscalateReason ON EscalateReason.ScheduleId = Schedule.Id "
                                + " LEFT JOIN JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId "
                                + " LEFT JOIN Vendor ON AppUser.VendorId = Vendor.Id WHERE Schedule.UserId = @userId AND Schedule.ProblemId != 0";
                }
                
            }
            else if (type == "Preventive")//////////////////////////////////////Preventive////////////////////////////////////////////
            {
                if (InstrumentCat != 0)
                {
                    sql = " SELECT Schedule.Id, Schedule.ProblemId, Schedule.SiteId, Schedule.InstrumentId, Schedule.Description, Schedule.ScheduledDateFrom, JobPushBackReason.Reason,  "
                                 + " Schedule.ScheduledDateTo, Schedule.ScheduleForId, Schedule.IsExternal, Schedule.EnginerId, Schedule.Date, Schedule.UserId, Site.SiteName,  "
                                 + " AppUser.FirstName, AppUser.LastName, Vendor.VendorName, InstrumentLookup.InstrumentName, Schedule.PreventiveScheduleStatus, Schedule.CurativeScheduleStatus "

                                 + " FROM Schedule "

                                 + " LEFT JOIN Instrument ON Schedule.InstrumentId= Instrument.Id "
                                 + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id "
                                 + " LEFT JOIN InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id "
                                 + " LEFT JOIN Site ON Site.Id = Schedule.SiteId "
                                 + " LEFT JOIN AppUser ON Schedule.EnginerId = AppUser.UserId "
                                    + " LEFT JOIN EscalateReason ON EscalateReason.ScheduleId = Schedule.Id "
                                     + " LEFT JOIN JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId "
                                 + " LEFT JOIN Vendor ON AppUser.VendorId = Vendor.Id WHERE @userId = Schedule.UserId AND InstrumentCatagory.Id = @InstrumentCat AND Schedule.ProblemId = 0";
                }
                else if(InstrumentCat == 0)
                {
                    sql = " SELECT Schedule.Id, Schedule.ProblemId, Schedule.SiteId, Schedule.InstrumentId, Schedule.Description, Schedule.ScheduledDateFrom, JobPushBackReason.Reason,   "
                                + " Schedule.ScheduledDateTo, Schedule.ScheduleForId, Schedule.IsExternal, Schedule.EnginerId, Schedule.Date, Schedule.UserId, Site.SiteName,  "
                                + " AppUser.FirstName, AppUser.LastName, Vendor.VendorName, InstrumentLookup.InstrumentName, Schedule.PreventiveScheduleStatus, Schedule.CurativeScheduleStatus "

                                + " FROM Schedule "

                                + " LEFT JOIN Instrument ON Schedule.InstrumentId= Instrument.Id "
                                + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id "
                                + " LEFT JOIN InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id "
                                + " LEFT JOIN Site ON Site.Id = Schedule.SiteId "
                                + " LEFT JOIN AppUser ON Schedule.EnginerId = AppUser.UserId "
                                      + " LEFT JOIN EscalateReason ON EscalateReason.ScheduleId = Schedule.Id "
                                + " LEFT JOIN JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId "
                                + " LEFT JOIN Vendor ON AppUser.VendorId = Vendor.Id WHERE @userId = Schedule.UserId AND Schedule.ProblemId = 0";
                }
               
            }
            IList<Schedule> lstSchedule = new List<Schedule>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {           
                if (InstrumentCat != 0)
                    DatabaseHelper.InsertInt32Param("@InstrumentCat", cm, InstrumentCat);
                if(userId != 0)
                    DatabaseHelper.InsertInt32Param("@userId", cm, userId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstSchedule.Add(GetSchedule(dr));
                        }
                    }
                }
            }
            return lstSchedule;
        }

        public IList<Schedule> GetSchedulesByEnginerId(int enginerId, string maintenanceType)
        {
            string sql = "";
            if (maintenanceType == "Curative")
            {
                sql = " SELECT Schedule.Id, Schedule.ProblemId, Schedule.SiteId, Schedule.InstrumentId, Schedule.Description, Schedule.ScheduledDateFrom, JobPushBackReason.Reason, "
                            + " Schedule.ScheduledDateTo, Schedule.ScheduleForId, Schedule.IsExternal, Schedule.EnginerId, Schedule.Date, Schedule.UserId, Site.SiteName,  "
                            + " AppUser.FirstName, AppUser.LastName, Vendor.VendorName, InstrumentLookup.InstrumentName, Schedule.PreventiveScheduleStatus, Schedule.CurativeScheduleStatus "

                            + " FROM Schedule "

                            + " LEFT JOIN Instrument ON Schedule.InstrumentId= Instrument.Id "
                            + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id "
                            + " LEFT JOIN InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id "
                            + " LEFT JOIN Site ON Site.Id = Schedule.SiteId "
                            + " LEFT JOIN AppUser ON Schedule.EnginerId = AppUser.UserId "
                            + " LEFT JOIN EscalateReason ON EscalateReason.ScheduleId = Schedule.Id "
                            + " LEFT JOIN JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId "
                            + " LEFT JOIN Vendor ON AppUser.VendorId = Vendor.Id WHERE Schedule.EnginerId = @enginerId AND  Schedule.CurativeScheduleStatus = 'Not Performed'";
            }
            else if (maintenanceType == "Preventive")
            {
            sql = " SELECT Schedule.Id, Schedule.ProblemId, Schedule.SiteId, Schedule.InstrumentId, Schedule.Description, Schedule.ScheduledDateFrom,  JobPushBackReason.Reason, "
                             + " Schedule.ScheduledDateTo, Schedule.ScheduleForId, Schedule.IsExternal, Schedule.EnginerId, Schedule.Date, Schedule.UserId, Site.SiteName,  "
                             + " AppUser.FirstName, AppUser.LastName, Vendor.VendorName, InstrumentLookup.InstrumentName, Schedule.PreventiveScheduleStatus, Schedule.CurativeScheduleStatus "

                             + " FROM Schedule "

                             + " LEFT JOIN Instrument ON Schedule.InstrumentId= Instrument.Id "
                             + " LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id "
                             + " LEFT JOIN InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id "
                             + " LEFT JOIN Site ON Site.Id = Schedule.SiteId "
                             + " LEFT JOIN AppUser ON Schedule.EnginerId = AppUser.UserId "
                             + " LEFT JOIN EscalateReason ON EscalateReason.ScheduleId = Schedule.Id "
                             + " LEFT JOIN JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId "
                             + " LEFT JOIN Vendor ON AppUser.VendorId = Vendor.Id WHERE @enginerId = Schedule.EnginerId AND Schedule.PreventiveScheduleStatus = 'Not Performed' ";     
            }
            IList<Schedule> lstSchedule = new List<Schedule>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
              
                if(enginerId != 0)
                    DatabaseHelper.InsertInt32Param("@enginerId", cm, enginerId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstSchedule.Add(GetSchedule(dr));
                        }
                    }
                }
            }
            return lstSchedule;
        }
      
        private static Schedule GetSchedule(SqlDataReader dr)
        {
            Schedule _Schedule = new Schedule();
            _Schedule.Id = DatabaseHelper.GetInt32("Id", dr);
            _Schedule.Description = DatabaseHelper.GetString("Description", dr);
            _Schedule.InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr);
            _Schedule.SiteId = DatabaseHelper.GetInt32("SiteId", dr);
            _Schedule.ScheduledDateFrom = DatabaseHelper.GetDateTime("ScheduledDateFrom", dr);
            _Schedule.ScheduledDateTo = DatabaseHelper.GetDateTime("ScheduledDateTo", dr);
            _Schedule.ScheduleForId = DatabaseHelper.GetInt32("ScheduleForId", dr);
            _Schedule.ProblemId = DatabaseHelper.GetInt32("ProblemId", dr);
            _Schedule.IsExternal = DatabaseHelper.GetBoolean("IsExternal", dr);
            _Schedule.Date = DatabaseHelper.GetDateTime("Date", dr);
            _Schedule.EnginerId = DatabaseHelper.GetInt32("EnginerId", dr);
            _Schedule.FirstName = DatabaseHelper.GetString("FirstName", dr);
            _Schedule.LastName = DatabaseHelper.GetString("LastName", dr);
            _Schedule.VendorName = DatabaseHelper.GetString("VendorName", dr);
            _Schedule.UserId = DatabaseHelper.GetInt32("UserId", dr);
            _Schedule.PreventiveScheduleStatus = DatabaseHelper.GetString("PreventiveScheduleStatus", dr);
            _Schedule.CurativeScheduleStatus = DatabaseHelper.GetString("CurativeScheduleStatus", dr);
            _Schedule.InstrumentName = DatabaseHelper.GetString("InstrumentName", dr);
            _Schedule.SiteName = DatabaseHelper.GetString("SiteName", dr);
            _Schedule.PushedReason = DatabaseHelper.GetString("Reason", dr);
            return _Schedule;
        }

        private static void SetSchedule(SqlCommand cm, Schedule _Schedule)
        {
            DatabaseHelper.InsertStringVarCharParam("Description", cm, _Schedule.Description);
            DatabaseHelper.InsertInt32Param("InstrumentId", cm, _Schedule.InstrumentId);
            DatabaseHelper.InsertInt32Param("SiteId", cm, _Schedule.SiteId);
            DatabaseHelper.InsertDateTimeParam("ScheduledDateFrom", cm, _Schedule.ScheduledDateFrom);
            DatabaseHelper.InsertDateTimeParam("ScheduledDateTo", cm, _Schedule.ScheduledDateTo);
            DatabaseHelper.InsertInt32Param("ScheduleForId", cm, _Schedule.ScheduleForId);
            DatabaseHelper.InsertInt32Param("ProblemId", cm, _Schedule.ProblemId);
            DatabaseHelper.InsertBooleanParam("IsExternal", cm, _Schedule.IsExternal);
            DatabaseHelper.InsertDateTimeParam("Date", cm, _Schedule.Date);
            DatabaseHelper.InsertInt32Param("EnginerId", cm, _Schedule.EnginerId);

            DatabaseHelper.InsertStringVarCharParam("FirstName", cm, _Schedule.FirstName);
            DatabaseHelper.InsertStringVarCharParam("LastName", cm, _Schedule.LastName);
            DatabaseHelper.InsertStringVarCharParam("VendorName", cm, _Schedule.VendorName);
            DatabaseHelper.InsertInt32Param("UserId", cm, _Schedule.UserId);
            DatabaseHelper.InsertStringVarCharParam("CurativeScheduleStatus", cm, _Schedule.CurativeScheduleStatus);
            DatabaseHelper.InsertStringVarCharParam("PreventiveScheduleStatus", cm, _Schedule.PreventiveScheduleStatus);
            DatabaseHelper.InsertStringVarCharParam("Reason", cm, _Schedule.PushedReason);
        }

        public void Save(Schedule _Schedule, string type, int escalateId)
        {
            string sql = "INSERT INTO Schedule(ProblemId, SiteId, InstrumentId, Description, ScheduledDateFrom, ScheduledDateTo, ScheduleForId, IsExternal, EnginerId, Date, UserId, CurativeScheduleStatus, PreventiveScheduleStatus)"
                         + " VALUES (@ProblemId, @SiteId, @InstrumentId, @Description, @ScheduledDateFrom, @ScheduledDateTo, @ScheduleForId, @IsExternal, @EnginerId, @Date, @UserId,  @CurativeScheduleStatus, @PreventiveScheduleStatus) "
                         + " SELECT @@identity";
            SqlTransaction tr = DefaultConnection.BeginTransaction();
            ProblemDao _problem = new ProblemDao();
            try
            {
                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                {
                    SetSchedule(cm, _Schedule);
                    if (type == "Curative")
                        _problem.UpdateStatus("CurativeSchedule", (int)MaintenanceStatus.CurativeSchedule, _Schedule.ProblemId, "", tr);
                    else if(type == "EscalatedCurative")
                        _problem.UpdateStatus("EscalatedCurative", 0, escalateId, "", tr);
                    else if (type == "Rescheduling")
                        _problem.UpdateStatus("Rescheduling", 0, escalateId, "", tr);
                    _Schedule.Id = int.Parse(cm.ExecuteScalar().ToString());
                    tr.Commit();
                }
            }
            catch(SqlException ex)
            {
                tr.Rollback();
            }
        }

        public void Update(Schedule _Schedule)
        {
            string sql = "Update Schedule SET ProblemId=@ProblemId, SiteId=@SiteId, InstrumentId=@InstrumentId, Description=@Description, ScheduledDateFrom=@ScheduledDateFrom, UserId=@UserId, "
                                 + "ScheduledDateTo=@ScheduledDateTo, ScheduleForId=@ScheduleForId, IsExternal=@IsExternal, EnginerId=@EnginerId, Date=@Date,  CurativeScheduleStatus=@CurativeScheduleStatus, PreventiveScheduleStatus=@PreventiveScheduleStatus "
                                  +" where Id =@Id";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@Id", cm, _Schedule.Id);
                SetSchedule(cm, _Schedule);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int ScheduleId)
        {
            string sql = "Delete Schedule where Id = @ScheduleId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@ScheduleId", cm, ScheduleId);
                cm.ExecuteNonQuery();
            }
        }


    }
}
