﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.DataAccess.Maintenance
{
    public class EscalateReasonDao : BaseDao
    {
        public EscalateReason GetEscalateReasonById(int escalateId)
        {
            string sql = "SELECT EscalateReason.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, Problem.ProblemNumber, JobPushBackReason.Reason " +
                        "FROM EscalateReason left JOIN" +

                        "Instrument ON EscalateReason.InstrumentId = Instrument.Id left JOIN " +
                        "Problem ON Problem.Id = EscalateReason.ProblemId LEFT JOIN" +
                        "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN  " +
                        "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN  " +
                        "Site ON Instrument.Site_Id = Site.Id LEFT JOIN " +
                        "AppUser ON AppUser.SiteId = Site.Id LEFT JOIN" +
                        "JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId " +
                        "where EscalateReason.Id = @escalateId";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@escalateId", cm, escalateId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetEscalateReason(dr);
                        }
                    }
                }
            }
            return null;
        }
        public EscalateReason GetEscalateReasonByProblemIdANDScheduleId(int problemId, int scheduleId)
        {
            string sql = "SELECT TOP (1)EscalateReason.Id, EscalateReason.*, JobPushBackReason.Reason, " +
                         "Site.SiteName, InstrumentLookup.InstrumentName, Problem.ProblemNumber " +
                         "FROM EscalateReason " +
                         "LEFT JOIN Instrument ON EscalateReason.InstrumentId = Instrument.Id " +
                         "LEFT JOIN Problem ON Problem.Id = EscalateReason.ProblemId " +
                         "LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id " +
                         "LEFT JOIN InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                         "LEFT JOIN Site ON Instrument.Site_Id = Site.Id " +
                         "LEFT JOIN AppUser ON AppUser.SiteId = Site.Id " +
                         "LEFT JOIN JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId " +
                         "where EscalateReason.ProblemId = @problemId AND EscalateReason.ScheduleId = @scheduleId ORDER BY EscalateReason.Id DESC";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@problemId", cm, problemId);
                DatabaseHelper.InsertInt32Param("@scheduleId", cm, scheduleId);


                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetEscalateReason(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static EscalateReason GetEscalateReason(SqlDataReader dr)
        {
            EscalateReason escalateReason = new EscalateReason()
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr),
                ScheduleId = DatabaseHelper.GetInt32("ScheduleId", dr),
                ProblemId = DatabaseHelper.GetInt32("ProblemId", dr),
                EnginerId = DatabaseHelper.GetInt32("EnginerId", dr),

                ReasonId = DatabaseHelper.GetInt32("ReasonId", dr),
                Date = DatabaseHelper.GetDateTime("Date", dr),
                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                SiteName = DatabaseHelper.GetString("SiteName", dr),
                ProblemNumber = DatabaseHelper.GetString("ProblemNumber", dr),

                Reason = DatabaseHelper.GetString("Reason", dr)
            };


            return escalateReason;
        }

        private static void SetEscalateReason(SqlCommand cm, EscalateReason escalateReason)
        {

            DatabaseHelper.InsertInt32Param("InstrumentId", cm, escalateReason.InstrumentId);
            DatabaseHelper.InsertInt32Param("ProblemId", cm, escalateReason.ProblemId);
         
            DatabaseHelper.InsertInt32Param("ScheduleId", cm, escalateReason.ScheduleId);
            DatabaseHelper.InsertInt32Param("EnginerId", cm, escalateReason.EnginerId);
            DatabaseHelper.InsertInt32Param("ReasonId", cm, escalateReason.ReasonId);
            DatabaseHelper.InsertDateTimeParam("Date", cm, escalateReason.Date);
           
        }

        public void Save(EscalateReason escalateReason)
        {
            string sql = "INSERT INTO EscalateReason(InstrumentId, ProblemId, ScheduleId, EnginerId, ReasonId, Date)"
                         + " VALUES (@InstrumentId, @ProblemId, @ScheduleId, @EnginerId, @ReasonId, @Date) "
                         + " SELECT @@identity";

           ProblemDao problem = new ProblemDao();
            SqlTransaction tr = DefaultConnection.BeginTransaction();
            try
            {
                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                {
                    SetEscalateReason(cm, escalateReason);
                    problem.UpdateStatus("EscalateReason", (int)MaintenanceStatus.PushJobSchedule, escalateReason.ScheduleId, "", tr);
             
                    escalateReason.Id = int.Parse(cm.ExecuteScalar().ToString());
                    tr.Commit();
                }
            }
            catch (SqlException ex)
            {
                tr.Rollback();
            }
        }

        public void Update(EscalateReason escalateReason)
        {
            string sql = "Update EscalateReason SET InstrumentId = @InstrumentId, ProblemId = @ProblemId, ScheduleId = @ScheduleId, EnginerId = @EnginerId, ReasonId = @ReasonId, Date = @Date where Id =@Id";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@Id", cm, escalateReason.Id);
                SetEscalateReason(cm, escalateReason);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int escalateId)
        {
            string sql = "Delete EscalateReason where Id = @escalateId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@escalateId", cm, escalateId);
                cm.ExecuteNonQuery();
            }
        }

        public IList<EscalateReason> GetEscalateReasonBySiteInsCategoryId(int SiteId, int InstrumentCat, int enginerId)
        {
            string sql = "";
            if (SiteId != 0 && InstrumentCat != 0 && enginerId != 0)
            {
                sql = "SELECT EscalateReason.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, Problem.ProblemNumber, JobPushBackReason.Reason " +
                         "FROM EscalateReason left JOIN" +

                         "Instrument ON EscalateReason.InstrumentId = Instrument.Id left JOIN " +
                         "Problem ON Problem.Id = EscalateReason.ProblemId LEFT JOIN" +
                         "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN  " +
                         "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN  " +
                         "Site ON Instrument.Site_Id = Site.Id LEFT JOIN " +
                         "JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId LEFT JOIN" +
                         "AppUser ON AppUser.SiteId = Site.Id" +

                        "WHERE InstrumentCatagory.Id = @InstrumentCat AND EscalateReason.SiteId = @SiteId AND EscalateReason.EnginerId = @enginerId";
            }

            else if (SiteId != 0 && InstrumentCat == 0 && enginerId == 0)
            {
                sql = "SELECT EscalateReason.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, Problem.ProblemNumber, JobPushBackReason.Reason " +
                          "FROM EscalateReason left JOIN" +

                          "Instrument ON EscalateReason.InstrumentId = Instrument.Id left JOIN " +
                          "Problem ON Problem.Id = EscalateReason.ProblemId LEFT JOIN" +
                          "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN  " +
                          "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN  " +
                          "Site ON Instrument.Site_Id = Site.Id LEFT JOIN " +
                           "JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId LEFT JOIN" +
                          "AppUser ON AppUser.SiteId = Site.Id" +

                          "WHERE EscalateReason.SiteId = @SiteId ";
            }
            else if (SiteId == 0 && InstrumentCat == 0 && enginerId == 0)
            {
                sql = "SELECT EscalateReason.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, Problem.ProblemNumber, JobPushBackReason.Reason " +
                           "FROM EscalateReason left JOIN" +

                           "Instrument ON EscalateReason.InstrumentId = Instrument.Id left JOIN " +
                           "Problem ON Problem.Id = EscalateReason.ProblemId LEFT JOIN" +
                           "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN  " +
                           "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN  " +
                           "Site ON Instrument.Site_Id = Site.Id LEFT JOIN " +
                            "JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId LEFT JOIN" +
                           "AppUser ON AppUser.SiteId = Site.Id";
            }
            else if (SiteId == 0 && InstrumentCat != 0 && enginerId != 0)
            {
                sql = "SELECT EscalateReason.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, Problem.ProblemNumber, JobPushBackReason.Reason " +
                        "FROM EscalateReason left JOIN" +

                        "Instrument ON EscalateReason.InstrumentId = Instrument.Id left JOIN " +
                        "Problem ON Problem.Id = EscalateReason.ProblemId LEFT JOIN" +
                        "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN  " +
                        "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN  " +
                        "Site ON Instrument.Site_Id = Site.Id LEFT JOIN " +
                         "JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId LEFT JOIN" +
                        "AppUser ON AppUser.SiteId = Site.Id" +
                        "WHERE InstrumentCatagory.Id = @InstrumentCat AND EscalateReason.EnginerId = @enginerId";
            }

            else if (SiteId == 0 && InstrumentCat == 0 && enginerId != 0)
            {
                sql = "SELECT EscalateReason.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, Problem.ProblemNumber, JobPushBackReason.Reason " +
                         "FROM EscalateReason left JOIN" +

                         "Instrument ON EscalateReason.InstrumentId = Instrument.Id left JOIN " +
                         "Problem ON Problem.Id = EscalateReason.ProblemId LEFT JOIN" +
                         "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN  " +
                         "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN  " +
                         "Site ON Instrument.Site_Id = Site.Id LEFT JOIN " +
                          "JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId LEFT JOIN" +
                         "AppUser ON AppUser.SiteId = Site.Id " +
                        "WHERE EscalateReason.EnginerId = @enginerId";
            }
            else if (SiteId != 0 && InstrumentCat == 0 && enginerId != 0)
            {
                sql = "SELECT EscalateReason.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, Problem.ProblemNumber, JobPushBackReason.Reason " +
                         "FROM EscalateReason left JOIN" +

                         "Instrument ON EscalateReason.InstrumentId = Instrument.Id left JOIN " +
                         "Problem ON Problem.Id = EscalateReason.ProblemId LEFT JOIN" +
                         "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN  " +
                         "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN  " +
                         "Site ON Instrument.Site_Id = Site.Id LEFT JOIN " +
                          "JobPushBackReason ON JobPushBackReason.Id = EscalateReason.ReasonId LEFT JOIN" +
                         "AppUser ON AppUser.SiteId = Site.Id" +
                        "WHERE  EscalateReason.SiteId = @SiteId AND EscalateReason.EnginerId = @enginerId";
            }
            IList<EscalateReason> lstEscalate = new List<EscalateReason>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (SiteId != 0)
                    DatabaseHelper.InsertInt32Param("@SiteId", cm, SiteId);
                if (InstrumentCat != 0)
                    DatabaseHelper.InsertInt32Param("@InstrumentCat", cm, InstrumentCat);
                if (enginerId != 0)
                    DatabaseHelper.InsertInt32Param("@enginerId", cm, enginerId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstEscalate.Add(GetEscalateReason(dr));
                        }
                    }
                }


            }
            return lstEscalate;
        }

    }
}