﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.DataAccess.Maintenance
{
    public class CurativeMaintenanceDao : BaseDao
    {
        public CurativeMaintenance GetCurativeMaintenanceById(int CurativeMaintenanceId)
        {
            string sql = "SELECT CurativeMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, ProblemType.Name AS ProblemType" +
                         " FROM CurativeMaintenance LEFT JOIN" +
                         " Problem ON Problem.Id = CurativeMaintenance.ProblemId LEFT JOIN " +
                         " ProblemType ON Problem.ProblemTypeId = ProblemType.Id LEFT JOIN " +
                         " Schedule ON CurativeMaintenance.ScheduleId = Schedule.Id left JOIN " +
                         " Site ON Schedule.SiteId = Site.Id left JOIN  " +
                         " Instrument ON CurativeMaintenance.InstrumentId = Instrument.Id left JOIN " +
                         " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id " +
                         " where CurativeMaintenance.Id = @CurativeMaintenanceId" +
   
                       " SELECT Spareparts.*,SparepartType.Name AS SparePartName FROM Spareparts" +
                       " LEFT JOIN SparepartType on Spareparts.SparepartId = SparepartType.Id WHERE Spareparts.CurativeMaintenanceId = @CurativeMaintenanceId";
                      

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@CurativeMaintenanceId", cm, CurativeMaintenanceId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetCurativeMaintenance(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static CurativeMaintenance GetCurativeMaintenancelist(SqlDataReader dr)
        {
            CurativeMaintenance curative = new CurativeMaintenance
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                DescriptionOfEqptFailure = DatabaseHelper.GetString("DescriptionOfEqptFailure", dr),
                FailureCode = DatabaseHelper.GetString("FailureCode", dr),
                CauseOfEqptFailure = DatabaseHelper.GetString("CauseOfEqptFailure", dr),
                MaintainedParts = DatabaseHelper.GetString("MaintainedParts", dr),
                CurativeActionTaken = DatabaseHelper.GetString("CurativeActionTaken", dr),
                PerformanceTestsDone = DatabaseHelper.GetBoolean("PerformanceTestsDone", dr),
                EquipmentFullyFunctional = DatabaseHelper.GetBoolean("EquipmentFullyFunctional", dr),
                EngineerComments = DatabaseHelper.GetString("EngineerComments", dr),
                FollowUpRequired = DatabaseHelper.GetBoolean("FollowUpRequired", dr),

                VisitCompletionDate = DatabaseHelper.GetDateTime("VisitCompletionDate", dr),
                InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr),
                ProblemId = DatabaseHelper.GetInt32("ProblemId", dr),
                Date = DatabaseHelper.GetDateTime("Date", dr),
                EnginerId = DatabaseHelper.GetInt32("EnginerId", dr),
                Scheduleid = DatabaseHelper.GetInt32("ScheduleId", dr),
                SiteName = DatabaseHelper.GetString("SiteName", dr),
                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                ProblemType = DatabaseHelper.GetString("ProblemType", dr)
            };
            return curative;
        }

        private static CurativeMaintenance GetCurativeMaintenance(SqlDataReader dr)
        {
            CurativeMaintenance curative = new CurativeMaintenance
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                DescriptionOfEqptFailure = DatabaseHelper.GetString("DescriptionOfEqptFailure", dr),
                FailureCode = DatabaseHelper.GetString("FailureCode", dr),
                CauseOfEqptFailure = DatabaseHelper.GetString("CauseOfEqptFailure", dr),
                MaintainedParts = DatabaseHelper.GetString("MaintainedParts", dr),
                CurativeActionTaken = DatabaseHelper.GetString("CurativeActionTaken", dr),
                PerformanceTestsDone = DatabaseHelper.GetBoolean("PerformanceTestsDone", dr),
                EquipmentFullyFunctional = DatabaseHelper.GetBoolean("EquipmentFullyFunctional", dr),
                EngineerComments = DatabaseHelper.GetString("EngineerComments", dr),
                FollowUpRequired = DatabaseHelper.GetBoolean("FollowUpRequired", dr),
                VisitCompletionDate = DatabaseHelper.GetDateTime("VisitCompletionDate", dr),
                InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr),
                ProblemId = DatabaseHelper.GetInt32("ProblemId", dr),
                Date = DatabaseHelper.GetDateTime("Date", dr),
                EnginerId = DatabaseHelper.GetInt32("EnginerId", dr),
                Scheduleid = DatabaseHelper.GetInt32("ScheduleId", dr),
                SiteName = DatabaseHelper.GetString("SiteName", dr),
                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                ProblemType = DatabaseHelper.GetString("ProblemType", dr)
            };
            dr.NextResult();
            while (dr.Read())
            {
                Spareparts spares = new Spareparts()
                {
                    Id = DatabaseHelper.GetInt32("Id", dr),
                    CurativeMaintenanceId = DatabaseHelper.GetInt32("CurativeMaintenanceId", dr),
                    PreventiveMaintenanceId = DatabaseHelper.GetInt32("PreventiveMaintenanceId", dr),
                    SparepartId = DatabaseHelper.GetInt32("SparepartId", dr),
                    Quantity = DatabaseHelper.GetInt32("Quantity", dr),
                    SparePartName = DatabaseHelper.GetString("SparePartName", dr)
                };
                curative.Sapres.Add(spares);
            }

            return curative;
        }

        private static void SetCurativeMaintenance(SqlCommand cm, CurativeMaintenance curative)
        {
            DatabaseHelper.InsertStringVarCharParam("DescriptionOfEqptFailure", cm, curative.DescriptionOfEqptFailure);
            DatabaseHelper.InsertStringVarCharParam("FailureCode", cm, curative.FailureCode);
            DatabaseHelper.InsertStringVarCharParam("CauseOfEqptFailure", cm, curative.CauseOfEqptFailure);
            DatabaseHelper.InsertStringVarCharParam("MaintainedParts", cm, curative.MaintainedParts);
            DatabaseHelper.InsertStringVarCharParam("CurativeActionTaken", cm, curative.CurativeActionTaken);
            DatabaseHelper.InsertBooleanParam("PerformanceTestsDone", cm, curative.PerformanceTestsDone);
            DatabaseHelper.InsertBooleanParam("EquipmentFullyFunctional", cm, curative.EquipmentFullyFunctional);
            DatabaseHelper.InsertStringVarCharParam("EngineerComments", cm, curative.EngineerComments);
            DatabaseHelper.InsertBooleanParam("FollowUpRequired", cm, curative.FollowUpRequired);
            DatabaseHelper.InsertDateTimeParam("VisitCompletionDate", cm, curative.VisitCompletionDate);
            DatabaseHelper.InsertInt32Param("InstrumentId", cm, curative.InstrumentId);
            DatabaseHelper.InsertInt32Param("ProblemId", cm, curative.ProblemId);
            DatabaseHelper.InsertDateTimeParam("Date", cm, curative.Date);
            DatabaseHelper.InsertInt32Param("EnginerId", cm, curative.EnginerId);
            DatabaseHelper.InsertInt32Param("ScheduleId", cm, curative.Scheduleid);
        }

        public void Save(CurativeMaintenance curative)
        {
            string sql = "INSERT INTO CurativeMaintenance(DescriptionOfEqptFailure, FailureCode, CauseOfEqptFailure, MaintainedParts, CurativeActionTaken, PerformanceTestsDone, EquipmentFullyFunctional, EngineerComments, FollowUpRequired, VisitCompletionDate, InstrumentId, ProblemId, Date, EnginerId,  ScheduleId) "
                        + " VALUES (@DescriptionOfEqptFailure, @FailureCode, @CauseOfEqptFailure, @MaintainedParts, @CurativeActionTaken, @PerformanceTestsDone, @EquipmentFullyFunctional, @EngineerComments, @FollowUpRequired, @VisitCompletionDate, @InstrumentId, @ProblemId, @Date, @EnginerId, @ScheduleId) SELECT @@identity";
            ProblemDao _problem = new ProblemDao();
            SqlTransaction tr = DefaultConnection.BeginTransaction();
            try
            {
                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                {
                    SetCurativeMaintenance(cm, curative);
                    if (curative.EquipmentFullyFunctional == true)
                    {
                        _problem.UpdateStatus("CurativeMaintenance", (int)MaintenanceStatus.CurativeMaintenanceFunctionalCheckedStatus, curative.ProblemId, "", tr);
                        _problem.UpdateStatus("CurativeMaintenanceInstrumentFuncConfirmation", (int)MaintenanceStatus.ConfirmationForFuncInstrumentFromCurativeMain, curative.InstrumentId, "", tr);
                        _problem.UpdateStatus("CurativeMaintenanceInstrumentFuncChangeStatus", Convert.ToInt32(true), curative.InstrumentId, "", tr);

                    }
                    else
                    {
                        _problem.UpdateStatus("CurativeMaintenance", (int)MaintenanceStatus.CurativeMaintenanceFunctionalUNCheckedStatus, curative.ProblemId, "", tr);
                        _problem.UpdateStatus("CurativeMaintenanceInstrumentFuncConfirmation", (int)MaintenanceStatus.ConfirmationForNotFuncInstrumentFromCurativeMain, curative.InstrumentId, "", tr);
                        _problem.UpdateStatus("CurativeMaintenanceInstrumentFuncChangeStatus", Convert.ToInt32(false), curative.InstrumentId, "", tr);
                    }

                    _problem.UpdateStatus("ConfirmationForCurative", (int)MaintenanceStatus.ConfirmationForCurative, curative.Scheduleid, "Curative", tr);
                    curative.Id = int.Parse(cm.ExecuteScalar().ToString());
                    tr.Commit();
                }
            }
            catch (Exception ex)
            {
                tr.Rollback();
            }

        }

        public void Update(CurativeMaintenance curative)
        {
            string sql = "Update CurativeMaintenance SET DescriptionOfEqptFailure =@DescriptionOfEqptFailure, FailureCode=@FailureCode, CauseOfEqptFailure=@CauseOfEqptFailure, MaintainedParts=@MaintainedParts, CurativeActionTaken=@CurativeActionTaken, PerformanceTestsDone=@PerformanceTestsDone, EquipmentFullyFunctional=@EquipmentFullyFunctional, EngineerComments=@EngineerComments, FollowUpRequired=@FollowUpRequired, VisitCompletionDate=@VisitCompletionDate, InstrumentId=@InstrumentId, ProblemId=@ProblemId, Date=@Date, EnginerId=@EnginerId, ScheduleId=@ScheduleId  where Id = @curativeid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@curativeid", cm, curative.Id);
                SetCurativeMaintenance(cm, curative);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int curativeId)
        {
            string sql = "Delete CurativeMaintenance where Id = @CurativeID";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@CurativeID", cm, curativeId);
                cm.ExecuteNonQuery();
            }
        }

        public IList<CurativeMaintenance> GetListOfCurativeMaintenances(string problemNumber, int InstrumentID)
        {
            string sql;
            if (InstrumentID != 0 && problemNumber == null)
                sql = "SELECT CurativeMaintenance.* FROM CurativeMaintenance WHERE @InstrumentID=CurativeMaintenance.InstrumentID";
            else if (InstrumentID == 0 && problemNumber != null)
                sql = "SELECT CurativeMaintenance.* FROM CurativeMaintenance where @problemNumber = Problem.ProblemNumber " +
                      "LEFT JOIN Problem ON Problem.Id = CurativeMaintenance.ProblemId";
            else
                sql = "SELECT CurativeMaintenance.* FROM CurativeMaintenance";
            IList<CurativeMaintenance> lstCurative = new List<CurativeMaintenance>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertStringVarCharParam("@problemNumber", cm, problemNumber);
                DatabaseHelper.InsertInt32Param("@InstrumentID", cm, InstrumentID);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstCurative.Add(GetCurativeMaintenancelist(dr));
                        }
                    }
                }
            }
            return lstCurative;
        }

        public IList<CurativeMaintenance> GetCurativeMaintetanceBySiteInsCategoryId(int SiteId, int InstrumentCat, int userId)
        {
            string sql = "";
            if (SiteId != 0 && InstrumentCat != 0 && userId != 0)
            {
                sql = " SELECT CurativeMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, ProblemType.Name AS ProblemType" +

                           " FROM CurativeMaintenance left JOIN " +
                           " Problem ON Problem.Id = CurativeMaintenance.ProblemId LEFT JOIN " +
                           " ProblemType ON Problem.ProblemTypeId = ProblemType.Id LEFT JOIN "+
                           " Schedule ON CurativeMaintenance.ScheduleId = Schedule.Id left JOIN " +
                           " Site ON Problem.SiteId = Site.Id left JOIN " +
                           " Instrument ON CurativeMaintenance.InstrumentId = Instrument.Id OR Problem.InstrumentId = Instrument.Id left JOIN  " +
                           " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                           " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                           " WHERE InstrumentCatagory.Id = @InstrumentCat AND Problem.SiteId = @SiteId and CurativeMaintenance.EnginerId = @UserId";
            }
            else if (SiteId == 0 && InstrumentCat != 0 && userId == 0)
            {
                sql = " SELECT CurativeMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, ProblemType.Name AS ProblemType " +

                           " FROM CurativeMaintenance left JOIN " +
                           " Problem ON Problem.Id = CurativeMaintenance.ProblemId LEFT JOIN " +
                            " ProblemType ON Problem.ProblemTypeId = ProblemType.Id LEFT JOIN " +
                           " Schedule ON CurativeMaintenance.ScheduleId = Schedule.Id left JOIN " +
                           " Site ON Problem.SiteId = Site.Id left JOIN " +
                           " Instrument ON CurativeMaintenance.InstrumentId = Instrument.Id OR Problem.InstrumentId = Instrument.Id left JOIN  " +
                           " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                           " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                            " WHERE InstrumentCatagory.Id = @InstrumentCat";
            }
            else if (SiteId == 0 && InstrumentCat == 0 && userId == 0)
            {
                sql = " SELECT CurativeMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, ProblemType.Name AS ProblemType " +

                           " FROM CurativeMaintenance left JOIN " +
                           " Problem ON Problem.Id = CurativeMaintenance.ProblemId LEFT JOIN " +
                            " ProblemType ON Problem.ProblemTypeId = ProblemType.Id LEFT JOIN " +
                           " Schedule ON CurativeMaintenance.ScheduleId = Schedule.Id left JOIN " +
                           " Site ON Problem.SiteId = Site.Id left JOIN " +
                           " Instrument ON CurativeMaintenance.InstrumentId = Instrument.Id OR Problem.InstrumentId = Instrument.Id left JOIN  " +
                           " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                           " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id ";
            }
            else if (SiteId != 0 && InstrumentCat == 0 && userId == 0)
            {
                sql = " SELECT CurativeMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, ProblemType.Name AS ProblemType " +

                            " FROM CurativeMaintenance left JOIN " +
                            " Problem ON Problem.Id = CurativeMaintenance.ProblemId LEFT JOIN " +
                            " ProblemType ON Problem.ProblemTypeId = ProblemType.Id LEFT JOIN " +
                            " Schedule ON CurativeMaintenance.ScheduleId = Schedule.Id left JOIN " +
                            " Site ON Problem.SiteId = Site.Id left JOIN " +
                            " Instrument ON CurativeMaintenance.InstrumentId = Instrument.Id OR Problem.InstrumentId = Instrument.Id left JOIN  " +
                            " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                            " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                            " WHERE Problem.SiteId = @SiteId";
            }
            else if (SiteId != 0 && InstrumentCat == 0 && userId != 0)
            {
                sql = " SELECT CurativeMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, ProblemType.Name AS ProblemType" +

                           " FROM CurativeMaintenance left JOIN " +
                           " Problem ON Problem.Id = CurativeMaintenance.ProblemId LEFT JOIN " +
                           " ProblemType ON Problem.ProblemTypeId = ProblemType.Id LEFT JOIN " +
                           " Schedule ON CurativeMaintenance.ScheduleId = Schedule.Id left JOIN " +
                           " Site ON Problem.SiteId = Site.Id left JOIN " +
                           " Instrument ON CurativeMaintenance.InstrumentId = Instrument.Id OR Problem.InstrumentId = Instrument.Id left JOIN  " +
                           " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                           " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                           " WHERE Problem.SiteId = @SiteId and CurativeMaintenance.EnginerId = @UserId";
            }
            else if (SiteId == 0 && InstrumentCat == 0 && userId != 0)
            {
                sql = " SELECT CurativeMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, ProblemType.Name AS ProblemType" +

                           " FROM CurativeMaintenance left JOIN " +
                           " Problem ON Problem.Id = CurativeMaintenance.ProblemId LEFT JOIN " +
                           " ProblemType ON Problem.ProblemTypeId = ProblemType.Id LEFT JOIN " +
                           " Schedule ON CurativeMaintenance.ScheduleId = Schedule.Id left JOIN " +
                           " Site ON Problem.SiteId = Site.Id left JOIN " +
                           " Instrument ON CurativeMaintenance.InstrumentId = Instrument.Id OR Problem.InstrumentId = Instrument.Id left JOIN  " +
                           " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                           " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                           " WHERE CurativeMaintenance.EnginerId = @UserId";
            }
            else if (SiteId == 0 && InstrumentCat != 0 && userId != 0)
            {
                sql = " SELECT CurativeMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName, ProblemType.Name AS ProblemType " +

                           " FROM CurativeMaintenance left JOIN " +
                           " Problem ON Problem.Id = CurativeMaintenance.ProblemId LEFT JOIN " +
                            " ProblemType ON Problem.ProblemTypeId = ProblemType.Id LEFT JOIN " +
                           " Schedule ON CurativeMaintenance.ScheduleId = Schedule.Id left JOIN " +
                           " Site ON Problem.SiteId = Site.Id left JOIN " +
                           " Instrument ON CurativeMaintenance.InstrumentId = Instrument.Id OR Problem.InstrumentId = Instrument.Id left JOIN  " +
                           " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                           " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                            " WHERE InstrumentCatagory.Id = @InstrumentCat and CurativeMaintenance.EnginerId = @UserId";
            }
            IList<CurativeMaintenance> lstCurativeMaintenance = new List<CurativeMaintenance>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (SiteId != 0)
                    DatabaseHelper.InsertInt32Param("@SiteId", cm, SiteId);
                if (InstrumentCat != 0)
                    DatabaseHelper.InsertInt32Param("@InstrumentCat", cm, InstrumentCat);
                if (userId != 0)
                    DatabaseHelper.InsertInt32Param("@UserId", cm, userId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstCurativeMaintenance.Add(GetCurativeMaintenancelist(dr));
                        }
                    }
                }


            }
            return lstCurativeMaintenance;
        }
        
    }
}
