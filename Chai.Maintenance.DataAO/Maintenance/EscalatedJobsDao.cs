﻿ 
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.DataAccess.Maintenance
{
    public class EscalatedJobsDao : BaseDao
    {
        public EscalatedJobs GetEscalatedJobsById(int escalatedJobsId)
        {
            string sql = "SELECT EscalatedJobs.*" +
                        "FROM EscalatedJobs " +
                        "where EscalatedJobs.Id = @escalatedJobsId";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@escalatedJobsId", cm, escalatedJobsId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetEscalatedJobs(dr);
                        }
                    }
                }
            }
            return null;
        }
        

        private static EscalatedJobs GetEscalatedJobs(SqlDataReader dr)
        {
            EscalatedJobs escalatedJobs = new EscalatedJobs()
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr),
                ScheduleId = DatabaseHelper.GetInt32("ScheduleId", dr),
                ProblemId = DatabaseHelper.GetInt32("ProblemId", dr),
                EscalatedFromRegionId = DatabaseHelper.GetInt32("EscalatedFromRegionId", dr),
                EnginerEscalateReasonId = DatabaseHelper.GetInt32("EnginerEscalateReasonId", dr),
                Status = DatabaseHelper.GetString("Status", dr),

                RegionName = DatabaseHelper.GetString("RegionName", dr),
                EnginerName = DatabaseHelper.GetString("EnginerName", dr),
                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                ProblemNumber = DatabaseHelper.GetString("ProblemNumber", dr),
                SiteName = DatabaseHelper.GetString("SiteName", dr),
                SerialNo = DatabaseHelper.GetString("SerialNo", dr),
                
            };


            return escalatedJobs;
        }

        private static void SetEscalatedJobs(SqlCommand cm, EscalatedJobs escalatedJobs)
        {

            DatabaseHelper.InsertInt32Param("InstrumentId", cm, escalatedJobs.InstrumentId);
            DatabaseHelper.InsertInt32Param("ProblemId", cm, escalatedJobs.ProblemId);
            DatabaseHelper.InsertInt32Param("ScheduleId", cm, escalatedJobs.ScheduleId);
            DatabaseHelper.InsertInt32Param("EscalatedFromRegionId", cm, escalatedJobs.EscalatedFromRegionId);
            DatabaseHelper.InsertInt32Param("EnginerEscalateReasonId", cm, escalatedJobs.EnginerEscalateReasonId);
            DatabaseHelper.InsertStringVarCharParam("Status", cm, escalatedJobs.Status);
           
        }

        public void Save(EscalatedJobs escalatedJobs)
        {
            string sql = "INSERT INTO EscalatedJobs(InstrumentId, ProblemId, ScheduleId, EscalatedFromRegionId, EnginerEscalateReasonId, Status)"
                         + " VALUES (@InstrumentId, @ProblemId, @ScheduleId, @EscalatedFromRegionId, @EnginerEscalateReasonId, @Status) "
                         + " SELECT @@identity";

           ProblemDao problem = new ProblemDao();
            SqlTransaction tr = DefaultConnection.BeginTransaction();
            try
            {
                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                {
                    SetEscalatedJobs(cm, escalatedJobs);
                    problem.UpdateStatus("EscalatedSchedule", (int)MaintenanceStatus.EscalatedSchedule, escalatedJobs.ScheduleId, "", tr);

                    escalatedJobs.Id = int.Parse(cm.ExecuteScalar().ToString());
                    tr.Commit();
                }
            }
            catch (SqlException ex)
            {
                tr.Rollback();
            }
        }

        public IList<EscalatedJobs> GetEscalatedJobsByRegionSite(int regionId, int siteId)
        {
            string sql = " ";
            if(regionId != 0 && siteId != 0)
            {
                sql = "SELECT EscalatedJobs.*, Region.RegionName, InstrumentLookup.InstrumentName, Problem.ProblemNumber, Site.SiteName,  " +
                         "(AppUser.FirstName + ' ' + AppUser.LastName) AS EnginerName, Instrument.SerialNo " +
                         " FROM EscalatedJobs  " +

                        "LEFT JOIN Instrument ON EscalatedJobs.InstrumentId = Instrument.Id  " +
                        "LEFT JOIN Problem ON Problem.Id = EscalatedJobs.ProblemId " +
                        "LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id " +
                        "LEFT JOIN InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                        "LEFT JOIN Site ON Instrument.Site_Id = Site.Id " +
                        "LEFT JOIN EscalateReason ON EscalatedJobs.EnginerEscalateReasonId = EscalateReason.Id " +
                        "LEFT JOIN Region ON EscalatedJobs.EscalatedFromRegionId = Region.Id " +
                        "LEFT JOIN AppUser ON EscalateReason.EnginerId = AppUser.UserId " +
                      "WHERE EscalatedJobs.EscalatedFromRegionId = @regionId AND Instrument.Site_Id = @siteId";
            }
            else if(regionId !=0 && siteId == 0)
            {
                sql = "SELECT EscalatedJobs.*, Region.RegionName, InstrumentLookup.InstrumentName, Problem.ProblemNumber, Site.SiteName,  " +
                         "(AppUser.FirstName + ' ' + AppUser.LastName) AS EnginerName, Instrument.SerialNo " +
                         " FROM EscalatedJobs  " +

                        "LEFT JOIN Instrument ON EscalatedJobs.InstrumentId = Instrument.Id  " +
                        "LEFT JOIN Problem ON Problem.Id = EscalatedJobs.ProblemId " +
                        "LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id " +
                        "LEFT JOIN InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                        "LEFT JOIN Site ON Instrument.Site_Id = Site.Id " +
                        "LEFT JOIN EscalateReason ON EscalatedJobs.EnginerEscalateReasonId = EscalateReason.Id " +
                        "LEFT JOIN Region ON EscalatedJobs.EscalatedFromRegionId = Region.Id " +
                        "LEFT JOIN AppUser ON EscalateReason.EnginerId = AppUser.UserId " +
                        "WHERE EscalatedJobs.EscalatedFromRegionId = @regionId";
            }
            else if (regionId == 0 && siteId == 0)
            {
                sql = "SELECT EscalatedJobs.*, Region.RegionName, InstrumentLookup.InstrumentName, Problem.ProblemNumber, Site.SiteName,  " +
                         "(AppUser.FirstName + ' ' + AppUser.LastName) AS EnginerName, Instrument.SerialNo " +
                         " FROM EscalatedJobs  " +
             
                        "LEFT JOIN Instrument ON EscalatedJobs.InstrumentId = Instrument.Id  " +
                        "LEFT JOIN Problem ON Problem.Id = EscalatedJobs.ProblemId " +
                        "LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id " +
                        "LEFT JOIN InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                        "LEFT JOIN Site ON Instrument.Site_Id = Site.Id " +
                        "LEFT JOIN EscalateReason ON EscalatedJobs.EnginerEscalateReasonId = EscalateReason.Id " +
                        "LEFT JOIN Region ON EscalatedJobs.EscalatedFromRegionId = Region.Id " +
                        "LEFT JOIN AppUser ON EscalateReason.EnginerId = AppUser.UserId";
            }                                                

            IList<EscalatedJobs> lstEscalatedJobs = new List<EscalatedJobs>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                DatabaseHelper.InsertInt32Param("@siteId", cm, siteId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstEscalatedJobs.Add(GetEscalatedJobs(dr));
                        }
                    }
                }


            }
            return lstEscalatedJobs;
        }
        
    

    }
}