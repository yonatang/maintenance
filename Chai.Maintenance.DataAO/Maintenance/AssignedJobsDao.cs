﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;

namespace Chai.Maintenance.DataAccess.Maintenance
{
    public class AssignedJobsDao : BaseDao
    {


        public IList<AssignedJobs> GetAssignedJobs(string searchBy, int UserId, int siteid)
        {
            string sql = " ";

            if (searchBy == "Preventive")
            {
                sql = "SELECT  Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, Instrument.PreventiveMaintenancePeriod, " +
                       " Instrument.LastPreventiveMaintenanceDate, Instrument.IsFunctional,'' AS ProblemType, '' AS ErrorCode, " +
                       " InstrumentLookup.InstrumentName, '' as ProblemNumber, '' AS ProblemDescription, 0 AS ProblemId, Schedule.PreventiveScheduleStatus,Schedule.Id AS ScheduleID," +
                       " Schedule.Description AS ScheduleDescription, Schedule.ScheduledDateFrom, Schedule.ScheduledDateTo, Schedule.Date AS ScheduledDate, " +
                       " Site.SiteName, (AppUser.FirstName   +' '+ AppUser.LastName) AS SchedulerFullName, Instrument.Id as InstrumentId " +

                       " FROM  Schedule left JOIN" +

                       " Instrument ON Instrument.Id = Schedule.InstrumentId LEFT JOIN " +
                       " InstrumentLookup ON InstrumentLookup.Id = Instrument.InstrumentName_Id LEFT OUTER JOIN" +
                       //" Problem ON Instrument.Id = Problem.InstrumentId LEFT OUTER JOIN" +
                       " Site ON Instrument.Site_Id = Site.Id LEFT JOIN" +
                       //" ProblemType ON Problem.ProblemTypeId = ProblemType.Id LEFT JOIN" +
                       //" ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id LEFT JOIN" +
                       " AppUser ON Schedule.UserId = AppUser.UserId  WHERE Schedule.ProblemId = 0 AND PreventiveScheduleStatus = 'Not Performed'  AND @UserId = Schedule.EnginerId";
            }
            else if (searchBy == "Curative")
            {
                sql = "SELECT  Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, Instrument.PreventiveMaintenancePeriod, " +
                      " Instrument.LastPreventiveMaintenanceDate, Instrument.IsFunctional,ProblemType.Name AS ProblemType, ErrorCode.Name AS ErrorCode, " +
                      " InstrumentLookup.InstrumentName, Problem.ProblemNumber, dbo.Problem.Description AS ProblemDescription, Problem.Id AS ProblemId,  Schedule.PreventiveScheduleStatus," +
                      " Schedule.Description AS ScheduleDescription, Schedule.ScheduledDateFrom, Schedule.ScheduledDateTo, Schedule.Date AS ScheduledDate, Schedule.Id AS ScheduleID," +
                      " Site.SiteName, (AppUser.FirstName   +' '+ AppUser.LastName) AS SchedulerFullName, Instrument.Id as InstrumentId " +

                      " FROM  Schedule left JOIN" +

                      " Instrument ON Instrument.Id = Schedule.InstrumentId LEFT JOIN " +
                      " InstrumentLookup ON InstrumentLookup.Id = Instrument.InstrumentName_Id   LEFT JOIN" +
                      " Problem ON Instrument.Id = Problem.InstrumentId LEFT OUTER JOIN" +
                      " Site ON Instrument.Site_Id = Site.Id LEFT JOIN" +
                      " ProblemType ON Problem.ProblemTypeId = ProblemType.Id LEFT JOIN" +
                      " ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id LEFT JOIN" +
                      " AppUser ON Schedule.UserId = AppUser.UserId WHERE Schedule.ProblemId != 0 AND Problem.Status = @Status AND CurativeScheduleStatus = 'Not Performed' AND @UserId = Schedule.EnginerId";
            }
            else if (searchBy == "CurativeConfirmation")
            {
                sql = "SELECT  Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, Instrument.PreventiveMaintenancePeriod, " +
                      " Instrument.LastPreventiveMaintenanceDate, Instrument.IsFunctional,ProblemType.Name AS ProblemType, ErrorCode.Name AS ErrorCode, " +
                      " InstrumentLookup.InstrumentName, Problem.ProblemNumber, dbo.Problem.Description AS ProblemDescription, Problem.Id AS ProblemId,  Schedule.PreventiveScheduleStatus," +
                      " Schedule.Description AS ScheduleDescription, Schedule.ScheduledDateFrom, Schedule.ScheduledDateTo, Schedule.Date AS ScheduledDate, Schedule.Id AS ScheduleID," +
                      " Site.SiteName, (AppUser.FirstName   +' '+ AppUser.LastName) AS SchedulerFullName, Instrument.Id as InstrumentId " +

                      " FROM  Schedule left JOIN" +

                      " Instrument ON Instrument.Id = Schedule.InstrumentId LEFT JOIN " +
                      " InstrumentLookup ON InstrumentLookup.Id = Instrument.InstrumentName_Id   LEFT JOIN" +
                      " Problem ON Instrument.Id = Problem.InstrumentId LEFT OUTER JOIN" +
                      " Site ON Instrument.Site_Id = Site.Id LEFT JOIN" +
                      " ProblemType ON Problem.ProblemTypeId = ProblemType.Id LEFT JOIN" +
                      " ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id LEFT JOIN" +
                      " AppUser ON Schedule.UserId = AppUser.UserId WHERE Schedule.ProblemId != 0 AND Problem.Status = 2 AND Schedule.CurativeScheduleStatus = 'Needs Approval' AND @UserId = Problem.UserId";
            }


            if (searchBy == "PreventiveConfirmation")
            {
                sql = "SELECT  Instrument.SerialNo, Instrument.InstallationDate, Instrument.WarranityExpireDate, Instrument.PreventiveMaintenancePeriod, " +
                       " Instrument.LastPreventiveMaintenanceDate, Instrument.IsFunctional,'' AS ProblemType, '' AS ErrorCode, " +
                       " InstrumentLookup.InstrumentName, '' as ProblemNumber, '' AS ProblemDescription, 0 AS ProblemId, Schedule.PreventiveScheduleStatus,Schedule.Id AS ScheduleID," +
                       " Schedule.Description AS ScheduleDescription, Schedule.ScheduledDateFrom, Schedule.ScheduledDateTo, Schedule.Date AS ScheduledDate, " +
                       " Site.SiteName, (AppUser.FirstName   +' '+ AppUser.LastName) AS SchedulerFullName, Instrument.Id as InstrumentId " +

                       " FROM  Schedule left JOIN" +

                       " Instrument ON Instrument.Id = Schedule.InstrumentId LEFT JOIN " +
                       " InstrumentLookup ON InstrumentLookup.Id = Instrument.InstrumentName_Id LEFT OUTER JOIN" +
                    //" Problem ON Instrument.Id = Problem.InstrumentId LEFT OUTER JOIN" +
                       " Site ON Instrument.Site_Id = Site.Id LEFT JOIN" +
                    //" ProblemType ON Problem.ProblemTypeId = ProblemType.Id LEFT JOIN" +
                    //" ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id LEFT JOIN" +
                       " AppUser ON Schedule.UserId = AppUser.UserId  WHERE Schedule.ProblemId = 0 AND PreventiveScheduleStatus = 'Needs Approval' AND @UserId = Schedule.UserId";
            }

            IList<AssignedJobs> lstAssignedJobs = new List<AssignedJobs>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@UserId", cm, UserId);
                DatabaseHelper.InsertInt32Param("@SiteId", cm, siteid);
                DatabaseHelper.InsertInt32Param("@Status",cm, 3);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstAssignedJobs.Add(GetAssignedJobs(dr));
                        }
                    }
                }


            }
             return lstAssignedJobs;
        }
        private static AssignedJobs GetAssignedJobs(SqlDataReader dr)
        {
            AssignedJobs _AssignedJobs = new AssignedJobs();

            _AssignedJobs.SiteName = DatabaseHelper.GetString("SiteName", dr);
            _AssignedJobs.InstrumentName = DatabaseHelper.GetString("InstrumentName", dr);
            _AssignedJobs.SerialNo = DatabaseHelper.GetString("SerialNo", dr);
            _AssignedJobs.InstallationDate = DatabaseHelper.GetDateTime("InstallationDate", dr);
            _AssignedJobs.WarranityExpireDate = DatabaseHelper.GetDateTime("WarranityExpireDate", dr);
            _AssignedJobs.LastPreventiveMaintenanceDate = DatabaseHelper.GetDateTime("LastPreventiveMaintenanceDate", dr);
            _AssignedJobs.PreventiveMaintenancePeriod = DatabaseHelper.GetString("PreventiveMaintenancePeriod", dr);
            _AssignedJobs.IsFunctional = DatabaseHelper.GetBoolean("IsFunctional", dr);
            _AssignedJobs.ProblemNumber = DatabaseHelper.GetString("ProblemNumber", dr);
            _AssignedJobs.ProblemDescription = DatabaseHelper.GetString("ProblemDescription", dr);
            _AssignedJobs.ScheduledDateFrom = DatabaseHelper.GetDateTime("ScheduledDateFrom", dr);
            _AssignedJobs.ScheduledDateTo = DatabaseHelper.GetDateTime("ScheduledDateTo", dr);
            _AssignedJobs.SchedulerFullName = DatabaseHelper.GetString("SchedulerFullName", dr);
            _AssignedJobs.InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr);
            _AssignedJobs.ProblemId = DatabaseHelper.GetInt32("ProblemId", dr);
            _AssignedJobs.ProblemType = DatabaseHelper.GetString("ProblemType", dr);
            _AssignedJobs.ErrorCode = DatabaseHelper.GetString("ErrorCode", dr);
            _AssignedJobs.PreventiveScheduleStatus = DatabaseHelper.GetString("PreventiveScheduleStatus", dr);
            _AssignedJobs.ScheduleID = DatabaseHelper.GetInt32("ScheduleID", dr);
            return _AssignedJobs;
        }

    }
}