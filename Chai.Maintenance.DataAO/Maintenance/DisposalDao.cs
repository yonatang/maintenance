﻿ 
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.DataAccess.Maintenance
{
    public class DisposalDao : BaseDao
    {
        public Disposal GetDisposalById(int DisposalId)
        {
            string sql = "SELECT Disposal.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +
                         "FROM Disposal " +

                         "LEFT JOIN Instrument ON Disposal.InstrumentId = Instrument.Id " +

                         "LEFT JOIN InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id " +
                         "LEFT JOIN InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                         "LEFT JOIN Site ON Instrument.Site_Id = Site.Id " +
                         "where Disposal.Id = @DisposalId";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@DisposalId", cm, DisposalId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetDisposal(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static Disposal GetDisposallist(SqlDataReader dr)
        {
            Disposal _Disposal = new Disposal
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr),
                DisposedByID = DatabaseHelper.GetInt32("DisposedByID", dr),
                Reason = DatabaseHelper.GetString("Reason", dr),
                Date = DatabaseHelper.GetDateTime("Date", dr),
                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                SiteName = DatabaseHelper.GetString("SiteName", dr)
            };
            return _Disposal;
        }

        private static Disposal GetDisposal(SqlDataReader dr)
        {
            Disposal _Disposal = new Disposal
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr),
                DisposedByID = DatabaseHelper.GetInt32("DisposedByID", dr),
                Reason = DatabaseHelper.GetString("Reason", dr),
                Date = DatabaseHelper.GetDateTime("Date", dr),
                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                SiteName = DatabaseHelper.GetString("SiteName", dr)
            };


            return _Disposal;
        }

        private static void SetDisposal(SqlCommand cm, Disposal _Disposal)
        {

            DatabaseHelper.InsertInt32Param("InstrumentId", cm, _Disposal.InstrumentId);
            DatabaseHelper.InsertInt32Param("DisposedByID", cm, _Disposal.DisposedByID);
            DatabaseHelper.InsertStringVarCharParam("Reason", cm, _Disposal.Reason);
            DatabaseHelper.InsertDateTimeParam("Date", cm, _Disposal.Date);
           
        }

        public void Save(Disposal _Disposal)
        {
            string sql = "INSERT INTO Disposal(InstrumentId, DisposedByID, Reason, Date)"
                         + " VALUES (@InstrumentId, @DisposedByID, @Reason, @Date) "
                         + " SELECT @@identity";

            ProblemDao _problem = new ProblemDao();
            SqlTransaction tr = DefaultConnection.BeginTransaction();
            try
            {
                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                {
                    SetDisposal(cm, _Disposal);
                    _problem.UpdateStatus("Disposal", (int)MaintenanceStatus.DisposalStatus, _Disposal.InstrumentId, "", tr);
             
                    _Disposal.Id = int.Parse(cm.ExecuteScalar().ToString());
                    tr.Commit();
                }
            }
            catch (SqlException ex)
            {
                tr.Rollback();
            }
        }

        public void Update(Disposal _Disposal)
        {
            string sql = "Update Disposal SET InstrumentId = @InstrumentId, DisposedByID = @DisposedByID, Reason = @Reason, Date = @Date where Id =@Id";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@Id", cm, _Disposal.Id);
                SetDisposal(cm, _Disposal);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int DisposalId)
        {
            string sql = "Delete Disposal where Id = @DisposalId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@DisposalId", cm, DisposalId);
                cm.ExecuteNonQuery();
            }
        }

        public IList<Disposal> GetDisposalBySiteInsCategoryId(int SiteId, int InstrumentCat, int userId)
        {
            string sql = "";
            if (SiteId != 0 && InstrumentCat != 0 && userId !=0)
            {
                sql = "SELECT Disposal.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +
                        "FROM Disposal left JOIN " +
                        "Instrument ON Disposal.InstrumentId = Instrument.Id left JOIN " +
                        "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN  " +
                        "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN  " +
                        "Site ON Instrument.Site_Id = Site.Id " +
                        "WHERE InstrumentCatagory.Id = @InstrumentCat AND Instrument.Site_Id = @SiteId AND Disposal.DisposedByID = @userId";
            }

            else if (SiteId != 0 && InstrumentCat == 0 && userId == 0)
            {
                sql = "SELECT Disposal.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +
                          "FROM Disposal left JOIN " +
                          "Instrument ON Disposal.InstrumentId = Instrument.Id left JOIN " +
                          "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN  " +
                          "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN  " +
                          "Site ON Instrument.Site_Id = Site.Id " +
                          "WHERE Instrument.Site_Id = @SiteId";
            }
            else if (SiteId == 0 && InstrumentCat == 0 && userId == 0)
            {
                sql = "SELECT Disposal.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +
                      "FROM Disposal left JOIN " +
                      "Instrument ON Disposal.InstrumentId = Instrument.Id left JOIN " +
                      "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN  " +
                      "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN  " +
                      "Site ON Instrument.Site_Id = Site.Id ";
            }
            else if (SiteId == 0 && InstrumentCat != 0 && userId != 0)
            {
                sql = "SELECT Disposal.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +
                        "FROM Disposal left JOIN " +
                        "Instrument ON Disposal.InstrumentId = Instrument.Id left JOIN " +
                        "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN  " +
                        "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN  " +
                        "Site ON Instrument.Site_Id = Site.Id " +
                        "WHERE InstrumentCatagory.Id = @InstrumentCat AND Disposal.DisposedByID = @userId";
            }

            else if (SiteId == 0 && InstrumentCat == 0 && userId != 0)
            {
                sql = "SELECT Disposal.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +
                        "FROM Disposal left JOIN " +
                        "Instrument ON Disposal.InstrumentId = Instrument.Id left JOIN " +
                        "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN  " +
                        "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN  " +
                        "Site ON Instrument.Site_Id = Site.Id " +
                        "WHERE Disposal.DisposedByID = @userId";
            }
            else if (SiteId != 0 && InstrumentCat == 0 && userId != 0)
            {
                sql = "SELECT Disposal.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +
                        "FROM Disposal left JOIN " +
                        "Instrument ON Disposal.InstrumentId = Instrument.Id left JOIN " +
                        "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN  " +
                        "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN  " +
                        "Site ON Instrument.Site_Id = Site.Id " +
                        "WHERE Instrument.Site_Id = @SiteId AND Disposal.DisposedByID = @userId";
            }
            IList<Disposal> lstDisposal = new List<Disposal>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (SiteId != 0)
                    DatabaseHelper.InsertInt32Param("@SiteId", cm, SiteId);
                if (InstrumentCat != 0)
                    DatabaseHelper.InsertInt32Param("@InstrumentCat", cm, InstrumentCat);
                if(userId !=0)
                    DatabaseHelper.InsertInt32Param("@userId",cm,userId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstDisposal.Add(GetDisposal(dr));
                        }
                    }
                }


            }
            return lstDisposal;
        }

    }
}