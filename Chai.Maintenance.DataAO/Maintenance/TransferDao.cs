﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.DataAccess.Maintenance
{
    public class TransferDao : BaseDao
    {
        public Transfer GetTransferById(int TransferId)
        {
            string sql = "SELECT Transfer.*, NewSite.SiteName As NewSite, OldSite.SiteName As OldSite, InstrumentLookup.InstrumentName " +
                
                        "FROM Transfer LEFT JOIN " +

                        "Instrument ON Transfer.InstrumentId = Instrument.Id left JOIN " +
                        "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                        "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN " +
                        "Site AS OldSite ON  Transfer.CurrentSiteId = OldSite.Id Left Join " +
                        "Site AS NewSite On Transfer.TransferToSIteId = NewSite.Id " +
                        "WHERE Transfer.Id = @TransferId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@TransferId", cm, TransferId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetTransfer(dr);
                        }
                    }
                    dr.Close();
                }
            }
            return null;
        }

        //public IList<Transfer> GetTransferList(int siteId, int InstrumentCategoryId)
        //{
        //    string sql = " ";
           
        //    IList<Transfer> lstTransfer = new List<Transfer>();
        //    using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
        //    {
        //        if (siteId != 0)
        //            DatabaseHelper.InsertInt32Param("@siteId", cm, siteId);
        //        if (InstrumentCategoryId != 0)
        //            DatabaseHelper.InsertInt32Param("@InstrumentCategoryId", cm, InstrumentCategoryId);
        //        using (SqlDataReader dr = cm.ExecuteReader())
        //        {
        //            if (dr != null)
        //            {
        //                while (dr.Read())
        //                {
        //                    lstTransfer.Add(GetTransfer(dr));
        //                }
        //            }
        //        }


        //    }
        //    return lstTransfer;
        //}
     
        private static Transfer GetTransfer(SqlDataReader dr)
        {
            Transfer transfer = new Transfer();
            transfer.Id = DatabaseHelper.GetInt32("Id", dr);
            transfer.InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr);
            transfer.TransferToSiteId = DatabaseHelper.GetInt32("TransferToSiteId", dr);
            transfer.CurrentSiteId = DatabaseHelper.GetInt32("CurrentSiteId", dr);
            transfer.TransferedById = DatabaseHelper.GetInt32("TransferedById", dr);
            transfer.NewUserId = DatabaseHelper.GetInt32("NewUserId", dr);
            transfer.Reason = DatabaseHelper.GetString("Reason", dr);
            transfer.Date = DatabaseHelper.GetDateTime("Date", dr);
            transfer.InstrumentName = DatabaseHelper.GetString("InstrumentName", dr);
            transfer.NewSite = DatabaseHelper.GetString("NewSite", dr);
            transfer.OldSite = DatabaseHelper.GetString("OldSite", dr);
            return transfer;
        }

        private static void SetTransfer(SqlCommand cm, Transfer transfer)
        {
            DatabaseHelper.InsertInt32Param("InstrumentId", cm, transfer.InstrumentId);
            DatabaseHelper.InsertInt32Param("TransferToSiteId", cm, transfer.TransferToSiteId);
            DatabaseHelper.InsertInt32Param("CurrentSiteId", cm, transfer.CurrentSiteId);
            DatabaseHelper.InsertInt32Param("TransferedById", cm, transfer.TransferedById);
            DatabaseHelper.InsertInt32Param("NewUserId", cm, transfer.NewUserId);
            DatabaseHelper.InsertStringVarCharParam("Reason", cm, transfer.Reason);

            DatabaseHelper.InsertDateTimeParam("Date", cm, transfer.Date);
             
        }

        public void Save(Transfer transfer)
        {
            string sql = "INSERT INTO Transfer(InstrumentId, TransferToSiteId, CurrentSiteId, TransferedById, NewUserId,Reason,Date) " +
                         " VALUES (@InstrumentId, @TransferToSiteId, @CurrentSiteId, @TransferedById, @NewUserId, @Reason, @Date) " +
                         " SELECT @@identity";
            SqlTransaction tr = DefaultConnection.BeginTransaction();
            ProblemDao _problemDao = new ProblemDao();
            try
            {
                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                {
                    SetTransfer(cm, transfer);
                    _problemDao.UpdateStatus("Transfer", (int)MaintenanceStatus.TransferStatus, transfer.InstrumentId, "", tr);
               
                    transfer.Id = int.Parse(cm.ExecuteScalar().ToString());
                    tr.Commit();
                }
            }
            catch (SqlException ex)
            {
                tr.Rollback();
            }
        }

        public void Update(Transfer transfer)
        {
            string sql = "Update Transfer SET InstrumentId=@InstrumentId, TransferToSiteId=@TransferToSiteId, CurrentSiteId=@CurrentSiteId, TransferedById=@TransferedById, " +
                                  "NewUserId=@NewUserId, Reason=@Reason, Date=@Date where Id =@Id";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@Id", cm, transfer.Id);
                SetTransfer(cm, transfer);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int transferId)
        {
            string sql = "Delete Transfer where Id = @transferId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@transferId", cm, transferId);
                cm.ExecuteNonQuery();
            }
        }

        public IList<Transfer> GetTransferBySiteInsCategoryId(int UserId, int InstrumentCat)
        {
            string sql = "";
            if (UserId != 0 && InstrumentCat != 0)
            {
                sql = "SELECT Transfer.*, NewSite.SiteName As NewSite, OldSite.SiteName As OldSite, InstrumentLookup.InstrumentName " +

                        "FROM Transfer LEFT JOIN " +

                        "Instrument ON Transfer.InstrumentId = Instrument.Id left JOIN " +
                        "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                        "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN " +
                        "Site AS OldSite ON  Transfer.CurrentSiteId = OldSite.Id Left Join " +
                        "Site AS NewSite On Transfer.TransferToSIteId = NewSite.Id " +
                        "WHERE InstrumentCatagory.Id = @InstrumentCat AND Transfer.TransferedById = @UserId";
            }

            else if (UserId != 0 && InstrumentCat == 0)
            {
                sql = "SELECT Transfer.*, NewSite.SiteName As NewSite, OldSite.SiteName As OldSite, InstrumentLookup.InstrumentName " +

                        "FROM Transfer LEFT JOIN " +

                        "Instrument ON Transfer.InstrumentId = Instrument.Id left JOIN " +
                        "InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                        "InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id left JOIN " +
                        "Site AS OldSite ON  Transfer.CurrentSiteId = OldSite.Id Left Join " +
                        "Site AS NewSite On Transfer.TransferToSIteId = NewSite.Id " +
                        "WHERE Transfer.TransferedById = @UserId";
            }

            IList<Transfer> lstTransfer = new List<Transfer>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (UserId != 0)
                    DatabaseHelper.InsertInt32Param("@UserId", cm, UserId);
                if (InstrumentCat != 0)
                    DatabaseHelper.InsertInt32Param("@InstrumentCat", cm, InstrumentCat);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstTransfer.Add(GetTransfer(dr));
                        }
                    }
                }


            }
            return lstTransfer;
        }
    }
}
