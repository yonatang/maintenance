﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.DataAccess.Maintenance
{
    public class ConsumableNotificationDao : BaseDao
    {
        public ConsumableNotification GetConsumableNotificationById(int consumableNotificationId)
        {
            string sql = "SELECT ConsumableNotification.*, Site.SiteName, (AppUser.FirstName   +' '+ AppUser.LastName) AS NotifiedBy, Consumables.Name AS ConsumableName " +
                         "FROM ConsumableNotification " +

                         "LEFT JOIN Consumables ON Consumables.Id = ConsumableNotification.ConsumableId " +
                         "LEFT JOIN Site ON ConsumableNotification.SiteId = Site.Id " +
                         "LEFT JOIN AppUser ON Appuser.UserId = ConsumableNotification.NotifiedById " +
                         "where ConsumableNotification.Id = @consumableNotificationId";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@consumableNotificationId", cm, consumableNotificationId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetConsumableNotification(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static ConsumableNotification GetConsumableNotificationList(SqlDataReader dr)
        {
            ConsumableNotification _consumableNotification = new ConsumableNotification
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                ConsumableId = DatabaseHelper.GetInt32("ConsumableId", dr),
                SiteId = DatabaseHelper.GetInt32("SiteId", dr),
                NotifiedById = DatabaseHelper.GetInt32("NotifiedById", dr),
                Date = DatabaseHelper.GetDateTime("Date", dr),
                NotifiedBy = DatabaseHelper.GetString("NotifiedBy", dr),
                Remark = DatabaseHelper.GetString("Remark", dr),
                SiteName = DatabaseHelper.GetString("SiteName", dr),
                ConsumableName = DatabaseHelper.GetString("ConsumableName", dr)
            };
            return _consumableNotification;
        }

        private static ConsumableNotification GetConsumableNotification(SqlDataReader dr)
        {
            ConsumableNotification _consumableNotification = new ConsumableNotification
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                ConsumableId = DatabaseHelper.GetInt32("ConsumableId", dr),
                SiteId = DatabaseHelper.GetInt32("SiteId", dr),
                NotifiedById = DatabaseHelper.GetInt32("NotifiedById", dr),
                Date = DatabaseHelper.GetDateTime("Date", dr),
                NotifiedBy = DatabaseHelper.GetString("NotifiedBy", dr),
                Remark = DatabaseHelper.GetString("Remark", dr),
                SiteName = DatabaseHelper.GetString("SiteName", dr),
                ConsumableName = DatabaseHelper.GetString("ConsumableName", dr)
            };


            return _consumableNotification;
        }

        private static void SetConsumableNotification(SqlCommand cm, ConsumableNotification consumableNotification)
        {

            DatabaseHelper.InsertInt32Param("ConsumableId", cm, consumableNotification.ConsumableId);
            DatabaseHelper.InsertInt32Param("SiteId", cm, consumableNotification.SiteId);
            DatabaseHelper.InsertInt32Param("NotifiedById", cm, consumableNotification.NotifiedById);
            DatabaseHelper.InsertDateTimeParam("Date", cm, consumableNotification.Date);
            DatabaseHelper.InsertStringVarCharParam("NotifiedBy", cm, consumableNotification.NotifiedBy);
            DatabaseHelper.InsertStringNVarCharParam("Remark", cm, consumableNotification.Remark);
            DatabaseHelper.InsertStringVarCharParam("SiteName", cm, consumableNotification.SiteName);
            DatabaseHelper.InsertStringVarCharParam("ConsumableName", cm, consumableNotification.ConsumableName);
        }

        public void Save(ConsumableNotification consumableNotification)
        {
            string sql = "INSERT INTO ConsumableNotification(ConsumableId, SiteId, NotifiedById, Date, Remark)"
                         + " VALUES (@ConsumableId, @SiteId, @NotifiedById, @Date, @Remark) "
                         + " SELECT @@identity";

           
            SqlTransaction tr = DefaultConnection.BeginTransaction();
            try
            {
                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                {
                    SetConsumableNotification(cm, consumableNotification);
                    consumableNotification.Id = int.Parse(cm.ExecuteScalar().ToString());
                    tr.Commit();
                }
            }
            catch (SqlException ex)
            {
                tr.Rollback();
            }
        }

        public void Update(ConsumableNotification consumableNotification)
        {
            string sql = "Update ConsumableNotification SET ConsumableId = @ConsumableId, SiteId = @SiteId, NotifiedById = @NotifiedById, Date = @Date, Remark = @Remark where Id =@Id";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@Id", cm, consumableNotification.Id);
                SetConsumableNotification(cm, consumableNotification);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int consumableNotificationId)
        {
            string sql = "Delete ConsumableNotification where Id = @consumableNotificationId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@consumableNotificationId", cm, consumableNotificationId);
                cm.ExecuteNonQuery();
            }
        }

        public IList<ConsumableNotification> GetConsumableNotificationBySiteConsumableId(int siteId, int consumableId, int notifiedById)
        {
            string sql = "";
            if (siteId != 0 && consumableId != 0 && notifiedById != 0)
            {
                sql = "SELECT ConsumableNotification.*, Site.SiteName, (AppUser.FirstName   +' '+ AppUser.LastName) AS NotifiedBy, Consumables.Name AS ConsumableName " +
                        "FROM ConsumableNotification " +
                         "LEFT JOIN Consumables ON Consumables.Id = ConsumableNotification.ConsumableId " +
                         "LEFT JOIN Site ON ConsumableNotification.SiteId = Site.Id " +
                         "LEFT JOIN AppUser ON Appuser.UserId = ConsumableNotification.NotifiedById " +
                        "WHERE ConsumableNotification.ConsumableId = @consumableId AND ConsumableNotification.SiteId = @SiteId AND ConsumableNotification.NotifiedById = @notifiedById";
            }
            else if (siteId != 0 && consumableId != 0 && notifiedById == 0)
            {
                sql = "SELECT ConsumableNotification.*, Site.SiteName, (AppUser.FirstName   +' '+ AppUser.LastName) AS NotifiedBy, Consumables.Name AS ConsumableName " +
                      "FROM ConsumableNotification " +
                       "LEFT JOIN Consumables ON Consumables.Id = ConsumableNotification.ConsumableId " +
                       "LEFT JOIN Site ON ConsumableNotification.SiteId = Site.Id " +
                       "LEFT JOIN AppUser ON Appuser.UserId = ConsumableNotification.NotifiedById " +
                       "WHERE ConsumableNotification.ConsumableId = @consumableId AND ConsumableNotification.SiteId = @SiteId";
            }
            else if (siteId != 0 && consumableId == 0 && notifiedById == 0)
            {
                sql = "SELECT ConsumableNotification.*, Site.SiteName, (AppUser.FirstName   +' '+ AppUser.LastName) AS NotifiedBy, Consumables.Name AS ConsumableName " +
                      "FROM ConsumableNotification " +
                       "LEFT JOIN Consumables ON Consumables.Id = ConsumableNotification.ConsumableId " +
                       "LEFT JOIN Site ON ConsumableNotification.SiteId = Site.Id " +
                       "LEFT JOIN AppUser ON Appuser.UserId = ConsumableNotification.NotifiedById " +
                       "WHERE ConsumableNotification.ConsumableId = @consumableId ";
            }
            else if (siteId != 0 && consumableId == 0 && notifiedById != 0)
            {
                sql = "SELECT ConsumableNotification.*, Site.SiteName, (AppUser.FirstName   +' '+ AppUser.LastName) AS NotifiedBy, Consumables.Name AS ConsumableName " +
                      "FROM ConsumableNotification " +
                       "LEFT JOIN Consumables ON Consumables.Id = ConsumableNotification.ConsumableId " +
                       "LEFT JOIN Site ON ConsumableNotification.SiteId = Site.Id " +
                       "LEFT JOIN AppUser ON Appuser.UserId = ConsumableNotification.NotifiedById " +
                        "WHERE ConsumableNotification.SiteId = @SiteId AND ConsumableNotification.NotifiedById = @notifiedById";
            }
            else if (siteId != 0 && consumableId != 0 && notifiedById == 0)
            {
                sql = "SELECT ConsumableNotification.*, Site.SiteName, (AppUser.FirstName   +' '+ AppUser.LastName) AS NotifiedBy, Consumables.Name AS ConsumableName " +
                       "FROM ConsumableNotification " +
                        "LEFT JOIN Consumables ON Consumables.Id = ConsumableNotification.ConsumableId " +
                        "LEFT JOIN Site ON ConsumableNotification.SiteId = Site.Id " +
                        "LEFT JOIN AppUser ON Appuser.UserId = ConsumableNotification.NotifiedById " +
                        "WHERE ConsumableNotification.SiteId = @SiteId AND ConsumableNotification.NotifiedById = @notifiedById";
            }
            else if (siteId == 0 && consumableId == 0 && notifiedById != 0)
            {
                sql = "SELECT ConsumableNotification.*, Site.SiteName, (AppUser.FirstName   +' '+ AppUser.LastName) AS NotifiedBy, Consumables.Name AS ConsumableName " +
                                     "FROM ConsumableNotification " +
                                      "LEFT JOIN Consumables ON Consumables.Id = ConsumableNotification.ConsumableId " +
                                      "LEFT JOIN Site ON ConsumableNotification.SiteId = Site.Id " +
                                      "LEFT JOIN AppUser ON Appuser.UserId = ConsumableNotification.NotifiedById " +
                        "WHERE ConsumableNotification.NotifiedById = @notifiedById";
            }
            else if (siteId == 0 && consumableId != 0 && notifiedById != 0)
            {
                sql = "SELECT ConsumableNotification.*, Site.SiteName, (AppUser.FirstName   +' '+ AppUser.LastName) AS NotifiedBy, Consumables.Name AS ConsumableName " +
                        "FROM ConsumableNotification " +
                         "LEFT JOIN Consumables ON Consumables.Id = ConsumableNotification.ConsumableId " +
                         "LEFT JOIN Site ON ConsumableNotification.SiteId = Site.Id " +
                         "LEFT JOIN AppUser ON Appuser.UserId = ConsumableNotification.NotifiedById " +
                        "WHERE ConsumableNotification.ConsumableId = @consumableId AND ConsumableNotification.NotifiedById = @notifiedById";
            }
            IList<ConsumableNotification> lstConsumableNotification = new List<ConsumableNotification>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (siteId != 0)
                    DatabaseHelper.InsertInt32Param("@SiteId", cm, siteId);
                if (consumableId != 0)
                    DatabaseHelper.InsertInt32Param("@consumableId", cm, consumableId);
                if(notifiedById !=0)
                    DatabaseHelper.InsertInt32Param("@notifiedById", cm, notifiedById);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstConsumableNotification.Add(GetConsumableNotification(dr));
                        }
                    }
                }


            }
            return lstConsumableNotification;
        }

    }
}
