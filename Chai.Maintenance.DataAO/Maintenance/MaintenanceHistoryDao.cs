﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;
namespace Chai.Maintenance.DataAccess.Maintenance
{
   public class MaintenanceHistoryDao : BaseDao
    {
       public IList<MaintenanceHistory> GetMaintenanceHistory(int instrumentId, int scheduleId, int problemId)
       {
           string sql = " SELECT  ProblemType.Name AS ProblemType, ErrorCode.Name AS ErrorCode,Problem.DateReported," +
                        " Problem.ProblemNumber, dbo.Problem.Description AS Description, (AppUser.FirstName + ' ' + AppUser.LastName) as EnginerName," +
                        " CurativeMaintenance.Date as MaintainedDate, CurativeMaintenance.EngineerComments as EnginerComment, CurativeMaintenance.CauseOfEqptFailure as CauseOfFailure," +
                        " CurativeMaintenance.CurativeActionTaken as ActionTaken, CurativeMaintenance.MaintainedParts " +
                        " FROM  CurativeMaintenance left JOIN" +

                            " Problem ON CurativeMaintenance.ProblemId = Problem.InstrumentId LEFT JOIN" +
                            " ProblemType ON Problem.ProblemTypeId = ProblemType.Id LEFT JOIN" +
                            " ErrorCode ON Problem.ErrorCodeId = ErrorCode.Id LEFT JOIN" +
                            " AppUser ON CurativeMaintenance.EnginerId = AppUser.UserId " +
                            " where CurativeMaintenance.InstrumentId = @instrumentId";

           IList<MaintenanceHistory> lstMaintenanceHistory = new List<MaintenanceHistory>();
           using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
           {
               DatabaseHelper.InsertInt32Param("@instrumentId", cm, instrumentId);
               //DatabaseHelper.InsertInt32Param("@scheduleId", cm, scheduleId);
               //DatabaseHelper.InsertInt32Param("@problemId", cm, problemId);
               using (SqlDataReader dr = cm.ExecuteReader())
               {
                   if (dr != null)
                   {
                       while (dr.Read())
                       {
                           lstMaintenanceHistory.Add(GetMaintenanceHistory(dr));
                       }
                   }
               }


           }
           return lstMaintenanceHistory;
       }
       private static MaintenanceHistory GetMaintenanceHistory(SqlDataReader dr)
        {
            MaintenanceHistory _maintenanceHistory = new MaintenanceHistory();

            _maintenanceHistory.ProblemNumber = DatabaseHelper.GetString("ProblemNumber", dr);
            _maintenanceHistory.Description = DatabaseHelper.GetString("Description", dr);
            _maintenanceHistory.ProblemType = DatabaseHelper.GetString("ProblemType", dr);
            _maintenanceHistory.ErrorCode = DatabaseHelper.GetString("ErrorCode", dr);
            _maintenanceHistory.DateReported = DatabaseHelper.GetDateTime("DateReported", dr);
            _maintenanceHistory.EnginerName = DatabaseHelper.GetString("EnginerName", dr);
            _maintenanceHistory.CauseOfFailure = DatabaseHelper.GetString("CauseOfFailure", dr);
            _maintenanceHistory.MaintainedParts = DatabaseHelper.GetString("MaintainedParts", dr);
            _maintenanceHistory.ActionTaken = DatabaseHelper.GetString("ActionTaken", dr);
            _maintenanceHistory.EnginerComment = DatabaseHelper.GetString("EnginerComment", dr);
            _maintenanceHistory.MaintainedDate = DatabaseHelper.GetDateTime("MaintainedDate", dr);

            return _maintenanceHistory;
        }

    }
}