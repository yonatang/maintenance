﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Maintenance;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.DataAccess.Maintenance
{
    public class PreventiveMaintenanceDao : BaseDao
    {
        public PreventiveMaintenance GetPreventiveMaintenanceById(int PreventiveMaintenanceId)
        {
            string sql = "SELECT PreventiveMaintenance.*, Site.SiteName, InstrumentLookup.InstrumentName "+
                         " FROM PreventiveMaintenance left JOIN " +

                         " Schedule ON PreventiveMaintenance.ScheduleId = Schedule.Id left JOIN " +
                         " Site ON Schedule.SiteId = Site.Id left JOIN  " +
                         " Instrument ON PreventiveMaintenance.InstrumentId = Instrument.Id left JOIN " +
                         " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id " +
                         " where PreventiveMaintenance.Id = @PreventiveMaintenanceId" +
   
                       " SELECT Spareparts.*,SparepartType.Name AS SparePartName FROM Spareparts" +
                       " LEFT JOIN SparepartType on Spareparts.SparepartId = SparepartType.Id WHERE Spareparts.PreventiveMaintenanceId = @PreventiveMaintenanceId";
                      

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@PreventiveMaintenanceId", cm, PreventiveMaintenanceId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetPreventiveMaintenance(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static PreventiveMaintenance GetPreventiveMaintenancelist(SqlDataReader dr)
        {
            PreventiveMaintenance preventive = new PreventiveMaintenance
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                PreventiveActionTaken = DatabaseHelper.GetString("PreventiveActionTaken", dr),
                PerformanceTestsDone = DatabaseHelper.GetBoolean("PerformanceTestsDone", dr),
                EquipmentFullyFunctional = DatabaseHelper.GetBoolean("EquipmentFullyFunctional", dr),
                EngineerComments = DatabaseHelper.GetString("EngineerComments", dr),
                FollowUpRequired = DatabaseHelper.GetBoolean("FollowUpRequired", dr),

                NextVistScheduledDate = DatabaseHelper.GetDateTime("NextVistScheduledDate", dr),
                InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr),
                ScheduleId = DatabaseHelper.GetInt32("ScheduleId", dr),
                Date = DatabaseHelper.GetDateTime("Date", dr),
                EnginerId = DatabaseHelper.GetInt32("EnginerId", dr),

                DailyIQC = DatabaseHelper.GetString("DailyIQC", dr),
                WeeklyIQC = DatabaseHelper.GetString("WeeklyIQC", dr),
                MonthlyIQC = DatabaseHelper.GetString("MonthlyIQC", dr),
                SiteName = DatabaseHelper.GetString("SiteName", dr),
                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr)
            };
            return preventive;
        }

        private static PreventiveMaintenance GetPreventiveMaintenance(SqlDataReader dr)
        {
            PreventiveMaintenance preventive = new PreventiveMaintenance
            {

                Id = DatabaseHelper.GetInt32("Id", dr),
                PreventiveActionTaken = DatabaseHelper.GetString("PreventiveActionTaken", dr),
                PerformanceTestsDone = DatabaseHelper.GetBoolean("PerformanceTestsDone", dr),
                EquipmentFullyFunctional = DatabaseHelper.GetBoolean("EquipmentFullyFunctional", dr),
                EngineerComments = DatabaseHelper.GetString("EngineerComments", dr),
                FollowUpRequired = DatabaseHelper.GetBoolean("FollowUpRequired", dr),
                NextVistScheduledDate = DatabaseHelper.GetDateTime("NextVistScheduledDate", dr),
                InstrumentId = DatabaseHelper.GetInt32("InstrumentId", dr),
                ScheduleId = DatabaseHelper.GetInt32("ScheduleId", dr),
                Date = DatabaseHelper.GetDateTime("Date", dr),
                EnginerId = DatabaseHelper.GetInt32("EnginerId", dr),
                DailyIQC = DatabaseHelper.GetString("DailyIQC", dr),
                WeeklyIQC = DatabaseHelper.GetString("WeeklyIQC", dr),
                MonthlyIQC = DatabaseHelper.GetString("MonthlyIQC", dr),
                SiteName = DatabaseHelper.GetString("SiteName", dr),
                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
               
            };
            dr.NextResult();
            while (dr.Read())
            {
                Spareparts spares = new Spareparts()
                {
                    Id = DatabaseHelper.GetInt32("Id", dr),
                    CurativeMaintenanceId = DatabaseHelper.GetInt32("CurativeMaintenanceId", dr),
                    PreventiveMaintenanceId = DatabaseHelper.GetInt32("PreventiveMaintenanceId", dr),
                    SparepartId = DatabaseHelper.GetInt32("SparepartId", dr),
                    Quantity = DatabaseHelper.GetInt32("Quantity", dr),
                    SparePartName = DatabaseHelper.GetString("SparePartName", dr)
                };
                preventive.Sapres.Add(spares);
            }

            return preventive;
        }

        private static void SetPreventiveMaintenance(SqlCommand cm, PreventiveMaintenance preventive)
        {

            DatabaseHelper.InsertStringVarCharParam("PreventiveActionTaken", cm, preventive.PreventiveActionTaken);
            DatabaseHelper.InsertBooleanParam("PerformanceTestsDone", cm, preventive.PerformanceTestsDone);
            DatabaseHelper.InsertBooleanParam("EquipmentFullyFunctional", cm, preventive.EquipmentFullyFunctional);
            DatabaseHelper.InsertStringVarCharParam("EngineerComments", cm, preventive.EngineerComments);
            DatabaseHelper.InsertBooleanParam("FollowUpRequired", cm, preventive.FollowUpRequired);
            DatabaseHelper.InsertDateTimeParam("NextVistScheduledDate", cm, preventive.NextVistScheduledDate);
            DatabaseHelper.InsertInt32Param("InstrumentId", cm, preventive.InstrumentId);
            DatabaseHelper.InsertInt32Param("ScheduleId", cm, preventive.ScheduleId);
            DatabaseHelper.InsertDateTimeParam("Date", cm, preventive.Date);
            DatabaseHelper.InsertInt32Param("EnginerId", cm, preventive.EnginerId);
            DatabaseHelper.InsertStringVarCharParam("DailyIQC", cm, preventive.DailyIQC);
            DatabaseHelper.InsertStringVarCharParam("WeeklyIQC", cm, preventive.WeeklyIQC);
            DatabaseHelper.InsertStringVarCharParam("MonthlyIQC", cm, preventive.MonthlyIQC);

        }

        public void Save(PreventiveMaintenance preventive)
        {
            string sql = "INSERT INTO PreventiveMaintenance(PreventiveActionTaken, PerformanceTestsDone, EquipmentFullyFunctional, EngineerComments, FollowUpRequired, NextVistScheduledDate, InstrumentId, ScheduleId, Date, EnginerId,DailyIQC,WeeklyIQC,MonthlyIQC) "
                        + " VALUES (@PreventiveActionTaken, @PerformanceTestsDone, @EquipmentFullyFunctional, @EngineerComments, @FollowUpRequired, @NextVistScheduledDate, @InstrumentId, @ScheduleId, @Date, @EnginerId,@DailyIQC, @WeeklyIQC, @MonthlyIQC) SELECT @@identity";
            ProblemDao _problem = new ProblemDao();
            SqlTransaction tr = DefaultConnection.BeginTransaction();
            try
            {
                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                {
                    SetPreventiveMaintenance(cm, preventive);
                    if (preventive.EquipmentFullyFunctional == true)
                    {
                        _problem.UpdateStatus("PreventiveMaintenanceForSchedulePerformed", (int)MaintenanceStatus.PreventiveMaintenanceChangeScheduleToPerformed, preventive.ScheduleId, "", tr);
                    }
                    preventive.Id = int.Parse(cm.ExecuteScalar().ToString());
                    tr.Commit();
                }
            }
            catch (SqlException ex)
            {
                tr.Rollback();
            }

        }

        public void Update(PreventiveMaintenance preventive)
        {
            string sql = "Update PreventiveMaintenance SET PreventiveActionTaken=@PreventiveActionTaken, PerformanceTestsDone=@PerformanceTestsDone, EquipmentFullyFunctional=@EquipmentFullyFunctional, EngineerComments=@EngineerComments, FollowUpRequired=@FollowUpRequired, NextVistScheduledDate=@NextVistScheduledDate, InstrumentId=@InstrumentId, ScheduleId=@ScheduleId, Date=@Date, EnginerId=@EnginerId,DailyIQC=@DailyIQC, WeeklyIQC=@WeeklyIQC, MonthlyIQC=@MonthlyIQC  where Id = @preventiveid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@preventiveid", cm, preventive.Id);
                SetPreventiveMaintenance(cm, preventive);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int preventiveid)
        {
            string sql = "Delete PreventiveMaintenance where Id = @preventiveid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@preventiveid", cm, preventiveid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<PreventiveMaintenance> GetListOfPreventiveMaintenance(int InstrumentID)
        {
            string sql;
            if (InstrumentID != 0)
                sql = "SELECT PreventiveMaintenance.* FROM PreventiveMaintenance WHERE @InstrumentID=PreventiveMaintenance.InstrumentID";
            else
                sql = "SELECT PreventiveMaintenance.* FROM PreventiveMaintenance";


            IList<PreventiveMaintenance> lstPreventive = new List<PreventiveMaintenance>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
           
                DatabaseHelper.InsertInt32Param("@InstrumentID", cm, InstrumentID);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstPreventive.Add(GetPreventiveMaintenancelist(dr));
                        }
                    }
                }
            }
            return lstPreventive;
        }

        public IList<PreventiveMaintenance> GetPreventiveMaintetanceBySiteInsCategoryId(int SiteId, int InstrumentCat, int userId)
        {
            string sql = "";
            if (SiteId != 0 && InstrumentCat != 0 && userId !=0)
            {
                sql = " SELECT PreventiveMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +
                          
                           " FROM PreventiveMaintenance left JOIN " +

                           " Schedule ON PreventiveMaintenance.ScheduleId = Schedule.Id left JOIN " +
                           " Site ON Schedule.SiteId = Site.Id left JOIN " +
                           " Instrument ON PreventiveMaintenance.InstrumentId = Instrument.Id left JOIN " +
                           " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                           " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                           " WHERE InstrumentCatagory.Id = @InstrumentCat AND Schedule.SiteId = @SiteId and PreventiveMaintenance.EnginerId = @UserId";
            }
            else if (SiteId == 0 && InstrumentCat != 0 && userId == 0)
            {
                sql = " SELECT PreventiveMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +

                            " FROM PreventiveMaintenance left JOIN " +

                            " Schedule ON PreventiveMaintenance.ScheduleId = Schedule.Id left JOIN " +
                            " Site ON Schedule.SiteId = Site.Id left JOIN " +
                            " Instrument ON PreventiveMaintenance.InstrumentId = Instrument.Id left JOIN " +
                            " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                            " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                            " WHERE InstrumentCatagory.Id = @InstrumentCat";
            }
            else if (SiteId == 0 && InstrumentCat == 0 && userId == 0)
            {
                sql = " SELECT PreventiveMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +

                            " FROM PreventiveMaintenance left JOIN " +

                            " Schedule ON PreventiveMaintenance.ScheduleId = Schedule.Id left JOIN " +
                            " Site ON Schedule.SiteId = Site.Id left JOIN " +
                            " Instrument ON PreventiveMaintenance.InstrumentId = Instrument.Id left JOIN " +
                            " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                            " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id ";
            }
            else if (SiteId != 0 && InstrumentCat == 0 && userId == 0)
            {
                sql = " SELECT PreventiveMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +

                             " FROM PreventiveMaintenance left JOIN " +

                             " Schedule ON PreventiveMaintenance.ScheduleId = Schedule.Id left JOIN " +
                             " Site ON Schedule.SiteId = Site.Id left JOIN " +
                             " Instrument ON PreventiveMaintenance.InstrumentId = Instrument.Id left JOIN " +
                             " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                             " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                             " WHERE Schedule.SiteId = @SiteId";
            }
            else if (SiteId != 0 && InstrumentCat == 0 && userId != 0)
            {
                sql = " SELECT PreventiveMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +

                           " FROM PreventiveMaintenance left JOIN " +

                           " Schedule ON PreventiveMaintenance.ScheduleId = Schedule.Id left JOIN " +
                           " Site ON Schedule.SiteId = Site.Id left JOIN " +
                           " Instrument ON PreventiveMaintenance.InstrumentId = Instrument.Id left JOIN " +
                           " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                           " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                           " WHERE Schedule.SiteId = @SiteId and PreventiveMaintenance.EnginerId = @UserId";
            }
            else if (SiteId == 0 && InstrumentCat != 0 && userId != 0)
            {
                sql = " SELECT PreventiveMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +

                           " FROM PreventiveMaintenance left JOIN " +

                           " Schedule ON PreventiveMaintenance.ScheduleId = Schedule.Id left JOIN " +
                           " Site ON Schedule.SiteId = Site.Id left JOIN " +
                           " Instrument ON PreventiveMaintenance.InstrumentId = Instrument.Id left JOIN " +
                           " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                           " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                           " WHERE InstrumentCatagory.Id = @InstrumentCat AND PreventiveMaintenance.EnginerId = @UserId";
            }
            else if (SiteId == 0 && InstrumentCat == 0 && userId != 0)
            {
                sql = " SELECT PreventiveMaintenance.*, dbo.Site.SiteName, InstrumentLookup.InstrumentName " +

                           " FROM PreventiveMaintenance left JOIN " +

                           " Schedule ON PreventiveMaintenance.ScheduleId = Schedule.Id left JOIN " +
                           " Site ON Schedule.SiteId = Site.Id left JOIN " +
                           " Instrument ON PreventiveMaintenance.InstrumentId = Instrument.Id left JOIN " +
                           " InstrumentLookup ON Instrument.InstrumentName_Id = InstrumentLookup.Id left JOIN " +
                           " InstrumentCatagory ON InstrumentLookup.InstrumentCategoryId = InstrumentCatagory.Id " +
                           " WHERE  PreventiveMaintenance.EnginerId = @UserId";
            }
            IList<PreventiveMaintenance> lstPreventiveMaintenance = new List<PreventiveMaintenance>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (SiteId != 0)
                    DatabaseHelper.InsertInt32Param("@SiteId", cm, SiteId);
                if (InstrumentCat != 0)
                    DatabaseHelper.InsertInt32Param("@InstrumentCat", cm, InstrumentCat);
                if (userId != 0)
                    DatabaseHelper.InsertInt32Param("@UserId", cm, userId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstPreventiveMaintenance.Add(GetPreventiveMaintenancelist(dr));
                        }
                    }
                }


            }
            return lstPreventiveMaintenance;
        }



    }
}
