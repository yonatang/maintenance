﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Enums;


namespace Chai.Maintenance.DataAccess.Admin
{
    public class NodeDao : BaseDao
    {
        public Node GetNodeById(int nodeid)
        {
            string sql = "SELECT AspxPageTypeId, ViewType, Title, FolderPath, ImagePath, Description  "
                + "FROM Node where AspxPageTypeId = @NodeId  "
                + "SELECT NodeRoleId, ViewAllowed, EditAllowed, Role.RoleId as role_id, Name, PermissionLevel "
                + "FROM NodeRole INNER JOIN Role ON NodeRole.RoleId = Role.RoleId WHERE NodeRole.NodeId = @NodeId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@NodeId", cm, nodeid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetNode(dr);
                        }
                    }
                    dr.Close();
                }
            }
            return null;
        }

        private static Node GetNode(SqlDataReader dr)
        {
            Node node = new Node
            {
                Id = DatabaseHelper.GetInt32("AspxPageTypeId", dr),
                Title = DatabaseHelper.GetString("Title", dr),
                FolderPath = DatabaseHelper.GetString("FolderPath", dr),
                ImagePath = DatabaseHelper.GetString("ImagePath", dr),
                Description = DatabaseHelper.GetString("Description", dr),
            };
            node.ViewType = (PageViewType)Enum.ToObject(typeof(PageViewType), DatabaseHelper.GetInt32("ViewType", dr));

            dr.NextResult();
            while (dr.Read())
            {
                NodePermission np = new NodePermission
                {
                    Id = DatabaseHelper.GetInt32("NodeRoleId", dr),
                    NodeId = node.Id,
                    EditAllowed = DatabaseHelper.GetBoolean("ViewAllowed", dr),
                    ViewAllowed = DatabaseHelper.GetBoolean("EditAllowed", dr)
                };

                np.Role = new Role
                {
                    Id = DatabaseHelper.GetInt32("role_id", dr),
                    Name = DatabaseHelper.GetString("Name", dr),
                    PermissionLevel = DatabaseHelper.GetInt32("PermissionLevel", dr)
                };

                node.NodePermissions.Add(np);
            }
           
            return node;
        }
        private static void SetNode(SqlCommand cm, Node node)
        {
            DatabaseHelper.InsertInt32Param("@ASPXPageTypeId", cm, (int)node.Id);
            DatabaseHelper.InsertInt32Param("@ViewType", cm, (int)node.ViewType);
            DatabaseHelper.InsertStringNVarCharParam("@Title", cm, node.Title);
            DatabaseHelper.InsertStringNVarCharParam("@FolderPath", cm, node.FolderPath);
            DatabaseHelper.InsertStringNVarCharParam("@ImagePath", cm, node.ImagePath);
            DatabaseHelper.InsertStringNVarCharParam("@Description", cm, node.Description);
        }

        public void Save(Node node, SqlTransaction sqltransaction)
        {
            //string sql = "INSERT INTO Node([AspxPageTypeId], [ViewType], [Title], [FolderPath], [ImagePath], "
            //        +"[Description]) VALUES(@AspxPageTypeId, @ViewType, @Title, @FolderPath, @ImagePath, @Description)";

            string sql = "INSERT INTO Node([AspxPageTypeId], [ViewType], [Title], [FolderPath], [ImagePath],  [Description])"
                         + "VALUES(@ASPXPageTypeId, @ViewType, @Title, @FolderPath, @ImagePath, @Description)";
                      // + "SET @AspxPageTypeId = @@Scop_Identity()";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, sqltransaction))
            {
                //DatabaseHelper.InsertInt32Param("AspxPageTypeId", cm, node.Id);
                SetNode(cm, node);
                cm.ExecuteNonQuery();
                foreach (NodePermission np in node.NodePermissions)
                {
                    np.NodeId = node.Id;
                    SaveNodePermission(np, sqltransaction);
                }
            }
        }

        private void SaveNodePermission(NodePermission np, SqlTransaction sqltransaction)
        {
            string sql="INSERT INTO [NodeRole]([NodeId], [RoleId], [ViewAllowed], [EditAllowed])"
                    + "VALUES(@NodeId, @RoleId, @ViewAllowed, @EditAllowed) SELECT @@identity";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, sqltransaction))
            {
                SetNodePermission(cm, np);
                 np.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }
        
        private void SetNodePermission(SqlCommand cm, NodePermission np)
        {
            DatabaseHelper.InsertInt32Param("@NodeId", cm, np.NodeId);
            DatabaseHelper.InsertInt32Param("@RoleId",cm,np.Role.Id);
            DatabaseHelper.InsertBooleanParam("@ViewAllowed",cm,np.ViewAllowed);
            DatabaseHelper.InsertBooleanParam("@EditAllowed", cm, np.EditAllowed);
        }

        public void Update(Node node, SqlTransaction sqltransaction)
        {
            string sql = "UPDATE [Node] SET [ViewType] = @ViewType, [Title] = @Title, [FolderPath] = @FolderPath, "
            + "[ImagePath] = @ImagePath, [Description] = @Description WHERE AspxPageTypeId = @NodeId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, sqltransaction))
            {
                DatabaseHelper.InsertInt32Param("@NodeId", cm, node.Id);
                SetNode(cm, node);
                cm.ExecuteNonQuery();
                UpdateNodePermission(node, sqltransaction);
            }
        }

        private void UpdateNodePermission(Node node, SqlTransaction sqltransaction)
        {
            string sql = "DELETE FROM NodeRole WHERE NodeRoleId = @NodeRoleId";

            foreach (NodePermission np in node.NodePermissions)
            {
                if (np.IsDirty && np.Id > 0)
                {
                    using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, sqltransaction))
                    {
                        DatabaseHelper.InsertInt32Param("@NodeRoleId", cm, np.Id);
                        cm.ExecuteNonQuery();
                    }
                }
                else if (np.IsNew() && !np.IsDirty)
                {
                    np.NodeId = node.Id;
                    SaveNodePermission(np, sqltransaction);
                }
            }
        }

        public void Delete(Node node)
        {
            string sql = "Delete Node where AspxPageTypeId = @NodeId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@NodeId", cm, node.Id);
                cm.ExecuteNonQuery();
            }
        }

        public IList<Node> GetListOfAllNodes()
        {
            string sql = "SELECT AspxPageTypeId FROM Node";
            IList<Node> lstnode = new List<Node>();
            IList<int> pageid = new List<int>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            pageid.Add(DatabaseHelper.GetInt32("AspxPageTypeId", dr));
                        }
                    }
                    dr.Close();
                }
            }

            foreach (int id in pageid)
            {
                lstnode.Add(GetNodeById(id));
            }


            return lstnode;
        }

        public IList<Node> GetListOfNodes(PageViewType viewtype)
        {
            string sql = "SELECT AspxPageTypeId FROM Node WHERE ViewType = @Vtype";
            IList<Node> lstnode = new List<Node>();
            IList<int> pageid = new List<int>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@Vtype", cm, (int)viewtype);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            pageid.Add(DatabaseHelper.GetInt32("AspxPageTypeId", dr));                            
                        }
                    }
                    dr.Close();
                }
            }

            foreach (int id in pageid)
            {
                lstnode.Add(GetNodeById(id));
            }

            return lstnode;
        }
    }
}
