﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Enums;
using Chai.Maintenance.CoreDomain;

namespace Chai.Maintenance.DataAccess.Admin
{
    public class TabDao :BaseDao
    {
        private NodeDao _nodeDao;

        public TabDao()
        {
            _nodeDao = new NodeDao();
        }

        public MenuTab GetMenuTab(TabType ttype)
        {
            MenuTab tab = new MenuTab(ttype);
            tab.ActionSectionNodes = GetListOfTabNode(tab.Tab, MenuSectionType.Action);
            tab.FindSectionNodes = GetListOfTabNode(tab.Tab, MenuSectionType.Find);
            tab.DropdownSectionNodes = GetListOfTabNode(tab.Tab, MenuSectionType.Dropdown);

            return tab;
        }

        public MenuTab GetMenuTab(int tabid)
        {
            return GetMenuTab((TabType)Enum.ToObject(typeof(TabType), tabid));
        }

        public void SaveOrUpdateMenuTab(MenuTab mtab, SqlTransaction sqltransaction)
        {
            SaveUpdateDeleteTabNode(mtab.ActionSectionNodes, sqltransaction);
            SaveUpdateDeleteTabNode(mtab.FindSectionNodes, sqltransaction);
            SaveUpdateDeleteTabNode(mtab.DropdownSectionNodes, sqltransaction);
        }

        private void SaveUpdateDeleteTabNode(IList<TabNode> tnodes, SqlTransaction sqltransaction)
        {
            foreach (TabNode tn in tnodes)
            {
                if (tn.IsDirty)
                {
                    if (!tn.IsNew())
                    {
                        Delete(tn, sqltransaction);
                    }
                }
                else if (tn.IsNew())
                    Save(tn, sqltransaction);
                else
                    Update(tn, sqltransaction);
            }
        }

        public TabNode GetTabNodeById(int tabnodeid)
        {
            string sql = "SELECT [TabNodeId], [TabId], [Section], [NodeId], [Position] FROM TabNode WHERE TabNodeId = @Id";
            TabNode tnode = null;

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@Id", cm, tabnodeid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            tnode = GetTabNode(dr);
                        }
                    }

                    dr.Close();
                }
                
            }

            if(tnode != null)
                tnode.Node = _nodeDao.GetNodeById(tnode.NodeId);
            
            return tnode;
        }

        private TabNode GetTabNode(SqlDataReader dr)
        {
            TabNode tnode = new TabNode
            {
                Id = DatabaseHelper.GetInt32("TabNodeId", dr),
                Position = DatabaseHelper.GetInt32("Position", dr)
            };
            tnode.Tab = (TabType)Enum.ToObject(typeof(TabType), DatabaseHelper.GetInt32("TabId", dr));
            tnode.Section = (MenuSectionType)Enum.Parse(typeof(MenuSectionType), DatabaseHelper.GetString("Section", dr));
            tnode.NodeId = DatabaseHelper.GetInt32("NodeId", dr);
            
            return tnode;
        }
        private void SetTabNode(SqlCommand cm, TabNode tnode)
        {
            DatabaseHelper.InsertInt32Param("@TabId", cm, (int)tnode.Tab);
            DatabaseHelper.InsertStringNVarCharParam("@Section", cm, tnode.Section.ToString());
            DatabaseHelper.InsertInt32Param("@Position", cm, tnode.Position);
            DatabaseHelper.InsertInt32Param("@NodeId", cm, tnode.Node.Id);
        }

        public void Save(TabNode tabnode, SqlTransaction sqltransaction)
        {
            string sql = "INSERT INTO TabNode([TabId], [Section], [NodeId], [Position])"
                    + " VALUES(@TabId, @Section, @NodeId, @Position) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, sqltransaction))
            {
                SetTabNode(cm, tabnode);
                tabnode.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(TabNode tnode, SqlTransaction sqltransaction)
        {
            string sql = "UPDATE TabNode SET [TabId] = @TabId, [Section] = @Section, [NodeId] = @NodeId, "
                + "[Position] = @Position WHERE TabNodeId = @Id";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, sqltransaction))
            {
                DatabaseHelper.InsertInt32Param("@Id", cm, tnode.Id);
                SetTabNode(cm, tnode);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(TabNode tnode, SqlTransaction sqltransaction)
        {
            string sql = "Delete TabNode where TabNodeId = @Id";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, sqltransaction))
            {
                DatabaseHelper.InsertInt32Param("@Id", cm, tnode.Id);
                cm.ExecuteNonQuery();
            }
        }
              

        public IList<TabNode> GetListOfTabNode(TabType tab, MenuSectionType section)
        {
            string sql = "SELECT [TabNodeId], [TabId], [Section], [NodeId], [Position] FROM TabNode "
            + "WHERE TabId = @TabId order by Position ";// ;and Section = @Section ";
            IList<TabNode> lstnode = new List<TabNode>();
            
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@TabId", cm, (int)tab);
               // DatabaseHelper.InsertStringNVarCharParam("@Section", cm, section.ToString());

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstnode.Add(GetTabNode(dr));
                        }
                    }
                }
            }

            foreach (TabNode t in lstnode)
            {
                t.Node = _nodeDao.GetNodeById(t.NodeId);
            }
            return lstnode;
        }
    }
}
