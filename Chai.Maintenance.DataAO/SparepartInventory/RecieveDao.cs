﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.SparepartInventory;

namespace Chai.Maintenance.DataAccess.SparepartInventory
{
   public class RecieveDao:BaseDao
    {
        public Recieve GetRecieveById(int ReciveId)
        {
            string sql = "SELECT Id, ReceiptNo,ReceiveDate,Vendor_Id,VendorAgent,Source_of_Finanance,PurchaseNo,TendorNo,LetterNo,SourceDescription,ToolKeeper_Id,Region_Id,IsPosted FROM ReceiveHeader where Id = @recieveid" +

                " SELECT ReceiveDetail.Id,ReceiveDetail.Instrumentlk_Id, ReceiveDetail.Receive_Id,ReceiveDetail.Manfacturer_Id,ReceiveDetail.Sparepart_Id,ReceiveDetail.Qty,ReceiveDetail.UnitPrice,ReceiveDetail.TotalPrice,Manufacturer.Name as ManufacturerName,SparepartType.Name as SparepartName,InstrumentLookup.InstrumentName FROM ReceiveDetail" +
                   " Left Join Manufacturer on Manufacturer.Id =ReceiveDetail.Manfacturer_Id " +
                   " Left join SparepartType on Spareparttype.Id = ReceiveDetail.Sparepart_Id " +
                   " Left join InstrumentLookup on InstrumentLookup.Id =ReceiveDetail.Instrumentlk_Id where (Receive_Id = @recieveid)"; 

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@recieveid", cm, ReciveId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetRecieve(dr);
                        }
                    }
                }
            }
            return null;
        }
        private static Recieve GetRecievelist(SqlDataReader dr)
        {
            Recieve recieve = new Recieve
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                ReceiptNo = DatabaseHelper.GetString("ReceiptNo", dr),
                ReceiveDate = DatabaseHelper.GetDateTime("ReceiveDate", dr),
                PurchaseNo = DatabaseHelper.GetString("PurchaseNo", dr),
                LetterNo = DatabaseHelper.GetString("LetterNo", dr),
                TendorNo = DatabaseHelper.GetString("TendorNo", dr),
                SourceDescription = DatabaseHelper.GetString("SourceDescription", dr),
                SourceOfFinanance = DatabaseHelper.GetString("Source_of_Finanance", dr),
                ToolkeeperId = DatabaseHelper.GetInt32("ToolKeeper_Id", dr),
                VendorAgent = DatabaseHelper.GetString("VendorAgent", dr),
                VendorId = DatabaseHelper.GetInt32("Vendor_Id", dr),
                RegionId = DatabaseHelper.GetInt32("Region_Id", dr),
                IsPosted = DatabaseHelper.GetBoolean("IsPosted", dr)
            };
            return recieve;
        }
        private static Recieve GetRecieve(SqlDataReader dr)
        {
            Recieve recieve = new Recieve
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                ReceiptNo = DatabaseHelper.GetString("ReceiptNo", dr),
                ReceiveDate = DatabaseHelper.GetDateTime("ReceiveDate", dr),
                PurchaseNo = DatabaseHelper.GetString("PurchaseNo", dr),
                LetterNo = DatabaseHelper.GetString("LetterNo", dr),
                TendorNo = DatabaseHelper.GetString("TendorNo", dr),
                SourceDescription = DatabaseHelper.GetString("SourceDescription", dr),
                SourceOfFinanance = DatabaseHelper.GetString("Source_of_Finanance", dr),
                ToolkeeperId = DatabaseHelper.GetInt32("ToolKeeper_Id", dr),
                VendorAgent = DatabaseHelper.GetString("VendorAgent", dr),
                VendorId = DatabaseHelper.GetInt32("Vendor_Id", dr),
                RegionId = DatabaseHelper.GetInt32("Region_Id", dr),
                IsPosted = DatabaseHelper.GetBoolean("IsPosted", dr)
            };

            dr.NextResult();
            while (dr.Read())
            {
                RecieveItemDetail ReciveDetail = new RecieveItemDetail()// { Id = DatabaseHelper.GetInt32("UserRoleId", dr), UserId = user.Id };
             
                {
                 Id = DatabaseHelper.GetInt32("Id", dr),
                ReceiveId = DatabaseHelper.GetInt32("Receive_Id", dr),
                ManfacturerId = DatabaseHelper.GetInt32("Manfacturer_Id", dr),
                SparepartId = DatabaseHelper.GetInt32("Sparepart_Id", dr),
                Qty = DatabaseHelper.GetInt32("Qty", dr),
                UnitPrice = DatabaseHelper.GetMoney("UnitPrice", dr),
                TotalPrice = DatabaseHelper.GetMoney("TotalPrice", dr),
                InstrumentId = DatabaseHelper.GetInt32("Instrumentlk_Id",dr),
                InstrumentName =DatabaseHelper.GetString("InstrumentName",dr),
                Manufacturer = DatabaseHelper.GetString("ManufacturerName",dr),
                SparepartName = DatabaseHelper.GetString("SparepartName",dr)
                };
                recieve.ItemDetail.Add(ReciveDetail);
            }
           
            return recieve;
        }

        private static void SetRecieve(SqlCommand cm, Recieve recieve)
        {
              DatabaseHelper.InsertStringVarCharParam("ReceiptNo",cm,recieve.ReceiptNo);
              DatabaseHelper.InsertDateTimeParam("ReceiveDate",cm,recieve.ReceiveDate); 
              DatabaseHelper.InsertStringVarCharParam("PurchaseNo",cm,recieve.PurchaseNo); 
              DatabaseHelper.InsertStringVarCharParam("LetterNo", cm,recieve.LetterNo);
              DatabaseHelper.InsertStringVarCharParam("TendorNo", cm,recieve.TendorNo);
              DatabaseHelper.InsertStringVarCharParam("SourceDescription",cm,recieve.SourceDescription); 
              DatabaseHelper.InsertStringVarCharParam("Source_of_Finanance",cm,recieve.SourceOfFinanance); 
              DatabaseHelper.InsertInt32Param("ToolKeeper_Id", cm,recieve.ToolkeeperId);
              DatabaseHelper.InsertStringVarCharParam("VendorAgent", cm,recieve.VendorAgent);
              DatabaseHelper.InsertInt32Param("Vendor_Id",cm,recieve.VendorId);
              DatabaseHelper.InsertInt32Param("Region_Id",cm,recieve.RegionId);
              DatabaseHelper.InsertBooleanParam("IsPosted", cm, recieve.IsPosted); 

        }

        public void Save(Recieve recieve)
        {
            string sql = "INSERT INTO ReceiveHeader(ReceiptNo,ReceiveDate,Vendor_Id,VendorAgent,Source_of_Finanance,PurchaseNo,TendorNo,LetterNo,SourceDescription,ToolKeeper_Id,Region_Id,IsPosted) VALUES (@ReceiptNo,@ReceiveDate,@Vendor_Id,@VendorAgent,@Source_of_Finanance,@PurchaseNo,@TendorNo,@LetterNo,@SourceDescription,@ToolKeeper_Id,@Region_Id,@IsPosted) SELECT @@identity";
          
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))

            {
                SetRecieve(cm, recieve);
                recieve.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
                
        }

        public void Update(Recieve recieve)
        {
            string sql = "Update ReceiveHeader SET ReceiptNo=@ReceiptNo,ReceiveDate=@ReceiveDate,Vendor_Id=@Vendor_Id,VendorAgent=@VendorAgent,Source_of_Finanance=@Source_of_Finanance,PurchaseNo=@PurchaseNo,TendorNo=@TendorNo,LetterNo=@LetterNo,SourceDescription=@SourceDescription,ToolKeeper_Id=@ToolKeeper_Id,Region_Id=@Region_Id,IsPosted=@IsPosted  where Id = @recieveid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@recieveid", cm, recieve.Id);
                SetRecieve(cm, recieve);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int recieveid)
        {
            string sql = "Delete ReceiveHeader where Id = @recieveid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@recieveid", cm, recieveid);
                cm.ExecuteNonQuery();
            }
        }
       
        public IList<Recieve> GetListOfRecieves(int selectedby,string value,string datefrom,string dateto,int regionid)
        {
            string sql;
            if (selectedby == 5)
                sql = "SELECT Id, ReceiptNo,ReceiveDate,Vendor_Id,VendorAgent,Source_of_Finanance,PurchaseNo,TendorNo,LetterNo,SourceDescription,ToolKeeper_Id,Region_Id,IsPosted FROM ReceiveHeader where Region_Id=@regionId and ReceiveDate between @DateFrom and @Dateto order by ReceiveDate DESC";
                
            else if(selectedby == 0)
                sql = "SELECT Id, ReceiptNo,ReceiveDate,Vendor_Id,VendorAgent,Source_of_Finanance,PurchaseNo,TendorNo,LetterNo,SourceDescription,ToolKeeper_Id,Region_Id,IsPosted FROM ReceiveHeader where Region_Id=@regionId order by ReceiveDate DESC";
            else
                sql = "SELECT Id, ReceiptNo,ReceiveDate,Vendor_Id,VendorAgent,Source_of_Finanance,PurchaseNo,TendorNo,LetterNo,SourceDescription,ToolKeeper_Id,Region_Id,IsPosted FROM ReceiveHeader where  Region_Id=@regionId and (PurchaseNo = @value or TendorNo =@value or LetterNo = @value or ReceiptNo=@value) order by ReceiveDate DESC";

            IList<Recieve> lstRecieve = new List<Recieve>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (selectedby == 5)
                {
                     DatabaseHelper.InsertStringVarCharParam("@DateFrom", cm, datefrom);
                    DatabaseHelper.InsertStringVarCharParam("@Dateto", cm, dateto);
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionid);
                }
                
                else if(selectedby !=5 && selectedby !=0)
 
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionid);
                   
                }
                else
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionid);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstRecieve.Add(GetRecievelist(dr));
                        }
                    }
                }
            }
            return lstRecieve;
        }
    }
}
