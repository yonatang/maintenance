﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.SparepartInventory;

namespace Chai.Maintenance.DataAccess.SparepartInventory
{
    public class ApprovalItemdetailDao : BaseDao
    {
        public ApprovalItemDetail GetApprovalItemDetailById(int detailId)
        {
            string sql = "SELECT Id,Instrumentlk_Id, Approval_Id,Sparepart_Id,RequestedQty,ApprovedQty,SparePartAvailableQty.Qty as StockQty,SparepartType.Name as SparepartName,InstrumentLookup.InstrumentName FROM ApprovalDetail" +
            " Left join SparepartType on Spareparttype.Id = ApprovalDetail.Sparepart_Id " +
            " Left join SparePartAvailableQty on SparePartAvailableQty.sparepart_Id = ApprovalDetail.Sparepart_Id" +
            " Left join InstrumentLookup on InstrumentLookup.Id =ApprovalDetail.Instrumentlk_Id where Id = @detailId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@detailId", cm, detailId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetApprovalItemDetail(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static ApprovalItemDetail GetApprovalItemDetail(SqlDataReader dr)
        {
            ApprovalItemDetail approveitemdetail = new ApprovalItemDetail
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                ApprovalId = DatabaseHelper.GetInt32("Approval_Id", dr),
                SparepartId = DatabaseHelper.GetInt32("Sparepart_Id", dr),
                RequestedQty = DatabaseHelper.GetInt32("RequestedQty", dr),
                ApprovedQty = DatabaseHelper.GetInt32("ApprovedQty", dr),
                InsrumentId = DatabaseHelper.GetInt32("Instrumentlk_Id", dr),
                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                SparepartName = DatabaseHelper.GetString("SparepartName", dr),
                StockQty = DatabaseHelper.GetInt32("StockQty", dr)
            };

            return approveitemdetail;
        }

        private static void SetApprovalDetail(SqlCommand cm, ApprovalItemDetail approvaldetail)
        {

            // DatabaseHelper.InsertInt32Param("Receive_Id", cm, recievedetail.ReceiveId);
           
            DatabaseHelper.InsertInt32Param("Sparepart_Id", cm, approvaldetail.SparepartId);
            DatabaseHelper.InsertInt32Param("RequestedQty", cm, approvaldetail.RequestedQty);
            DatabaseHelper.InsertInt32Param("ApprovedQty", cm, approvaldetail.ApprovedQty);
            DatabaseHelper.InsertInt32Param("Instrumentlk_Id", cm, approvaldetail.InsrumentId);
        }

        public void Save(ApprovalItemDetail approvaldetail, int approvalid)
        {
            string sql = "INSERT INTO ApprovalDetail(Approval_Id,Instrumentlk_Id,Sparepart_Id,RequestedQty,ApprovedQty) VALUES (@approvalid,@Instrumentlk_Id,@Sparepart_Id,@RequestedQty,@ApprovedQty) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@approvalid", cm, approvalid);
                SetApprovalDetail(cm, approvaldetail);
                approvaldetail.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(ApprovalItemDetail approvaldetail)
        {
            string sql = "Update ApprovalDetail SET Approval_Id=@Approval_Id,Instrumentlk_Id=@Instrumentlk_Id,Sparepart_Id=@Sparepart_Id,RequestedQty=@RequestedQty,ApprovedQty=@ApprovedQty  where Id = @approvaldetailid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@approvaldetailid", cm, approvaldetail.Id);
                DatabaseHelper.InsertInt32Param("@Approval_Id", cm, approvaldetail.ApprovalId);
                SetApprovalDetail(cm, approvaldetail);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int approvaldetailid)
        {
            string sql = "Delete ApprovalDetail where Id = @approvaldetailid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@approvaldetailid", cm, approvaldetailid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<ApprovalItemDetail> GetListOfApprovalDetails(int approvaid,int regionId)
        {
            string sql;

            sql = " SELECT ApprovalDetail.Id,ApprovalDetail.Instrumentlk_Id, SparePartAvailableQty.Qty as StockQty,ApprovalDetail.Approval_Id,ApprovalDetail.Sparepart_Id,ApprovalDetail.RequestedQty,ApprovalDetail.ApprovedQty,SparepartType.Name as SparepartName,InstrumentLookup.InstrumentName FROM ApprovalDetail" +
                  " Left join SparepartType on Spareparttype.Id = ApprovalDetail.Sparepart_Id " +
                   " Left join SparePartAvailableQty on SparePartAvailableQty.sparepart_Id = ApprovalDetail.Sparepart_Id" +
                  " Left join InstrumentLookup on InstrumentLookup.Id = ApprovalDetail.Instrumentlk_Id where Approval_Id = @approvaid AND SparePartAvailableQty.Region_Id =@regionId";




            IList<ApprovalItemDetail> lstRecievedetail = new List<ApprovalItemDetail>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {

                DatabaseHelper.InsertInt32Param("@approvaid", cm, approvaid);
                DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstRecievedetail.Add(GetApprovalItemDetail(dr));
                        }
                    }
                }
            }
            return lstRecievedetail;
        }
    }
}
