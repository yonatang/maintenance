﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.DataAccess.Maintenance;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.DataAccess.SparepartInventory
{
    public class IssueDao :BaseDao
    {
        private IssueItemDetailDao _issueitemdetaildao;
        private IssueItemDetail _issueitemdetail;
        public Issue GetIssueById(int issueId,int regionId)
        {
            string sql = "SELECT Id, IssueNo,IssueDate,ApprovalId,ToolkeeperId,IssuedFor,Region_Id,Approver,IsPosted FROM IssueHeader where Id = @issueId" +

                " SELECT IssueDetail.Id,IssueDetail.Instrumentlk_Id, IssueDetail.Issue_Id,SparePartAvailableQty.Qty as StockQty,IssueDetail.Sparepart_Id,IssueDetail.ApprovedQty,IssueDetail.IssuedQty,SparepartType.Name as SparepartName,InstrumentLookup.InstrumentName FROM IssueDetail" +
                    " Left join SparepartType on Spareparttype.Id = IssueDetail.Sparepart_Id " +
                    " Left join SparePartAvailableQty on SparePartAvailableQty.sparepart_Id = IssueDetail.Sparepart_Id" +
                    " Left join InstrumentLookup on InstrumentLookup.Id = IssueDetail.Instrumentlk_Id where (IssueDetail.Issue_Id = @issueId) AND (SparePartAvailableQty.Region_Id = @regionId)";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@issueId", cm, issueId);
                DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetIssue(dr);
                        }
                    }
                }
            }
            return null;
        }
        private static Issue GetIssuelist(SqlDataReader dr)
        {
            Issue issue = new Issue
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                IssueNo = DatabaseHelper.GetString("IssueNo", dr),
                IssueDate = DatabaseHelper.GetDateTime("IssueDate", dr),
                ApprovalId = DatabaseHelper.GetInt32("ApprovalId", dr),
                ToolkeeperId = DatabaseHelper.GetInt32("ToolkeeperId", dr),
                RegionId = DatabaseHelper.GetInt32("Region_Id", dr),
                IssuedFor = DatabaseHelper.GetInt32("IssuedFor", dr),
                Approver = DatabaseHelper.GetInt32("Approver", dr),
                IsPosted=DatabaseHelper.GetBoolean("IsPosted",dr)
            };
            return issue;
        }
        private static Issue GetIssue(SqlDataReader dr)
        {
            Issue issue = new Issue
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                IssueNo = DatabaseHelper.GetString("IssueNo", dr),
                IssueDate = DatabaseHelper.GetDateTime("IssueDate", dr),
                ApprovalId = DatabaseHelper.GetInt32("ApprovalId", dr),
                ToolkeeperId = DatabaseHelper.GetInt32("ToolkeeperId", dr),
                RegionId = DatabaseHelper.GetInt32("Region_Id", dr),
                IssuedFor = DatabaseHelper.GetInt32("IssuedFor", dr),
                Approver = DatabaseHelper.GetInt32("Approver",dr),
                IsPosted=DatabaseHelper.GetBoolean("IsPosted",dr)
            };

            dr.NextResult();
            while (dr.Read())
            {
                IssueItemDetail issuedetail = new IssueItemDetail()// { Id = DatabaseHelper.GetInt32("UserRoleId", dr), UserId = user.Id };

                {
                    Id = DatabaseHelper.GetInt32("Id", dr),
                    IssueId = DatabaseHelper.GetInt32("Issue_Id", dr),
                    SparepartId = DatabaseHelper.GetInt32("Sparepart_Id", dr),
                    IssuedQty = DatabaseHelper.GetInt32("IssuedQty", dr),
                    ApprovedQty = DatabaseHelper.GetInt32("ApprovedQty", dr),
                    InstrumentId = DatabaseHelper.GetInt32("Instrumentlk_Id", dr),
                    InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                    SparepartName = DatabaseHelper.GetString("SparepartName", dr),
                    StockQty = DatabaseHelper.GetInt32("StockQty", dr)
                };
                issue.ItemDetail.Add(issuedetail);
            }

            return issue;
        }

        private static void SetIssue(SqlCommand cm, Issue issue)
        {
            DatabaseHelper.InsertStringVarCharParam("IssueNo", cm, issue.IssueNo);
            DatabaseHelper.InsertDateTimeParam("IssueDate", cm, issue.IssueDate);
            DatabaseHelper.InsertInt32Param("Region_Id", cm, issue.RegionId);
            DatabaseHelper.InsertInt32Param("IssuedFor", cm, issue.IssuedFor);
            DatabaseHelper.InsertInt32Param("Approver", cm, issue.Approver);
            DatabaseHelper.InsertInt32Param("ToolkeeperId", cm, issue.ToolkeeperId);
            DatabaseHelper.InsertInt32Param("ApprovalId", cm, issue.ApprovalId);
            DatabaseHelper.InsertBooleanParam("IsPosted", cm, true);
        }

        public string Save(Issue issue)
        {

            string sql = "INSERT INTO IssueHeader(IssueNo,IssueDate,ApprovalId,ToolkeeperId,IssuedFor,Region_Id,Approver,IsPosted) VALUES (@IssueNo,@IssueDate,@ApprovalId,@ToolkeeperId,@IssuedFor,@Region_Id,@Approver,@IsPosted) SELECT @@identity";
            SqlTransaction tr = DefaultConnection.BeginTransaction();
            _issueitemdetaildao = new IssueItemDetailDao();
            try
            {
                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                {
                    ProblemDao problem = new ProblemDao();
                    SetIssue(cm, issue);
                    issue.Id = int.Parse(cm.ExecuteScalar().ToString());
                   _issueitemdetaildao.Save(issue.ItemDetail,issue.Id,issue.RegionId,tr);
                   UpdateApproval(issue.ApprovalId, tr);
                   problem.UpdateAutoNumber("Issue", (int)AspxPageType.Approval, issue.IssueNo, tr);
                   tr.Commit();
                   return "Issue Info Successfully Saved";
                }
            }
            catch (SqlException ex)
            {
                tr.Rollback();
                return ex.Message;
            }

        }
        private void UpdateApproval(int ApprovalId,SqlTransaction tr)
        {
            string sql = "Update ApprovalHeader SET IssuedStatus=@IssuedStatus  where Id = @approvalid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection,tr))
            {
                DatabaseHelper.InsertInt32Param("@approvalid", cm, ApprovalId);
                DatabaseHelper.InsertBooleanParam("@IssuedStatus", cm, true);
                cm.ExecuteNonQuery();
            }
        }
        public void Update(Issue issue)
        {
            string sql = "Update IssueHeader SET  IssueNo=@IssueNo,IssueDate=@IssueDate,ApprovalId=@ApprovalId,ToolkeeperId=@ToolkeeperId,IssuedFor=@IssuedFor,Region_Id=@Region_Id,Approver=@Approver,IsPosted=@IsPosted  where Id = @issueid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@issueid", cm, issue.Id);
                SetIssue(cm, issue);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int issueid)
        {
            string sql = "Delete IssueHeader where Id = @issueid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@issueid", cm, issueid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<Issue> GetIssues(int selectedby, string value, string datefrom, string dateto,int regionId)
        {
            string sql;
            if (selectedby == 2)
                sql = "SELECT Id, IssueNo,IssueDate,ApprovalId,ToolkeeperId,IssuedFor,Region_Id,Approver,IsPosted FROM IssueHeader where Region_Id=@regionId and IssueDate between @DateFrom and @Dateto order by IssueDate DESC";

            else if (selectedby == 0)
                sql = "SELECT  Id, IssueNo,IssueDate,ApprovalId,ToolkeeperId,IssuedFor,Region_Id,Approver,IsPosted FROM IssueHeader where Region_Id=@regionId  order by IssueDate DESC";
            else
                sql = "SELECT  Id, IssueNo,IssueDate,ApprovalId,ToolkeeperId,IssuedFor,Region_Id,Approver,IsPosted FROM IssueHeader  where Region_Id=@regioId and IssueNo = @value order by IssueDate DESC";

            IList<Issue> lstissue = new List<Issue>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (selectedby == 1)
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                }

                else if (selectedby != 1 && selectedby != 0)
                {
                    DatabaseHelper.InsertStringVarCharParam("@DateFrom", cm, datefrom);
                    DatabaseHelper.InsertStringVarCharParam("@Dateto", cm, dateto);
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                }
                else {
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                }
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstissue.Add(GetIssuelist(dr));
                        }
                    }
                }
            }
            return lstissue;
        }
    }
}
