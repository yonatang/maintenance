﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Enums;
using Chai.Maintenance.DataAccess.Maintenance;

namespace Chai.Maintenance.DataAccess.SparepartInventory
{
   public class RequestDao : BaseDao
    {
       public Request GetRequestById(int RequestId)
        {
            string sql = "SELECT Id, RequestNo,RequestDate,RequestedBy,ApprovalStatus,Region_Id,RejectedReason FROM RequestHeader where Id = @requestid" +

                " SELECT RequestDetail.Id,RequestDetail.Instrumentlk_Id, RequestDetail.Request_Id,RequestDetail.Sparepart_Id,RequestDetail.Qty,RequestDetail.Problem_Id,SparepartType.Name as SparepartName,InstrumentLookup.InstrumentName FROM RequestDetail" +
                   " Left join SparepartType on Spareparttype.Id = RequestDetail.Sparepart_Id " +
                   " Left join InstrumentLookup on InstrumentLookup.Id =RequestDetail.Instrumentlk_Id where (Request_Id = @requestid)";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@requestid", cm, RequestId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetRequest(dr);
                        }
                    }
                }
            }
            return null;
        }
        private static Request GetRequestlist(SqlDataReader dr)
        {
            Request request = new Request
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                RequestNo = DatabaseHelper.GetString("RequestNo", dr),
                RequestDate = DatabaseHelper.GetDateTime("RequestDate", dr),
                Region_Id = DatabaseHelper.GetInt32("Region_Id", dr),
                RequestedBy = DatabaseHelper.GetInt32("RequestedBy", dr),
                ApprovalStatus = DatabaseHelper.GetString("ApprovalStatus", dr),
                RejectedReason = DatabaseHelper.GetString("RejectedReason",dr)
            };
            return request;
        }
        private static Request GetRequest(SqlDataReader dr)
        {
            Request request = new Request
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                RequestNo = DatabaseHelper.GetString("RequestNo", dr),
                RequestDate = DatabaseHelper.GetDateTime("RequestDate", dr),
                Region_Id = DatabaseHelper.GetInt32("Region_Id", dr),
                RequestedBy = DatabaseHelper.GetInt32("RequestedBy", dr),
                ApprovalStatus = DatabaseHelper.GetString("ApprovalStatus", dr),
                RejectedReason = DatabaseHelper.GetString("RejectedReason", dr)
               
            };

            dr.NextResult();
            while (dr.Read())
            {
                RequestItemDetail RequestDetail = new RequestItemDetail()// { Id = DatabaseHelper.GetInt32("UserRoleId", dr), UserId = user.Id };

                {
                    Id = DatabaseHelper.GetInt32("Id", dr),
                    RequestId = DatabaseHelper.GetInt32("Request_Id", dr),
                    SparepartId = DatabaseHelper.GetInt32("Sparepart_Id", dr),
                    Qty = DatabaseHelper.GetInt32("Qty", dr),
                    InstrumentId = DatabaseHelper.GetInt32("Instrumentlk_Id", dr),
                    InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                    SparepartName = DatabaseHelper.GetString("SparepartName", dr),
                   
                };
                request.ItemDetail.Add(RequestDetail);
            }

            return request;
        }

        private static void SetRequest(SqlCommand cm, Request request)
        {
            DatabaseHelper.InsertStringVarCharParam("RequestNo", cm, request.RequestNo);
            DatabaseHelper.InsertDateTimeParam("RequestDate", cm, request.RequestDate);
            DatabaseHelper.InsertInt32Param("Region_Id", cm, request.Region_Id);
            DatabaseHelper.InsertInt32Param("RequestedBy", cm, request.RequestedBy);
            DatabaseHelper.InsertStringVarCharParam("ApprovalStatus", cm, request.ApprovalStatus);
            

        }

        public void Save(Request request)
        {
            string sql = "INSERT INTO RequestHeader(RequestNo,RequestDate,Region_Id,RequestedBy,ApprovalStatus) VALUES (@RequestNo,@RequestDate,@Region_Id,@RequestedBy,@ApprovalStatus) SELECT @@identity";

             SqlTransaction tr = DefaultConnection.BeginTransaction();
             try
             {
                 using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                 {
                     ProblemDao problem = new ProblemDao();
                     SetRequest(cm, request);
                     problem.UpdateAutoNumber("Request", (int) AspxPageType.Request, request.RequestNo, tr);
                     request.Id = int.Parse(cm.ExecuteScalar().ToString());
                     tr.Commit();
                 }
             }
            catch (SqlException ex)
             {
                 tr.Rollback();
             }
        }

        

        public void Update(Request request)
        {
            string sql = "Update RequestHeader SET RequestNo=@RequestNo,RequestDate=@RequestDate,Region_Id=@Region_Id,RequestedBy=@RequestedBy,ApprovalStatus=@RequestedBy  where Id = @requestid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@requestid", cm, request.Id);
                SetRequest(cm, request);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int requestid)
        {
            string sql = "Delete RequestHeader where Id = @requestid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@requestid", cm, requestid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<Request> GetRequests(int selectedby, string value, string datefrom, string dateto,int regionId,int userId)
        {
            string sql;
            if (selectedby == 2)
                sql = "SELECT Id, RequestNo,RequestDate,RequestedBy,ApprovalStatus,Region_Id,RejectedReason FROM RequestHeader where Region_Id=@regionId and RequestedBy=@userId and RequestDate between @DateFrom and @Dateto order by RequestDate DESC";

            else if (selectedby == 0)
                sql = "SELECT Id, RequestNo,RequestDate,RequestedBy,ApprovalStatus,Region_Id,RejectedReason FROM RequestHeader where Region_Id=@regionId and RequestedBy=@userId order by RequestDate DESC";
            else
                sql = "SELECT Id, RequestNo,RequestDate,RequestedBy,ApprovalStatus,Region_Id,RejectedReason FROM RequestHeader  where Region_Id=@regionId and RequestedBy=@userId order by RequestDate DESC";

            IList<Request> lstRequest = new List<Request>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (selectedby == 1)
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                    DatabaseHelper.InsertInt32Param("@userId", cm, userId);
                }

                else if (selectedby != 1 && selectedby != 0)
                {
                    DatabaseHelper.InsertStringVarCharParam("@DateFrom", cm, datefrom);
                    DatabaseHelper.InsertStringVarCharParam("@Dateto", cm, dateto);
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                    DatabaseHelper.InsertInt32Param("@userId", cm, userId);
                }
                else
                {
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                    DatabaseHelper.InsertInt32Param("@userId", cm, userId);
                }
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstRequest.Add(GetRequestlist(dr));
                        }
                    }
                }
            }
            return lstRequest;
        }
        public IList<Request> GetRequestsforApproval(int selectedby, string value, string datefrom, string dateto, int regionId, int userId)
        {
            string sql;
            if (selectedby == 2)
                sql = "SELECT Id, RequestNo,RequestDate,RequestedBy,ApprovalStatus,Region_Id,RejectedReason FROM RequestHeader where Region_Id=@regionId and ApprovalStatus=@ApprovalStatus and RequestDate between @DateFrom and @Dateto order by RequestDate DESC";

            else if (selectedby == 0)
                sql = "SELECT Id, RequestNo,RequestDate,RequestedBy,ApprovalStatus,Region_Id,RejectedReason FROM RequestHeader where Region_Id=@regionId and ApprovalStatus=@ApprovalStatus order by RequestDate DESC";
            else if (selectedby ==3)
                sql = "SELECT Id, RequestNo,RequestDate,RequestedBy,ApprovalStatus,Region_Id,RejectedReason FROM RequestHeader where Region_Id=@regionId and ApprovalStatus=@ApprovalStatus and RequestedBy = @userId order by RequestDate DESC";
            else
                sql = "SELECT Id, RequestNo,RequestDate,RequestedBy,ApprovalStatus,Region_Id,RejectedReason FROM RequestHeader  where Region_Id=@regionId and ApprovalStatus=@ApprovalStatus and RequestNo = @value order by RequestDate DESC";

            IList<Request> lstRequest = new List<Request>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (selectedby == 1)
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
          
                    //DatabaseHelper.InsertStringVarCharParam("@ApprovalStatus", cm, "Pending");
                    DatabaseHelper.InsertInt32Param("@userId", cm, userId);
                }

                else if (selectedby != 1 && selectedby != 0)
                {
                    DatabaseHelper.InsertStringVarCharParam("@DateFrom", cm, datefrom);
                    DatabaseHelper.InsertStringVarCharParam("@Dateto", cm, dateto);
                   // DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                    DatabaseHelper.InsertInt32Param("@userId", cm, userId);
                 //   DatabaseHelper.InsertStringVarCharParam("@ApprovalStatus", cm, "Pending");

                }
                else if(selectedby == 3)
                {
                   
                    DatabaseHelper.InsertInt32Param("@userId", cm, userId);
                   // DatabaseHelper.InsertStringVarCharParam("@ApprovalStatus", cm, "Pending");
                    //DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                }
                DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                DatabaseHelper.InsertStringVarCharParam("@ApprovalStatus", cm, "Pending");
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstRequest.Add(GetRequestlist(dr));
                        }
                    }
                }
            }
            return lstRequest;
        }
    }
}
