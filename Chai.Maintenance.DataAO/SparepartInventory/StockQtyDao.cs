﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.SparepartInventory;

namespace Chai.Maintenance.DataAccess.SparepartInventory
{
    public class StockQtyDao:BaseDao
    {
        public StockQty GetStockQtyBy(int regionId,int sparepartId)
        {
            string sql = "SELECT SparePartAvailableQty.Id,SparePartAvailableQty.Region_Id,SparePartAvailableQty.Instrument_Id,SparePartAvailableQty.Sparepart_Id,InstrumentLookup.InstrumentName,SparePartAvailableQty.Qty,SparepartType.ReorderQty,SparepartType.Name as SparepartName FROM SparePartAvailableQty" +
            " Left join SparepartType on SparepartType.Id = SparePartAvailableQty.Sparepart_Id " +
             " Left join InstrumentLookup on InstrumentLookup.Id =SparePartAvailableQty.Instrument_Id" +
            " where SparePartAvailableQty.Sparepart_Id=@sparepartId and SparePartAvailableQty.Region_Id = @regionId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@sparepartId", cm, sparepartId);
                DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetStockQty(dr);
                        }
                    }
                }
            }
            return null;
        }
        private static StockQty GetStockQty(SqlDataReader dr)
        {
            StockQty stockqty = new StockQty
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                RegionId = DatabaseHelper.GetInt32("Region_Id", dr),
                InstrumentId = DatabaseHelper.GetInt32("Instrument_Id",dr),
                SparepartId = DatabaseHelper.GetInt32("Sparepart_Id", dr),
                Qty = DatabaseHelper.GetInt32("Qty", dr),
                SparepartName = DatabaseHelper.GetString("SparepartName", dr),
                ReorderQty = DatabaseHelper.GetInt32("ReorderQty", dr),
                InstrumentName = DatabaseHelper.GetString("InstrumentName",dr)
            };

            return stockqty;
        }
        private static void SetStockQty(SqlCommand cm, StockQty stockqty)
        {
            DatabaseHelper.InsertInt32Param("Region_Id", cm, stockqty.RegionId);
            DatabaseHelper.InsertInt32Param("Instrument_Id", cm, stockqty.InstrumentId);
            DatabaseHelper.InsertInt32Param("Sparepart_Id", cm, stockqty.SparepartId);
            DatabaseHelper.InsertInt32Param("Qty", cm, stockqty.Qty);
         
        }
        public void Save(StockQty stockqty)
        {
            string sql = "Insert Into SparePartAvailableQty(Region_Id,Instrument_Id,Sparepart_Id,Qty)Values(@Region_Id,@Instrument_Id,@Sparepart_Id,@Qty)SELECT @@identity";
            using(SqlCommand cm = new SqlCommand(sql,DefaultConnection))
            {
                SetStockQty(cm, stockqty);
                stockqty.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }
        public void Update(StockQty stockqty)
        {
            string sql = "Update SparePartAvailableQty SET Region_Id=@Region_Id,Instrument_Id=@Instrument_Id,Sparepart_Id=@Sparepart_Id,Qty=@Qty Where Id=@Id";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@Id", cm, stockqty.Id);
                SetStockQty(cm, stockqty);
                cm.ExecuteNonQuery();
            }
        }
        public void Delete(int Id)
        {
            string sql = "DELETE FROM SparePartAvailableQty WHERE Id =@Id";
                
            using(SqlCommand cm = new SqlCommand(sql,DefaultConnection))
               
            {
                DatabaseHelper.InsertInt32Param("@Id",cm,Id);
                cm.ExecuteNonQuery();
               
            }
        }
        public IList<StockQty> GetReorderSparepart(int regionId)
        {
            string sql = " SELECT SparePartAvailableQty.Id,SparePartAvailableQty.Region_Id,SparePartAvailableQty.Instrument_Id,SparePartAvailableQty.Sparepart_Id,InstrumentLookup.InstrumentName,SparePartAvailableQty.Qty,SparepartType.ReorderQty,SparepartType.Name as SparepartName FROM SparePartAvailableQty" +
                       " Left join SparepartType on SparepartType.Id = SparePartAvailableQty.Sparepart_Id " +
                        " Left join InstrumentLookup on InstrumentLookup.Id =SparePartAvailableQty.Instrument_Id" +
                       " where SparePartAvailableQty.Region_Id = @regionId and SparePartAvailableQty.Qty < SparepartType.ReorderQty";
            IList<StockQty> lststockQty = new List<StockQty>();
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);                
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lststockQty.Add(GetStockQty(dr));
                        }
                    }
                }
            }
            return lststockQty;
           

        
        }
        public IList<StockQty> GetStockQtys(int instrumentId,int sparepartId,int regionId)
        {
            string sql;
            if (sparepartId != 0)
            {
                sql = " SELECT SparePartAvailableQty.Id,SparePartAvailableQty.Region_Id,SparePartAvailableQty.Instrument_Id,SparePartAvailableQty.Sparepart_Id,InstrumentLookup.InstrumentName,SparePartAvailableQty.Qty,SparepartType.ReorderQty,SparepartType.Name as SparepartName FROM SparePartAvailableQty" +
                " Left join SparepartType on SparepartType.Id = SparePartAvailableQty.Sparepart_Id " +
                 " Left join InstrumentLookup on InstrumentLookup.Id =SparePartAvailableQty.Instrument_Id" +
                " where SparePartAvailableQty.Sparepart_Id=@sparepartId and SparePartAvailableQty.Region_Id = @regionId order by SparePartAvailableQty.Id DESC";
             
            }

            else if (instrumentId != 0 && sparepartId == 0)
            {
                sql = " SELECT SparePartAvailableQty.Id,SparePartAvailableQty.Region_Id,SparePartAvailableQty.Instrument_Id,SparePartAvailableQty.Sparepart_Id,InstrumentLookup.InstrumentName,SparePartAvailableQty.Qty,SparepartType.ReorderQty,SparepartType.Name as SparepartName FROM SparePartAvailableQty" +
                " Left join SparepartType on SparepartType.Id = SparePartAvailableQty.Sparepart_Id " +
                 " Left join InstrumentLookup on InstrumentLookup.Id =SparePartAvailableQty.Instrument_Id" +
                " where SparepartType.InstrumentLookup_Id=@InstrumnetId and SparePartAvailableQty.Region_Id = @regionId order by SparePartAvailableQty.Id DESC";
            }
            else
            {
                sql = " SELECT SparePartAvailableQty.Id,SparePartAvailableQty.Region_Id,SparePartAvailableQty.Instrument_Id,SparePartAvailableQty.Sparepart_Id,InstrumentLookup.InstrumentName,SparePartAvailableQty.Qty,SparepartType.ReorderQty,SparepartType.Name as SparepartName FROM SparePartAvailableQty" +
                   " Left join SparepartType on SparepartType.Id = SparePartAvailableQty.Sparepart_Id " +
                    " Left join InstrumentLookup on InstrumentLookup.Id =SparePartAvailableQty.Instrument_Id " +
                   " where SparePartAvailableQty.Region_Id = @regionId order by SparePartAvailableQty.Id DESC";
            }

            IList<StockQty> lststockQty = new List<StockQty>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (sparepartId != 0)
                {
                    DatabaseHelper.InsertInt32Param("@sparepartId", cm, sparepartId);
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                }

                else if (instrumentId != 0 && sparepartId == 0)
                {
                    DatabaseHelper.InsertInt32Param("@InstrumnetId", cm, instrumentId);
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                }
                else 
                {
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);                
                }
               
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lststockQty.Add(GetStockQty(dr));
                        }
                    }
                }
            }
            return lststockQty;
        }
    }
}
