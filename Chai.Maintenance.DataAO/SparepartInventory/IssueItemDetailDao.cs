﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.SparepartInventory;

namespace Chai.Maintenance.DataAccess.SparepartInventory
{
    public class IssueItemDetailDao:BaseDao
    {
        public IssueItemDetail GetIssueItemDetailById(int detailId)
        {
            string sql = "SELECT Id,Instrumentlk_Id, Issue_Id,Sparepart_Id,IssuedQty,ApprovedQty,SparePartAvailableQty.Qty as StockQty,SparepartType.Name as SparepartName,InstrumentLookup.InstrumentName FROM IssueDetail" +
            " Left join SparepartType on Spareparttype.Id = AppIssueDetailrovalDetail.Sparepart_Id " +
            " Left join InstrumentLookup on InstrumentLookup.Id =IssueDetail.Instrumentlk_Id" +
            " Left join SparePartAvailableQty on SparePartAvailableQty.sparepart_Id = IssueDetail.Sparepart_Id" +
            " where Id = @detailId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@detailId", cm, detailId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetIssueItemDetail(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static IssueItemDetail GetIssueItemDetail(SqlDataReader dr)
        {
            IssueItemDetail IssueItemDetail = new IssueItemDetail
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                IssueId = DatabaseHelper.GetInt32("Issue_Id", dr),
                SparepartId = DatabaseHelper.GetInt32("Sparepart_Id", dr),
                IssuedQty = DatabaseHelper.GetInt32("IssuedQty", dr),
                ApprovedQty = DatabaseHelper.GetInt32("ApprovedQty", dr),
                InstrumentId = DatabaseHelper.GetInt32("Instrumentlk_Id", dr),
                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                SparepartName = DatabaseHelper.GetString("SparepartName", dr),
                StockQty = DatabaseHelper.GetInt32("StockQty",dr)
            };

            return IssueItemDetail;
        }

        private static void SetIssueDetail(SqlCommand cm, IssueItemDetail issueitemdetail)
        {

            // DatabaseHelper.InsertInt32Param("Receive_Id", cm, recievedetail.ReceiveId);
            DatabaseHelper.InsertInt32Param("Issue_Id", cm, issueitemdetail.IssueId);
            DatabaseHelper.InsertInt32Param("Sparepart_Id", cm, issueitemdetail.SparepartId);
            DatabaseHelper.InsertInt32Param("IssuedQty", cm, issueitemdetail.IssuedQty);
            DatabaseHelper.InsertInt32Param("ApprovedQty", cm, issueitemdetail.ApprovedQty);
            DatabaseHelper.InsertInt32Param("Instrumentlk_Id", cm, issueitemdetail.InstrumentId);
        }
        
        public void Save(IList<IssueItemDetail> issueitemdetail, int issueid,int RegionId,SqlTransaction tr)
        {
            string sql="";
                 
            
                sql = "INSERT INTO IssueDetail(Issue_Id,Instrumentlk_Id,Sparepart_Id,IssuedQty,ApprovedQty) VALUES (@Issue_Id,@Instrumentlk_Id,@Sparepart_Id,@IssuedQty,@ApprovedQty) SELECT @@identity";
                              
                 foreach (IssueItemDetail detial in issueitemdetail)
                    {
                        using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                        {             
                        detial.IssueId = issueid;
                        //DatabaseHelper.InsertInt32Param("@issueid", cm, issueid);
                        SetIssueDetail(cm, detial);
                        detial.Id = int.Parse(cm.ExecuteScalar().ToString());
                        PostItems(detial, RegionId, tr);
                    }
                    
               
               
                
            
        }
    }

        public void Update(IssueItemDetail issueitemdetail)
        {
            string sql = "Update IssueDetail SET Issue_Id=@IssueId,Instrumentlk_Id=@Instrumentlk_Id,Sparepart_Id=@Sparepart_Id,IssuedQty=@IssuedQty,ApprovedQty=@ApprovedQty  where Id = @approvaldetailid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@approvaldetailid", cm, issueitemdetail.Id);
                DatabaseHelper.InsertInt32Param("@IssueId", cm, issueitemdetail.IssueId);
                SetIssueDetail(cm, issueitemdetail);
                cm.ExecuteNonQuery();
            }
        }
        private void PostItems(IssueItemDetail ItemDetail, int regionid,SqlTransaction tr)
        {

          //  foreach (IssueItemDetail detail in ItemDetail)
           // {
                string sql = "UPDATE SparePartAvailableQty SET Qty = Qty - @Qty Where Sparepart_Id=@Sparepart_Id and Region_Id=@Region_Id";

                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection,tr))
                {
                    
                    DatabaseHelper.InsertInt32Param("@Region_Id", cm, regionid);
                    DatabaseHelper.InsertInt32Param("@Sparepart_Id", cm, ItemDetail.SparepartId);
                    DatabaseHelper.InsertInt32Param("@Qty", cm, ItemDetail.IssuedQty);
                    cm.ExecuteNonQuery();

                }
           // }

        }        
        public void Delete(int issueitemdetailid)
        {
            string sql = "Delete IssueDetail where Id = @issueitemdetailid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@approvaldetailid", cm, issueitemdetailid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<IssueItemDetail> GetListOfIssueDetails(int issueid)
        {
            string sql;

            sql = " SELECT Id,Instrumentlk_Id, Issue_Id,Sparepart_Id,IssuedQty,ApprovedQty,SparePartAvailableQty.Qty as StockQty,SparepartType.Name as SparepartName,InstrumentLookup.InstrumentName FROM IssueDetail" +
                  " Left join SparepartType on Spareparttype.Id = IssueDetail.Sparepart_Id " +
                   " Left join SparePartAvailableQty on SparePartAvailableQty.sparepart_Id = IssueDetail.Sparepart_Id" +
                  " Left join InstrumentLookup on InstrumentLookup.Id =IssueDetail.Instrumentlk_Id where Issue_Id = @issueid";




            IList<IssueItemDetail> lstIssuedetail = new List<IssueItemDetail>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {

                DatabaseHelper.InsertInt32Param("@issueid", cm, issueid);


                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstIssuedetail.Add(GetIssueItemDetail(dr));
                        }
                    }
                }
            }
            return lstIssuedetail;
        }
    ////public void PostItems(IList<IssueItemDetail> ItemDetail,int regionid)
    ////    {
    ////        foreach (IssueItemDetail detail in ItemDetail)
    ////        {

    ////            if (CheckSparepartAvailability(detail.SparepartId, detail.ApprovedQty, regionid) != true)
    ////            {
    ////                string sql = "INSERT INTO SparePartAvailableQty(Region_Id,Sparepart_Id,Qty)Values(@Region_Id,@Sparepart_Id,@Qty) SELECT @@identity";
                                 

    ////                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
    ////                {
    ////                    DatabaseHelper.InsertInt32Param("@Region_Id", cm, regionid);
    ////                    DatabaseHelper.InsertInt32Param("@Sparepart_Id", cm, detail.SparepartId);
    ////                    DatabaseHelper.InsertInt32Param("@Qty", cm, detail.Qty);
    ////                    cm.ExecuteNonQuery();

    ////                }
    ////            }


    ////            else
    ////            {

    ////                string sql = "UPDATE SparePartAvailableQty SET Qty = @Qty Where Sparepart_Id=@Sparepart_Id and Region_Id=@Region_Id";

    ////                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
    ////                {
    ////                    DatabaseHelper.InsertInt32Param("@Region_Id", cm, regionid);
    ////                    DatabaseHelper.InsertInt32Param("@Sparepart_Id", cm, detail.SparepartId);
    ////                    DatabaseHelper.InsertInt32Param("@Qty", cm, detail.Qty);
    ////                    cm.ExecuteNonQuery();

    ////                }

    ////            }

    ////        }
        
           
        
    ////   }
    ////   private bool CheckSparepartAvailability(int SaprepartId, int regionId)
       ////{
       ////    string sql;

       ////    sql = "Select * from SparePartAvailableQty where Region_Id=@Region_Id and Sparepart_Id=@Sparepart_Id";
       ////    using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
       ////      {
       ////          DatabaseHelper.InsertInt32Param("@Region_Id", cm, regionId);
       ////          DatabaseHelper.InsertInt32Param("@Sparepart_Id", cm, SaprepartId);

       ////           if(Convert.ToInt32(cm.ExecuteScalar()) != 0)
       ////               {
                     
       ////                  return true;
       ////               } 
                     
       ////              else 
       ////              {
       ////                  return false;
       ////              }
                 
                
           
       ////      }
          
       
       ////}
    }
}
