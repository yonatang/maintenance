﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.DataAccess.Maintenance;
using Chai.Maintenance.Enums;
namespace Chai.Maintenance.DataAccess.SparepartInventory
{
    public class ApprovalDao : BaseDao
    {
        public Approval GetApprovalById(int approvalId)
        {
            string sql = "SELECT Id,ApprovalNo,ApprovalDate,Request_Id,RequestedBy,Approver,ApprovalStatus,Region_Id,RejectedReason FROM ApprovalHeader where Id = @approvalId" +

                " SELECT ApprovalDetail.Id,ApprovalDetail.Instrumentlk_Id, ApprovalDetail.Approval_Id,ApprovalDetail.Sparepart_Id,ApprovalDetail.RequestedQty,ApprovalDetail.ApprovedQty,SparepartType.Name as SparepartName,InstrumentLookup.InstrumentName FROM ApprovalDetail" +
                   " Left join SparepartType on Spareparttype.Id = ApprovalDetail.Sparepart_Id " +
                    //" Left join SparePartAvailableQty on SparePartAvailableQty.sparepart_Id = ApprovalDetail.Sparepart_Id" +
                   " Left join InstrumentLookup on InstrumentLookup.Id =ApprovalDetail.Instrumentlk_Id where (ApprovalDetail.Approval_Id = @approvalid)";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@approvalid", cm, approvalId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetApproval(dr);
                        }
                    }
                }
                return null;
            }
        }
        private static Approval GetApprovallist(SqlDataReader dr)
        {
            Approval approval = new Approval
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                ApprovalNo = DatabaseHelper.GetString("ApprovalNo", dr),
                ApprovalDate = DatabaseHelper.GetDateTime("ApprovalDate", dr), 
                RequestId = DatabaseHelper.GetInt32("Request_Id",dr),
                Approver = DatabaseHelper.GetInt32("Approver",dr),
                RegionId = DatabaseHelper.GetInt32("Region_Id", dr),
                RequestedBy = DatabaseHelper.GetInt32("RequestedBy", dr),
                ApprovalStatus = DatabaseHelper.GetString("ApprovalStatus", dr),
                RejectedReason = DatabaseHelper.GetString("RejectedReason",dr)
            };
            return approval;
        }
        private static Approval GetApproval(SqlDataReader dr)
        {
            Approval approval = new Approval
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                ApprovalNo = DatabaseHelper.GetString("ApprovalNo", dr),
                ApprovalDate = DatabaseHelper.GetDateTime("ApprovalDate", dr),
                RequestId = DatabaseHelper.GetInt32("Request_Id", dr),
                Approver = DatabaseHelper.GetInt32("Approver", dr),
                RegionId = DatabaseHelper.GetInt32("Region_Id", dr),
                RequestedBy = DatabaseHelper.GetInt32("RequestedBy", dr),
                ApprovalStatus = DatabaseHelper.GetString("ApprovalStatus", dr),
                RejectedReason = DatabaseHelper.GetString("RejectedReason", dr)
            };

            dr.NextResult();
            while (dr.Read())
            {
                ApprovalItemDetail approvaldetail = new ApprovalItemDetail()// { Id = DatabaseHelper.GetInt32("UserRoleId", dr), UserId = user.Id };

                {
                    Id = DatabaseHelper.GetInt32("Id", dr),
                    ApprovalId = DatabaseHelper.GetInt32("Approval_Id", dr),
                    SparepartId = DatabaseHelper.GetInt32("Sparepart_Id", dr),
                    RequestedQty = DatabaseHelper.GetInt32("RequestedQty", dr),
                    ApprovedQty = DatabaseHelper.GetInt32("ApprovedQty", dr),
                    InsrumentId = DatabaseHelper.GetInt32("Instrumentlk_Id", dr),
                    InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                    SparepartName = DatabaseHelper.GetString("SparepartName", dr),
                    //StockQty = DatabaseHelper.GetInt32("StockQty", dr)
                };
                approval.ItemDetail.Add(approvaldetail);
            }

            return approval;
        }

        private static void SetApproval(SqlCommand cm, Approval approval)
        {
            DatabaseHelper.InsertStringVarCharParam("ApprovalNo", cm, approval.ApprovalNo);
            DatabaseHelper.InsertDateTimeParam("ApprovalDate", cm, approval.ApprovalDate);
            DatabaseHelper.InsertInt32Param("Region_Id", cm, approval.RegionId);
            DatabaseHelper.InsertInt32Param("RequestedBy", cm, approval.RequestedBy);
            DatabaseHelper.InsertStringVarCharParam("ApprovalStatus", cm, approval.ApprovalStatus);
            DatabaseHelper.InsertInt32Param("approver", cm, approval.Approver);
            DatabaseHelper.InsertInt32Param("Request_Id", cm, approval.RequestId);
            DatabaseHelper.InsertStringVarCharParam("RejectedReason", cm, approval.RejectedReason);

        }

        public void Save(Approval approval)
        {
            string sql = "INSERT INTO ApprovalHeader(ApprovalNo,ApprovalDate,Request_Id,RequestedBy,Approver,ApprovalStatus,Region_Id,IssuedStatus,RejectedReason) VALUES (@ApprovalNo,@ApprovalDate,@Request_Id,@RequestedBy,@Approver,@ApprovalStatus,@Region_Id,@IssuedStatus,@RejectedReason) SELECT @@identity";
            SqlTransaction tr = DefaultConnection.BeginTransaction();
            try
            {
                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                {
                    ProblemDao problem = new ProblemDao();
                    DatabaseHelper.InsertBooleanParam("@IssuedStatus", cm, false);
                    SetApproval(cm, approval);
                    problem.UpdateAutoNumber("Approval", (int)AspxPageType.Approval, approval.ApprovalNo, tr);
                    approval.Id = int.Parse(cm.ExecuteScalar().ToString());
                }
                UpdateRequest(approval, tr);
                tr.Commit();
            }
            catch(Exception ex)
            {
             tr.Rollback();
            }
        }
        private void UpdateRequest(Approval approval,SqlTransaction tr)
        {
            string sql = "Update RequestHeader SET ApprovalStatus=@Status  where Id = @requestId";
           
                using (SqlCommand cm = new SqlCommand(sql, DefaultConnection, tr))
                {
                    DatabaseHelper.InsertInt32Param("@requestId", cm, approval.RequestId);
                    DatabaseHelper.InsertStringVarCharParam("@Status", cm, approval.ApprovalStatus);
                    cm.ExecuteNonQuery();
                }
              
        }
        public void Update(Approval approval)
        {
            string sql = "Update ApprovalHeader SET  ApprovalNo=@ApprovalNo,ApprovalDate=@ApprovalDate,RequestId=@RequestId,RequestedBy=@RequestedBy,Approver=@Approver,ApprovalStatus=@ApprovalStatus,Region_Id=@Region_Id,RejectedReason=@RejectedReason  where Id = @approvalid";
             SqlTransaction tr = DefaultConnection.BeginTransaction();
            try
            {
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@approvalid", cm, approval.Id);
                SetApproval(cm, approval);
                cm.ExecuteNonQuery();
            }
            UpdateRequest(approval, tr);
            tr.Commit();
            }
            catch (Exception ex)
            {
                tr.Rollback();
            }
        }

        public void Delete(int approvalid)
        {
            string sql = "Delete ApprovalHeader where Id = @approvalid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@approvalid", cm, approvalid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<Approval> GetApprovals(int selectedby, string value, string datefrom, string dateto,int regionId,int userId)
        {
            string sql;
            if (selectedby == 2)
                sql = "SELECT Id, ApprovalNo,ApprovalDate,Request_Id,RequestedBy,Approver,ApprovalStatus,Region_Id,RejectedReason FROM ApprovalHeader where Region_Id=@regionId and Approver=@userId and ApprovalDate between @DateFrom and @Dateto and IssuedStatus = @IssuedStatus order by ApprovalDate";

            else if (selectedby == 0)
                sql = "SELECT  Id, ApprovalNo,ApprovalDate,Request_Id,RequestedBy,Approver,ApprovalStatus,Region_Id,RejectedReason FROM ApprovalHeader Where Region_Id=@regionId and Approver=@userId and IssuedStatus = @IssuedStatus order by ApprovalDate";
            else
                sql = "SELECT  Id, ApprovalNo,ApprovalDate,Request_Id,RequestedBy,Approver,ApprovalStatus,Region_Id FROM ApprovalHeader  where Region_Id=@regionId and Approver=@userId and ApprovalNo = @value and IssuedStatus = @IssuedStatus order by ApprovalDate";

            IList<Approval> lstapproval = new List<Approval>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (selectedby == 1)
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                }

                else if (selectedby != 1 && selectedby != 0)
                {
                    DatabaseHelper.InsertStringVarCharParam("@DateFrom", cm, datefrom);
                    DatabaseHelper.InsertStringVarCharParam("@Dateto", cm, dateto);
                }
               
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                    DatabaseHelper.InsertInt32Param("@userId", cm, userId);
                
                DatabaseHelper.InsertBooleanParam("@IssuedStatus", cm, false);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstapproval.Add(GetApprovallist(dr));
                        }
                    }
                }
            }
            return lstapproval;
        }
        public IList<Approval> GetApprovalsforIssue(int selectedby, string value, string datefrom, string dateto, int regionId, int userId)
        {
            string sql;
            if (selectedby == 2)
                sql = "SELECT Id, ApprovalNo,ApprovalDate,Request_Id,RequestedBy,Approver,ApprovalStatus,Region_Id,RejectedReason FROM ApprovalHeader where Region_Id=@regionId and  ApprovalDate between @DateFrom and @Dateto and IssuedStatus = @IssuedStatus order by ApprovalDate";

            else if (selectedby == 0)
                sql = "SELECT  Id, ApprovalNo,ApprovalDate,Request_Id,RequestedBy,Approver,ApprovalStatus,Region_Id,RejectedReason FROM ApprovalHeader Where Region_Id=@regionId and IssuedStatus = @IssuedStatus order by ApprovalDate";
            else if(selectedby ==3)
                sql = "SELECT  Id, ApprovalNo,ApprovalDate,Request_Id,RequestedBy,Approver,ApprovalStatus,Region_Id,RejectedReason FROM ApprovalHeader Where Region_Id=@regionId and RequestedBy=@userId and IssuedStatus = @IssuedStatus order by ApprovalDate";
            else
                sql = "SELECT  Id, ApprovalNo,ApprovalDate,Request_Id,RequestedBy,Approver,ApprovalStatus,Region_Id,RejectedReason FROM ApprovalHeader  where Region_Id=@regionId and ApprovalNo = @value and IssuedStatus = @IssuedStatus order by ApprovalDate";

            IList<Approval> lstapproval = new List<Approval>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (selectedby == 1)
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                }

                else if (selectedby == 2)
                {
                    DatabaseHelper.InsertStringVarCharParam("@DateFrom", cm, datefrom);
                    DatabaseHelper.InsertStringVarCharParam("@Dateto", cm, dateto);
                }
                else if (selectedby == 3)
                {
                    DatabaseHelper.InsertInt32Param("@userId", cm, userId);
                }
                DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                DatabaseHelper.InsertBooleanParam("@IssuedStatus", cm, false);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstapproval.Add(GetApprovallist(dr));
                        }
                    }
                }
            }
            return lstapproval;
        }
    }
}
