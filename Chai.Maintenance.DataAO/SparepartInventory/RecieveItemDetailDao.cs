﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.SparepartInventory;

namespace Chai.Maintenance.DataAccess.SparepartInventory
{
   public class RecieveItemDetailDao : BaseDao
    {
        public RecieveItemDetail GetRecieveItemDetailById(int detailId)
        {
            string sql = "SELECT Id,Instrumentlk_Id, Receive_Id,Manfacturer_Id,Sparepart_Id,Qty,UnitPrice,TotalPrice Manufacturer.Name as ManufacturerName,SparepartType.Name as SparepartName,InstrumentLookup.InstrumentName FROM ReceiveDetail" +
            " Left Join Manufacturer on Manufacturer.Id =ReceiveDetail.Manfacturer_Id " +
            " Left join SparepartType on Spareparttype.Id = ReceiveDetail.Sparepart_Id " +
            " Left join InstrumentLookup on InstrumentLookup.Id =ReceiveDetail.Instrumentlk_Id where Id = @detailId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@detailId", cm, detailId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetRecieveItemDetail(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static RecieveItemDetail GetRecieveItemDetail(SqlDataReader dr)
        {
            RecieveItemDetail recieveitemdetail = new RecieveItemDetail
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                ReceiveId = DatabaseHelper.GetInt32("Receive_Id", dr),
                ManfacturerId = DatabaseHelper.GetInt32("Manfacturer_Id", dr),
                SparepartId = DatabaseHelper.GetInt32("Sparepart_Id", dr),
                Qty = DatabaseHelper.GetInt32("Qty", dr),
                UnitPrice = DatabaseHelper.GetMoney("UnitPrice", dr),
                TotalPrice = DatabaseHelper.GetMoney("TotalPrice", dr),
                InstrumentId = DatabaseHelper.GetInt32("Instrumentlk_Id",dr),
                InstrumentName =DatabaseHelper.GetString("InstrumentName",dr),
                Manufacturer = DatabaseHelper.GetString("ManufacturerName",dr),
                SparepartName = DatabaseHelper.GetString("SparepartName",dr)
            };

            return recieveitemdetail;
        }

        private static void SetRecieveDetail(SqlCommand cm, RecieveItemDetail recievedetail)
        {
            
           // DatabaseHelper.InsertInt32Param("Receive_Id", cm, recievedetail.ReceiveId);
            DatabaseHelper.InsertInt32Param("Manfacturer_Id", cm, recievedetail.ManfacturerId);
            DatabaseHelper.InsertInt32Param("Sparepart_Id", cm, recievedetail.SparepartId);
            DatabaseHelper.InsertInt32Param("Qty", cm, recievedetail.Qty);
            DatabaseHelper.InsertMoneyParam("UnitPrice", cm, recievedetail.UnitPrice);
            DatabaseHelper.InsertMoneyParam("TotalPrice", cm, recievedetail.TotalPrice);
            DatabaseHelper.InsertInt32Param("Instrumentlk_Id", cm, recievedetail.InstrumentId);
        }

        public void Save(RecieveItemDetail recievedetail,int recieveid)
        {
            string sql = "INSERT INTO ReceiveDetail(Receive_Id,Manfacturer_Id,Sparepart_Id,Qty,UnitPrice,TotalPrice,Instrumentlk_Id) VALUES (@receiveid,@Manfacturer_Id,@Sparepart_Id,@Qty,@UnitPrice,@TotalPrice,@Instrumentlk_Id) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                
                SetRecieveDetail(cm, recievedetail);
                DatabaseHelper.InsertInt32Param("@receiveid", cm, recieveid);
                recievedetail.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(RecieveItemDetail recievedetail)
        {
            string sql = "Update ReceiveDetail SET Receive_Id=@Receive_Id,Manfacturer_Id=@Manfacturer_Id,Sparepart_Id=@Sparepart_Id,Qty=@Qty,UnitPrice=@UnitPrice,TotalPrice=@TotalPrice,Instrumentlk_Id=Instrumentlk_Id  where Id = @recievedetailid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@recievedetailid", cm, recievedetail.Id);
                DatabaseHelper.InsertInt32Param("@Receive_Id", cm, recievedetail.ReceiveId);
                SetRecieveDetail(cm,recievedetail);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int recievedetailid)
        {
            string sql = "Delete ReceiveDetail where Id = @recievedetailid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@recievedetailid", cm, recievedetailid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<RecieveItemDetail> GetListOfRecieves(int recieveid)
        {
            string sql;

            sql = " SELECT Id,Instrumentlk_Id, Receive_Id,Manfacturer_Id,Sparepart_Id,Qty,UnitPrice,TotalPrice Manufacturer.Name as ManufacturerName,SparepartType.Name as SparepartName,InstrumentLookup.InstrumentName FROM ReceiveDetail" +
                   " Left Join Manufacturer on Manufacturer.Id =ReceiveDetail.Manfacturer_Id " +
                   " Left join SparepartType on Spareparttype.Id = ReceiveDetail.Sparepart_Id " +
                   " Left join InstrumentLookup on InstrumentLookup.Id =ReceiveDetail.Instrumentlk_Id where Receive_Id = @recieveid";

             


                IList<RecieveItemDetail> lstRecievedetail = new List<RecieveItemDetail>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                
                    DatabaseHelper.InsertInt32Param("@recieveid", cm, recieveid);
                

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstRecievedetail.Add(GetRecieveItemDetail(dr));
                        }
                    }
                }
            }
            return lstRecievedetail;
        }
        
       public void PostItems(IList<RecieveItemDetail> ItemDetail,int regionid,int recieveId)
        {
            foreach (RecieveItemDetail detail in ItemDetail)
            {

                if (CheckSparepartAvailability(detail.SparepartId, regionid) != true)
                {
                    string sql = "INSERT INTO SparePartAvailableQty(Region_Id,Instrument_Id,Sparepart_Id,Qty)Values(@Region_Id,@Instrument_Id,@Sparepart_Id,@Qty) SELECT @@identity";
                                 

                    using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
                    {
                        DatabaseHelper.InsertInt32Param("@Region_Id", cm, regionid);
                        DatabaseHelper.InsertInt32Param("@Instrument_Id", cm, detail.InstrumentId);
                        DatabaseHelper.InsertInt32Param("@Sparepart_Id", cm, detail.SparepartId);
                        DatabaseHelper.InsertInt32Param("@Qty", cm, detail.Qty);
                        cm.ExecuteNonQuery();

                    }
                }


                else
                {

                    string sql = "UPDATE SparePartAvailableQty SET Qty = Qty + @Qty Where Sparepart_Id=@Sparepart_Id and Region_Id=@Region_Id";

                    using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
                    {
                        DatabaseHelper.InsertInt32Param("@Region_Id", cm, regionid);
                        DatabaseHelper.InsertInt32Param("@Sparepart_Id", cm, detail.SparepartId);
                        DatabaseHelper.InsertInt32Param("@Qty", cm, detail.Qty);
                        cm.ExecuteNonQuery();

                    }

                }

            }
            UpdateRecieve(recieveId);
           
        
       }
       private bool CheckSparepartAvailability(int SaprepartId, int regionId)
       {
           string sql;

           sql = "Select * from SparePartAvailableQty where Region_Id=@Region_Id and Sparepart_Id=@Sparepart_Id";
           using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
             {
                 DatabaseHelper.InsertInt32Param("@Region_Id", cm, regionId);
                 DatabaseHelper.InsertInt32Param("@Sparepart_Id", cm, SaprepartId);

                  if(Convert.ToInt32(cm.ExecuteScalar()) != 0)
                      {
                     
                         return true;
                      } 
                     
                     else 
                     {
                         return false;
                     }
                 
                
           
             }
          
       
       }
       private void UpdateRecieve(int recieveid)
       {
           string sql = "Update ReceiveHeader SET IsPosted=@IsPosted  where Id = @recieveid";

           using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
           {
               DatabaseHelper.InsertInt32Param("@recieveid", cm, recieveid);
               DatabaseHelper.InsertBooleanParam("@IsPosted", cm, true);
               cm.ExecuteNonQuery();
           }
       }
    }
}
