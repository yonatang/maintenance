﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.SparepartInventory;

namespace Chai.Maintenance.DataAccess.SparepartInventory
{
    public class RequestItemDetailDao : BaseDao
    {
        public RequestItemDetail GetRequestItemDetailById(int detailId)
        {
            string sql = " SELECT RequestDetail.Id,RequestDetail.Instrumentlk_Id, RequestDetail.Request_Id,RequestDetail.Sparepart_Id,RequestDetail.Qty,RequestDetail.Problem_Id,SparepartType.Name as SparepartName,InstrumentLookup.InstrumentName FROM RequestDetail" +
                   " Left join SparepartType on Spareparttype.Id = RequestDetail.Sparepart_Id " +
                   " Left join InstrumentLookup on InstrumentLookup.Id =RequestDetail.Instrumentlk_Id where Id = @detailId";  

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@detailId", cm, detailId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetRequestItemDetail(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static RequestItemDetail GetRequestItemDetail(SqlDataReader dr)
        {
            RequestItemDetail requestitemdetail = new RequestItemDetail
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                RequestId = DatabaseHelper.GetInt32("Request_Id", dr),
                SparepartId = DatabaseHelper.GetInt32("Sparepart_Id", dr),
                Qty = DatabaseHelper.GetInt32("Qty", dr),
                ProblemId = DatabaseHelper.GetInt32("Problem_Id",dr),
                InstrumentId = DatabaseHelper.GetInt32("Instrumentlk_Id", dr),
                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                SparepartName = DatabaseHelper.GetString("SparepartName", dr)
            };

            return requestitemdetail;
        }

        private static void SetRequestDetail(SqlCommand cm, RequestItemDetail requstitemdetail)
        {

            // DatabaseHelper.InsertInt32Param("Receive_Id", cm, recievedetail.ReceiveId);
           
            DatabaseHelper.InsertInt32Param("Sparepart_Id", cm, requstitemdetail.SparepartId);
            DatabaseHelper.InsertInt32Param("Qty", cm, requstitemdetail.Qty);
            DatabaseHelper.InsertInt32Param("Instrumentlk_Id", cm, requstitemdetail.InstrumentId);
            DatabaseHelper.InsertInt32Param("Problem_Id", cm, requstitemdetail.ProblemId);
        }

        public void Save(RequestItemDetail requstitemdetail, int requestid)
        {
            string sql = "INSERT INTO RequestDetail(Request_Id,Instrumentlk_Id,Sparepart_Id,Qty,Problem_Id) VALUES (@requestid,@Instrumentlk_Id,@Sparepart_Id,@Qty,@Problem_Id) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@requestid", cm, requestid);
                SetRequestDetail(cm, requstitemdetail);
                requstitemdetail.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(RequestItemDetail requestdetail)
        {
            string sql = "Update RequestDetail SET Request_Id=@Request_Id,Instrumentlk_Id=@Instrumentlk_Id,Sparepart_Id=@Sparepart_Id,Qty=@Qty,Problem_Id=@Problem_Id where Id = @requestdetailid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@requestdetailid", cm, requestdetail.Id);
                DatabaseHelper.InsertInt32Param("@Request_Id", cm, requestdetail.RequestId);
                SetRequestDetail(cm, requestdetail);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int recievedetailid)
        {
            string sql = "Delete ReceiveDetail where Id = @recievedetailid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@recievedetailid", cm, recievedetailid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<RequestItemDetail> GetListOfRequestDetails(int requestid)
        {
            string sql;

            sql = " SELECT RequestDetail.Id,RequestDetail.Instrumentlk_Id, RequestDetail.Request_Id,RequestDetail.Sparepart_Id,RequestDetail.Qty,RequestDetail.Problem_Id,SparepartType.Name as SparepartName,InstrumentLookup.InstrumentName FROM RequestDetail" +
                   " Left join SparepartType on Spareparttype.Id = RequestDetail.Sparepart_Id " +
                   " Left join InstrumentLookup on InstrumentLookup.Id =RequestDetail.Instrumentlk_Id where RequestDetail.Request_Id = @requestid";




            IList<RequestItemDetail> lstRequestdetail = new List<RequestItemDetail>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {

                DatabaseHelper.InsertInt32Param("@requestid", cm, requestid);


                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstRequestdetail.Add(GetRequestItemDetail(dr));
                        }
                    }
                }
            }
            return lstRequestdetail;
        }
    }
}
