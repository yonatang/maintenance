﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.DBConnection;
using Chai.Maintenance.Shared;
namespace Chai.Maintenance.DataAccess
{
    public class UserInfoDao : BaseDao
    {
        public UserInfo GetUserInfo(int userId)
        {
            string sql = "select Region.RegionName, Site.SiteName, Region.Id AS RegionId, AppUser.UserName, (AppUser.FirstName + ' ' + AppUser.LastName) AS UserFullName from AppUser " +
                         "LEFT JOIN Region ON AppUser.RegionId = Region.Id " +
                         "LEFT JOIN Site ON AppUser.SiteId = Site.Id WHERE AppUser.UserId = @UserId";

            string connstring = TechnicalConfig.GetConfiguration()["ConnectionString"].ToString();
            using (SqlConnection cn = new SqlConnection(connstring))
            {
                cn.Open();
                using (SqlCommand cm = new SqlCommand(sql, cn))
                {
                    DatabaseHelper.InsertInt32Param("@UserId", cm, userId);

                    using (SqlDataReader dr = cm.ExecuteReader())
                    {
                        if (dr != null)
                        {
                            if (dr.HasRows)
                            {
                                dr.Read();
                                return GetUserInfo(dr);
                            }
                        }
                    }
                    cn.Close();
                }
                return null;
            }   
         
        }

        private static UserInfo GetUserInfo(SqlDataReader dr)
        {
            UserInfo _userInfo = new UserInfo();

            _userInfo.SiteName = DatabaseHelper.GetString("SiteName", dr);
            _userInfo.RegionName = DatabaseHelper.GetString("RegionName", dr);
            _userInfo.RegionId = DatabaseHelper.GetInt32("RegionId", dr);
            _userInfo.UserFullName = DatabaseHelper.GetString("UserFullName", dr);
            _userInfo.UserName = DatabaseHelper.GetString("UserName", dr);
            return _userInfo;
        }

    }
}