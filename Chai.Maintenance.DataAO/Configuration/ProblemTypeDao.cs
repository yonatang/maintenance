﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.DataAccess.Configuration
{
    public class ProblemTypeDao : BaseDao
    {
        public ProblemType GetProblemTypeById(int problemTypeId)
        {
            string sql = "SELECT Id, Name FROM ProblemType where Id = @problemTypeId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@problemTypeId", cm, problemTypeId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetProblemType(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static ProblemType GetProblemType(SqlDataReader dr)
        {
            ProblemType problemType = new ProblemType
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                Name = DatabaseHelper.GetString("Name", dr),

            };

            return problemType;
        }

        private static void SetProblemType(SqlCommand cm, ProblemType problemType)
        {
            DatabaseHelper.InsertStringNVarCharParam("@Name", cm, problemType.Name);

        }

        public void Save(ProblemType problemType)
        {
            string sql = "INSERT INTO ProblemType(Name) VALUES (@Name) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetProblemType(cm, problemType);
                problemType.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(ProblemType problemType)
        {
            string sql = "Update ProblemType SET Name =@Name where Id = @problemTypeid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@problemTypeid", cm, problemType.Id);
                SetProblemType(cm, problemType);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int problemTypeid)
        {
            string sql = "Delete ProblemType where Id = @problemTypeid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@problemTypeid", cm, problemTypeid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<ProblemType> GetListOfProblemType(string value)
        {
            string sql;
            if (value == "")

                sql = "SELECT Id, Name FROM ProblemType order by Name";
            else
                sql = "SELECT Id, Name FROM ProblemType where Name = @value order by Name";

            IList<ProblemType> lstProblemType = new List<ProblemType>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (value != "")
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                }

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstProblemType.Add(GetProblemType(dr));
                        }
                    }
                }
            }
            return lstProblemType;
        }
    }
}
