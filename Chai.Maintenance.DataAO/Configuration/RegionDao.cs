﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.DataAccess.Configuration
{
    public class RegionDao : BaseDao
    {
        public Region GetRegionById(int regionid)
        {
            string sql = "SELECT Id, RegionName, RegionCode,MapId FROM Region where Id = @regionid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@regionid", cm, regionid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetRegion(dr);
                        }
                    }
                }
            }
            return null;
        }     
        private static Region GetRegion(SqlDataReader dr)
        {
            Region region = new Region
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                RegionName = DatabaseHelper.GetString("RegionName", dr),
                RegionCode = DatabaseHelper.GetString("RegionCode", dr),
                Mapid = DatabaseHelper.GetString("MapId", dr)
            };

            return region;
        }
        private static void SetRegion(SqlCommand cm, Region region)
        {
            DatabaseHelper.InsertStringNVarCharParam("@RegionName", cm, region.RegionName);
            DatabaseHelper.InsertStringNVarCharParam("@RegionCode", cm, region.RegionCode);
            DatabaseHelper.InsertStringNVarCharParam("@MapId", cm, region.Mapid);
        }
        public void Save(Region region)
        {
            string sql = "INSERT INTO Region(RegionName, RegionCode,MapId) VALUES (@RegionName,@RegionCode,@MapId) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetRegion(cm, region);
                region.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }
        public void Update(Region region)
        {
            string sql = "Update Region SET RegionName =@RegionName , RegionCode = @RegionCode,MapId=@MapId where Id = @regionid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@regionid", cm, region.Id);
                SetRegion(cm, region);
                cm.ExecuteNonQuery();
            }
        }
        public void Delete(int regionid)
        {
            string sql = "Delete Region where Id = @regionid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@regionid", cm, regionid);
                cm.ExecuteNonQuery();
            }
        }
        public IList<Region> GetListOfRegion(int findby,string value,int regionId)
        {

            string sql;
           
            if (regionId == -1 || regionId == 1000)
            {

                if (findby == 0 )

                    sql = "SELECT Id,RegionName,RegionCode,MapId FROM Region order by RegionName";
                else
                    sql = "SELECT Id,RegionName,RegionCode,MapId FROM Region where RegionName = @value or RegionCode = @value order by RegionName";
            }
            else
            {
                if (findby == 0 && value == "" || value != "")

                    sql = "SELECT Id,RegionName,RegionCode,MapId FROM Region Where Id=@regionId order by RegionName";
                else
                    sql = "SELECT Id,RegionName,RegionCode,MapId FROM Region where Id=@regionId and RegionName = @value or RegionCode = @value order by RegionName";
            }
            
            IList<Region> lstregion = new List<Region>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (findby != 0 && value != "")
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value );
                }
                else if (findby == 0 && value == "")
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                }
                else if(findby != 0 && value == "")
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                }
                DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstregion.Add(GetRegion(dr));
                        }
                    }
                }
            }
            return lstregion;
        }
         public IList<Region> GetAllListOfRegion()
        {

            string sql;

            sql = "SELECT Id,RegionName,RegionCode,MapId FROM Region order by RegionName";
           
            
            IList<Region> lstregion = new List<Region>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                
               
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstregion.Add(GetRegion(dr));
                        }
                    }
                }
            }
            return lstregion;
        }
    }
    
}
