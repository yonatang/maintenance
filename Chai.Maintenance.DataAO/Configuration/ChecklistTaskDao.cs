﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;

namespace Chai.Maintenance.DataAccess.Configuration
{
    public class ChecklistTaskDao : BaseDao
    {
        public ChecklistTask GetChecklistTaskById(int checklistTaskId)
        {
            string sql = "SELECT ChecklistTask.*, InstrumentLookup.InstrumentName FROM ChecklistTask left JOIN " +
                         "InstrumentLookup ON ChecklistTask.InstrumentLookupId = InstrumentLookup.Id " +
                         "where ChecklistTask.Id = @checklistTaskId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@checklistTaskId", cm, checklistTaskId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetChecklistTask(dr);
                        }
                    }
                }
            }
            return null;
        }


        private static ChecklistTask GetChecklistTask(SqlDataReader dr)
        {
            ChecklistTask checklistTask = new ChecklistTask
                                {
                                    Id = DatabaseHelper.GetInt32("Id", dr),
                                    Name = DatabaseHelper.GetString("Name", dr),
                                    InstrumentLookupId = DatabaseHelper.GetInt32("InstrumentLookupId", dr),
                                    InstrumentName = DatabaseHelper.GetString("InstrumentName", dr)
                                };

            return checklistTask;
        }

        private static void SetChecklistTask(SqlCommand cm, ChecklistTask checklistTask)
        {
            DatabaseHelper.InsertStringNVarCharParam("@Name", cm, checklistTask.Name);
            DatabaseHelper.InsertInt32Param("InstrumentLookupId", cm, checklistTask.InstrumentLookupId);
        }

        public void Save(ChecklistTask checklistTask)
        {
            string sql = "INSERT INTO ChecklistTask(Name, InstrumentLookupId) VALUES (@Name, @InstrumentLookupId) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetChecklistTask(cm, checklistTask);
                checklistTask.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(ChecklistTask checklistTask)
        {
            string sql = "Update ChecklistTask SET Name = @Name, InstrumentLookupId = @InstrumentLookupId where Id = @checklistTaskId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@checklistTaskId", cm, checklistTask.Id);
                SetChecklistTask(cm, checklistTask);
                cm.ExecuteNonQuery();
            }
        }


        public void Delete(int checklistTaskId)
        {
            string sql = "Delete ChecklistTask where Id = @checklistTaskId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@checklistTaskId", cm, checklistTaskId);
                cm.ExecuteNonQuery();
            }
        }

        public IList<ChecklistTask> GetListOfChecklistTask(string task, int instrumentLookupId)
        {
            string sql = "";
            if (task == "" && instrumentLookupId == 0)
                sql = "SELECT ChecklistTask.*, InstrumentLookup.InstrumentName FROM ChecklistTask left JOIN " +
                       "InstrumentLookup ON ChecklistTask.InstrumentLookupId = InstrumentLookup.Id";
                                                                           
            else if (task != "" && instrumentLookupId != 0)
                sql = "SELECT ChecklistTask.*, InstrumentLookup.InstrumentName FROM ChecklistTask left JOIN " +
                      "InstrumentLookup ON ChecklistTask.InstrumentLookupId = InstrumentLookup.Id " +
                      "where InstrumentLookup.Id = @instrumentLookupId AND ChecklistTask.Name = @Name";
            else if (task == "" && instrumentLookupId != 0)
                sql = "SELECT ChecklistTask.*, InstrumentLookup.InstrumentName FROM ChecklistTask left JOIN " +
                      "InstrumentLookup ON ChecklistTask.InstrumentLookupId = InstrumentLookup.Id " +
                      "where InstrumentLookup.Id = @instrumentLookupId";
            else if (task != "" && instrumentLookupId == 0)
                sql = "SELECT ChecklistTask.*, InstrumentLookup.InstrumentName FROM ChecklistTask left JOIN " +
                      "InstrumentLookup ON ChecklistTask.InstrumentLookupId = InstrumentLookup.Id " +
                      "where  ChecklistTask.Name = @Name";

            IList<ChecklistTask> lstChecklistTask = new List<ChecklistTask>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (task != "")
                    DatabaseHelper.InsertStringVarCharParam("@Name", cm, task);
                if(instrumentLookupId !=0)
                    DatabaseHelper.InsertInt32Param("@instrumentLookupId", cm, instrumentLookupId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstChecklistTask.Add(GetChecklistTask(dr));
                        }
                    }
                }
            }
            return lstChecklistTask;
        }
    }
}