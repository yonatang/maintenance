﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.DataAccess.Configuration
{
    public class AutoNumberDao : BaseDao
    {
        public AutoNumber GetAutoNumberByPageId(int PageId)
        {
            string sql = "SELECT Id, PageId, Prefix, Sequence, Status FROM AutoNumber where PageId = @PageId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@PageId", cm, PageId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetAutoNumber(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static AutoNumber GetAutoNumber(SqlDataReader dr)
        {
            AutoNumber auto = new AutoNumber
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                PageId = DatabaseHelper.GetInt32("PageId", dr),
                Prefix = DatabaseHelper.GetString("Prefix",dr),
                Sequence = DatabaseHelper.GetInt32("Sequence", dr),
                Status = DatabaseHelper.GetString("Status", dr)
            };

            return auto;
        }

        private static void SetAutoNumber(SqlCommand cm, AutoNumber auto)
        {
             DatabaseHelper.InsertInt32Param("@PageId", cm, auto.PageId);
             DatabaseHelper.InsertStringNVarCharParam("@Prefix", cm, auto.Prefix);
             DatabaseHelper.InsertInt32Param("@Sequence", cm, auto.Sequence);
             DatabaseHelper.InsertStringNVarCharParam("@Status", cm, auto.Status);            
        }

        public void Save(AutoNumber auto)
        {
            string sql = "INSERT INTO AutoNumber(PageId, Prefix, Sequence, Status) VALUES (@PageId, @Prefix, @Sequence, @Status) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetAutoNumber(cm, auto);
                auto.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(AutoNumber auto)
        {
            string sql = "Update AutoNumber SET PageId=@PageId, Prefix=@Prefix, Sequence=@Sequence, Status=@Status where Id = @AutoNumberId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@AutoNumberId", cm, auto.Id);
                SetAutoNumber(cm, auto);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int AutoNumberId)
        {
            string sql = "Delete AutoNumber where Id = @AutoNumberId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@AutoNumberId", cm, AutoNumberId);
                cm.ExecuteNonQuery();
            }
        }

        public IList<AutoNumber> GetListOfAutoNumber(int pageId)
        {
            string sql;
            if (pageId == 0)

                sql = "SELECT AutoNumber.* FROM AutoNumber";
            else
                sql = "SELECT AutoNumber.* FROM AutoNumber where AutoNumber.PageId = @pageId";

            IList<AutoNumber> lstAutoNumber = new List<AutoNumber>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (pageId != 0)
                    DatabaseHelper.InsertInt32Param("@pageId", cm, pageId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstAutoNumber.Add(GetAutoNumber(dr));
                        }
                    }
                }
            }
            return lstAutoNumber;
        }
    }
}

