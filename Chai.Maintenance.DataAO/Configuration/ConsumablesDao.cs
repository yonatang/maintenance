﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;

namespace Chai.Maintenance.DataAccess.Configuration
{
    public class ConsumablesDao : BaseDao
    {
        public Consumables GetConsumablesById(int consumableId)
        {
            string sql = "SELECT Id, Name,Description FROM Consumables where Id = @consumableId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@consumableId", cm, consumableId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetConsumable(dr);
                        }
                    }
                }
            }
            return null;
        }


        private static Consumables GetConsumable(SqlDataReader dr)
        {
            Consumables consumable = new Consumables
                                {
                                    Id = DatabaseHelper.GetInt32("Id", dr),
                                    Name = DatabaseHelper.GetString("Name", dr),
                                    Description = DatabaseHelper.GetString("Description", dr)
                                };

            return consumable;
        }

        private static void SetConsumables(SqlCommand cm, Consumables consumables)
        {
            DatabaseHelper.InsertStringNVarCharParam("@Name", cm, consumables.Name);
            DatabaseHelper.InsertStringNVarCharParam("@Description", cm, consumables.Description);
        }

        public void Save(Consumables consumables)
        {
            string sql = "INSERT INTO Consumables(Name, Description) VALUES (@Name, @Description) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetConsumables(cm, consumables);
                consumables.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(Consumables consumables)
        {
            string sql = "Update Consumables SET Name =@Name, Description=@Description where Id = @consumableId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@consumableId", cm, consumables.Id);
                SetConsumables(cm, consumables);
                cm.ExecuteNonQuery();
            }
        }


        public void Delete(int consumableId)
        {
            string sql = "Delete Consumables where Id = @consumableId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@consumableId", cm, consumableId);
                cm.ExecuteNonQuery();
            }
        }

        public IList<Consumables> GetListOfConsumables(string value)
        {
            string sql;
            if (value == "")
                sql = "SELECT Consumables.* FROM Consumables order by Consumables.Name";
            else
                sql = "SELECT Consumables.* FROM Consumables where Consumables.Name = @value order by Consumables.Name";

            IList<Consumables> lstConsumables = new List<Consumables>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (value != "")
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstConsumables.Add(GetConsumable(dr));
                        }
                    }
                }
            }
            return lstConsumables;
        }
    }
}