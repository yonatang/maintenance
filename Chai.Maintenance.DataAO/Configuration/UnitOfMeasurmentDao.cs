﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.DataAccess.Configuration
{
    public class UnitOfMeasurmentDao : BaseDao
    {
        public UnitOfMeasurment GetUnitOfMeasurmentById(int unitOfMeasurmentid)
        {
            string sql = "SELECT Id, Name, Description FROM UnitOfMeasurment where Id = @unitOfMeasurmentid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@unitOfMeasurmentid", cm, unitOfMeasurmentid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetUnitOfMeasurment(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static UnitOfMeasurment GetUnitOfMeasurment(SqlDataReader dr)
        {
            UnitOfMeasurment unitOfMeasurment = new UnitOfMeasurment
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                Name = DatabaseHelper.GetString("Name", dr),
                Description = DatabaseHelper.GetString("Description", dr)
            };

            return unitOfMeasurment;
        }

        private static void SetUnitOfMeasurment(SqlCommand cm, UnitOfMeasurment unitOfMeasurment)
        {
            DatabaseHelper.InsertStringNVarCharParam("@Name", cm, unitOfMeasurment.Name);
            DatabaseHelper.InsertStringNVarCharParam("@Description", cm, unitOfMeasurment.Description);
        }

        public void Save(UnitOfMeasurment unitOfMeasurment)
        {
            string sql = "INSERT INTO UnitOfMeasurment(Name, Description) VALUES (@Name,@Description) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetUnitOfMeasurment(cm, unitOfMeasurment);
                unitOfMeasurment.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(UnitOfMeasurment unitOfMeasurment)
        {
            string sql = "Update UnitOfMeasurment SET Name =@Name , Description = @Description where Id = @unitOfMeasurmentid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@unitOfMeasurmentid", cm, unitOfMeasurment.Id);
                SetUnitOfMeasurment(cm, unitOfMeasurment);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int unitOfMeasurmentid)
        {
            string sql = "Delete UnitOfMeasurment where Id = @unitOfMeasurmentid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@unitOfMeasurmentid", cm, unitOfMeasurmentid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<UnitOfMeasurment> GetListOfUnitOfMeasurment(string value)
        {
            string sql;
            if (value == "")

                sql = "SELECT Id, Name, Description FROM UnitOfMeasurment order by Name";
            else
                sql = "SELECT Id, Name, Description FROM UnitOfMeasurment where Name = @value order by Name";

            IList<UnitOfMeasurment> lstUnitOfMeasurment = new List<UnitOfMeasurment>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (value != "")
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                }

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstUnitOfMeasurment.Add(GetUnitOfMeasurment(dr));
                        }
                    }
                }
            }
            return lstUnitOfMeasurment;
        }
    }
}
