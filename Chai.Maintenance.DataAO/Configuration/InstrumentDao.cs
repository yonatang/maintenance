﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;

namespace Chai.Maintenance.DataAccess.Configuration
{
    public class InstrumentDao:BaseDao
    {
        public Instrument GetInstrumentById(int instrumentid)
        {
            string sql = "SELECT Instrument.Id,Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate,Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate,InstrumentLookup.ServiceManual, "
                + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument "
                + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                + " LEFT JOIN Site on Instrument.Site_Id=Site.Id "
                + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where Instrument.Id = @siteid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@siteid", cm, instrumentid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetInstrument(dr);
                        }
                    }
                    dr.Close();
                }
            }
            return null;
        }

        private static Instrument GetInstrument(SqlDataReader dr)
        {

            Instrument instrument = new Instrument();
            instrument.Id = DatabaseHelper.GetInt32("Id", dr);
            instrument.SiteId = DatabaseHelper.GetInt32("Site_Id", dr);
            instrument.nameId = DatabaseHelper.GetInt32("InstrumentName_Id", dr);
            instrument.LotNumber = DatabaseHelper.GetString("LotNumber", dr);
            instrument.SerialNo = DatabaseHelper.GetString("SerialNo", dr);
            
            instrument.InstallationDate = DatabaseHelper.GetDateTime("InstallationDate", dr);
            instrument.ManufacturerId = DatabaseHelper.GetInt32("Manufacturer_Id", dr);
            instrument.VendorId = DatabaseHelper.GetInt32("Vendor_Id", dr);
            instrument.WarranityExpireDate = DatabaseHelper.GetDateTime("WarranityExpireDate", dr);
            instrument.UnderServiceContract = DatabaseHelper.GetBoolean("UnderServiceContract", dr);
            instrument.ContractwithId = DatabaseHelper.GetInt32("ContractWith_Id", dr);
            instrument.PreventiveMaintenancePeriod = DatabaseHelper.GetString("PreventiveMaintenancePeriod", dr);
            instrument.IsFunctional = DatabaseHelper.GetBoolean("IsFunctional", dr);
            instrument.InstallbyId = DatabaseHelper.GetInt32("InstallBy_ID", dr);
            instrument.Status = DatabaseHelper.GetInt32("Status", dr);
            instrument.LastPreventiveMaintenanceDate = DatabaseHelper.GetDateTime("LastPreventiveMaintenanceDate", dr);
            instrument.SiteName = DatabaseHelper.GetString("SiteName", dr);
            instrument.InstrumentName = DatabaseHelper.GetString("InstrumentName",dr);

            instrument.RegionName = DatabaseHelper.GetString("RegionName", dr);
            instrument.ManufacturerName = DatabaseHelper.GetString("ManufacturerName", dr);

            return instrument;
        }
        private static void SetInstrument(SqlCommand cm, Instrument instrument)
        {

          
                 DatabaseHelper.InsertInt32Param("Site_Id",cm,instrument.SiteId);
                 DatabaseHelper.InsertInt32Param("InstrumentName_Id", cm,instrument.nameId);
                 DatabaseHelper.InsertStringVarCharParam("LotNumber", cm,instrument.LotNumber);
                 DatabaseHelper.InsertStringVarCharParam("SerialNo", cm,instrument.SerialNo);
                 DatabaseHelper.InsertDateTimeParam("InstallationDate", cm,instrument.InstallationDate);
                 DatabaseHelper.InsertInt32Param("Manufacturer_Id", cm,instrument.ManufacturerId);
                 DatabaseHelper.InsertInt32Param("Vendor_Id", cm,instrument.VendorId);
                 DatabaseHelper.InsertDateTimeParam("WarranityExpireDate",cm,instrument.WarranityExpireDate);
                 DatabaseHelper.InsertBooleanParam("UnderServiceContract", cm,instrument.UnderServiceContract);
                 DatabaseHelper.InsertInt32Param("ContractWith_Id", cm,instrument.ContractwithId);
                 DatabaseHelper.InsertStringVarCharParam("PreventiveMaintenancePeriod", cm,instrument.PreventiveMaintenancePeriod);
                 DatabaseHelper.InsertBooleanParam("IsFunctional", cm,instrument.IsFunctional);
                 DatabaseHelper.InsertInt32Param("InstallBy_ID", cm,instrument.InstallbyId);
                 DatabaseHelper.InsertInt32Param("Status", cm, instrument.Status);
                 DatabaseHelper.InsertDateTimeParam("LastPreventiveMaintenanceDate", cm, instrument.LastPreventiveMaintenanceDate);
                 
            
        }

        public void Save(Instrument instrument)
        {
            string sql = "INSERT INTO Instrument(Site_Id, InstrumentName_Id,LotNumber,SerialNo,InstallationDate,Manufacturer_Id,Vendor_Id,WarranityExpireDate,UnderServiceContract,ContractWith_Id,PreventiveMaintenancePeriod,IsFunctional,InstallBy_ID,Status,LastPreventiveMaintenanceDate,ServiceManual) VALUES (@Site_Id,@InstrumentName_Id,@LotNumber,@SerialNo,@InstallationDate,@Manufacturer_Id,@Vendor_Id,@WarranityExpireDate,@UnderServiceContract,@ContractWith_Id,@PreventiveMaintenancePeriod,@IsFunctional,@InstallBy_ID,@Status,@LastPreventiveMaintenanceDate) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetInstrument(cm, instrument);
                instrument.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(Instrument instrument)
        {
            string sql = "Update Instrument SET Site_Id=@Site_Id, InstrumentName_Id=@InstrumentName_Id,LotNumber=@LotNumber,SerialNo=@SerialNo,InstallationDate=@InstallationDate,Manufacturer_Id=@Manufacturer_Id,Vendor_Id=@Vendor_Id,WarranityExpireDate=@WarranityExpireDate,UnderServiceContract=@UnderServiceContract,ContractWith_Id=@ContractWith_Id,PreventiveMaintenancePeriod=@PreventiveMaintenancePeriod,IsFunctional=@IsFunctional,InstallBy_ID=@InstallBy_ID,Status=@Status,LastPreventiveMaintenanceDate=@LastPreventiveMaintenanceDate where Id =@Id";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@Id", cm, instrument.Id);
                SetInstrument(cm, instrument);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int instrumentid)
        {
            string sql = "Delete Instrument where Id = @instrumentid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@instrumentid", cm, instrumentid);
                cm.ExecuteNonQuery();
            }
        }
        public IList<Instrument> GetInstrumentByRSIIG(int regionId, int siteId, int instrumentId, int instrumentCategoryId, int functionality)
        {
            
            string sql = "";
            if (siteId == 0 && instrumentId == 0 && instrumentCategoryId == 0 && regionId == 0 && functionality == 0)
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where Instrument.Status != 4 AND Instrument.Status != 3";

            }
         
            else if (siteId != 0 && instrumentId == 0 && instrumentCategoryId == 0 && regionId == 0 && functionality == 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id AND Instrument.Status != 4 AND Instrument.Status != 3";
            }
            else if (siteId != 0 && instrumentId == 0 && instrumentCategoryId != 0 && regionId == 0 && functionality == 0)
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                         + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id and @instrumentCategoryId = InstrumentCatagory.Id AND Instrument.Status != 4 AND Instrument.Status != 3";
            }
            else if (siteId != 0 && instrumentId != 0 && instrumentCategoryId == 0 && regionId == 0 && functionality == 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id and Instrument.Id = @instrumentId and Instrument.Status != 4 AND Instrument.Status != 3";
            }
            else if (siteId != 0 && instrumentId != 0 && instrumentCategoryId == 0 && regionId != 0 && functionality == 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id and Instrument.Id = @instrumentId and Instrument.Status != 4 AND Instrument.Status != 3 and @RegionId = Site.Region_Id";
            }
            else if (siteId != 0 && instrumentId == 0 && instrumentCategoryId == 0 && regionId != 0 && functionality == 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id and Instrument.Status != 4 AND Instrument.Status != 3 and @RegionId = Site.Region_Id";
            }
            else if (siteId == 0 && instrumentId == 0 && instrumentCategoryId == 0 && regionId != 0 && functionality == 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where Instrument.Status != 4 AND Instrument.Status != 3 and @RegionId = Site.Region_Id";
            }
            else if (siteId == 0 && instrumentId == 0 && instrumentCategoryId != 0 && regionId != 0 && functionality == 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                      + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where Instrument.Status != 4 AND Instrument.Status != 3 and @RegionId = Site.Region_Id and @instrumentCategoryId = InstrumentCatagory.Id";
            }
            else if (siteId != 0 && instrumentId == 0 && instrumentCategoryId != 0 && regionId != 0 && functionality == 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                      + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where  @siteId = Instrument.Site_Id and Instrument.Status != 4 AND Instrument.Status != 3 and @RegionId = Site.Region_Id and @instrumentCategoryId = InstrumentCatagory.Id";
            }
            else if (siteId != 0 && instrumentId == 0 && instrumentCategoryId == 0 && regionId != 0 && functionality == 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                      + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where  @siteId = Instrument.Site_Id and Instrument.Status != 4 AND Instrument.Status != 3 and @RegionId = Site.Region_Id";
            }
            ////////////////////////
            else if (siteId != 0 && instrumentId == 0 && instrumentCategoryId != 0 && regionId != 0 && functionality != 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id "
                    + " where  @siteId = Instrument.Site_Id and Instrument.Status != 4 AND Instrument.Status != 3 and "
                           + " @RegionId = Site.Region_Id and Instrument.Status = @functionality and @instrumentCategoryId = InstrumentCatagory.Id";
            }
            else if (siteId == 0 && instrumentId == 0 && instrumentCategoryId == 0 && regionId == 0 && functionality != 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id "
                    + " where Instrument.Status != 4 AND Instrument.Status != 3 and "
                           + " Instrument.Status = @functionality";
            }
            else if (siteId == 0 && instrumentId == 0 && instrumentCategoryId == 0 && regionId != 0 && functionality != 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id "
                    + " where  Instrument.Status != 4 AND Instrument.Status != 3 and "
                           + " @RegionId = Site.Region_Id and Instrument.Status = @functionality";
            }
            else if (siteId == 0 && instrumentId == 0 && instrumentCategoryId != 0 && regionId != 0 && functionality != 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id "
                    + " where Instrument.Status != 4 AND Instrument.Status != 3 and "
                           + " @RegionId = Site.Region_Id and Instrument.Status = @functionality and @instrumentCategoryId = InstrumentCatagory.Id";
            }
            else if (siteId == 0 && instrumentId == 0 && instrumentCategoryId != 0 && regionId == 0 && functionality != 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id "
                    + " where Instrument.Status != 4 AND Instrument.Status != 3 and "
                           + " Instrument.Status = @functionality and @instrumentCategoryId = InstrumentCatagory.Id";
            }
            else if (siteId != 0 && instrumentId == 0 && instrumentCategoryId == 0 && regionId == 0 && functionality != 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                      + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                      + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                      + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                      + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                      + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                      + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                        + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                      + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id "
                      + " where  @siteId = Instrument.Site_Id and Instrument.Status != 4 AND Instrument.Status != 3 and "
                             + " @RegionId = Site.Region_Id and Instrument.Status = @functionality ";
            }
            else if (siteId != 0 && instrumentId == 0 && instrumentCategoryId == 0 && regionId != 0 && functionality != 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id "
                    + " where  @siteId = Instrument.Site_Id and Instrument.Status != 4 AND Instrument.Status != 3 and "
                           + " @RegionId = Site.Region_Id and Instrument.Status = @functionality";
            }
            else if (siteId == 0 && instrumentId == 0 && instrumentCategoryId != 0 && regionId == 0 && functionality == 0)
            {

                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName, Vendor.VendorName As ContractedWith FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                      + " LEFT JOIN Vendor on vendor.Id = Instrument.ContractWith_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id "
                    + " where  Instrument.Status != 4 AND Instrument.Status != 3 and "
                           + " @instrumentCategoryId = InstrumentCatagory.Id";
            }
            IList<Instrument> lstinstrument = new List<Instrument>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (siteId != 0)
                    DatabaseHelper.InsertInt32Param("@siteId", cm, siteId);

                if (instrumentId != 0)
                    DatabaseHelper.InsertInt32Param("@instrumentId", cm, instrumentId);

                if (instrumentCategoryId != 0)
                    DatabaseHelper.InsertInt32Param("@instrumentCategoryId", cm, instrumentCategoryId);

                if (regionId != 0)
                    DatabaseHelper.InsertInt32Param("@RegionId", cm, regionId);

                if (functionality != 0)
                    DatabaseHelper.InsertInt32Param("@functionality", cm, functionality);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstinstrument.Add(GetInstrument(dr));
                        }
                    }
                    dr.Close();
                }
            }
            return lstinstrument;
        }
        public IList<Instrument> GetInstrumentForPreventiveMaint(int regionId, int siteId, int instrumentCategoryId, string LastPrevMainDate)
        {
            string sql = "";
            if (siteId == 0 && instrumentCategoryId == 0 && regionId == 0 && LastPrevMainDate == "")
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id";

            }
            else if (siteId != 0 && instrumentCategoryId != 0 && regionId == 0 && LastPrevMainDate == "")
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id and @instrumentCategoryId = InstrumentCatagory.Id";
            }
            else if (siteId != 0 && instrumentCategoryId != 0 && regionId != 0 && LastPrevMainDate != "")
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id and @instrumentCategoryId = InstrumentCatagory.Id and @RegionId = Site.Region_Id  and @LastPrevMainDate=Instrument.LastPreventiveMaintenanceDate";
            }
            else if (siteId != 0 && instrumentCategoryId != 0 && regionId != 0 && LastPrevMainDate == "")
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id and @instrumentCategoryId = InstrumentCatagory.Id and @RegionId = Site.Region_Id ";
            }
            else if (siteId == 0 && instrumentCategoryId != 0 && regionId == 0 && LastPrevMainDate != "")
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @instrumentCategoryId = InstrumentCatagory.Id and @RegionId = Site.Region_Id and @LastPrevMainDate=Instrument.LastPreventiveMaintenanceDate";
            }
            else if (siteId == 0 && instrumentCategoryId == 0 && regionId != 0 && LastPrevMainDate == "")
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @RegionId = Site.Region_Id";
            }
            else if (siteId == 0 && instrumentCategoryId != 0 && regionId != 0 && LastPrevMainDate == "")
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @RegionId = Site.Region_Id and @instrumentCategoryId = InstrumentCatagory.Id";
            }
            else if (siteId == 0 && instrumentCategoryId != 0 && regionId == 0 && LastPrevMainDate == "")
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @instrumentCategoryId = InstrumentCatagory.Id";
            }
            else if (siteId != 0 && instrumentCategoryId == 0 && regionId != 0 && LastPrevMainDate == "")
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id and @RegionId = Site.Region_Id";
            }
            else if (siteId == 0 && instrumentCategoryId != 0 && regionId != 0 && LastPrevMainDate != "")
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @instrumentCategoryId = InstrumentCatagory.Id and @RegionId = Site.Region_Id  and @LastPrevMainDate=Instrument.LastPreventiveMaintenanceDate";
            }
            else if (siteId != 0 && instrumentCategoryId == 0 && regionId == 0 && LastPrevMainDate == "")
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id";
            }
            else if (siteId != 0 && instrumentCategoryId == 0 && regionId == 0 && LastPrevMainDate != "") 
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id and @LastPrevMainDate=Instrument.LastPreventiveMaintenanceDate";
            }
            else if (siteId != 0 && instrumentCategoryId == 0 && regionId != 0 && LastPrevMainDate != "") 
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id and @LastPrevMainDate=Instrument.LastPreventiveMaintenanceDate and @RegionId = Site.Region_Id";
            }
            else if (siteId == 0 && instrumentCategoryId == 0 && regionId != 0 && LastPrevMainDate != "")
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @LastPrevMainDate=Instrument.LastPreventiveMaintenanceDate and @RegionId = Site.Region_Id";
            }
            IList<Instrument> lstinstrument = new List<Instrument>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (siteId != 0)
                    DatabaseHelper.InsertInt32Param("@siteId", cm, siteId);

                if (LastPrevMainDate != "")
                    DatabaseHelper.InsertStringNVarCharParam("@LastPrevMainDate", cm, LastPrevMainDate);
                
                if(instrumentCategoryId !=0)
                    DatabaseHelper.InsertInt32Param("@instrumentCategoryId", cm, instrumentCategoryId);

                if (regionId != 0)
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstinstrument.Add(GetInstrument(dr));
                        }
                    }
                    dr.Close();
                }
            }
            return lstinstrument;
        }

        public IList<Instrument> GetInstrumentForTransfer(int regionId, int siteId, int instrumentCategoryId)
        {
            string sql = "";
            if (siteId == 0 && instrumentCategoryId == 0 && regionId == 0)
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                    + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                    + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                    + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                    + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                    + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                    + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id ";

            }
            else if (siteId != 0 && instrumentCategoryId != 0 && regionId == 0)
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id and @instrumentCategoryId = InstrumentCatagory.Id";
            }
            else if (siteId != 0 && instrumentCategoryId != 0 && regionId != 0)
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id and @instrumentCategoryId = InstrumentCatagory.Id and @RegionId = Site.Region_Id ";
            }
           
            else if (siteId == 0 && instrumentCategoryId != 0 && regionId == 0)
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @instrumentCategoryId = InstrumentCatagory.Id and @RegionId = Site.Region_Id ";
            }
            else if (siteId == 0 && instrumentCategoryId == 0 && regionId != 0)
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @RegionId = Site.Region_Id ";
            }
            else if (siteId == 0 && instrumentCategoryId != 0 && regionId != 0)
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @RegionId = Site.Region_Id and @instrumentCategoryId = InstrumentCatagory.Id ";
            }
            else if (siteId != 0 && instrumentCategoryId == 0 && regionId != 0)
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id and @RegionId = Site.Region_Id ";
            }
            else if (siteId != 0 && instrumentCategoryId == 0 && regionId == 0)
            {
                sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                       + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                       + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                       + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                       + " LEFT JOIN InstrumentCatagory on  InstrumentCatagory.Id = InstrumentLookup.InstrumentCategoryId"
                       + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                       + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                       + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where @siteId = Instrument.Site_Id";
            }
            IList<Instrument> lstinstrument = new List<Instrument>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (siteId != 0)
                    DatabaseHelper.InsertInt32Param("@siteId", cm, siteId);

                 if (instrumentCategoryId != 0)
                    DatabaseHelper.InsertInt32Param("@instrumentCategoryId", cm, instrumentCategoryId);

               // if (regionId != 0)
                    DatabaseHelper.InsertInt32Param("@RegionId", cm, regionId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstinstrument.Add(GetInstrument(dr));
                        }
                    }
                    dr.Close();
                }
            }
            return lstinstrument;
        }
        public IList<Instrument> GetListOfInstrument(int findby, string value, int instrumentid,int siteId,int regionId)
        {
            string sql = "";
            if (findby == 0 && value == "")
            {
                if (regionId == 1000)
                {
                    sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                        + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate , "
                        + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                        + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                        + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                        + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                        + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id";
                }
                else
                {
                    sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                            + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate "
                            + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                            + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                            + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                            + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                            + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id Where Region.Id = @RegionId";
                }
            }
            else if (findby == 1 || findby == 2)
            {
            //    sql = "SELECT Instrument.Id,Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate,Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, Site.SiteName,InstrumentLookup.InstrumentName FROM Instrument"
            //+ " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id = InstrumentLookup.Id"
            //+ " LEFT JOIN Site on Instrument.Site_Id=Site.Id where Instrument.SerialNo = @value or Instrument.LotNumber = @value order by Instrument.InstallationDate";
                if (regionId == 1000)
                {
                    sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                        + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                        + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                        + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                        + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                        + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                        + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where Instrument.SerialNo = @value or Instrument.LotNumber = @value order by Instrument.InstallationDate";
                }
                else 
                
                {
                    sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                        + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                        + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                        + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                        + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                        + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                        + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where Region.Id = @RegionId AND Instrument.SerialNo = @value or Instrument.LotNumber = @value order by Instrument.InstallationDate";
                }
            }
            else if(findby == 3)
            {
                if (regionId == 1000)
                {

                    sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                        + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                        + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                        + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                        + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                        + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                        + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where Instrument.InstrumentName_Id = @InstrumentName_Id order by Instrument.InstallationDate";
                }
                else 
                
                {
                    sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                        + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                        + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                        + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                        + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                        + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                        + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where Region.Id = @RegionId and Instrument.InstrumentName_Id = @InstrumentName_Id order by Instrument.InstallationDate";
                }
            }
            else if (findby == 4)
            {
                if (regionId == 1000)
                {
                    sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                        + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                        + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                        + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                        + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                        + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                        + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where Instrument.Site_Id = @Site_Id order by Instrument.InstallationDate";
                }
                else 
                {
                    sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                         + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                         + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                         + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                         + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                         + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                         + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where Region.Id = @regionId AND Instrument.Site_Id = @Site_Id order by Instrument.InstallationDate";
                }
            }
            IList<Instrument> lstinstrument = new List<Instrument>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (findby == 0 && value == "")
                {
                    if (regionId != 1000)
                    {
                        DatabaseHelper.InsertInt32Param("@RegionId", cm, regionId);
                    }
                }
                else if (findby == 1 || findby == 2)
                {
                    if (regionId == 1000)
                    {
                        DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                    }
                    else
                    {
                        DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                        DatabaseHelper.InsertInt32Param("@RegionId", cm, regionId);
                    }
                }
                else if (findby == 3)
                {
                    if (regionId == 1000)
                    {
                        DatabaseHelper.InsertInt32Param("@InstrumentName_Id", cm, instrumentid);
                    }
                    else
                    {
                        DatabaseHelper.InsertInt32Param("@InstrumentName_Id", cm, instrumentid);
                        DatabaseHelper.InsertInt32Param("@RegionId", cm, regionId);
                    }
                }
                else if (findby == 4)
                {
                    if (regionId == 1000)
                    {
                        DatabaseHelper.InsertInt32Param("@site_Id", cm, siteId);
                    }
                    else 
                    {
                        DatabaseHelper.InsertInt32Param("@site_Id", cm, siteId);
                        DatabaseHelper.InsertInt32Param("@RegionId", cm, regionId);
                    }
                    
                }
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstinstrument.Add(GetInstrument(dr));
                        }
                    }
                    dr.Close();
                }
            }
            return lstinstrument;
        }

        public IList<Instrument> GetInstrumentBySiteID(int siteId)
        {
            string sql;
            //sql = "SELECT Instrument.Id,Instrument.Site_Id, Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate,Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, Site.SiteName,InstrumentLookup.InstrumentName FROM Instrument"
            //+ " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
            //+ " LEFT JOIN Site on Instrument.Site_Id=Site.Id where Instrument.Site_Id = @siteId order by Instrument.InstallationDate";

            sql = "SELECT Instrument.Id, Instrument.Site_Id,Instrument.InstrumentName_Id,Instrument.LotNumber,Instrument.SerialNo,Instrument.InstallationDate,Instrument.Manufacturer_Id,Instrument.Vendor_Id,Instrument.WarranityExpireDate, "
                     + " Instrument.UnderServiceContract,Instrument.ContractWith_Id,Instrument.PreventiveMaintenancePeriod,Instrument.IsFunctional,Instrument.InstallBy_ID,Instrument.Status,Instrument.LastPreventiveMaintenanceDate, "
                     + " Site.SiteName,InstrumentLookup.InstrumentName,Region.RegionName, Manufacturer.Name as ManufacturerName FROM Instrument"
                     + " LEFT JOIN InstrumentLookup on Instrument.InstrumentName_Id=InstrumentLookup.Id"
                     + " LEFT JOIN Site on Instrument.Site_Id=Site.Id"
                     + " LEFT JOIN Region on Region.Id = Site.Region_Id"
                     + " LEFT JOIN Manufacturer on Manufacturer.Id = Instrument.Manufacturer_Id where Instrument.Site_Id = @siteId order by Instrument.InstallationDate";
            IList<Instrument> lstinstrument = new List<Instrument>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {

                DatabaseHelper.InsertInt32Param("@siteId", cm, siteId);


                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstinstrument.Add(GetInstrument(dr));
                        }
                    }
                    dr.Close();
                }
            }
            return lstinstrument;
        }
        public void SaveTransferedInstrument(Instrument instrument, int OldInstrumentId, int NewSiteId)
        {
            string sql = "INSERT INTO Instrument(Site_Id, InstrumentName_Id,LotNumber,SerialNo,InstallationDate,Manufacturer_Id,Vendor_Id,WarranityExpireDate,UnderServiceContract,ContractWith_Id,PreventiveMaintenancePeriod,IsFunctional,InstallBy_ID,Status,LastPreventiveMaintenanceDate,ParentId) " +
                " VALUES (@NewSiteId,@InstrumentName_Id,@LotNumber,@SerialNo,@InstallationDate,@Manufacturer_Id,@Vendor_Id,@WarranityExpireDate,@UnderServiceContract,@ContractWith_Id,@PreventiveMaintenancePeriod,@IsFunctional,@InstallBy_ID,@Status,@LastPreventiveMaintenanceDate,@OldInstrumentId) SELECT @@identity";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@OldInstrumentId", cm, OldInstrumentId);
                DatabaseHelper.InsertInt32Param("@NewSiteId", cm, NewSiteId);
                SetInstrument(cm, instrument);
                instrument.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }
        
    }
}
