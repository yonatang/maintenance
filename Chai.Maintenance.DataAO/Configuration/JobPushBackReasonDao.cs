﻿ 
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.DataAccess.Configuration
{
    public class JobPushBackReasonDao : BaseDao
    {
        public JobPushBackReason GetJobPushBackReasonById(int jobPushBackReasonId)
        {
            string sql = "SELECT Id, Reason FROM JobPushBackReason where Id = @jobPushBackReasonId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@jobPushBackReasonId", cm, jobPushBackReasonId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetJobPushBackReason(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static JobPushBackReason GetJobPushBackReason(SqlDataReader dr)
        {
            JobPushBackReason jobPushBackReason = new JobPushBackReason
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                Reason = DatabaseHelper.GetString("Reason", dr),

            };

            return jobPushBackReason;
        }

        private static void SetJobPushBackReason(SqlCommand cm, JobPushBackReason jobPushBackReason)
        {
            DatabaseHelper.InsertStringNVarCharParam("@Reason", cm, jobPushBackReason.Reason);

        }

        public void Save(JobPushBackReason jobPushBackReason)
        {
            string sql = "INSERT INTO JobPushBackReason(Reason) VALUES (@Reason) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetJobPushBackReason(cm, jobPushBackReason);
                jobPushBackReason.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(JobPushBackReason jobPushBackReason)
        {
            string sql = "Update JobPushBackReason SET Reason =@Reason where Id = @jobPushBackReasonId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@jobPushBackReasonId", cm, jobPushBackReason.Id);
                SetJobPushBackReason(cm, jobPushBackReason);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int jobPushBackReasonId)
        {
            string sql = "Delete JobPushBackReason where Id = @jobPushBackReasonId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@jobPushBackReasonId", cm, jobPushBackReasonId);
                cm.ExecuteNonQuery();
            }
        }

        public IList<JobPushBackReason> GetJobPushBackReason(string value)
        {
            string sql;
            if (value == "")

                sql = "SELECT Id, Reason FROM JobPushBackReason";
            else
                sql = "SELECT Id, Reason FROM JobPushBackReason where Reason = @value";

            IList<JobPushBackReason> lstJobPushBackReason = new List<JobPushBackReason>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (value != "")
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                }

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstJobPushBackReason.Add(GetJobPushBackReason(dr));
                        }
                    }
                }
            }
            return lstJobPushBackReason;
        }
    }
}
