﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.DataAccess.Configuration
{
  public class InstrumentTypeDao : BaseDao
    {
        public InstrumentType GetInstrumentTypeById(int instrumenttypeid)
        {
            //string sql = "SELECT Id, InstrumentCategoryId, InstrumentName FROM InstrumentLookup where Id = @instrumenttypeid";
            string sql = "SELECT IL.Id, IL.InstrumentCategoryId, IC.Name, IL.InstrumentName,IL.ServiceManual,IL.ServiceManualFileName,IL.ServiceManualFileSize,IL.ServiceManualContentType FROM InstrumentLookup IL LEFT join InstrumentCatagory IC ON IC.Id = IL.InstrumentCategoryId where IL.Id = @instrumenttypeid ORDER BY IC.Name";
            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@instrumenttypeid", cm, instrumenttypeid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetInstrumentType(dr);
                        }
                    }
                    dr.Close();
                }
            }
            return null;
        }

        private static InstrumentType GetInstrumentType(SqlDataReader dr)
        {
            InstrumentType InstrumentType = new InstrumentType
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                InstrumentCatagoryId = DatabaseHelper.GetInt32("InstrumentCategoryId", dr),
                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                Name = DatabaseHelper.GetString("Name", dr),
               InstrumentService = DatabaseHelper.GetBytes("ServiceManual", dr),
                FileName = DatabaseHelper.GetString("ServiceManualFileName", dr),
                FileSize = DatabaseHelper.GetInt32("ServiceManualFileSize", dr),
                ContentType = DatabaseHelper.GetString("ServiceManualContentType", dr)
            };

            return InstrumentType;
        }
        private static void SetInstrumentType(SqlCommand cm, InstrumentType instrumenttype)
        {
            DatabaseHelper.InsertInt32Param("@InstrumentCatagoryId", cm, instrumenttype.InstrumentCatagoryId);
            DatabaseHelper.InsertStringNVarCharParam("@InstrumentName", cm, instrumenttype.InstrumentName);
            DatabaseHelper.InsertImageParam("ServiceManual", cm, instrumenttype.InstrumentService);
            DatabaseHelper.InsertStringNVarCharParam("@ServiceManualFileName", cm, instrumenttype.FileName);
            DatabaseHelper.InsertStringNVarCharParam("@ServiceManualContentType", cm, instrumenttype.ContentType);
            DatabaseHelper.InsertInt32Param("@ServiceManualFileSize", cm, instrumenttype.FileSize);
        }

        public void Save(InstrumentType instrumenttype)
        {
            string sql = "INSERT INTO InstrumentLookup(InstrumentCategoryId, InstrumentName,ServiceManual,ServiceManualFileName,ServiceManualContentType,ServiceManualFileSize) VALUES (@InstrumentCatagoryId,@InstrumentName,@ServiceManual,@ServiceManualFileName,@ServiceManualContentType,@ServiceManualFileSize) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetInstrumentType(cm, instrumenttype);
                instrumenttype.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(InstrumentType instrumenttype)
        {
            string sql = "Update InstrumentLookup SET InstrumentCategoryId =@InstrumentCatagoryId , InstrumentName = @InstrumentName,ServiceManual=@ServiceManual, ServiceManualFileName=@ServiceManualFileName,ServiceManualContentType=@ServiceManualContentType,ServiceManualFileSize=@ServiceManualFileSize where Id = @InstrumentTypeId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@InstrumentTypeId", cm, instrumenttype.Id);
                SetInstrumentType(cm, instrumenttype);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int instrumenttypeid)
        {
            string sql = "Delete InstrumentLookup where Id = @InstrumentTypeId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@InstrumentTypeId", cm, instrumenttypeid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<InstrumentType> GetListOfInstrumentType(int findby, string value,int ddlvalue)
        {
            string sql;
            if (findby == 0)

                //sql = "SELECT Id, InstrumentCategoryId, InstrumentName FROM InstrumentLookup order by InstrumentName";
                sql = "SELECT IL.Id, IL.InstrumentCategoryId, IC.Name, IL.InstrumentName,IL.ServiceManual,IL.ServiceManualFileName,IL.ServiceManualContentType,IL.ServiceManualFileSize " +
                      " FROM InstrumentLookup IL " +
                      " JOIN InstrumentCatagory IC ON IC.Id = IL.InstrumentCategoryId ORDER BY IC.Name";
            else if(findby ==1)
                //sql = "SELECT Id, InstrumentCategoryId, InstrumentName FROM InstrumentLookup where InstrumentCategoryId = @ddlvalue order by InstrumentName";
                sql = "SELECT IL.Id, IL.InstrumentCategoryId, IC.Name, IL.InstrumentName, IL.ServiceManual,IL.ServiceManualFileName,IL.ServiceManualContentType,IL.ServiceManualFileSize FROM InstrumentLookup IL left join InstrumentCatagory IC ON IC.Id = IL.InstrumentCategoryId where InstrumentCategoryId = @ddlvalue ORDER BY IC.Name";

            else
                //sql = "SELECT Id, InstrumentCategoryId, InstrumentName FROM InstrumentLookup where InstrumentName = @value order by InstrumentName";
                sql = "SELECT IL.Id, IL.InstrumentCategoryId, IC.Name, IL.InstrumentName,IL.ServiceManual,IL.ServiceManualFileName,IL.ServiceManualContentType,IL.ServiceManualFileSize FROM InstrumentLookup IL left join InstrumentCatagory IC ON IC.Id = IL.InstrumentCategoryId where InstrumentName = @value ORDER BY IC.Name";


            IList<InstrumentType> lstInstrumentType = new List<InstrumentType>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (findby == 1 )
                {
                    DatabaseHelper.InsertInt32Param("@ddlvalue", cm, ddlvalue);
                }
                else if (findby == 2)
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                }
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstInstrumentType.Add(GetInstrumentType(dr));
                        }
                    }
                    dr.Close();
                }
            }
            return lstInstrumentType;
        }
    }
}
