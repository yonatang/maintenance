﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.DataAccess.Configuration
{
    public class VendorDao : BaseDao
    {
        public Vendor GetVendorById(int vendorid)
        {
            string sql = "SELECT Id, VendorName FROM Vendor where Id = @vendorid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@vendorid", cm, vendorid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetVendor(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static Vendor GetVendor(SqlDataReader dr)
        {
            Vendor vendor = new Vendor
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                Name = DatabaseHelper.GetString("VendorName", dr),

            };

            return vendor;
        }

        private static void SetVendor(SqlCommand cm, Vendor vendor)
        {
            DatabaseHelper.InsertStringNVarCharParam("@Name", cm, vendor.Name);

        }

        public void Save(Vendor vendor)
        {
            string sql = "INSERT INTO Vendor(VendorName) VALUES (@Name) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetVendor(cm, vendor);
                vendor.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(Vendor vendor)
        {
            string sql = "Update Vendor SET VendorName =@Name where Id = @vendorid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@vendorid", cm, vendor.Id);
                SetVendor(cm, vendor);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int vendorid)
        {
            string sql = "Delete Vendor where Id = @vendorid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@vendorid", cm, vendorid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<Vendor> GetListOfVendor(string value)
        {
            string sql;
            if (value == "")

                sql = "SELECT Id, VendorName FROM Vendor order by VendorName";
            else
                sql = "SELECT Id, VendorName FROM Vendor where VendorName = @value order by VendorName";

            IList<Vendor> lstVendor = new List<Vendor>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (value != "")
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                }

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstVendor.Add(GetVendor(dr));
                        }
                    }
                }
            }
            return lstVendor;
        }
    }
}
