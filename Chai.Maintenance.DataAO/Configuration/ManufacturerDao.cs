﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.DataAccess.Configuration
{
    public class ManufacturerDao : BaseDao
    {
        public Manufacturer GetManufacturerById(int manufacturerid)
        {
            string sql = "SELECT Id, Name FROM Manufacturer where Id = @manufacturerid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@manufacturerid", cm, manufacturerid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetManufacturer(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static Manufacturer GetManufacturer(SqlDataReader dr)
        {
            Manufacturer manufacturer = new Manufacturer
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                Name = DatabaseHelper.GetString("Name", dr),
                 
            };

            return manufacturer;
        }

        private static void SetManufacturer(SqlCommand cm, Manufacturer manufacturer)
        {
            DatabaseHelper.InsertStringNVarCharParam("@Name", cm, manufacturer.Name);
            
        }

        public void Save(Manufacturer manufacturer)
        {
            string sql = "INSERT INTO Manufacturer(Name) VALUES (@Name) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetManufacturer(cm, manufacturer);
                manufacturer.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(Manufacturer manufacturer)
        {
            string sql = "Update Manufacturer SET Name =@Name where Id = @manufacturerid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@manufacturerid", cm, manufacturer.Id);
                SetManufacturer(cm, manufacturer);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int manufacturerid)
        {
            string sql = "Delete Manufacturer where Id = @manufacturerid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@manufacturerid", cm, manufacturerid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<Manufacturer> GetListOfManufacturer(string value)
        {
            string sql;
            if (value == "")

                sql = "SELECT Id, Name FROM Manufacturer order by Name";
            else
                sql = "SELECT Id, Name FROM Manufacturer where Name = @value order by Name";

            IList<Manufacturer> lstManufacturer = new List<Manufacturer>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (value != "")
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                }

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstManufacturer.Add(GetManufacturer(dr));
                        }
                    }
                }
            }
            return lstManufacturer;
        }
    }
}
