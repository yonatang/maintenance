﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.DataAccess.Configuration
{
    public class SiteDao :BaseDao
    {
         public Site GetSiteById(int siteid)
        {
            string sql = "SELECT Id, Region_Id, SiteName,SiteCode,City,SiteType_Id,Telephone1,Telephone2,Address,Email,SiteLabTechnicianUser_Id,Status FROM Site where Id = @siteid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@siteid", cm, siteid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetSite(dr);
                        }
                    }
                }
            }
            return null;
        }

         public Site GetRegionIDBySiteId(int siteid)
         {
             string sql = "SELECT Id, Region_Id, SiteName,SiteCode,City,SiteType_Id,Telephone1,Telephone2,Address,Email,SiteLabTechnicianUser_Id,Status FROM Site where Id = @siteid";

             using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
             {
                 DatabaseHelper.InsertInt32Param("@siteid", cm, siteid);

                 using (SqlDataReader dr = cm.ExecuteReader())
                 {
                     if (dr != null)
                     {
                         if (dr.HasRows)
                         {
                             dr.Read();
                             return GetSite(dr);
                         }
                     }
                 }
             }
             return null;
         }
        private static Site GetSite(SqlDataReader dr)
        {
            Site site = new Site
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                RegionId = DatabaseHelper.GetInt32("Region_Id", dr),
                Name = DatabaseHelper.GetString("SiteName", dr),
                Code = DatabaseHelper.GetString("SiteCode", dr),
                City = DatabaseHelper.GetString("City", dr),
                typeId = DatabaseHelper.GetInt32("SiteType_Id", dr),
                Telephone1 = DatabaseHelper.GetString("Telephone1", dr),
                Telephone2 = DatabaseHelper.GetString("Telephone2", dr),
                Address = DatabaseHelper.GetString("Address", dr),
                Email = DatabaseHelper.GetString("Email", dr),
               // labtechnicianuserId = DatabaseHelper.GetInt32("SiteLabTechnicianUser_Id", dr),
                //Status= DatabaseHelper.GetBoolean("Status",dr)
            };

            return site;
        }
        private static void SetSite(SqlCommand cm, Site site)
        {
            
              
              DatabaseHelper.InsertInt32Param("@Region_Id", cm, site.RegionId);
              DatabaseHelper.InsertStringNVarCharParam("@SiteName", cm, site.Name);
              DatabaseHelper.InsertStringNVarCharParam("@SiteCode", cm, site.Code);
              DatabaseHelper.InsertStringNVarCharParam("@City", cm, site.City);
              DatabaseHelper.InsertInt32Param("@SiteType_Id", cm, site.typeId);
              DatabaseHelper.InsertStringNVarCharParam("@Telephone1", cm, site.Telephone1);
              DatabaseHelper.InsertStringNVarCharParam("@Telephone2", cm, site.Telephone2);
              DatabaseHelper.InsertStringNVarCharParam("@Address", cm, site.Address);
              DatabaseHelper.InsertStringNVarCharParam("@Email", cm, site.Email);
              DatabaseHelper.InsertInt32Param("@SiteLabTechnicianUser_Id", cm, site.labtechnicianuserId);
              DatabaseHelper.InsertBooleanParam("@Status", cm, site.Status);
        }

        public void Save(Site site)
        {
            string sql = "INSERT INTO Site(Region_Id, SiteName,SiteCode,City,SiteType_Id,Telephone1,Telephone2,Address,Email,SiteLabTechnicianUser_Id,Status) VALUES (@Region_Id,@SiteName,@SiteCode,@City,@SiteType_Id,@Telephone1,@Telephone2,@Address,@Email,@SiteLabTechnicianUser_Id,@Status) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetSite(cm, site);
                site.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(Site site)
        {
            string sql = "Update Site SET Region_Id=@Region_Id, SiteName=@SiteName,SiteCode=@SiteCode,City=@City,SiteType_Id=SiteType_Id,Telephone1=@Telephone1,Telephone2=@Telephone2,Address=@Address,Email=@Email,SiteLabTechnicianUser_Id=@SiteLabTechnicianUser_Id,Status=@Status where Id = @siteid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@siteid", cm, site.Id);
                SetSite(cm, site);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(Site site)
        {
            string sql = "Delete Site where Id = @siteid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@siteid", cm, site.Id);
                cm.ExecuteNonQuery();
            }
        }

        public IList<Site> GetListOfSite(int findby,string value,int regionId)
        {
            string sql;
            if (findby == 2 || findby == 3)
                sql = "SELECT Id, Region_Id, SiteName,SiteCode,City,SiteType_Id,Telephone1,Telephone2,Address,Email FROM Site where Region_Id = @ddlvalue and SiteName = @value or SiteCode = @value order by SiteName";
            else if(findby == 1)
                sql = "SELECT Id, Region_Id, SiteName,SiteCode,City,SiteType_Id,Telephone1,Telephone2,Address,Email FROM Site where Region_Id = @ddlvalue order by SiteName";
            else
                sql = "SELECT Id, Region_Id, SiteName,SiteCode,City,SiteType_Id,Telephone1,Telephone2,Address,Email FROM Site  where Region_Id = @ddlvalue order by SiteName ";
            
            IList<Site> lstsite = new List<Site>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (findby ==2 || findby ==3)
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                    //DatabaseHelper.InsertInt32Param("@ddlvalue", cm, regionId);
                }
                else if (findby == 1)
                {
                   // DatabaseHelper.InsertInt32Param("@ddlvalue", cm, regionId);
                }
                DatabaseHelper.InsertInt32Param("@ddlvalue", cm, regionId);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstsite.Add(GetSite(dr));
                        }
                    }
                }
            }
            return lstsite;
        }

        public IList<Site> GetSiteByRegionID(int regionId)
        {
            string sql;
         
                sql = "SELECT Id, Region_Id, SiteName,SiteCode,City,SiteType_Id,Telephone1,Telephone2,Address,Email FROM Site where Region_Id = @regionId order by SiteName";
            

            IList<Site> lstsite = new List<Site>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
               
                    DatabaseHelper.InsertInt32Param("@regionId", cm, regionId);
                

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstsite.Add(GetSite(dr));
                        }
                    }
                }
            }
            return lstsite;
        }
    }


    }

