﻿ 
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.DataAccess.Configuration
{
    public class SiteTypeDao : BaseDao
    {
        public SiteType GetSiteTypeById(int siteTypeId)
        {
            string sql = "SELECT Id, Name FROM SiteType where Id = @siteTypeId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@siteTypeId", cm, siteTypeId);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetSiteType(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static SiteType GetSiteType(SqlDataReader dr)
        {
            SiteType siteType = new SiteType
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                Name = DatabaseHelper.GetString("Name", dr),
                 
            };

            return siteType;
        }

        private static void SetSiteType(SqlCommand cm, SiteType siteType)
        {
            DatabaseHelper.InsertStringNVarCharParam("@Name", cm, siteType.Name);
            
        }

        public void Save(SiteType siteType)
        {
            string sql = "INSERT INTO SiteType(Name) VALUES (@Name) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetSiteType(cm, siteType);
                siteType.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(SiteType siteType)
        {
            string sql = "Update SiteType SET Name =@Name where Id = @siteTypeId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@siteTypeId", cm, siteType.Id);
                SetSiteType(cm, siteType);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int siteTypeId)
        {
            string sql = "Delete SiteType where Id = @siteTypeId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@siteTypeId", cm, siteTypeId);
                cm.ExecuteNonQuery();
            }
        }

        public IList<SiteType> GetListOfSiteType(string value)
        {
            string sql;
            if (value == "")

                sql = "SELECT Id, Name FROM SiteType order by Name";
            else
                sql = "SELECT Id, Name FROM SiteType where Name = @value order by Name";

            IList<SiteType> lstSiteType = new List<SiteType>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (value != "")
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                }

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstSiteType.Add(GetSiteType(dr));
                        }
                    }
                }
            }
            return lstSiteType;
        }
    }
}
