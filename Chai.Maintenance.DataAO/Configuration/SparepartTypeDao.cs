﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.DataAccess.Configuration
{
   public class SparepartTypeDao:BaseDao
    {
        public SparepartType GetSparepartTypeById(int spareparttypeid)
        {
            string sql = " SELECT S.Id, S.Name, S.InstrumentLookup_Id, S.PartNumber, S.UOM, S.ReorderQty, S.Description, IC.Name as InstrumentName, UM.Name as UOMName FROM SparepartType S " +
                         " Left JOIN InstrumentCatagory  IC ON IC.Id = S.InstrumentLookup_Id " +
                         " Left JOIN UnitOfMeasurment UM ON UM.Id = S.UOM where S.Id = @spareparttypeid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@spareparttypeid", cm, spareparttypeid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetSparepartType(dr);
                        }
                    }
                    dr.Close();
                }
            }
            return null;
        }

        private static SparepartType GetSparepartType(SqlDataReader dr)
        {
            SparepartType spareparttype = new SparepartType
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                Name = DatabaseHelper.GetString("Name", dr),
                InstrumentlookupId = DatabaseHelper.GetInt32("InstrumentLookup_Id", dr),
                PartNumber = DatabaseHelper.GetString("PartNumber", dr),
                Uom = DatabaseHelper.GetInt32("UOM", dr),
                ReorderQty = DatabaseHelper.GetInt32("ReorderQty", dr),
                Description = DatabaseHelper.GetString("Description", dr),

                InstrumentName = DatabaseHelper.GetString("InstrumentName", dr),
                UOMName = DatabaseHelper.GetString("UOMName", dr)

            };

            return spareparttype;
        }
        private static void SetSparepartType(SqlCommand cm, SparepartType spareparttype)
        {
            DatabaseHelper.InsertStringNVarCharParam("@Name", cm, spareparttype.Name);
            DatabaseHelper.InsertInt32Param("@InstrumentlookupId", cm, spareparttype.InstrumentlookupId);
            DatabaseHelper.InsertStringNVarCharParam("@PartNumber", cm, spareparttype.PartNumber);
            DatabaseHelper.InsertInt32Param("@UOM", cm, spareparttype.Uom);
            DatabaseHelper.InsertInt32Param("@ReorderQty", cm, spareparttype.ReorderQty);
            DatabaseHelper.InsertStringNVarCharParam("@Description", cm, spareparttype.Description);
        }

        public void Save(SparepartType spareparttype)
        {
            string sql = "INSERT INTO SparepartType(Name, InstrumentLookup_Id,PartNumber,UOM,ReorderQty,Description) VALUES (@Name,@InstrumentlookupId,@PartNumber,@UOM,@ReorderQty,@Description) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetSparepartType(cm, spareparttype);
                spareparttype.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(SparepartType spareparttype)
        {
            string sql = "Update SparepartType SET Name =@Name , InstrumentLookup_Id = @InstrumentlookupId,PartNumber=@PartNumber,UOM=@UOM,ReorderQty=@ReorderQty,Description=@Description where Id = @spareparttypeid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@spareparttypeid", cm, spareparttype.Id);
                SetSparepartType(cm, spareparttype);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int spareparttypeid)
        {
            string sql = "Delete SparepartType where Id = @spareparttypeid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@spareparttypeid", cm, spareparttypeid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<SparepartType> GetListOfSparepartType(int findby, string value,int ddlvalue)
        {
            string sql="";
            if (findby == 0)


                sql = " SELECT S.Id, S.Name, S.InstrumentLookup_Id, S.PartNumber, S.UOM, S.ReorderQty, S.Description, IL.InstrumentName, UM.Name as UOMName FROM SparepartType S  " +
                                         " LEFT JOIN InstrumentLookup  IL ON IL.Id = S.InstrumentLookup_Id " +
                                         " INNER JOIN UnitOfMeasurment UM ON UM.Id = S.UOM ";

            else if (findby == 1 || findby == 2)

                //sql = "SELECT Id, Name, InstrumentLookup_Id,PartNumber,UOM,ReorderQty,Description FROM SparepartType where Name = @value or PartNumber = @value order by Name";
                sql = " SELECT S.Id, S.Name, S.InstrumentLookup_Id, S.PartNumber, S.UOM, S.ReorderQty, S.Description,  IL.InstrumentName, UM.Name as UOMName FROM SparepartType S  " +
                                        " LEFT JOIN InstrumentLookup  IL ON IL.Id = S.InstrumentLookup_Id " +
                                        " LEFT JOIN UnitOfMeasurment UM ON UM.Id = S.UOM where S.Name = @value or S.PartNumber = @value order by S.Name ";
            else if (findby == 3)

                //sql = "SELECT Id, Name, InstrumentLookup_Id,PartNumber,UOM,ReorderQty,Description FROM SparepartType where InstrumentLookup_Id = @InstrumentLookup_Id";
                sql = " SELECT S.Id, S.Name, S.InstrumentLookup_Id, S.PartNumber, S.UOM, S.ReorderQty, S.Description,  IL.InstrumentName, UM.Name as UOMName FROM SparepartType S  " +
                                       " LEFT JOIN InstrumentLookup  IL ON IL.Id = S.InstrumentLookup_Id " +
                                       " LEFT JOIN UnitOfMeasurment UM ON UM.Id = S.UOM where S.InstrumentLookup_Id = @InstrumentLookup_Id order by S.Name ";

            IList<SparepartType> lstsparepart = new List<SparepartType>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (findby == 1 ||  findby == 2)
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                }
                else if(findby==3)
                    
                {
                    DatabaseHelper.InsertInt32Param("@InstrumentLookup_Id", cm, ddlvalue);
                }

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstsparepart.Add(GetSparepartType(dr));
                        }
                    }
                    dr.Close();
                }
            }
            return lstsparepart;
        }
    }
}
