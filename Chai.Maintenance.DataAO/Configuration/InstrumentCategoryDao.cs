﻿ 
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.DataAccess.Configuration
{
    public class InstrumentCategoryDao : BaseDao
    {
        public InstrumentCategory GetInstrumentCategoryById(int instrumentCategoryid)
        {
            string sql = "SELECT Id, Name, Description FROM InstrumentCatagory where Id = @instrumentCategoryid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@instrumentCategoryid", cm, instrumentCategoryid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetInstrumentCategory(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static InstrumentCategory GetInstrumentCategory(SqlDataReader dr)
        {
            InstrumentCategory instrumentCategory = new InstrumentCategory
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                Name = DatabaseHelper.GetString("Name", dr),
                Description = DatabaseHelper.GetString("Description", dr) 
            };

            return instrumentCategory;
        }

        private static void SetInstrumentCategory(SqlCommand cm, InstrumentCategory instrumentCategory)
        {
            DatabaseHelper.InsertStringNVarCharParam("@Name", cm, instrumentCategory.Name);
            DatabaseHelper.InsertStringNVarCharParam("@Description", cm, instrumentCategory.Description);
            
        }

        public void Save(InstrumentCategory instrumentCategory)
        {
            string sql = "INSERT INTO InstrumentCatagory(Name, Description) VALUES (@Name, @Description) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetInstrumentCategory(cm, instrumentCategory);
                instrumentCategory.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(InstrumentCategory instrumentCategory)
        {
            string sql = "Update InstrumentCatagory SET Name =@Name, Description=@Description  where Id = @instrumentCategoryid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@instrumentCategoryid", cm, instrumentCategory.Id);
                SetInstrumentCategory(cm, instrumentCategory);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int instrumentCategoryid)
        {
            string sql = "Delete InstrumentCatagory where Id = @instrumentCategoryid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@instrumentCategoryid", cm, instrumentCategoryid);
                cm.ExecuteNonQuery();
            }
        }

        public IList<InstrumentCategory> GetListOfInstrumentCategorys(string value)
        {
            string sql;
            if (value == "")

                sql = "SELECT Id, Name, Description FROM InstrumentCatagory order by Name";
            else
                sql = "SELECT Id, Name, Description FROM InstrumentCatagory where Name = @value order by Name";

            IList<InstrumentCategory> lstInstrumentCategory = new List<InstrumentCategory>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (value != "")
                {
                    DatabaseHelper.InsertStringVarCharParam("@value", cm, value);
                }

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstInstrumentCategory.Add(GetInstrumentCategory(dr));
                        }
                    }
                }
            }
            return lstInstrumentCategory;
        }
    }
}
