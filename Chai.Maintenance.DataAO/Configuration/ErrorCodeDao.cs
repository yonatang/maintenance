﻿
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Chai.Maintenance.CoreDomain.Configuration;
namespace Chai.Maintenance.DataAccess.Configuration
{
    public class ErrorCodeDao : BaseDao
    {
        public ErrorCode GetErrorCodeById(int errorCodeid)
        {
            string sql = "SELECT ErrorCode.*, ProblemType.Name AS ProblemTypeName  FROM ErrorCode " +
                      " LEFT JOIN ProblemType ON ProblemType.Id = ErrorCode.ProblemTypeId where ErrorCode.Id = @errorCodeid";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@errorCodeid", cm, errorCodeid);

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        if (dr.HasRows)
                        {
                            dr.Read();
                            return GetErrorCode(dr);
                        }
                    }
                }
            }
            return null;
        }

        private static ErrorCode GetErrorCode(SqlDataReader dr)
        {
            ErrorCode errorCode = new ErrorCode
            {
                Id = DatabaseHelper.GetInt32("Id", dr),
                Name = DatabaseHelper.GetString("Name", dr),
                ProblemTypeId = DatabaseHelper.GetInt32("ProblemTypeId", dr),
                ProblemTypeName = DatabaseHelper.GetString("ProblemTypeName", dr)
            };

            return errorCode;
        }

        private static void SetErrorCode(SqlCommand cm, ErrorCode errorCode)
        {
            DatabaseHelper.InsertStringNVarCharParam("@Name", cm, errorCode.Name);
            DatabaseHelper.InsertInt32Param("@ProblemTypeId", cm, errorCode.ProblemTypeId);

        }

        public void Save(ErrorCode errorCode)
        {
            string sql = "INSERT INTO ErrorCode(Name, ProblemTypeId) VALUES (@Name, @ProblemTypeId) SELECT @@identity";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                SetErrorCode(cm, errorCode);
                errorCode.Id = int.Parse(cm.ExecuteScalar().ToString());
            }
        }

        public void Update(ErrorCode errorCode)
        {
            string sql = "Update ErrorCode SET Name =@Name, ProblemTypeId=@ProblemTypeId  where Id = @ErrorCodeId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@ErrorCodeId", cm, errorCode.Id);
                SetErrorCode(cm, errorCode);
                cm.ExecuteNonQuery();
            }
        }

        public void Delete(int ProblemTypeId)
        {
            string sql = "Delete ErrorCode where Id = @ProblemTypeId";

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                DatabaseHelper.InsertInt32Param("@ProblemTypeId", cm, ProblemTypeId);
                cm.ExecuteNonQuery();
            }
        }

        public IList<ErrorCode> GetErrorCodeListByProblemTypeID(int ProblemTypeID)
        {
            string sql;
            if (ProblemTypeID != 0)
                sql = "SELECT ErrorCode.Id, ErrorCode.Name, ErrorCode.ProblemTypeId, ProblemType.Name AS ProblemTypeName  FROM ErrorCode " +
                      " LEFT JOIN ProblemType ON ProblemType.Id = ErrorCode.ProblemTypeId where ProblemTypeId = @ProblemTypeID order by ErrorCode.Name";
            else
                sql = "SELECT ErrorCode.Id, ErrorCode.Name, ErrorCode.ProblemTypeId, ProblemType.Name AS ProblemTypeName  FROM ErrorCode " +
                      " LEFT JOIN ProblemType ON ProblemType.Id = ErrorCode.ProblemTypeId where ProblemTypeId = @ProblemTypeID order by ErrorCode.Name";

            IList<ErrorCode> lstErrorCode = new List<ErrorCode>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (ProblemTypeID != 0)
                {
                    DatabaseHelper.InsertInt32Param("@ProblemTypeID", cm, ProblemTypeID);
                }

                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstErrorCode.Add(GetErrorCode(dr));
                        }
                    }
                }
            }
            return lstErrorCode;
        }
        public IList<ErrorCode> GetListOfErrorCodes(string errorCode, int problemType)
        {
            string sql="";
            if (errorCode == "" && problemType == 0)
                sql = "SELECT ErrorCode.*, ProblemType.Name AS ProblemTypeName  FROM ErrorCode " +
                      " LEFT JOIN ProblemType ON ProblemType.Id = ErrorCode.ProblemTypeId order by ErrorCode.Name";
            else if (errorCode != "" && problemType != 0)
                sql = "SELECT ErrorCode.*, ProblemType.Name AS ProblemTypeName   FROM ErrorCode " +
                      " LEFT JOIN ProblemType ON ProblemType.Id = ErrorCode.ProblemTypeId where ErrorCode.Name = @errorCode AND ErrorCode.ProblemTypeId = @problemType order by ErrorCode.Name";
            else if (errorCode == "" && problemType != 0)
                sql = "SELECT ErrorCode.*, ProblemType.Name AS ProblemTypeName   FROM ErrorCode " +
                      " LEFT JOIN ProblemType ON ProblemType.Id = ErrorCode.ProblemTypeId where ErrorCode.ProblemTypeId = @problemType order by ErrorCode.Name";
            else if (errorCode != "" && problemType == 0)
                sql = "SELECT ErrorCode.*, ProblemType.Name AS ProblemTypeName   FROM ErrorCode " +
                      " LEFT JOIN ProblemType ON ProblemType.Id = ErrorCode.ProblemTypeId where ErrorCode.Name = @errorCode order by ErrorCode.Name";
            IList<ErrorCode> lstErrorCode = new List<ErrorCode>();

            using (SqlCommand cm = new SqlCommand(sql, DefaultConnection))
            {
                if (errorCode != "")
                    DatabaseHelper.InsertStringVarCharParam("@errorCode", cm, errorCode);
                if (problemType != 0)
                    DatabaseHelper.InsertInt32Param("@problemType", cm, problemType);
                using (SqlDataReader dr = cm.ExecuteReader())
                {
                    if (dr != null)
                    {
                        while (dr.Read())
                        {
                            lstErrorCode.Add(GetErrorCode(dr));
                        }
                    }
                }
            }
            return lstErrorCode;
        }
    }
}
