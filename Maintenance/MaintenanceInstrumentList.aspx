﻿

<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.MaintenanceInstrumentList, App_Web_4ftijqq5" stylesheettheme="Default" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <asp:Panel ID="PanInstrumentList" runat="server" Width="100%">
    <div Class="section">

    <div cssClass="group">
    <h4>  <asp:Label ID="Label2" runat="server" Text="Find Instrument" CssClass="label" 
            ForeColor="White"></asp:Label></h4>
       

            <table style="width: 100%">
                <tr>
                    <td style="width: 156px">
                        <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
                    </td>
                    <td style="width: 225px">
                        <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                             onselectedindexchanged="ddlRegion_SelectedIndexChanged" 
                            CssClass="textbox">
                            <asp:ListItem Value="0">Select Region</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="Name" DataValueField="Id" 
                            CssClass="textbox">
                            <asp:ListItem Value="0">Select Site</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <table style="width: 100%">
                <tr>
                    <td style="width: 156px">
                        <asp:Label ID="lblInstrument" runat="server" Text="Instrument Category" 
                            CssClass="label"></asp:Label>
                    </td>
                    <td style="width: 225px">
                        <asp:DropDownList ID="ddlInstrumentCategory" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             AutoPostBack="True" CssClass="textbox">
                            <asp:ListItem Value="0">Select Instrument Category</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 77px; text-align: left">
                        <asp:Label ID="Label1" runat="server" Text="Functionality" CssClass="label"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlFunctionality" runat="server" AutoPostBack="True" 
                             CssClass="textbox">
                            <asp:ListItem Value="0">Select Functionality</asp:ListItem>
                            <asp:ListItem Value="1">Functional</asp:ListItem>
                            <asp:ListItem Value="2">Not-Functional</asp:ListItem>
                            <asp:ListItem Value="5">Functional With Problem</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            </div>
            </div>
        <br />
         <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4>  <asp:Label ID="fsInstrumentList" runat="server" Text="Instrument List" 
            CssClass="label"></asp:Label></h4>
               
      
            <asp:GridView ID="grvInstrumentList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                     GridLines="Horizontal" 
                    onrowdatabound="grvInstrumentList_RowDataBound" 
                 
                onselectedindexchanged="grvInstrumentList_SelectedIndexChanged" 
                onpageindexchanging="grvInstrumentList_PageIndexChanging" 
                AllowPaging="True">
                
                 
                <Columns>
                    <asp:BoundField DataField="RegionName" HeaderText="Region" />
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument Name" />
                    <asp:BoundField DataField="SerialNo" HeaderText="SerialNo." />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="ManufacturerName" HeaderText="Manufacturer" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplNotifyProblem" runat="server">Report Problem</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle ForeColor="White" HorizontalAlign="Center" />
               <%-- <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            <br />
            </asp:Panel>
         <br />
        <table style="width: 100%; margin-bottom: 4px;">
            <tr>
                <td style="width: 700px">
                    &nbsp;</td>
                <td style="text-align: right">
                    <asp:Image ID="Image1" runat="server" 
                        ImageUrl="~/Images/functionalNotfunction.png" />
                </td>
            </tr>
        </table>
    <br />
    </asp:Panel>
</asp:Content>



