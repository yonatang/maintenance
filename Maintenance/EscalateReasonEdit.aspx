﻿<%@ page language="C#" autoeventwireup="true" inherits="Maintenance_EscalateReasonEdit, App_Web_4ftijqq5" stylesheettheme="Default" %>

<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 <link type="text/css" rel="stylesheet" href="css/controls.css" />
<head runat="server">
    <title></title>
     
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .style4
        {
            width: 129px;
            height: 21px;
        }
        .style5
        {
            height: 21px;
        }
        .style6
        {
            width: 266px;
            text-align: left;
        }
        .style9
        {
            width: 129px;
        }
        .style10
        {
            width: 266px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     
        <table class="style1">
            <tr>
                <td class="style2" colspan="2">
                    <strong style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Push Maintenance Job</strong></td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style9">
                    <asp:Label ID="Label1" runat="server" 
                        style="font-family: Arial, Helvetica, sans-serif; font-size: x-small" 
                        Text="Reason"></asp:Label>
                </td>
                <td class="style6">
                    <asp:DropDownList ID="ddlReason" runat="server"  
                        AppendDataBoundItems="True" DataTextField="Reason" 
                        DataValueField="Id" CssClass="textbox">
                        <asp:ListItem Value="0">Select Reason</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="ddlReason" Display="Dynamic" ErrorMessage="*" 
                        style="font-size: x-small" ValidationGroup="1" InitialValue="0">*</asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style4">
                    <asp:Label ID="Label2" runat="server" 
                        style="font-family: Arial, Helvetica, sans-serif; font-size: x-small" 
                        Text="Date"></asp:Label>
                </td>
                <td class="style10">
                        <cc1:Calendar ID="Calendar1" runat="server" 
                        style="font-size: x-small; font-family: Arial, Helvetica, sans-serif" 
                        Width="212px" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="Calendar1" Display="Dynamic" ErrorMessage="*" 
                            style="font-size: x-small" ValidationGroup="1">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                            ControlToValidate="Calendar1" 
                            ErrorMessage="Date is not a valid date" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1" style="font-size: small">*</asp:RegularExpressionValidator>
                    </td>
                <td class="style5">
                </td>
                <td class="style5">
                </td>
            </tr>
            <tr>
                <td class="style9">
                    <asp:Label ID="lblEscalate" runat="server" 
                        style="font-size: x-small; font-family: Arial, Helvetica, sans-serif"></asp:Label>
                </td>
                <td " style="text-align: right" style="text-align: right" class="style10" >
                
                    <asp:LinkButton ID="lnkEcsalate" runat="server" 
                        style="font-size: x-small; font-family: Arial, Helvetica, sans-serif" 
                        onclick="lnkEcsalate_Click" ValidationGroup="1">Push Job</asp:LinkButton>
                &nbsp;<asp:LinkButton ID="lnkClose" runat="server" 
                        style="font-size: x-small; font-family: Arial, Helvetica, sans-serif" 
                        onclick="lnkClose_Click">Close</asp:LinkButton>
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
