﻿<%@ page language="C#" autoeventwireup="true" inherits="Maintenance_PrintMaintenanceHistory, App_Web_4ftijqq5" stylesheettheme="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  <%--  <style type="text/css">
        .style1
        {
            width: 100%;
            background-color: #FFFFFF;
        }
        .style2
        {
            font-size: x-large;
        }
        .style3
        {
            text-align: right;
            font-family: "Times New Roman";
            font-size: large;
        }
        .style4
        {
            font-family: "Times New Roman";
        }
        .style5
        {
            font-family: "Times New Roman";
            text-align: left;
        }
        .style6
        {
            width: 52px;
        }
        .style7
        {
            width: 72px;
        }
        .style8
        {
            width: 172px;
            font-family: "Times New Roman";
        }
        .style9
        {
            text-align: center;
            font-family: "Times New Roman";
            font-size: large;
        }
        .style10
        {
            font-size: small;
            text-align: right;
            font-weight: 700;
        }
        .style11
        {
            font-family: "Times New Roman";
            font-size: small;
            text-align: right;
        }
        .style12
        {
            font-size: x-small;
        }
        .style13
        {
            width: 52px;
            font-size: small;
        }
        .style16
        {
            width: 172px;
            font-family: "Times New Roman";
            font-size: small;
        }
    </style>--%>
    <style type="text/css">
        .style1
        {
            font-size: large;
        }
        .style2
        {
            font-size: xx-large;
        }
        .style8
        {
            text-align: center;
        }
        .style9
        {
            text-align : center;
        }
        .style10
        {
            text-align: right;
            font-size: x-small;
        }
        .style12
        {
            text-align: right;
            font-size: small;
            font-family: "Times New Roman", Times, serif;
        }
        .style13
        {
            text-align: left
            font-size: small;
            font-family: "Times New Roman", Times, serif;
            font-size: small;
        }
        .style14
        {
            text-align: right
            font-size: small;
            font-family: "Times New Roman", Times, serif;
            font-size: small;
            font-weight: 700;
        }
        .style15
        {
            text-align: left font-size: small;
            font-family: "Times New Roman", Times, serif;
            font-size: small;
            width: 317px;
        }
        .style16
        {
            text-align: left font-size: small;
            font-family: "Times New Roman", Times, serif;
            font-size: small;
            width: 93px;
        }
        .style19
        {
            width: 192px;
        }
        .style20
        {
            text-align: left font-size: small;
            font-family: "Times New Roman", Times, serif;
            font-size: small;
            width: 192px;
        }
        .style21
        {
            border-style: solid;
            border-color: #DDDDDD;
            background-color: #DDDDDD;
        }
        .style22
        {
            border-style: solid;
            border-color: #DDDDDD;
            width: 192px;
            background-color: #DDDDDD;
        }
        .style23
        {
            border-style: solid;
            border-color: #DDDDDD;
            text-align: center;
            background-color: #DDDDDD;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divprint">

        <table class="style1">
            <tr>
                <td colspan="5" class="style2" 
                    style="text-align: center; font-family: 'Times New Roman'">
                    <strong>Ethiopian Health and Nutrition Research 
                    Institute</strong></td>
            </tr>
            <tr>
                <td class="style9" colspan="5">
                    <strong>Instrument Maintenance History</strong></td>
            </tr>
            <tr>
                <td class="style10" colspan="5">
                    <span class="style12">Date :</span><asp:Label 
                        ID="lblDate" runat="server" CssClass="style12"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td class="style16">
                    Instrument Name</td>
                <td class="style14" style="font-weight: 700">
                    <asp:Label ID="lblName" runat="server" CssClass="style5"></asp:Label>
                </td>
                <td class="style19">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    </span></td>
            </tr>
            <tr>
                <td class="style16">
                    Serial Number</td>
                <td class="style7">
                    <span class="style14">
                    <asp:Label ID="lblSerialNumber" runat="server" CssClass="style15" 
                        style="font-size: small; font-weight: 700;"></asp:Label>
                </td>
                <td class="style20">
                    &nbsp;</td>
                <td class="style13">
                    Last Preventive Maintenance Date</td>
                <td class="style14">
                    <asp:Label ID="lblLastPrevMaintenanceDate" runat="server"></asp:Label>
                    <span class="style10">
                    </span></span></td>
            </tr>
            <tr>
                <td class="style16">
                    Installation Date</td>
                <td class="style14">
                    <span class="style14">
                    <asp:Label ID="lblInstallationDate" runat="server" ></asp:Label>
                </td>
                <td class="style20">
                    &nbsp;</td>
                <td class="style13">
                    Manufacturer</td>
                <td class="style14">
                    <asp:Label ID="lblManufacturer" runat="server"></asp:Label>
                    <span class="style10">
                    </span></span></span></td>
            </tr>
            <tr>
                <td class="style8">
                    &nbsp;</td>
                <td class="style7">
                    &nbsp;</td>
                <td class="style19">
                    &nbsp;</td>
                <td class="style13">
                    Site</td>
                <td class="style14">
                    <asp:Label ID="lblSiteName" runat="server" 
                        
                        style="font-family: 'Times New Roman'; font-size: small; font-weight: 700;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style23" colspan="2">
                    <strong class="style9">Maintenance History . . .</strong></td>
                <td class="style22">
                    &nbsp;</td>
                <td class="style21">
                    &nbsp;</td>
                <td class="style21">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style5" colspan="5">
                    <asp:GridView ID="dgvMaintenanceHistory" runat="server" 
                        AutoGenerateColumns="False" EnableModelValidation="True" 
                        
                        style="font-size: small; font-style: italic; font-family: 'Times New Roman';" 
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="ProblemNumber" HeaderText="Problem Number" />
                            <asp:BoundField DataField="Description" HeaderText="Description" />
                            <asp:BoundField DataField="ProblemType" HeaderText="Problem Type" />
                            <asp:BoundField DataField="ErrorCode" HeaderText="Error Code" />
                            <asp:BoundField DataField="DateReported" HeaderText="Date Reported" />
                            <asp:BoundField DataField="EnginerName" HeaderText="Enginer" />
                            <asp:BoundField DataField="MaintainedDate" HeaderText="Maintained Date" />
                            <asp:BoundField DataField="EnginerComment" HeaderText="Enginer Comment" />
                            <asp:BoundField DataField="CauseOfFailure" HeaderText="Cause Of Failure" />
                            <asp:BoundField DataField="ActionTaken" HeaderText="Action Taken" />
                            <asp:BoundField DataField="MaintainedParts" HeaderText="Maintained Parts" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td class="style5" colspan="5">
                    <asp:Label ID="lblMesage" runat="server" 
                        style="font-style: italic; color: #FF3300; text-align: center; font-family: 'Times New Roman';" 
                        Text="Have No Maintenance History"></asp:Label>
                </td>
            </tr>
        </table>
    
    </div>
     <asp:Button ID="btnPrint" runat="server" style="font-size: small" 
                        Text="Print" />
    <br />
    </form>
</body>
</html>
