﻿
<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.ProblemNotification, App_Web_4ftijqq5" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc11" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
   
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblEditing" runat="server" 
                    style="color: #FF3300; background-color: #FFFFFF; font-weight: 700;" Text="Editing..." 
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
   
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        ValidationGroup="1" HeaderText="Error Message" />
    
     <div Class="section">

    <div cssClass="group">
    <h4> <asp:Label ID="fsInstrumentInfo" runat="server" Text="Instrument Information" 
            CssClass="label" ForeColor="White"></asp:Label></h4>

       
        <table style="width: 100%" __designer:mapid="3">
            <tr __designer:mapid="4">
                <td __designer:mapid="5" colspan="4">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblInstrument" runat="server" Text="Selected Instrument" 
                        style="font-weight: 700" CssClass="label"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblSelectedinstrument" runat="server" style="font-weight: 700" 
                        CssClass="label"></asp:Label>
                </td>
            </tr>
            <tr __designer:mapid="4">
                <td style="width: 104px;  text-align: left;" 
                    __designer:mapid="5">
                    <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
                </td>
                <td style="width: 226px;  " __designer:mapid="7">
                    <asp:Label ID="lblregionName" runat="server" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="a"  >
                    <asp:Label ID="Label1" runat="server" Text="Installation Date" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c"  >
                    <asp:Label ID="lblInstallationDate" runat="server" CssClass="label"></asp:Label>
                </td>
            </tr>
            <tr __designer:mapid="4">
                <td style="width: 104px;   text-align: left;" 
                    __designer:mapid="5">
                    <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                </td>
                <td style="width: 226px; " __designer:mapid="7">
                    <asp:Label ID="lblSiteName" runat="server" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="a"  >
                    <asp:Label ID="Label2" runat="server" Text="Manufacturer" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c"  >
                    <asp:Label ID="lblManufacturer" runat="server" CssClass="label"></asp:Label>
                </td>
            </tr>
        </table>
  </div>
  </div>

    <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4>     <asp:Label ID="Label3" runat="server" Text="Report Problem" 
            CssClass="label"></asp:Label></h4>
   
   
    <table style="width: 100%">
        <tr>
            <td style="width: 98px; background-color: #CCCCCC" rowspan="9">
                &nbsp;</td>
            <td style="width: 158px">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Problem Number" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblProbNumber" runat="server" CssClass="label"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblProblemType" runat="server" Text="Problem Type" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlProblemType" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="ddlProblemType_SelectedIndexChanged" 
                    DataTextField="Name" DataValueField="Id" ValidationGroup="1" 
                    AppendDataBoundItems="True" CssClass="textbox">
                    <asp:ListItem Value="0">Select Problem Type</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    Display="Dynamic" ErrorMessage="Select Problem Type" style="font-size: medium" 
                    ValidationGroup="1" ControlToValidate="ddlProblemType" InitialValue="0">*</asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 158px">
                <asp:Label ID="lblErrorCode" runat="server" Text="Error Code" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlErrorCode" runat="server" AppendDataBoundItems="True" 
                     DataTextField="Name" DataValueField="Id" ValidationGroup="1" 
                    CssClass="textbox">
                    <asp:ListItem Value="0">Select Error Code</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    Display="Dynamic" ErrorMessage="Select Error Code " style="font-size: medium" 
                    ValidationGroup="1" ControlToValidate="ddlErrorCode">*</asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 158px">
                <asp:Label ID="lbldate" runat="server" Text="Date Reported" CssClass="label"></asp:Label>
            </td>
            <td>

       
                        <cc11:Calendar ID="Calendar1" runat="server" Height="48px" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="Calendar1" Display="Dynamic" ErrorMessage="Date Required" 
                    style="font-size: medium" ValidationGroup="1">*</asp:RequiredFieldValidator>

                
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                            ControlToValidate="Calendar1" ErrorMessage="Date Reported is not a valid date" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator>

                
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 158px">
                <asp:Label ID="lblDescription" runat="server" CssClass="label" 
                    Text="Description"></asp:Label>
            </td>
            <td>
                <asp:CheckBox ID="chkIsFunctional" runat="server" Text="Not Functional" 
                    CssClass="label" />
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td >
                &nbsp;</td>
            <td rowspan="3" valign="bottom">
                <asp:TextBox ID="txtdescription" runat="server" TextMode="MultiLine" 
                     CssClass="textboxDescShort"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        <table style="width: 100%">
            <tr>
                <td style="text-align: right">
                    <asp:Button ID="btnNew" runat="server" onclick="btnNew_Click" Text="New" />
&nbsp;
                    <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" 
                        ValidationGroup="1" />
&nbsp;
                    <asp:Button ID="btnDelete" runat="server" onclick="btnDelete_Click" 
                        Text="Delete" />
&nbsp;
                    <asp:Button ID="btnCancel" runat="server" onclick="btnCancel_Click" 
                        Text="Cancel"  />
                </td>
            </tr>
        </table>
   </asp:Panel>
    </asp:Content>

