﻿
<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.EscalatedJobs, App_Web_4ftijqq5" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %><%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <div class="section">

    <div cssClass="group">
          <h4>  <asp:Label ID="fsFindInstrument" runat="server" 
                    Text="Find Escalated Jobs" CssClass="label" ForeColor="White"></asp:Label></h4>
     
        <table style="width: 100%" __designer:mapid="97">
            <tr __designer:mapid="98">
                <td __designer:mapid="99">
                    <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
                </td>
                <td style="width: 226px" __designer:mapid="9b">
                    <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                            Width="150px" onselectedindexchanged="ddlRegion_SelectedIndexChanged" 
                        CssClass="textbox">
                        <asp:ListItem Value="0">Select Region</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td __designer:mapid="9e">
                    <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="a0">
                    <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                            DataTextField="Name" DataValueField="Id" Width="150px" 
                         AutoPostBack="True" CssClass="textbox">
                        <asp:ListItem Value="0">Select Site</asp:ListItem>
                    </asp:DropDownList>
                    </td>
            </tr>
        </table>
 </div></div>

  <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4>     <asp:Label ID="Lblll" runat="server" Text=" Escalated Curative Maintenance Jobs From Regions" 
            CssClass="label"></asp:Label></h4>

   
    
    
    <asp:GridView ID="grvEscalatedJobs" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                     GridLines="Horizontal"  
                    onrowdatabound="grvEscalatedJobs_RowDataBound" 
                 AllowPaging="True" 
             onpageindexchanging="grvEscalatedJobs_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="RegionName" 
                        HeaderText="Region" />
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="SerialNo" 
                        HeaderText="Serial No" />
                    <asp:BoundField DataField="EnginerName" HeaderText="Enginer" />
                    <asp:BoundField DataField="ProblemNumber" HeaderText="Problem Number" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplschedule" runat="server">Re-Schedule Curative Maintenance</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle   ForeColor="White" HorizontalAlign="Center" />
               <%-- <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
    
  
    <br />
    <table style="width: 100%">
        <tr>
            <td align="right">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/EscalatedScheduleLegend.jpg" />
            </td>
        </tr>
    </table></asp:Panel>
</asp:Content>
