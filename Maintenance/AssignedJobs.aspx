﻿
<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.AssignedJobs, App_Web_4ftijqq5" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
     <div class="section">

   <div cssClass="group">
         <h4>   <asp:Label ID="fsAssignedjobs" runat="server" Text="Assigned Jobs" 
                CssClass="label" ForeColor="White"></asp:Label></h4>
           
       
            <table style="width: 100%">
                <tr>
                    <td style="text-align: center" >
                        <asp:RadioButton ID="rbtPreventive" runat="server" AutoPostBack="True" 
                            GroupName="1" Text="Show Preventive Jobs" CssClass="label" />
                    </td>
                    <td style="text-align: center">
                        <asp:RadioButton ID="rbtCurative" runat="server" AutoPostBack="True" 
                            GroupName="1" Text="Show Curative Jobs" CssClass="label" />
                    </td>
                </tr>
        </table>
        </div></div>

            <asp:GridView ID="grvAssignedJobs" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                     GridLines="Horizontal" 
                 
            onrowdatabound="grvAssignedJobs_RowDataBound" Visible="False" 
            AllowPaging="True" onpageindexchanging="grvAssignedJobs_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="PreventiveMaintenancePeriod" 
                        HeaderText="Preventive Maint Period" />
                    <asp:BoundField DataField="LastPreventiveMaintenanceDate" 
                        HeaderText="Last Preventive Maint. Date" />
                    <asp:BoundField DataField="ErrorCode" HeaderText="Error Code" />
                    <asp:BoundField DataField="Type" HeaderText="Type" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplMaintain" runat="server" 
                                Text="Maintain Instrument"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit Maintenance History"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplViewHistory" runat="server">View History</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle   ForeColor="White" HorizontalAlign="Center" />
             <%--   <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
           <br />




            <table style="width: 100%">
                <tr>
                    <td style="text-align: right">
            <asp:GridView ID="grvAssignedJobs0" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    GridLines="Horizontal" 
               
            onrowdatabound="grvAssignedJobs0_RowDataBound" Visible="False" AllowPaging="True" 
                            onpageindexchanging="grvAssignedJobs0_PageIndexChanging">
               
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="PreventiveMaintenancePeriod" 
                        HeaderText="Preventive Maint Period" />
                    <asp:BoundField DataField="LastPreventiveMaintenanceDate" 
                        HeaderText="Last Preventive Maint. Date" />
                    <asp:BoundField DataField="Type" HeaderText="Type" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplMaintain0" runat="server" 
                                Text="Maintain Instrument"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit0" runat="server" Text="Edit Maintenance History"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle  ForeColor="White" HorizontalAlign="Center" />
              <%--  <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            
                        </td>
                </tr>
                 
                <tr>
                    <td style="text-align: right">
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <%-- </fieldset>--%>
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Preventivelegend.png" 
                            Visible="False" />
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/curMaintenance.png" 
                            Visible="False" />
                       <%-- </fieldset>--%>
                        </td>
                </tr>
    </table>
     
</asp:Content>




