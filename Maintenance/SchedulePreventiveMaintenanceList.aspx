﻿ <%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.SchedulePreventiveMaintenanceList, App_Web_4ftijqq5" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
 <%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
 
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
     <div class="section">
     <div cssClass="group">
             <h4><asp:Label ID="fsFindInstrument" runat="server" 
                    Text="Find Instrument To Schedule Preventive Maintenance" 
                 CssClass="label" ForeColor="White"></asp:Label></h4>
       
         <table style="width: 100%" __designer:mapid="97">
             <tr __designer:mapid="98">
                 <td style="width: 123px" __designer:mapid="99">
                     <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
                 </td>
                 <td style="width: 260px" __designer:mapid="9b">
                     <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                              onselectedindexchanged="ddlRegion_SelectedIndexChanged" 
                         CssClass="textbox">
                         <asp:ListItem Value="0">Select Region</asp:ListItem>
                     </asp:DropDownList>
                 </td>
                 <td __designer:mapid="9e" style="width: 145px">
                     <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                 </td>
                 <td __designer:mapid="a0">
                     <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                            DataTextField="Name" DataValueField="Id" 
                         AutoPostBack="True" CssClass="textbox">
                         <asp:ListItem Value="0">Select Site</asp:ListItem>
                     </asp:DropDownList>
                 </td>
             </tr>
         </table>
         <table style="width: 100%" __designer:mapid="a3">
             <tr __designer:mapid="a4">
                 <td class="editDropDown" style="width: 140px" __designer:mapid="a5">
                     <asp:Label ID="lblInstrument" runat="server" Text="Instrument Category" 
                         CssClass="label"></asp:Label>
                 </td>
                 <td style="width: 246px" __designer:mapid="a7">
                     <asp:DropDownList ID="ddlInstrumentCategory" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             AutoPostBack="True" CssClass="textbox">
                         <asp:ListItem Value="0">Select Instrument Category</asp:ListItem>
                     </asp:DropDownList>
                 </td>
                 <td style="width: 194px; text-align: left" __designer:mapid="aa">
                     <asp:Label ID="Label2" runat="server" Text="Last Prev Maintenance Date" 
                         CssClass="label"></asp:Label>
                 </td>
                 <td style="width: 66px; text-align: center" __designer:mapid="aa">
                     <asp:TextBox ID="txtlastprevMdate" runat="server"   
                         CssClass="textbox"></asp:TextBox>
                     <cc1:CalendarExtender ID="txtlastprevMdate_CalendarExtender" runat="server" 
                         Enabled="True" TargetControlID="txtlastprevMdate">
                     </cc1:CalendarExtender>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" 
                            runat="server" ControlToValidate="txtlastprevMdate" ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator>
                 </td>
                 <td __designer:mapid="ac">
&nbsp;
                     <asp:Button ID="btnFind" runat="server" onclick="btnFind_Click" Text="Find" 
                         ValidationGroup="1" />
                 </td>
             </tr>
         </table>
      </div></div>

    <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4>     <asp:Label ID="Label1" runat="server" Text="List Of Instruments" 
            CssClass="label"></asp:Label></h4>
        
        
            <asp:GridView ID="grvInstrumentList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                      GridLines="Horizontal" 
                    onrowdatabound="grvInstrumentList_RowDataBound" 
                 AllowPaging="True" 
             onpageindexchanging="grvInstrumentList_PageIndexChanging">
               
                
                <Columns>
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument Name" />
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="SerialNo" HeaderText="Serial Number" />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="PreventiveMaintenancePeriod" 
                        HeaderText="Preventive Maintenance Period" />
                    <asp:BoundField DataField="LastPreventiveMaintenanceDate" 
                        HeaderText="Last Preventive Maintenance Date" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplschedule" runat="server">Schedule Preventive Maintenance</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle   ForeColor="White" HorizontalAlign="Center" />
                <%--<SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            <br />
         <table style="width: 100%">
             <tr>
                 <td style="text-align: right">
                     <asp:Image ID="Image1" runat="server" 
                         ImageUrl="~/Images/PreventiveScehudleLegend9999.png" />
                 </td>
             </tr>
         </table>
           </asp:Panel>
 </asp:Content>
