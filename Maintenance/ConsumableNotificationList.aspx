﻿
<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.ConsumableNotificationList, App_Web_4ftijqq5" stylesheettheme="Default" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
     <div class ="section">
     <div cssClass="group">
    <h4> <asp:Label ID="Label1" runat="server" 
                Text="Find Consumable Notification" CssClass="label" ForeColor="White"></asp:Label></h4>
       
        <%--<table style="width: 100%" __designer:mapid="342">
            <tr __designer:mapid="343">
                <td style="width: 102px" __designer:mapid="344">
                    <asp:Label ID="lblRegion" runat="server" Text="Region"></asp:Label>
                </td>
                <td style="width: 226px" __designer:mapid="346">
                    <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                            Width="150px" onselectedindexchanged="ddlRegion_SelectedIndexChanged">
                        <asp:ListItem Value="0">Select Region</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td __designer:mapid="349">
                    <asp:Label ID="lblSite" runat="server" Text="Site"></asp:Label>
                </td>
                <td __designer:mapid="34b">
                    <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="Name" DataValueField="Id" Width="150px">
                        <asp:ListItem Value="0">Select Site</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>--%>
        <table style="width: 100%" __designer:mapid="34e">
            <tr __designer:mapid="34f">
                <td class="editDropDown" style="width: 101px" __designer:mapid="350">
                    <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                </td>
                <td style="width: 190px" __designer:mapid="352">
                    <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                        AutoPostBack="True"   DataTextField="Name" DataValueField="Id" 
                        CssClass="textbox">
                        <asp:ListItem Value="0">Select Site</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 100px; text-align: center" __designer:mapid="355">
                    &nbsp;</td>
                <td __designer:mapid="357" style="width: 147px">
                    <asp:Label ID="lblConsumable" runat="server" Text="Consumable" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="357">
                    <asp:DropDownList ID="ddlConsumables" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                          AutoPostBack="True" CssClass="textbox">
                        <asp:ListItem Value="0">Select Consumables</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table></div></div>
  
    <asp:GridView ID="grvConsumableNotification" runat="server" AutoGenerateColumns="False" 
                 DataKeyNames="Id" EnableModelValidation="True"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    ForeColor="#333333" GridLines="Horizontal" 
               
            onrowdatabound="grvConsumableNotification_RowDataBound" 
        AllowPaging="True" 
        onpageindexchanging="grvConsumableNotification_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="ConsumableName" HeaderText="Consumable" />
                    <asp:BoundField DataField="NotifiedBy" 
                        HeaderText="Notified By" />
                    <asp:BoundField DataField="Date" HeaderText="Date" />
                    <asp:TemplateField>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit Notification"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle   ForeColor="White" HorizontalAlign="Center" />
               <%-- <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
</asp:Content>

