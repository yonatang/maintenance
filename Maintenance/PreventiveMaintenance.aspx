﻿
<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.PreventiveMaintenance, App_Web_4ftijqq5" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
 
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc11" %>
 

 
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">

     <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblEditing" runat="server" 
                    style="color: #FF3300; background-color: #FFFFFF; font-weight: 700;" Text="Editing..." 
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
         HeaderText="Error Message" ValidationGroup="4" />
   <div class="section">
   <div cssClass="group">
       <h4>     <asp:Label ID="fsInstrumentInfo" runat="server" Text="Job Information" 
                CssClass="label" ForeColor="White"></asp:Label></h4>
      
        <table style="width: 100%" __designer:mapid="3">
            <tr __designer:mapid="4">
                <td __designer:mapid="5" colspan="4" align="left">
                  
                    <asp:Label ID="lblInstrument" runat="server" Text="Selected Instrument" 
                        style="font-weight: 700" CssClass="label"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblSelectedinstrument" runat="server" style="font-weight: 700" 
                        CssClass="label"></asp:Label>
                </td>
            </tr>
            <tr __designer:mapid="4">
                <td 
                    __designer:mapid="5">
                    <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="7">
                    <asp:Label ID="lblSiteName" runat="server" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="a">
                    <asp:Label ID="Label1" runat="server" Text="Installation Date" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c">
                    <asp:Label ID="lblInstallationDate" runat="server" CssClass="label"></asp:Label>
                </td>
            </tr>
            <tr __designer:mapid="4">
                <td 
                    __designer:mapid="5">
                    <asp:Label ID="Label8" runat="server" Text="Serial Number" CssClass="label"></asp:Label>
                </td>
                <td 
                    __designer:mapid="7">
                    <asp:Label ID="lblSerialNumber" runat="server" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="a">
                    <asp:Label ID="Label6" runat="server" Text="Maintenance Type" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c">
                    <asp:Label ID="lblMaintenanceType" runat="server" CssClass="label"></asp:Label>
                </td>
            </tr>
            </table>
    </div></div>
  <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4>  
        <asp:Label ID="schedules" runat="server" Text="Preventive Maintenance" 
            CssClass="label"></asp:Label></h4>
 

    <table style="width: 100%">
        <tr>
            <td style="width: 113px; background-color: #CCCCCC;" rowspan="9">
                &nbsp;</td>
            <td colspan="2">
                
                <fieldset style="width: 570px"><legend>
                    <asp:Label ID="Label11" runat="server" 
                        Text="Instrument IQC Performed and Recorded?" CssClass="label"></asp:Label></legend>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 96px">
                                &nbsp;</td>
                            <td style="text-align: center">
                                <b>
                                <asp:Label ID="Label27" runat="server" Text="Regularly" CssClass="label"></asp:Label>
                                </b>
                            </td>
                            <td style="text-align: center; width: 108px">
                                <b>
                                <asp:Label ID="Label28" runat="server" Text="Sometimes" CssClass="label"></asp:Label>
                                </b>
                            </td>
                            <td style="text-align: center; width: 108px">
                                <b>
                                <asp:Label ID="Label29" runat="server" Text="Never" CssClass="label"></asp:Label>
                                </b>
                            </td>
                            <td style="text-align: center">
                                <b>
                                <asp:Label ID="Label30" runat="server" Text="N/A" CssClass="label"></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 96px">
                                <asp:Label ID="Label24" runat="server" Text="Daily IQC" CssClass="label"></asp:Label>
                            </td>
                            <td style="text-align: center" align="center">
                                <asp:RadioButton ID="rbtD_Regular" runat="server" GroupName="Daily" 
                                    Checked="True" CssClass="label" />
                            </td>
                            <td style="text-align: center; width: 108px">
                                <asp:RadioButton ID="rbtD_smtms" runat="server" GroupName="Daily" 
                                    CssClass="label" />
                            </td>
                            <td style="text-align: center; width: 108px">
                                <asp:RadioButton ID="rbtD_Never" runat="server" GroupName="Daily" 
                                    CssClass="label" />
                            </td>
                            <td style="text-align: center">
                                <asp:RadioButton ID="rbtD_NA" runat="server" GroupName="Daily" 
                                    CssClass="label" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 96px">
                                <asp:Label ID="Label25" runat="server" Text="Weekly IQC" CssClass="label"></asp:Label>
                            </td>
                            <td style="text-align: center" align="center">
                                <asp:RadioButton ID="rbtW_Regular" runat="server" GroupName="Weekly" 
                                    Checked="True" CssClass="label" />
                            </td>
                            <td style="text-align: center; width: 108px">
                                <asp:RadioButton ID="rbtW_smtms" runat="server" GroupName="Weekly" 
                                    CssClass="label" />
                            </td>
                            <td style="text-align: center; width: 108px">
                                <asp:RadioButton ID="rbtW_Never" runat="server" GroupName="Weekly" 
                                    CssClass="label" />
                            </td>
                            <td style="text-align: center">
                                <asp:RadioButton ID="rbtW_NA" runat="server" GroupName="Weekly" 
                                    CssClass="label" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label26" runat="server" Text="Monthly IQC" CssClass="label"></asp:Label>
                            </td>
                            <td align="center">
                                <asp:RadioButton ID="rbtM_regular" runat="server" GroupName="Monthly" 
                                    Checked="True" />
                            </td>
                            <td style="text-align: center; width: 108px">
                                <asp:RadioButton ID="rbtM_smtms" runat="server" GroupName="Monthly" 
                                    CssClass="label" />
                            </td>
                            <td style="text-align: center; width: 108px">
                                <asp:RadioButton ID="rbtM_Never" runat="server" GroupName="Monthly" 
                                    CssClass="label" />
                            </td>
                            <td style="text-align: center">
                                <asp:RadioButton ID="rbtM_NA" runat="server" GroupName="Monthly" 
                                    CssClass="label" />
                            </td>
                        </tr>
                    </table>
                </fieldset></td>
        </tr>
        <tr>
            <td colspan="2" >
                               
                                   <asp:Label ID="Label21" runat="server" 
                                       Text="Preventive Acion Taken" CssClass="label"></asp:Label> 
                </td>
        </tr>
        <tr>
            <td colspan="2" >
                               
                <asp:TextBox ID="txtPreventiveActionTaken" runat="server"  
                     TextMode="MultiLine"  
                         CssClass="textboxDescLong"></asp:TextBox>
                </td>
        </tr>
        <tr>
            <td colspan="2" >
                               
                                   <asp:Label ID="Label22" runat="server" 
                                       Text="Engineer Comments" CssClass="label"></asp:Label> 
                </td>
        </tr>
        <tr>
            <td colspan="2" >
                               
                <asp:TextBox ID="txtEngComments" runat="server" 
                      TextMode="MultiLine"  
                         CssClass="textboxDescLong"></asp:TextBox>
                </td>
        </tr>
        <tr>
            <td colspan="2" >
                               
                <asp:CheckBox ID="chkPerformance" runat="server" 
                    Text="  Performance Test Done" CssClass="label" />
                
                <asp:CheckBox ID="chkFunctional" runat="server" 
                    Text="  Equipment Fully Functional?" CssClass="label" />
                
                <asp:CheckBox ID="chkFollowup" runat="server" Text="  Follow Up Required?" 
                    CssClass="label" />
                </td>
        </tr>
        <tr>
            <td style="width: 155px" >
                               
                <asp:Label ID="Label19" runat="server" Text="Next Visit Scheduled" 
                    CssClass="label"></asp:Label>
                </td>
            <td class="editTextBox" style="width: 370px" >
                               
                        <cc11:Calendar ID="Calendar2" runat="server" Height="57px"  />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="Calendar2" Display="Dynamic" 
                    ErrorMessage="Next Vist Scheduled Required" ValidationGroup="4">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator22" 
                            runat="server" ControlToValidate="Calendar2" 
                            ErrorMessage="Next Schedule Date is not valid" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="4">*</asp:RegularExpressionValidator>
                </td>
        </tr>
        <tr>
            <td style="width: 155px" >
                               
                <asp:Label ID="Label23" runat="server" Text="Date" CssClass="label"></asp:Label>
                </td>
            <td class="editTextBox" style="width: 370px"   >
                               
                        <cc11:Calendar ID="Calendar1" runat="server" Height="60px"  />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="Calendar1" Display="Dynamic" ErrorMessage="Date Required" 
                    ValidationGroup="4">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" 
                            runat="server" ControlToValidate="Calendar1" ErrorMessage="Date is not valid" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="4">*</asp:RegularExpressionValidator>
                </td>
        </tr>
        <tr>
            <td align="right" colspan="2" >
                                
                               <asp:HyperLink ID="hplChecklist" runat="server" CssClass="label">Fill Out Maintenance Checklist</asp:HyperLink>
                </td>
</td>
        </tr>
        </table>
   </asp:Panel>
   
            <asp:Panel ID="Panel1" runat="server" CssClass="group">
    <h4>  
        <asp:Label ID="Label12" runat="server" Text="Spare parts replaced" 
            CssClass="label"></asp:Label></h4>
     
                <asp:DataGrid ID="dgspare" runat="server" AutoGenerateColumns="False" 
                    CellPadding="0"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    DataKeyField="Id"  GridLines="None" 
                    oncancelcommand="dgspare_CancelCommand" 
                    ondeletecommand="dgspare_DeleteCommand" 
                    oneditcommand="dgspare_EditCommand" 
                    onitemcommand="dgspare_ItemCommand" 
                    onitemdatabound="dgspare_ItemDataBound" 
                    onupdatecommand="dgspare_UpdateCommand" ShowFooter="True" Width="99%">
                    <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="Part Name">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "SparePartName")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlSparepart" runat="server" AppendDataBoundItems="True" 
                                    DataTextField="Name" DataValueField="Id" Width="300px" CssClass="textbox">
                                    <asp:ListItem Value="0">Select Spare</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RfvSparepart" runat="server" 
                                    ControlToValidate="ddlSparepart" ErrorMessage="Sparepart Required" 
                                    InitialValue="0" ValidationGroup="3">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:DropDownList ID="ddlFSparepart" runat="server" 
                                    AppendDataBoundItems="True" DataTextField="Name" 
                                    DataValueField="Id" EnableViewState="true" 
                                    ValidationGroup="2" Width="300px" CssClass="textbox">
                                    <asp:ListItem Value="0">Select Spare</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RfvFSparepart" runat="server" 
                                    ControlToValidate="ddlFSparepart" Display="Dynamic" 
                                    ErrorMessage="Sparepart Required" InitialValue="0" ValidationGroup="2">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                     
                    
                        <asp:TemplateColumn HeaderText="Quantity">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtQty" runat="server" 
                                    Text=' <%# DataBinder.Eval(Container.DataItem, "Quantity")%>' ValidationGroup="3" 
                                    Width="100px" CssClass="textbox"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtQty_FilteredTextBoxExtender" runat="server" 
                                    Enabled="True" FilterType="Numbers" TargetControlID="txtQty">
                                </cc1:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="RfvQty" runat="server" 
                                    ControlToValidate="txtQty" ErrorMessage="Qty Required" 
                                    ValidationGroup="3">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtFQty" runat="server" EnableViewState="true" 
                                    ValidationGroup="2" Width="100px" CssClass="textbox"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="txtFQty_FilteredTextBoxExtender" 
                                    runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtFQty">
                                </cc1:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="RfvFQty" runat="server" 
                                    ControlToValidate="txtFQty" Display="Dynamic" 
                                    ErrorMessage="Quantity Required" ValidationGroup="2">*</asp:RequiredFieldValidator>
                            </FooterTemplate>
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "Quantity")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                       
                    
                        <asp:TemplateColumn>
                            <EditItemTemplate>
                                <asp:LinkButton ID="lnkUpdate" runat="server" CommandName="Update" 
                                    ValidationGroup="3">Update</asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:LinkButton ID="lnkAddNew" runat="server" CommandName="AddNew" 
                                    ValidationGroup="2">Add New</asp:LinkButton>
                            </FooterTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                                &nbsp;<asp:LinkButton ID="lnkDelete0" runat="server" CommandName="Delete" 
                                    Text="Delete" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                   <%-- <EditItemStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />--%>
                    
                    <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <PagerStyle ForeColor="White" HorizontalAlign="Center" />
                    <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                </asp:DataGrid>
   </asp:Panel>
    
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Button ID="btnSave" runat="server" onclick="btnSave_Click" Text="Save" 
                    ValidationGroup="4" />
&nbsp;
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    onclick="btnCancel_Click" />
&nbsp;
                <asp:Button ID="btnDelete" runat="server" Text="Delete" 
                    onclick="btnDelete_Click" />
            </td>
        </tr>
    </table>
   
 </asp:Content>
