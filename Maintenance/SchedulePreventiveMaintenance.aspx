﻿
<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.SchedulePreventiveMaintenance, App_Web_4ftijqq5" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %><%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc11" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblEditing" runat="server" 
                    style="color: #FF3300; background-color: #FFFFFF; font-weight: 700;" Text="Editing..." 
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        HeaderText="Error Message" ValidationGroup="1" />
   <asp:Label ID="errormsg" runat="server" Visible="false" ForeColor="Red"></asp:Label>
   <div class="section"> 
   <div cssClass="group">
       <h4> <asp:Label ID="lbllgdInfo" runat="server" 
            Text="Instrument Information" CssClass="label" ForeColor="White"></asp:Label></h4>
     
        <table style="width: 100%" __designer:mapid="3">
            <tr __designer:mapid="4">
                <td __designer:mapid="5" colspan="7">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblInstrument" runat="server" Text="Selected Instrument" 
                        style="font-weight: 700" CssClass="label"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblSelectedinstrument" runat="server" style="font-weight: 700" 
                        CssClass="label"></asp:Label>
                </td>
            </tr>
            <tr __designer:mapid="4">
                <td  
                    __designer:mapid="5">
                    <asp:Label ID="lblRegion111" runat="server" Text="Region" CssClass="label"></asp:Label>
                </td>
                <td   __designer:mapid="7">
                    <asp:Label ID="lblregionName" runat="server" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="a"  >
                    <asp:Label ID="Label1" runat="server" Text="Installation Date" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c"  >
                    <asp:Label ID="lblInstallationDate" runat="server" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c"  >
                    <asp:Label ID="lblContract" runat="server" Text="Contracted With" 
                        Visible="False" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c" >
                    <asp:Label ID="lblcontractedWith" runat="server" Visible="False" 
                        CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c" >
                    &nbsp;</td>
            </tr>
            <tr __designer:mapid="4">
                <td   
                    __designer:mapid="5">
                    <asp:Label ID="lblSite111" runat="server" Text="Site" CssClass="label"></asp:Label>
                </td>
                <td  __designer:mapid="7">
                    <asp:Label ID="lblSiteName" runat="server" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="a" >
                    <asp:Label ID="label111" runat="server" Text="Last Preventive Maintenance Date" 
                        CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c" >
                    <asp:Label ID="lblLastPreventivemain" runat="server" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c" >
                    <asp:Label ID="lblexpireDate" runat="server" Text="Warranty Expire Date" 
                        Visible="False" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c" >
                    <asp:Label ID="lblWarantiyDate" runat="server" Visible="False" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c" >
                    &nbsp;</td>
            </tr>
            </table>
   </div></div><br />
<asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4>     <asp:Label ID="Label5" runat="server" Text="Schedule Preventive Maintenance" 
            CssClass="label"></asp:Label></h4>
    
 
    <table style="width: 100%">
        <tr>
            <td rowspan="8" style="width: 160px; background-color: #CCCCCC">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px; height: 24px;">
                <asp:CheckBox ID="chkAssign" runat="server" 
                    Text="Assign For External Enginer" AutoPostBack="True" 
                    oncheckedchanged="chkAssign_CheckedChanged" CssClass="label" />
            </td>
            <td style="height: 24px">
                &nbsp;</td>
            <td style="height: 24px">
                &nbsp;</td>
            <td style="height: 24px">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px">
                <asp:Label ID="lblexternalegn" runat="server" Text="External Enginer" 
                    Visible="False" CssClass="label"></asp:Label>
                <br />
                <asp:Label ID="lblenginer" runat="server" Text="Enginer" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlExternalEnginer" runat="server"  
                    AppendDataBoundItems="True" Visible="False" DataTextField="FullName" 
                    DataValueField="Id" CssClass="textbox">
                    <asp:ListItem Value="0">Select External Enginer</asp:ListItem>
                </asp:DropDownList>
                <br />
                <asp:DropDownList ID="ddlEnginer" runat="server" AppendDataBoundItems="True" 
                     DataTextField="FullName" DataValueField="Id" 
                    AutoPostBack="True" 
                    onselectedindexchanged="ddlEnginer_SelectedIndexChanged" CssClass="textbox">
                    <asp:ListItem Value="0">Select Enginer</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td rowspan="5">
              
                <asp:Calendar ID="Calendar2" BorderColor="#3366CC" 
                    BorderWidth="1px" CellPadding="1"
                                DayNameFormat="Shortest" Font-Names="Arial" Font-Size="8pt"
                                ForeColor="#003399" NextPrevFormat="ShortMonth" 
                     runat="server" ondayrender="Calendar2_DayRender" Height="164px" 
                    Width="278px" SelectedDate="11/15/2011 03:51:33" Visible="False"><WeekendDayStyle BackColor="#CCCCFF" /><DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                    <NextPrevStyle ForeColor="#ffffff" Font-Size="7pt"/>
                    <TitleStyle BackColor="#004488" BorderColor="#3366CC" BorderWidth="1px"  
                        Font-Bold="True" Font-Size="10pt"  ForeColor="#ffffff" Height="25px" 
                        Font-Names="Arial Narrow" />
                    </asp:Calendar>
     
             
 
     
             
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     
             
 
     
             
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Booked.png" 
                    Visible="False" />
     
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px; height: 23px;">
                <asp:Label ID="Label7" runat="server" Text="Schedule Date From" 
                    CssClass="label"></asp:Label>
            </td>
            <td style="height: 23px">
                        <asp:TextBox ID="CalendarFrom" runat="server" Width="200px"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarFrom_CalendarExtender" runat="server" 
                            Enabled="True" TargetControlID="CalendarFrom" PopupButtonID="ImgCfrom1">
                        </cc1:CalendarExtender>
            <asp:ImageButton ID="ImgCfrom1" runat="server" 
                                    ImageUrl="~/Images/Calendar_scheduleHS.png" meta:resourcekey="ImgCResource1" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="CalendarFrom" Display="Dynamic" 
                    ErrorMessage="Schedules Date From Required" ValidationGroup="1">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator22" 
                            runat="server" ControlToValidate="CalendarFrom" 
                            ErrorMessage="Schedule Date from is not valid" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator>
            </td>
            <td style="height: 23px">
                </td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px; text-align: left;">
                <asp:Label ID="Label10" runat="server" Text="Schedule Date To" CssClass="label"></asp:Label>
            </td>
            <td>
                        <asp:TextBox ID="CalendarTo" runat="server" Width="200px"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarTo_CalendarExtender" runat="server" 
                            Enabled="True" TargetControlID="CalendarTo" PopupButtonID="ImgCTo1">
                        </cc1:CalendarExtender>
            <asp:ImageButton ID="ImgCTo1" runat="server" 
                                    ImageUrl="~/Images/Calendar_scheduleHS.png" 
                    meta:resourcekey="ImgCResource1" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="CalendarTo" Display="Dynamic" 
                    ErrorMessage="Scheduled Date To Required" ValidationGroup="1">*</asp:RequiredFieldValidator>
            &nbsp;<asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToCompare="CalendarTo" ControlToValidate="CalendarFrom" 
                    Display="Dynamic" 
                    ErrorMessage="Schedule date from should be lessthan to schedule date to" 
                    Operator="LessThanEqual" ValidationGroup="1" Type="Date">*</asp:CompareValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" 
                            runat="server" ControlToValidate="CalendarTo" 
                            ErrorMessage="Schedule Date To is not valid" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px">
                <asp:Label ID="Label8" runat="server" Text="Description" CssClass="label"></asp:Label>
            </td>
            <td rowspan="2">
                <asp:TextBox ID="txtdesc" runat="server"  TextMode="MultiLine" 
                    CssClass="textboxDescShort"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px">
                <asp:Label ID="Label9" runat="server" Text="Date" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtDate" runat="server" Width="200px"></asp:TextBox>
                <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtDate" PopupButtonID="ImgCfrom2">
                </cc1:CalendarExtender>
            <asp:ImageButton ID="ImgCfrom2" runat="server" 
                                    ImageUrl="~/Images/Calendar_scheduleHS.png" 
                    meta:resourcekey="ImgCResource1" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="txtDate" Display="Dynamic" ErrorMessage="Date Required" 
                    ValidationGroup="1">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator24" 
                    runat="server" ControlToValidate="txtDate" ErrorMessage="Date is not valid" 
                    ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                    ValidationGroup="1">*</asp:RegularExpressionValidator>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        </table>
    
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Button ID="btnNew" runat="server" Text="New" onclick="btnNew_Click" />
&nbsp;
                <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="1" 
                    onclick="btnSave_Click" />
&nbsp;
                <asp:Button ID="btnDelete" runat="server" Text="Delete" 
                    onclick="btnDelete_Click" />
&nbsp;
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    onclick="btnCancel_Click" />
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>
