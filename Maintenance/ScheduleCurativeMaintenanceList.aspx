﻿ 

<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.ScheduleCurativeMaintenanceList, App_Web_4ftijqq5" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
         <div Class="section">

    <div cssClass="group">
              <h4>   <asp:Label ID="fsFindInstrument" runat="server" 
                    Text="Find Instrument with Reported Problems" CssClass="label"  ForeColor="White"></asp:Label> </h4>
            </legend>
            <table style="width: 100%" __designer:mapid="97">
                <tr __designer:mapid="98">
                    <td style="width: 151px" __designer:mapid="99">
                        <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
                    </td>
                    <td style="width: 226px" __designer:mapid="9b">
                        <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                            onselectedindexchanged="ddlRegion_SelectedIndexChanged" 
                            Enabled="False" CssClass="textbox">
                            <asp:ListItem Value="0">Select Region</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td __designer:mapid="9e" style="width: 79px">
                        <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                    </td>
                    <td __designer:mapid="a0">
                        <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                            DataTextField="Name" DataValueField="Id"  AutoPostBack="True" 
                            Enabled="False" CssClass="textbox">
                            <asp:ListItem Value="0">Select Site</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <table style="width: 100%" __designer:mapid="a3">
                <tr __designer:mapid="a4">
                    <td class="editDropDown" style="width: 152px" __designer:mapid="a5">
                        <asp:Label ID="lblInstrument" runat="server" Text="Instrument Category" 
                            CssClass="label"></asp:Label>
                    </td>
                    <td style="width: 227px" __designer:mapid="a7">
                        <asp:DropDownList ID="ddlInstrumentCategory" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             AutoPostBack="True" CssClass="textbox">
                            <asp:ListItem Value="0">Select Instrument Category</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 97px; text-align: left" __designer:mapid="aa">
                        <asp:Label ID="lblProblemType" runat="server" Text="Problem Type" 
                            CssClass="label"></asp:Label>
                    </td>
                    <td style="width: 7px; text-align: center" __designer:mapid="aa">
                        <asp:DropDownList ID="ddlproblemType" runat="server" 
                            AppendDataBoundItems="True"  DataTextField="Name" 
                            DataValueField="Id" AutoPostBack="True" CssClass="textbox">
                            <asp:ListItem Value="0">Select Problem Type</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td __designer:mapid="ac">
                        &nbsp;</td>
                </tr>
                </table>
       </div></div>


        <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4>  <asp:Label ID="fsInstrumentList" runat="server" Text="Instrument with Reported Problem List" 
            CssClass="label"></asp:Label></h4>
                
            <asp:GridView ID="grvProblemWithInstrumentList" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True" 
                    ForeColor="#333333" GridLines="Horizontal"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    onrowdatabound="grvProblemWithInstrumentList_RowDataBound" 
                Width="100%" style="text-align: left" AllowPaging="True" 
                onpageindexchanging="grvProblemWithInstrumentList_PageIndexChanging">
            <%--    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />--%>
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="SerialNo" HeaderText="Serial No" />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="WarranityExpireDate" 
                        HeaderText="Waranty Expire Date" />
                    <asp:BoundField DataField="ProblemNumber" HeaderText="Problem No" />
                    <asp:BoundField DataField="ProblemTypeName" HeaderText="Problem Type" />
                    <asp:BoundField DataField="ErrorCodeName" HeaderText="Error Code" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hplSchedule" runat="server" 
                                Text="Schedule Curative Maintenance"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle  ForeColor="White" HorizontalAlign="Center" />
              <%--  <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            <br />
         <br />
        <table style="width: 100%">
            <tr>
                <td style="text-align: right">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/schedule.png" />
                </td>
            </tr>
        </table>
        </asp:Panel>

&nbsp;

</asp:Content>
