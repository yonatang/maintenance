﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.MaintenanceConfirmation, App_Web_4ftijqq5" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc11" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
    
     <div class="section">

     <div cssClass="group"> 
    <table style="width: 100%">
        <tr>
            <td style="text-align: center; width: 501px">
                <asp:RadioButton ID="rbtPrvConfirmation" runat="server" AutoPostBack="True" 
                    Checked="True" GroupName="confirm" Text=" Confirm Preventive Maintenance" 
                    oncheckedchanged="rbtPrvConfirmation_CheckedChanged" CssClass="label" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="btCurative" runat="server" AutoPostBack="True" 
                    GroupName="confirm" Text=" Confirm Curative Maintenance" 
                    oncheckedchanged="btCurative_CheckedChanged" CssClass="label" />
            </td>
        </tr>
    </table>
    </div></div>
    <br />
            <asp:GridView ID="grvCurativeMainenance" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3"   CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
        DataKeyNames="ScheduleID,InstrumentId,ProblemId" EnableModelValidation="True" 
                    GridLines="Horizontal" 
                Width="100%"  
            onrowdatabound="grvCurativeMainenance_RowDataBound" Visible="False" 
        onselectedindexchanged="grvCurativeMainenance_SelectedIndexChanged" 
        AllowPaging="True" 
        onpageindexchanging="grvCurativeMainenance_PageIndexChanging">
                 
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="PreventiveMaintenancePeriod" 
                        HeaderText="Preventive Maint Period" />
                    <asp:BoundField DataField="LastPreventiveMaintenanceDate" 
                        HeaderText="Last Preventive Maint. Date" />
                    <asp:BoundField DataField="ErrorCode" HeaderText="Error Code" />
                    <asp:BoundField DataField="Type" HeaderText="Type" />
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                CommandName="Select" Text="Confirm"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle   ForeColor="White" HorizontalAlign="Center" />
               <%-- <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            <asp:GridView ID="grvPrevMaintenances" runat="server" AutoGenerateColumns="False" 
                     CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
        DataKeyNames="ScheduleID,InstrumentId,ProblemId" EnableModelValidation="True" 
                    GridLines="Horizontal" 
                Width="100%"  
            onrowdatabound="grvPrevMaintenances_RowDataBound" Visible="False" 
        onselectedindexchanged="grvPrevMaintenances_SelectedIndexChanged" 
        AllowPaging="True" onpageindexchanging="grvPrevMaintenances_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="InstallationDate" HeaderText="Installation Date" />
                    <asp:BoundField DataField="PreventiveMaintenancePeriod" 
                        HeaderText="Preventive Maint Period" />
                    <asp:BoundField DataField="LastPreventiveMaintenanceDate" 
                        HeaderText="Last Preventive Maint. Date" />
                    <asp:BoundField DataField="Type" HeaderText="Type" />
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                CommandName="Select" Text="Confirm"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle  ForeColor="White" HorizontalAlign="Center" />
               <%-- <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
               <asp:Panel ID="pnlConfirmation" runat="server" CssClass="group"  Visible="False">
    
                   
                  <div class="section">

     <div cssClass="group"> 
                                <h4>  <asp:Label ID="lbllgdInfo" runat="server" 
                                   Text="Instrument and Notified Problems" CssClass="label" ForeColor="White"></asp:Label></h4>
                        <br />
                               <table style="width: 100%">
                                   <tr>
                                       <td colspan="2">
                                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                           <asp:Label ID="lblInstrument" runat="server" style="font-weight: 700" 
                                               Text="Selected Instrument" CssClass="label"></asp:Label>
                                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                           <asp:Label ID="lblSelectedinstrument" runat="server" style="font-weight: 700" 
                                               CssClass="label"></asp:Label>
                                       </td>
                                       <td rowspan="4">
                                           &nbsp;</td>
                                   </tr>
                                   <tr>
                                       <td>
                                           <asp:Label ID="lblRegion111" runat="server" Text="Region" CssClass="label"></asp:Label>
                                       </td>
                                       <td>
                                           <asp:Label ID="lblregionName" runat="server" CssClass="label"></asp:Label>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td 
                                           valign="top">
                                           <asp:Label ID="Label1" runat="server" Text="Installation Date" CssClass="label"></asp:Label>
                                       </td>
                                       <td valign="top">
                                           <asp:Label ID="lblInstallationDate" runat="server" CssClass="label"></asp:Label>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td colspan="2" 
                                           valign="top">
                                           <asp:Label ID="lblMainType" runat="server" style="font-weight: 700" 
                                               CssClass="label"></asp:Label>
                                       </td>
                                   </tr>
                               </table>
                       </div>                       </div>
                            <table style="width: 100%">
                                <tr>
                                    <td rowspan="4" style="width: 111px; background-color: #CCCCCC">
                                        &nbsp;</td>
                                    <td style="width: 140px; text-align: left">
                                        <asp:Label ID="Label5" runat="server" Text="Is Resolved?" CssClass="label"></asp:Label>
                                    </td>
                                    <td style="text-align: left">
                                        <asp:CheckBox ID="CheckBox1" runat="server" CssClass="label" Enabled="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 140px; text-align: left; height: 52px">
                                        <asp:Label ID="Label6" runat="server" style="text-align: left" 
                                            Text="User Comments" CssClass="label"></asp:Label>
                                    </td>
                                    <td style="height: 52px">
                                        <asp:TextBox ID="txtcomments" runat="server"  
                                            TextMode="MultiLine"   CssClass="textboxDescShort" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 140px; text-align: left; height: 22px">
                                        <asp:Label ID="Label7" runat="server" CssClass="label" Text="Date"></asp:Label>
                                    </td>
                                    <td style="height: 22px">
                                        <cc11:Calendar ID="Calendar1" runat="server" Height="58px" Enabled="False" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                            ControlToValidate="Calendar1" Display="Dynamic" ValidationGroup="1">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator22" 
                                            runat="server" ControlToValidate="Calendar1" ErrorMessage="Date is not valid" 
                                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                                            ValidationGroup="1">*</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 140px; height: 6px">
                                        </td>
                                    <td style="text-align: left">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:LinkButton ID="lnkConfirm0" runat="server" onclick="lnkConfirm0_Click" 
                                            ValidationGroup="1" Enabled="False">Confirm </asp:LinkButton>
                                        &nbsp;<asp:LinkButton ID="lnkCancel" runat="server" onclick="lnkCancel_Click">Close</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
    </asp:Panel>
    <br />
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                        <asp:Image ID="Image2" runat="server" 
                            ImageUrl="~/Images/PrevConfirmationLegend.png" />
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/legends.png" 
                            Visible="False" />
                        </td>
        </tr>
    </table>
</asp:Content>
