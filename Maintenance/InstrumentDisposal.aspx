﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.InstrumentDisposal, App_Web_4ftijqq5" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc11" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblEditing" runat="server" 
                    style="color: #FF3300; background-color: #FFFFFF; font-weight: 700;" Text="Editing..." 
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
   
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        ValidationGroup="1" />
   
   <div class ="section">

   <div cssClass ="group">
       <h4> <asp:Label ID="lbllgdInfo" runat="server" 
            Text="Instrument Information" CssClass="label" ForeColor="White"></asp:Label></h4>
        
        <table style="width: 100%" __designer:mapid="3">
            <tr __designer:mapid="4">
                <td __designer:mapid="5" colspan="4">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblInstrument" runat="server" Text="Selected Instrument" 
                        style="font-weight: 700" CssClass="label"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblSelectedinstrument" runat="server" style="font-weight: 700" 
                        CssClass="label"></asp:Label>
                </td>
            </tr>
            <tr __designer:mapid="4">
                <td 
                    __designer:mapid="5">
                    <asp:Label ID="lblRegion111" runat="server" Text="Region" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="7">
                    <asp:Label ID="lblregionName" runat="server" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="a">
                    <asp:Label ID="Label1" runat="server" Text="Installation Date" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c">
                    <asp:Label ID="lblInstallationDate" runat="server" CssClass="label"></asp:Label>
                </td>
            </tr>
            <tr __designer:mapid="4">
                <td 
                    __designer:mapid="5">
                    <asp:Label ID="lblSite111" runat="server" Text="Site" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="7">
                    <asp:Label ID="lblSiteName" runat="server" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="a">
                    <asp:Label ID="Label14" runat="server" Text="Manufacturer" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="c">
                    <asp:Label ID="lblManufacturer" runat="server" CssClass="label"></asp:Label>
                </td>
            </tr>
            </table>
    </div></div><br />
  <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4>     <asp:Label ID="Label5" runat="server" Text="Instrument Disposal" 
            CssClass="label"></asp:Label></h4>
    
    <table style="width: 100%">
        <tr>
            <td rowspan="5" style="width: 160px; background-color: #CCCCCC">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px; text-align: left;">
                <asp:Label ID="Label11" runat="server" Text="Disposed By" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblDisposedById" runat="server" style="font-weight: 700" 
                    CssClass="label"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px">
                <asp:Label ID="Label8" runat="server" Text="Disposal Reason" CssClass="label"></asp:Label>
            </td>
            <td rowspan="2">
                <asp:TextBox ID="txtreason" runat="server"  TextMode="MultiLine" 
                     CssClass="textboxDescShort"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px">
                <asp:Label ID="Label9" runat="server" Text="Date" CssClass="label"></asp:Label>
            </td>
            <td>
                        <cc11:Calendar ID="Calendar1" runat="server" Height="55px" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="Calendar1" Display="Dynamic" ErrorMessage="Date Required" 
                    ValidationGroup="1">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                            ControlToValidate="Calendar1" ErrorMessage="Disposal Date is not a valid date" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        </table>
    
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Button ID="btnNew" runat="server" Text="New" onclick="btnNew_Click" />
&nbsp;
                <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="1" 
                    onclick="btnSave_Click" />
&nbsp;
                <asp:Button ID="btnDelete" runat="server" Text="Delete" 
                    onclick="btnDelete_Click" />
&nbsp;
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    onclick="btnCancel_Click" />
            </td>
        </tr>
    </table>
    </asp:Panel>
    </asp:Content>

