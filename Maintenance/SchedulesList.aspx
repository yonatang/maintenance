﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.SchedulesList, App_Web_4ftijqq5" stylesheettheme="Default" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <table style="width: 100%">
        <tr>
            <td style="text-align: center; width: 506px">
                <asp:RadioButton ID="rbtPreventive" runat="server" AutoPostBack="True" 
                            GroupName="1" Text="Show Preventive Schedules" CssClass="label" />
            </td>
            <td style="text-align: center">
                <asp:RadioButton ID="rbtCurative" runat="server" AutoPostBack="True" 
                            GroupName="1" Text="Show Curative Schedules" CssClass="label" />
            </td>
        </tr>
        </table>
    <div class="section">

    <div cssClass="group">
      <h4>  <asp:Label ID="Label1" runat="server" CssClass="label" Text="Find Schedule" ForeColor="White"></asp:Label></h4>
      
        <%--    <table style="width: 100%" __designer:mapid="342">
                <tr __designer:mapid="343">
                    <td style="width: 102px" __designer:mapid="344">
                        <asp:Label ID="lblRegion" runat="server" Text="Region"></asp:Label>
                    </td>
                    <td style="width: 226px" __designer:mapid="346">
                        <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                            Width="150px" onselectedindexchanged="ddlRegion_SelectedIndexChanged" 
                            Enabled="False">
                            <asp:ListItem Value="0">Select Region</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td __designer:mapid="349">
                        <asp:Label ID="lblSite" runat="server" Text="Site"></asp:Label>
                    </td>
                    <td __designer:mapid="34b">
                        <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="Name" DataValueField="Id" Width="150px" 
                            Enabled="False">
                            <asp:ListItem Value="0">Select Site</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>--%>
            <table style="width: 100%" __designer:mapid="34e">
                <tr __designer:mapid="34f">
                    <td class="editDropDown" style="width: 151px" __designer:mapid="350">
                        <asp:Label ID="lblInstrument" runat="server" Text="Instrument Category" 
                            CssClass="label"></asp:Label>
                    </td>
                    <td style="width: 222px" __designer:mapid="352">
                        <asp:DropDownList ID="ddlInstrumentCategory" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             AutoPostBack="True" CssClass="textbox">
                            <asp:ListItem Value="0">Select Instrument Category</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="width: 232px; text-align: center" __designer:mapid="355">
                        &nbsp;</td>
                    <td __designer:mapid="357">
                        &nbsp;</td>
                </tr>
            </table>
        </div></div>




            <asp:GridView ID="grvPreventiveSchedule" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                   GridLines="Horizontal" 
                
            onrowdatabound="grvPreventiveSchedule_RowDataBound" Visible="False" 
        AllowPaging="True" 
        onpageindexchanging="grvPreventiveSchedule_PageIndexChanging">
               
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="Type" HeaderText="Type" />
                    <asp:BoundField DataField="ScheduledDateFrom" 
                        HeaderText="Scheduled Date From" />
                    <asp:BoundField DataField="ScheduledDateTo" HeaderText="Scheduled Date To" />
                    <asp:TemplateField>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit Schedule"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle  ForeColor="White" HorizontalAlign="Center" />
               <%-- <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
           <br />
            <asp:GridView ID="grvCurativetiveSchedule" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" 
        DataKeyNames="Id,InstrumentId,ProblemId,SiteId" EnableModelValidation="True"  CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    GridLines="Horizontal" 
               
            onrowdatabound="grvCurativetiveSchedule_RowDataBound" Visible="False" 
        AllowPaging="True" 
        onpageindexchanging="grvCurativetiveSchedule_PageIndexChanging" 
        onselectedindexchanged="grvCurativetiveSchedule_SelectedIndexChanged">
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <Columns>
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="ScheduledDateFrom" 
                        HeaderText="Scheduled Date From" />
                    <asp:BoundField DataField="ScheduledDateTo" HeaderText="Scheduled Date To" />
                    <asp:BoundField DataField="FullName" HeaderText="Enginer" />
                    <asp:BoundField DataField="PushedReason" HeaderText="Pushed Reason" />
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit Schedule"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkEscalate" runat="server" CausesValidation="False" 
                                CommandName="Select" Text="Escalate"></asp:LinkButton>
                            <cc1:ModalPopupExtender ID="lnkEscalate_ModalPopupExtender" runat="server" 
                                CancelControlID="btnNo" DynamicServicePath="" Enabled="True" 
                                OkControlID="btnYes" PopupControlID="pnlConfirmation" 
                                TargetControlID="lnkEscalate">
                            </cc1:ModalPopupExtender>
                            <cc1:ConfirmButtonExtender ID="lnkEscalate_ConfirmButtonExtender" 
                                runat="server" 
                                ConfirmText="Are you sure , You want to escalate?" 
                                DisplayModalPopupID="lnkEscalate_ModalPopupExtender" Enabled="True" 
                                TargetControlID="lnkEscalate">
                            </cc1:ConfirmButtonExtender>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkreschdule" runat="server" Text="Re-Schedule"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle ForeColor="White" HorizontalAlign="Center" />
              <%--  <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
           <br />
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/pushedlegend.jpg" />
            </td>
        </tr>
    </table>

    <asp:Panel ID="pnlConfirmation" runat="server" 
            style="display:none; width:300px; background-color:White; border-width:2px; border-color:Black; border-style:solid; padding:20px;" 
            meta:resourcekey="pnlConfirmationResource1">
                      <asp:Label ID="configmMessage" runat="server" 
                          Text="Are you sure , You want to escalate" 
                          meta:resourceKey="configmMessageResource1" ></asp:Label>
                        <br /><br />
                        <div style="text-align:right;">
                            <asp:Button ID="btnYes" runat="server" Text="Yes"  Width="60px" 
                                meta:resourceKey="btnYesResource1"/>
                            <asp:Button ID="btnNo" runat="server" Text="No" Width="60px" 
                                meta:resourceKey="btnNoResource1"/>
                        </div>
                    </asp:Panel>      
           </asp:Content>