﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.ProblemList, App_Web_4ftijqq5" stylesheettheme="Default" %>


<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
     <div class="section">

     <div cssClass="group">
          <h4>  <asp:Label ID="Label1" runat="server" CssClass="label" Text="Find Schedule" ForeColor="White"></asp:Label></h4>
     
        <table style="width: 100%" __designer:mapid="342">
            <tr __designer:mapid="343">
                <td style="width: 102px" __designer:mapid="344">
                    <asp:Label ID="lblRegion" runat="server" Text="Region" CssClass="label"></asp:Label>
                </td>
                <td style="width: 223px" __designer:mapid="346">
                    <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                             onselectedindexchanged="ddlRegion_SelectedIndexChanged" 
                        Enabled="False" CssClass="textbox">
                        <asp:ListItem Value="0">Select Region</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td __designer:mapid="349" style="width: 136px">
                    <asp:Label ID="lblSite" runat="server" Text="Site" CssClass="label"></asp:Label>
                </td>
                <td __designer:mapid="34b">
                    <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="Name" DataValueField="Id" 
                          Enabled="False" CssClass="textbox">
                        <asp:ListItem Value="0">Select Site</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table style="width: 100%" __designer:mapid="34e">
            <tr __designer:mapid="34f">
                <td class="editDropDown" style="width: 101px" __designer:mapid="350">
                    <asp:Label ID="lblProblemType" runat="server" Text="Problem Type" 
                        CssClass="label"></asp:Label>
                </td>
                <td style="width: 222px" __designer:mapid="352">
                    <asp:DropDownList ID="ddlProblemtype" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             AutoPostBack="True" CssClass="textbox">
                        <asp:ListItem Value="0">Select Problem Type</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 139px; text-align: left" __designer:mapid="355">
                        <asp:Label ID="lblInstrument" runat="server" Text="Instrument Category" 
                            CssClass="label"></asp:Label>
                    </td>
                <td __designer:mapid="357">
                        <asp:DropDownList ID="ddlInstrumentCategory" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                             AutoPostBack="True" CssClass="textbox">
                            <asp:ListItem Value="0">Select Instrument Category</asp:ListItem>
                        </asp:DropDownList>
                    </td>
            </tr>
        </table>

   </div></div>
    <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4>  <asp:Label ID="schedules" runat="server" Text="Schedule List" 
            CssClass="label"></asp:Label></h4>
            <asp:GridView ID="grvProblem" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    GridLines="Horizontal" 
               
            onrowdatabound="grvProblem_RowDataBound" 
        AllowPaging="True" onpageindexchanging="grvProblem_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="ProblemNumber" HeaderText="Problem Number" />
                    <asp:BoundField DataField="SiteName" HeaderText="Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="Description" HeaderText="Description" />
                    <asp:BoundField DataField="ProblemTypeName" 
                        HeaderText="Problem Type" />
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit Problems"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle  ForeColor="White" HorizontalAlign="Center" />
               <%-- <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>

    <br />
    <br />
    <br />
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Image ID="Image1" runat="server" 
                    ImageUrl="~/Images/problemListLegend.png" />
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>