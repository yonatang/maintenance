﻿
<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.ConsumablenotificationEdit, App_Web_4ftijqq5" stylesheettheme="Default" %>

<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %><%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc11" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
    <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Label ID="lblEditing" runat="server" 
                    style="color: #FF3300; background-color: #FFFFFF; font-weight: 700;" Text="Editing..." 
                    Visible="False"></asp:Label>
            </td>
        </tr>
    </table>
   
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        ValidationGroup="1" />

 <asp:Panel ID="Panel2" runat="server" CssClass="group">
    <h4>     <asp:Label ID="Label5" runat="server" Text="Consumbale Notification" 
            CssClass="label"></asp:Label></h4>
  
     
    <table style="width: 100%">
        <tr>
            <td rowspan="7" style="width: 160px; background-color: #CCCCCC">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px; text-align: left;">
                <asp:Label ID="Label13" runat="server" CssClass="label" Text="Site"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                    DataTextField="Name" DataValueField="Id" CssClass="textbox">
                    <asp:ListItem Value="0">Select Site</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                    ControlToValidate="ddlSite" ErrorMessage="Site is Required" InitialValue="0" 
                    ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px; text-align: left;">
                <asp:Label ID="Label11" runat="server" Text="Notified By" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblNotifiedById" runat="server" style="font-weight: 700" 
                    CssClass="label"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px; text-align: left;">
                <asp:Label ID="Label12" runat="server" Text="Consumable" CssClass="label"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlConsumbale" runat="server" AppendDataBoundItems="True" 
                    DataTextField="Name" DataValueField="Id" CssClass="textbox">
                    <asp:ListItem Value="0">Select Consumable</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                    ControlToValidate="ddlConsumbale" ErrorMessage="Consumable is required" 
                    InitialValue="0" ValidationGroup="1">*</asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px">
                <asp:Label ID="Label8" runat="server" Text="Remark" CssClass="label"></asp:Label>
            </td>
            <td rowspan="2">
                <asp:TextBox ID="txtremark" runat="server"  TextMode="MultiLine" CssClass="textboxDescShort" 
                    ></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="editDropDown" style="width: 159px">
                <asp:Label ID="Label9" runat="server" Text="Date" CssClass="label"></asp:Label>
            </td>
            <td>
                        <cc11:Calendar ID="Calendar1" runat="server" Height="45px" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="Calendar1" Display="Dynamic" ErrorMessage="Date Required" 
                    ValidationGroup="1">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                            ControlToValidate="Calendar1" 
                            ErrorMessage="Notification Date is not a valid date" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1">*</asp:RegularExpressionValidator>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        </table>
    
     <table style="width: 100%">
        <tr>
            <td style="text-align: right">
                <asp:Button ID="btnNew" runat="server" Text="New" onclick="btnNew_Click" />
&nbsp;
                <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="1" 
                    onclick="btnSave_Click" />
&nbsp;
                <asp:Button ID="btnDelete" runat="server" Text="Delete" 
                    onclick="btnDelete_Click" />
&nbsp;
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                    onclick="btnCancel_Click" />
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>

