﻿<%@ page title="" language="C#" masterpagefile="~/Shared/ModuleMaster.master" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.InstrumentTransferList, App_Web_4ftijqq5" stylesheettheme="Default" %>
<asp:Content ID="Content1" runat="server" contentplaceholderid="DefaultContent">
     <div class="section">

     <div cssClass="group"> 
         <h4>   <asp:Label ID="Label1" runat="server" CssClass="label" ForeColor="White" 
                Text="Find Transfered Instruments"></asp:Label></h4>
      
        <%--<table style="width: 100%" __designer:mapid="342">
            <tr __designer:mapid="343">
                <td style="width: 102px" __designer:mapid="344">
                    <asp:Label ID="lblRegion" runat="server" Text="Region"></asp:Label>
                </td>
                <td style="width: 226px" __designer:mapid="346">
                    <asp:DropDownList ID="ddlRegion" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="RegionName" DataValueField="Id" 
                            Width="150px" onselectedindexchanged="ddlRegion_SelectedIndexChanged">
                        <asp:ListItem Value="0">Select Region</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td __designer:mapid="349">
                    <asp:Label ID="lblSite" runat="server" Text="Site"></asp:Label>
                </td>
                <td __designer:mapid="34b">
                    <asp:DropDownList ID="ddlSite" runat="server" AppendDataBoundItems="True" 
                            AutoPostBack="True" DataTextField="Name" DataValueField="Id" Width="150px">
                        <asp:ListItem Value="0">Select Site</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>--%>
        <table style="width: 100%" __designer:mapid="34e">
            <tr __designer:mapid="34f">
                <td class="editDropDown" style="width: 151px" __designer:mapid="350">
                    <asp:Label ID="lblInstrument" runat="server" Text="Instrument Category" 
                        CssClass="label"></asp:Label>
                </td>
                <td style="width: 222px" __designer:mapid="352">
                    <asp:DropDownList ID="ddlInstrumentCategory" runat="server" 
                            AppendDataBoundItems="True" DataTextField="Name" DataValueField="Id" 
                              AutoPostBack="True" CssClass="textbox">
                        <asp:ListItem Value="0">Select Instrument Category</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 232px; text-align: center" __designer:mapid="355">
                    &nbsp;</td>
                <td __designer:mapid="357">
                    &nbsp;</td>
            </tr>
        </table>
   </div></div>
    <asp:GridView ID="grvTransferedInstruments" runat="server" AutoGenerateColumns="False" 
                    CellPadding="3" DataKeyNames="Id" EnableModelValidation="True"   CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"
                    GridLines="Horizontal" 
                Width="100%"  
            onrowdatabound="grvTransferedInstruments_RowDataBound" 
        AllowPaging="True" 
        onpageindexchanging="grvTransferedInstruments_PageIndexChanging">
                
                <Columns>
                    <asp:BoundField DataField="OldSite" HeaderText="Old Site" />
                    <asp:BoundField DataField="NewSite" HeaderText="New Site" />
                    <asp:BoundField DataField="InstrumentName" HeaderText="Instrument" />
                    <asp:BoundField DataField="Reason" 
                        HeaderText="Transfer Reason" />
                    <asp:BoundField DataField="Date" HeaderText="Date" />
                    <asp:TemplateField>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit Transfer"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle   ForeColor="White" HorizontalAlign="Center" />
               <%-- <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#999999" />--%>
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>

</asp:Content>

