﻿<%@ page language="C#" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Maintenance.Views.MaintenanceDefault, App_Web_4ftijqq5" title="Default" masterpagefile="~/Shared/ModuleMaster.master" stylesheettheme="Default" %>

<%@ Register assembly="Chai.Maintenance.ServerControls" namespace="Chai.Maintenance.ServerControls" tagprefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" Runat="Server">
    <script src="../FusionCharts/FusionCharts.js" type="text/javascript"></script>
    <br/>
    <table style="width: 100%">
        <tr>
            <td style="width: 177px; text-align: center">
                <asp:Label ID="Label3" runat="server" Text="Date From" 
                style="font-weight: 700" CssClass="label"></asp:Label>
            </td>
            <td style="width: 76px">
            <asp:TextBox ID="txtDatefrom1" runat="server" ValidationGroup="1"></asp:TextBox>
            <cc2:CalendarExtender ID="txtDatefrom1_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtDatefrom1" PopupButtonID="ImgCfrom1">
            </cc2:CalendarExtender>
            </td>
            <td>
            <asp:ImageButton ID="ImgCfrom1" runat="server" 
                                    ImageUrl="~/Images/Calendar_scheduleHS.png" meta:resourcekey="ImgCResource1" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                            ControlToValidate="txtDatefrom1" 
                            ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1" style="font-size: small">*</asp:RegularExpressionValidator>
            </td>
            <td class="editTextBox" style="width: 109px">
                <asp:Label ID="Label4" runat="server" Text="Date To" 
                style="font-weight: 700"></asp:Label>
            </td>
            <td style="width: 91px">
            <asp:TextBox ID="txtDateTo1" runat="server" ValidationGroup="1"></asp:TextBox>
                <cc2:CalendarExtender ID="txtDateTo1_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtDateTo1" PopupButtonID="ImgCTo1">
                </cc2:CalendarExtender>
            </td>
            <td class="editTextBox" style="width: 63px">
            <asp:ImageButton ID="ImgCTo1" runat="server" 
                                    ImageUrl="~/Images/Calendar_scheduleHS.png" 
                    meta:resourcekey="ImgCResource1" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                            ControlToValidate="txtDateTo1" 
                            ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="1" style="font-size: small">*</asp:RegularExpressionValidator>
            </td>
            <td class="editTextBox" style="width: 125px">
            <asp:LinkButton ID="lnkView1" runat="server" onclick="lnkView1_Click" 
                    ValidationGroup="1">View</asp:LinkButton>
            </td>
            <td style="width: 312px; text-align: right">
                <asp:Label ID="Label5" runat="server" Text="Date From" 
                style="font-weight: 700" CssClass="label"></asp:Label>
            </td>
            <td style="width: 92px">
            <asp:TextBox ID="txtDatefrom2" runat="server" ValidationGroup="2"></asp:TextBox>
                <cc2:CalendarExtender ID="txtDatefrom2_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtDatefrom2" PopupButtonID="ImgCfrom2">
                </cc2:CalendarExtender>
            </td>
            <td class="editTextBox" style="width: 66px">
            <asp:ImageButton ID="ImgCfrom2" runat="server" 
                                    ImageUrl="~/Images/Calendar_scheduleHS.png" 
                    meta:resourcekey="ImgCResource1" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                            ControlToValidate="txtDatefrom2" 
                            ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="2" style="font-size: small">*</asp:RegularExpressionValidator>
            </td>
            <td class="editTextBox" style="width: 85px">
                <asp:Label ID="Label6" runat="server" Text="Date To" 
                style="font-weight: 700"></asp:Label>
            </td>
            <td>
            <asp:TextBox ID="txtDateTo2" runat="server" ValidationGroup="2"></asp:TextBox>
                <cc2:CalendarExtender ID="txtDateTo2_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtDateTo2" PopupButtonID="ImgCto2">
                </cc2:CalendarExtender>
            </td>
            <td>
            <asp:ImageButton ID="ImgCto2" runat="server" 
                                    ImageUrl="~/Images/Calendar_scheduleHS.png" 
                    meta:resourcekey="ImgCResource1" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                            ControlToValidate="txtDateTo2" 
                            ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="2" style="font-size: small">*</asp:RegularExpressionValidator>
            </td>
            <td>
            <asp:LinkButton ID="lnkView2" runat="server" onclick="lnkView2_Click" 
                    ValidationGroup="2">View</asp:LinkButton>
            </td>
        </tr>
    </table>
<table style="width: 100%">
    <tr>
        <td style="text-align: center">
            <asp:Literal ID="ltrNotifiedProblems" runat="server"></asp:Literal>
        </td>
        <td style="text-align: center">
            <asp:Literal ID="ltrFrequentlyMaindIns" runat="server"></asp:Literal>
        </td>
    </tr>
    </table>

    <table style="width: 100%">
        <tr>
            <td style="width: 177px; text-align: center">
                <asp:Label ID="Label7" runat="server" Text="Date From" 
                style="font-weight: 700" CssClass="label"></asp:Label>
            </td>
            <td style="width: 76px">
            <asp:TextBox ID="txtDatefrom3" runat="server" ValidationGroup="3"></asp:TextBox>
            <cc2:CalendarExtender ID="txtDatefrom3_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtDatefrom3" PopupButtonID="ImgCfrom3">
            </cc2:CalendarExtender>
            </td>
            <td>
            <asp:ImageButton ID="ImgCfrom3" runat="server" 
                                    ImageUrl="~/Images/Calendar_scheduleHS.png" 
                    meta:resourcekey="ImgCResource1" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" 
                            ControlToValidate="txtDatefrom3" 
                            ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="3" style="font-size: small">*</asp:RegularExpressionValidator>
            </td>
            <td class="editTextBox" style="width: 109px">
                <asp:Label ID="Label8" runat="server" Text="Date To" 
                style="font-weight: 700" CssClass="label"></asp:Label>
            </td>
            <td style="width: 91px">
            <asp:TextBox ID="txtDateTo3" runat="server" ValidationGroup="3"></asp:TextBox>
                <cc2:CalendarExtender ID="txtDateTo3_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtDateTo3" PopupButtonID="ImgCTo3">
                </cc2:CalendarExtender>
            </td>
            <td class="editTextBox" style="width: 63px">
            <asp:ImageButton ID="ImgCTo3" runat="server" 
                                    ImageUrl="~/Images/Calendar_scheduleHS.png" 
                    meta:resourcekey="ImgCResource1" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
                            ControlToValidate="txtDateTo3" 
                            ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="3" style="font-size: small">*</asp:RegularExpressionValidator>
            </td>
            <td class="editTextBox" style="width: 125px">
            <asp:LinkButton ID="lnkView3" runat="server" onclick="lnkView3_Click" 
                    ValidationGroup="3">View</asp:LinkButton>
            </td>
            <td style="width: 312px; text-align: right">
                <asp:Label ID="Label9" runat="server" Text="Date From" 
                style="font-weight: 700" CssClass="label"></asp:Label>
            </td>
            <td style="width: 92px">
            <asp:TextBox ID="txtDatefrom4" runat="server" ValidationGroup="4"></asp:TextBox>
                <cc2:CalendarExtender ID="txtDatefrom4_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtDatefrom4" PopupButtonID="ImgCfrom4">
                </cc2:CalendarExtender>
            </td>
            <td class="editTextBox" style="width: 66px">
            <asp:ImageButton ID="ImgCfrom4" runat="server" 
                                    ImageUrl="~/Images/Calendar_scheduleHS.png" 
                    meta:resourcekey="ImgCResource1" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" 
                            ControlToValidate="txtDatefrom4" 
                            ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="4" style="font-size: small">*</asp:RegularExpressionValidator>
            </td>
            <td class="editTextBox" style="width: 85px">
                <asp:Label ID="Label10" runat="server" Text="Date To" 
                style="font-weight: 700" CssClass="label"></asp:Label>
            </td>
            <td>
            <asp:TextBox ID="txtDateTo4" runat="server" ValidationGroup="4"></asp:TextBox>
                <cc2:CalendarExtender ID="txtDateTo4_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="txtDateTo4" PopupButtonID="ImgCto4">
                </cc2:CalendarExtender>
            </td>
            <td>
            <asp:ImageButton ID="ImgCto4" runat="server" 
                                    ImageUrl="~/Images/Calendar_scheduleHS.png" 
                    meta:resourcekey="ImgCResource1" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" 
                            ControlToValidate="txtDateTo4" 
                            ErrorMessage="*" 
                            ValidationExpression="^([1-9]|0[1-9]|1[0-2])[- / .]([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])[- / .](1[9][0-9][0-9]|2[0][0-9][0-9])$" 
                            ValidationGroup="4" style="font-size: small">*</asp:RegularExpressionValidator>
            </td>
            <td>
            <asp:LinkButton ID="lnkView4" runat="server" onclick="lnkView4_Click" 
                    ValidationGroup="4">View</asp:LinkButton>
            </td>
        </tr>
    </table>


    <table style="width: 100%">
        <tr>
            <td style="text-align: center">
                <asp:Literal ID="ltrFrequentProblemNotificationByManu" runat="server"></asp:Literal>
            </td>
            <td style="text-align: center">
                <asp:Literal ID="ltrFrequentlyReportedProblemTypes" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>

