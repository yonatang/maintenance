﻿<%@ page language="C#" autoeventwireup="true" inherits="ShellDefault, App_Web_smodhl5n" masterpagefile="~/Shared/HomeMaster.master" stylesheettheme="Default" %>
<%@ MasterType TypeName="Chai.Maintenance.Modules.Shell.BaseMaster" %>

<asp:Content ID="content" ContentPlaceHolderID="DefaultContent" Runat="Server">

    <script src="FusionMaps/FusionMaps.js" type="text/javascript"></script>
   <asp:Label ID="lblHovermap" runat="server" Text="Hover on each State/Region to view summary" Font-Bold="True" Font-Italic="True"></asp:Label>
                    
                   
   <center><asp:Literal ID="ltrmap" runat="server"></asp:Literal></center>

    
</asp:Content>