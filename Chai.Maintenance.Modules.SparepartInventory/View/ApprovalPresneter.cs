﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Admin;
using Chai.Maintenance.CoreDomain;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public class ApprovalPresneter: Presenter<IApprovalEditView>
    {
          private SparepartInventoryController _controller;
          private ConfigurationController _configcontroller;
          private ApprovalItemDetail appdetail;
          private AdminController _admincontroller;
          public ApprovalPresneter([CreateNew] SparepartInventoryController controller,[CreateNew] ConfigurationController concontroller,[CreateNew] AdminController admincontroller)
        {
            this._controller = controller;
            this._configcontroller = concontroller;
            this._admincontroller = admincontroller;
        }
          public override void OnViewLoaded()
          {
              View.user = _controller.CurrentUser;
              View.approval = _controller.ApprovalObject;
              View.Requester = _admincontroller.GetRequesterByUserId(View.RequestedBy);
              if (View.approval.Id < 0)
              {
                  if (View.approval.ItemDetail.Count == 0)
                  {
                      if (View.requestId != 0)
                          foreach (RequestItemDetail detail in _controller.GetListofRequestedItems(View.requestId))
                          {
                              appdetail = new ApprovalItemDetail();

                              appdetail.InsrumentId = detail.InstrumentId;
                              appdetail.SparepartId = detail.SparepartId;
                              appdetail.RequestedQty = detail.Qty;
                              appdetail.InstrumentName = detail.InstrumentName;
                              appdetail.SparepartName = detail.SparepartName;
                              View.approval.ItemDetail.Add(appdetail);


                          }
                  }

              }
          }
        public override void OnViewInitialized()
        {



            if (View.Id != 0)
            {
                _controller.GetApprovalById(View.Id);
            }
            else
            {
               
                _controller.NewApproval();
               
            }
          View.Requester=  _admincontroller.GetRequesterByUserId(View.RequestedBy);
        }
        public void SaveorUpdateApproval(Approval approval)
        {
            _controller.SaveOrUpdateApproval(approval);
        }
        public IList<InstrumentType> GetInstrumentName()
        {
            return _configcontroller.GetInstrumentTypes(0, "", 0);
        }

        public IList<SparepartType> GetSparepart(int insId)
        {
            return _configcontroller.GetSparepartTypes(3, "", insId);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/SparepartInventory/ApprovalList.aspx?{0}=3", AppConstants.TABID));
        }
        public void DeleteApproval(int approvalid)
        {
            _controller.DeleteApproval(approvalid);
        }
        public AutoNumber GetAutoNumberByPageId(int pageId)
        {
            return _configcontroller.GetAutoNumberByPageId(pageId);
        }
    }
}
