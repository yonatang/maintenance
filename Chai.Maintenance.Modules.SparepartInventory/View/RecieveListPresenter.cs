﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain.SparepartInventory;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
   public class RecieveListPresenter : Presenter<IRecieveListView>
    {
        private SparepartInventoryController _controller;

        public RecieveListPresenter([CreateNew] SparepartInventoryController controller)
        {
            this._controller = controller;
        }
        public override void OnViewLoaded()
        {

            View.user = _controller.CurrentUser;
            View._recieve = _controller.RecieveListObject;

        }

        public override void OnViewInitialized()
        {


           

        }
        public void GetRecieveList(int findby,string value,string dateFrom,string dateto,int regionId)
        {
            _controller.GetRecieves(findby, value, dateFrom, dateto, regionId);
            View._recieve=_controller.RecieveListObject;
        }
    }
}
