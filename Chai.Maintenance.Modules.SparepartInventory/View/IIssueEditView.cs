﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.CoreDomain;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public interface IIssueEditView
    {
        int Id { get; }
        int ApprovedId { get; }
        int RequesterId { get; }
        int approver { get; }
        User Requester { set; }
        User Approver { set; }
        User user { set; }
        Issue issue { get; set; }
        IList<IssueItemDetail> _issueitemdetail { get; set; }
    }
}
