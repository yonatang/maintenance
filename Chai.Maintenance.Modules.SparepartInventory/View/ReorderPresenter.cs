﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Shared;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public class ReorderPresenter: Presenter<IReorderView>
    {
        private SparepartInventoryController _controller;
        public ReorderPresenter([CreateNew] SparepartInventoryController controller)
          {
             this._controller = controller;
            
          }
        public override void OnViewLoaded()
        {

            View._stockqty = _controller.GetReorderSparepart(_controller.CurrentUser.RegionId);
            View.user = _controller.CurrentUser;
        }

        public override void OnViewInitialized()
        {


        }
    }
}
