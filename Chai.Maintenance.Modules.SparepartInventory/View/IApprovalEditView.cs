﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.CoreDomain;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public interface IApprovalEditView
    {
        int Id { get; }
        int requestId { get; }
        int RequestedBy { get; }
        User Requester { set; }
        Approval approval { get; set; }
        User user { set; }
        IList<ApprovalItemDetail> _approvaldetail { get; set; }
    }
}
