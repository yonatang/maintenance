﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Shared;
using Chai.Maintenance.Modules.Admin;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public class IssuePresneter : Presenter<IIssueEditView>
    {
        private SparepartInventoryController _controller;
        private ConfigurationController _configcontroller;
        private IssueItemDetail issuedetail;
        AdminController _admincontroller;
        public IssuePresneter([CreateNew] SparepartInventoryController controller, [CreateNew] AdminController admincontroller, [CreateNew] ConfigurationController configcontroller)
        {
            this._controller = controller;
            this._admincontroller = admincontroller;
            this._configcontroller = configcontroller;
        }
        public override void OnViewLoaded()
        {
                View.issue = _controller.IssueObject;
                View.Requester = _admincontroller.GetRequesterByUserId(View.RequesterId);
                View.Approver = _admincontroller.GetApprover;
                View.user = _controller.CurrentUser;
            if (View.issue.ItemDetail.Count == 0)
            {
                if (View.ApprovedId != 0)
                    foreach (ApprovalItemDetail detail in _controller.GetListofapprovalItems(View.ApprovedId, _controller.CurrentUser.RegionId))
                    {
                        issuedetail = new IssueItemDetail();

                        issuedetail.InstrumentId = detail.InsrumentId;
                        issuedetail.SparepartId = detail.SparepartId;
                        issuedetail.ApprovedQty = detail.ApprovedQty;
                        issuedetail.InstrumentName = detail.InstrumentName;
                        issuedetail.SparepartName = detail.SparepartName;
                        if (_controller.GetStockQty(_controller.CurrentUser.RegionId, detail.SparepartId) != null)
                        {
                            issuedetail.StockQty = _controller.GetStockQty(_controller.CurrentUser.RegionId, detail.SparepartId).Qty;

                        }
                        else
                        {
                            issuedetail.StockQty = 0;
                        }
                        View.issue.ItemDetail.Add(issuedetail);


                    }
            }
            

        }

        public override void OnViewInitialized()
        {

            if (View.Id != 0)
            {
                _controller.GetIssueById(View.Id,_controller.CurrentUser.RegionId);
            }
            else
            {

                _controller.NewIssue();

            }
            _admincontroller.GetApproverByUserId(View.approver);
            _admincontroller.GetRequesterByUserId(View.RequesterId);

        }
        public void SaveorUpdateIssue(Issue issue)
        {
            _controller.SaveOrUpdateIssue(issue);
        }
        public IList<InstrumentType> GetInstrumentName()
        {
            return _configcontroller.GetInstrumentTypes(0, "", 0);
        }

        public IList<SparepartType> GetSparepart(int insId)
        {
            return _configcontroller.GetSparepartTypes(3, "", insId);
        }

        public void CancelPage()
        {
            _controller.Navigate(String.Format("~/SparepartInventory/IssueList.aspx?{0}=3", AppConstants.TABID));
        }
        public void DeleteIssue(int issueid)
        {
            _controller.DeleteIssue(issueid);
        }
        public AutoNumber GetAutoNumberByPageId(int pageId)
        {
            return _configcontroller.GetAutoNumberByPageId(pageId);
        }
    }
}
