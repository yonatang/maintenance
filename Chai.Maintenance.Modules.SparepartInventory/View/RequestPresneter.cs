﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Shared;

namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    
    public class RequestPresneter : Presenter<IRequestEditView>
        
    {
        private SparepartInventoryController _controller;
         private ConfigurationController _configcontroller;
         public RequestPresneter([CreateNew] SparepartInventoryController controller, [CreateNew] ConfigurationController configcontroller)
          {
             this._controller = controller;
             this._configcontroller = configcontroller;
          }
         public override void OnViewLoaded()
         {
            
                 View.request = _controller.RequestObject;
                 View.user = _controller.CurrentUser;
            
         }

         public override void OnViewInitialized()
         {


             if (View.Id != 0)
                 _controller.GetRequestById(View.Id);
             else
                
                 _controller.NewRequest();

         }
         public void SaveorUpdateRequest(Request request)
         {
             _controller.SaveOrUpdateRequest(request);
         }
         public IList<InstrumentType> GetInstrumentName()
         {
            return _configcontroller.GetInstrumentTypes(0, "", 0);
         }
         
         public IList<SparepartType> GetSparepart(int insId)
         {
             return _configcontroller.GetSparepartTypes(3, "", insId);
         }
        
         public void CancelPage()
         {
             _controller.Navigate(String.Format("~/SparepartInventory/requestList.aspx?{0}=3", AppConstants.TABID));
         }
         public void DeleteRequest(int requestid)
         {
             _controller.DeleteRequest(requestid);
         }
         public void DeleteRequestDetail(int requestdetailId)
         {
             _controller.DeleteRequestDetail(requestdetailId);
         }
         public AutoNumber GetAutoNumberByPageId(int pageId)
         {
             return _configcontroller.GetAutoNumberByPageId(pageId);
         }
    }
}
