﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Shared;

namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public class BeginingBalancePresenter:Presenter<IBeginingBalanceView>
    {
        
        private SparepartInventoryController _controller;
        private ConfigurationController _configcontroller;
         public BeginingBalancePresenter([CreateNew] SparepartInventoryController controller, [CreateNew] ConfigurationController configcontroller)
          {
             this._controller = controller;
             this._configcontroller = configcontroller;
          }
         public override void OnViewLoaded()
         {

             View.user = _controller.CurrentUser;
             View.beginingbalanceList = _controller.BeginingBalanceObject;
         }

         public override void OnViewInitialized()
         {

             _controller.GetListofStock(0, 0, _controller.CurrentUser.RegionId);
         }
         public IList<InstrumentType> GetInstrumentName()
         {
             return _configcontroller.GetInstrumentTypes(0, "", 0);
         }
         public IList<SparepartType> GetSparepart(int insId)
         {
             return _configcontroller.GetSparepartTypes(3, "", insId);
         }
         public IList<Manufacturer> getManufacturer()
         {
             return _configcontroller.GetManufacturers("");
         }
         public void SaveorUpdateStockQty(StockQty stockqty)
         {
             _controller.SaveorUpdateStockQty(stockqty);
         }
         public void DeleteStockQty(int Id)
         {
             _controller.DeleteStockQty(Id);
         }
         public void GetListOfStockQty(int instrumentId,int sparepartId,int regionId)
         {
             _controller.GetListofStock(instrumentId, sparepartId, regionId);
             View.beginingbalanceList = _controller.BeginingBalanceObject;
         }
    }
}
