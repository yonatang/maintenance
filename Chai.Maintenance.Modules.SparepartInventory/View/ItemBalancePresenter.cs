﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Shared;

namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public class ItemBalancePresenter :Presenter<IItemBalanceView>
    {
        private SparepartInventoryController _controller;
         private ConfigurationController _configcontroller;
         public ItemBalancePresenter([CreateNew] SparepartInventoryController controller, [CreateNew] ConfigurationController configcontroller)
          {
             this._controller = controller;
             this._configcontroller = configcontroller;
          }
         public void OnViewLoaded()
         {
            
             View.user = _controller.CurrentUser;
         }
         public override void OnViewInitialized()
         {



         }
        public IList<StockQty> GetItembalance(int instrumentid,int sparepartId,int regionId)
        {
            return _controller.GetItembalance(instrumentid, sparepartId, regionId);
        }
        public IList<StockQty> GetReorderSparepart(int regionId)
        {
            return _controller.GetReorderSparepart(regionId);
        }
         public IList<InstrumentType> GetInstrumentName()
         {
             return _configcontroller.GetInstrumentTypes(0, "", 0);
         }
         public IList<SparepartType> GetSparepart(int insId)
         {
             return _configcontroller.GetSparepartTypes(3, "", insId);
         }
    }
}
