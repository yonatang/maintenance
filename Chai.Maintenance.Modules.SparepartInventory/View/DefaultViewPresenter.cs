using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.Modules.Report;
using Chai.Maintenance.Modules.Configuration;
using System.Data;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public class DefaultViewPresenter : Presenter<IDefaultView>
    {
        private ReportController _reportController;
        private SparepartInventoryController _controller;
        private ConfigurationController _configcontroller;
        public DefaultViewPresenter([CreateNew] SparepartInventoryController controller,[CreateNew] ConfigurationController configcontroller, [CreateNew] ReportController reportController)
        {
            this._controller = controller;
            this._configcontroller = configcontroller;
            this._reportController = reportController;
        }
        public override void OnViewLoaded()
        {
            View.user = _controller.CurrentUser;
        }
        public override void OnViewInitialized()
        {
           
        }
        public DataSet GetMostIssuedSparepartsbyInstrumentsChart(DateTime datefrom, DateTime dateto)
        {

            return _reportController.GetMostIssuedSparepartsbyInstrumentsChart(datefrom, dateto);
        }
        public DataSet GetMostRequestedSparepartsbyInstrumentsChart(DateTime datefrom, DateTime dateto)
        {

            return _reportController.GetMostRequestedSparepartsbyInstrumentsChart(datefrom, dateto);
        }
    }
}
