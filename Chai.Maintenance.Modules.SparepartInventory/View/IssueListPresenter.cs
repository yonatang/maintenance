﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain.SparepartInventory;

namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public class IssueListPresenter:Presenter<IIssueListView>
    {
        private SparepartInventoryController _controller;
        public IssueListPresenter([CreateNew] SparepartInventoryController controller)
        {
            this._controller = controller;
        }
        public override void OnViewLoaded()
        {

            View.issue = _controller.IssueListObject;
            View.user = _controller.CurrentUser;

        }

        public override void OnViewInitialized()
        {


           

        }
        public void GetIssueList(int findby, string value, string dateFrom, string dateto,int regionId)
        {
            _controller.GetIssues(findby, value, dateFrom, dateto, regionId);
            View.issue = _controller.IssueListObject;
        }
    }
}
