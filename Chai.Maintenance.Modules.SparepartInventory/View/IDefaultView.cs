using System;
using System.Collections.Generic;
using System.Text;
using Chai.Maintenance.CoreDomain;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public interface IDefaultView
    {
        User user { set; }
    }
}
