﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.Configuration;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Enums;
using Chai.Maintenance.Shared;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public class RecievePresenter : Presenter<IRecieveEditView>
    {
         private SparepartInventoryController _controller;
         private ConfigurationController _configcontroller;
         public RecievePresenter([CreateNew] SparepartInventoryController controller,[CreateNew] ConfigurationController configcontroller)
          {
             this._controller = controller;
             this._configcontroller = configcontroller;
          }
         public override void OnViewLoaded()
         {
            
                 View.recieve = _controller.RecieveObject;
                 View.user = _controller.CurrentUser;
         }

         public override void OnViewInitialized()
         {


             if (View.Id != 0)
                 _controller.GetRecieveById(View.Id);
             else
                 _controller.NewRecieve();

         }
         public void SaveorUpdateRecieve(Recieve recieve)
         {
             _controller.SaveOrUpdateRecieve(recieve);
         }
         public IList<InstrumentType> GetInstrumentName()
         {
            return _configcontroller.GetInstrumentTypes(0, "", 0);
         }
         public IList<Vendor> GetVendor()
         {
             return _configcontroller.GetVendors("");
         }
         public IList<SparepartType> GetSparepart(int insId)
         {
             return _configcontroller.GetSparepartTypes(3, "", insId);
         }
         public IList<Manufacturer> getManufacturer()
         {
             return _configcontroller.GetManufacturers("");
         }
         public void CancelPage()
         {
             _controller.Navigate(String.Format("~/SparepartInventory/RecieveList.aspx?{0}=3", AppConstants.TABID));
         }
         public void DeleteRecieve(int recieveid)
         {
             _controller.DeleteRecieve(recieveid);
         }
         public void DeleteRecieveDetail(int recievedetailId)
         {
             _controller.DeleteRecieveDetail(recievedetailId);
         }
         public void  PostItems(IList<RecieveItemDetail> ItemDetail,int regionId,int recieveId)

         {
             _controller.postItems(ItemDetail, regionId, recieveId);
         }
            
    }
}
