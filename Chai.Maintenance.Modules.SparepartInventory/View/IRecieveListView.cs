﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.CoreDomain;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public interface IRecieveListView
    {
        IList<Recieve> _recieve { set; }
        User user { set; }
    }
}
