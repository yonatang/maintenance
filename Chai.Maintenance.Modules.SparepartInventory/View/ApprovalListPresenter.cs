﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.Modules.Admin;
using Chai.Maintenance.CoreDomain;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public class ApprovalListPresenter : Presenter<IApprovalListView>
    {
         private SparepartInventoryController _controller;
         private AdminController _admincontrller;
         public ApprovalListPresenter([CreateNew] SparepartInventoryController controller, [CreateNew] AdminController admincontroller)
        {
            this._controller = controller;
            this._admincontrller = admincontroller;
        }
        public override void OnViewLoaded()
        {

            View.approval = _controller.ApprovalListObject;
            View.user = _controller.CurrentUser;
        }

        public override void OnViewInitialized()
        {
                      

        }
        public void GetApprovalList(int findby, string value, string dateFrom, string dateto,int regionId,int userId)
        {
            _controller.GetApprovals(findby, value, dateFrom, dateto, regionId, userId);
             View.approval = _controller.ApprovalListObject;
        }
        public void GetApprovalforIssue(int findby, string value, string dateFrom, string dateto, int regionId, int userId)
        {
            _controller.GetApprovalsforIssue(findby, value, dateFrom, dateto, regionId, userId);
            View.approval = _controller.ApprovalListObject;
        }
        public IList<User> GetUsers(int regionId)
        {
            return _admincontrller.GetUsers(regionId);
        }
    }
}
