﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.ObjectBuilder;
using Microsoft.Practices.CompositeWeb;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.Modules.Admin;
namespace Chai.Maintenance.Modules.SparepartInventory.Views
{
    public class RequestListPresenter : Presenter<IRequestListView>
    {
        private SparepartInventoryController _controller;
        private AdminController _admincontrller;
        public RequestListPresenter([CreateNew] SparepartInventoryController controller,[CreateNew] AdminController admincontroller) 
        {
            this._controller = controller;
            this._admincontrller = admincontroller;
        }
        public void OnViewLoaded()
        {
            View.Requests = _controller.RequestListObject;
            View.user = _controller.CurrentUser;
        }
        public override void OnViewInitialized()
        {



        }
        public void  GetRequestList(int findby, string value, string dateFrom, string dateto,int regionId,int userId)
        {
            _controller.GetRequests(findby, value, dateFrom, dateto, regionId, userId);
             View.Requests = _controller.RequestListObject;
        }
        public void GetRequestListforApprovals(int findby, string value, string dateFrom, string dateto, int regionId, int userId)
        {
            _controller.GetRequestsforApprovals(findby, value, dateFrom, dateto, regionId, userId);
            View.Requests = _controller.RequestListObject;
        }
        public IList<User> GetUsers(int regionId)
        {
           return _admincontrller.GetUsers(regionId);
        }
    }
}
