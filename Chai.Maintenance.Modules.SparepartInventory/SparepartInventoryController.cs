﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.Practices.CompositeWeb;
using Microsoft.Practices.CompositeWeb.Interfaces;
using Microsoft.Practices.CompositeWeb.Utility;
using Microsoft.Practices.ObjectBuilder;
using Chai.Maintenance.CoreDomain;
using Chai.Maintenance.CoreDomain.SparepartInventory;
using Chai.Maintenance.CoreDomain.Configuration;
using Chai.Maintenance.Services.Configuration;
using Chai.Maintenance.Shared.Navigation;
using Chai.Maintenance.Services;
using Chai.Maintenance.Services.SparepartInventory;
namespace Chai.Maintenance.Modules.SparepartInventory
{
    
    public class SparepartInventoryController
    {
        private IHttpContextLocatorService _httpContextLocatorService;
        private INavigationService _navigationService;
        private ISessionStateLocatorService _sessionstate;
        private RecieveServices _recieveservices;
        private RecieveItemDetailServices _recieveItemDetailServices;
        private RequestServices _requestservice;
        private RequestItemdetailServices _requestitemservices;
        private ApprovalServices _approvalservices;
        private IssueServices _issueservices;
        private ApprovalItemDetailServices _approvaldetailservices;
        private IssueItemDetailServices _issuedetailservices;
        private StockQtyServices _stockservices;
        [InjectionConstructor]
        public SparepartInventoryController([ServiceDependency] IHttpContextLocatorService httpContextLocatorService,
            [ServiceDependency] INavigationService navigationService,[ServiceDependency] ISessionStateLocatorService sessionstate)
        {
            _httpContextLocatorService = httpContextLocatorService;
            _navigationService = navigationService;
            _sessionstate = sessionstate;
        }
        public User CurrentUser
        {
            get { return _httpContextLocatorService.GetCurrentContext().User.Identity as User; }
        }
        public void Navigate(string to)
        {
            _navigationService.Navigate(to);
        }
        #region Recieve
        public Recieve RecieveObject
        {
            get
            {
                Recieve _recieve = _sessionstate.GetSessionState()["Recieve"] as Recieve;

                if (_recieve != null)
                    return _recieve;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["Recieve"] = value;

            }
        }

        public IList<Recieve> RecieveListObject
        {
            get
            {
                IList<Recieve> _recieve = _sessionstate.GetSessionState()["Recieve"] as IList<Recieve>;

                if (_recieve != null)
                    return _recieve;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["Recieve"] = value;

            }
        }
        public void GetRecieves(int findby, string value,string datefrom,string dateto,int regionId)
        {

            if (_recieveservices == null)
                _recieveservices = new RecieveServices();

            RecieveListObject = _recieveservices.GetAllRecieve(findby, value, datefrom, dateto, regionId);

        }

        public void GetRecieveById(int recieveid)
        {
            if (_recieveservices == null)
                _recieveservices = new RecieveServices();

            this.RecieveObject = _recieveservices.GetRecieveById(recieveid);
        }

        public void SaveOrUpdateRecieve(Recieve recieve)
        {

            if (_recieveservices == null)
                _recieveservices = new RecieveServices();
            _recieveservices.SaveOrUpdateRecieve(recieve);
            this.RecieveObject = recieve;
        }
        public void NewRecieve()
        {
            Recieve recieve = new Recieve();
            this.RecieveObject = recieve;
        }
        public void DeleteRecieve(int recieveid)
        {
            if (_recieveservices == null)
                _recieveservices = new RecieveServices();
            _recieveservices.DeleteRecieve(recieveid);
        }
        public void DeleteRecieveDetail(int Id)
        {
            _recieveItemDetailServices = new RecieveItemDetailServices();
           _recieveItemDetailServices.DeleteRecieveDetail(Id);
        }
        public void postItems(IList<RecieveItemDetail> ItemDetail,int regionid,int recieveId)
        {
            _recieveItemDetailServices = new RecieveItemDetailServices();
            _recieveItemDetailServices.PostItems(ItemDetail, regionid, recieveId);
        }
        #endregion
        #region Request
        public Request RequestObject
        {
            get
            {
                Request _request = _sessionstate.GetSessionState()["Request"] as Request;

                if (_request != null)
                    return _request;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["Request"] = value;

            }
        }

        public IList<Request> RequestListObject
        {
            get
            {
                IList<Request> _request = _sessionstate.GetSessionState()["Request"] as IList<Request>;

                if (_request != null)
                    return _request;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["Request"] = value;

            }
        }
        public void GetRequests(int findby, string value, string datefrom, string dateto,int regionId,int userId)
        {

            if (_requestservice == null)
                _requestservice = new RequestServices();

            RequestListObject = _requestservice.GetAllRequest(findby, value, datefrom, dateto, regionId, userId);

        }
        public void GetRequestsforApprovals(int findby, string value, string datefrom, string dateto, int regionId, int userId)
        {

            if (_requestservice == null)
                _requestservice = new RequestServices();

            RequestListObject = _requestservice.GetAllRequestforapprovals(findby, value, datefrom, dateto, regionId, userId);

        }
        public void GetRequestById(int requestid)
        {
            if (_requestservice == null)
                _requestservice = new RequestServices();

            this.RequestObject = _requestservice.GetRequestById(requestid);
        }

        public void SaveOrUpdateRequest(Request request)
        {

            if (_recieveservices == null)
                _requestservice = new RequestServices();
            _requestservice.SaveOrUpdateRequest(request);
            this.RequestObject = request;
        }
        public void NewRequest()
        {
            Request request = new Request();
            this.RequestObject = request;
        }
        public void DeleteRequest(int requestid)
        {
            if (_requestservice == null)
                _requestservice = new RequestServices();
            _requestservice.DeleteRequest(requestid);
        }
        public void DeleteRequestDetail(int Id)
        {
            _requestitemservices = new RequestItemdetailServices();
            _requestitemservices.DeleteRecieveDetail(Id);
        }
        
        #endregion
        #region Approval
        public Approval ApprovalObject
        {
            get
            {
                Approval _aproval = _sessionstate.GetSessionState()["Approval"] as Approval;

                if (_aproval != null)
                    return _aproval;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["Approval"] = value;

            }
        }
        public IList<Approval> ApprovalListObject
        {
            get
            {
                IList<Approval> _aproval = _sessionstate.GetSessionState()["Approval"] as IList<Approval>;

                if (_aproval != null)
                    return _aproval;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["Approval"] = value;

            }
        }

        public void GetApprovals(int findby, string value, string datefrom, string dateto,int regionId,int userId)
        {

            if (_approvalservices == null)
                _approvalservices = new ApprovalServices();

            ApprovalListObject = _approvalservices.GetAllApproval(findby, value, datefrom, dateto, regionId, userId);

        }
        public void GetApprovalsforIssue(int findby, string value, string datefrom, string dateto, int regionId, int userId)
        {

            if (_approvalservices == null)
                _approvalservices = new ApprovalServices();

            ApprovalListObject = _approvalservices.GetAllApprovalforIssue(findby, value, datefrom, dateto, regionId, userId);

        }
        public void GetApprovalById(int approvalid)
        {
            if (_approvalservices == null)
                _approvalservices = new ApprovalServices();

            this.ApprovalObject = _approvalservices.GetApprovalById(approvalid);
        }

        public void SaveOrUpdateApproval(Approval approval)
        {

            if (_approvalservices == null)
                _approvalservices = new ApprovalServices();
            _approvalservices.SaveOrUpdateApproval(approval);
            this.ApprovalObject = approval;
        }
        public void NewApproval()
        {
            Approval approval = new Approval();
            this.ApprovalObject = approval;
        }
        public void DeleteApproval(int approvalid)
        {
            if (_approvalservices == null)
                _approvalservices = new ApprovalServices();
            _approvalservices.DeleteApproval(approvalid);
        }
        public void DeleteApprovalDetail(int Id)
        {
            _approvaldetailservices = new ApprovalItemDetailServices();
            _approvaldetailservices.DeleteApprovalDetail(Id);
        }
        public IList<RequestItemDetail> GetListofRequestedItems(int requestid)
        {
            if (_approvalservices == null)
                _approvalservices = new ApprovalServices();
            return _approvalservices.GetListofRequestedItems(requestid);
        }
        #endregion
        #region Issue
        public Issue IssueObject
        {
            get
            {
                Issue _issue = _sessionstate.GetSessionState()["Issue"] as Issue;

                if (_issue != null)
                    return _issue;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["Issue"] = value;

            }
        }

        public IList<Issue> IssueListObject
        {
            get
            {
                IList<Issue> _issue = _sessionstate.GetSessionState()["Issue"] as IList<Issue>;

                if (_issue != null)
                    return _issue;
                return null;
            }
            set
            {
                _sessionstate.GetSessionState()["Issue"] = value;

            }
        }

        public void GetIssues(int findby, string value, string datefrom, string dateto,int regionId)
        {

            if (_issueservices == null)
                _issueservices = new IssueServices();

            this.IssueListObject = _issueservices.GetAllissue(findby, value, datefrom, dateto, regionId);

        }

        public void GetIssueById(int issueid,int RegionId)
        {
            if (_issueservices == null)
                _issueservices = new IssueServices();

            this.IssueObject = _issueservices.GetIssueById(issueid,RegionId);
        }

        public void SaveOrUpdateIssue(Issue issue)
        {

            if (_issueservices == null)
                _issueservices = new IssueServices();
            _issueservices.SaveOrUpdateIssue(issue);
            this.IssueObject = issue;
        }
        public void NewIssue()
        {
            Issue issue = new Issue();
            this.IssueObject = issue;
        }
        public void DeleteIssue(int issueid)
        {
            if (_issueservices == null)
                _issueservices = new IssueServices();
            _issueservices.DeleteIssue(issueid);
        }
        public void DeleteDeleteDetail(int Id)
        {
            _issuedetailservices = new IssueItemDetailServices();
            _issuedetailservices.DeleteIssueDetail(Id);
        }
        public IList<ApprovalItemDetail> GetListofapprovalItems(int approvalid,int regionId)
        {
            if (_issueservices == null)
                _issueservices = new IssueServices();
            return _issueservices.GetListofApprovedItems(approvalid, regionId);
        }
        public StockQty GetStockQty(int region_Id, int Sparepart_Id)
        {
            if (_stockservices == null)
                _stockservices = new StockQtyServices();
            return _stockservices.GetStockQtyById(region_Id, Sparepart_Id);
        }
        #endregion
        #region ItemBalance
        public IList<StockQty> GetItembalance(int instrumentid, int Sparepart_Id,int region_Id)
        {
            if (_stockservices == null)
                _stockservices = new StockQtyServices();
            return _stockservices.GetStockQty(instrumentid, Sparepart_Id, region_Id);
        }
        public IList<StockQty> GetReorderSparepart(int region_Id)
        {
            if (_stockservices == null)
                _stockservices = new StockQtyServices();
            return _stockservices.GetReorderSparepart(region_Id);
        }
        #endregion
        #region StockQty
        public IList<StockQty> BeginingBalanceObject
        {
            get 
            
            {
                IList<StockQty> stock = _sessionstate.GetSessionState()["StockQty"] as IList<StockQty>;
                if (stock != null)
               
                    return stock;
                    return null;
                
            }
            set
            {
                _sessionstate.GetSessionState()["StockQty"] = value;
            }
        
        }
        public void GetListofStock(int InstrumentId,int sparepartId,int regionId)
        {
            if (_stockservices == null)
                _stockservices = new StockQtyServices();
            this.BeginingBalanceObject = _stockservices.GetStockQty(InstrumentId,sparepartId,CurrentUser.RegionId);
        }
        public void SaveorUpdateStockQty(StockQty stockqty)
        {
            if (_stockservices == null)
                _stockservices = new StockQtyServices();
            _stockservices.SaveorUpdateStockQty(stockqty);
        
        }
        public void NewStockQty()
        { 
           if (_stockservices == null)
                _stockservices = new StockQtyServices();
           //this.BeginingBalanceObject = _stockservices.NewStockQty();
        }
        public void DeleteStockQty(int Id)
        {
            if (_stockservices == null)
                _stockservices = new StockQtyServices();
            _stockservices.DeleteStockQty(Id);  
        }
        #endregion
    }
}
