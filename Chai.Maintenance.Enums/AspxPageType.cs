using System;
using System.Collections.Generic;

namespace Chai.Maintenance.Enums
{
    public enum AspxPageType
    {        
        PriceListList = 60,

        RegionEdit = 70,
        RegionList = 42,
        SiteList = 90,
        SiteEdit = 91,
        InstrumentCategoryEdit = 10,
        InstrumentCategoryList = 15,

        ManufacturerEdit = 20,
        ManufacturerList = 25,

        SiteTypeEdit = 30,
        SiteTypeList = 35,

        UnitOfMeasurmentEdit = 40,
        UnitOfMeasurmentList = 45,

        VendorEdit = 50,
        VendorList = 55,

        SparepartTypEdit = 60,
        SparepartTypeList = 65,

        InstrumentEdit = 70,
        InstrumentList = 75,

        InstrumentTypeEdit = 80,
        InstrumentTypeList = 85,

        MaintenanceInstrumentList = 100,
        ScheduleCurativeMaintenanceList = 105,

        AssignedJobs = 110,
        SchedulePreventiveMaintenanceList = 120,
        PreventiveMaintenance = 115,

        InstrumentListForTransfer = 130,
        InstrumentListForDisposal = 135,
        MaintenanceConfirmation = 154,

        SchedulesList = 155,
        ProblemList = 160,

        ProblemNotification = 170, // For AutoNumber Purpose

        PreventiveMaintenanceList = 175,

        CurativeMaintenanceList = 180,
        InstrumentDisposalList = 185,
        InstrumentTransferList = 190,
        
	RecieveEdit = 200,
        RecieveList = 201,

        RequestEdit = 202,
        RequestList = 203,

        ApprovalEdit = 204,
        ApprovalList = 205,

        IssueEdit = 206,
        IssueList = 207,

        ViewRequests = 208,
        ViewApprovals =209,

        ItemBalance = 210,
        Reorder=211,
        BeginningBalance = 212,
        // For AutoNumber Purpose//
        Request = 270,
        Approval = 280,
        Issue = 290,

        ConsumableList = 350,
        ConsumableNotificationList = 300,
        ConsumablenotificationEdit = 310,
        ErrorCodeList = 320,
        ProblemTypeList = 330,
        ChecklistList = 340,
            EscalatedJobs = 380,
        ReportViewer_RRegion = 500,
        ReportViewer_RSite = 501,
        ReportViewer_RSiteContact = 502,
        ReportViewer_RInstrument = 503,
        ReportViewer_RInstrumentProblemByManufacturer = 504,
        ReportViewer_RQuarterCurative = 505,
        ReportViewer_RQuarterPreventive = 506,
        ReportViewer_RDisposedInstruments = 507,
        ReportViewer_RNonFunctionalInstrument = 508,
        ReportViewer_RPerformedCurativeMaintenance = 509,
        ReportViewer_RPerformedPreventiveMaintenance = 510,
        ReportViewer_RTransferedInstruments = 511,
        ReportViewer_RUpcomingCurativeMaintenance = 512,
        ReportViewer_RUpcomingPreventiveMaintenance = 513,
        ReportViewer_REscalatedCurativeMaintenances = 514,
        ReportViewer_RNotifiedConsumables = 515,

        ReportViewer_RInstrumentsUnderContract = 560,
        ReportViewer_RCurativeMaintenanceTracking = 600,
        JobPushBackReasonList = 550
    }
}
