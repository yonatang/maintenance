﻿using System;


namespace Chai.Maintenance.Enums
{
    public enum PageViewType
    {
        Action = 1,
        Find = 2,
        Report = 3
    }
}
