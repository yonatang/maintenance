﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chai.Maintenance.Enums
{
    public enum TabType
    {
        Admin        = 0,
        Home         = 1,
        Maintenance      = 2,
        SparepartInventory    = 3,
        Configuration = 4,
        Report     = 5,
        Help=6
     }

    public enum MenuSectionType
    {
        Action ,
        Find ,
        Dropdown,
        Report
    }

    public enum ReportSectionType
    {
    }
}
