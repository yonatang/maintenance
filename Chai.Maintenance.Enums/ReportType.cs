﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ramcs.Accounting.Enums
{
    public enum ReportType
    {
        Company = 1,
        Customers = 2,
        Vendors = 3,
        Inventorys =4,
        Banking = 5,
        Employees = 6,
        Fixed_Assets = 7
    }   
    
}
