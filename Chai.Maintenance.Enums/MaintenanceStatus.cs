﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chai.Maintenance.Enums
{
    public enum MaintenanceStatus
    {
        CurativeMaintenanceFunctionalCheckedStatus = 2,  //Curative Maintenance

        CurativeMaintenanceFunctionalUNCheckedStatus = 6, //Curative Maintenance

        DisposalStatus = 4, //Disposed instrument

        TransferStatus = 3, //Transfered Instrument

       ConfirmationForFuncInstrumentFromCurativeMain = 1, // sets the instrument status to functional Curative
       ConfirmationForFuncInstrumentFromPreventiveMain = 1, // sets the instrument status to functional Preventive

       ConfirmationForNotFuncInstrumentFromCurativeMain = 2, // sets the instrument status to Not functional Curative
       ConfirmationForNotFuncInstrumentFromPreventiveMain = 2, // sets the instrument status to Not functional Preventive
      
        ConfirmationForPreventive = 2, // changes the atstus of table schedule filed Preventive status to 'Performed'
        ConfirmationForCurative = 2,
        ConfirmationForInstrument = 1,////Changes the status of table Instrumnet to FUNCTIONAL

        PreventiveMaintenanceForScheduleStatus = 8, //changes the atstus of table schedule filed Preventive status to 'Needs User Approval'

        PreventiveMaintenanceChangeScheduleToPerformed =1, //changes the status of table schdule filed preventive status to preformed

        CurativeMaintenanceForScheduleStatus = 8,//changes the atstus of table schedule filed Curative status to 'Needs User Approval'

        ProblemNotification = 2, //Changes the status of instrument table status field to Not- Functional (2)
        ProblemNotificationFuntional = 5, // //Changes the status of instrument table status field to   Functional with Problem (5)
        CurativeSchedule = 3, // When Curative Scheduled, changes the notifed problem status to scheduled

        PushJobSchedule = 5, //Changes the status of CurativeSchedulestatus on Schedule to "Pushed"

        EscalatedSchedule = 9 //Changes the status of CurativeMaintenanceStatus on Schedule to "Escalated"
    }
}
