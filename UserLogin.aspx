﻿<%@ page language="C#" autoeventwireup="true" inherits="Chai.Maintenance.Modules.Shell.Views.UserLogin, App_Web_smodhl5n" title="UserLogin" stylesheettheme="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head" runat="server">
    <title>Ramcs Accounting</title>
  
    <link href="Shared/css/960.css" rel="stylesheet" type="text/css" />
    <link href="Shared/css/reset.css" rel="stylesheet" type="text/css" />
    <link href="Shared/css/text.css" rel="stylesheet" type="text/css" />
    <link href="Shared/css/login.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="mainForm" runat="server">
<div class="container_16">
  <div class="grid_6 prefix_5 suffix_5"><br /><br /><br /><br />
   	 <div align=center  style="color:White; font-size:14px; width:100%; ">
ForMain
                        <br />
                    
                      LABORATORY INSTRUMENTS MAINTENANCE TRACKING 
                       SYSTEM<br /></div>
            <h1 style="color:Black"> User Login
            </h1>
    	<div id="login">

         <p>
    	      <label><strong>User Name</strong>
 <asp:TextBox class="inputText"  ID="txtUsername" runat="server"></asp:TextBox>
    	      </label>
  	      </p>
           <p>
    	      <label><strong>Password&nbsp; </strong>
   &nbsp;<asp:TextBox class="inputText"  ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
  	        </label>
    	    </p>
<p>                    <asp:CheckBox ID="chkPersistLogin" runat="server" 
                        Text="Remember Password"  Font-Size="Small" style="font-size: x-small" />
                        </p>

              <p>
                    <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Login" 
                        Width="64px" style="font-size: x-small" />
               </p>

                <p class="error">
                    <asp:Label ID="lblLoginError" runat="server" CssClass="error" EnableViewState="False"></asp:Label>
            </p>

          
    	</div>
      
  </div>
</div>
<br clear="all" />
    </form>
</body>
</html>
