﻿using System;
using System.Data;
using System.Data.SqlClient;
using Chai.Maintenance.Shared.Settings;

namespace Chai.Maintenance.DBConnection
{    
    public class DatabaseConn : IConnectionManager
    {
        private SqlConnection _connection;
        private SqlConnection _connectionOnMasterDatabase;
        private string _connectionStringForRestore;

        private static IConnectionManager _theUniqueInstance;
        private SqlTransaction sqlTransaction;

        public SqlConnection GetSqlConnection()
        {
            return _connection;
        }

        private DatabaseConn()
        {
            InitConnections(TechnicalSettings.DatabaseLoginName, TechnicalSettings.DatabasePassword, TechnicalSettings.DatabaseServerName, TechnicalSettings.DatabaseName, "240");
        }

        private DatabaseConn(string pLogin, string pPassword, string pServer, string pDatabase, string pTimeout)
        {
            InitConnections(pLogin, pPassword, pServer, pDatabase, pTimeout);
        }

        private bool _connectionInitSuceeded = false;

        public bool ConnectionInitSuceeded 
        { 
            get { return _connectionInitSuceeded; } 
        }

        private void InitConnections(string pLogin, string pPassword, string pServer, string pDatabase, string pTimeout)
        {
            try
            {
                _connectionOnMasterDatabase = new SqlConnection(
                   String.Format("user id={0};password={1};data source={2};persist security info=False;initial catalog=MASTER;connection timeout={3}",
                                 pLogin, pPassword, pServer, pTimeout));

                _connectionStringForRestore =
                    String.Format("user id={0};password={1};data source={2};persist security info=False;initial catalog=MASTER;connection timeout={3}",
                                  pLogin, pPassword, pServer, pTimeout);

                _connection = new SqlConnection(
                    String.Format("user id={0};password={1};data source={2};persist security info=False;initial catalog={3};connection timeout={4};Asynchronous Processing=true",
                                  pLogin, pPassword, pServer, pDatabase, pTimeout));
                _connection.Open();

                _connectionInitSuceeded = true;
            }
            catch
            {
                _connectionInitSuceeded = false;
            }
        }

        public static IConnectionManager GetInstance()
        {
            if (_theUniqueInstance == null)
                return _theUniqueInstance = new DatabaseConn();
            else
                return _theUniqueInstance;
        }

        public static void KillSingleton()
        {
            _theUniqueInstance = null;
        }

        public static IConnectionManager GetInstance(string pLogin, string pPassword, string pServer, string pDatabase, string pTimeout)
        {
            if (_theUniqueInstance == null)
                return _theUniqueInstance = new DatabaseConn(pLogin, pPassword, pServer, pDatabase, pTimeout);
            else
                return _theUniqueInstance;
        }

        public SqlConnection SqlConnection
        {
            get
            {
                if (_connection.State == ConnectionState.Closed)
                {
                    try
                    {
                        _connection.Open();
                    }
                    catch (SqlException sqlEx)
                    {
                        throw new ApplicationException("Unable to connect to database (" + _connection.DataSource + "/" + _connection.Database + "). Please contact your local IT administrator.", sqlEx);
                    }
                }
                return _connection;
            }
        }

        public SqlTransaction GetSqlTransaction()
        {
            if (_connection.State == ConnectionState.Closed)
            {
                try
                {
                    _connection.Open();
                }
                catch (SqlException ex)
                {
                    throw new ApplicationException("Unable to connect to database (" + _connection.DataSource + "/" + _connection.Database +
                        "). Please contact your local IT administrator.", ex);
                }
            }
            else
            {
                try
                {
                    throw new ApplicationException("COUCOU");
                }
                catch (ApplicationException ex)
                {
                    System.Diagnostics.Trace.WriteLine(ex.StackTrace);
                }
                sqlTransaction = _connection.BeginTransaction();
            }
            return sqlTransaction;
        }

        public void CloseConnection()
        {
            if (_connection.State != ConnectionState.Closed)
            {
                _connection.Close();
            }
        }
      
        public SqlConnection SqlConnectionOnMaster
        {
            get
            {
                return _connectionOnMasterDatabase;
            }
        }

        public SqlConnection SqlConnectionForRestore
        {
            get
            {
                return new SqlConnection(_connectionStringForRestore);
            }
        }

        public void KillAllConnections()
        {
            String sql = @"DECLARE loop_name INSENSITIVE CURSOR FOR
                      SELECT spid
                       FROM master..sysprocesses
                       WHERE dbid = DB_ID('{0}')

                    OPEN loop_name
                    DECLARE @conn_id SMALLINT
                    DECLARE @exec_str VARCHAR(255)
                    FETCH NEXT FROM loop_name INTO @conn_id
                    WHILE (@@fetch_status = 0)
                      BEGIN
                        SELECT @exec_str = 'KILL ' + CONVERT(VARCHAR(7), @conn_id)
                        EXEC( @exec_str )
                        FETCH NEXT FROM loop_name INTO @conn_id
                      END
                    DEALLOCATE loop_name
                    ";
            _connectionOnMasterDatabase.Open();
            sql = String.Format(sql, TechnicalSettings.DatabaseName);
            SqlCommand cmd = new SqlCommand(sql, _connectionOnMasterDatabase);
            cmd.ExecuteNonQuery();
            _connectionOnMasterDatabase.Close();
        }
                
        #region IConnectionManager Members
        
        public void SetConnection(SqlConnection pConnection)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        public static bool CheckSQLServerConnection()
        {
            string sqlConnection = String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog=MASTER;connection timeout=10",
                TechnicalSettings.DatabaseLoginName, TechnicalSettings.DatabasePassword, TechnicalSettings.DatabaseServerName);
            SqlConnection connection = new SqlConnection(sqlConnection);
            try
            {
                connection.Open();
                connection.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool CheckSQLDatabaseConnection()
        {
            string sqlConnection = String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog={3};connection timeout=10",
                TechnicalSettings.DatabaseLoginName, TechnicalSettings.DatabasePassword, TechnicalSettings.DatabaseServerName,TechnicalSettings.DatabaseName);
            SqlConnection connection = new SqlConnection(sqlConnection);
            try
            {
                connection.Open();
                connection.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static SqlConnection MasterConnection()
        {
            string sqlConnection = String.Format(@"user id={0};password={1};data source={2};persist security info=False;initial catalog=MASTER;connection timeout=10",
                TechnicalSettings.DatabaseLoginName, TechnicalSettings.DatabasePassword, TechnicalSettings.DatabaseServerName);
            return new SqlConnection(sqlConnection);
        }
    }

}
