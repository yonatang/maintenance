﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Chai.Maintenance.DBConnection
{
    public interface IConnectionManager
    {
        void CloseConnection();
        bool ConnectionInitSuceeded { get; }
        void KillAllConnections();
        void SetConnection(SqlConnection pConnection);
        SqlConnection GetSqlConnection();
        SqlTransaction GetSqlTransaction();

        SqlConnection SqlConnection { get; }
        SqlConnection SqlConnectionForRestore { get; }
        SqlConnection SqlConnectionOnMaster { get; }
    }
}
