﻿using System;
using System.Data.SqlClient;
using Chai.Maintenance.Shared.Settings;
using Chai.Maintenance.Shared;

namespace Chai.Maintenance.DBConnection
{
    public class ConnectionManager
    {
        private readonly IConnectionManager _connectionManager;
        private static ConnectionManager _theUniqueInstance;

        private ConnectionManager()
        {
            _connectionManager = DatabaseConn.GetInstance();
        }

        private ConnectionManager(string pLogin, string pPassword, string pServer, string pDatabase, string pTimeout)
        {
            _connectionManager = DatabaseConn.GetInstance(pLogin, pPassword, pServer, pDatabase, pTimeout);
        }

        public static ConnectionManager GetInstance()
        {
            return _theUniqueInstance ?? (_theUniqueInstance = new ConnectionManager());
        }

        public static ConnectionManager GetInstance(string pLogin, string pPassword, string pServer, string pDatabase, string pTimeout)
        {
            return _theUniqueInstance ??  (_theUniqueInstance = new ConnectionManager(pLogin, pPassword, pServer, pDatabase, pTimeout));
        }

        public static void KillSingleton()
        {
            DatabaseConn.KillSingleton();
            _theUniqueInstance = null;
        }

        public SqlConnection SqlConnection
        {
            get { return _connectionManager.SqlConnection; }
        }
                
        public SqlTransaction GetSqlTransaction()
        {
            return _connectionManager.GetSqlTransaction();
        }

        public void CloseConnection()
        {
            _connectionManager.CloseConnection();
        }
        
        public bool ConnectionInitSuceeded
        {
            get { return _connectionManager.ConnectionInitSuceeded; }
        }

        public SqlConnection SqlConnectionOnMaster
        {
            get { return _connectionManager.SqlConnectionOnMaster; }
        }

        public SqlConnection SqlConnectionForRestore
        {
            get { return _connectionManager.SqlConnectionForRestore; }
        }

        public void KillAllConnections()
        {
            _connectionManager.KillAllConnections();
        }

        public void SetConnection(SqlConnection pConnection)
        {
            _connectionManager.SetConnection(pConnection);
        }
        
        public static bool CheckSQLServerConnection()
        {
            return DatabaseConn.CheckSQLServerConnection();
        }

        public static bool CheckSQLDatabaseConnection()
        {
            return DatabaseConn.CheckSQLDatabaseConnection();
        }

        public static SqlConnection GeneralSqlConnection
        {
            get
            {
                return DatabaseConn.MasterConnection();
            }
        }

        public static string ConnectionString
        {
            get
            {
                return TechnicalConfig.GetConfiguration()["ConnectionString"];
            }
        }
    }
}
